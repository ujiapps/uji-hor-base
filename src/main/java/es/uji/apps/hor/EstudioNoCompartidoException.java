package es.uji.apps.hor;

@SuppressWarnings("serial")
public class EstudioNoCompartidoException extends Exception {
    public EstudioNoCompartidoException(String estudio) {
        super("Un o més dels estudis seleccionats no es compartit amb l'estudi '" + estudio + "'");
    }
}
