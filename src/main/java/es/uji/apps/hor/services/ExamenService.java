package es.uji.apps.hor.services;

import es.uji.apps.hor.DiaNoValidoExamenException;
import es.uji.apps.hor.dao.CalendarioAcademicoDAO;
import es.uji.apps.hor.dao.ExamenDAO;
import es.uji.apps.hor.model.*;
import es.uji.commons.rest.Role;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ExamenService
{
    @Autowired
    private ExamenDAO examenDAO;

    @Autowired
    private CalendarioAcademicoDAO calendarioAcademicoDAO;

    public List<Convocatoria> getConvocatorias()
    {
        return examenDAO.getConvocatorias();
    }

    public List<TipoExamen> getTiposExamen()
    {
        return examenDAO.getTiposExamen();
    }

    public Convocatoria getConvocatoriaById(Long convocatoriaId)
            throws RegistroNoEncontradoException
    {
        return examenDAO.getConvocatoriaById(convocatoriaId);
    }

    public List<Examen> getExamenesNoPlanificados(Long estudioId, Long convocatoriaId,
            List<Long> cursosList, Long connectedUserId)
    {
        return examenDAO.getExamenesNoPlanificados(estudioId, convocatoriaId, cursosList);
    }

    public List<Examen> getExamenesPlanificados(Long estudioId, Long convocatoriaId,
            List<Long> cursosList, Long connectedUserId)
    {
        return examenDAO.getExamenesPlanificados(estudioId, convocatoriaId, cursosList);
    }

    public Examen planificaExamen(Long examenId, Long estudioId, Date fecha, Long connectedUserId)
            throws RegistroNoEncontradoException
    {
        Examen examen = examenDAO.getExamenById(examenId);

        Convocatoria convocatoria = examenDAO.getConvocatoriaEstudioById(examen.getConvocatoria()
                .getId(), estudioId, examen.getCursoId());

        if (fecha == null) {
            fecha = convocatoria.getInicio();
        }

        CalendarioAcademico calendarioAcademico = calendarioAcademicoDAO
                .getCalendarioAcademicoByFecha(TipoEstudio.getTipoEstudioId(estudioId), fecha);

        try
        {
            calendarioAcademico.compruebaDiaValidoExamen(TipoEstudio.getTipoEstudioId(estudioId), examen.getPermiteSabados());
            convocatoria.compruebaFechaDentroConvocatoria(fecha);
        }
        catch (DiaNoValidoExamenException e)
        {
            fecha = convocatoria.getInicio();
        }

        examen.planifica(fecha);
        examenDAO.updateExamen(examen);
        return examen;
    }

    public Examen modificaFechaExamen(Long examenId, Long estudioId, Date inicio, Date fin,
            Long connectedUserId) throws RegistroNoEncontradoException, DiaNoValidoExamenException
    {
        Examen examen = examenDAO.getExamenById(examenId);

        if (inicio != null)
        {
            CalendarioAcademico calendarioAcademico = calendarioAcademicoDAO
                    .getCalendarioAcademicoByFecha(TipoEstudio.getTipoEstudioId(estudioId), inicio);

            calendarioAcademico.compruebaDiaValidoExamen(TipoEstudio.getTipoEstudioId(estudioId), examen.getPermiteSabados());

            Convocatoria convocatoria = examenDAO.getConvocatoriaEstudioById(examen.getConvocatoria()
                    .getId(), estudioId, examen.getCursoId());

            convocatoria.compruebaFechaDentroConvocatoria(inicio);
        }

        examen.setFecha(inicio);
        examen.setHoraInicio(inicio);
        examen.setHoraFin(fin);
        examenDAO.updateExamen(examen);
        return examen;
    }

    public Examen getExamenById(Long examenId, Long connectedUserId)
            throws RegistroNoEncontradoException
    {
        return examenDAO.getExamenById(examenId);
    }

    public void updateExamen(Examen examen, Long estudioId, Long connectedUserId)
            throws RegistroNoEncontradoException, DiaNoValidoExamenException
    {
        if (examen.getFecha() != null)
        {
            CalendarioAcademico calendarioAcademico = calendarioAcademicoDAO
                    .getCalendarioAcademicoByFecha(TipoEstudio.getTipoEstudioId(estudioId), examen.getFecha());
            calendarioAcademico.compruebaDiaValidoExamen(TipoEstudio.getTipoEstudioId(estudioId), examen.getPermiteSabados());

            Convocatoria convocatoria = getConvocatoriasByEstudio(estudioId).stream()
                    .filter(c -> c.getId() == examen.getConvocatoria().getId()).findFirst().get();
            convocatoria.compruebaFechaDentroConvocatoria(examen.getFecha());
        }

        examenDAO.updateExamen(examen);
    }

    public List<Curso> getCursosEstudio(Long estudioId, Long connectedUserId)
    {
        return examenDAO.getCursosEstudio(estudioId);
    }

    @Role({ "ADMIN", "USUARIO" })
    public void desplanificaExamen(Long examenId, Long connectedUserId)
            throws RegistroNoEncontradoException
    {
        Examen examen = examenDAO.getExamenById(examenId);

        List<Examen> listaExamenesPorAsignatura = examenDAO
                .getExamenesPlanificadosByAsignaturaAndConvocatoria(examen.getNombre(), examen
                        .getConvocatoria().getId());

        if (listaExamenesPorAsignatura.size() > 1)
        {
            examenDAO.deleteExamenAsignaturaByExamenId(examen.getId());
            examenDAO.deleteExamenAulaByExamenId(examen.getId());
            examenDAO.deleteExamenById(examen.getId());
        }
        else
        {
            examen.desplanifica();
            examenDAO.updateExamen(examen);
        }
    }

    @Role({ "ADMIN", "USUARIO" })
    public void dividirExamen(Long examenId, Long connectedUserId)
            throws RegistroNoEncontradoException
    {
        Examen examen = examenDAO.getExamenById(examenId);

        Examen nuevoExamen = examen.divide();

        examenDAO.insertaExamen(nuevoExamen, examenId);
        examenDAO.updateExamen(examen);
    }

    public List<Convocatoria> getConvocatoriasByEstudio(Long estudioId)
    {
        List<Convocatoria> convocatorias = examenDAO.getConvocatoriasByEstudio(estudioId);

        Map<Long, List<Convocatoria>> convocatoriasById = convocatorias.stream().collect(
                Collectors.groupingBy(Convocatoria::getId));

        List<Convocatoria> convocatoriasPeriodoCompleto = new ArrayList<>();
        for (Map.Entry<Long, List<Convocatoria>> entry : convocatoriasById.entrySet())
        {
            Convocatoria convocatoria = new Convocatoria();
            convocatoria.setId(entry.getKey());
            convocatoria.setInicio(entry.getValue().stream().map(c -> c.getInicio())
                    .min(Date::compareTo).get());
            convocatoria.setFin(entry.getValue().stream().map(c -> c.getFin()).max(Date::compareTo)
                    .get());
            convocatoria.setNombre(entry.getValue().get(0).getNombre());
            convocatoriasPeriodoCompleto.add(convocatoria);
        }

        return convocatoriasPeriodoCompleto;
    }

    public void sincronizaAulas(Long examenId, List<Long> aulaIds)
    {
        examenDAO.borraAulasExamen(examenId);
        for (Long aulaId : aulaIds)
        {
            examenDAO.insertaAulasExamen(examenId, aulaId);
        }
    }

}
