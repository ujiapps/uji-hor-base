package es.uji.apps.hor.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.hor.model.Departamento;
import es.uji.apps.hor.services.DepartamentoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.AccessManager;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("departamento")
public class DepartamentoResource extends CoreBaseService
{
    @InjectParam
    private DepartamentoService departamentoService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getDepartamentos()
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<Departamento> departamentos = departamentoService.getDepartamentos(connectedUserId);

        return modelToUI(departamentos);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("mipod")
    public List<UIEntity> getDepartamentosMiPOD() throws RegistroNoEncontradoException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<Departamento> departamentos = departamentoService.getDepartamentosMidPOD(connectedUserId);

        return modelToUI(departamentos);
    }


    private UIEntity modelToUI(Departamento departamento)
    {
        UIEntity entity = UIEntity.toUI(departamento);
        if (departamento.getCentro() != null)
        {
            entity.put("centroNombre", departamento.getCentro().getNombre());
        }
        return entity;
    }

    private List<UIEntity> modelToUI(List<Departamento> departamentos)
    {
        List<UIEntity> entities = new ArrayList<UIEntity>();
        for (Departamento departamento : departamentos)
        {
            entities.add(modelToUI(departamento));
        }
        return entities;
    }
}