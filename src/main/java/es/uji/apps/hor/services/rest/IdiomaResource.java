package es.uji.apps.hor.services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.hor.AulaNoAsignadaAEstudioDelEventoException;
import es.uji.apps.hor.model.Evento;
import es.uji.apps.hor.model.Idioma;
import es.uji.apps.hor.services.IdiomaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;

@Path("idioma")
public class IdiomaResource extends CoreBaseService
{
    @InjectParam
    private IdiomaService idiomaService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getIdiomas()
    {
        List<Idioma> idiomas = idiomaService.getIdiomas();
        return UIEntity.toUI(idiomas);
    }

    @GET
    @Path("profesor/{profesorId}")

    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getIdiomasItemProfesor(@PathParam("profesorId") Long profesorId,
            @QueryParam("itemId") Long itemId)
    {
        List<Idioma> idiomas = idiomaService.getIdiomasProfesorItem(profesorId, itemId);
        return UIEntity.toUI(idiomas);
    }

    @PUT
    @Path("profesor/{profesorId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateIdiomasItemProfesor(@PathParam("profesorId") Long profesorId,
            UIEntity entity) throws RegistroNoEncontradoException
    {
        String listaIdsIdiomas = entity.get("idiomas");
        Long itemId = ParamUtils.parseLong(entity.get("itemId"));
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        idiomaService.updateIdiomasItemProfesor(profesorId, itemId,
                deserializaIdiomaIds(listaIdsIdiomas), connectedUserId);
        return Response.ok().build();

    }

    private List<Long> deserializaIdiomaIds(String idiomas)
    {
        List<Long> idiomaIds = new ArrayList<>();
        for (String idiomaId : idiomas.split(","))
        {
            if (idiomaId != null && !idiomaId.isEmpty())
            {
                idiomaIds.add(Long.parseLong(idiomaId));
            }
        }
        return idiomaIds;
    }

}
