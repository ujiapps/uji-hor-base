package es.uji.apps.hor.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.hor.NoSePuedeDeshacerException;
import es.uji.apps.hor.dao.LogDAO;
import es.uji.apps.hor.model.Evento;
import es.uji.apps.hor.model.Log;

@Service
public class UndoService
{
    @InjectParam
    private LogDAO logDAO;

    @InjectParam
    private EventosService eventosService;

    // Obtener las acciones que se pueden deshacer, si se hace una acción que no se puede deshacer se invalidan todas las anterioes
    public List<Log> getUndoLogs(String usuario, Long connectedUserId)
    {
        Date hoy = new Date();
        List<Log> logsTodos = logDAO.getLogsByFecha(hoy);

        Log primerCambioNoDeshacibleUsuario = logsTodos.stream().filter(l -> l.getUsuario().equals(usuario) && !l.sePuedeDeshacer() && !l.isDeshecho()).findFirst().orElse(null);
        List<Log> logsUsuario = logsTodos.stream()
                .filter(l -> l.getUsuario().equals(usuario) &&
                        l.sePuedeDeshacer() &&
                        !l.isDeshecho() &&
                        (primerCambioNoDeshacibleUsuario == null || (l.getFecha().after(primerCambioNoDeshacibleUsuario.getFecha()) && !l.getId().equals(primerCambioNoDeshacibleUsuario.getId()))))
                .collect(Collectors.toList());

        // Filtrar las entradas asociadas a los items que han sido modificados por otros usuarios
        List<Log> logsFiltrados = new ArrayList<>();
        for (Log log : logsUsuario)
        {
            if (!logsTodos.stream().anyMatch(l -> !l.getUsuario().equals(usuario) && l.getEvento().getId().equals(log.getEvento().getId()) && l.getFecha().after(log.getFecha())))
            {
                logsFiltrados.add(log);
            }
        }
        return logsFiltrados;
    }

    // Devolver las acciones que se pueden deshacer y no hayan sido invalidadas
    public List<Log> getUndoLogsIsolated(String usuario, Long itemId, Long connectedUserId)
    {
        Date hoy = new Date();
        List<Log> logsTodos = logDAO.getLogsByFecha(hoy);

        List<Log> logsUsuario = logsTodos.stream()
                .filter(l -> l.getUsuario().equals(usuario) &&
                        l.sePuedeDeshacer() &&
                        (itemId == null || l.getEvento().getId().equals(itemId)) &&
                        !l.isDeshecho())
                .collect(Collectors.toList());

        // Filtrar las acciones asociadas a los items que han sido modificados por otros usuarios
        List<Log> logsFiltrados = new ArrayList<>();
        for (Log log : logsUsuario)
        {
            Boolean modificadoPorOtroUsuario = logsTodos.stream().anyMatch(l -> !l.getUsuario().equals(usuario) && l.getEvento().getId().equals(log.getEvento().getId()) && l.getFecha().after(log.getFecha()));
            Boolean existeAccionNoDeshaciblePosterior = logsTodos.stream().anyMatch(l -> l.getEvento().getId().equals(log.getEvento().getId()) && !l.sePuedeDeshacer() && l.getFecha().after(log.getFecha()));
            if (!modificadoPorOtroUsuario && !existeAccionNoDeshaciblePosterior)
            {
                logsFiltrados.add(log);
            }
        }
        return logsFiltrados;
    }

    public Evento undo(Long id, String usuario) throws NoSePuedeDeshacerException
    {
        Log log = logDAO.getLogById(id);
        checkSePuedeDeshacer(log, usuario);
        return eventosService.undo(log);
    }

    private void checkSePuedeDeshacer(Log log, String usuario) throws NoSePuedeDeshacerException
    {
        if (!log.getUsuario().equals(usuario))
        {
            throw new NoSePuedeDeshacerException("No pots desfer canvis fet per un altre usuari");
        }

        if (!log.sePuedeDeshacer())
        {
            throw new NoSePuedeDeshacerException();
        }

        List<Log> logsItem = logDAO.getLogsByItemId(log.getEvento().getId());
        if (!logsItem.isEmpty())
        {
            Log ultimoLog = logsItem.get(0);
            if (!ultimoLog.getId().equals(log.getId()))
            {
                throw new NoSePuedeDeshacerException("Només es pot desfer l'últim canvi fet, és possible que un altre usuari hagi fet una modificació posterior en el subgrup");
            }
        }
    }
}
