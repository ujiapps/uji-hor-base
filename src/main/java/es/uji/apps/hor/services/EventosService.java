package es.uji.apps.hor.services;

import es.uji.apps.hor.*;
import es.uji.apps.hor.dao.*;
import es.uji.apps.hor.db.ItemProfesorDetalleVistaDTO;
import es.uji.apps.hor.model.*;
import es.uji.commons.rest.Role;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class EventosService
{
    private static final int ASIGNAR_CIRCUITO_A_EVENTO = 1;
    private static final int DESASIGNAR_CIRCUITO_DE_EVENTO = 2;

    private final EventosDAO eventosDAO;

    private final AulaDAO aulaDAO;

    @Autowired
    private PersonaDAO personaDAO;

    @Autowired
    private RangoHorarioDAO rangoHorarioDAO;

    @Autowired
    private SemestresDetalleDAO semestresDetalleDAO;

    @Autowired
    private ItemProfesorDetalleVistaDAO itemProfesorDetalleVistaDAO;

    @Autowired
    private CalendarioAcademicoDAO calendarioAcademicoDAO;

    @Autowired
    private AsignaturaDAO asignaturaDAO;

    @Autowired
    private CircuitoDAO circuitoDAO;

    @Autowired
    private LogDAO logDAO;

    @Autowired
    private EstudiosDAO estudioDAO;

    @Autowired
    private LogService logService;

    @Autowired
    public EventosService(EventosDAO eventosDAO, AulaDAO aulaDAO)
    {
        this.eventosDAO = eventosDAO;
        this.aulaDAO = aulaDAO;
    }

    @Role({"ADMIN", "USUARIO"})
    public List<Evento> eventosSemanaGenericaDeUnEstudio(Long estudioId, Long cursoId,
                                                         Long semestreId, List<String> gruposIds, List<String> asignaturasIds,
                                                         List<Long> calendariosIds, Long connectedUserId)
            throws UnauthorizedUserException, RegistroNoEncontradoException
    {
        if (!personaDAO.esAdmin(connectedUserId) && !estudioDAO.isEstudioAbiertoConsulta(estudioId))
        {
            Persona persona = personaDAO.getPersonaConTitulacionesYCentrosById(connectedUserId);
            persona.compruebaAccesoAEstudio(estudioId);
        }

        return eventosDAO.getEventosSemanaGenerica(estudioId, cursoId, semestreId, gruposIds,
                asignaturasIds, calendariosIds);
    }

    @Role({"ADMIN", "USUARIO"})
    public Evento modificaDiaYHoraEvento(Long eventoId, Date inicio, Date fin, String tipoSubgrupoId, Long subgrupoId, Long connectedUserId)
            throws DuracionEventoIncorrectaException, RegistroNoEncontradoException,
            UnauthorizedUserException, EventoFueraDeRangoException
    {
        Evento evento = eventosDAO.getEventoByIdConDetalles(eventoId);
        evento.setFechaInicioYFin(inicio, fin);

        // En los masters se permite el cambio de tipo subgrupo
        if (evento.getTipoEstudioId().equals("M") && tipoSubgrupoId != null)
        {
            evento.setCalendario(new Calendario(tipoSubgrupoId));
            evento.setSubgrupoId(subgrupoId);
        }

        List<RangoHorario> rangosHorarios = rangoHorarioDAO.getRangosHorariosDelEvento(evento);
        evento.compruebaDentroDeLosRangosHorarios(rangosHorarios);

        eventosDAO.updateHorasEventoYSusDetalles(evento);
        return evento;
    }

    public Evento getEventoById(Long eventoId)
            throws RegistroNoEncontradoException
    {
        return eventosDAO.getEventoByIdConDetalles(eventoId);
    }

    @Role({"ADMIN", "USUARIO"})
    public Evento modificaDiaYHoraEventoEnVistaDetalle(Long eventoId, Date inicio, Date fin,
                                                       Long connectedUserId) throws DuracionEventoIncorrectaException,
            RegistroNoEncontradoException, UnauthorizedUserException, EventoFueraDeRangoException,
            EventoMasDeUnaRepeticionException, EventoFueraDeFechasSemestreException,
            DiaNoLectivoException
    {
        Evento evento = eventosDAO.getEventoByIdConDetalles(eventoId);

        if (!personaDAO.esAdmin(connectedUserId))
        {
            Persona persona = personaDAO.getPersonaConTitulacionesYCentrosById(connectedUserId);
            persona.compruebaAccesoAEvento(evento);
        }

        if (elEventoTieneMasDeUnaRepeticion(evento))
        {
            throw new EventoMasDeUnaRepeticionException();
        }

        evento.setFechaInicioYFin(inicio, fin);

        SemestreDetalle semestre = getSemestreDelEvento(evento);
        evento.compruebaDentroFechasSemestre(semestre.getFechaInicio(),
                semestre.getFechaExamenesFin());

        List<RangoHorario> rangosHorarios = rangoHorarioDAO.getRangosHorariosDelEvento(evento);
        evento.compruebaDentroDeLosRangosHorarios(rangosHorarios);

        CalendarioAcademico calendario = calendarioAcademicoDAO.getCalendarioAcademicoByFecha(TipoEstudio.GRADO,
                CalendarioAcademico.getFechaSinHoraEstablecida(evento.getInicio()));
        calendario.compruebaDiaLectivo();

        eventosDAO.updateHorasEventoYSusDetalles(evento);
        return evento;
    }

    private SemestreDetalle getSemestreDelEvento(Evento evento)
    {
        Estudio unEstudio = evento.getAsignaturas().get(0).getEstudio();
        return semestresDetalleDAO.getSemestresDetallesPorEstudioIdYSemestreId(unEstudio.getId(),
                evento.getSemestre().getSemestre()).get(0);
    }

    private boolean elEventoTieneMasDeUnaRepeticion(Evento evento)
    {
        return evento.getEventosDetalle().size() > 1;
    }

    @Role({"ADMIN", "USUARIO"})
    public void deleteEventoSemanaGenerica(Long eventoId, Long connectedUserId, String usuario)
            throws RegistroNoEncontradoException, UnauthorizedUserException
    {
        Evento evento = eventosDAO.getEventoById(eventoId);

        if (!personaDAO.esAdmin(connectedUserId))
        {
            Persona persona = personaDAO.getPersonaConTitulacionesYCentrosById(connectedUserId);
            persona.compruebaAccesoAEvento(evento);
        }

        for (EventoDetalle detalle : evento.getEventosDetalle())
        {
            eventosDAO.deleteEventoDetalle(detalle);
        }

        if (esElultimoEventoAsignadoDelGrupo(evento))
        {
            logService.registraEventoBorrado(Log.ACCION_DESPLANIFICAR_ITEM, evento, usuario, connectedUserId);
            evento.desplanificar();
            eventosDAO.desplanificaEvento(evento);
        }
        else
        {
            logService.registraEventoBorrado(Log.ACCION_BORRAR_ITEM, evento, usuario, connectedUserId);
            eventosDAO.deleteEventoSemanaGenerica(eventoId);
        }
    }

    private long cantidadEventosDelMismoGrupo(Evento evento)
    {

        return getEventosDelMismoGrupo(evento).size();
    }

    private List<Evento> getEventosDelMismoGrupo(Evento eventoReferencia)
    {
        return eventosDAO.getEventosDelMismoGrupo(eventoReferencia);
    }

    private boolean esElultimoEventoAsignadoDelGrupo(Evento evento)
    {
        long cantidadEventosDelMismoGrupo = cantidadEventosDelMismoGrupo(evento);
        return cantidadEventosDelMismoGrupo == 0;
    }

    @Role({"ADMIN", "USUARIO"})
    public void divideEventoSemanaGenerica(Long eventoId, Long connectedUserId)
            throws RegistroNoEncontradoException, EventoNoDivisibleException,
            UnauthorizedUserException
    {
        Evento evento = eventosDAO.getEventoById(eventoId);

        if (!personaDAO.esAdmin(connectedUserId))
        {
            Persona persona = personaDAO.getPersonaConTitulacionesYCentrosById(connectedUserId);
            persona.compruebaAccesoAEvento(evento);
        }

        Evento nuevoEvento = evento.divide();

        eventosDAO.insertEvento(nuevoEvento);
        eventosDAO.updateHorasEventoYSusDetalles(evento);
    }

    @Role({"ADMIN", "USUARIO"})
    public Evento modificaDetallesGrupoAsignatura(Long eventoId, Date inicio, Date fin,
                                                  Date desdeElDia, Integer numeroIteraciones, Integer repetirCadaSemanas, Date hastaElDia,
                                                  Boolean detalleManual, String comentarios, String tipoSubgrupoId, Long subgrupoId, Long connectedUserId)
            throws DuracionEventoIncorrectaException, RegistroNoEncontradoException,
            UnauthorizedUserException
    {

        Evento evento = eventosDAO.getEventoById(eventoId);

        if (!personaDAO.esAdmin(connectedUserId))
        {
            Persona persona = personaDAO.getPersonaConTitulacionesYCentrosById(connectedUserId);
            persona.compruebaAccesoAEvento(evento);
        }

        // En los masters se permite el cambio de tipo subgrupo
        if (evento.getTipoEstudioId().equals("M"))
        {
            evento.setCalendario(new Calendario(tipoSubgrupoId));
            evento.setSubgrupoId(subgrupoId);
        }

        evento.setFechaInicioYFin(inicio, fin);
        evento.setDesdeElDia(desdeElDia);
        evento.setNumeroIteraciones(numeroIteraciones);
        evento.setRepetirCadaSemanas(repetirCadaSemanas);
        evento.setHastaElDia(hastaElDia);
        evento.setDetalleManual(detalleManual);
        evento.setComentarios(comentarios);
        eventosDAO.modificaDetallesGrupoAsignatura(evento);

        return evento;
    }

    @Role({"ADMIN", "USUARIO"})
    public List<EventoDocencia> getDiasDocenciaDeUnEventoByEventoId(Long eventoId,
                                                                    Long connectedUserId)
            throws RegistroNoEncontradoException, UnauthorizedUserException
    {
        Evento evento = eventosDAO.getEventoById(eventoId);

        if (!personaDAO.esAdmin(connectedUserId))
        {
            Persona persona = personaDAO.getPersonaConTitulacionesYCentrosById(connectedUserId);
            persona.compruebaAccesoAEvento(evento);
        }

        return eventosDAO.getDiasDocenciaDeUnEventoByEventoId(eventoId);
    }

    @Role({"ADMIN", "USUARIO"})
    public List<EventoDocencia> getDiasFestivosDeUnEventoByEventoId(Long eventoId,
                                                                    Long connectedUserId)
            throws RegistroNoEncontradoException, UnauthorizedUserException
    {
        return eventosDAO.getDiasFestivosDeUnEventoByEventoId(eventoId);
    }

    public Evento updateEventoConDetalleManual(Long eventoId, List<Date> fechas, Date inicio,
                                               Date fin, String comentarios, String tipoSubgrupoId, Long subgrupoId, Long connectedUserId)
            throws RegistroNoEncontradoException, EventoDetalleSinEventoException,
            DuracionEventoIncorrectaException, UnauthorizedUserException
    {
        Evento evento = eventosDAO.getEventoByIdConDetalles(eventoId);

        if (!personaDAO.esAdmin(connectedUserId))
        {
            Persona persona = personaDAO.getPersonaConTitulacionesYCentrosById(connectedUserId);
            persona.compruebaAccesoAEvento(evento);
        }

        evento.setDetalleManual(true);
        evento.setFechaInicioYFin(inicio, fin);
        evento.setComentarios(comentarios);
        // En los masters se permite el cambio de tipo subgrupo
        if (evento.getTipoEstudioId().equals("M"))
        {
            evento.setCalendario(new Calendario(tipoSubgrupoId));
            evento.setSubgrupoId(subgrupoId);
        }
        eventosDAO.updateEvento(evento);
        sincronizaFechasConEventosDetalle(fechas, evento);

        return evento;
    }

    private void sincronizaFechasConEventosDetalle(List<Date> fechas, Evento evento)
            throws EventoDetalleSinEventoException
    {
        for (Date fecha : fechas)
        {
            if (!evento.tieneDetalleEnFecha(fecha))
            {
                EventoDetalle detalle = evento.creaDetalleEnFecha(fecha);
                eventosDAO.insertEventoDetalle(detalle);
            }
        }

        List<EventoDetalle> eventosAEliminar = new ArrayList<>();
        for (EventoDetalle detalle : evento.getEventosDetalle())
        {
            if (!contieneFecha(fechas, detalle.getInicio()))
            {
                eventosAEliminar.add(detalle);
            }
        }

        for (EventoDetalle detalle : eventosAEliminar)
        {
            evento.getEventosDetalle().remove(detalle);
            eventosDAO.deleteEventoDetalle(detalle);
        }
    }

    private boolean contieneFecha(List<Date> fechas, Date fecha)
    {
        for (Date f : fechas)
        {
            if (DateUtils.isSameDay(f, fecha))
            {
                return true;
            }
        }
        return false;
    }

    @Role({"ADMIN", "USUARIO"})
    public List<EventoDetalle> eventosDetalleDeUnEstudio(Long estudioId, Long cursoId,
                                                         Long semestreId, List<String> gruposIds, List<String> asignaturasIds,
                                                         List<Long> calendariosIds, Date rangoFechaInicio, Date rangoFechaFin,
                                                         Long connectedUserId)
            throws UnauthorizedUserException, RegistroNoEncontradoException
    {

        if (!personaDAO.esAdmin(connectedUserId) && !estudioDAO.isEstudioAbiertoConsulta(estudioId))
        {
            Persona persona = personaDAO.getPersonaConTitulacionesYCentrosById(connectedUserId);
            persona.compruebaAccesoAEstudio(estudioId);
        }

        List<EventoDetalle> listaEventos = eventosDAO.getEventosDetalle(estudioId, cursoId,
                semestreId, gruposIds, asignaturasIds, calendariosIds, rangoFechaInicio,
                rangoFechaFin);

        List<Long> eventoIds = listaEventos.stream().map(e -> e.getEvento().getId()).distinct()
                .collect(Collectors.toList());
        List<Long> eventosIdEditablesEnSemanaDetalle = eventosDAO
                .getEventosEditablesEnSemanaDetalleByEventoIds(eventoIds).stream()
                .map(e -> e.getId()).collect(Collectors.toList());

        for (EventoDetalle eventoDetalle : listaEventos)
        {
            eventoDetalle.setEditable(
                    eventosIdEditablesEnSemanaDetalle.contains(eventoDetalle.getEvento().getId()));
        }

        return listaEventos;
    }

    @Role({"ADMIN", "USUARIO"})
    public List<Evento> actualizaAulaAsignadaAEvento(Long eventoId, Long aulaId, boolean propagar,
                                                     Long connectedUserId, String usuario)
            throws RegistroNoEncontradoException,
            AulaNoAsignadaAEstudioDelEventoException, UnauthorizedUserException
    {
        Evento eventoOriginal = getEventoById(eventoId);
        Evento evento = eventosDAO.getEventoById(eventoId);

        if (!personaDAO.esAdmin(connectedUserId))
        {
            Persona persona = personaDAO.getPersonaConTitulacionesYCentrosById(connectedUserId);
            persona.compruebaAccesoAEvento(evento);
        }

        Aula aula = null;

        if (aulaId != null)
        {
            aula = aulaDAO.getAulaYPlanificacionesByAulaId(aulaId);
        }
        actualizaAula(evento, aula);
        logService.registraAsinacionDeAulaAEvento(eventoOriginal, evento, usuario, connectedUserId);

        List<Evento> eventos = new ArrayList<Evento>();

        if (propagar)
        {
            eventos = getEventosDelMismoGrupo(evento);
            for (Evento grupoComun : eventos)
            {
                eventoOriginal = getEventoById(grupoComun.getId());
                if (grupoComun.getDia() != null) {
                    actualizaAula(grupoComun, aula);
                    logService.registraAsinacionDeAulaAEvento(eventoOriginal, evento, usuario, connectedUserId);
                }
            }
        }
        eventos.add(evento);

        return eventos;
    }

    private void actualizaAula(Evento evento, Aula aula)
            throws AulaNoAsignadaAEstudioDelEventoException, RegistroNoEncontradoException
    {
        if (aula != null)
        {
            evento.actualizaAula(aula);
            eventosDAO.actualizaAulaAsignadaAEvento(evento.getId(), aula.getId());
        }
        else
        {
            evento.desasignaAula();
            eventosDAO.desasignaAula(evento.getId());
        }
    }

    @Role({"ADMIN", "USUARIO"})
    public List<EventoDetalle> getEventosDetallePorAula(Long aulaId, Long semestreId,
                                                        List<Long> calendariosIds, Date rangoFechaInicio, Date rangoFechaFin,
                                                        Long connectedUserId)
            throws RegistroNoEncontradoException, UnauthorizedUserException
    {
        if (!personaDAO.esAdmin(connectedUserId)
                && !personaDAO.isAulaAutorizada(aulaId, connectedUserId))
        {
            throw new UnauthorizedUserException();
        }

        return eventosDAO.getEventosDetallePorAula(aulaId, semestreId, calendariosIds,
                rangoFechaInicio, rangoFechaFin);
    }

    @Role({"ADMIN", "USUARIO"})
    public List<Evento> getEventosSemanaGenericaPorAula(Long aulaId, Long semestreId,
                                                        List<Long> calendariosIds, Long connectedUserId)
            throws RegistroNoEncontradoException, UnauthorizedUserException
    {
        if (!personaDAO.esAdmin(connectedUserId)
                && !personaDAO.isAulaAutorizada(aulaId, connectedUserId))
        {
            throw new UnauthorizedUserException();
        }

        return eventosDAO.getEventosSemanaGenericaPorAula(aulaId, semestreId, calendariosIds);
    }

    @Role({"ADMIN", "USUARIO"})
    public List<Evento> eventosSemanaGenericaCircuito(Long estudioId, Long semestreId,
                                                      String grupoId, Long circuitoId, List<Long> calendariosList, Long connectedUserId)
            throws RegistroNoEncontradoException, UnauthorizedUserException
    {

        if (!personaDAO.esAdmin(connectedUserId) && !estudioDAO.isEstudioAbiertoConsulta(estudioId))
        {
            Persona persona = personaDAO.getPersonaConTitulacionesYCentrosById(connectedUserId);
            persona.compruebaAccesoAEstudio(estudioId);
        }

        List<Evento> eventos = new ArrayList<Evento>();

        if (calendariosList.size() > 0)
        {
            List<Evento> todosEventos = eventosDAO.getTodosLosEventosSemanaGenericaCircuito(
                    estudioId, semestreId, grupoId, circuitoId);

            for (Evento evento : todosEventos)
            {
                if (circuitoId == null
                        || (calendariosList.contains(TipoCalendario.FC.getCalendarioId())
                        && (evento.getCircuito() == null)))
                {
                    evento.setCalendarioCircuito(CalendarioCircuito.getCalendarioCircuitoFC());
                }
                else if (circuitoId != null
                        && calendariosList.contains(TipoCalendario.EC.getCalendarioId())
                        && evento.getCircuito() != null
                        && evento.getCircuito().getId().equals(circuitoId))
                {
                    evento.setCalendarioCircuito(CalendarioCircuito.getCalendarioCircuitoEC());
                }

                if (evento.getCalendarioCircuito() != null)
                {
                    eventos.add(evento);
                }
            }
        }

        return eventos;
    }

    @Role({"ADMIN", "USUARIO"})
    public List<Evento> eventosNoPlanificadosCircuito(Long estudioId, Long semestreId,
                                                      String grupoId, Long circuitoId, List<Long> calendariosList, Long connectedUserId)
            throws RegistroNoEncontradoException, UnauthorizedUserException
    {
        if (!personaDAO.esAdmin(connectedUserId) && !estudioDAO.isEstudioAbiertoConsulta(estudioId))
        {
            Persona persona = personaDAO.getPersonaConTitulacionesYCentrosById(connectedUserId);
            persona.compruebaAccesoAEstudio(estudioId);
        }

        List<Evento> eventos = new ArrayList<Evento>();

        if (calendariosList.size() > 0)
        {
            List<Evento> todosEventos = eventosDAO.getEventosNoPlanificadosSemanaGenericaCircuito(
                    estudioId, semestreId, grupoId, circuitoId);

            for (Evento evento : todosEventos)
            {
                if (circuitoId == null
                        || (calendariosList.contains(TipoCalendario.FC.getCalendarioId())
                        && (evento.getCircuito() == null)))
                {
                    evento.setCalendarioCircuito(CalendarioCircuito.getCalendarioCircuitoFC());
                }
                else if (circuitoId != null
                        && calendariosList.contains(TipoCalendario.EC.getCalendarioId())
                        && evento.getCircuito() != null
                        && evento.getCircuito().getId().equals(circuitoId))
                {
                    evento.setCalendarioCircuito(CalendarioCircuito.getCalendarioCircuitoEC());
                }

                if (evento.getCalendarioCircuito() != null)
                {
                    eventos.add(evento);
                }
            }
        }

        return eventos;
    }

    @Role({"ADMIN", "USUARIO"})
    public void gestionaEventoConCircuito(Long eventoId, Long circuitoId, Long estudioId,
                                          int tipoAccion, Long connectedUserId)
            throws RegistroNoEncontradoException, UnauthorizedUserException, GeneralHORException
    {
        Evento evento = eventosDAO.getEventoById(eventoId);

        if (!personaDAO.esAdmin(connectedUserId))
        {
            Persona persona = personaDAO.getPersonaConTitulacionesYCentrosById(connectedUserId);
            persona.compruebaAccesoAEvento(evento);
        }

        Circuito circuito = circuitoDAO.getCircuitoById(circuitoId);
        circuito.compruebaEstudioEnCircuito(evento);

        List<Evento> eventos = eventosDAO.getEventosDelMismoGrupoIncluyendoAnuales(evento);

        switch (tipoAccion)
        {
            case ASIGNAR_CIRCUITO_A_EVENTO:
                asignaCircuitoAEventos(eventos, circuito);
                break;
            case DESASIGNAR_CIRCUITO_DE_EVENTO:
                desasignaCircuitoAEventos(eventos, circuito);
                break;
            default:
                throw new GeneralHORException();
        }
    }

    private void asignaCircuitoAEventos(List<Evento> eventos, Circuito circuito)
            throws RegistroNoEncontradoException
    {
        List<Long> eventosIds = new ArrayList<Long>();

        for (Evento evento : eventos)
        {
            evento.setCircuito(circuito);
            evento.setCalendarioCircuito(CalendarioCircuito.getCalendarioCircuitoEC());
            eventosIds.add(evento.getId());
        }

        eventosDAO.asignaCircuitoAEventos(eventosIds, circuito.getId());
    }

    private void desasignaCircuitoAEventos(List<Evento> eventos, Circuito circuito)
            throws RegistroNoEncontradoException
    {
        List<Long> eventosIds = new ArrayList<Long>();

        for (Evento evento : eventos)
        {
            evento.unsetCircuito();
            evento.setCalendarioCircuito(CalendarioCircuito.getCalendarioCircuitoFC());
            eventosIds.add(evento.getId());
        }

        eventosDAO.desasignaCircuitoDeEventos(eventosIds, circuito.getId());
    }

    @Role({"ADMIN", "USUARIO"})
    public List<EventoDetalle> eventosSemanaDetalleCircuito(Long estudioId, Long semestreId,
                                                            String grupoId, Long circuitoId, List<Long> calendariosList, Date rangoFechaInicio,
                                                            Date rangoFechaFin, Long connectedUserId)
            throws RegistroNoEncontradoException, UnauthorizedUserException
    {
        if (!personaDAO.esAdmin(connectedUserId) && !estudioDAO.isEstudioAbiertoConsulta(estudioId))
        {
            Persona persona = personaDAO.getPersonaConTitulacionesYCentrosById(connectedUserId);
            persona.compruebaAccesoAEstudio(estudioId);
        }

        List<EventoDetalle> todosEventos = eventosDAO.getTodosLosEventosSemanaDetalleCircuito(
                estudioId, semestreId, grupoId, circuitoId, rangoFechaInicio, rangoFechaFin);

        List<EventoDetalle> eventosDetalle = new ArrayList<EventoDetalle>();

        for (EventoDetalle eventoDetalle : todosEventos)
        {
            Evento evento = eventoDetalle.getEvento();

            if (circuitoId == null || (calendariosList.contains(TipoCalendario.FC.getCalendarioId())
                    && (evento.getCircuito() == null)))
            {
                evento.setCalendarioCircuito(CalendarioCircuito.getCalendarioCircuitoFC());
            }
            else if (circuitoId != null
                    && calendariosList.contains(TipoCalendario.EC.getCalendarioId())
                    && evento.getCircuito() != null
                    && evento.getCircuito().getId().equals(circuitoId))
            {
                evento.setCalendarioCircuito(CalendarioCircuito.getCalendarioCircuitoEC());
            }

            if (evento.getCalendarioCircuito() != null)
            {
                eventosDetalle.add(eventoDetalle);
            }
        }

        return eventosDetalle;
    }

    public Evento getEventoById(Long eventoId, Long connectedUserId)
            throws RegistroNoEncontradoException
    {
        return eventosDAO.getEventoById(eventoId);
    }

    @Role({"ADMIN", "USUARIO"})
    public List<Evento> eventosSemanaGenericaPlanificadosByAsignaturaAndSemestre(
            String asignaturaId, Long semestreId, List<String> gruposList,
            List<String> tipoSubgruposList, Long connectedUserId)
            throws RegistroNoEncontradoException, UnauthorizedUserException
    {
        return eventosDAO.getEventosSemanaGenericaPlanificadosPorAsignaturaIdYSemestreId(
                asignaturaId, semestreId, gruposList, tipoSubgruposList);
    }

    @Role({"ADMIN", "USUARIO"})
    public List<Evento> getEventosByAsignatura(String asignaturaId, Long connectedUserId)
            throws RegistroNoEncontradoException, UnauthorizedUserException
    {
        return eventosDAO.getEventosByAsignatura(asignaturaId);
    }

    public List<Subgrupo> subgruposPorAsignaturaEstudioYCurso(String asignaturaId, Long estudioId,
                                                              Long cursoId)
            throws RegistroNoEncontradoException, UnauthorizedUserException
    {
        return eventosDAO.subgruposPorAsignaturaEstudioYCurso(asignaturaId, estudioId, cursoId);
    }

    @Role({"ADMIN", "USUARIO"})
    public List<EventoDetalle> eventosSemanaDetallePlanificadosByAsignaturaAndSemestre(
            String asignaturaId, Long semestreId, List<String> gruposList,
            List<String> tipoSubgruposList, Long connectedUserId)
            throws RegistroNoEncontradoException, UnauthorizedUserException
    {
        List<EventoDetalle> eventosDetalle = eventosDAO
                .getEventosSemanaDetallePlanificadosPorAsignaturaIdYSemestreId(asignaturaId,
                        semestreId, gruposList, tipoSubgruposList);

        List<Long> listaItemIds = obtenerListaEventoIds(eventosDetalle);
        List<ItemProfesorDetalleVistaDTO> itemProfesorDetalleVistaDTOs = itemProfesorDetalleVistaDAO
                .getEventosProfesorSemanaDetallePlanificadosPorItemId(listaItemIds);

        Map<Long, List<Profesor>> mapItemProfesor = generaHashMapEventoDetalleIdProfesor(
                itemProfesorDetalleVistaDTOs);
        for (EventoDetalle eventoDetalle : eventosDetalle)
        {
            eventoDetalle.getProfesores().clear();
            if (mapItemProfesor.containsKey(eventoDetalle.getId()))
            {
                List<Profesor> profesores = new ArrayList<>();
                for (Profesor profesor : mapItemProfesor.get(eventoDetalle.getId()))
                {
                    profesores.add(profesor);
                }
                eventoDetalle.setProfesores(profesores);
            }
        }

        return eventosDetalle;
    }

    private Map<Long, List<Profesor>> generaHashMapEventoDetalleIdProfesor(
            List<ItemProfesorDetalleVistaDTO> itemProfesorDetalleVistaDTOs)
    {
        Map<Long, List<Profesor>> mapEventoProfesores = new HashMap<>();

        for (ItemProfesorDetalleVistaDTO itemProfesorDetalleVistaDTO : itemProfesorDetalleVistaDTOs)
        {
            if (itemProfesorDetalleVistaDTO.getSeleccionado().equals("S"))
            {
                Long detalleId = itemProfesorDetalleVistaDTO.getDetalleId();

                Profesor profesor = new Profesor();
                profesor.setId(itemProfesorDetalleVistaDTO.getProfesorId());
                profesor.setNombre(itemProfesorDetalleVistaDTO.getProfesor());

                if (mapEventoProfesores.containsKey(detalleId))
                {
                    mapEventoProfesores.get(detalleId).add(profesor);
                }
                else
                {
                    List<Profesor> profesores = new ArrayList<>();
                    profesores.add(profesor);
                    mapEventoProfesores.put(detalleId, profesores);
                }
            }
        }
        return mapEventoProfesores;
    }

    private List<Long> obtenerListaEventoIds(List<EventoDetalle> eventosDetalle)
    {
        Set<Long> eventoIds = new HashSet<>();

        for (EventoDetalle eventoDetalle : eventosDetalle)
        {
            eventoIds.add(eventoDetalle.getEvento().getId());
        }
        return new ArrayList<>(eventoIds);
    }

    @Role({"ADMIN", "USUARIO"})
    public List<Evento> eventosSemanaGenericaNoPlanificadosByAsignaturaAndSemestre(
            String asignaturaId, Long semestreId, List<String> gruposList,
            List<String> tipoSubgruposList, Long connectedUserId)
            throws RegistroNoEncontradoException, UnauthorizedUserException
    {
        List<Evento> eventos = eventosDAO
                .getEventosSemanaGenericaNoPlanificadosPorAsignaturaIdYSemestreIdConProfesores(
                        asignaturaId, semestreId, gruposList, tipoSubgruposList);

        return eventos;
    }

    @Role({"ADMIN", "USUARIO"})
    public List<Evento> getEventosMismoGrupoConDetalleProfesor(Long eventoId, Long profesorId,
                                                               Long connectedUserId)
            throws RegistroNoEncontradoException
    {
        Evento evento = eventosDAO.getEventoById(eventoId);
        return eventosDAO.getEventosMismoGrupoConDetalleProfesor(evento, profesorId);
    }

    public List<EventoDocencia> getFechasEventoByProfesor(Long profesorId, Long eventoId,
                                                          Long connectedUserId)
    {
        List<EventoDocencia> eventosProfesor = eventosDAO.getFechasEventoByProfesor(profesorId,
                eventoId);
        List<EventoDocencia> eventosOcupados = eventosDAO.getFechasEventoSeleccionadasDetalleManualOtrosProfesores(profesorId,
                eventoId);

        for (EventoDocencia eventoDocencia : eventosProfesor)
        {
            if (eventosOcupados.stream()
                    .anyMatch(ed -> ed.getFecha().equals(eventoDocencia.getFecha())))
            {
                eventoDocencia.setDocencia("X");
            }
        }
        return eventosProfesor;
    }

    @Role({"ADMIN", "USUARIO"})
    public List<Evento> eventosProfesor(Long profesorId, String asignaturaId, Long semestreId,
                                        Long connectedUserId)
    {
        return eventosDAO.getEventosProfesor(profesorId, asignaturaId, semestreId);
    }

    @Role({"ADMIN", "USUARIO"})
    public List<Evento> eventosArea(Long areaId, Long semestreId, Long connectedUserId)
    {
        List<String> asignaturaIds = asignaturaDAO
                .getAsignaturasByAreaIdAndSemestreId(areaId, semestreId).stream()
                .map(asig -> asig.getId()).collect(Collectors.toList());
        return eventosDAO.getEventosByListaAsignaturasYSemestre(asignaturaIds, semestreId);
    }

    @Role({"ADMIN", "USUARIO"})
    public List<Evento> getEventosProfesorNoPlanificadosQueNoPertenezcanAsignatura(Long profesorId,
                                                                                   String asignaturaId, Long semestreId, Long connectedUserId)
    {
        return eventosDAO.getEventosProfesorNoPlanificadosQueNoPertenezcanAsignatura(profesorId,
                asignaturaId, semestreId);
    }

    @Role({"ADMIN", "USUARIO"})
    public List<Evento> eventosSemanaGenericaDeUnaAgrupacion(Long estudioId, Long agrupacionId,
                                                             Long semestreId, List<String> gruposIds, List<String> asignaturasIds,
                                                             List<Long> calendariosIds, Long connectedUserId)
            throws UnauthorizedUserException, RegistroNoEncontradoException
    {
        if (!personaDAO.esAdmin(connectedUserId) && !estudioDAO.isEstudioAbiertoConsulta(estudioId))
        {
            Persona persona = personaDAO.getPersonaConTitulacionesYCentrosById(connectedUserId);
            persona.compruebaAccesoAEstudio(estudioId);
        }

        return eventosDAO.getEventosSemanaGenericaByAgrupacion(estudioId, agrupacionId, semestreId,
                gruposIds, asignaturasIds, calendariosIds);
    }

    @Role({"ADMIN", "USUARIO"})
    public List<EventoDetalle> eventosDetalleDeUnEstudioPorAgrupacion(Long estudioId,
                                                                      Long agrupacionId, Long semestreId, List<String> gruposIds, List<String> asignaturasIds,
                                                                      List<Long> calendariosIds, Date rangoFechaInicio, Date rangoFechaFin,
                                                                      Long connectedUserId)
            throws UnauthorizedUserException, RegistroNoEncontradoException
    {
        if (!personaDAO.esAdmin(connectedUserId) && !estudioDAO.isEstudioAbiertoConsulta(estudioId))
        {
            Persona persona = personaDAO.getPersonaConTitulacionesYCentrosById(connectedUserId);
            persona.compruebaAccesoAEstudio(estudioId);
        }

        List<EventoDetalle> listaEventos = eventosDAO.getEventosDetallePorAgrupacion(estudioId,
                agrupacionId, semestreId, gruposIds, asignaturasIds, calendariosIds,
                rangoFechaInicio, rangoFechaFin);

        List<Long> eventoIds = listaEventos.stream().map(e -> e.getEvento().getId()).distinct()
                .collect(Collectors.toList());
        List<Long> eventosIdEditablesEnSemanaDetalle = eventosDAO
                .getEventosEditablesEnSemanaDetalleByEventoIds(eventoIds).stream()
                .map(e -> e.getId()).collect(Collectors.toList());

        for (EventoDetalle eventoDetalle : listaEventos)
        {
            eventoDetalle.setEditable(
                    eventosIdEditablesEnSemanaDetalle.contains(eventoDetalle.getEvento().getId()));
        }

        return listaEventos;
    }

    public Evento undo(Log log)
    {
        Evento evento = eventosDAO.undo(log);
        logDAO.invalidaLog(log.getId());
        return evento;
    }

    public List<Evento> getEventosByProfesor(Long profesorId, String asignaturaId, Long semestreId)
    {
        return eventosDAO.getEventosProfesorByAsignaturaAndSemestre(profesorId, asignaturaId, semestreId);
    }
}