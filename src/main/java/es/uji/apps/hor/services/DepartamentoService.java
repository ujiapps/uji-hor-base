package es.uji.apps.hor.services;

import es.uji.apps.hor.dao.DepartamentoDAO;
import es.uji.apps.hor.dao.PersonaDAO;
import es.uji.apps.hor.model.Cargo;
import es.uji.apps.hor.model.Departamento;
import es.uji.commons.rest.Role;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DepartamentoService
{
    @Autowired
    private DepartamentoDAO departamentoDAO;

    @Autowired
    private PersonaDAO personaDAO;

    @Role({ "ADMIN", "USUARIO" })
    public List<Departamento> getDepartamentos(Long connectedUserId)
    {
        if (personaDAO.esAdmin(connectedUserId))
        {
            return departamentoDAO.getDepartamentos();
        }

        return departamentoDAO.getDepartamentosByPersonaId(connectedUserId);
    }

    public List<Departamento> getDepartamentosMidPOD(Long connectedUserId)
            throws RegistroNoEncontradoException
    {

        if (personaDAO.esAdmin(connectedUserId))
        {
            return departamentoDAO.getDepartamentos();
        }

        List<Cargo> listaCargos = personaDAO.getCargosByPersonaId(connectedUserId);
        for (Cargo cargo: listaCargos) {
            if (cargo.gestionaPOD()) {
                return departamentoDAO.getDepartamentosByPersonaId(connectedUserId);

            }
        }

        return departamentoDAO.getMiDepartamentoByPersonaId(connectedUserId);
    }
}
