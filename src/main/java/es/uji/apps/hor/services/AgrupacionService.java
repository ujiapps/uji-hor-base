package es.uji.apps.hor.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.hor.AgrupacionNoPerteneceAEstudioException;
import es.uji.apps.hor.EstudioNoCompartidoException;
import es.uji.apps.hor.dao.AgrupacionDAO;
import es.uji.apps.hor.dao.AsignaturaDAO;
import es.uji.apps.hor.dao.EstudiosDAO;
import es.uji.apps.hor.dao.PersonaDAO;
import es.uji.apps.hor.model.*;
import es.uji.commons.rest.Role;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;

@Service
public class AgrupacionService
{
    @Autowired
    private AgrupacionDAO agrupacionDAO;

    @Autowired
    private EstudiosDAO estudioDAO;

    @Autowired
    private AsignaturaDAO asignaturaDAO;

    @Autowired
    private PersonaDAO personaDAO;

    @Role({"ADMIN", "USUARIO"})
    public List<Agrupacion> getAgrupacionesByEstudioId(Long estudioId, Long connectedUserId)
            throws RegistroNoEncontradoException, UnauthorizedUserException
    {
        if (!personaDAO.esAdmin(connectedUserId) && !estudioDAO.isEstudioAbiertoConsulta(estudioId))
        {
            Persona persona = personaDAO.getPersonaConTitulacionesYCentrosById(connectedUserId);
            persona.compruebaAccesoAEstudio(estudioId);
        }

        return agrupacionDAO.getAgrupacionesByEstudioId(estudioId);
    }

    public Agrupacion insertaAgrupacion(String nombre, List<Long> estudiosCompartidos,
                                        Long estudioId, Long connectedUserId) throws RegistroNoEncontradoException,
            UnauthorizedUserException, EstudioNoCompartidoException
    {
        if (!personaDAO.esAdmin(connectedUserId))
        {
            Persona persona = personaDAO.getPersonaConTitulacionesYCentrosById(connectedUserId);
            persona.compruebaAccesoAEstudio(estudioId);
        }

        Agrupacion agrupacion = new Agrupacion();
        agrupacion.setNombre(nombre);
        Estudio estudio = estudioDAO.getEstudioById(estudioId);

        List<Estudio> listaEstudios = new ArrayList<Estudio>();
        listaEstudios.add(estudio);

        for (Long estudioCompartidoId : estudiosCompartidos)
        {
            Estudio estudioCompartido = new Estudio(estudioCompartidoId);

            estudio.compruebaEstudioCompartido(estudioCompartido);
            listaEstudios.add(estudioCompartido);
        }

        agrupacion.setEstudios(listaEstudios);
        agrupacion = agrupacionDAO.insertAgrupacion(agrupacion);

        return agrupacion;
    }

    @Transactional
    public Agrupacion updateAgrupacion(Long agrupacionId, String nombre, Long estudioId,
                                       Long connectedUserId)
            throws RegistroNoEncontradoException, UnauthorizedUserException,
            AgrupacionNoPerteneceAEstudioException, EstudioNoCompartidoException
    {
        if (!personaDAO.esAdmin(connectedUserId))
        {
            Persona persona = personaDAO.getPersonaConTitulacionesYCentrosById(connectedUserId);
            persona.compruebaAccesoAEstudio(estudioId);
        }

        Agrupacion agrupacion = agrupacionDAO.getAgrupacionCompletaByIdAndEstudioId(agrupacionId,
                estudioId);
        agrupacion.compruebaEstudioEnAgrupacion(estudioId);

        agrupacion.setNombre(nombre);
        agrupacionDAO.updateAgrupacion(agrupacion);

        return agrupacion;
    }

    public void borraAgrupacion(Long agrupacionId, Long estudioId, Long connectedUserId)
            throws RegistroNoEncontradoException, UnauthorizedUserException,
            AgrupacionNoPerteneceAEstudioException
    {
        if (!personaDAO.esAdmin(connectedUserId))
        {
            Persona persona = personaDAO.getPersonaConTitulacionesYCentrosById(connectedUserId);
            persona.compruebaAccesoAEstudio(estudioId);
        }
        Agrupacion agrupacion = agrupacionDAO.getAgrupacionCompletaByIdAndEstudioId(agrupacionId,
                estudioId);
        agrupacion.compruebaEstudioEnAgrupacion(estudioId);

        agrupacionDAO.delete(agrupacionId);
    }

    public Agrupacion getAgrupacionConEstudios(Long agrupacionId)
    {
        return agrupacionDAO.getAgrupacionConEstudios(agrupacionId);
    }

    public Agrupacion getAgrupacionConEstudio(Long agrupacionId, Long estudioId)
    {
        return agrupacionDAO.getAgrupacionConEstudio(agrupacionId, estudioId);
    }

    public AsignaturaAgrupacion getAsignaturaAgrupacionById(Long asignaturaAgrupacionId)
    {
        return agrupacionDAO.getAsignaturaAgrupacionById(asignaturaAgrupacionId);
    }

    @Transactional
    public void deleteAsignaturaAgrupacionConCompartidas(Long agrupacionId, String asignaturaId,
                                                         Long connectedUserId)
            throws RegistroNoEncontradoException, UnauthorizedUserException,
            AgrupacionNoPerteneceAEstudioException
    {
        Estudio estudio = estudioDAO.getEstudioByAsignaturaId(asignaturaId);

        if (!personaDAO.esAdmin(connectedUserId))
        {
            Persona persona = personaDAO.getPersonaConTitulacionesYCentrosById(connectedUserId);
            persona.compruebaAccesoAEstudio(estudio.getId());
        }
        Agrupacion agrupacion = agrupacionDAO.getAgrupacionConEstudios(agrupacionId);
        agrupacion.compruebaEstudioEnAgrupacion(estudio.getId());

        List<Asignatura> asignaturas = getAsignaturasCompartidasByAgrupacion(agrupacion,
                asignaturaId);
        for (Asignatura asignatura : asignaturas)
        {
            agrupacionDAO.deleteAsignaturaDeAgrupacion(agrupacionId, asignatura.getId());
        }

        if (asignaturas.isEmpty())
        {
            agrupacionDAO.deleteAsignaturaDeAgrupacion(agrupacionId, asignaturaId);
        }
    }

    public void deleteAsignaturaAgrupacion(AsignaturaAgrupacion asignaturaAgrupacion,
                                           Long connectedUserId)
    {
        Agrupacion agrupacion = agrupacionDAO.getAgrupacionConEstudios(asignaturaAgrupacion
                .getAgrupacion().getId());
        List<Asignatura> asignaturas = getAsignaturasCompartidasByAgrupacion(agrupacion,
                asignaturaAgrupacion.getAsignatura().getId());

        for (Asignatura asignatura : asignaturas)
        {
            AsignaturaAgrupacion asignaturaAgrupacionCompartida = asignaturaAgrupacion.clonar();
            asignaturaAgrupacionCompartida.setAsignatura(asignatura);
            agrupacionDAO
                    .deleteAsignaturaDeAgrupacionConCompartidas(asignaturaAgrupacionCompartida);
        }

        if (asignaturas.isEmpty())
        {
            agrupacionDAO.deleteAsignaturaAgrupacion(asignaturaAgrupacion.getId());
        }
    }

    public void addAsignaturaAgrupacionConCompartidas(Long agrupacionId, String asignaturaId,
                                                      Long connectedUserId)
            throws RegistroNoEncontradoException, UnauthorizedUserException,
            AgrupacionNoPerteneceAEstudioException
    {
        Estudio estudio = estudioDAO.getEstudioByAsignaturaId(asignaturaId);

        if (!personaDAO.esAdmin(connectedUserId))
        {
            Persona persona = personaDAO.getPersonaConTitulacionesYCentrosById(connectedUserId);
            persona.compruebaAccesoAEstudio(estudio.getId());
        }
        Agrupacion agrupacion = agrupacionDAO.getAgrupacionConEstudios(agrupacionId);
        agrupacion.compruebaEstudioEnAgrupacion(estudio.getId());

        List<Asignatura> asignaturas = getAsignaturasCompartidasByAgrupacion(agrupacion,
                asignaturaId);
        for (Asignatura asignatura : asignaturas)
        {
            agrupacionDAO.addAsignaturaDeAgrupacion(agrupacionId, asignatura.getId());
        }

        if (asignaturas.isEmpty())
        {
            agrupacionDAO.addAsignaturaDeAgrupacion(agrupacionId, asignaturaId);
        }
    }

    private List<Asignatura> getAsignaturasCompartidasByAgrupacion(Agrupacion agrupacion,
                                                                   String asignaturaId)
    {
        List<Long> estudiosCompartidosIds = new ArrayList<>();
        for (Estudio estudioCompartido : agrupacion.getEstudios())
        {
            estudiosCompartidosIds.add(estudioCompartido.getId());
        }

        return asignaturaDAO.getAsignaturasCompartidaByEstudioYAsignaturaId(asignaturaId,
                estudiosCompartidosIds);
    }

    public void addAsignaturaAgrupacion(AsignaturaAgrupacion asignaturaAgrupacion)
    {
        Agrupacion agrupacion = agrupacionDAO.getAgrupacionConEstudios(asignaturaAgrupacion
                .getAgrupacion().getId());

        List<Asignatura> asignaturas = getAsignaturasCompartidasByAgrupacion(agrupacion,
                asignaturaAgrupacion.getAsignatura().getId());
        for (Asignatura asignatura : asignaturas)
        {
            AsignaturaAgrupacion asignaturaAgrupacionCompartida = asignaturaAgrupacion.clonar();
            asignaturaAgrupacionCompartida.setAsignatura(asignatura);
            agrupacionDAO.addAsignaturaAgrupacion(asignaturaAgrupacionCompartida);
        }

        if (asignaturas.isEmpty())
        {
            agrupacionDAO.addAsignaturaAgrupacion(asignaturaAgrupacion);
        }
    }

    public boolean existeAsignaturaAgrupacion(AsignaturaAgrupacion asignaturaAgrupacion)
    {
        return agrupacionDAO.existeAsignaturaAgrupacion(asignaturaAgrupacion);
    }
}
