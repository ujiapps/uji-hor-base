package es.uji.apps.hor.services;

import es.uji.apps.hor.dao.AsignaturaDAO;
import es.uji.apps.hor.dao.EventosDAO;
import es.uji.apps.hor.dao.PersonaDAO;
import es.uji.apps.hor.model.Asignatura;
import es.uji.apps.hor.model.AsignaturaAgrupacion;
import es.uji.apps.hor.model.Evento;
import es.uji.commons.rest.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Service
public class AsignaturaService
{
    @Autowired
    private AsignaturaDAO asignaturaDAO;

    @Autowired
    private PersonaDAO personaDAO;

    @Autowired
    private EventosDAO eventosDAO;

    @Role({ "ADMIN", "USUARIO" })
    public List<Asignatura> getAsignaturasByDepartamentoAndArea(Long areaId, Long connectedUserId)
    {
        List<Asignatura> asignaturas = asignaturaDAO.getAsignaturasByAreaId(areaId);

        ordenaListaAsignaturas(asignaturas);
        return asignaturas;
    }

    private void ordenaListaAsignaturas(List<Asignatura> asignaturas)
    {
        Collections.sort(asignaturas, (a1, a2) -> a1.getId().compareTo(a2.getId()));
    }

    public List<Asignatura> getAsignaturasByProfesor(Long profesorId, Long semestreId)
    {
        List<Asignatura> asignaturas = asignaturaDAO.getAsignaturasByProfesorId(profesorId, semestreId);

        ordenaListaAsignaturas(asignaturas);
        return asignaturas;
    }

    public List<Asignatura> getAsignaturasByTitulacionAndCursoAndSemestre(Long estudioId,
            Long cursoId, Long semestreId, List<String> gruposIds)
    {
        List<Asignatura> asignaturas = asignaturaDAO.getAsignaturasByTitulacionAndCursoAndSemestre(
                estudioId, cursoId, semestreId, gruposIds);

        ordenaListaAsignaturas(asignaturas);
        return asignaturas;
    }

    public List<Asignatura> getAsignaturasByTitulacionAndCurso(Long estudioId, Long cursoId)
    {
        List<Asignatura> asignaturas = asignaturaDAO.getAsignaturasByTitulacionAndCurso(estudioId,
                cursoId);
        return asignaturas;
    }

    public List<AsignaturaAgrupacion> getAsignaturasByAgrupacionId(Long agrupacionId,
            Long connectedUserId)
    {
        return asignaturaDAO.getAsignaturasByAgrupacionId(agrupacionId);
    }

    public List<Asignatura> getAsignaturasByAgrupacionAndSemestreAndGrupos(Long estudioId,
            Long agrupacionId, Long semestreId, List<String> gruposAsList)
    {
        List<Asignatura> asignaturas = asignaturaDAO.getAsignaturasByAgrupacionAndSemestreAndGrupos(
                estudioId, agrupacionId, semestreId, gruposAsList);

        ordenaListaAsignaturas(asignaturas);
        return asignaturas;
    }

    public String getCodigoComunByAsignaturaId(String asignaturaId)
    {
        return asignaturaDAO.getCodigoComunByAsignaturaId(asignaturaId);
    }

    public Map<String, BigDecimal> getCreditosTotalesPodAsignatura(List<String> asignaturasId)
    {
        boolean asignaturaComparteItems = asignaturaComparteItems(asignaturasId);
        return asignaturaDAO.getCreditosPodAsignatura(asignaturasId, asignaturaComparteItems);
    }

    public Map<String, BigDecimal> getCreditosTotalesPodAsignatura(String asignaturasId)
    {
        return asignaturaDAO.getCreditosPodAsignatura(asignaturasId);
    }

    protected boolean asignaturaComparteItems(List<String> asignaturasId)
    {
        List<Evento> eventos = eventosDAO.getEventosByAsignatura(asignaturasId.get(0));
        Long numeroItemsCompartidos = eventos.stream().filter(c -> c.getAsignaturas().size() > 1)
                .count();
        return numeroItemsCompartidos > 0;
    }

    public Map<String, BigDecimal>  getCreditosPodAsignados(List<String> asignaturasId)
    {
        return asignaturaDAO.getCreditosPodAsignados(asignaturasId);
    }

    public List<String> getCodigosAsignaturasCompartidas(String asignaturaId)
    {
        List<String> codigosAsignaturasComunes = asignaturaDAO
                .getCodigosAsignaturasCompartidas(asignaturaId);
        if (codigosAsignaturasComunes.isEmpty())
        {
            codigosAsignaturasComunes.add(asignaturaId);
        }
        return codigosAsignaturasComunes;
    }
}
