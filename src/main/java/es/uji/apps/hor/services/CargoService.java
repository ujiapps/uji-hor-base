package es.uji.apps.hor.services;

import es.uji.apps.hor.dao.CargoDAO;
import es.uji.apps.hor.dao.PersonaDAO;
import es.uji.apps.hor.model.Cargo;
import es.uji.commons.rest.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CargoService
{
    @Autowired
    private PersonaDAO personaDAO;

    @Autowired
    private CargoDAO cargoDAO;

    @Role({ "ADMIN", "USUARIO" })
    public List<Cargo> getCargos(Long connectedUserId)
    {
        return cargoDAO.getCargos(connectedUserId);
    }
}
