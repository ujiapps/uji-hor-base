package es.uji.apps.hor.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.hor.dao.EstudiosDAO;
import es.uji.apps.hor.dao.PersonaDAO;
import es.uji.apps.hor.model.Estudio;
import es.uji.apps.hor.model.Persona;
import es.uji.commons.rest.Role;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;

@Service
public class EstudiosService
{
    @Autowired
    private PersonaDAO personaDAO;

    private final EstudiosDAO estudiosDAO;

    @Autowired
    public EstudiosService(EstudiosDAO estudiosDAO)
    {
        this.estudiosDAO = estudiosDAO;
    }

    @Role({ "ADMIN", "USUARIO" })
    public List<Estudio> getEstudios(Long connectedUserId) throws RegistroNoEncontradoException
    {
        Persona persona = personaDAO.getPersonaConTitulacionesYCentrosById(connectedUserId);
        if (personaDAO.esAdmin(connectedUserId))
        {
            return estudiosDAO.getEstudios();
        }
        else if (persona.esGestorDeCentro())
        {
            return estudiosDAO.getEstudiosByCentroId(persona.getCentroAutorizado().getId());
        }
        else
        {
            return estudiosDAO.getEstudiosVisiblesPorUsuario(connectedUserId);
        }
    }

    public List<Estudio> getEstudiosByTipo(Long connectedUserId, String tipoEstudio) throws RegistroNoEncontradoException {
        List<Estudio> estudios = getEstudios(connectedUserId);
        return estudios.stream().filter(e -> e.getTipoEstudio().getId().equals(tipoEstudio)).collect(Collectors.toList());
    }

    @Role({ "ADMIN", "USUARIO" })
    public List<Estudio> getEstudiosAbiertosConsulta(Long connectedUserId)
    {
        return estudiosDAO.getEstudiosAbiertosConsulta();
    }

    @Role({ "ADMIN", "USUARIO" })
    public List<Estudio> getEstudiosByCentroId(Long centroId, Long connectedUserId)
            throws UnauthorizedUserException, RegistroNoEncontradoException
    {
        Persona persona = personaDAO.getPersonaConTitulacionesYCentrosById(connectedUserId);

        if (!personaDAO.esAdmin(connectedUserId))
        {
            persona.compruebaAccesoACentro(centroId);
        }
        return estudiosDAO.getEstudiosByCentroId(centroId);
    }

    @Role({ "ADMIN", "USUARIO" })
    public List<Estudio> getTodosLosEstudios(Long connectedUserId)
            throws RegistroNoEncontradoException, UnauthorizedUserException
    {
        Persona persona = personaDAO.getPersonaConTitulacionesYCentrosById(connectedUserId);

        if (personaDAO.esAdmin(connectedUserId) || persona.esGestorDeCentro())
        {
            return estudiosDAO.getEstudios();
        }
        else
        {
            throw new UnauthorizedUserException();
        }
    }

    @Role({ "ADMIN", "USUARIO" })
    public List<Estudio> getEstudiosCompartidosDeEstudio(Long estudioId, Long connectedUserId)
            throws RegistroNoEncontradoException, UnauthorizedUserException
    {
        Estudio estudio = estudiosDAO.getEstudioById(estudioId);

        if (!this.getEstudios(connectedUserId).contains(estudio))
        {
            throw new UnauthorizedUserException();
        }

        return estudio.getEstudiosCompartidos();
    }

    public List<Estudio> getEstudiosByAgrupacionId(Long agrupacionId, Long connectedUserId)
            throws RegistroNoEncontradoException
    {
        return estudiosDAO.getEstudiosByAgrupacionId(agrupacionId);
    }

    public List<Estudio> getEstudiosByEvento(Long itemId, Long connectedUserId)
    {
        return estudiosDAO.getEstudiosByEvento(itemId);
    }

}
