package es.uji.apps.hor.services;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.hor.dao.LogDAO;
import es.uji.apps.hor.dao.PersonaDAO;
import es.uji.apps.hor.model.Evento;
import es.uji.apps.hor.model.Log;
import es.uji.commons.rest.Role;

@Service
public class LogService
{
    @Autowired
    private PersonaDAO personaDAO;

    private LogDAO logDAO;

    @Autowired
    public LogService(LogDAO logDAO)
    {
        this.logDAO = logDAO;
    }

    private void register(int tipoAccion, String descripcion, Evento evento, String usuario, Long connectedUserId)
    {
        logDAO.register(tipoAccion, descripcion, evento, usuario);
    }

    @Role({"ADMIN", "USUARIO"})
    public List<Log> getLogsPorFecha(Date fecha, String usuario, Long connectedUserId)
    {
        if (personaDAO.esAdmin(connectedUserId))
        {
            return logDAO.getLogsByFecha(fecha);
        }
        else
        {
            return logDAO.getLogsByFechaYUsuario(fecha, usuario);
        }
    }

    @Role({"ADMIN", "USUARIO"})
    public List<Log> getLogsPorFechaYItemId(Date fecha, Long itemId, String usuario,
                                            Long connectedUserId)
    {
        if (personaDAO.esAdmin(connectedUserId))
        {
            return logDAO.getLogsByFechaYItemId(fecha, itemId);
        }
        else
        {
            return logDAO.getLogsByFechaYItemIdYUsuario(fecha, itemId, usuario);
        }
    }

    @Role({"ADMIN", "USUARIO"})
    public List<Log> getLogsPorItemId(Long itemId, Long connectedUserId)
    {
        return logDAO.getLogsByItemId(itemId);
    }

    public void registraModificacionDeEvento(int tipoAccion, Evento eventoOriginal, Evento eventoModificado, String usuario, Long connectedUserId)
    {
        String descripcionEventoOriginal = getDescripcionEvento(eventoOriginal);
        String descripcionEventoModificado = getDescripcionEvento(eventoModificado);
        String mensaje = MessageFormat.format(
                "Canviada la classe del grup(s) \"{0}\" {1} DADES ANTERIORS {2}",
                eventoOriginal.getListaClases(), descripcionEventoModificado, descripcionEventoOriginal);

        register(tipoAccion, mensaje, eventoOriginal, usuario, connectedUserId);
    }

    protected String getDescripcionEvento(Evento evento)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(evento.getInicio());
        String horaInicio = getHoraFormateada(cal);
        cal.setTime(evento.getFin());
        String horaFin = getHoraFormateada(cal);
        String detalleManual = evento.hasDetalleManual() ? "SI" : "NO";
        String numeroSesiones = getNumeroSesiones(evento);
        String fechaDesde = (evento.getDesdeElDia() != null && !evento.hasDetalleManual()) ? MessageFormat
                .format(", des del dia {0}", getFechaFormateada(evento.getDesdeElDia())) : "";
        String fechaHasta = (evento.getHastaElDia() != null && !evento.hasDetalleManual()) ? MessageFormat
                .format(", fins al dia {0}", getFechaFormateada(evento.getHastaElDia())) : "";
        String repetirCada = (evento.getRepetirCadaSemanas() != null && !evento.hasDetalleManual()) ? MessageFormat
                .format(", repetir cada {0} setmanes", evento.getRepetirCadaSemanas()) : "";

        return MessageFormat.format(
                "{0}, de {1} - {2}, detall manual: {3}{4}{5}{6}{7}",
                Log.getNombreDiaSemana(cal.get(Calendar.DAY_OF_WEEK)), horaInicio, horaFin,
                detalleManual, fechaDesde, fechaHasta, numeroSesiones, repetirCada);
    }

    private String getNumeroSesiones(Evento evento)
    {
        if ((evento.getNumeroIteraciones() != null && !evento.hasDetalleManual()))
        {
            return MessageFormat.format(", {0} repeticions", evento.getNumeroIteraciones());
        }
        return MessageFormat.format(", {0} sessions", evento.getEventosDetalle().size());
    }

    private String getHoraFormateada(Calendar cal)
    {
        SimpleDateFormat dateFormatter = new SimpleDateFormat("HH:mm");
        return dateFormatter.format(cal.getTime());
    }

    private String getFechaFormateada(Date fecha)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(fecha);
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM//yyyy");
        return dateFormatter.format(cal.getTime());
    }

    @Role({"ADMIN", "USUARIO"})
    public void registraEventoBorrado(int tipoAccion, Evento evento, String usuario, Long connectedUserId)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(evento.getInicio());
        String dia = Log.getNombreDiaSemana(cal.get(Calendar.DAY_OF_WEEK));
        String horaInicio = getHoraFormateada(cal);
        cal.setTime(evento.getFin());
        String horaFin = getHoraFormateada(cal);
        String mensaje = MessageFormat.format(
                "Esborrada la clase del grup (o grups) \"{0}\" del dia {1} de {2} a {3}",
                evento.getListaClases(), dia, horaInicio, horaFin);

        register(tipoAccion, mensaje, evento, usuario, connectedUserId);
    }

    @Role({"ADMIN", "USUARIO"})
    public void registraEventoDividido(Evento evento, String usuario, Long connectedUserId)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(evento.getInicio());
        String dia = Log.getNombreDiaSemana(cal.get(Calendar.DAY_OF_WEEK));
        String horaInicio = getHoraFormateada(cal);
        String mensaje = MessageFormat.format(
                "Divisió de la classe del grup (o grups) \"{0}\" el dia {1} a les {2}",
                evento.getListaClases(), dia, horaInicio);

        register(Log.ACCION_DIVIDIR_ITEM, mensaje, evento, usuario, connectedUserId);

    }

    public void registraAsinacionDeAulaAEvento(Evento eventoOriginal, Evento eventoModificado, String usuario, Long connectedUserId)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(eventoOriginal.getInicio());
        String dia = Log.getNombreDiaSemana(cal.get(Calendar.DAY_OF_WEEK));
        String horaInicio = getHoraFormateada(cal);
        if (eventoModificado.getAula() != null)
        {
            String mensaje = MessageFormat.format("{0} assignada a les classes del grup (o grups) {1} el dia {2} a les {3} DADES ANTERIORS {4}",
                    eventoModificado.getAula().getNombre(), eventoModificado.getListaClases(), dia, horaInicio, eventoOriginal.getAula() != null ? eventoOriginal.getAula().getNombre() : "cap aula assingada");
            register(Log.ACCION_ASIGNAR_AULA_ITEM, mensaje, eventoOriginal, usuario, connectedUserId);
        }
        else
        {
            String mensaje = MessageFormat.format("Aula desasignada a les classes del grup (o grups) {0} el dia {1} a les {2} DADES ANTERIORS {3}",
                    eventoModificado.getListaClases(), dia, horaInicio, eventoOriginal.getAula() != null ? eventoOriginal.getAula().getNombre() : "cap aula assingada");
            register(Log.ACCION_ASIGNAR_AULA_ITEM, mensaje, eventoOriginal, usuario, connectedUserId);
        }
    }

    public void registraModificacionEventoDetalle(Evento eventoOriginal, Evento eventoModificado, String usuario,
                                                  Long connectedUserId)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(eventoOriginal.getInicio());
        Date fechaOriginal = eventoOriginal.getInicio();
        cal.setTime(eventoOriginal.getFin());
        String horaFinOriginal = getHoraFormateada(cal);

        cal.setTime(eventoModificado.getInicio());
        Date fechaModificada = eventoModificado.getInicio();
        cal.setTime(eventoModificado.getFin());
        String horaFinModificada = getHoraFormateada(cal);

        String mensaje = MessageFormat.format("Canviada la classe del grup (o grups) \"{0}\" al {1} - {2} data ANTERIOR {3} - {4}",
                eventoModificado.getListaClases(), fechaModificada, horaFinModificada, fechaOriginal, horaFinOriginal);

        register(Log.ACCION_MOVER_ITEM_SEMANA_DETALLE, mensaje, eventoOriginal, usuario, connectedUserId);
    }

    public void registraNuevaPlanificacionDeEvento(Evento evento, String usuario,
                                                   Long connectedUserId)
    {
        String mensaje = MessageFormat.format("Nova planificació de la classe del grup (o grups) \"{0}\"", evento.getListaClases());
        register(Log.ACCION_PLANIFICAR_ITEM, mensaje, evento, usuario, connectedUserId);

    }

}
