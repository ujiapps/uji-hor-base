package es.uji.apps.hor.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.hor.dao.CursosDAO;
import es.uji.apps.hor.dao.EstudiosDAO;
import es.uji.apps.hor.dao.PersonaDAO;
import es.uji.apps.hor.model.Curso;
import es.uji.apps.hor.model.Persona;
import es.uji.commons.rest.Role;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;

@Service
public class CursosService
{
    @Autowired
    private PersonaDAO personaDAO;

    @Autowired
    private EstudiosDAO estudioDAO;

    private final CursosDAO cursosDAO;

    @Autowired
    public CursosService(CursosDAO cursosDAO)
    {
        this.cursosDAO = cursosDAO;
    }

    @Role({"ADMIN", "USUARIO"})
    public List<Curso> getCursos(Long estudioId, Long connectedUserId)
            throws UnauthorizedUserException, RegistroNoEncontradoException
    {
        if (!personaDAO.esAdmin(connectedUserId) && !estudioDAO.isEstudioAbiertoConsulta(estudioId))
        {
            Persona persona = personaDAO.getPersonaConTitulacionesYCentrosById(connectedUserId);
            persona.compruebaAccesoAEstudio(estudioId);
        }

        return cursosDAO.getCursos(estudioId);
    }

    @Role({"ADMIN", "USUARIO"})
    public List<Curso> getCursosEstudio(Long estudioId, Long connectedUserId)

    {
        return cursosDAO.getCursos(estudioId);
    }


}
