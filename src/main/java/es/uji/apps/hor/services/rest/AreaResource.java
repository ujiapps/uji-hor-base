package es.uji.apps.hor.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.hor.model.Area;
import es.uji.apps.hor.services.AreaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.AccessManager;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("area")
public class AreaResource extends CoreBaseService
{
    @InjectParam
    private AreaService areaService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getAreas(@QueryParam("departamentoId") String departamentoId)
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        ParamUtils.checkNotNull(departamentoId);

        List<Area> areas = areaService.getAreaByDepartamentoId(
                ParamUtils.parseLong(departamentoId), connectedUserId);

        return UIEntity.toUI(areas);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("mipod")
    public List<UIEntity> getAreasMiPod(@QueryParam("departamentoId") String departamentoId)
            throws RegistroNoEncontradoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        ParamUtils.checkNotNull(departamentoId);

        List<Area> areas = areaService.getAreaMiPODByDepartamentoId(
                ParamUtils.parseLong(departamentoId), connectedUserId);

        return UIEntity.toUI(areas);
    }

}
