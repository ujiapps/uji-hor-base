package es.uji.apps.hor.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.hor.DiaNoLectivoException;
import es.uji.apps.hor.DiaNoValidoExamenException;
import es.uji.apps.hor.model.*;
import es.uji.apps.hor.services.CalendarioAcademicoService;
import es.uji.apps.hor.services.ExamenService;
import es.uji.apps.hor.services.ParamParseService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;
import org.apache.commons.lang3.StringUtils;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Path("examen")
public class ExamenResource extends CoreBaseService
{
    @InjectParam
    private ExamenService examenService;

    @InjectParam
    private CalendarioAcademicoService calendarioAcademicoService;

    private SimpleDateFormat formateadorFecha = new SimpleDateFormat("yyyy-MM-dd");
    private SimpleDateFormat uIEntitydateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    private SimpleDateFormat humanDateFormat = new SimpleDateFormat("dd/MM/yyyy");

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getExamenById(@PathParam("id") Long examenId)
            throws RegistroNoEncontradoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Examen examen = examenService.getExamenById(examenId, connectedUserId);
        return modelToUI(examen);
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateExamen(UIEntity entity) throws ParseException,
            RegistroNoEncontradoException, DiaNoValidoExamenException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Examen examen = uiToModel(entity);
        Long estudioId = entity.getLong("estudioId");
        examenService.updateExamen(examen, estudioId, connectedUserId);
        return UIEntity.toUI(examen);
    }

    private Examen uiToModel(UIEntity entity) throws ParseException
    {
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Examen examen = entity.toModel(Examen.class);
        examen.setFecha(dateFormatter.parse(entity.get("fecha")));
        examen.setHoraInicio(dateFormatter.parse(entity.get("horaInicio")));
        examen.setHoraFin(dateFormatter.parse(entity.get("horaFin")));
        Convocatoria convocatoria = new Convocatoria();
        convocatoria.setId(Long.parseLong(entity.get("convocatoriaId")));
        convocatoria.setNombre(entity.get("convocatoriaNombre"));
        examen.setConvocatoria(convocatoria);
        return examen;
    }

    @GET
    @Path("noplanificados")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getExamenesNoPlanificados(@QueryParam("estudioId") Long estudioId,
            @QueryParam("convocatoriaId") Long convocatoriaId,
            @QueryParam("cursosId") String cursosId)
    {
        ParamUtils.checkNotNull(estudioId, convocatoriaId);
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<Long> cursosList = ParamParseService.parseAsLongList(cursosId);

        List<Examen> examenes = examenService.getExamenesNoPlanificados(estudioId, convocatoriaId,
                cursosList, connectedUserId);
        return UIEntity.toUI(examenes);
    }

    @GET
    @Path("planificados")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getExamenesPlanificados(@QueryParam("estudioId") Long estudioId,
            @QueryParam("convocatoriaId") Long convocatoriaId,
            @QueryParam("cursosId") String cursosId) throws ParseException,
            RegistroNoEncontradoException
    {
        ParamUtils.checkNotNull(estudioId, convocatoriaId);
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<Long> cursosList = ParamParseService.parseAsLongList(cursosId);

        List<Examen> examenes = examenService.getExamenesPlanificados(estudioId, convocatoriaId,
                cursosList, connectedUserId);

        Convocatoria convocatoria = examenService.getConvocatoriaById(convocatoriaId);

        List<UIEntity> uiExamenes = new ArrayList<UIEntity>();

        for (Examen examen : examenes)
        {
            String inicio = uIEntitydateFormat.format(examen.getHoraInicio());
            String fin = uIEntitydateFormat.format(examen.getHoraFin());

            UIEntity entity = new UIEntity();
            entity.put("id", examen.getId());
            entity.put("fecha", examen.getFecha());
            entity.put("convocatoriaId", examen.getConvocatoria().getId());
            entity.put("asignaturasComunes", examen.getAsignaturasComunes());
            entity.put("cid", TipoCalendario.EC.getCalendarioId());
            entity.put("title", examen.getNombreCompleto());
            entity.put("start", inicio);
            entity.put("end", fin);
            entity.put("ad", false);
            entity.put("comentario", examen.getComentario());
            entity.put("definitivo", examen.isDefinitivo());
            if (examen.getAsignaturasExamen() != null)
            {
                List<String> asignaturasInfo = new ArrayList();
                for (Asignatura asignatura : examen.getAsignaturasExamen())
                {
                    asignaturasInfo.add(String.format("%s - %s (%d)", asignatura.getId(),
                            asignatura.getNombre(), asignatura.getCursoId()));
                }
                entity.put("asignaturas", StringUtils.join(asignaturasInfo, ", "));
            }

            if (examen.getAulasExamen() != null)
            {
                List<String> aulasInfo = new ArrayList();
                for (Aula aula : examen.getAulasExamen())
                {
                    aulasInfo.add(aula.getCodigo());
                }
                entity.put("aulas", StringUtils.join(aulasInfo, ", "));
            }

            uiExamenes.add(entity);
        }

        uiExamenes.addAll(getDiasFestivos(TipoEstudio.getTipoEstudioId(estudioId), convocatoria.getInicio(), convocatoria.getFin(),
                connectedUserId));

        return uiExamenes;
    }

    private UIEntity modelToUI(Examen examen)
    {
        UIEntity entity = UIEntity.toUI(examen);
        entity.put("convocatoriaNombre", examen.getConvocatoria().getNombre());
        entity.put("codigoAulas", this.getCodigosAulas(examen.getAulasExamen()));
        return entity;
    }

    private String getCodigosAulas(List<Aula> aulas)
    {
        List<String> codigosAulas = new ArrayList();
        for (Aula aula : aulas)
        {
            codigosAulas.add(aula.getId().toString());
        }
        return StringUtils.join(codigosAulas, ", ");
    }

    @POST
    @Path("planificados")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public UIEntity asignaNuevoExamenPlanificado(@FormParam("examenId") Long examenId,
            @FormParam("estudioId") Long estudioId, @FormParam("fechaInicial") String fechaInicial)
            throws RegistroNoEncontradoException, ParseException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        ParamUtils.checkNotNull(examenId);

        Examen examen = examenService.planificaExamen(examenId, estudioId, null, connectedUserId);
        return UIEntity.toUI(examen);
    }

    @PUT
    @Path("planificados/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateEventoSemanaDetalle(UIEntity entity,
            @QueryParam("estudioId") Long estudioId) throws ParseException,
            RegistroNoEncontradoException, DiaNoValidoExamenException, DiaNoLectivoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date inicio = dateFormat.parse(entity.get("start"));
        Date fin = dateFormat.parse(entity.get("end"));
        Long examenId = (Long.parseLong(entity.get("id")));

        examenService.modificaFechaExamen(examenId, estudioId, inicio, fin, connectedUserId);

        return Response.ok().build();

    }

    @PUT
    @Path("desplanifica/{examenId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response desplanificaEvento(@PathParam("examenId") Long examenId) throws ParseException,
            RegistroNoEncontradoException, DiaNoValidoExamenException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        examenService.desplanificaExamen(examenId, connectedUserId);

        return Response.ok().build();

    }

    @PUT
    @Path("dividir/{examenId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response divideEvento(@PathParam("examenId") Long examenId) throws ParseException,
            RegistroNoEncontradoException, DiaNoValidoExamenException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        examenService.dividirExamen(examenId, connectedUserId);

        return Response.ok().build();

    }

    @GET
    @Path("convocatoria")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getConvocatorias()
    {
        List<Convocatoria> convocatorias = examenService.getConvocatorias();

        List<UIEntity> uiConvocatorias = new ArrayList<UIEntity>();

        for (Convocatoria convocatoria : convocatorias)
        {
            UIEntity entity = new UIEntity();
            String inicio = uIEntitydateFormat.format(convocatoria.getInicio());
            String fin = uIEntitydateFormat.format(convocatoria.getFin());

            entity.put("id", convocatoria.getId());
            entity.put(
                    "nombre",
                    convocatoria.getNombre() + " del "
                            + humanDateFormat.format(convocatoria.getInicio()) + " al "
                            + humanDateFormat.format(convocatoria.getFin()));
            entity.put("inicio", inicio);
            entity.put("fin", fin);
            uiConvocatorias.add(entity);
        }

        return uiConvocatorias;
    }

    @GET
    @Path("estudio/{estudioId}/convocatoria")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getConvocatoriasByEstudio(@PathParam("estudioId") Long estudioId)
    {
        List<Convocatoria> convocatorias = examenService.getConvocatoriasByEstudio(estudioId);

        List<UIEntity> uiConvocatorias = new ArrayList<UIEntity>();

        for (Convocatoria convocatoria : convocatorias)
        {
            UIEntity entity = new UIEntity();
            String inicio = uIEntitydateFormat.format(convocatoria.getInicio());
            String fin = uIEntitydateFormat.format(convocatoria.getFin());

            entity.put("id", convocatoria.getId());
            entity.put(
                    "nombre",
                    convocatoria.getNombre() + " del "
                            + humanDateFormat.format(convocatoria.getInicio()) + " al "
                            + humanDateFormat.format(convocatoria.getFin()));
            entity.put("inicio", inicio);
            entity.put("fin", fin);
            uiConvocatorias.add(entity);
        }

        return uiConvocatorias;
    }

    @POST
    @Path("sincronizaaulas")
    public Response sincronizaItemsDetalle(UIEntity entity) throws Exception
    {
        List<Long> aulaIds = deserializaAulaIds(entity.get("aulas"));
        Long examenId = entity.getLong("examenId");
        examenService.sincronizaAulas(examenId, aulaIds);
        return Response.ok().build();
    }

    private List<Long> deserializaAulaIds(String aulas)
    {
        List<Long> aulaIds = new ArrayList<>();
        for (String aulaId : aulas.split(","))
        {
            if (aulaId != null && !aulaId.isEmpty())
            {
                aulaIds.add(Long.parseLong(aulaId));
            }
        }
        return aulaIds;
    }

    private List<UIEntity> getDiasFestivos(String tipoEstudioId, Date startDate, Date endDate, Long connectedUserId)
            throws ParseException
    {
        List<CalendarioAcademico> festivos = calendarioAcademicoService
                .getCalendarioAcademicoFestivos(tipoEstudioId, startDate, endDate, connectedUserId);
        List<UIEntity> eventosFestivos = new ArrayList<UIEntity>();

        for (CalendarioAcademico festivo : festivos)
        {
            String fecha = uIEntitydateFormat.format(festivo.getFecha());

            UIEntity entity = new UIEntity();
            entity.put("cid", TipoCalendario.F.getCalendarioId());
            entity.put("title", "Festiu / No lectiu");
            entity.put("ad", true);
            entity.put("start", fecha.replaceAll("T00:00:00","T08:00:00"));
            entity.put("end", fecha.replaceAll("T00:00:00","T22:00:00"));
            entity.put("AllDay", true);

            eventosFestivos.add(entity);
        }

        return eventosFestivos;
    }

    @GET
    @Path("tipo")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTiposExamen()
    {
        List<TipoExamen> examenes = examenService.getTiposExamen();
        return UIEntity.toUI(examenes);
    }

    @GET
    @Path("titulacion/curso")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getGruposAsignatura(@QueryParam("estudioId") Long estudioId)
            throws UnauthorizedUserException, RegistroNoEncontradoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        ParamUtils.checkNotNull(estudioId);

        List<Curso> cursos = examenService.getCursosEstudio(estudioId, connectedUserId);

        return UIEntity.toUI(cursos);
    }
}
