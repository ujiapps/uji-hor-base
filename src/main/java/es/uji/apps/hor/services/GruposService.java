package es.uji.apps.hor.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.hor.dao.EstudiosDAO;
import es.uji.apps.hor.dao.GruposDAO;
import es.uji.apps.hor.dao.PersonaDAO;
import es.uji.apps.hor.model.Grupo;
import es.uji.apps.hor.model.Persona;
import es.uji.commons.rest.Role;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;

@Service
public class GruposService
{
    @Autowired
    private PersonaDAO personaDAO;

    @Autowired
    private EstudiosDAO estudioDAO;

    private final GruposDAO gruposDAO;

    @Autowired
    public GruposService(GruposDAO gruposDAO)
    {
        this.gruposDAO = gruposDAO;
    }

    @Role({ "ADMIN", "USUARIO" })
    public List<Grupo> getGrupos(Long semestreId, Long cursoId, Long estudioId, Long connectedUserId)
            throws UnauthorizedUserException, RegistroNoEncontradoException
    {
        if (!personaDAO.esAdmin(connectedUserId) && !estudioDAO.isEstudioAbiertoConsulta(estudioId))
        {
            Persona persona = personaDAO.getPersonaConTitulacionesYCentrosById(connectedUserId);
            persona.compruebaAccesoAEstudio(estudioId);
        }

        return gruposDAO.getGrupos(semestreId, cursoId, estudioId);
    }

    @Role({ "ADMIN", "USUARIO" })
    public List<Grupo> getGruposCircuitos(Long cursoId, Long estudioId, Long connectedUserId)
            throws UnauthorizedUserException, RegistroNoEncontradoException
    {
        if (!personaDAO.esAdmin(connectedUserId) && !estudioDAO.isEstudioAbiertoConsulta(estudioId))
        {
            Persona persona = personaDAO.getPersonaConTitulacionesYCentrosById(connectedUserId);
            persona.compruebaAccesoAEstudio(estudioId);
        }

        return gruposDAO.getGruposCircuitos(estudioId, cursoId);
    }

    @Role({ "ADMIN", "USUARIO" })
    public List<Grupo> getGruposAsignatura(String asignaturaId, Long semestreId,
            Long connectedUserId) throws UnauthorizedUserException, RegistroNoEncontradoException
    {
        // TODO : Comprobar permisos
        /*
         * if (!personaDAO.esAdmin(connectedUserId)) { Persona persona =
         * personaDAO.getPersonaConTitulacionesYCentrosById(connectedUserId);
         * persona.compruebaAccesoAEstudio(estudioId); }
         */

        return gruposDAO.getGruposAsignatura(asignaturaId, semestreId);
    }

    public List<Grupo> getGruposAgrupacion(Long agrupacionId, Long semestreId,
            Long connectedUserId)
    {
        return gruposDAO.getGruposAgrupacion(agrupacionId, semestreId);
    }
}