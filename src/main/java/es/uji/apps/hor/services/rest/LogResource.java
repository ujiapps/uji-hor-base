package es.uji.apps.hor.services.rest;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.hor.model.Log;
import es.uji.apps.hor.services.LogService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

@Path("log")
public class LogResource extends CoreBaseService
{
    final private static String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";

    @InjectParam
    private LogService logService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getLogsPorFecha(@QueryParam("fecha") String cadenaFecha,
            @QueryParam("itemId") String itemId) throws ParseException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        ParamUtils.checkNotNull(cadenaFecha);

        DateFormat df = new SimpleDateFormat(DATE_FORMAT);
        Date fecha = df.parse(cadenaFecha);
        String usuario = AccessManager.getConnectedUser(request).getName();

        List<Log> listaLogs;
        if (itemId != null && !itemId.equals(""))
        {
            listaLogs = logService.getLogsPorFechaYItemId(fecha, ParamUtils.parseLong(itemId),
                    usuario, connectedUserId);
        }
        else
        {
            listaLogs = logService.getLogsPorFecha(fecha, usuario, connectedUserId);
        }

        return UIEntity.toUI(listaLogs);
    }

    @GET
    @Path("evento/{itemId}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getLogsPorItemId(@PathParam("itemId") Long itemId)
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<Log> listaLogs = logService.getLogsPorItemId(itemId, connectedUserId);
        return UIEntity.toUI(listaLogs);
    }

    @GET
    @Path("evento")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getListaItemsIdsPorFecha(@QueryParam("fecha") String cadenaFecha)
            throws ParseException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        ParamUtils.checkNotNull(cadenaFecha);

        DateFormat df = new SimpleDateFormat(DATE_FORMAT);
        Date fecha = df.parse(cadenaFecha);
        String usuario = AccessManager.getConnectedUser(request).getName();

        List<Log> listaLogs = logService.getLogsPorFecha(fecha, usuario, connectedUserId);

        List<Long> listaItemIds = new ArrayList<Long>();

        List<UIEntity> entityList = new ArrayList<UIEntity>();

        for (Log log : listaLogs)
        {
            if (!listaItemIds.contains(log.getEvento().getId()))
            {
                listaItemIds.add(log.getEvento().getId());
                UIEntity entity = new UIEntity();
                entity.put("id", log.getEvento().getId());
                entity.put("name", log.getEvento().getId());
                entityList.add(entity);
            }
        }

        return entityList;
    }
}
