package es.uji.apps.hor.services.rest;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.hor.*;
import es.uji.apps.hor.model.*;
import es.uji.apps.hor.services.*;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;

import org.apache.commons.lang3.StringUtils;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Path("calendario")
public class CalendarResource extends CoreBaseService
{
    private static final String END_DATE_QUERY_PARAM = "endDate";
    private static final String START_DATE_QUERY_PARAM = "startDate";
    private static final String CALENDARIOS_IDS_QUERY_PARAM = "calendariosIds";
    private static final String GRUPO_ID_QUERY_PARAM = "grupoId";
    private static final String ASIGNATURA_ID_QUERY_PARAM = "asignaturaId";
    private static final String AREA_ID_QUERY_PARAM = "areaId";
    private static final String CIRCUITO_ID_QUERY_PARAM = "circuitoId";
    private static final String GRUPOS_ID_QUERY_PARAM = "gruposId";
    private static final String TIPO_SUBGRUPOS_ID_QUERY_PARAM = "tipoSubgruposId";
    private static final String PROFESOR_ID_QUERY_PARAM = "profesorId";
    private static final String CURSO_ID_QUERY_PARAM = "cursoId";
    private static final String SEMESTRE_ID_QUERY_PARAM = "semestreId";
    private static final String AGRUPACION_ID_QUERY_PARAM = "agrupacionId";
    private static final String ESTUDIO_ID_QUERY_PARAM = "estudioId";
    private static final String OCUPACION_PROFESOR_QUERY_PARAM = "ocupacion";
    private static final String AREA_COMPLETA_QUERY_PARAM = "areaCompleta";
    private static final String TIPO_ACCION_PARAM = "tipoAccion";
    private static final String AULA_ID_PARAM = "aulaId";
    private static final String ID_PATH_PARAM = "id";
    private static final int ULTIMA_HORA_DIA = 23;
    private static final int ULTIMO_MINUTO_HORA = 59;
    private static final int ULTIMO_SEGUNDO_MINUTO = 59;
    private static final int COLOR_BASE = 20;

    @InjectParam
    private EventosService eventosService;

    @InjectParam
    private CalendariosService calendariosService;

    @InjectParam
    private AsignaturaService asignaturaService;

    @InjectParam
    private RangoHorarioService rangoHorarioService;

    @InjectParam
    private CalendarioAcademicoService calendarioAcademicoService;

    @InjectParam
    private IdiomaService idiomaService;

    @InjectParam
    private ProfesorService profesorService;

    @InjectParam
    private LogService logService;

    private SimpleDateFormat queryParamDateFormat;
    private SimpleDateFormat uIEntitydateFormat;

    public CalendarResource()
    {
        queryParamDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        uIEntitydateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    }

    @GET
    @Path("config")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getConfiguracion(@QueryParam(ESTUDIO_ID_QUERY_PARAM) Long estudioId,
                                           @QueryParam(CURSO_ID_QUERY_PARAM) Long cursoId,
                                           @QueryParam(SEMESTRE_ID_QUERY_PARAM) Long semestreId,
                                           @QueryParam(GRUPO_ID_QUERY_PARAM) String grupoId)
            throws RegistroNoEncontradoException, UnauthorizedUserException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        ParamUtils.checkNotNull(estudioId, cursoId, semestreId, grupoId);

        RangoHorario rangoHorario = rangoHorarioService.getHorario(estudioId, cursoId, semestreId,
                grupoId, connectedUserId);

        return rangoHorarioToUI(rangoHorario);
    }

    @GET
    @Path("config/adjust")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getConfiguracionAjustada(
            @QueryParam(ESTUDIO_ID_QUERY_PARAM) Long estudioId,
            @QueryParam(CURSO_ID_QUERY_PARAM) Long cursoId,
            @QueryParam(SEMESTRE_ID_QUERY_PARAM) Long semestreId,
            @QueryParam(AGRUPACION_ID_QUERY_PARAM) Long agrupacionId,
            @QueryParam(GRUPOS_ID_QUERY_PARAM) String gruposIds)
            throws RegistroNoEncontradoException, UnauthorizedUserException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        ParamUtils.checkNotNull(estudioId, semestreId, gruposIds);

        List<String> gruposList = ParamParseService.parseAsStringList(gruposIds);

        if (cursoId != null)
        {
            return rangoHorarioToUI(rangoHorarioService.getHorarioAjustado(estudioId, cursoId,
                    semestreId, gruposList, connectedUserId));
        }

        if (agrupacionId != null)
        {
            return rangoHorarioToUI(rangoHorarioService.getHorarioPorDefecto());
        }

        throw new IllegalArgumentException();
    }

    @POST
    @Path("config")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<UIEntity> guardaConfiguracion(UIEntity entity) throws ParseException,
            RangoHorarioFueradeLimites, UnauthorizedUserException, RegistroNoEncontradoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        Long estudioId = ParamUtils.parseLong(entity.get("estudioId"));
        Long cursoId = ParamUtils.parseLong(entity.get("cursoId"));
        Long semestreId = ParamUtils.parseLong(entity.get("semestreId"));
        String grupoId = entity.get("grupoId");

        Calendar inicio = Calendar.getInstance();
        Calendar fin = Calendar.getInstance();

        String horaInicio = entity.get("horaInicio");
        String horaFin = entity.get("horaFin");

        inicio.set(Calendar.HOUR_OF_DAY, Integer.parseInt(horaInicio.split(":")[0]));
        inicio.set(Calendar.MINUTE, Integer.parseInt(horaInicio.split(":")[1]));
        inicio.set(Calendar.SECOND, 0);
        fin.set(Calendar.HOUR_OF_DAY, Integer.parseInt(horaFin.split(":")[0]));
        fin.set(Calendar.MINUTE, Integer.parseInt(horaFin.split(":")[1]));
        fin.set(Calendar.SECOND, 0);

        RangoHorario rangoHorario = rangoHorarioService.guardaConfiguracionRangoHorario(estudioId,
                cursoId, semestreId, grupoId, inicio.getTime(), fin.getTime(), connectedUserId);

        return rangoHorarioToUI(rangoHorario);
    }

    @GET
    @Path("eventos/generica")
    @Produces(MediaType.APPLICATION_JSON)
    public CrudManagerEventResponse getEventosGenerica2(@QueryParam(ESTUDIO_ID_QUERY_PARAM) Long estudioId,
                                             @QueryParam(CURSO_ID_QUERY_PARAM) Long cursoId,
                                             @QueryParam(SEMESTRE_ID_QUERY_PARAM) Long semestreId,
                                             @QueryParam(GRUPOS_ID_QUERY_PARAM) String gruposIds,
                                             @QueryParam(AGRUPACION_ID_QUERY_PARAM) Long agrupacionId,
                                             @QueryParam("asignaturasId") String asignaturasId,
                                             @QueryParam(CALENDARIOS_IDS_QUERY_PARAM) String calendariosIds)
            throws ParseException, UnauthorizedUserException, RegistroNoEncontradoException
    {
        try {
            CrudManagerEventResponse response = new CrudManagerEventResponse();

            Long connectedUserId = AccessManager.getConnectedUserId(request);

            ParamUtils.checkNotNull(estudioId, semestreId, gruposIds);

            List<Long> calendariosList = ParamParseService.parseAsLongList(calendariosIds);
            List<String> gruposList = ParamParseService.parseAsStringList(gruposIds);
            List<String> asignaturasList = ParamParseService.parseAsStringList(asignaturasId);

            if (calendariosList.isEmpty()) {
                return response;
            }
            List<Evento> eventos = new ArrayList<>();
            if (cursoId != null) {
                 eventos= eventosService.eventosSemanaGenericaDeUnEstudio(estudioId,
                        cursoId, semestreId, gruposList, asignaturasList, calendariosList,
                        connectedUserId);
            }

            if (agrupacionId != null) {
                eventos = eventosService.eventosSemanaGenericaDeUnaAgrupacion(estudioId,
                        agrupacionId, semestreId, gruposList, asignaturasList, calendariosList,
                        connectedUserId);
            }

            DateFormat formatter = new SimpleDateFormat("dd/MM/yy");

            response.setEvents(eventos.stream().map(
                    evento -> new Event(
                            evento.getId(),
                            evento.getDescripcionConGrupoYComunes(),
                            evento.getInicio(),
                            evento.getFin(),
                            evento.getDescripcionParaUnaAsignatura(),
                            evento.getInicio().getTime(),
                            evento.getCalendario().getId(),
                            evento.getAula() != null? evento.getAula().getPlazas(): null,
                            evento.getEventosDetalle().stream().map(ed -> ed.getInicio())
                                    .sorted().map(f -> formatter.format(f)).collect(Collectors.joining(", ")),
                            evento.getTitulo(),
                            evento.getComentarios(),
                            evento.getAsignaturasComunes(estudioId),
                            evento.getCalendario().getId(),
                            evento.getDiaAsString(),
                            evento.getObservaciones(),
                            evento.getRepetirCadaSemanas(),
                            evento.getDesdeElDia(),
                            evento.getHastaElDia(),
                            evento.getNumeroIteraciones(),
                            evento.hasDetalleManual(),
                            evento.getCreditos(),
                            evento.getCreditosComputables(),
                            evento.getSemestre(),
                            evento.getSubgrupoId(),
                            null
                            )).distinct().collect(Collectors.toList()));
            List<Resource> resources = new ArrayList<>();
            resources.add(new Resource(1, "Teoría", "#1a699c", "TE" ));
            resources.add(new Resource(2, "Problemes", "#64b9d9", "PR" ));
            resources.add(new Resource(3, "Laboratoris", "#2e8f0c", "LA" ));
            resources.add(new Resource(4, "Seminaris", "#176413", "SE" ));
            resources.add(new Resource(5, "Tutories", "#1a5173", "TU" ));
            resources.add(new Resource(6, "Avaluació", "#83ad47", "AV" ));
            resources.add(new Resource(100, "Festiu", "#ff0000", "" ));
            response.setResources(resources);

            List<Assignment> assignments = new ArrayList<>();

            Long index = 0L;
            for (Event event : response.getEvents().getRows()){
                assignments.add(new Assignment(index, new Long(event.getId()), event.getResourceId()));
                ++index;
            }

            response.setAssignments(assignments);

            Boolean success = true;
            response.setSuccess(success);
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            throw new IllegalArgumentException();
        }
    }

    @GET
    @Path("eventos/generica2")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getEventosGenerica(@QueryParam(ESTUDIO_ID_QUERY_PARAM) Long estudioId,
                                             @QueryParam(CURSO_ID_QUERY_PARAM) Long cursoId,
                                             @QueryParam(SEMESTRE_ID_QUERY_PARAM) Long semestreId,
                                             @QueryParam(GRUPOS_ID_QUERY_PARAM) String gruposIds,
                                             @QueryParam(AGRUPACION_ID_QUERY_PARAM) Long agrupacionId,
                                             @QueryParam("asignaturasId") String asignaturasId,
                                             @QueryParam(CALENDARIOS_IDS_QUERY_PARAM) String calendariosIds)
            throws ParseException, UnauthorizedUserException, RegistroNoEncontradoException
    {

        Long connectedUserId = AccessManager.getConnectedUserId(request);

        ParamUtils.checkNotNull(estudioId, semestreId, gruposIds);

        List<Long> calendariosList = ParamParseService.parseAsLongList(calendariosIds);
        List<String> gruposList = ParamParseService.parseAsStringList(gruposIds);
        List<String> asignaturasList = ParamParseService.parseAsStringList(asignaturasId);

        if (calendariosList.isEmpty())
        {
            return new ArrayList<>();
        }

        if (cursoId != null)
        {
            List<Evento> eventos = eventosService.eventosSemanaGenericaDeUnEstudio(estudioId,
                    cursoId, semestreId, gruposList, asignaturasList, calendariosList,
                    connectedUserId);
            return toUI(eventos, estudioId);
        }

        if (agrupacionId != null)
        {
            List<Evento> eventos = eventosService.eventosSemanaGenericaDeUnaAgrupacion(estudioId,
                    agrupacionId, semestreId, gruposList, asignaturasList, calendariosList,
                    connectedUserId);
            return toUI(eventos, estudioId);
        }

        throw new IllegalArgumentException();
    }

    @GET
    @Path("eventos/planificados/asignatura/generica")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getEventosPlanificadosByAsignaturaGenerica(
            @QueryParam(ASIGNATURA_ID_QUERY_PARAM) String asignaturaId,
            @QueryParam(AREA_ID_QUERY_PARAM) Long areaId,
            @QueryParam(SEMESTRE_ID_QUERY_PARAM) Long semestreId,
            @QueryParam(PROFESOR_ID_QUERY_PARAM) Long profesorId,
            @QueryParam(GRUPOS_ID_QUERY_PARAM) String gruposId,
            @QueryParam(TIPO_SUBGRUPOS_ID_QUERY_PARAM) String tipoSubgruposId,
            @QueryParam(OCUPACION_PROFESOR_QUERY_PARAM) Boolean ocupacion,
            @QueryParam(AREA_COMPLETA_QUERY_PARAM) Boolean areaCompleta)
            throws RegistroNoEncontradoException, UnauthorizedUserException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        ParamUtils.checkNotNull(asignaturaId, semestreId);

        if (asignaturaId.isEmpty())
        {
            throw new IllegalArgumentException();
        }

        List<String> gruposList = ParamParseService.parseAsStringList(gruposId);
        List<String> tipoSubgruposList = ParamParseService.parseAsStringList(tipoSubgruposId);

        List<Evento> eventos = eventosService
                .eventosSemanaGenericaPlanificadosByAsignaturaAndSemestre(asignaturaId, semestreId,
                        gruposList, tipoSubgruposList, connectedUserId);

        String codigoComun = asignaturaService.getCodigoComunByAsignaturaId(asignaturaId);
        if (codigoComun != null)
        {
            for (Evento evento : eventos)
            {
                setCodigoComunAsignaturas(evento, codigoComun);
            }
        }

        List<Evento> eventosOcupacion = new ArrayList<>();
        List<UIEntity> listaOcupacionUI = new ArrayList<>();
        if (ocupacion)
        {
            eventosOcupacion = eventosService.eventosProfesor(profesorId, asignaturaId, semestreId,
                    connectedUserId);
            for (Evento evento : eventosOcupacion)
            {
                if (!existeEventoEn(evento, eventos))
                {
                    UIEntity entity = convierteEventoAUIEntityConProfesor(evento, null, profesorId,
                            true);
                    listaOcupacionUI.add(entity);
                }
            }
        }

        List<UIEntity> listaAreaUI = new ArrayList<>();
        if (areaCompleta)
        {
            List<Evento> eventosArea = eventosService.eventosArea(areaId, semestreId,
                    connectedUserId);
            for (Evento evento : eventosArea)
            {
                if (!existeEventoEn(evento, eventos) && !existeEventoEn(evento, eventosOcupacion))
                {
                    UIEntity entity = convierteEventoAUIEntityConProfesor(evento, null, profesorId,
                            true);
                    entity.put("cid", CalendarioAsignatura.getCalendarioRestoArea().getId());
                    listaAreaUI.add(entity);
                }
            }
        }

        List<UIEntity> eventosUI = toUI(eventos, null, profesorId, false);
        eventosUI.addAll(listaOcupacionUI);
        eventosUI.addAll(listaAreaUI);
        return eventosUI;
    }

    @GET
    @Path("eventos/profesor")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getEventosProfesor(@QueryParam("asignaturaId") String asignaturaId, @QueryParam("semestreId") Long semestreId) throws RegistroNoEncontradoException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        if (semestreId == null) {
            return new ArrayList<>();
        }

        DateFormat formatter = new SimpleDateFormat("dd/MM/yy");
        Profesor profesor = profesorService.getProfesorById(connectedUserId);
        List<Evento> eventos = eventosService.getEventosByProfesor(connectedUserId, asignaturaId, semestreId);
        List<UIEntity> eventosUI = new ArrayList<>();
        for (Evento evento : eventos)
        {
            UIEntity entity = convierteEventoAUIEntityConTitulo(evento, null);
            entity.put("cid", CalendarioAsignatura.getCalendarioConMismoProfesorAsignado().getId());
            entity.put("title", evento.getDescripcionParaUnaAsignatura());
            entity.put("profesorId", profesor.getId());
            entity.put("profesorNombre", profesor.getNombre());

            List<EventoDocencia> fechaDocencia = eventosService.getFechasEventoByProfesor(profesor.getId(), evento.getId(), connectedUserId);
            entity.put("fechasDetalles", fechaDocencia.stream().filter(fd -> fd.getDocencia().equals("S")).map(fd -> fd.getFecha())
                    .sorted().map(f -> formatter.format(f)).collect(Collectors.joining(", ")));
            eventosUI.add(entity);
        }
        return eventosUI;
    }

    private boolean existeEventoEn(Evento evento, List<Evento> eventos)
    {
        return eventos.stream().anyMatch(e -> e.getId().equals(evento.getId()));
    }

    @GET
    @Path("eventos/planificados/asignatura/detalle")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getEventosPlanificadosByAsignaturaDetalle(
            @QueryParam(ASIGNATURA_ID_QUERY_PARAM) String asignaturaId,
            @QueryParam(SEMESTRE_ID_QUERY_PARAM) Long semestreId,
            @QueryParam(PROFESOR_ID_QUERY_PARAM) Long profesorId,
            @QueryParam(GRUPOS_ID_QUERY_PARAM) String gruposId,
            @QueryParam(TIPO_SUBGRUPOS_ID_QUERY_PARAM) String tipoSubgruposId)
            throws RegistroNoEncontradoException, UnauthorizedUserException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        ParamUtils.checkNotNull(asignaturaId, semestreId);

        List<String> gruposList = ParamParseService.parseAsStringList(gruposId);
        List<String> tipoSubgruposList = ParamParseService.parseAsStringList(tipoSubgruposId);

        List<EventoDetalle> eventosDetalle = eventosService
                .eventosSemanaDetallePlanificadosByAsignaturaAndSemestre(asignaturaId, semestreId,
                        gruposList, tipoSubgruposList, connectedUserId);

        String codigoComun = asignaturaService.getCodigoComunByAsignaturaId(asignaturaId);
        List<UIEntity> eventosUI = new ArrayList<>();
        for (EventoDetalle eventoDetalle : eventosDetalle)
        {
            if (codigoComun != null)
            {
                setCodigoComunAsignaturas(eventoDetalle.getEvento(), codigoComun);
            }
            eventosUI.add(eventoDetalleAsignaturaConProfesoresToUI(eventoDetalle, profesorId));
        }

        return eventosUI;
    }

    private void setCodigoComunAsignaturas(Evento evento, String codigoComun)
    {
        for (Asignatura asignatura : evento.getAsignaturas())
        {
            asignatura.setCodigoComun(codigoComun);
        }
    }

    @GET
    @Path("eventos/noplanificados/asignatura/generica")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getEventosNoPlanificadosByAsignaturaGenerica(
            @QueryParam(ASIGNATURA_ID_QUERY_PARAM) String asignaturaId,
            @QueryParam(SEMESTRE_ID_QUERY_PARAM) Long semestreId,
            @QueryParam(PROFESOR_ID_QUERY_PARAM) Long profesorId,
            @QueryParam(GRUPOS_ID_QUERY_PARAM) String gruposId,
            @QueryParam(TIPO_SUBGRUPOS_ID_QUERY_PARAM) String tipoSubgruposId,
            @QueryParam(OCUPACION_PROFESOR_QUERY_PARAM) Boolean ocupacion)
            throws RegistroNoEncontradoException, UnauthorizedUserException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        ParamUtils.checkNotNull(asignaturaId, semestreId);

        if (asignaturaId.isEmpty())
        {
            throw new IllegalArgumentException();
        }

        List<String> gruposList = ParamParseService.parseAsStringList(gruposId);
        List<String> tipoSubgruposList = ParamParseService.parseAsStringList(tipoSubgruposId);

        List<Evento> eventos = eventosService
                .eventosSemanaGenericaNoPlanificadosByAsignaturaAndSemestre(asignaturaId,
                        semestreId, gruposList, tipoSubgruposList, connectedUserId);

        String codigoComun = asignaturaService.getCodigoComunByAsignaturaId(asignaturaId);
        if (codigoComun != null)
        {
            for (Evento evento : eventos)
            {
                setCodigoComunAsignaturas(evento, codigoComun);
            }
        }

        List<UIEntity> listaOcupacionUI = new ArrayList<>();
        if (ocupacion)
        {
            List<Evento> eventosOcupacionProfesor = eventosService
                    .getEventosProfesorNoPlanificadosQueNoPertenezcanAsignatura(profesorId,
                            asignaturaId, semestreId, connectedUserId);
            for (Evento evento : eventosOcupacionProfesor)
            {
                if (!existeEventoEn(evento, eventos))
                {
                    UIEntity entity = convierteEventoAUIEntityConProfesor(evento, null, profesorId,
                            true);
                    listaOcupacionUI.add(entity);
                }
            }
        }

        List<UIEntity> eventosUI = toUI(eventos, null, profesorId, false);
        eventosUI.addAll(listaOcupacionUI);
        return eventosUI;
    }

    @GET
    @Path("eventos/detalle")
    @Produces(MediaType.APPLICATION_JSON)
    public CrudManagerEventResponse getEventosDetalle(@QueryParam(ESTUDIO_ID_QUERY_PARAM) Long estudioId,
                                            @QueryParam(CURSO_ID_QUERY_PARAM) Long cursoId,
                                            @QueryParam(AGRUPACION_ID_QUERY_PARAM) Long agrupacionId,
                                            @QueryParam(SEMESTRE_ID_QUERY_PARAM) Long semestreId,
                                            @QueryParam(GRUPOS_ID_QUERY_PARAM) String gruposIds,
                                            @QueryParam("asignaturasId") String asignaturasId,
                                            @QueryParam(CALENDARIOS_IDS_QUERY_PARAM) String calendariosIds,
                                            @QueryParam(START_DATE_QUERY_PARAM) String startDate,
                                            @QueryParam(END_DATE_QUERY_PARAM) String endDate)
            throws ParseException, UnauthorizedUserException, RegistroNoEncontradoException
    {
        CrudManagerEventResponse response = new CrudManagerEventResponse();
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        try
        {
            ParamUtils.checkNotNull(estudioId, semestreId, gruposIds, startDate, endDate);
        }
        catch (Exception e)
        {
            return response;
        }

        Date rangoFechaInicio = new Date(startDate);
        Date rangoFechaFin = new Date(endDate);

        // Todos los eventos hasta el final del día
        Calendar c = Calendar.getInstance();
        c.setTime(rangoFechaFin);
        c.set(Calendar.HOUR_OF_DAY, ULTIMA_HORA_DIA);
        c.set(Calendar.MINUTE, ULTIMO_MINUTO_HORA);
        c.set(Calendar.SECOND, ULTIMO_SEGUNDO_MINUTO);
        rangoFechaFin = c.getTime();

        List<Long> calendariosList = ParamParseService.parseAsLongList(calendariosIds);
        List<String> gruposList = ParamParseService.parseAsStringList(gruposIds);
        List<String> asignaturasList = ParamParseService.parseAsStringList(asignaturasId);

        List<EventoDetalle> eventosDetalle = new ArrayList<EventoDetalle>();

        if (calendariosList.isEmpty())
        {
            return response;
        }

        if (cursoId != null)
        {
            eventosDetalle = eventosService.eventosDetalleDeUnEstudio(estudioId, cursoId,
                    semestreId, gruposList, asignaturasList, calendariosList, rangoFechaInicio,
                    rangoFechaFin, connectedUserId);
        }

        if (agrupacionId != null)
        {
            eventosDetalle = eventosService.eventosDetalleDeUnEstudioPorAgrupacion(estudioId,
                    agrupacionId, semestreId, gruposList, asignaturasList, calendariosList,
                    rangoFechaInicio, rangoFechaFin, connectedUserId);
        }

        List<UIEntity> events = eventosDetalletoUI(eventosDetalle, estudioId);;

        DateFormat formatter = new SimpleDateFormat("dd/MM/yy");

        response.setEvents(eventosDetalle.stream().map(
                evento -> new Event(
                        evento.getId(),
                        evento.getDescripcionConGrupoYComunes(),
                        evento.getInicio(),
                        evento.getFin(),
                        evento.getEvento().getDescripcionParaUnaAsignatura(),
                        evento.getInicio().getTime(),
                        evento.getEvento().getCalendario().getId(),
                        evento.getEvento().getAula() != null? evento.getEvento().getAula().getPlazas(): null,
                        evento.getEvento().getEventosDetalle().stream().map(ed -> ed.getInicio())
                                .sorted().map(f -> formatter.format(f)).collect(Collectors.joining(", ")),
                        evento.getEvento().getTitulo(),
                        evento.getEvento().getComentarios(),
                        evento.getEvento().getAsignaturasComunes(estudioId),
                        evento.getEvento().getCalendario().getId(),
                        evento.getEvento().getDiaAsString(),
                        evento.getEvento().getObservaciones(),
                        evento.getEvento().getRepetirCadaSemanas(),
                        evento.getEvento().getDesdeElDia(),
                        evento.getEvento().getHastaElDia(),
                        evento.getEvento().getNumeroIteraciones(),
                        evento.getEvento().hasDetalleManual(),
                        evento.getEvento().getCreditos(),
                        evento.getEvento().getCreditosComputables(),
                        evento.getEvento().getSemestre(),
                        evento.getEvento().getSubgrupoId(),
                        evento.getEvento().getId()
                        )).distinct().collect(Collectors.toList()));
        response.getEvents().getRows().addAll(getDiasFestivosL(TipoEstudio.getTipoEstudioId(estudioId), rangoFechaInicio, rangoFechaFin, connectedUserId));
        List<Resource> resources = new ArrayList<>();
        resources.add(new Resource(1, "Teoría", "#1a699c", "TE" ));
        resources.add(new Resource(2, "Problemes", "#64b9d9", "PR" ));
        resources.add(new Resource(3, "Laboratoris", "#2e8f0c", "LA" ));
        resources.add(new Resource(4, "Seminaris", "#176413", "SE" ));
        resources.add(new Resource(5, "Tutories", "#1a5173", "TU" ));
        resources.add(new Resource(6, "Avaluació", "#83ad47", "AV" ));
        resources.add(new Resource(100, "Festiu", "#ff0000", "" ));
        response.setResources(resources);

        List<Assignment> assignments = new ArrayList<>();

        Long index = 0L;
        for (Event event : response.getEvents().getRows()){
            assignments.add(new Assignment(index, new Long(event.getId()), event.getResourceId()));
            ++index;
        }

        response.setAssignments(assignments);

        Boolean success = true;
        response.setSuccess(success);
        return response;
    }

    @GET
    @Path("eventos/detalle2")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getEventosDetalle2(@QueryParam(ESTUDIO_ID_QUERY_PARAM) Long estudioId,
                                            @QueryParam(CURSO_ID_QUERY_PARAM) Long cursoId,
                                            @QueryParam(AGRUPACION_ID_QUERY_PARAM) Long agrupacionId,
                                            @QueryParam(SEMESTRE_ID_QUERY_PARAM) Long semestreId,
                                            @QueryParam(GRUPOS_ID_QUERY_PARAM) String gruposIds,
                                            @QueryParam("asignaturasId") String asignaturasId,
                                            @QueryParam(CALENDARIOS_IDS_QUERY_PARAM) String calendariosIds,
                                            @QueryParam(START_DATE_QUERY_PARAM) String startDate,
                                            @QueryParam(END_DATE_QUERY_PARAM) String endDate)
            throws ParseException, UnauthorizedUserException, RegistroNoEncontradoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        try
        {
            ParamUtils.checkNotNull(estudioId, semestreId, gruposIds, startDate, endDate);
        }
        catch (Exception e)
        {
            return new ArrayList<>();
        }

        Date rangoFechaInicio = queryParamDateFormat.parse(startDate);
        Date rangoFechaFin = queryParamDateFormat.parse(endDate);

        // Todos los eventos hasta el final del día
        Calendar c = Calendar.getInstance();
        c.setTime(rangoFechaFin);
        c.set(Calendar.HOUR_OF_DAY, ULTIMA_HORA_DIA);
        c.set(Calendar.MINUTE, ULTIMO_MINUTO_HORA);
        c.set(Calendar.SECOND, ULTIMO_SEGUNDO_MINUTO);
        rangoFechaFin = c.getTime();

        List<Long> calendariosList = ParamParseService.parseAsLongList(calendariosIds);
        List<String> gruposList = ParamParseService.parseAsStringList(gruposIds);
        List<String> asignaturasList = ParamParseService.parseAsStringList(asignaturasId);

        List<EventoDetalle> eventosDetalle = new ArrayList<EventoDetalle>();

        if (calendariosList.isEmpty())
        {
            return new ArrayList<>();
        }

        if (cursoId != null)
        {
            eventosDetalle = eventosService.eventosDetalleDeUnEstudio(estudioId, cursoId,
                    semestreId, gruposList, asignaturasList, calendariosList, rangoFechaInicio,
                    rangoFechaFin, connectedUserId);
        }

        if (agrupacionId != null)
        {
            eventosDetalle = eventosService.eventosDetalleDeUnEstudioPorAgrupacion(estudioId,
                    agrupacionId, semestreId, gruposList, asignaturasList, calendariosList,
                    rangoFechaInicio, rangoFechaFin, connectedUserId);
        }

        List<UIEntity> events = eventosDetalletoUI(eventosDetalle, estudioId);
        events.addAll(getDiasFestivos(TipoEstudio.getTipoEstudioId(estudioId), rangoFechaInicio, rangoFechaFin, connectedUserId));

        return events;
    }

    @PUT
    @Path("eventos/detalle/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> updateEventoSemanaDetalle(UIEntity entity) throws ParseException,
            DuracionEventoIncorrectaException, RegistroNoEncontradoException,
            UnauthorizedUserException, EventoFueraDeRangoException,
            EventoMasDeUnaRepeticionException, EventoFueraDeFechasSemestreException,
            DiaNoLectivoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        String usuario = AccessManager.getConnectedUser(request).getName();

        DateFormat uIEntityDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date inicio = uIEntityDateFormat.parse(entity.get("start"));
        Date fin = uIEntityDateFormat.parse(entity.get("end"));
        Long eventoId = entity.getLong("eventoId");

        Evento eventoOriginal = eventosService.getEventoById(eventoId, connectedUserId);

        eventosService.modificaDiaYHoraEventoEnVistaDetalle(eventoId, inicio, fin, connectedUserId);

        Evento evento = eventosService.getEventoById(eventoId, connectedUserId);

        logService.registraModificacionEventoDetalle(eventoOriginal, evento, usuario, connectedUserId);

        return Collections.singletonList(entity);
    }

    private UIEntity eventoDetalleAsignaturaConProfesoresToUI(EventoDetalle eventoDetalle,
                                                              Long profesorId)
    {
        UIEntity eventoUI = new UIEntity();
        eventoUI.put("id", eventoDetalle.getId());
        eventoUI.put("cid", CalendarioAsignatura.getCalendarioSinProfesoresAsignados().getId());
        eventoUI.put("eventoId", eventoDetalle.getEvento().getId());
        eventoUI.put("title", eventoDetalle.getDescripcionConAsignaturaComun());
        eventoUI.put("start", uIEntitydateFormat.format(eventoDetalle.getInicio()));
        eventoUI.put("end", uIEntitydateFormat.format(eventoDetalle.getFin()));
        eventoUI.put("comentarios", eventoDetalle.getEvento().getComentarios());
        if (eventoDetalle.getProfesores() != null && eventoDetalle.getProfesores().size() > 0)
        {
            List<String> nombresProfesores = new ArrayList<String>();
            Boolean encontrado = false;
            for (Profesor profesor : eventoDetalle.getProfesores())
            {
                if (profesor.getId().equals(profesorId))
                {
                    encontrado = true;
                }
                nombresProfesores.add(profesor.getNombre());
            }

            eventoUI.put("cid",
                    encontrado
                            ? CalendarioAsignatura.getCalendarioConMismoProfesorAsignado().getId()
                            : CalendarioAsignatura.getCalendarioConProfesoresAsignados().getId());

            eventoUI.put("profesores", StringUtils.join(nombresProfesores, ", "));
        }

        return eventoUI;
    }

    private UIEntity eventoDetalleToUI(EventoDetalle eventoDetalle, Long estudioId)
    {
        UIEntity eventoUI = new UIEntity();
        eventoUI.put("id", eventoDetalle.getId());
        eventoUI.put("eventoId", eventoDetalle.getEvento().getId());

        eventoUI.put("cid", eventoDetalle.getEvento().getCalendario().getId());
        eventoUI.put("comentarios", eventoDetalle.getEvento().getComentarios());

        if (estudioId != null)
        {
            eventoUI.put("title", eventoDetalle.getDescripcionParaUnEstudio(estudioId));
            eventoUI.put("nombreAsignatura",
                    eventoDetalle.getEvento().getAsignaturaDelEstudio(estudioId).getNombre());
        }
        else
        {
            eventoUI.put("title", eventoDetalle.getDescripcionConPrimeraAsignatura());
        }

        if (eventoDetalle.getEvento().getAula() != null)
        {
            eventoUI.put("plazasAula", eventoDetalle.getEvento().getAula().getPlazas());
        }

        if (eventoDetalle.getInicio() != null)
        {
            eventoUI.put("start", uIEntitydateFormat.format(eventoDetalle.getInicio()));
        }

        if (eventoDetalle.getFin() != null)
        {
            eventoUI.put("end", uIEntitydateFormat.format(eventoDetalle.getFin()));
        }

        if (eventoDetalle.getProfesores() != null)
        {
            List<String> nombresProfesores = new ArrayList<String>();
            for (Profesor profesor : eventoDetalle.getProfesores())
            {
                nombresProfesores.add(profesor.getNombre());
            }
            eventoUI.put("profesores", StringUtils.join(nombresProfesores, ", "));
        }
        return eventoUI;
    }

    private List<UIEntity> eventosDetalletoUI(List<EventoDetalle> eventosDetalle, Long estudioId)
    {
        List<UIEntity> eventosUI = new ArrayList<UIEntity>();

        for (EventoDetalle eventoDetalle : eventosDetalle)
        {
            eventosUI.add(eventoDetalleToUI(eventoDetalle, estudioId));
        }

        return eventosUI;
    }

    @PUT
    @Path("eventos/generica/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> updateEventoSemanaGenerica(UIEntity entity) throws ParseException,
            DuracionEventoIncorrectaException, JSONException, RegistroNoEncontradoException,
            EventoDetalleSinEventoException, UnauthorizedUserException, EventoFueraDeRangoException
    {

        Long connectedUserId = AccessManager.getConnectedUserId(request);
        String usuario = AccessManager.getConnectedUser(request).getName();

        DateFormat uIEntityDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date inicio = uIEntityDateFormat.parse(entity.get("start"));
        Date fin = uIEntityDateFormat.parse(entity.get("end"));
        Long idEvento = (Long.parseLong(entity.get("id")));
        String comentarios = entity.get("comentarios");
        String tipoSubgrupoId = entity.get("tipoSubgrupoId");
        Long subgrupoId = Long.parseLong(entity.get("subgrupoId"));

        Evento eventoOriginal = eventosService.getEventoById(idEvento);
        if (!enviandoDatosDetalleEvento(entity))
        {
            Evento evento = eventosService.modificaDiaYHoraEvento(idEvento, inicio, fin, tipoSubgrupoId, subgrupoId,connectedUserId);

            logService.registraModificacionDeEvento(getTipoAccion(eventoOriginal, evento), eventoOriginal, evento, usuario, connectedUserId);
            return toUI(Collections.singletonList(evento));
        }
        else
        {
            if (enviandoDetalleManual(entity))
            {
                List<Date> fechas = getListaFechasDetalleManual(entity);
                Evento evento = eventosService.updateEventoConDetalleManual(idEvento, fechas,
                        inicio, fin, comentarios, tipoSubgrupoId, subgrupoId, connectedUserId);

                logService.registraModificacionDeEvento(Log.ACCION_MOVER_ITEM, eventoOriginal, evento, usuario, connectedUserId);

                return toUI(Collections.singletonList(evento));
            }
            else
            {
                Date desdeElDia = valorPropiedadDateDeUIEntity(entity, "start_date_rep");
                Integer repetirCadaSemanas = valorPropiedadIntegerDeUIEntity(entity,
                        "repetir_cada");
                Integer numeroIteraciones = null;
                Date hastaElDia = null;
                if (finalizaRepeticionesPorNumero(entity))
                {
                    numeroIteraciones = valorPropiedadIntegerDeUIEntity(entity,
                            "end_rep_number_comp");
                }
                else if (finalizaRepeticionesPorFecha(entity))
                {
                    hastaElDia = valorPropiedadDateDeUIEntity(entity, "end_date_rep_comp");
                }

                eventosService.modificaDetallesGrupoAsignatura(idEvento, inicio, fin, desdeElDia,
                        numeroIteraciones, repetirCadaSemanas, hastaElDia, false, comentarios, tipoSubgrupoId, subgrupoId,
                        connectedUserId);

                Evento evento = eventosService.getEventoById(idEvento, connectedUserId);

                logService.registraModificacionDeEvento(Log.ACCION_EDICION_DETALLE_ITEM, eventoOriginal, evento, usuario, connectedUserId);

                return toUI(Collections.singletonList(evento));
            }
        }
    }

    private int getTipoAccion(Evento original, Evento modificado)
    {
        if (!modificado.hasDetalleManual())
        {
            return Log.ACCION_MOVER_ITEM;
        }

        return original.getDia().equals(modificado.getDia()) ? Log.ACCION_MOVER_ITEM : Log.ACCION_MOVER_ITEM_DETALLE_MANUAL;
    }

    private boolean finalizaRepeticionesPorNumero(UIEntity entity)
    {
        String criterioFinalizacionRepeticiones = valorPropiedadDeUIEntity(entity,
                "seleccionRadioFechaFin");
        return criterioFinalizacionRepeticiones.equals("R");
    }

    private boolean finalizaRepeticionesPorFecha(UIEntity entity)
    {
        String criterioFinalizacionRepeticiones = valorPropiedadDeUIEntity(entity,
                "seleccionRadioFechaFin");
        return criterioFinalizacionRepeticiones.equals("D");
    }

    private Date valorPropiedadDateDeUIEntity(UIEntity entity, String clave) throws ParseException
    {
        DateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");

        String valor = valorPropiedadDeUIEntity(entity, clave);
        if (valor == null)
        {
            return null;
        }
        else
        {
            return dateFormatter.parse(valor);
        }
    }

    private Integer valorPropiedadIntegerDeUIEntity(UIEntity entity, String clave)
    {
        String valor = valorPropiedadDeUIEntity(entity, clave);
        if (valor == null)
        {
            return null;
        }
        else
        {
            return Integer.parseInt(valor);
        }
    }

    private String valorPropiedadDeUIEntity(UIEntity entity, String clave)
    {
        if (entity.keySet().contains(clave) && entity.get(clave).length() > 0)
        {
            return entity.get(clave);
        }
        else
        {
            return null;
        }
    }

    private List<Date> getListaFechasDetalleManual(UIEntity entity)
            throws JSONException, ParseException
    {
        DateFormat formatoFechaManual = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        String strFechas = entity.get("fecha_detalle_manual_int");
        JSONArray array = new JSONArray(strFechas);

        List<Date> fechas = new ArrayList<Date>();
        for (int i = 0; i < array.length(); i++)
        {
            fechas.add(formatoFechaManual.parse((String) array.get(i)));
        }
        return fechas;
    }

    private boolean enviandoDatosDetalleEvento(UIEntity entity)
    {
        Integer posteoDetalle = Integer.parseInt(entity.get("posteo_detalle"));
        return posteoDetalle == 1;
    }

    private boolean enviandoDetalleManual(UIEntity entity)
    {
        return entity.get("detalle_manual") != null && entity.get("detalle_manual").equals("on");
    }

    @DELETE
    @Path("eventos/generica/{id}")
    public Response deleteEventoSemanaGenerica(@PathParam(ID_PATH_PARAM) String eventoId)
            throws RegistroNoEncontradoException, NumberFormatException, UnauthorizedUserException
    {

        Long connectedUserId = AccessManager.getConnectedUserId(request);
        String usuario = AccessManager.getConnectedUser(request).getName();

        eventosService.deleteEventoSemanaGenerica(Long.parseLong(eventoId), connectedUserId, usuario);
        return Response.ok().build();
    }

    @POST
    @Path("eventos/generica/divide/{id}")
    public Response divideEventoSemanaGenerica(@PathParam(ID_PATH_PARAM) String eventoId)
            throws RegistroNoEncontradoException, EventoNoDivisibleException, NumberFormatException,
            UnauthorizedUserException
    {

        Long connectedUserId = AccessManager.getConnectedUserId(request);
        String usuario = AccessManager.getConnectedUser(request).getName();

        Evento eventoOriginal = eventosService.getEventoById(ParamUtils.parseLong(eventoId),
                connectedUserId);

        eventosService.divideEventoSemanaGenerica(Long.parseLong(eventoId), connectedUserId);
        logService.registraEventoDividido(eventoOriginal, usuario, connectedUserId);

        return Response.ok().build();
    }

    private List<UIEntity> toUI(List<Evento> eventos, Long estudioId, Long profesorId,
                                Boolean ocupacion)
    {
        List<UIEntity> eventosUI = new ArrayList<UIEntity>();

        for (Evento evento : eventos)
        {
            UIEntity eventoUI = convierteEventoAUIEntityConProfesor(evento, estudioId, profesorId,
                    ocupacion);

            eventosUI.add(eventoUI);
        }

        return eventosUI;
    }

    private List<UIEntity> toUI(List<Evento> eventos, Long estudioId)
    {
        List<UIEntity> eventosUI = new ArrayList<UIEntity>();

        for (Evento evento : eventos)
        {
            UIEntity eventoUI = convierteEventoAUIEntityConTitulo(evento, estudioId);

            eventosUI.add(eventoUI);
        }

        return eventosUI;
    }

    private List<UIEntity> toUI(List<Evento> eventos)
    {
        List<UIEntity> eventosUI = new ArrayList<UIEntity>();

        for (Evento evento : eventos)
        {
            UIEntity eventoUI = convierteEventoAUIEntity(evento, null);

            eventosUI.add(eventoUI);
        }

        return eventosUI;
    }

    public UIEntity convierteEventoAUIEntityConTitulo(Evento evento, Long estudioId)
    {
        UIEntity entity = convierteEventoAUIEntity(evento, estudioId);
        entity.put("title", evento.getDescripcionParaUnEstudio(estudioId));
        entity.put("descripcion", evento.getDescripcionCompletaParaUnEstudio(estudioId));
        Asignatura asignatura = evento.getAsignaturaDelEstudio(estudioId);
        entity.put("nombreAsignatura", (asignatura != null) ? asignatura.getNombre() : "");
        entity.put("creditos", evento.getCreditos());
        entity.put("creditosComputables", evento.getCreditosComputables());
        entity.put("detalle_manual_profesor", hasDetalleManualPod(evento));
        entity.put("comentarios", evento.getComentarios());
        entity.put("semestre", evento.getSemestre());
        if (evento.getAula() != null)
        {
            entity.put("plazasAula", evento.getAula().getPlazas());
        }
        DateFormat formatter = new SimpleDateFormat("dd/MM/yy");
        String fechasDetalles = evento.getEventosDetalle().stream().map(ed -> ed.getInicio())
                .sorted().map(f -> formatter.format(f)).collect(Collectors.joining(", "));
        entity.put("fechasDetalles", fechasDetalles);
        entity.put("subgrupoId", evento.getSubgrupoId());
        return entity;
    }

    private Boolean hasDetalleManualPod(Evento evento)
    {
        for (ProfesorDetalle profesorDetalle : evento.getProfesoresDetalle())
        {
            if (profesorDetalle.getDetalleManual())
            {
                return true;
            }
        }
        return false;
    }

    private UIEntity convierteEventoAUIEntity(Evento evento, Long estudioId)
    {
        UIEntity eventoUI = new UIEntity();
        eventoUI.put("id", evento.getId());
        eventoUI.put("cid", evento.getCalendario().getId());
        eventoUI.put("notes", evento.getObservaciones());
        eventoUI.put("repetir_cada", evento.getRepetirCadaSemanas());
        eventoUI.put("start_date_rep", evento.getDesdeElDia());
        eventoUI.put("end_date_rep_comp", evento.getHastaElDia());
        eventoUI.put("end_rep_number_comp", evento.getNumeroIteraciones());
        eventoUI.put("detalle_manual", evento.hasDetalleManual());
        eventoUI.put("creditos", evento.getCreditos());
        eventoUI.put("creditosComputables", evento.getCreditosComputables());
        eventoUI.put("dia_semana", evento.getDiaAsString());
        eventoUI.put("comentarios", evento.getComentarios());
        eventoUI.put("semestre", evento.getSemestre());

        if ((evento.getProfesoresDetalle() != null) && (evento.getProfesoresDetalle().size() > 0))
        {
            eventoUI.put("detalle_manual_profesor",
                    evento.getProfesoresDetalle().get(0).getDetalleManual());
            eventoUI.put("creditos_profesor", evento.getProfesoresDetalle().get(0).getCreditos());
            eventoUI.put("creditos_computables_profesor",
                    evento.getProfesoresDetalle().get(0).getCreditosComputables());
            eventoUI.put("item_profesor_id", evento.getProfesoresDetalle().get(0).getId());
            eventoUI.put("creditos_profesor_calculados", evento.getProfesoresDetalle().get(0).getCreditosCalculados());
        }

        if (estudioId != null && evento.tieneComunes())
        {
            eventoUI.put("comunes", evento.getAsignaturasComunes(estudioId));
        }

        if (evento.getAsignaturas() != null)
        {
            eventoUI.put("titulo", evento.getDescripcionParaUnaAsignatura());
        }

        if (evento.getAula() != null)
        {
            eventoUI.put("aula_id", evento.getAula().getId());
            eventoUI.put("aula_codigo", evento.getAula().getCodigo());
        }

        if (evento.getInicio() != null)
        {
            eventoUI.put("start", uIEntitydateFormat.format(evento.getInicio()));
        }

        if (evento.getFin() != null)
        {
            eventoUI.put("end", uIEntitydateFormat.format(evento.getFin()));
        }
        return eventoUI;
    }

    private UIEntity convierteEventoAUIEntityConProfesor(Evento evento, Long estudioId,
                                                         Long profesorId, Boolean ocupacion)
    {
        String tituloEvento = new String();
        UIEntity eventoUI = new UIEntity();
        eventoUI.put("id", evento.getId());
        eventoUI.put("notes", evento.getObservaciones());
        eventoUI.put("cid", CalendarioAsignatura.getCalendarioSinProfesoresAsignados().getId());
        eventoUI.put("repetir_cada", evento.getRepetirCadaSemanas());
        eventoUI.put("start_date_rep", evento.getDesdeElDia());
        eventoUI.put("end_date_rep_comp", evento.getHastaElDia());
        eventoUI.put("end_rep_number_comp", evento.getNumeroIteraciones());
        eventoUI.put("detalle_manual", evento.hasDetalleManual());
        eventoUI.put("creditos", evento.getCreditos());
        eventoUI.put("creditosComputables", evento.getCreditosComputables());
        eventoUI.put("comentarios", evento.getComentarios());

        if (estudioId != null && evento.tieneComunes())
        {
            eventoUI.put("comunes", evento.getAsignaturasComunes(estudioId));
        }

        if (evento.getAsignaturas() != null)
        {
            tituloEvento = evento.getDescripcionParaUnaAsignatura();
            if (evento.getAsignaturas().size() > 0)
            {
                eventoUI.put("tipo_asignatura_id",
                        evento.getAsignaturas().get(0).getTipoAsignaturaId());
            }
        }

        if (evento.getAula() != null)
        {
            eventoUI.put("aula_id", evento.getAula().getId());
        }

        if (evento.getCreditos() != null)
        {
            tituloEvento += " (" + evento.getCreditos().toString() + " crèd.";

            if (evento.getCreditosComputables() != null)
            {
                tituloEvento += " " + evento.getCreditosComputables().toString() + " comp.";
            }

            tituloEvento += ")";
        }

        if (evento.getProfesores() != null)
        {
            Boolean profesorIncluido = false;
            List<String> nombresProfesores = new ArrayList<String>();
            BigDecimal creditosAsignados = new BigDecimal("0");
            BigDecimal creditosAsignadosComputables = new BigDecimal("0");

            for (Profesor profesor : evento.getProfesores())
            {
                boolean profesorTieneSesiones = profesorTieneSesionesEn(evento, profesor.getId());
                if (profesor.getId().equals(profesorId))
                {
                    profesorIncluido = true;
                    if (!ocupacion)
                    {
                        if (profesorTieneSesiones)
                        {
                            eventoUI.put("cid", CalendarioAsignatura
                                    .getCalendarioConMismoProfesorAsignado().getId());
                        }
                        else
                        {
                            eventoUI.put("cid", CalendarioAsignatura
                                    .getCalendarioConMismoProfesorAsignadoSinSesiones().getId());
                        }
                    }
                    else
                    {
                        eventoUI.put("cid",
                                CalendarioAsignatura.getCalendarioConOcupacionProfesor().getId());
                    }
                }
                String descripcion = profesor.getNombre();
                if (profesor.getCreditos() != null)
                {
                    if (profesorTieneSesiones)
                    {
                        if (evento.getAsignaturas() != null && evento.getAsignaturas().get(0).isMaster())
                        {
                            descripcion += MessageFormat.format(" ({0} crèdits, {1} comp.)",
                                    profesor.getCreditos(), profesor.getCreditosComputables());
                        }
                        else
                        {
                            descripcion += MessageFormat.format(" ({0} crèdits)",
                                    profesor.getCreditos());
                        }

                        /*Optional profesorDetalleOptional = evento.getProfesoresDetalle().stream()
                                .filter(pd -> pd.getProfesor().getId().equals(profesor.getId()))
                                .findFirst();
                        if (profesorDetalleOptional.isPresent())
                        {
                            ProfesorDetalle profesorDetalle = (ProfesorDetalle) profesorDetalleOptional
                                    .get();
                            if (profesorDetalle.getCreditosComputables() != null)
                            {
                                descripcion += MessageFormat.format(", {0} comp...",
                                        profesorDetalle.getCreditosComputables());
                            }
                        }
                        descripcion += ")";*/
                    }
                    else
                    {
                        descripcion += " (Sense sessions)";
                    }
                    creditosAsignados = creditosAsignados.add(profesor.getCreditos());
                    creditosAsignadosComputables = creditosAsignadosComputables.add(profesor.getCreditosComputables());
                }

                if (profesor.getId() != null)
                {
                    List<String> idiomas = idiomaService
                            .getIdiomasProfesorItem(profesor.getId(), evento.getId()).stream()
                            .filter(i -> i.isActivo()).map(i -> i.getAcronimo())
                            .collect(Collectors.toList());
                    if (idiomas.size() > 0)
                    {
                        descripcion += " [" + String.join(", ", idiomas) + "]";
                    }
                }
                nombresProfesores.add(descripcion);
            }

            eventoUI.put("creditosAsignados", creditosAsignados);
            eventoUI.put("creditosAsignadosComputables", creditosAsignadosComputables);
            if (nombresProfesores.size() > 0 && profesorIncluido == false
                    && eventoTieneSesiones(evento))
            {
                eventoUI.put("cid",
                        CalendarioAsignatura.getCalendarioConProfesoresAsignados().getId());
            }

            Collections.sort(nombresProfesores);
            eventoUI.put("profesores", StringUtils.join(nombresProfesores, "; "));
            tituloEvento += " (" + evento.getProfesores().size() + " prof.)";
        }

        if (evento.getProfesoresDetalle() != null)
        {
            for (ProfesorDetalle profesorDetalle : evento.getProfesoresDetalle())
            {
                if (profesorDetalle.getProfesor().getId().equals(profesorId))
                {
                    eventoUI.put("detalle_manual_profesor", profesorDetalle.getDetalleManual());
                    eventoUI.put("creditos_profesor", profesorDetalle.getCreditos());
                    eventoUI.put("creditos_computables_profesor",
                            profesorDetalle.getCreditosComputables());
                }
            }
        }

        eventoUI.put("title", tituloEvento);

        if (evento.getInicio() != null)
        {
            eventoUI.put("start", uIEntitydateFormat.format(evento.getInicio()));
        }

        if (evento.getFin() != null)
        {
            eventoUI.put("end", uIEntitydateFormat.format(evento.getFin()));
        }
        return eventoUI;
    }

    private boolean profesorTieneSesionesEn(Evento evento, Long profesorId)
    {
        Optional<ProfesorDetalle> profesorDetalleOptional = evento.getProfesoresDetalle().stream()
                .filter(pd -> pd.getProfesor().getId().equals(profesorId)).findFirst();
        ProfesorDetalle profesorDetalle;
        try
        {
            profesorDetalle = profesorDetalleOptional.get();
        }
        catch (NoSuchElementException e)
        {
            return false;
        }
        if (!profesorDetalle.getDetalleManual())
        {
            return true;
        }
        return !profesorDetalle.getEventosDetalle().isEmpty();
    }

    private boolean eventoTieneSesiones(Evento evento)
    {
        for (ProfesorDetalle profesorDetalle : evento.getProfesoresDetalle())
        {
            if (!profesorDetalle.getDetalleManual()
                    || !profesorDetalle.getEventosDetalle().isEmpty())
            {
                return true;
            }
        }
        return false;
    }

    private List<UIEntity> rangoHorarioToUI(RangoHorario rangoHorario)
    {
        UIEntity rangoHorarioUI = new UIEntity();
        rangoHorarioUI.put("estudioId", rangoHorario.getEstudioId());
        rangoHorarioUI.put("cursoId", rangoHorario.getCursoId());
        rangoHorarioUI.put("semestreId", rangoHorario.getSemestreId());
        rangoHorarioUI.put("grupoId", rangoHorario.getGrupoId());
        rangoHorarioUI.put("horaFin", rangoHorario.getHoraFin());
        rangoHorarioUI.put("horaInicio", rangoHorario.getHoraInicio());

        return Collections.singletonList(rangoHorarioUI);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getCalendarios()
    {

        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<UIEntity> calendars = new ArrayList<UIEntity>();

        List<Calendario> calendarios = calendariosService.getCalendarios(connectedUserId);

        int i = 1;
        for (Calendario calendario : calendarios)
        {
            UIEntity calendar = new UIEntity();
            calendar.put("id", calendario.getId());
            calendar.put("title", calendario.getNombre());
            calendar.put("color", COLOR_BASE + i);

            calendars.add(calendar);
            i++;
        }
        calendars.add(getCalendarioFestivos());

        return calendars;
    }

    @GET
    @Path("circuito")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getCalendariosCircuitos()
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<UIEntity> calendars = new ArrayList<UIEntity>();

        List<CalendarioCircuito> calendarios = calendariosService
                .getCalendariosCircuitos(connectedUserId);

        for (CalendarioCircuito calendario : calendarios)
        {
            UIEntity calendar = new UIEntity();
            calendar.put("id", calendario.getId());
            calendar.put("title", calendario.getTitulo());
            calendar.put("color", calendario.getColor());

            calendars.add(calendar);
        }
        calendars.add(getCalendarioFestivos());

        return calendars;
    }

    @GET
    @Path("eventos/docencia/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getEventosDocenciaByEventoId(@PathParam(ID_PATH_PARAM) String eventoId)
            throws RegistroNoEncontradoException, UnauthorizedUserException
    {

        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<EventoDocencia> eventosDocencia = eventosService.getDiasDocenciaDeUnEventoByEventoId(
                ParamUtils.parseLong(eventoId), connectedUserId);

        return UIEntity.toUI(eventosDocencia);
    }

    @PUT
    @Path("eventos/aula/evento/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> actualizaAulaAsignadaAEvento(@PathParam(ID_PATH_PARAM) String eventoId,
                                                       @FormParam(AULA_ID_PARAM) String aulaId,
                                                       @FormParam(TIPO_ACCION_PARAM) String tipoAccion,
                                                       @QueryParam(ESTUDIO_ID_QUERY_PARAM) String estudioId)
            throws RegistroNoEncontradoException, AulaNoAsignadaAEstudioDelEventoException,
            NumberFormatException, UnauthorizedUserException
    {

        Long connectedUserId = AccessManager.getConnectedUserId(request);
        String usuario = AccessManager.getConnectedUser(request).getName();

        boolean propagar = tipoAccion.equals("T");
        Long aula;
        Long estudio;
        try
        {
            aula = Long.parseLong(aulaId);

        }
        catch (Exception e)
        {
            aula = null;
        }
        try
        {
            estudio = Long.parseLong(estudioId);
        }
        catch (Exception e)
        {
            estudio = null;
        }

        List<Evento> eventos = eventosService.actualizaAulaAsignadaAEvento(Long.parseLong(eventoId),
                aula, propagar, connectedUserId, usuario);

        if (estudio != null)
        {
            return toUI(eventos, estudio);
        }
        else
        {
            return eventosSemanaGenericaPorAulaToUI(eventos);
        }
    }

    @GET
    @Path("eventos/aula/detalle")
    @Produces(MediaType.APPLICATION_JSON)
    public CrudManagerEventResponse getEventosDetalleByAula(@QueryParam(AULA_ID_PARAM) String aulaId,
                                                  @QueryParam(SEMESTRE_ID_QUERY_PARAM) String semestreId,
                                                  @QueryParam(CALENDARIOS_IDS_QUERY_PARAM) String calendariosIds,
                                                  @QueryParam(START_DATE_QUERY_PARAM) String startDate,
                                                  @QueryParam(END_DATE_QUERY_PARAM) String endDate)
            throws ParseException, RegistroNoEncontradoException, UnauthorizedUserException
    {
        CrudManagerEventResponse response = new CrudManagerEventResponse();
        try
        {
            ParamUtils.checkNotNull(aulaId, semestreId, startDate, endDate);
        }
        catch (Exception e)
        {
            return response;
        }

        Long connectedUserId = AccessManager.getConnectedUserId(request);

        Date rangoFechaInicio = new Date(startDate);
        Date rangoFechaFin = new Date(endDate);

        // Todos los eventos hasta el final del día
        Calendar c = Calendar.getInstance();
        c.setTime(rangoFechaFin);
        c.set(Calendar.HOUR_OF_DAY, ULTIMA_HORA_DIA);
        c.set(Calendar.MINUTE, ULTIMO_MINUTO_HORA);
        c.set(Calendar.SECOND, ULTIMO_SEGUNDO_MINUTO);
        rangoFechaFin = c.getTime();

        List<Long> calendariosList = ParamParseService.parseAsLongList(calendariosIds);

        List<EventoDetalle> eventosDetalle = new ArrayList<EventoDetalle>();

        if (calendariosList.size() != 0)
        {
            eventosDetalle = eventosService.getEventosDetallePorAula(ParamUtils.parseLong(aulaId),
                    ParamUtils.parseLong(semestreId), calendariosList, rangoFechaInicio,
                    rangoFechaFin, connectedUserId);
        }

        List<UIEntity> events = eventosDetallePorAulaToUI(eventosDetalle);

        DateFormat formatter = new SimpleDateFormat("dd/MM/yy");
        response.setEvents(eventosDetalle.stream().map(
                evento -> new Event(
                        evento.getId(),
                        evento.getDescripcionConGrupoYComunes(),
                        evento.getInicio(),
                        evento.getFin(),
                        evento.getDescripcionConAsignaturaComun(),
                        evento.getInicio().getTime(),
                        evento.getEvento().getCalendario().getId(),
                        evento.getEvento().getAula().getPlazas(),
                        evento.getEvento().getEventosDetalle().stream().map(ed -> ed.getInicio())
                                .sorted().map(f -> formatter.format(f)).collect(Collectors.joining(", ")),
                        evento.getEvento().getTitulo(),
                        evento.getEvento().getComentarios(),
                        evento.getDescripcionConAsignaturaComun(),
                        evento.getEvento().getCalendario().getId(),
                        evento.getEvento().getDiaAsString(),
                        evento.getEvento().getObservaciones(),
                        evento.getEvento().getRepetirCadaSemanas(),
                        evento.getEvento().getDesdeElDia(),
                        evento.getEvento().getHastaElDia(),
                        evento.getEvento().getNumeroIteraciones(),
                        evento.getEvento().hasDetalleManual(),
                        evento.getEvento().getCreditos(),
                        evento.getEvento().getCreditosComputables(),
                        evento.getEvento().getSemestre(),
                        evento.getEvento().getSubgrupoId(),
                        evento.getEvento().getId()
                )).distinct().collect(Collectors.toList()));
        response.getEvents().getRows().addAll(getDiasFestivosL(TipoEstudio.MASTER, rangoFechaInicio, rangoFechaFin, connectedUserId));
        List<Resource> resources = new ArrayList<>();
        resources.add(new Resource(1, "Teoría", "#1a699c", "TE" ));
        resources.add(new Resource(2, "Problemes", "#64b9d9", "PR" ));
        resources.add(new Resource(3, "Laboratoris", "#2e8f0c", "LA" ));
        resources.add(new Resource(4, "Seminaris", "#176413", "SE" ));
        resources.add(new Resource(5, "Tutories", "#1a5173", "TU" ));
        resources.add(new Resource(6, "Avaluació", "#83ad47", "AV" ));
        resources.add(new Resource(100, "Festiu", "#ff0000", "" ));
        response.setResources(resources);

        List<Assignment> assignments = new ArrayList<>();

        Long index = 0L;
        for (Event event : response.getEvents().getRows()){
            assignments.add(new Assignment(index, new Long(event.getId()), event.getResourceId()));
            ++index;
        }

        response.setAssignments(assignments);

        Boolean success = true;
        response.setSuccess(success);
        return response;
    }

    @GET
    @Path("eventos/aula/detalle2")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getEventosDetalleByAula2(@QueryParam(AULA_ID_PARAM) String aulaId,
                                                  @QueryParam(SEMESTRE_ID_QUERY_PARAM) String semestreId,
                                                  @QueryParam(CALENDARIOS_IDS_QUERY_PARAM) String calendariosIds,
                                                  @QueryParam(START_DATE_QUERY_PARAM) String startDate,
                                                  @QueryParam(END_DATE_QUERY_PARAM) String endDate)
            throws ParseException, RegistroNoEncontradoException, UnauthorizedUserException
    {
        try
        {
            ParamUtils.checkNotNull(aulaId, semestreId, startDate, endDate);
        }
        catch (Exception e)
        {
            return new ArrayList<UIEntity>();
        }

        Long connectedUserId = AccessManager.getConnectedUserId(request);

        Date rangoFechaInicio = queryParamDateFormat.parse(startDate);
        Date rangoFechaFin = queryParamDateFormat.parse(endDate);

        // Todos los eventos hasta el final del día
        Calendar c = Calendar.getInstance();
        c.setTime(rangoFechaFin);
        c.set(Calendar.HOUR_OF_DAY, ULTIMA_HORA_DIA);
        c.set(Calendar.MINUTE, ULTIMO_MINUTO_HORA);
        c.set(Calendar.SECOND, ULTIMO_SEGUNDO_MINUTO);
        rangoFechaFin = c.getTime();

        List<Long> calendariosList = ParamParseService.parseAsLongList(calendariosIds);

        List<EventoDetalle> eventosDetalle = new ArrayList<EventoDetalle>();

        if (calendariosList.size() != 0)
        {
            eventosDetalle = eventosService.getEventosDetallePorAula(ParamUtils.parseLong(aulaId),
                    ParamUtils.parseLong(semestreId), calendariosList, rangoFechaInicio,
                    rangoFechaFin, connectedUserId);
        }

        List<UIEntity> events = eventosDetallePorAulaToUI(eventosDetalle);
        events.addAll(getDiasFestivos(TipoEstudio.MASTER, rangoFechaInicio, rangoFechaFin, connectedUserId));

        return events;
    }

    private List<UIEntity> eventosDetallePorAulaToUI(List<EventoDetalle> eventosDetalle)
    {
        List<UIEntity> eventosUI = new ArrayList<UIEntity>();

        for (EventoDetalle eventoDetalle : eventosDetalle)
        {
            UIEntity eventoUI = new UIEntity();
            eventoUI.put("id", eventoDetalle.getId());
            eventoUI.put("title", eventoDetalle.getDescripcionConGrupoYComunes());
            eventoUI.put("cid", eventoDetalle.getEvento().getCalendario().getId());
            eventoUI.put("start", uIEntitydateFormat.format(eventoDetalle.getInicio()));
            eventoUI.put("end", uIEntitydateFormat.format(eventoDetalle.getFin()));

            eventosUI.add(eventoUI);
        }

        return eventosUI;
    }

    @GET
    @Path("eventos/aula/generica2")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getEventosSemanaGenericaByAula2(@QueryParam(AULA_ID_PARAM) String aulaId,
                                                         @QueryParam(SEMESTRE_ID_QUERY_PARAM) String semestreId,
                                                         @QueryParam(CALENDARIOS_IDS_QUERY_PARAM) String calendariosIds)
            throws ParseException, RegistroNoEncontradoException, UnauthorizedUserException
    {
        ParamUtils.checkNotNull(aulaId, semestreId);

        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<Long> calendariosList = ParamParseService.parseAsLongList(calendariosIds);

        List<Evento> eventos = new ArrayList<Evento>();

        if (calendariosList.size() != 0)
        {
            eventos = eventosService.getEventosSemanaGenericaPorAula(ParamUtils.parseLong(aulaId),
                    ParamUtils.parseLong(semestreId), calendariosList, connectedUserId);
        }

        return eventosSemanaGenericaPorAulaToUI(eventos);
    }

    @GET
    @Path("eventos/aula/generica")
    @Produces(MediaType.APPLICATION_JSON)
    public CrudManagerEventResponse getEventosSemanaGenericaByAula(@QueryParam(AULA_ID_PARAM) String aulaId,
                                                         @QueryParam(SEMESTRE_ID_QUERY_PARAM) String semestreId,
                                                         @QueryParam(CALENDARIOS_IDS_QUERY_PARAM) String calendariosIds)
            throws ParseException, RegistroNoEncontradoException, UnauthorizedUserException
    {
        CrudManagerEventResponse response = new CrudManagerEventResponse();
        ParamUtils.checkNotNull(aulaId, semestreId);

        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<Long> calendariosList = ParamParseService.parseAsLongList(calendariosIds);

        List<Evento> eventos = new ArrayList<Evento>();

        if (calendariosList.size() != 0)
        {
            eventos = eventosService.getEventosSemanaGenericaPorAula(ParamUtils.parseLong(aulaId),
                    ParamUtils.parseLong(semestreId), calendariosList, connectedUserId);
        }

        DateFormat formatter = new SimpleDateFormat("dd/MM/yy");

        response.setEvents(eventos.stream().map(
                evento -> new Event(
                        evento.getId(),
                        evento.getDescripcionConGrupoYComunes(),
                        evento.getInicio(),
                        evento.getFin(),
                        evento.getDescripcionParaUnaAsignatura(),
                        evento.getInicio().getTime(),
                        evento.getCalendario().getId(),
                        evento.getAula() != null? evento.getAula().getPlazas(): null,
                        evento.getEventosDetalle().stream().map(ed -> ed.getInicio())
                                .sorted().map(f -> formatter.format(f)).collect(Collectors.joining(", ")),
                        evento.getTitulo(),
                        evento.getComentarios(),
                        null,
                        evento.getCalendario().getId(),
                        evento.getDiaAsString(),
                        evento.getObservaciones(),
                        evento.getRepetirCadaSemanas(),
                        evento.getDesdeElDia(),
                        evento.getHastaElDia(),
                        evento.getNumeroIteraciones(),
                        evento.hasDetalleManual(),
                        evento.getCreditos(),
                        evento.getCreditosComputables(),
                        evento.getSemestre(),
                        evento.getSubgrupoId(),
                        null
                        )).distinct().collect(Collectors.toList()));

        List<Resource> resources = new ArrayList<>();
        resources.add(new Resource(1, "Teoría", "#1a699c", "TE" ));
        resources.add(new Resource(2, "Problemes", "#64b9d9", "PR" ));
        resources.add(new Resource(3, "Laboratoris", "#2e8f0c", "LA" ));
        resources.add(new Resource(4, "Seminaris", "#176413", "SE" ));
        resources.add(new Resource(5, "Tutories", "#1a5173", "TU" ));
        resources.add(new Resource(6, "Avaluació", "#83ad47", "AV" ));
        resources.add(new Resource(100, "Festiu", "#ff0000", "" ));
        response.setResources(resources);

        List<Assignment> assignments = new ArrayList<>();

        Long index = 0L;
        for (Event event : response.getEvents().getRows()){
            assignments.add(new Assignment(index, new Long(event.getId()), event.getResourceId()));
            ++index;
        }

        response.setAssignments(assignments);

        Boolean success = true;
        response.setSuccess(success);
        return response;
        //return eventosSemanaGenericaPorAulaToUI(eventos);
    }

    private List<UIEntity> eventosSemanaGenericaPorAulaToUI(List<Evento> eventos)
    {
        List<UIEntity> eventosUI = new ArrayList<UIEntity>();

        for (Evento evento : eventos)
        {
            UIEntity eventoUI = new UIEntity();
            eventoUI.put("id", evento.getId());
            eventoUI.put("title", evento.getDescripcionConGrupoYComunes());
            eventoUI.put("cid", evento.getCalendario().getId());
            eventoUI.put("start", uIEntitydateFormat.format(evento.getInicio()));
            eventoUI.put("comunes", evento.getTextoAsignaturasComunes());
            eventoUI.put("descripcion", evento.getDescripcionConGrupoYComunes());
            eventoUI.put("aula_id", evento.getAula().getId());
            eventoUI.put("end", uIEntitydateFormat.format(evento.getFin()));
            eventoUI.put("comentarios", evento.getComentarios());

            eventosUI.add(eventoUI);
        }

        return eventosUI;
    }

    @GET
    @Path("eventos/generica/circuito2")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getEventosGenericaCircuitos2(
            @QueryParam(ESTUDIO_ID_QUERY_PARAM) String estudioId,
            @QueryParam(CIRCUITO_ID_QUERY_PARAM) String circuitoId,
            @QueryParam(SEMESTRE_ID_QUERY_PARAM) String semestreId,
            @QueryParam(GRUPO_ID_QUERY_PARAM) String grupoId,
            @QueryParam(CALENDARIOS_IDS_QUERY_PARAM) String calendariosIds)
            throws ParseException, UnauthorizedUserException, RegistroNoEncontradoException
    {

        Long connectedUserId = AccessManager.getConnectedUserId(request);

        ParamUtils.checkNotNull(estudioId, semestreId, grupoId);

        List<Long> calendariosList = ParamParseService.parseAsLongList(calendariosIds);

        Long estudioIdComoLong = ParamUtils.parseLong(estudioId);
        List<Evento> eventos = eventosService.eventosSemanaGenericaCircuito(estudioIdComoLong,
                ParamUtils.parseLong(semestreId), grupoId, ParamUtils.parseLong(circuitoId),
                calendariosList, connectedUserId);

        return eventos.stream().map(evento -> eventoCircuitoToUI(evento, estudioIdComoLong))
                .collect(Collectors.toList());
    }

    @GET
    @Path("eventos/generica/circuito")
    @Produces(MediaType.APPLICATION_JSON)
    public CrudManagerEventResponse getEventosGenericaCircuitos(
            @QueryParam(ESTUDIO_ID_QUERY_PARAM) String estudioId,
            @QueryParam(CIRCUITO_ID_QUERY_PARAM) String circuitoId,
            @QueryParam(SEMESTRE_ID_QUERY_PARAM) String semestreId,
            @QueryParam(GRUPO_ID_QUERY_PARAM) String grupoId,
            @QueryParam(CALENDARIOS_IDS_QUERY_PARAM) String calendariosIds)
            throws ParseException, UnauthorizedUserException, RegistroNoEncontradoException
    {
        CrudManagerEventResponse response = new CrudManagerEventResponse();
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        ParamUtils.checkNotNull(estudioId, semestreId, grupoId);

        List<Long> calendariosList = ParamParseService.parseAsLongList(calendariosIds);

        Long estudioIdComoLong = ParamUtils.parseLong(estudioId);
        List<Evento> eventos = eventosService.eventosSemanaGenericaCircuito(estudioIdComoLong,
                ParamUtils.parseLong(semestreId), grupoId, ParamUtils.parseLong(circuitoId),
                calendariosList, connectedUserId);

        DateFormat formatter = new SimpleDateFormat("dd/MM/yy");

        response.setEvents(eventos.stream().map(
                evento -> new Event(
                        evento.getId(),
                        evento.getDescripcionParaUnEstudio(Long.valueOf(estudioId)),
                        evento.getInicio(),
                        evento.getFin(),
                        evento.getDescripcionParaUnaAsignatura(),
                        evento.getInicio().getTime(),
                        evento.getCalendarioCircuito().getId(),
                        evento.getAula() != null? evento.getAula().getPlazas(): null,
                        evento.getEventosDetalle().stream().map(ed -> ed.getInicio())
                                .sorted().map(f -> formatter.format(f)).collect(Collectors.joining(", ")),
                        evento.getAsignaturaDelEstudio(Long.valueOf(estudioId)).getNombre(),
                        evento.getComentarios(),
                        evento.getAsignaturasComunes(Long.valueOf(estudioId)),
                        evento.getCalendarioCircuito().getId(),
                        evento.getDiaAsString(),
                        evento.getObservaciones(),
                        evento.getRepetirCadaSemanas(),
                        evento.getDesdeElDia(),
                        evento.getHastaElDia(),
                        evento.getNumeroIteraciones(),
                        evento.hasDetalleManual(),
                        evento.getCreditos(),
                        evento.getCreditosComputables(),
                        evento.getSemestre(),
                        evento.getSubgrupoId(),
                        null
                )).distinct().collect(Collectors.toList()));
        List<Resource> resources = new ArrayList<>();
        resources.add(new Resource(1, "EnCircuit", "#1a699c", "TE" ));
        resources.add(new Resource(2, "ForaCircuit", "#64b9d9", "PR" ));
        response.setResources(resources);

        List<Assignment> assignments = new ArrayList<>();

        Long index = 0L;
        for (Event event : response.getEvents().getRows()){
            assignments.add(new Assignment(index, event.getId(), event.getResourceId()));
            ++index;
        }

        response.setAssignments(assignments);

        Boolean success = true;
        response.setSuccess(success);
        return response;
    }

    @GET
    @Path("eventos/noplanificados/circuito")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getEventosGenericaNoPlanificadosCircuitos(
            @QueryParam(ESTUDIO_ID_QUERY_PARAM) Long estudioId,
            @QueryParam(CIRCUITO_ID_QUERY_PARAM) Long circuitoId,
            @QueryParam(SEMESTRE_ID_QUERY_PARAM) Long semestreId,
            @QueryParam(GRUPO_ID_QUERY_PARAM) String grupoId,
            @QueryParam(CALENDARIOS_IDS_QUERY_PARAM) String calendariosIds)
            throws ParseException, UnauthorizedUserException, RegistroNoEncontradoException
    {

        Long connectedUserId = AccessManager.getConnectedUserId(request);

        ParamUtils.checkNotNull(estudioId, semestreId, grupoId);

        List<Long> calendariosList = ParamParseService.parseAsLongList(calendariosIds);

        List<Evento> eventos = eventosService.eventosNoPlanificadosCircuito(estudioId, semestreId,
                grupoId, circuitoId, calendariosList, connectedUserId);

        return eventos.stream().map(evento -> eventoCircuitoToUI(evento, estudioId))
                .collect(Collectors.toList());
    }

    private UIEntity eventoCircuitoToUI(Evento evento, Long estudioId)
    {
        UIEntity eventoUI = new UIEntity();

        eventoUI.put("id", evento.getId());
        eventoUI.put("title", evento.getDescripcionParaUnEstudio(estudioId));
        eventoUI.put("cid", evento.getCalendarioCircuito().getId());
        if (evento.getInicio() != null)
        {
            eventoUI.put("start", uIEntitydateFormat.format(evento.getInicio()));
        }
        if (evento.getFin() != null)
        {
            eventoUI.put("end", uIEntitydateFormat.format(evento.getFin()));
        }
        eventoUI.put("nombreAsignatura", evento.getAsignaturaDelEstudio(estudioId).getNombre());
        eventoUI.put("comentarios", evento.getComentarios());
        return eventoUI;
    }

    @PUT
    @Path("eventos/generica/circuito/{id}")
    public Response updateEventoConCircuito(@PathParam("id") Long eventoId,
                                            @QueryParam(CIRCUITO_ID_QUERY_PARAM) Long circuitoId,
                                            @QueryParam(ESTUDIO_ID_QUERY_PARAM) Long estudioId,
                                            @QueryParam(TIPO_ACCION_PARAM) String tipoAccion)
            throws RegistroNoEncontradoException,
            UnauthorizedUserException, NumberFormatException, GeneralHORException
    {
        ParamUtils.checkNotNull(circuitoId, estudioId, tipoAccion);

        Long connectedUserId = AccessManager.getConnectedUserId(request);

        eventosService.gestionaEventoConCircuito(eventoId, circuitoId, estudioId,
                Integer.parseInt(tipoAccion), connectedUserId);

        return Response.ok().build();
    }

    @GET
    @Path("eventos/detalle/circuito2")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getEventosDetalleCircuito2(
            @QueryParam(ESTUDIO_ID_QUERY_PARAM) String estudioId,
            @QueryParam(CIRCUITO_ID_QUERY_PARAM) String circuitoId,
            @QueryParam(SEMESTRE_ID_QUERY_PARAM) String semestreId,
            @QueryParam(GRUPO_ID_QUERY_PARAM) String grupoId,
            @QueryParam(CALENDARIOS_IDS_QUERY_PARAM) String calendariosIds,
            @QueryParam(START_DATE_QUERY_PARAM) String startDate,
            @QueryParam(END_DATE_QUERY_PARAM) String endDate)
            throws ParseException, UnauthorizedUserException, RegistroNoEncontradoException
    {

        Long connectedUserId = AccessManager.getConnectedUserId(request);

        try
        {
            ParamUtils.checkNotNull(estudioId, semestreId, grupoId, startDate, endDate);
        }
        catch (Exception e)
        {
            return new ArrayList<UIEntity>();
        }

        Date rangoFechaInicio = queryParamDateFormat.parse(startDate);
        Date rangoFechaFin = queryParamDateFormat.parse(endDate);

        // Todos los eventos hasta el final del día
        Calendar c = Calendar.getInstance();
        c.setTime(rangoFechaFin);
        c.set(Calendar.HOUR_OF_DAY, ULTIMA_HORA_DIA);
        c.set(Calendar.MINUTE, ULTIMO_MINUTO_HORA);
        c.set(Calendar.SECOND, ULTIMO_SEGUNDO_MINUTO);
        rangoFechaFin = c.getTime();

        List<Long> calendariosList = ParamParseService.parseAsLongList(calendariosIds);

        List<EventoDetalle> eventosDetalle = new ArrayList<EventoDetalle>();

        Long estudioIdLong = ParamUtils.parseLong(estudioId);

        if (calendariosList.size() != 0)
        {
            eventosDetalle = eventosService.eventosSemanaDetalleCircuito(estudioIdLong,
                    ParamUtils.parseLong(semestreId), grupoId, ParamUtils.parseLong(circuitoId),
                    calendariosList, rangoFechaInicio, rangoFechaFin, connectedUserId);
        }

        List<UIEntity> listaResultado = new ArrayList<UIEntity>();

        for (EventoDetalle eventoDetalle : eventosDetalle)
        {
            listaResultado.add(eventoDetalleCircuitoToUI(eventoDetalle, estudioIdLong));
        }

        listaResultado.addAll(getDiasFestivos(TipoEstudio.getTipoEstudioId(estudioIdLong), rangoFechaInicio, rangoFechaFin, connectedUserId));

        return listaResultado;
    }

    @GET
    @Path("eventos/detalle/circuito")
    @Produces(MediaType.APPLICATION_JSON)
    public CrudManagerEventResponse getEventosDetalleCircuito(
            @QueryParam(ESTUDIO_ID_QUERY_PARAM) String estudioId,
            @QueryParam(CIRCUITO_ID_QUERY_PARAM) String circuitoId,
            @QueryParam(SEMESTRE_ID_QUERY_PARAM) String semestreId,
            @QueryParam(GRUPO_ID_QUERY_PARAM) String grupoId,
            @QueryParam(CALENDARIOS_IDS_QUERY_PARAM) String calendariosIds,
            @QueryParam(START_DATE_QUERY_PARAM) String startDate,
            @QueryParam(END_DATE_QUERY_PARAM) String endDate)
            throws ParseException, UnauthorizedUserException, RegistroNoEncontradoException
    {
        CrudManagerEventResponse response = new CrudManagerEventResponse();
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        try
        {
            ParamUtils.checkNotNull(estudioId, semestreId, grupoId, startDate, endDate);
        }
        catch (Exception e)
        {
            return response;
        }

        Date rangoFechaInicio = new Date(startDate);
        Date rangoFechaFin = new Date(endDate);

        // Todos los eventos hasta el final del día
        Calendar c = Calendar.getInstance();
        c.setTime(rangoFechaFin);
        c.set(Calendar.HOUR_OF_DAY, ULTIMA_HORA_DIA);
        c.set(Calendar.MINUTE, ULTIMO_MINUTO_HORA);
        c.set(Calendar.SECOND, ULTIMO_SEGUNDO_MINUTO);
        rangoFechaFin = c.getTime();

        List<Long> calendariosList = ParamParseService.parseAsLongList(calendariosIds);

        List<EventoDetalle> eventosDetalle = new ArrayList<EventoDetalle>();

        Long estudioIdLong = ParamUtils.parseLong(estudioId);

        if (calendariosList.size() != 0)
        {
            eventosDetalle = eventosService.eventosSemanaDetalleCircuito(estudioIdLong,
                    ParamUtils.parseLong(semestreId), grupoId, ParamUtils.parseLong(circuitoId),
                    calendariosList, rangoFechaInicio, rangoFechaFin, connectedUserId);
        }

        List<UIEntity> listaResultado = new ArrayList<UIEntity>();

        for (EventoDetalle eventoDetalle : eventosDetalle)
        {
            listaResultado.add(eventoDetalleCircuitoToUI(eventoDetalle, estudioIdLong));
        }

        DateFormat formatter = new SimpleDateFormat("dd/MM/yy");

        response.setEvents(eventosDetalle.stream().map(
                evento -> new Event(
                        evento.getId(),
                        evento.getDescripcionParaUnEstudio(Long.valueOf(estudioId)),
                        evento.getInicio(),
                        evento.getFin(),
                        evento.getEvento().getDescripcionParaUnaAsignatura(),
                        evento.getInicio().getTime(),
                        evento.getEvento().getCalendarioCircuito().getId(),
                        evento.getEvento().getAula() != null? evento.getEvento().getAula().getPlazas(): null,
                        evento.getEvento().getEventosDetalle().stream().map(ed -> ed.getInicio())
                                .sorted().map(f -> formatter.format(f)).collect(Collectors.joining(", ")),
                        evento.getEvento().getAsignaturaDelEstudio(Long.valueOf(estudioId)).getNombre(),
                        evento.getEvento().getComentarios(),
                        evento.getEvento().getAsignaturasComunes(Long.valueOf(estudioId)),
                        evento.getEvento().getCalendarioCircuito().getId(),
                        evento.getEvento().getDiaAsString(),
                        evento.getEvento().getObservaciones(),
                        evento.getEvento().getRepetirCadaSemanas(),
                        evento.getEvento().getDesdeElDia(),
                        evento.getEvento().getHastaElDia(),
                        evento.getEvento().getNumeroIteraciones(),
                        evento.getEvento().hasDetalleManual(),
                        evento.getEvento().getCreditos(),
                        evento.getEvento().getCreditosComputables(),
                        evento.getEvento().getSemestre(),
                        evento.getEvento().getSubgrupoId(),
                        evento.getEvento().getId()
                )).distinct().collect(Collectors.toList()));
        response.getEvents().getRows().addAll(getDiasFestivosL(TipoEstudio.getTipoEstudioId(estudioIdLong), rangoFechaInicio, rangoFechaFin, connectedUserId));
        List<Resource> resources = new ArrayList<>();
        resources.add(new Resource(1, "EnCircuit", "#1a699c", "TE" ));
        resources.add(new Resource(2, "ForaCircuit", "#64b9d9", "PR" ));
        resources.add(new Resource(100, "Festiu", "#ff0000", "" ));
        response.setResources(resources);

        List<Assignment> assignments = new ArrayList<>();

        Long index = 0L;
        for (Event event : response.getEvents().getRows()){
            assignments.add(new Assignment(index, event.getId(), event.getResourceId()));
            ++index;
        }

        response.setAssignments(assignments);

        Boolean success = true;
        response.setSuccess(success);

        return response;
    }

    @GET
    @Path("eventos/mismogrupo/{eventoId}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getEventosMismoGrupoConDetalleProfesor(
            @PathParam("eventoId") Long eventoId, @QueryParam("profesorId") Long profesorId)
            throws RegistroNoEncontradoException, UnauthorizedUserException, NumberFormatException,
            GeneralHORException
    {
        ParamUtils.checkNotNull(eventoId, profesorId);

        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<Evento> eventos = eventosService.getEventosMismoGrupoConDetalleProfesor(eventoId,
                profesorId, connectedUserId);
        return toUI(eventos);
    }

    @GET
    @Path("eventos/fechas/profesor/{profesorId}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getFechasEventoByProfesor(@PathParam("profesorId") Long profesorId,
                                                    @QueryParam("eventoId") Long eventoId)
            throws RegistroNoEncontradoException, UnauthorizedUserException
    {
        ParamUtils.checkNotNull(eventoId);
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<EventoDocencia> fechasEvento = eventosService.getFechasEventoByProfesor(profesorId,
                eventoId, connectedUserId);

        List<EventoDocencia> fechasNoLectivasEvento = eventosService
                .getDiasFestivosDeUnEventoByEventoId(eventoId, connectedUserId);

        fechasEvento.addAll(fechasNoLectivasEvento);
        Collections.sort(fechasEvento, (ed1, ed2) -> ed1.getFecha().compareTo(ed2.getFecha()));

        List<UIEntity> fechasUI = new ArrayList<>();
        for (EventoDocencia eventoDocencia : fechasEvento)
        {
            UIEntity uiEntity = new UIEntity();
            uiEntity.put("detalleId", eventoDocencia.getEventoId());
            uiEntity.put("fecha", eventoDocencia.getFecha());
            uiEntity.put("tipoDia", eventoDocencia.getTipoDia());
            uiEntity.put("seleccionado", eventoDocencia.getDocencia());
            fechasUI.add(uiEntity);
        }
        return fechasUI;
    }

    private UIEntity eventoDetalleCircuitoToUI(EventoDetalle eventoDetalle, Long estudioId)
    {
        UIEntity eventoUI = new UIEntity();
        eventoUI.put("id", eventoDetalle.getId());
        eventoUI.put("cid", eventoDetalle.getEvento().getCalendarioCircuito().getId());
        eventoUI.put("title", eventoDetalle.getEvento().getDescripcionParaUnEstudio(estudioId));
        eventoUI.put("nombreAsignatura",
                eventoDetalle.getEvento().getAsignaturaDelEstudio(estudioId).getNombre());
        eventoUI.put("comentarios", eventoDetalle.getEvento().getComentarios());

        if (eventoDetalle.getInicio() != null)
        {
            eventoUI.put("start", uIEntitydateFormat.format(eventoDetalle.getInicio()));
        }

        if (eventoDetalle.getFin() != null)
        {
            eventoUI.put("end", uIEntitydateFormat.format(eventoDetalle.getFin()));
        }
        return eventoUI;
    }

    private List<UIEntity> getDiasFestivos(String tipoEstudioId, Date startDate, Date endDate, Long connectedUserId)
            throws ParseException
    {
        List<CalendarioAcademico> festivos = calendarioAcademicoService
                .getCalendarioAcademicoNoLectivos(tipoEstudioId, startDate, endDate, connectedUserId);
        List<UIEntity> eventosFestivos = new ArrayList<UIEntity>();

        for (CalendarioAcademico festivo : festivos)
        {
            String fecha = uIEntitydateFormat.format(festivo.getFecha());

            UIEntity entity = new UIEntity();
            entity.put("id", festivo.getId());
            entity.put("profesores", "");
            entity.put("plazasAula", "");
            entity.put("cid", TipoCalendario.F.getCalendarioId());
            entity.put("title", "Festiu / No Lectiu");
            entity.put("start", fecha.replaceAll("T00:00:00","T08:00:00"));
            entity.put("end", fecha.replaceAll("T00:00:00","T22:00:00"));
            entity.put("AllDay", true);
            eventosFestivos.add(entity);
        }
        return eventosFestivos;
    }

    private List<Event> getDiasFestivosL(String tipoEstudioId, Date startDate, Date endDate, Long connectedUserId)
            throws ParseException
    {
        List<CalendarioAcademico> festivos = calendarioAcademicoService
                .getCalendarioAcademicoNoLectivos(tipoEstudioId, startDate, endDate, connectedUserId);
        List<Event> eventosFestivos = new ArrayList<>();

        for (CalendarioAcademico festivo : festivos)
        {
            Date fecha = festivo.getFecha();

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(fecha);

            calendar.set(Calendar.HOUR_OF_DAY, 8);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            Date fechaIni = calendar.getTime();

            calendar.set(Calendar.HOUR_OF_DAY, 22);
            Date fechaFin = calendar.getTime();
            eventosFestivos.add(new Event(festivo.getId(),
                    "", 0L, TipoCalendario.F.getCalendarioId(),
                    "Festiu / No Lectiu", fechaIni,
                    fechaFin, true, 8L));
        }
        return eventosFestivos;
    }

    private UIEntity getCalendarioFestivos()
    {
        UIEntity calendar = new UIEntity();
        calendar.put("id", TipoCalendario.F.getCalendarioId());
        calendar.put("title", TipoCalendario.F.getNombre());
        calendar.put("color", TipoCalendario.F.getColor());

        return calendar;
    }
}
