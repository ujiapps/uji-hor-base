package es.uji.apps.hor.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.hor.AulaYaAsignadaAEstudioException;
import es.uji.apps.hor.model.Aula;
import es.uji.apps.hor.model.AulaPlanificacion;
import es.uji.apps.hor.model.TipoAula;
import es.uji.apps.hor.services.AulaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Path("aula")
public class AulaResource extends CoreBaseService
{
    @InjectParam
    private AulaService consultaAulas;

    @GET
    @Path("estudio/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getAulasAsignadasToEstudio(@PathParam("id") Long estudioId,
            @QueryParam("semestreId") Long semestreId) throws NumberFormatException,
            UnauthorizedUserException, RegistroNoEncontradoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<AulaPlanificacion> aulasAsignadas = consultaAulas.getAulasAsignadasToEstudioYSemestre(
                estudioId, semestreId, connectedUserId);

        List<UIEntity> listaAulas = new ArrayList<UIEntity>();

        for (AulaPlanificacion aulaPlanificacion : aulasAsignadas)
        {
            listaAulas.add(aulaPlanificacionToUI(aulaPlanificacion));
        }

        return listaAulas;
    }

    @GET
    @Path("estudio/{id}/ocupacion")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getAulasAsignadasToEstudioConOcupacion(@PathParam("id") Long estudioId,
            @QueryParam("semestreId") Long semestreId, @QueryParam("eventoId") Long eventoId)
            throws NumberFormatException, UnauthorizedUserException, RegistroNoEncontradoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<AulaPlanificacion> aulasAsignadas = consultaAulas
                .getAulasAsignadasToEstudioYSemestreConOcupacion(estudioId, semestreId, eventoId,
                        connectedUserId);

        List<UIEntity> listaAulas = new ArrayList<UIEntity>();

        for (AulaPlanificacion aulaPlanificacion : aulasAsignadas)
        {
            listaAulas.add(aulaPlanificacionToUI(aulaPlanificacion));
        }

        return listaAulas;
    }

    @GET
    @Path("estudio/{id}/todas")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getAulasAsignadasToEstudioYSemestre(@PathParam("id") Long estudioId)
            throws NumberFormatException, UnauthorizedUserException, RegistroNoEncontradoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<Aula> aulasAsignadas = consultaAulas.getAulasAsignadasToEstudio(estudioId,
                connectedUserId);

        List<UIEntity> listaAulas = new ArrayList<UIEntity>();

        for (Aula aula : aulasAsignadas)
        {
            listaAulas.add(UIEntity.toUI(aula));
        }

        return listaAulas;
    }

    @GET
    @Path("disponibles/evento")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getAulasAsignadas(@QueryParam("eventoId") Long eventoId,
            @QueryParam("semestreId") Long semestreId, @QueryParam("centroId") Long centroId)
            throws NumberFormatException, UnauthorizedUserException, RegistroNoEncontradoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        ParamUtils.checkNotNull(eventoId, semestreId, centroId);

        List<Aula> aulasAsignadas = consultaAulas.getTodasAulasPorEventoCentroYSemestre(eventoId,
                centroId, semestreId, connectedUserId);

        List<UIEntity> listaAulas = new ArrayList<>();

        for (Aula aula : aulasAsignadas)
        {
            listaAulas.add(UIEntity.toUI(aula));
        }

        return listaAulas;
    }

    private UIEntity aulaPlanificacionToUI(AulaPlanificacion aulaPlanificacion)
    {
        UIEntity entity = UIEntity.toUI(aulaPlanificacion);
        entity.put("nombre", aulaPlanificacion.getAula().getNombre());
        entity.put("edificio", aulaPlanificacion.getAula().getEdificio().getNombre());
        entity.put("tipo", aulaPlanificacion.getAula().getTipo().getNombre());
        entity.put("planta", aulaPlanificacion.getAula().getPlanta().getNombre());
        entity.put("plazas", aulaPlanificacion.getAula().getPlazas());
        entity.put("semestreId", aulaPlanificacion.getSemestre().getNombre());
        entity.put("ocupado", aulaPlanificacion.getAula().getOcupado());
        return entity;
    }

    @POST
    @Path("estudio")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> asignaAulaToEstudio(UIEntity entity)
            throws RegistroNoEncontradoException, AulaYaAsignadaAEstudioException,
            NumberFormatException, UnauthorizedUserException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        String estudioId = entity.get("estudioId");
        Long semestreId = ParamUtils.parseLong(entity.get("semestreId"));
        String aulaId = entity.get("aulaId");

        AulaPlanificacion aulaPlanificacion = consultaAulas.asignaAulaToEstudio(
                Long.parseLong(estudioId), Long.parseLong(aulaId), semestreId, connectedUserId);

        return Collections.singletonList(aulaPlanificacionToUI(aulaPlanificacion));
    }

    @DELETE
    @Path("estudio/{id}")
    public void deleteAulaAsignadaToEstudio(@PathParam("id") String aulaPlanificacionId)
            throws RegistroConHijosException, RegistroNoEncontradoException, NumberFormatException,
            UnauthorizedUserException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        ParamUtils.checkNotNull(aulaPlanificacionId);
        consultaAulas.deleteAulaAsignadaToEstudio(Long.parseLong(aulaPlanificacionId),
                connectedUserId);
    }

    @GET
    @Path("tipo")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTiposAulaByCentroAndSemestreAndEdificio(
            @QueryParam("centroId") String centroId, @QueryParam("semestreId") String semestreId,
            @QueryParam("edificio") String edificio) throws RegistroNoEncontradoException,
            UnauthorizedUserException
    {
        ParamUtils.checkNotNull(centroId, semestreId, edificio);

        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<TipoAula> tiposAula = consultaAulas.getTiposAulaByCentroAndSemestreAndEdificio(
                ParamUtils.parseLong(centroId), ParamUtils.parseLong(semestreId), edificio,
                connectedUserId);

        return UIEntity.toUI(tiposAula);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getAulasFiltradasPor(@QueryParam("centroId") String centroId,
            @QueryParam("semestreId") String semestreId, @QueryParam("edificio") String edificio,
            @QueryParam("tipoAula") String tipoAula, @QueryParam("planta") String planta)
            throws RegistroNoEncontradoException, UnauthorizedUserException
    {
        ParamUtils.checkNotNull(centroId, semestreId, edificio);

        Long connectedUserId = AccessManager.getConnectedUserId(request);

        tipoAula = (tipoAula == null || tipoAula.equals("")) ? null : tipoAula;
        planta = (planta == null || planta.equals("")) ? null : planta;

        List<Aula> aulas = consultaAulas.getAulasFiltradasPor(ParamUtils.parseLong(centroId),
                ParamUtils.parseLong(semestreId), edificio, tipoAula, planta, connectedUserId);

        return UIEntity.toUI(aulas);
    }
}