package es.uji.apps.hor.services;

import es.uji.apps.hor.AulaYaAsignadaAEstudioException;
import es.uji.apps.hor.dao.AulaDAO;
import es.uji.apps.hor.dao.EstudiosDAO;
import es.uji.apps.hor.dao.EventosDAO;
import es.uji.apps.hor.dao.PersonaDAO;
import es.uji.apps.hor.model.*;
import es.uji.commons.rest.Role;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AulaService
{
    @Autowired
    private PersonaDAO personaDAO;

    @Autowired
    private AulaDAO aulaDAO;

    @Autowired
    private EstudiosDAO estudiosDAO;

    @Autowired
    private EventosDAO eventosDAO;

    @Role({"ADMIN", "USUARIO"})
    public List<AulaPlanificacion> getAulasAsignadasToEstudioYSemestre(Long estudioId,
                                                                       Long semestreId, Long connectedUserId)
            throws UnauthorizedUserException,
            RegistroNoEncontradoException
    {
        Persona persona = personaDAO.getPersonaConTitulacionesYCentrosById(connectedUserId);
        if (!personaDAO.esAdmin(connectedUserId) && !persona.esGestorDeCentro())
        {
            persona.compruebaAccesoAEstudio(estudioId);
        }

        return aulaDAO.getAulasAsignadasToEstudioYSemestre(estudioId, semestreId);
    }

    @Role({"ADMIN", "USUARIO"})
    public List<AulaPlanificacion> getAulasAsignadasToEstudioYSemestreConOcupacion(Long estudioId,
                                                                                   Long semestreId, Long eventoId, Long connectedUserId)
            throws UnauthorizedUserException,
            RegistroNoEncontradoException
    {
        Persona persona = personaDAO.getPersonaConTitulacionesYCentrosById(connectedUserId);
        if (!personaDAO.esAdmin(connectedUserId) && !persona.esGestorDeCentro())
        {
            persona.compruebaAccesoAEstudio(estudioId);
        }

        Evento evento = eventosDAO.getEventoById(eventoId);

        List<AulaPlanificacion> aulas = aulaDAO.getAulasAsignadasToEstudioYSemestreConItems(estudioId, semestreId);
        for (AulaPlanificacion aulaPlanificacion : aulas)
        {
            String estadoOcupacion = aulaOcupadaEnFecha(aulaPlanificacion.getAula(), evento) ? "S"
                    : "N";
            aulaPlanificacion.getAula().setOcupado(estadoOcupacion);
        }
        return aulas;
    }

    @Role({"ADMIN", "USUARIO"})
    public List<Aula> getAulasAsignadasToEstudio(Long estudioId, Long connectedUserId)
            throws UnauthorizedUserException, RegistroNoEncontradoException
    {
        Persona persona = personaDAO.getPersonaConTitulacionesYCentrosById(connectedUserId);
        if (!personaDAO.esAdmin(connectedUserId) && !persona.esGestorDeCentro())
        {
            persona.compruebaAccesoAEstudio(estudioId);
        }

        return aulaDAO.getAulasAsignadasToEstudio(estudioId);
    }

    @Role({"ADMIN", "USUARIO"})
    public AulaPlanificacion getAulaPlanificacionById(Long aulaPlanificacionId, Long connectedUserId)
            throws RegistroNoEncontradoException
    {
        return aulaDAO.getAulaPlanificacionById(aulaPlanificacionId);
    }

    @Role({"ADMIN", "USUARIO"})
    public AulaPlanificacion asignaAulaToEstudio(Long estudioId, Long aulaId, Long semestreId,
                                                 Long connectedUserId) throws RegistroNoEncontradoException,
            AulaYaAsignadaAEstudioException, UnauthorizedUserException
    {
        Persona persona = personaDAO.getPersonaConTitulacionesYCentrosById(connectedUserId);
        if (personaDAO.esAdmin(connectedUserId) || persona.esGestorDeCentro())
        {
            AulaPlanificacion nuevaPlanificacion = aulaDAO.asignaAulaToEstudio(estudioId, aulaId,
                    semestreId);

            List<Estudio> listaEstudiosCompartidos = aulaDAO
                    .getEstudiosComunesByEstudioId(estudioId);

            for (Estudio estudio : listaEstudiosCompartidos)
            {
                aulaDAO.asignaAulaToEstudio(estudio.getId(), aulaId, semestreId);
            }
            return nuevaPlanificacion;
        }
        else
        {
            throw new UnauthorizedUserException();
        }

    }

    @Role({"ADMIN", "USUARIO"})
    public void deleteAulaAsignadaToEstudio(Long aulaPlanificacionId, Long connectedUserId)
            throws RegistroConHijosException, UnauthorizedUserException,
            RegistroNoEncontradoException
    {
        AulaPlanificacion aulaPlanificacion = aulaDAO.getAulaPlanificacionById(aulaPlanificacionId);

        if (!personaDAO.esAdmin(connectedUserId))
        {
            Persona persona = personaDAO.getPersonaConTitulacionesYCentrosById(connectedUserId);

            Centro centro = aulaPlanificacion.getAula().getCentro();

            if (!persona.esGestorDeCentro(centro.getId()))
            {
                throw new UnauthorizedUserException();
            }
        }

        Aula aula = aulaDAO.getAulaConEventosById(aulaPlanificacion.getAula().getId());
        List<Estudio> listaEstudiosCompartidos = aulaDAO
                .getEstudiosComunesByEstudioId(aulaPlanificacion.getEstudio().getId());

        if (sePuedeDesplanificarAulaDeTodosLosEstudios(aula, aulaPlanificacion.getEstudio(),
                listaEstudiosCompartidos))
        {
            aulaDAO.deleteAulaAsignadaToEstudio(aulaPlanificacionId);
            for (Estudio estudio : listaEstudiosCompartidos)
            {
                AulaPlanificacion aulaPlanificacionEstudioCompartido = aulaDAO
                        .getAulaPlanificacionByAulaEstudioSemestre(aulaPlanificacion.getAula()
                                .getId(), estudio.getId(), aulaPlanificacion.getSemestre()
                                .getSemestre());
                aulaDAO.deleteAulaAsignadaToEstudio(aulaPlanificacionEstudioCompartido.getId());
            }
        }
        else
        {
            throw new RegistroConHijosException();
        }
    }

    private Boolean sePuedeDesplanificarAulaDeTodosLosEstudios(Aula aula, Estudio estudio,
                                                               List<Estudio> listaEstudiosCompartidos)
    {

        Boolean sePuedeDesplanificar = true;
        if (!aula.sePuedeDesplanificar(estudio.getId()))
        {
            sePuedeDesplanificar = false;
        }

        for (Estudio estudioCompartido : listaEstudiosCompartidos)
        {
            if (!aula.sePuedeDesplanificar(estudioCompartido.getId()))
            {
                sePuedeDesplanificar = false;
            }
        }

        return sePuedeDesplanificar;
    }

    @Role({"ADMIN", "USUARIO"})
    public List<TipoAula> getTiposAulaByCentroAndSemestreAndEdificio(Long centroId,
                                                                     Long semestreId, String edificio, Long connectedUserId)
    {
        if (!personaDAO.esAdmin(connectedUserId))
        {
            return aulaDAO.getTiposAulaVisiblesPorUsuarioByCentroAndSemestreAndEdificio(centroId,
                    semestreId, edificio, connectedUserId);
        }

        return aulaDAO.getTiposAulaByCentroAndEdificio(centroId, edificio);
    }

    @Role({"ADMIN", "USUARIO"})
    public List<Aula> getAulasFiltradasPor(Long centroId, Long semestreId, String edificio,
                                           String tipoAula, String planta, Long connectedUserId)
            throws RegistroNoEncontradoException, UnauthorizedUserException
    {
        if (!personaDAO.esAdmin(connectedUserId))
        {
            return aulaDAO.getAulasVisiblesPorUsuarioFiltradasPor(centroId, semestreId, edificio,
                    tipoAula, planta, connectedUserId);
        }

        return aulaDAO.getAulasFiltradasPor(centroId, semestreId, edificio, tipoAula, planta);
    }

    @Role({"ADMIN", "USUARIO"})
    public List<Aula> getTodasAulasPorEventoCentroYSemestre(Long eventoId, Long centroId,
                                                            Long semestreId, Long connectedUserId)
            throws RegistroNoEncontradoException
    {
        List<Long> estudioIds = new ArrayList<>();
        for (Estudio estudio : estudiosDAO.getEstudiosByEvento(eventoId))
        {
            estudioIds.add(estudio.getId());
        }

        Evento evento = eventosDAO.getEventoById(eventoId);

        List<Aula> aulasTodas = aulaDAO.getAulasConItemsPlanificadosPorCentroEstudiosYSemestre(
                centroId, estudioIds, semestreId);

        for (Aula aula : aulasTodas)
        {
            Boolean ocupado = aulaOcupadaEnFecha(aula, evento);
            if (ocupado)
            {
                aula.setOcupado("S");
            }
            else
            {
                aula.setOcupado("N");
            }
        }

        return aulasTodas;
    }


    private Boolean aulaOcupadaEnFecha(Aula aula, Evento evento)
    {
        List<Evento> eventosMismoDia = aula.getEventosPlanificados().stream().filter(e -> e.getSemestre().equals(evento.getSemestre()) && e.getDia().equals(evento.getDia())).collect(Collectors.toList());
        for (Evento eventoPlanificado : eventosMismoDia)
        {
            if (evento.getFin().after(eventoPlanificado.getInicio())
                    && evento.getInicio().before(eventoPlanificado.getFin()))
            {
                return true;
            }
        }
        return false;
    }
}