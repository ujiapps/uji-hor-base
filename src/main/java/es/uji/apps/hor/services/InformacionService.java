package es.uji.apps.hor.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.hor.dao.InformacionDAO;
import es.uji.apps.hor.model.Informacion;
import es.uji.commons.rest.Role;

@Service
public class InformacionService
{
    private InformacionDAO informacionDAO;

    @Autowired
    public InformacionService(InformacionDAO informacionDAO)
    {
        this.informacionDAO = informacionDAO;
    }

    @Role({ "ADMIN", "USUARIO" })
    public List<Informacion> getInformacion(Long connectedUserId)
    {
        return informacionDAO.getInformacion(connectedUserId);
    }
}
