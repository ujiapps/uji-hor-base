package es.uji.apps.hor.services;

import java.util.List;

import es.uji.apps.hor.dao.IdiomaDAO;
import es.uji.apps.hor.dao.ProfesorDAO;
import es.uji.commons.rest.UIEntity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.hor.model.*;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

@Service
public class IdiomaService
{
    @Autowired
    private IdiomaDAO idiomaDAO;

    @Autowired
    private EventosService eventosService;

    public List<Idioma> getIdiomas()
    {
        return idiomaDAO.getIdiomas();
    }

    public List<Idioma> getIdiomasProfesorItem(Long profesorId, Long itemId)
    {
        List<Idioma> idiomasTodos = idiomaDAO.getIdiomas();
        List<Idioma> idiomasActivos = idiomaDAO.getIdiomasProfesorItem(profesorId, itemId);

        for (Idioma idiomaActivo : idiomasActivos)
        {
            Idioma idioma = idiomasTodos.stream()
                    .filter(i -> i.getId().equals(idiomaActivo.getId())).findFirst().get();
            idioma.setActivo(true);
        }
        return idiomasTodos;
    }

    public void updateIdiomasItemProfesor(Long profesorId, Long itemId,
                                          List<Long> idiomaIdsActivos, Long connectedUserId)
            throws RegistroNoEncontradoException
    {
        List<Evento> itemsMismoSubgrupo = eventosService.getEventosMismoGrupoConDetalleProfesor(itemId, profesorId, connectedUserId);
        if (itemsMismoSubgrupo.isEmpty())
        {
            idiomaDAO.actualizaIdiomas(profesorId, itemId, idiomaIdsActivos);
        }
        for (Evento evento : itemsMismoSubgrupo)
        {
            idiomaDAO.actualizaIdiomas(profesorId, evento.getId(), idiomaIdsActivos);
        }
    }
}
