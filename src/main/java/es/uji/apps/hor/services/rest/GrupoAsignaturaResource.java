package es.uji.apps.hor.services.rest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.hor.model.Evento;
import es.uji.apps.hor.model.GrupoAsignatura;
import es.uji.apps.hor.services.*;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;

@Path("grupoAsignatura")
public class GrupoAsignaturaResource extends CoreBaseService
{
    @InjectParam
    private GruposAsignaturasService gruposAsignaturasService;

    @InjectParam
    private LogService logService;

    @InjectParam
    private EventosService eventosService;

    @GET
    @Path("sinAsignar")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getGruposAsignaturasSinAsignar(@QueryParam("estudioId") Long estudioId,
            @QueryParam("agrupacionId") Long agrupacionId, @QueryParam("cursoId") Long cursoId,
            @QueryParam("semestreId") Long semestreId, @QueryParam("gruposId") String gruposIds,
            @QueryParam("calendariosIds") String calendariosIds,
            @QueryParam("asignaturasIds") String asignaturasId) throws UnauthorizedUserException,
            RegistroNoEncontradoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        ParamUtils.checkNotNull(estudioId, semestreId, gruposIds);

        List<Long> calendariosList = ParamParseService.parseAsLongList(calendariosIds);
        List<String> gruposList = ParamParseService.parseAsStringList(gruposIds);
        List<String> asignaturasList = ParamParseService.parseAsStringList(asignaturasId);

        if (calendariosList.isEmpty())
        {
            return new ArrayList<>();
        }

        if (cursoId != null)
        {
            List<GrupoAsignatura> gruposAsignaturas = gruposAsignaturasService
                    .getGruposAsignaturasSinAsignar(estudioId, cursoId, semestreId, gruposList,
                            calendariosList, asignaturasList, connectedUserId);

            return UIEntity.toUI(gruposAsignaturas);
        }

        if (agrupacionId != null)
        {
            List<GrupoAsignatura> gruposAsignaturas = gruposAsignaturasService
                    .getGruposAsignaturasSinAsignarByAgrupacion(estudioId, agrupacionId,
                            semestreId, gruposList, calendariosList, asignaturasList, connectedUserId);

            return UIEntity.toUI(gruposAsignaturas);
        }

        throw new IllegalArgumentException();
    }

    @PUT
    @Path("sinAsignar/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> planificaGruposAsignaturasSinAsignar(@PathParam("id") Long id,
            @QueryParam("estudioId") Long estudioId, @QueryParam("cursoId") Long cursoId,
            @QueryParam("semestreId") Long semestreId) throws RegistroNoEncontradoException,
            UnauthorizedUserException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        String usuario = AccessManager.getConnectedUser(request).getName();

        ParamUtils.checkNotNull(estudioId, semestreId);

        Evento eventoOriginal = eventosService.getEventoById(id, connectedUserId);

        GrupoAsignatura grupoAsignatura;
        if (cursoId != null)
        {
            grupoAsignatura = gruposAsignaturasService.planificaGrupoAsignaturaSinAsignar(id,
                    estudioId, cursoId, semestreId, connectedUserId);
        }
        else
        {
            grupoAsignatura = gruposAsignaturasService
                    .planificaGrupoAsignaturaSinAsignarHorarioPorDefecto(id, estudioId,
                            connectedUserId);
        }

        logService.registraNuevaPlanificacionDeEvento(eventoOriginal, usuario, connectedUserId);

        return Collections.singletonList(UIEntity.toUI(grupoAsignatura));
    }
}
