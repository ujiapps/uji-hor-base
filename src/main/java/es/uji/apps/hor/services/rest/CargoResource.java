package es.uji.apps.hor.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.hor.model.Cargo;
import es.uji.apps.hor.services.CargoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("cargo")
public class CargoResource extends CoreBaseService
{
    @InjectParam
    private CargoService cargoService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getCargos()
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<Cargo> cargos = cargoService.getCargos(connectedUserId);

        return UIEntity.toUI(cargos);
    }
}
