package es.uji.apps.hor.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.hor.services.SesionService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.math.BigDecimal;

@Path("report")
public class ReportsResource extends CoreBaseService {

    @InjectParam
    private SesionService sesionService;

    private static final String COCOON_URL = "https://www.uji.es/cocoon/";

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getPathBase() {
        Long personaId = AccessManager.getConnectedUser(request).getId(); //query a apaSession implement
        String session = sesionService.getUserSession(new BigDecimal(personaId));
        UIEntity uiEntity = new UIEntity();
        uiEntity.put("url", COCOON_URL + session + "/");
        return uiEntity;
    }

}
