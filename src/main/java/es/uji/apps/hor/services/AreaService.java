package es.uji.apps.hor.services;

import es.uji.apps.hor.dao.AreaDAO;
import es.uji.apps.hor.dao.PersonaDAO;
import es.uji.apps.hor.model.Area;
import es.uji.apps.hor.model.Cargo;
import es.uji.commons.rest.Role;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AreaService
{
    @Autowired
    private AreaDAO areaDAO;

    @Autowired
    private PersonaDAO personaDAO;

    @Role({ "ADMIN", "USUARIO" })
    public List<Area> getAreaByDepartamentoId(Long departamentoId, Long connectedUserId)
    {
        if (personaDAO.esAdmin(connectedUserId))
        {
            return areaDAO.getAreasByDepartamentoId(departamentoId);
        }

        return areaDAO.getAreasByDepartamentoIdAndPersonaId(departamentoId, connectedUserId);
    }

    public List<Area> getAreaMiPODByDepartamentoId(Long departamentoId, Long connectedUserId)
            throws RegistroNoEncontradoException
    {

        if (personaDAO.esAdmin(connectedUserId))
        {
            return areaDAO.getAreasByDepartamentoId(departamentoId);
        }

        List<Cargo> listaCargos = personaDAO.getCargosByPersonaId(connectedUserId);
        for (Cargo cargo: listaCargos) {
            if (cargo.gestionaPOD()) {
                return areaDAO.getAreasByDepartamentoIdAndPersonaId(departamentoId, connectedUserId);

            }
        }

        return areaDAO.getMiAreaByPersonaId(connectedUserId);

    }
}
