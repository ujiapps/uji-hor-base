package es.uji.apps.hor.services.rest;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.hor.model.Asignatura;
import es.uji.apps.hor.services.AsignaturaService;
import es.uji.apps.hor.services.ParamParseService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

@Path("asignatura")
public class AsignaturaResource extends CoreBaseService
{
    @InjectParam
    private AsignaturaService asignaturaService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getAsignaturasByArea(@QueryParam("areaId") String areaId,
            @QueryParam("semestreId") String semestreId)
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        ParamUtils.checkNotNull(areaId);
        List<Asignatura> asignaturas = asignaturaService
                .getAsignaturasByDepartamentoAndArea(ParamUtils.parseLong(areaId), connectedUserId);

        return modelToUI(asignaturas);
    }

    @GET
    @Path("estudio/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getAsignaturasByTitulacion(@PathParam("id") Long estudioId,
            @QueryParam("cursoId") Long cursoId, @QueryParam("semestreId") Long semestreId,
            @QueryParam("agrupacionId") Long agrupacionId, @QueryParam("gruposIds") String gruposIds)
    {
        ParamUtils.checkNotNull(estudioId, semestreId, gruposIds);

        List<String> gruposAsList = ParamParseService.parseAsStringList(gruposIds);

        if (cursoId != null)
        {
            return modelToUI(asignaturaService.getAsignaturasByTitulacionAndCursoAndSemestre(
                    estudioId, cursoId, semestreId, gruposAsList));
        }

        if (agrupacionId != null)
        {
            return modelToUI(asignaturaService.getAsignaturasByAgrupacionAndSemestreAndGrupos(
                    estudioId, agrupacionId, semestreId, gruposAsList));
        }

        throw new IllegalArgumentException();
    }

    @GET
    @Path("agrupacion/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getAsignaturasByAgrupacion(@PathParam("id") Long agrupacionId,
            @QueryParam("estudioId") Long estudioId, @QueryParam("semestreId") Long semestreId,
            @QueryParam("gruposIds") String gruposIds)
    {
        ParamUtils.checkNotNull(agrupacionId, semestreId, gruposIds);

        List<String> gruposAsList = ParamParseService.parseAsStringList(gruposIds);

        List<Asignatura> asignaturas = asignaturaService
                .getAsignaturasByAgrupacionAndSemestreAndGrupos(estudioId, agrupacionId,
                        semestreId, gruposAsList);

        return modelToUI(asignaturas);
    }

    @GET
    @Path("{id}/creditos")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getCreditosAsignatura(@PathParam("id") String asignaturaId)
    {
        UIEntity uiEntity = new UIEntity();

        List<String> codigosAsignaturasComunes = asignaturaService.getCodigosAsignaturasCompartidas(asignaturaId);

        Map<String, BigDecimal> mapaCreditosTotales = asignaturaService.getCreditosTotalesPodAsignatura(asignaturaId);
        Map<String, BigDecimal> mapaCreditosAsignados = asignaturaService.getCreditosPodAsignados(codigosAsignaturasComunes);

        uiEntity.put("creditosTotalesPod", mapaCreditosTotales.get("creditos"));
        uiEntity.put("creditosTotalesComputablesPod", mapaCreditosTotales.get("creditosComputables"));
        uiEntity.put("creditosAsignadosPod", mapaCreditosAsignados.get("creditos"));
        uiEntity.put("creditosAsignadosComputablesPod", mapaCreditosAsignados.get("creditosComputables"));
        String codigoComun = asignaturaService.getCodigoComunByAsignaturaId(asignaturaId);
        uiEntity.put("codigoComun", codigoComun != null ? codigoComun : asignaturaId);
        return uiEntity;
    }

    @GET
    @Path("profesor")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getAsignaturasProfesor(@QueryParam("semestreId") Long semestreId)
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<Asignatura> asignaturas = asignaturaService.getAsignaturasByProfesor(connectedUserId, semestreId);
        return modelToUI(asignaturas);
    }

    private UIEntity modelToUI(Asignatura asignatura)
    {
        UIEntity entity = new UIEntity();
        entity.setBaseClass("Asignatura");
        entity.put("asignaturaId", asignatura.getId());
        entity.put("nombre", asignatura.getNombre());
        entity.put("comun", asignatura.getComun());
        entity.put("codigoComun", asignatura.getCodigoComun());
        return entity;
    }

    private List<UIEntity> modelToUI(List<Asignatura> listaAsignaturas)
    {
        List<UIEntity> entities = new ArrayList<UIEntity>();
        String codigosAsignaturasComunes = "";
        for (Asignatura asignatura : listaAsignaturas)
        {
            Boolean asignaturaExisteEnListado = existeAsignatura(asignatura.getId(),
                    codigosAsignaturasComunes);
            if (!asignatura.getComun() || !asignaturaExisteEnListado)
            {
                entities.add(modelToUI(asignatura));
                if (asignatura.getComunes() != null)
                {
                    codigosAsignaturasComunes += asignatura.getComunes();
                }
            }
        }
        return entities;
    }

    private Boolean existeAsignatura(String asignaturaId, String codigosAsignaturasComunes)
    {
        return codigosAsignaturasComunes.indexOf(asignaturaId) != -1;
    }

}
