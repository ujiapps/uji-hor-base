package es.uji.apps.hor.services;

import java.util.ArrayList;
import java.util.List;

public class ParamParseService
{

    public static List<String> parseAsStringList(String cadenaSeparadaPorPuntoYComa)
    {
        List<String> elementosList = new ArrayList<String>();

        if (cadenaSeparadaPorPuntoYComa == null)
        {
            return elementosList;
        }

        String[] elementos = cadenaSeparadaPorPuntoYComa.split(";");

        for (String elemento : elementos)
        {
            elemento = elemento.trim();
            if (!elemento.equals(""))
            {
                elementosList.add(elemento);
            }
        }
        return elementosList;
    }

    public static List<Long> parseAsLongList(String cadenaSeparadaPorPuntoYComa)
    {
        List<Long> elementosList = new ArrayList<Long>();

        if (cadenaSeparadaPorPuntoYComa == null)
        {
            return elementosList;
        }

        String[] elementos = cadenaSeparadaPorPuntoYComa.split(";");

        for (String elemento : elementos)
        {
            elemento = elemento.trim();
            if (!elemento.equals(""))
            {
                elementosList.add(Long.parseLong(elemento));
            }
        }
        return elementosList;
    }

}
