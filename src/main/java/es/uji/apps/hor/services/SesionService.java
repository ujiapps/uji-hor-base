package es.uji.apps.hor.services;

import es.uji.apps.hor.dao.SesionDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class SesionService {

    @Autowired
    private SesionDAO sesionDAO;

    public String getUserSession(BigDecimal personaId) {
        return sesionDAO.getUserSession(personaId);
    }
}
