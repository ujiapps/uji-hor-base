package es.uji.apps.hor.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.hor.CircuitoNoPerteneceAEstudioException;
import es.uji.apps.hor.EstudioNoCompartidoException;
import es.uji.apps.hor.model.Circuito;
import es.uji.apps.hor.model.Estudio;
import es.uji.apps.hor.services.CircuitoService;
import es.uji.apps.hor.services.ParamParseService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("circuito")
public class CircuitoResource extends CoreBaseService
{
    @InjectParam
    private CircuitoService circuitoService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getCircuitos(@QueryParam("estudioId") String estudioId,
            @QueryParam("grupoId") String grupoId) throws UnauthorizedUserException,
            RegistroNoEncontradoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        ParamUtils.checkNotNull(estudioId, grupoId);

        Long estudioIdLong = ParamUtils.parseLong(estudioId);

        List<Circuito> circuitos = circuitoService.getCircuitosByEstudioIdAndGrupoId(estudioIdLong,
                grupoId, connectedUserId);

        List<UIEntity> listaResultado = new ArrayList<UIEntity>();

        for (Circuito circuito : circuitos)
        {
            listaResultado.add(circuitoToUI(circuito, estudioIdLong));
        }

        return listaResultado;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> insertaCircuito(UIEntity entity) throws RegistroNoEncontradoException,
            UnauthorizedUserException, EstudioNoCompartidoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        String estudioId = entity.get("estudioId");
        String grupoId = entity.get("grupo");
        String nombre = entity.get("nombre");
        String plazas = entity.get("plazas");
        String estudiosCompartidosIds = entity.get("estudiosCompartidosStr");

        ParamUtils.checkNotNull(estudioId, grupoId, nombre, plazas);

        List<Long> estudiosCompartidosList = ParamParseService.parseAsLongList(estudiosCompartidosIds);

        Circuito circuito = circuitoService.insertaCircuito(ParamUtils.parseLong(estudioId),
                grupoId, nombre, ParamUtils.parseLong(plazas), estudiosCompartidosList,
                connectedUserId);

        return Collections.singletonList(UIEntity.toUI(circuito));
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> updateCircuito(@PathParam("id") String circuitoId,
            @QueryParam("estudioId") String estudioId, UIEntity entity)
            throws RegistroNoEncontradoException, UnauthorizedUserException,
            CircuitoNoPerteneceAEstudioException, EstudioNoCompartidoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        String nombre = entity.get("nombre");
        String plazas = entity.get("plazas");
        String estudiosCompartidosIds = entity.get("estudiosCompartidosStr");

        ParamUtils.checkNotNull(circuitoId, nombre, plazas, estudioId);

        List<Long> estudiosCompartidosList = ParamParseService.parseAsLongList(estudiosCompartidosIds);

        Circuito circuito = circuitoService.updateCircuito(ParamUtils.parseLong(circuitoId),
                ParamUtils.parseLong(estudioId), nombre, ParamUtils.parseLong(plazas),
                estudiosCompartidosList, connectedUserId);

        return Collections.singletonList(UIEntity.toUI(circuito));
    }

    @DELETE
    @Path("{id}")
    public void deleteCircuito(@PathParam("id") String circuitoId,
            @QueryParam("estudioId") String estudioId) throws RegistroNoEncontradoException,
            RegistroConHijosException, UnauthorizedUserException,
            CircuitoNoPerteneceAEstudioException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        ParamUtils.checkNotNull(circuitoId, estudioId);
        circuitoService.borraCircuito(ParamUtils.parseLong(circuitoId),
                ParamUtils.parseLong(estudioId), connectedUserId);

    }

    private UIEntity circuitoToUI(Circuito circuito, Long estudioId)
    {
        UIEntity entity = UIEntity.toUI(circuito);

        List<String> estudiosCompartidos = new ArrayList<String>();
        for (Estudio estudio : circuito.getEstudios())
        {
            if (estudio.getId().equals(estudioId))
            {
                entity.put("estudioId", estudioId);
            }
            else
            {
                estudiosCompartidos.add(estudio.getId().toString());
            }
        }

        entity.put("estudiosCompartidos", estudiosCompartidos);

        return entity;
    }

}
