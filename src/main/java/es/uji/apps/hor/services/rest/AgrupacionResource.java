package es.uji.apps.hor.services.rest;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.hor.AgrupacionNoPerteneceAEstudioException;
import es.uji.apps.hor.EstudioNoCompartidoException;
import es.uji.apps.hor.model.*;
import es.uji.apps.hor.services.*;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.rest.json.tree.TreeRow;
import es.uji.commons.rest.json.tree.TreeRowset;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;

@Path("agrupacion")
public class AgrupacionResource extends CoreBaseService
{
    @InjectParam
    private AsignaturaService asignaturaService;

    @InjectParam
    private EstudiosService estudiosService;

    @InjectParam
    private AgrupacionService agrupacionService;

    @InjectParam
    private EventosService eventosService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getAgrupaciones(@QueryParam("estudioId") Long estudioId)
            throws UnauthorizedUserException, RegistroNoEncontradoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        ParamUtils.checkNotNull(estudioId);

        List<Agrupacion> agrupaciones = agrupacionService.getAgrupacionesByEstudioId(estudioId,
                connectedUserId);

        return agrupacionToUI(agrupaciones, estudioId);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> insertaAgrupacion(UIEntity entity) throws RegistroNoEncontradoException,
            UnauthorizedUserException, EstudioNoCompartidoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        String nombre = entity.get("nombre");
        String estudioId = entity.get("estudioId");
        String estudiosCompartidosIds = entity.get("estudiosCompartidosStr");

        ParamUtils.checkNotNull(nombre, estudioId);
        List<Long> estudiosCompartidosList = ParamParseService
                .parseAsLongList(estudiosCompartidosIds);

        Agrupacion agrupacion = agrupacionService.insertaAgrupacion(nombre,
                estudiosCompartidosList, ParamUtils.parseLong(estudioId), connectedUserId);

        return Collections.singletonList(UIEntity.toUI(agrupacion));
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> updateAgrupacion(@PathParam("id") Long agrupacionId, UIEntity entity)
            throws RegistroNoEncontradoException, UnauthorizedUserException,
            AgrupacionNoPerteneceAEstudioException, EstudioNoCompartidoException

    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        String nombre = entity.get("nombre");
        String estudioId = entity.get("estudioId");

        ParamUtils.checkNotNull(agrupacionId, nombre, estudioId);

        Agrupacion agrupacion = agrupacionService.updateAgrupacion(agrupacionId, nombre,
                ParamUtils.parseLong(estudioId), connectedUserId);

        return Collections.singletonList(UIEntity.toUI(agrupacion));
    }

    @DELETE
    @Path("{id}")
    public ResponseMessage deleteAgrupacion(@PathParam("id") Long agrupacionId,
            @QueryParam("estudioId") Long estudioId) throws RegistroNoEncontradoException,
            RegistroConHijosException, UnauthorizedUserException,
            AgrupacionNoPerteneceAEstudioException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        ParamUtils.checkNotNull(estudioId);

        agrupacionService.borraAgrupacion(agrupacionId, estudioId, connectedUserId);

        return new ResponseMessage(true);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{agrupacionId}/asignaturas")
    public List<UIEntity> getAsignaturas(@PathParam("agrupacionId") Long agrupacionId)
            throws RegistroNoEncontradoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        ParamUtils.checkNotNull(agrupacionId);

        List<AsignaturaAgrupacion> asignaturas = asignaturaService.getAsignaturasByAgrupacionId(
                agrupacionId, connectedUserId);

        return asignaturaToUI(asignaturas);
    }

    private List<UIEntity> asignaturaToUI(List<AsignaturaAgrupacion> asignaturas)
    {
        List<UIEntity> entities = new ArrayList<>();
        for (AsignaturaAgrupacion asignaturaAgrupacion : asignaturas)
        {
            entities.add(asignaturaToUI(asignaturaAgrupacion));
        }
        return entities;
    }

    private UIEntity asignaturaToUI(AsignaturaAgrupacion asignaturaAgrupacion)
    {
        UIEntity entity = new UIEntity();

        entity.put("id", asignaturaAgrupacion.getId());
        entity.put("agrupacionId", asignaturaAgrupacion.getAgrupacion().getId());
        entity.put("asignaturaId", asignaturaAgrupacion.getAsignatura().getId());
        entity.put("asignaturaNombre", asignaturaAgrupacion.getAsignatura().getNombre());
        entity.put("asignaturaCurso", asignaturaAgrupacion.getAsignatura().getCursoId());
        entity.put("subgrupoId", asignaturaAgrupacion.getSubgrupoId());
        entity.put("tipoSubgrupo", asignaturaAgrupacion.getTipoSubgrupo());

        return entity;
    }

    @DELETE
    @Path("{agrupacionId}/asignaturas/{asignaturaAgrupacionId}")
    public void deleteAsignaturaAgrupacion(@PathParam("agrupacionId") Long agrupacionId,
            @PathParam("asignaturaAgrupacionId") Long asignaturaAgrupacionId)
            throws RegistroNoEncontradoException, AgrupacionNoPerteneceAEstudioException,
            UnauthorizedUserException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        ParamUtils.checkNotNull(agrupacionId, asignaturaAgrupacionId);
        AsignaturaAgrupacion asignaturaAgrupacion = agrupacionService
                .getAsignaturaAgrupacionById(asignaturaAgrupacionId);

        if (asignaturaAgrupacion.getSubgrupoId() == null
                && asignaturaAgrupacion.getGrupoId() == null)
        {
            agrupacionService.deleteAsignaturaAgrupacionConCompartidas(agrupacionId,
                    asignaturaAgrupacion.getAsignatura().getId(), connectedUserId);
        }
        else
        {
            agrupacionService.deleteAsignaturaAgrupacion(asignaturaAgrupacion, connectedUserId);
        }
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{agrupacionId}/asignaturas")
    public void addAsignaturaAgrupacion(@PathParam("agrupacionId") Long agrupacionId,
            UIEntity entity) throws RegistroNoEncontradoException,
            AgrupacionNoPerteneceAEstudioException, UnauthorizedUserException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        String asignaturaId = entity.get("asignaturaId");
        ParamUtils.checkNotNull(agrupacionId, asignaturaId);

        AsignaturaAgrupacion asignaturaAgrupacion = entityToAsignaturaAgrupacion(entity);

        if (agrupacionService.existeAsignaturaAgrupacion(asignaturaAgrupacion))
        {
            return;
        }

        if (asignaturaAgrupacion.getSubgrupoId() == null
                && (asignaturaAgrupacion.getTipoSubgrupo() == null || asignaturaAgrupacion
                        .getTipoSubgrupo().isEmpty()))
        {
            agrupacionService.addAsignaturaAgrupacionConCompartidas(agrupacionId, asignaturaId,
                    connectedUserId);
        }
        else
        {
            agrupacionService.addAsignaturaAgrupacion(asignaturaAgrupacion);
        }
    }

    private AsignaturaAgrupacion entityToAsignaturaAgrupacion(UIEntity entity)
    {
        AsignaturaAgrupacion asignaturaAgrupacion = entity.toModel(AsignaturaAgrupacion.class);

        asignaturaAgrupacion.setAsignatura(new Asignatura(entity.get("asignaturaId")));
        asignaturaAgrupacion.setGrupoId(entity.get("grupoId"));
        asignaturaAgrupacion.setTipoSubGrupo(entity.get("tipoSubgrupo"));
        return asignaturaAgrupacion;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{agrupacionId}/estudios")
    public List<UIEntity> getEstudios(@PathParam("agrupacionId") String agrupacionId)
            throws RegistroNoEncontradoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        ParamUtils.checkNotNull(agrupacionId);

        List<Estudio> estudios = estudiosService.getEstudiosByAgrupacionId(
                ParamUtils.parseLong(agrupacionId), connectedUserId);

        return UIEntity.toUI(estudios);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{agrupacionId}/asignaturasdisponibles/tree")
    public TreeRowset getAsignaturasDisponiblesAgrupacion(
            @PathParam("agrupacionId") Long agrupacionId) throws RegistroNoEncontradoException,
            UnauthorizedUserException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        ParamUtils.checkNotNull(agrupacionId);

        TreeRowset treeRowSetAgrupacion = new TreeRowset();
        Agrupacion agrupacion = agrupacionService.getAgrupacionConEstudios(agrupacionId);

        for (Estudio estudio : agrupacion.getEstudios())
        {
            treeRowSetAgrupacion.getRow().add(creaRowSetDesdeEstudio(estudio));
        }
        return treeRowSetAgrupacion;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{agrupacionId}/estudio/{estudioId}/asignaturasdisponibles/tree")
    public TreeRowset getAsignaturasDisponiblesEstudio(
            @PathParam("agrupacionId") Long agrupacionId, @PathParam("estudioId") Long estudioId)
            throws RegistroNoEncontradoException, UnauthorizedUserException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        ParamUtils.checkNotNull(agrupacionId);

        TreeRowset treeRowSetAgrupacion = new TreeRowset();
        Agrupacion agrupacion = agrupacionService.getAgrupacionConEstudio(agrupacionId, estudioId);

        for (Estudio estudio : agrupacion.getEstudios())
        {
            treeRowSetAgrupacion.getRow().add(creaRowSetDesdeEstudio(estudio));
        }
        return treeRowSetAgrupacion;
    }

    private TreeRow creaRowSetDesdeEstudio(Estudio estudio) throws RegistroNoEncontradoException,
            UnauthorizedUserException
    {
        TreeRow treeRowTitulcacion = new TreeRow();

        treeRowTitulcacion.setId(UUID.randomUUID().toString());
        treeRowTitulcacion.setTitle(estudio.getNombre());
        treeRowTitulcacion.setText(estudio.getNombre());
        treeRowTitulcacion.setLeaf("false");

        List<TreeRow> listaTreeRowCursos = new ArrayList<>();
        for (Long curso = 1L; curso <= estudio.getNumeroCursos(); curso++)
        {
            listaTreeRowCursos.add(creaRowSetDesdeCurso(estudio, curso));
        }
        treeRowTitulcacion.setHijos(listaTreeRowCursos);
        return treeRowTitulcacion;
    }

    private TreeRow creaRowSetDesdeCurso(Estudio estudio, Long curso)
            throws RegistroNoEncontradoException, UnauthorizedUserException
    {
        TreeRow treeRowCurso = new TreeRow();

        treeRowCurso.setId(UUID.randomUUID().toString());
        treeRowCurso.setTitle("Curs " + curso.toString());
        treeRowCurso.setText("Curs " + curso.toString());
        treeRowCurso.setLeaf("false");

        List<TreeRow> listaTreeRowAsignaturas = new ArrayList<TreeRow>();
        for (Asignatura asignatura : asignaturaService.getAsignaturasByTitulacionAndCurso(
                estudio.getId(), curso))
        {
            listaTreeRowAsignaturas.add(creaAsignaturaToTreeRow(asignatura, estudio, curso));
        }
        treeRowCurso.setHijos(listaTreeRowAsignaturas);
        return treeRowCurso;
    }

    private TreeRow creaAsignaturaToTreeRow(Asignatura asignatura, Estudio estudio, Long cursoId)
            throws RegistroNoEncontradoException, UnauthorizedUserException
    {
        TreeRow treeRowAsignatura = new TreeRow();

        treeRowAsignatura.setId(asignatura.getId());
        treeRowAsignatura.setTitle(asignatura.getNombre());
        treeRowAsignatura.setText(asignatura.getId() + " - " + asignatura.getNombre());
        treeRowAsignatura.setLeaf("false");

        List<TreeRow> listaTreeRowSubgrupos = new ArrayList<>();
        for (Subgrupo subgrupo : eventosService.subgruposPorAsignaturaEstudioYCurso(
                asignatura.getId(), estudio.getId(), cursoId))
        {
            listaTreeRowSubgrupos.add(creaTreeRowDesdeSubgrupo(asignatura, subgrupo));
        }
        treeRowAsignatura.setHijos(listaTreeRowSubgrupos);

        return treeRowAsignatura;
    }

    private TreeRow creaTreeRowDesdeSubgrupo(Asignatura asignatura, Subgrupo subgrupo)
    {
        TreeRow treeRowSubgrupo = new TreeRow();

        String codigo = MessageFormat.format("{0};{1};{2}", asignatura.getId(),
                subgrupo.getTipoSubgrupoId(), subgrupo.getSubgrupoId());
        String titulo = MessageFormat.format("Grup {0} - {1} {2}", subgrupo.getGrupoId(),
                subgrupo.getTipoSubgrupoId(), subgrupo.getSubgrupoId());
        treeRowSubgrupo.setId(codigo);
        treeRowSubgrupo.setTitle(titulo);
        treeRowSubgrupo.setText(titulo);
        treeRowSubgrupo.setLeaf("true");

        return treeRowSubgrupo;
    }

    private List<UIEntity> agrupacionToUI(List<Agrupacion> agrupaciones, Long estudioId)
    {
        List<UIEntity> entities = new ArrayList<>();
        for (Agrupacion agrupacion : agrupaciones)
        {
            entities.add(agrupacionToUI(agrupacion, estudioId));
        }
        return entities;
    }

    private UIEntity agrupacionToUI(Agrupacion agrupacion, Long estudioId)
    {
        UIEntity entity = UIEntity.toUI(agrupacion);
        List<String> estudiosCompartidos = new ArrayList<String>();
        List<String> estudiosCompartidosNombre = new ArrayList<String>();
        for (Estudio estudio : agrupacion.getEstudios())
        {
            if (estudio.getId().equals(estudioId))
            {
                entity.put("estudioId", estudioId);
            }
            else
            {
                estudiosCompartidos.add(estudio.getId().toString());
                estudiosCompartidosNombre.add(estudio.getNombre());
            }
        }

        entity.put("estudiosCompartidos", estudiosCompartidos);
        entity.put("estudiosCompartidosNombre", estudiosCompartidosNombre);
        return entity;
    }

}
