package es.uji.apps.hor.services.rest;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.hor.dao.EstudiosDAO;
import es.uji.apps.hor.model.Grupo;
import es.uji.apps.hor.services.GruposService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;

@Path("grupo")
public class GrupoResource extends CoreBaseService
{
    @InjectParam
    private GruposService consultaGrupos;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getGrupos(@QueryParam("semestreId") Long semestreId,
            @QueryParam("cursoId") Long cursoId, @QueryParam("estudioId") Long estudioId,
            @QueryParam("agrupacionId") Long agrupacionId) throws UnauthorizedUserException,
            RegistroNoEncontradoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        ParamUtils.checkNotNull(estudioId, semestreId);

        if (cursoId != null)
        {
            return UIEntity.toUI(consultaGrupos.getGrupos(semestreId, cursoId, estudioId, connectedUserId));
        }

        if (agrupacionId != null)
        {
            return UIEntity.toUI(consultaGrupos.getGruposAgrupacion(agrupacionId, semestreId, connectedUserId));
        }

        throw new IllegalArgumentException();
    }

    @GET
    @Path("circuito")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getGruposCircuitos(@QueryParam("cursoId") String cursoId,
            @QueryParam("estudioId") String estudioId) throws UnauthorizedUserException,
            RegistroNoEncontradoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        ParamUtils.checkNotNull(cursoId, estudioId);

        List<Grupo> grupos = consultaGrupos.getGruposCircuitos(ParamUtils.parseLong(cursoId),
                ParamUtils.parseLong(estudioId), connectedUserId);

        return UIEntity.toUI(grupos);
    }

    @GET
    @Path("asignatura")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getGruposAsignatura(@QueryParam("asignaturaId") String asignaturaId,
            @QueryParam("semestreId") Long semestreId) throws UnauthorizedUserException,
            RegistroNoEncontradoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        ParamUtils.checkNotNull(asignaturaId, semestreId);

        List<Grupo> grupos = consultaGrupos.getGruposAsignatura(asignaturaId, semestreId,
                connectedUserId);

        return UIEntity.toUI(grupos);
    }

    @GET
    @Path("agrupacion")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getGruposAgrupacion(@QueryParam("agrupacionId") Long agrupacionId,
            @QueryParam("semestreId") Long semestreId) throws UnauthorizedUserException,
            RegistroNoEncontradoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        ParamUtils.checkNotNull(agrupacionId, semestreId);

        List<Grupo> grupos = consultaGrupos.getGruposAgrupacion(agrupacionId, semestreId,
                connectedUserId);

        return UIEntity.toUI(grupos);
    }

}