package es.uji.apps.hor.services;

import es.uji.apps.hor.dao.EstudiosDAO;
import es.uji.apps.hor.dao.PersonaDAO;
import es.uji.apps.hor.dao.SemestresDetalleDAO;
import es.uji.apps.hor.model.Persona;
import es.uji.apps.hor.model.SemestreDetalle;
import es.uji.commons.rest.Role;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class SemestresDetalleService {

    private final SemestresDetalleDAO semestresDetalleDAO;

    @Autowired
    private PersonaDAO personaDAO;

    @Autowired
    private EstudiosDAO estudioDAO;

    @Autowired
    public SemestresDetalleService(SemestresDetalleDAO semestresDetalleDAO) {
        this.semestresDetalleDAO = semestresDetalleDAO;
    }

    @Role({"ADMIN", "USUARIO"})
    public List<SemestreDetalle> getSemestresDetallesTodos(Long connectedUserId) {
        return semestresDetalleDAO.getSemestresDetalleTodos();
    }

    @Role({"ADMIN", "USUARIO"})
    public List<SemestreDetalle> getSemestresDetallesPorEstudioIdYSemestreIdYCursoId(Long estudioId,
                                                                                     Long semestreId, Long curso, Long connectedUserId) throws UnauthorizedUserException,
            RegistroNoEncontradoException {
        if (!personaDAO.esAdmin(connectedUserId) && !estudioDAO.isEstudioAbiertoConsulta(estudioId)) {
            Persona persona = personaDAO.getPersonaConTitulacionesYCentrosById(connectedUserId);
            persona.compruebaAccesoAEstudio(estudioId);
        }

        return semestresDetalleDAO.getSemestresDetallesPorEstudioIdYSemestreIdYCurso(estudioId,
                semestreId, curso);
    }


    @Role({"ADMIN", "USUARIO"})
    public List<SemestreDetalle> getRangoFechasSemestresDetallesPorEstudioIdYSemestreId(Long estudioId, Long semestreId, Long connectedUserId) throws RegistroNoEncontradoException, UnauthorizedUserException {
        if (!personaDAO.esAdmin(connectedUserId) && !estudioDAO.isEstudioAbiertoConsulta(estudioId))
        {
            Persona persona = personaDAO.getPersonaConTitulacionesYCentrosById(connectedUserId);
            persona.compruebaAccesoAEstudio(estudioId);
        }

        List<SemestreDetalle> semestresDetalle = semestresDetalleDAO.getRangoFechasSemestresDetallesPorEstudioIdYSemestreId(estudioId, semestreId);

        return new ArrayList<>(Collections.singleton(SemestreDetalle.creaSemestreDetalleGeneralDesdeSemestresDetalles(semestresDetalle)));
    }
}
