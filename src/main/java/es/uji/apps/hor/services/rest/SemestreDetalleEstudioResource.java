package es.uji.apps.hor.services.rest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.hor.model.SemestreDetalleEstudio;
import es.uji.apps.hor.services.SemestresDetalleEstudioService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.AccessManager;

@Path("semestredetalleestudio")
public class SemestreDetalleEstudioResource extends CoreBaseService
{
    @InjectParam
    private SemestresDetalleEstudioService semestresDetalleEstudioService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getSemestresDetalleEstudio()
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<SemestreDetalleEstudio> semestresDetalleEstudioss = semestresDetalleEstudioService
                .getSemestresDetallesTodos(connectedUserId);

        List<UIEntity> listaResultados = new ArrayList<UIEntity>();

        for (SemestreDetalleEstudio semestreDetalleEstudio : semestresDetalleEstudioss)
        {
            UIEntity resultado = UIEntity.toUI(semestreDetalleEstudio);
            listaResultados.add(resultado);
        }

        return listaResultados;
    }

    @GET
    @Path("{semestreDetalleEstudioId}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getSemestresDetalleEstudioById(
            @PathParam("semestreDetalleEstudioId") Long semestreDetalleEstudioId)
    {
        ParamUtils.checkNotNull(semestreDetalleEstudioId);
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        SemestreDetalleEstudio semestreDetalleEstudio = semestresDetalleEstudioService
                .getSemestresDetallesById(semestreDetalleEstudioId, connectedUserId);
        return Collections.singletonList(UIEntity.toUI(semestreDetalleEstudio));
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity insert(UIEntity entity) throws ParseException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        SemestreDetalleEstudio semestreDetalleEstudio = uiToModel(entity);
        semestreDetalleEstudio = semestresDetalleEstudioService.insert(semestreDetalleEstudio,
                connectedUserId);
        return UIEntity.toUI(semestreDetalleEstudio);
    }

    @PUT
    @Path("{semestreDetalleEstudioId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity update(@PathParam("semestreDetalleEstudioId") Long semestreDetalleEstudioId,
            UIEntity entity) throws ParseException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        SemestreDetalleEstudio semestreDetalleEstudio = uiToModel(entity);
        semestresDetalleEstudioService.update(semestreDetalleEstudio, connectedUserId);
        return entity;
    }

    @DELETE
    @Path("{semestreDetalleEstudioId}")
    public ResponseMessage deleteMeta(
            @PathParam("semestreDetalleEstudioId") Long semestreDetalleEstudioId)
            throws RegistroNoEncontradoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        return semestresDetalleEstudioService.delete(semestreDetalleEstudioId, connectedUserId);
    }

    private SemestreDetalleEstudio uiToModel(UIEntity entity) throws ParseException
    {
        SemestreDetalleEstudio semestreDetalleEstudio = entity
                .toModel(SemestreDetalleEstudio.class);

        semestreDetalleEstudio.setFechaInicio(fechaParse(entity.get("fechaInicio")));
        semestreDetalleEstudio.setFechaFin(fechaParse(entity.get("fechaFin")));
        semestreDetalleEstudio
                .setFechaExamenesInicio(fechaParse(entity.get("fechaExamenesInicio")));
        semestreDetalleEstudio.setFechaExamenesFin(fechaParse(entity.get("fechaExamenesFin")));
        semestreDetalleEstudio.setFechaExamenesInicioC2(fechaParse(entity
                .get("fechaExamenesInicioC2")));
        semestreDetalleEstudio.setFechaExamenesFinC2(fechaParse(entity.get("fechaExamenesFinC2")));

        return semestreDetalleEstudio;
    }

    private Date fechaParse(String fecha) throws ParseException
    {
        SimpleDateFormat formateadorFecha = new SimpleDateFormat("dd/MM/yyyy");
        return formateadorFecha.parse(fecha);
    }

}
