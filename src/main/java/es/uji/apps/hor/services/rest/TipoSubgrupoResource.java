package es.uji.apps.hor.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.hor.model.TipoSubgrupoUI;
import es.uji.apps.hor.services.TipoSubgrupoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("tiposubgrupo")
public class TipoSubgrupoResource extends CoreBaseService
{
    @InjectParam
    private TipoSubgrupoService tipoSubgrupoService;

    @GET
    @Path("asignatura")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getGruposAsignatura(@QueryParam("asignaturaId") String asignaturaId,
                                             @QueryParam("semestreId") Long semestreId) throws UnauthorizedUserException,
            RegistroNoEncontradoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        ParamUtils.checkNotNull(asignaturaId, semestreId);

        List<TipoSubgrupoUI> subGrupos = tipoSubgrupoService.getSubGruposAsignatura(asignaturaId, semestreId, connectedUserId);

        return UIEntity.toUI(subGrupos);
    }
}