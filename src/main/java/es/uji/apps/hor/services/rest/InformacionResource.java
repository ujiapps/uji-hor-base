package es.uji.apps.hor.services.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.hor.model.Informacion;
import es.uji.apps.hor.services.InformacionService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

@Path("informacion")
public class InformacionResource extends CoreBaseService
{
    @InjectParam
    private InformacionService informacionService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getInformaciones()
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<Informacion> listaInformacion = informacionService.getInformacion(connectedUserId);

        return UIEntity.toUI(listaInformacion);
    }
}
