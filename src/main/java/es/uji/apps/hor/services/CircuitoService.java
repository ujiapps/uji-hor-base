package es.uji.apps.hor.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.hor.CircuitoNoPerteneceAEstudioException;
import es.uji.apps.hor.EstudioNoCompartidoException;
import es.uji.apps.hor.dao.CircuitoDAO;
import es.uji.apps.hor.dao.EstudiosDAO;
import es.uji.apps.hor.dao.PersonaDAO;
import es.uji.apps.hor.model.Circuito;
import es.uji.apps.hor.model.Estudio;
import es.uji.apps.hor.model.Persona;
import es.uji.commons.rest.Role;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;

@Service
public class CircuitoService
{
    @Autowired
    private CircuitoDAO circuitoDAO;

    @Autowired
    private EstudiosDAO estudiosDAO;

    @Autowired
    private PersonaDAO personaDAO;

    @Role({ "ADMIN", "USUARIO" })
    public List<Circuito> getCircuitosByEstudioIdAndGrupoId(Long estudioId, String grupoId,
            Long connectedUserId) throws UnauthorizedUserException, RegistroNoEncontradoException
    {
        if (!personaDAO.esAdmin(connectedUserId))
        {
            Persona persona = personaDAO.getPersonaConTitulacionesYCentrosById(connectedUserId);
            persona.compruebaAccesoAEstudio(estudioId);
        }

        return circuitoDAO.getCircuitosByEstudioIdAndGrupoId(estudioId, grupoId);
    }

    @Role({ "ADMIN", "USUARIO" })
    public Circuito insertaCircuito(Long estudioId, String grupoId, String nombre, Long plazas,
            List<Long> estudiosCompartidos, Long connectedUserId)
            throws RegistroNoEncontradoException, UnauthorizedUserException,
            EstudioNoCompartidoException
    {
        if (!personaDAO.esAdmin(connectedUserId))
        {
            Persona persona = personaDAO.getPersonaConTitulacionesYCentrosById(connectedUserId);
            persona.compruebaAccesoAEstudio(estudioId);
        }

        Circuito circuito = new Circuito();
        circuito.setNombre(nombre);
        circuito.setGrupo(grupoId);
        circuito.setPlazas(plazas);

        Estudio estudio = estudiosDAO.getEstudioById(estudioId);
        List<Estudio> listaEstudios = new ArrayList<Estudio>();
        listaEstudios.add(estudio);

        for (Long estudioCompartidoId : estudiosCompartidos)
        {
            Estudio estudioCompartido = new Estudio(estudioCompartidoId);

            estudio.compruebaEstudioCompartido(estudioCompartido);
            listaEstudios.add(estudioCompartido);
        }

        circuito.setEstudios(listaEstudios);

        Circuito resultado = circuitoDAO.insertNuevoCircuito(circuito);

        return resultado;
    }

    @Role({ "ADMIN", "USUARIO" })
    public void borraCircuito(Long circuitoId, Long estudioId, Long connectedUserId)
            throws RegistroNoEncontradoException, UnauthorizedUserException,
            RegistroConHijosException, CircuitoNoPerteneceAEstudioException
    {
        if (!personaDAO.esAdmin(connectedUserId))
        {
            Persona persona = personaDAO.getPersonaConTitulacionesYCentrosById(connectedUserId);
            persona.compruebaAccesoAEstudio(estudioId);
        }

        Circuito circuito = circuitoDAO.getCircuitoById(circuitoId);
        circuito.compruebaEstudioEnCircuito(estudioId);

        circuitoDAO.deleteCircuitoById(circuitoId);
    }

    @Role({ "ADMIN", "USUARIO" })
    public Circuito updateCircuito(Long circuitoId, Long estudioId, String nombre, Long plazas,
            List<Long> estudiosCompartidos, Long connectedUserId)
            throws RegistroNoEncontradoException, UnauthorizedUserException,
            CircuitoNoPerteneceAEstudioException, EstudioNoCompartidoException
    {
        if (!personaDAO.esAdmin(connectedUserId))
        {
            Persona persona = personaDAO.getPersonaConTitulacionesYCentrosById(connectedUserId);
            persona.compruebaAccesoAEstudio(estudioId);
        }

        Circuito circuito = circuitoDAO.getCircuitoById(circuitoId);
        Estudio estudio = estudiosDAO.getEstudioById(estudioId);
        circuito.compruebaEstudioEnCircuito(estudioId);

        List<Estudio> listaEstudios = new ArrayList<Estudio>();
        listaEstudios.add(estudio);

        for (Long estudioCompartidoId : estudiosCompartidos)
        {
            Estudio estudioCompartido = new Estudio(estudioCompartidoId);

            estudio.compruebaEstudioCompartido(estudioCompartido);
            listaEstudios.add(estudioCompartido);
        }

        circuito.update(nombre, plazas, listaEstudios);

        circuitoDAO.updateCircuito(circuito);
        return circuito;
    }
}
