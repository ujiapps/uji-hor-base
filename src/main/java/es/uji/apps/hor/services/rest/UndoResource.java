package es.uji.apps.hor.services.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.hor.NoSePuedeDeshacerException;
import es.uji.apps.hor.model.Evento;
import es.uji.apps.hor.model.Log;
import es.uji.apps.hor.services.UndoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

@Path("undo")
public class UndoResource extends CoreBaseService
{
    @InjectParam
    private UndoService undoService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getUndoLogs()
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        String usuario = AccessManager.getConnectedUser(request).getName();

        List<Log> listaLogs = undoService.getUndoLogsIsolated(usuario, null, connectedUserId);

        return UIEntity.toUI(listaLogs);
    }

    @GET
    @Path("item/{itemId}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getUndoLogsItsm(@PathParam("itemId") Long itemId)
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        String usuario = AccessManager.getConnectedUser(request).getName();

        List<Log> listaLogs = undoService.getUndoLogsIsolated(usuario, itemId, connectedUserId);

        return UIEntity.toUI(listaLogs);
    }

    @PUT
    @Path("log/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity undo(@PathParam("id") Long id)
            throws NoSePuedeDeshacerException
    {
        String usuario = AccessManager.getConnectedUser(request).getName();
        Evento evento = undoService.undo(id, usuario);
        return UIEntity.toUI(evento);
    }
}
