package es.uji.apps.hor.services.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import es.uji.apps.hor.model.TipoEstudio;
import org.springframework.beans.factory.annotation.Autowired;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.hor.dao.EstudiosDAO;
import es.uji.apps.hor.model.Agrupacion;
import es.uji.apps.hor.model.Curso;
import es.uji.apps.hor.model.Estudio;
import es.uji.apps.hor.services.AgrupacionService;
import es.uji.apps.hor.services.CursosService;
import es.uji.apps.hor.services.EstudiosService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;

@Path("estudio")
public class EstudioResource extends CoreBaseService
{
    @InjectParam
    private EstudiosService estudiosService;

    @InjectParam
    private CursosService cursosService;

    @InjectParam
    private AgrupacionService agrupacionService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getEstudiosPorCentro(@QueryParam("centroId") String centroId)
            throws NumberFormatException, UnauthorizedUserException, RegistroNoEncontradoException
    {
        List<Estudio> estudios = new ArrayList<Estudio>();

        Long connectedUserId = AccessManager.getConnectedUserId(request);

        if (centroId == null) {
            estudios = estudiosService.getEstudios(connectedUserId);
        }
        else
        {
            estudios = estudiosService.getEstudiosByCentroId(Long.parseLong(centroId),
                    connectedUserId);
        }

        return UIEntity.toUI(estudios);
    }

    @GET
    @Path("grado")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getEstudiosGrado()
            throws NumberFormatException, UnauthorizedUserException, RegistroNoEncontradoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<Estudio> estudios = estudiosService.getEstudiosByTipo(connectedUserId, TipoEstudio.GRADO);
        return UIEntity.toUI(estudios);
    }

    @GET
    @Path("gestion")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTodosLosEstudios(@QueryParam("centroId") String centroId)
            throws NumberFormatException, UnauthorizedUserException, RegistroNoEncontradoException
    {
        List<Estudio> estudios = new ArrayList<Estudio>();

        Long connectedUserId = AccessManager.getConnectedUserId(request);

        if (centroId == null)
        {
            estudios = estudiosService.getTodosLosEstudios(connectedUserId);
        }
        else
        {
            estudios = estudiosService.getEstudiosByCentroId(Long.parseLong(centroId),
                    connectedUserId);
        }

        return UIEntity.toUI(estudios);
    }

    @GET
    @Path("compartido")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getEstudiosCompartidosDeEstudio(@QueryParam("estudioId") String estudioId)
            throws RegistroNoEncontradoException, UnauthorizedUserException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<Estudio> estudios = estudiosService.getEstudiosCompartidosDeEstudio(
                ParamUtils.parseLong(estudioId), connectedUserId);

        return UIEntity.toUI(estudios);
    }

    @GET
    @Path("abierto")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getEstudiosAbiertosConsulta()
            throws RegistroNoEncontradoException, UnauthorizedUserException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<Estudio> estudios = estudiosService.getEstudiosAbiertosConsulta(connectedUserId);

        return UIEntity.toUI(estudios);
    }

    @GET
    @Path("{estudioId}/cursoagrupacion")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getCursosAgrupacionesByEstudio(@PathParam("estudioId") Long estudioId)
            throws RegistroNoEncontradoException, UnauthorizedUserException
    {
        final Long CURSO_TIPO = 1L;
        final Long AGRUPACION_TIPO = 2L;

        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<UIEntity> entities = new ArrayList<>();
        for (Curso curso : cursosService.getCursos(estudioId, connectedUserId))
        {
            UIEntity entity = new UIEntity();
            entity.put("tipoId", CURSO_TIPO);
            entity.put("tipoNombre", "Curs");
            entity.put("valor", curso.getCurso());
            entity.put("nombre", curso.getCurso());
            entities.add(entity);
        }

        for (Agrupacion agrupacion : agrupacionService.getAgrupacionesByEstudioId(estudioId,
                connectedUserId))
        {
            UIEntity entity = new UIEntity();
            entity.put("tipoId", AGRUPACION_TIPO);
            entity.put("tipoNombre", "Agrupació");
            entity.put("valor", agrupacion.getId());
            entity.put("nombre", agrupacion.getNombre());
            entities.add(entity);
        }

        return entities;
    }

}