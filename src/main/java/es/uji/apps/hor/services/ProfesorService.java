package es.uji.apps.hor.services;

import es.uji.apps.hor.EventoYaAsignado;
import es.uji.apps.hor.dao.CargoDAO;
import es.uji.apps.hor.dao.EventosDAO;
import es.uji.apps.hor.dao.PersonaDAO;
import es.uji.apps.hor.dao.ProfesorDAO;
import es.uji.apps.hor.model.*;
import es.uji.commons.rest.Role;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProfesorService
{
    @Autowired
    private ProfesorDAO profesorDAO;

    @Autowired
    private CargoDAO cargoDAO;

    @Autowired
    private EventosDAO eventosDAO;

    @Autowired
    private PersonaDAO personaDAO;

    @Role({ "ADMIN", "USUARIO" })
    public List<Profesor> getProfesoresConCreditosRestantesByAreaId(Long areaId,
            Long connectedUserId) throws RegistroNoEncontradoException
    {
        List<Cargo> listaCargos = cargoDAO.getCargos(connectedUserId);

        if (listaCargos.size() > 0)
        {
            return profesorDAO.getProfesoresYCreditosRestantesByAreaIdAndPersonaId(areaId);
        }
        else
        {
            List<Profesor> listaProfesor = new ArrayList<Profesor>();
            listaProfesor.add(profesorDAO.getProfesorById(connectedUserId));
            return listaProfesor;
        }
    }

    public void asociaClase(Long itemId, Long profesorId, Long connectedUserId)
            throws RegistroNoEncontradoException
    {
        // Persona persona = personaDAO.g(connectedUserId);
        // Asignatura asignatura = asignaturaDAO.getAsignaturaByAsignaturaId(asignaturaId);
        // if (!personaDAO.esAdmin(connectedUserId))
        // {
        // persona.compruebaAccesoAAsignatura(asignatura);
        // }

        Evento evento = eventosDAO.getEventoById(itemId);
        List<Evento> eventos = eventosDAO.getEventosDelMismoGrupoIncluyendoAnuales(evento);

        asignaProfesorAEventos(eventos, profesorId);
    }

    public void desasociaClase(Long itemId, Long profesorId, Long connectedUserId)
            throws RegistroNoEncontradoException
    {
        // Persona persona = personaDAO.g(connectedUserId);
        // Asignatura asignatura = asignaturaDAO.getAsignaturaByAsignaturaId(asignaturaId);
        // if (!personaDAO.esAdmin(connectedUserId))
        // {
        // persona.compruebaAccesoAAsignatura(asignatura);
        // }

        Evento evento = eventosDAO.getEventoById(itemId);
        List<Evento> eventos = eventosDAO.getEventosDelMismoGrupoIncluyendoAnuales(evento);

        desasignaProfesoroAEventos(eventos, profesorId);

    }

    private void asignaProfesorAEventos(List<Evento> eventos, Long profesorId)
            throws RegistroNoEncontradoException
    {
        for (Evento evento : eventos)
        {
            profesorDAO.asociaClase(evento.getId(), profesorId);
        }
    }

    private void desasignaProfesoroAEventos(List<Evento> eventos, Long profesorId)
            throws RegistroNoEncontradoException
    {
        for (Evento evento : eventos)
        {
            profesorDAO.desasociaClase(evento.getId(), profesorId);
        }
    }

    public void sincronizaItemsDetalle(ProfesorDetalle profesorDetalle,
            HashMap<Long, List> fechasSeleccionadasPorEvento) throws Exception
    {
        Evento eventoPrincipal = eventosDAO.getEventoById(profesorDetalle.getEvento().getId());

        Double creditosProfesor = (profesorDetalle.getCreditos() != null)
                ? profesorDetalle.getCreditos().doubleValue() : null;
        Double creditosComputablesProfesor = (profesorDetalle.getCreditosComputables() != null)
                ? profesorDetalle.getCreditosComputables().doubleValue() : null;

        Double creditosSubgrupo = (eventoPrincipal.getCreditos() != null)
                ? eventoPrincipal.getCreditos().doubleValue() : null;
        Double creditosComputablesSubgrupo = (eventoPrincipal.getCreditosComputables() != null)
                ? eventoPrincipal.getCreditosComputables().doubleValue() : null;

        Asignatura asignatura = eventoPrincipal.getAsignaturas().get(0);
        if (asignatura.isMaster() && creditosProfesor != null
                && creditosComputablesProfesor == null)
        {
            throw new Exception(
                    "No es poden especificar crèdits del professor sense especificar els crèdits computables per a les assignatures de màster");
        }

        if (creditosProfesor != null
                && ((creditosProfesor <= 0) || (creditosProfesor > creditosSubgrupo)))
        {
            throw new Exception(
                    "El nombre de crèdits assignats al professor no pot superar el nombre total de crèdits del subgrup i ha de ser major de zero");
        }

        if (creditosComputablesSubgrupo == null && creditosComputablesProfesor != null)
        {
            throw new Exception(
                    "No es poden especificar crèdits computables quan el subgrup no te assignats crèdits computables");
        }

        if (creditosProfesor != null && creditosComputablesProfesor != null
                && (creditosComputablesProfesor.doubleValue() > creditosProfesor.doubleValue()))
        {
            throw new Exception(
                    "El nombre de crèdits computables no por set superior al nombre de crèdits assignats al subgrup");
        }

        if (creditosComputablesSubgrupo != null && creditosComputablesProfesor != null
                && ((creditosComputablesProfesor < 0)
                        || (creditosComputablesProfesor > creditosComputablesSubgrupo)))
        {
            throw new Exception(
                    "El nombre de crèdits computables del professor no pot superar el nombre total de crèdits computables del subgrup i ha de ser major o igual que zero");
        }

        actualizaCreditos(profesorDetalle, eventoPrincipal);
        actualizaDetalleManualYFechas(profesorDetalle, eventoPrincipal,
                fechasSeleccionadasPorEvento);
    }

    private void actualizaDetalleManualYFechas(ProfesorDetalle profesorDetalle,
            Evento eventoPrincipal, HashMap<Long, List> fechasSeleccionadasPorEvento)
            throws EventoYaAsignado
    {
        List<Evento> eventos = eventosDAO.getEventosMismoGrupoConDetalleProfesor(eventoPrincipal,
                profesorDetalle.getProfesor().getId());
        List<Long> eventoIds = eventos.stream().map(e -> e.getId()).collect(Collectors.toList());

        profesorDAO.updateDetalleManualByItemsId(eventoIds, profesorDetalle.getProfesor().getId(),
                profesorDetalle.getDetalleManual());
        profesorDAO.borraItemDetallesProfesorByEventosId(eventoIds,
                profesorDetalle.getProfesor().getId());
        if (profesorDetalle.getDetalleManual())
        {
            profesorDAO.insertaItemDetallesProfesor(fechasSeleccionadasPorEvento);
        }
    }

    private void actualizaCreditos(ProfesorDetalle profesorDetalle, Evento eventoPrincipal)
    {
        List<Evento> eventos = eventosDAO.getEventosDelMismoGrupoIncluyendoAnuales(eventoPrincipal);
        List<Long> eventosIncluyendoAnualesIds = new ArrayList<>();
        for (Evento evento : eventos)
        {
            eventosIncluyendoAnualesIds.add(evento.getId());
        }
        profesorDAO.updateCreditosByItemsId(eventosIncluyendoAnualesIds,
                profesorDetalle.getProfesor().getId(), profesorDetalle.getCreditos(),
                profesorDetalle.getCreditosComputables());
    }

    public void asignaCreditos(Long itemId, Long profesorId, BigDecimal creditosProfesor,
            BigDecimal creditosComputablesProfesor) throws Exception
    {
        Evento eventoPrincipal = eventosDAO.getEventoById(itemId);

        Asignatura asignatura = eventoPrincipal.getAsignaturas().get(0);
        if (asignatura.isMaster() && creditosProfesor != null
                && creditosComputablesProfesor == null)
        {
            throw new Exception(
                    "No es poden especificar crèdits del professor sense especificar els crèdits computables per a les assignatures de màster");
        }

        if (creditosProfesor != null && ((creditosProfesor.signum() <= 0)
                || (creditosProfesor.doubleValue() > eventoPrincipal.getCreditos().doubleValue())))
        {
            throw new Exception(
                    "El nombre de crèdits assignats al professor no pot superar el nombre total de crèdits de la assignatura i ha de ser major de zero");
        }

        if (eventoPrincipal.getCreditosComputables() == null && creditosComputablesProfesor != null)
        {
            throw new Exception(
                    "No es poden especificar crèdits computables quan el subgrup no te assignats crèdits computables");
        }

        if (eventoPrincipal.getCreditosComputables() != null && creditosComputablesProfesor != null
                && ((creditosComputablesProfesor.doubleValue() < 0) || (creditosComputablesProfesor
                        .doubleValue() > eventoPrincipal.getCreditosComputables().doubleValue())))
        {
            throw new Exception(
                    "El nombre de crèdits computables al professor no pot superar el nombre total de crèdits computables del subgrup i ha de ser major de zero");
        }

        if (creditosProfesor != null && creditosComputablesProfesor != null
                && creditosComputablesProfesor.doubleValue() > creditosProfesor.doubleValue())
        {
            throw new Exception(
                    "El nombre de crèdits computables al professor no pot superar el nombre de crèdits del professor en el subgrup");
        }

        List<Evento> eventos = eventosDAO.getEventosDelMismoGrupoIncluyendoAnuales(eventoPrincipal);
        for (Evento evento : eventos)
        {
            profesorDAO.updateCreditosEventoByProfesorId(evento.getId(), profesorId,
                    creditosProfesor, creditosComputablesProfesor);
        }
    }

    public ProfesorDetalle getProfesorDetalleByItemIdYProfesorId(Long itemId, Long profesorId,
            Long connectedUserId) throws RegistroNoEncontradoException
    {
        return profesorDAO.getProfesorDetalleByItemIdYProfesorId(itemId, profesorId);
    }

    public List<Profesor> getProfesoresMiPODByAreaId(Long areaId, Long connectedUserId)
            throws RegistroNoEncontradoException
    {
        if (personaDAO.esAdmin(connectedUserId))
        {
            return profesorDAO.getProfesoresYCreditosRestantesByAreaIdAndPersonaId(areaId);
        }

        List<Cargo> listaCargos = personaDAO.getCargosByPersonaId(connectedUserId);
        for (Cargo cargo : listaCargos)
        {
            if (cargo.gestionaPOD())
            {
                return profesorDAO.getProfesoresYCreditosRestantesByAreaIdAndPersonaId(areaId);
            }
        }

        List<Profesor> listaProfesor = new ArrayList<Profesor>();
        listaProfesor.add(profesorDAO.getProfesorById(connectedUserId));
        return listaProfesor;
    }

    public Profesor getProfesorById(Long connectedUserId) throws RegistroNoEncontradoException
    {
        return profesorDAO.getProfesorById(connectedUserId);
    }
}
