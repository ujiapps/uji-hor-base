package es.uji.apps.hor.services;

import java.util.List;

import es.uji.commons.rest.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.hor.dao.PersonaDAO;
import es.uji.apps.hor.dao.SemestresDetalleEstudioDAO;
import es.uji.apps.hor.model.SemestreDetalleEstudio;
import es.uji.commons.rest.ResponseMessage;

@Service
public class SemestresDetalleEstudioService
{

    private final SemestresDetalleEstudioDAO semestresDetalleEstudioDAO;

    @Autowired
    private PersonaDAO personaDAO;

    @Autowired
    public SemestresDetalleEstudioService(SemestresDetalleEstudioDAO semestresDetalleEstudioDAO)
    {
        this.semestresDetalleEstudioDAO = semestresDetalleEstudioDAO;
    }

    @Role({ "ADMIN", "USUARIO" })
    public SemestreDetalleEstudio getSemestresDetallesById(Long semestreDetalleEstudioId,
            Long connectedUserId)
    {
        return semestresDetalleEstudioDAO.getSemestresDetalleEstudioById(semestreDetalleEstudioId);
    }

    @Role({ "ADMIN", "USUARIO" })
    public List<SemestreDetalleEstudio> getSemestresDetallesTodos(Long connectedUserId)
    {
        return semestresDetalleEstudioDAO.getSemestresDetalleEstudios();
    }

    @Role({ "ADMIN" })
    public SemestreDetalleEstudio insert(SemestreDetalleEstudio semestreDetalleEstudio,
            Long connectedUserId)
    {
        return semestresDetalleEstudioDAO.insert(semestreDetalleEstudio);
    }

    @Role({ "ADMIN" })
    public SemestreDetalleEstudio update(SemestreDetalleEstudio semestreDetalleEstudio,
            Long connectedUserId)
    {
        return semestresDetalleEstudioDAO.update(semestreDetalleEstudio);
    }

    @Role({ "ADMIN" })
    public ResponseMessage delete(Long semestreDetalleEstudioId, Long connectedUserId)
    {
        semestresDetalleEstudioDAO.delete(semestreDetalleEstudioId);
        return new ResponseMessage(true);
    }
}
