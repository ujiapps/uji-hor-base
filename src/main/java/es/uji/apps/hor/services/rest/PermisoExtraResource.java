package es.uji.apps.hor.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.hor.model.*;
import es.uji.apps.hor.services.PermisoExtraService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Path("permisoExtra")
public class PermisoExtraResource extends CoreBaseService
{
    @InjectParam
    private PermisoExtraService permisoExtraService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getPermisosExtra() throws RegistroNoEncontradoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<PermisoExtra> listaPermisosExtra = permisoExtraService
                .getPermisosExtra(connectedUserId);

        List<UIEntity> listaUIEntity = new ArrayList<UIEntity>();

        for (PermisoExtra permiso : listaPermisosExtra)
        {
            UIEntity entity = UIEntity.toUI(permiso);
            entity.put("persona", permiso.getPersona().getNombre());
            entity.put("tipoCargo", permiso.getCargo().getNombre());
            if (permiso.getEstudio() != null)
            {
                entity.put("estudio", permiso.getEstudio().getNombre());
            }
            if (permiso.getDepartamento() != null)
            {
                entity.put("departamento", permiso.getDepartamento().getNombre());
            }
            if (permiso.getArea() != null)
            {
                entity.put("area", permiso.getArea().getNombre());
            }

            if (permiso.getCentro() != null)
            {
                entity.put("centro", permiso.getCentro().getNombre());
            }

            listaUIEntity.add(entity);
        }

        return listaUIEntity;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<UIEntity> anyadePermisoExtra(UIEntity entity) throws RegistroNoEncontradoException,
            UnauthorizedUserException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        ParamUtils.checkNotNull(entity.get("personaId"), entity.get("tipoCargoId"));

        PermisoExtra permisoExtra = uiToModel(entity);

        PermisoExtra nuevoPermiso = permisoExtraService.addPermisosExtra(permisoExtra, connectedUserId);

        return Collections.singletonList(UIEntity.toUI(nuevoPermiso));
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public void deletePermisosExtra(@PathParam("id") String id, UIEntity entity)
            throws RegistroNoEncontradoException, UnauthorizedUserException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        ParamUtils.checkNotNull(id);

        Long permisoExtraId = ParamUtils.parseLong(id);
        PermisoExtra permisoExtra = permisoExtraService.getPermisoExtraById(permisoExtraId,
                connectedUserId);
        permisoExtraService.deletePermiso(permisoExtra, connectedUserId);
    }

    public PermisoExtra uiToModel(UIEntity entity)
    {
        PermisoExtra permisoExtra = new PermisoExtra();

        Long personaId = ParamUtils.parseLong(entity.get("personaId"));
        Persona persona = new Persona();
        persona.setId(personaId);
        permisoExtra.setPersona(persona);

        Long tipoCargoId = ParamUtils.parseLong(entity.get("tipoCargoId"));
        Cargo cargo = new Cargo();
        cargo.setId(tipoCargoId);
        permisoExtra.setCargo(cargo);

        Long estudioId = ParamUtils.parseLong(entity.get("estudioId"));
        if (estudioId != null)
        {
            Estudio estudio = new Estudio();
            estudio.setId(estudioId);
            permisoExtra.setEstudio(estudio);
        }

        Long departamentoId = ParamUtils.parseLong(entity.get("departamentoId"));
        if (departamentoId != null)
        {
            Departamento departamento = new Departamento();
            departamento.setId(departamentoId);
            permisoExtra.setDepartamento(departamento);
        }

        Long areaId = ParamUtils.parseLong(entity.get("areaId"));
        if (areaId != null)
        {
            Area area = new Area();
            area.setId(areaId);
            permisoExtra.setArea(area);
        }

        Long centroId = ParamUtils.parseLong(entity.get("centroId"));
        if (centroId != null)
        {
            Centro centro = new Centro();
            centro.setId(centroId);
            permisoExtra.setCentro(centro);
        }

        return permisoExtra;
    }

}