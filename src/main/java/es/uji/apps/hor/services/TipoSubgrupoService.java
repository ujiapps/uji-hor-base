package es.uji.apps.hor.services;

import es.uji.apps.hor.dao.PersonaDAO;
import es.uji.apps.hor.dao.TipoSubgruposDAO;
import es.uji.apps.hor.model.TipoSubgrupoUI;
import es.uji.commons.rest.Role;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TipoSubgrupoService
{
    @Autowired
    private PersonaDAO personaDAO;

    @Autowired
    private TipoSubgruposDAO tipoSubgruposDAO;

    @Role({ "ADMIN", "USUARIO" })
    public List<TipoSubgrupoUI> getSubGruposAsignatura(String asignaturaId, Long semestreId, Long connectedUserId)
            throws UnauthorizedUserException, RegistroNoEncontradoException
    {
        // TODO : Comprobar permisos
        /*if (!personaDAO.esAdmin(connectedUserId))
        {
            Persona persona = personaDAO.getPersonaConTitulacionesYCentrosById(connectedUserId);
            persona.compruebaAccesoAEstudio(estudioId);
        }*/

        return tipoSubgruposDAO.getSubGruposAsignatura(asignaturaId, semestreId);
    }
}
