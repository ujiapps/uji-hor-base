package es.uji.apps.hor.services;

import es.uji.apps.hor.dao.CalendarioAcademicoDAO;
import es.uji.apps.hor.model.CalendarioAcademico;
import es.uji.commons.rest.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class CalendarioAcademicoService
{
    private SimpleDateFormat formateadorFecha = new SimpleDateFormat("dd/MM/yyyy");
    private final CalendarioAcademicoDAO calendarioAcademicoDAO;

    @Autowired
    public CalendarioAcademicoService(CalendarioAcademicoDAO calendarioAcademicoDAO)
    {
        this.calendarioAcademicoDAO = calendarioAcademicoDAO;
    }

    @Role({ "ADMIN", "USUARIO" })
    public List<CalendarioAcademico> getCalendarioAcademicoNoLectivos(String tipoEstudioId, String fechaInicio,
            String fechaFin, Long connectedUserId) throws ParseException
    {
        return calendarioAcademicoDAO.getCalendarioAcademicoNoLectivos(tipoEstudioId,
                formateadorFecha.parse(fechaInicio), formateadorFecha.parse(fechaFin));
    }

    @Role({ "ADMIN", "USUARIO" })
    public List<CalendarioAcademico> getCalendarioAcademicoNoLectivos(String tipoEstudioId, Date fechaInicio,
            Date fechaFin, Long connectedUserId) throws ParseException
    {
        return calendarioAcademicoDAO.getCalendarioAcademicoNoLectivos(tipoEstudioId, fechaInicio, fechaFin);
    }

    @Role({ "ADMIN", "USUARIO" })
    public List<CalendarioAcademico> getCalendarioAcademicoFestivos(String tipoEstudioId,Date fechaInicio,
            Date fechaFin, Long connectedUserId) throws ParseException
    {
        return calendarioAcademicoDAO.getCalendarioAcademicoFestivos(tipoEstudioId, fechaInicio, fechaFin);
    }
}