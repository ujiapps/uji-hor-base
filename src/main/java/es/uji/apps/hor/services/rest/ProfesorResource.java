package es.uji.apps.hor.services.rest;

import javax.ws.rs.*;
import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.hor.model.Evento;
import es.uji.apps.hor.model.Profesor;
import es.uji.apps.hor.model.ProfesorDetalle;
import es.uji.apps.hor.services.ProfesorService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Path("profesor")
public class ProfesorResource extends CoreBaseService
{
    private static final String ITEM_ID_QUERY_PARAM = "itemId";
    private static final String PROFESOR_ID_QUERY_PARAM = "profesorId";

    @InjectParam
    private ProfesorService profesorService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getProgesoresByArea(@QueryParam("areaId") String areaId)
            throws RegistroNoEncontradoException
    {
        ParamUtils.checkNotNull(areaId);

        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<Profesor> profesores = profesorService.getProfesoresConCreditosRestantesByAreaId(
                ParamUtils.parseLong(areaId), connectedUserId);

        return UIEntity.toUI(profesores);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("mipod")
    public List<UIEntity> getProgesoresMiPodByArea(@QueryParam("areaId") String areaId)
            throws RegistroNoEncontradoException
    {
        ParamUtils.checkNotNull(areaId);

        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<Profesor> profesores = profesorService.getProfesoresMiPODByAreaId(
                ParamUtils.parseLong(areaId), connectedUserId);

        return UIEntity.toUI(profesores);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("detalle")
    public UIEntity getProfesorDetalleByItemIdYProfesorId(@QueryParam("itemId") Long itemId,
            @QueryParam("profesorId") Long profesorId) throws RegistroNoEncontradoException
    {
        ParamUtils.checkNotNull(itemId, profesorId);

        Long connectedUserId = AccessManager.getConnectedUserId(request);

        ProfesorDetalle profesorDetalle = profesorService.getProfesorDetalleByItemIdYProfesorId(
                itemId, profesorId, connectedUserId);

        return UIEntity.toUI(profesorDetalle);
    }

    @PUT
    @Path("detalle")
    public Response asignaCreditos(UIEntity entity) throws Exception
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        Long itemId = entity.getLong("itemId");
        Long profesorId = entity.getLong("profesorId");
        BigDecimal creditos = getCreditos(entity.get("creditos"));
        BigDecimal creditosComputables = getCreditos(entity.get("creditosComputables"));

        profesorService.asignaCreditos(itemId, profesorId, creditos, creditosComputables);
        return Response.ok().build();
    }

    protected BigDecimal getCreditos(String creditosAsString)
    {
        return (creditosAsString != null && !creditosAsString.isEmpty()) ? new BigDecimal(
                ParamUtils.parseFloat(creditosAsString)).setScale(2, BigDecimal.ROUND_HALF_UP)
                : null;
    }

    @POST
    @Path("clase")
    public Response addClaseProfesor(UIEntity entity) throws RegistroNoEncontradoException,
            UnauthorizedUserException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Long profesorId = entity.getLong("profesorId");
        Long itemId = entity.getLong("itemId");
        ParamUtils.checkNotNull(itemId, profesorId);

        profesorService.asociaClase(itemId, profesorId, connectedUserId);
        return Response.ok().build();
    }

    @DELETE
    @Path("clase")
    public Response removeClaseProfesor(UIEntity entity) throws RegistroNoEncontradoException,
            UnauthorizedUserException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Long profesorId = entity.getLong("profesorId");
        Long itemId = entity.getLong("itemId");
        ParamUtils.checkNotNull(itemId, profesorId);

        profesorService.desasociaClase(itemId, profesorId, connectedUserId);
        return Response.ok().build();
    }

    @POST
    @Path("sincronizaitemsdetalle")
    public Response sincronizaItemsDetalle(UIEntity entity) throws Exception
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        ProfesorDetalle profesorDetalle = UIToProfesorDetalle(entity);
        String fechas = entity.get("fechas");
        HashMap<Long, List> fechasSeleccionadasPorEvento = deserializaFechas(fechas);

        profesorService.sincronizaItemsDetalle(profesorDetalle, fechasSeleccionadasPorEvento);
        return Response.ok().build();
    }

    private HashMap<Long, List> deserializaFechas(String fechasPorItem)
    {
        HashMap<Long, List> fechas = new HashMap<Long, List>();

        String[] eventos = fechasPorItem.split(",");

        for (String evento : eventos)
        {
            evento = evento.trim();
            if (!evento.isEmpty())
            {
                String[] itemProfesorItemDetalle = evento.split(":");
                Long itemProfesor = Long.parseLong(itemProfesorItemDetalle[0]);
                Long itemDetalle = Long.parseLong(itemProfesorItemDetalle[1]);

                List<Long> itemsDetalleProfesor = fechas.get(itemProfesor);
                if (itemsDetalleProfesor == null)
                {
                    itemsDetalleProfesor = new ArrayList<>();
                    fechas.put(itemProfesor, itemsDetalleProfesor);
                }
                itemsDetalleProfesor.add(itemDetalle);
            }
        }
        return fechas;
    }

    private ProfesorDetalle UIToProfesorDetalle(UIEntity entity)
    {
        ProfesorDetalle profesorDetalle = new ProfesorDetalle();

        Profesor profesor = new Profesor();
        profesor.setId(entity.getLong("profesorId"));
        profesorDetalle.setProfesor(profesor);

        Evento evento = new Evento();
        evento.setId(entity.getLong("itemId"));
        profesorDetalle.setEvento(evento);

        if (entity.get("creditos") != null && !entity.get("creditos").isEmpty())
        {
            profesorDetalle.setCreditos(new BigDecimal(entity.get("creditos")));
        }
        else if (entity.get("creditos") == null)
        {
            profesorDetalle.setCreditos(null);
        }

        if (entity.get("creditosComputables") != null && !entity.get("creditosComputables").isEmpty())
        {
            profesorDetalle.setCreditosComputables(new BigDecimal(entity.get("creditosComputables")));
        }
        else if (entity.get("creditosComputables") == null)
        {
            profesorDetalle.setCreditosComputables(null);
        }

        profesorDetalle.setDetalleManual(entity.getBoolean("detalleManual"));

        return profesorDetalle;
    }

}
