package es.uji.apps.hor.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.hor.dao.EstudiosDAO;
import es.uji.apps.hor.dao.GrupoAsignaturaDAO;
import es.uji.apps.hor.dao.PersonaDAO;
import es.uji.apps.hor.dao.RangoHorarioDAO;
import es.uji.apps.hor.model.GrupoAsignatura;
import es.uji.apps.hor.model.Persona;
import es.uji.apps.hor.model.RangoHorario;
import es.uji.commons.rest.Role;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;

@Service
public class GruposAsignaturasService
{
    @Autowired
    private PersonaDAO personaDAO;

    private final GrupoAsignaturaDAO grupoAsignaturaDAO;

    private final RangoHorarioDAO rangoHorarioDAO;

    @Autowired
    private EstudiosDAO estudioDAO;

    @Autowired
    public GruposAsignaturasService(GrupoAsignaturaDAO grupoAsignaturaDAO,
            RangoHorarioDAO rangoHorarioDAO)
    {
        this.grupoAsignaturaDAO = grupoAsignaturaDAO;
        this.rangoHorarioDAO = rangoHorarioDAO;
    }

    @Role({"ADMIN", "USUARIO"})
    public List<GrupoAsignatura> getGruposAsignaturasSinAsignar(Long estudioId, Long cursoId,
            Long semestreId, List<String> gruposIds, List<Long> calendariosIds,
            List<String> asignaturasIds, Long connectedUserId) throws UnauthorizedUserException,
            RegistroNoEncontradoException
    {
        if (!personaDAO.esAdmin(connectedUserId) && !estudioDAO.isEstudioAbiertoConsulta(estudioId))
        {
            Persona persona = personaDAO.getPersonaConTitulacionesYCentrosById(connectedUserId);
            persona.compruebaAccesoAEstudio(estudioId);
        }

        return grupoAsignaturaDAO.getGruposAsignaturasSinAsignar(estudioId, cursoId, semestreId,
                gruposIds, calendariosIds, asignaturasIds);
    }

    @Role({"ADMIN", "USUARIO"})
    public GrupoAsignatura planificaGrupoAsignaturaSinAsignar(Long grupoAsignaturaId,
            Long estudioId, Long cursoId, Long semestreId, Long connectedUserId)
            throws RegistroNoEncontradoException, UnauthorizedUserException
    {
        if (!personaDAO.esAdmin(connectedUserId))
        {
            Persona persona = personaDAO.getPersonaConTitulacionesYCentrosById(connectedUserId);
            persona.compruebaAccesoAEstudio(estudioId);
        }

        GrupoAsignatura grupoAsignatura = grupoAsignaturaDAO.getGrupoAsignaturaById(
                grupoAsignaturaId, estudioId);

        try
        {
            RangoHorario rangoHorario = rangoHorarioDAO.getRangoHorario(estudioId, cursoId,
                    semestreId, grupoAsignatura.getGrupoId());

            grupoAsignatura.planificaGrupoAsignaturaSinAsignar(rangoHorario.getHoraInicio());
        }
        catch (RegistroNoEncontradoException e)
        {
            grupoAsignatura.planificaGrupoAsignaturaSinAsignar();
        }
        grupoAsignaturaDAO.updateGrupoAsignaturaPlanificado(grupoAsignatura);

        return grupoAsignatura;
    }

    @Role({"ADMIN", "USUARIO"})
    public GrupoAsignatura planificaGrupoAsignaturaSinAsignarHorarioPorDefecto(
            Long grupoAsignaturaId, Long estudioId, Long connectedUserId)
            throws RegistroNoEncontradoException, UnauthorizedUserException
    {
        if (!personaDAO.esAdmin(connectedUserId))
        {
            Persona persona = personaDAO.getPersonaConTitulacionesYCentrosById(connectedUserId);
            persona.compruebaAccesoAEstudio(estudioId);
        }

        GrupoAsignatura grupoAsignatura = grupoAsignaturaDAO.getGrupoAsignaturaById(
                grupoAsignaturaId, estudioId);

        grupoAsignatura.planificaGrupoAsignaturaSinAsignar();
        grupoAsignaturaDAO.updateGrupoAsignaturaPlanificado(grupoAsignatura);

        return grupoAsignatura;
    }

    public List<GrupoAsignatura> getGruposAsignaturasSinAsignarByAgrupacion(Long estudioId,
            Long agrupacionId, Long semestreId, List<String> gruposIds, List<Long> calendariosIds,
            List<String> asignaturasIds, Long connectedUserId)
            throws RegistroNoEncontradoException, UnauthorizedUserException
    {
        if (!personaDAO.esAdmin(connectedUserId) && !estudioDAO.isEstudioAbiertoConsulta(estudioId))
        {
            Persona persona = personaDAO.getPersonaConTitulacionesYCentrosById(connectedUserId);
            persona.compruebaAccesoAEstudio(estudioId);
        }

        return grupoAsignaturaDAO.getGruposAsignaturasSinAsignarByAgrupacion(estudioId,
                agrupacionId, semestreId, gruposIds, calendariosIds, asignaturasIds);
    }
}
