package es.uji.apps.hor.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.hor.model.SemestreDetalle;
import es.uji.apps.hor.services.SemestresDetalleService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("semestredetalle")
public class SemestreDetalleResource extends CoreBaseService
{

    @InjectParam
    private SemestresDetalleService consultaSemestresDetalle;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getSemestresDetailTodos()
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<SemestreDetalle> semestresDetalles = consultaSemestresDetalle
                .getSemestresDetallesTodos(connectedUserId);

        List<UIEntity> listaResultados = new ArrayList<UIEntity>();

        for (SemestreDetalle semestreDetalle : semestresDetalles)
        {
            UIEntity resultado = UIEntity.toUI(semestreDetalle);
            resultado.put("nombreSemestre", semestreDetalle.getSemestre().getNombre());
            resultado.put("nombreTipoEstudio", semestreDetalle.getTipoEstudio().getNombre());
            listaResultados.add(resultado);
        }

        return listaResultados;
    }

    @GET
    @Path("estudio/{estudioId}/semestre/{semestreId}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getSemestresDetallePorTipoEstudio(@PathParam("estudioId") Long estudioId,
            @PathParam("semestreId") String semestreId, @QueryParam("cursoId") Long cursoId)
            throws NumberFormatException, UnauthorizedUserException, RegistroNoEncontradoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<SemestreDetalle> semestresDetalles = new ArrayList<>();

        if (cursoId != null)
        {
            semestresDetalles = consultaSemestresDetalle
                    .getSemestresDetallesPorEstudioIdYSemestreIdYCursoId(estudioId,
                            Long.parseLong(semestreId), cursoId, connectedUserId);
        }
        else
        {
            semestresDetalles = consultaSemestresDetalle
                    .getRangoFechasSemestresDetallesPorEstudioIdYSemestreId(estudioId,
                            Long.parseLong(semestreId), connectedUserId);
        }

        List<UIEntity> listaResultados = new ArrayList<UIEntity>();

        for (SemestreDetalle semestreDetalle : semestresDetalles)
        {
            UIEntity resultado = UIEntity.toUI(semestreDetalle);
            resultado.put("nombreSemestre", semestreDetalle.getSemestre().getNombre());
            resultado.put("nombreTipoEstudio", semestreDetalle.getTipoEstudio().getNombre());
            listaResultados.add(resultado);
        }

        return listaResultados;
    }

}
