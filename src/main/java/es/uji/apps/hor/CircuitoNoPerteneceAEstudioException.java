package es.uji.apps.hor;

@SuppressWarnings("serial")
public class CircuitoNoPerteneceAEstudioException extends GeneralHORException
{
    public CircuitoNoPerteneceAEstudioException()
    {
        super("El circuit no pertany a l'estudi del event seleccionat");
    }
}
