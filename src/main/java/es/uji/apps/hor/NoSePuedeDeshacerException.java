package es.uji.apps.hor;

@SuppressWarnings("serial")
public class NoSePuedeDeshacerException extends GeneralHORException
{
    public NoSePuedeDeshacerException(String msg)
    {
        super(msg);
    }

    public NoSePuedeDeshacerException()
    {
        super("El canvi no es pot desfer");
    }
}
