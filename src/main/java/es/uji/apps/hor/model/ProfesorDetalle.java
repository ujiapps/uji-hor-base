package es.uji.apps.hor.model;

import java.math.BigDecimal;
import java.util.List;

public class ProfesorDetalle
{
    private Long id;
    private Evento evento;
    private Profesor profesor;
    private Boolean detalleManual;
    private BigDecimal creditos;
    private BigDecimal creditosComputables;
    private List<EventoDetalle> eventosDetalle;
    private BigDecimal creditosCalculados;

    public ProfesorDetalle()
    {

    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Evento getEvento()
    {
        return evento;
    }

    public void setEvento(Evento evento)
    {
        this.evento = evento;
    }

    public Profesor getProfesor()
    {
        return profesor;
    }

    public void setProfesor(Profesor profesor)
    {
        this.profesor = profesor;
    }

    public Boolean getDetalleManual()
    {
        return detalleManual;
    }

    public void setDetalleManual(Boolean detalleManual)
    {
        this.detalleManual = detalleManual;
    }

    public BigDecimal getCreditos()
    {
        return creditos;
    }

    public void setCreditos(BigDecimal creditos)
    {
        this.creditos = creditos;
    }

    public BigDecimal getCreditosComputables()
    {
        return creditosComputables;
    }

    public void setCreditosComputables(BigDecimal creditosComputables)
    {
        this.creditosComputables = creditosComputables;
    }

    public List<EventoDetalle> getEventosDetalle()
    {
        return eventosDetalle;
    }

    public void setEventosDetalle(List<EventoDetalle> eventosDetalle)
    {
        this.eventosDetalle = eventosDetalle;
    }

    public BigDecimal getCreditosCalculados()
    {
        return creditosCalculados;
    }

    public void setCreditosCalculados(BigDecimal creditosCalculados)
    {
        this.creditosCalculados = creditosCalculados;
    }

    public ProfesorDetalle clonar()
    {
        ProfesorDetalle nuevoProfesorDetalle = new ProfesorDetalle();
        nuevoProfesorDetalle.setDetalleManual(this.getDetalleManual());
        nuevoProfesorDetalle.setCreditos(this.getCreditos());
        nuevoProfesorDetalle.setCreditosComputables(this.getCreditosComputables());
        nuevoProfesorDetalle.setProfesor(this.getProfesor());
        nuevoProfesorDetalle.setEvento(this.getEvento());
        return nuevoProfesorDetalle;
    }
}
