package es.uji.apps.hor.model;

import java.util.Calendar;
import java.util.Date;

import es.uji.apps.hor.DiaNoLectivoException;
import es.uji.apps.hor.DiaNoValidoExamenException;

public class CalendarioAcademico
{
    private Long id;

    private Long dia;

    private Long mes;

    private Long anyo;

    private String diaSemana;

    private Long diaSemanaId;

    private String tipoDia;

    private Date fecha;

    private Long vacaciones;

    final private static String DIA_EXAMENES = "E";

    final private static String DIA_LECTIVO = "L";

    final private static String DIA_FESTIVO = "F";

    final private static String DIA_NO_LECTIVO = "N";

    final private static Long SABADO = new Long(6);

    final private static Long DOMINGO = new Long(7);

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getDia()
    {
        return dia;
    }

    public void setDia(Long dia)
    {
        this.dia = dia;
    }

    public Long getMes()
    {
        return mes;
    }

    public void setMes(Long mes)
    {
        this.mes = mes;
    }

    public Long getAnyo()
    {
        return anyo;
    }

    public void setAnyo(Long anyo)
    {
        this.anyo = anyo;
    }

    public String getDiaSemana()
    {
        return diaSemana;
    }

    public void setDiaSemana(String diaSemana)
    {
        this.diaSemana = diaSemana;
    }

    public Long getDiaSemanaId()
    {
        return diaSemanaId;
    }

    public void setDiaSemanaId(Long diaSemanaId)
    {
        this.diaSemanaId = diaSemanaId;
    }

    public String getTipoDia()
    {
        return tipoDia;
    }

    public void setTipoDia(String tipoDia)
    {
        this.tipoDia = tipoDia;
    }

    public Date getFecha()
    {
        return fecha;
    }

    public void setFecha(Date fecha)
    {
        this.fecha = fecha;
    }

    public Long getVacaciones()
    {
        return vacaciones;
    }

    public void setVacaciones(Long vacaciones)
    {
        this.vacaciones = vacaciones;
    }

    public void compruebaDiaLectivo() throws DiaNoLectivoException
    {
        if (!(tipoDia.equals(DIA_LECTIVO) || (tipoDia.equals(DIA_EXAMENES) && !diaSemanaId
                .equals(DOMINGO))))
        {
            throw new DiaNoLectivoException();
        }
    }

    public void compruebaDiaValidoExamen(String tipoEstudioId, Boolean permiteSabados) throws DiaNoValidoExamenException
    {
        if (tipoEstudioId.equals(TipoEstudio.GRADO) && diaSemanaId.equals(SABADO) && !permiteSabados)
        {
            throw new DiaNoValidoExamenException("No es permet planificar exàmens en dissabte en titulacions de grau");
        }

        if ((tipoDia.equals(DIA_FESTIVO) || diaSemanaId.equals(DOMINGO)))
        {
            throw new DiaNoValidoExamenException("No es permet planificar exàmens en dies festius");
        }

        if (tipoDia.equals(DIA_NO_LECTIVO) && !(tipoEstudioId.equals(TipoEstudio.GRADO) && diaSemanaId.equals(SABADO) && permiteSabados))
        {
            throw new DiaNoValidoExamenException("No es permet planificar exàmens en dies no lectius");
        }
    }

    public static Date getFechaSinHoraEstablecida(Date fecha)
    {
        Calendar calendarioOrig = Calendar.getInstance();
        calendarioOrig.setTime(fecha);

        Calendar calendario = Calendar.getInstance();
        calendario.set(calendarioOrig.get(Calendar.YEAR), calendarioOrig.get(Calendar.MONTH),
                calendarioOrig.get(Calendar.DAY_OF_MONTH));

        return calendario.getTime();
    }
}
