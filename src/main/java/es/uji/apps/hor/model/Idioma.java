package es.uji.apps.hor.model;

public class Idioma
{
    private Long id;
    private String nombre;
    private String codigoPop;
    private String acronimo;
    private Boolean activo;

    public Idioma()
    {
        activo = false;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getCodigoPop()
    {
        return codigoPop;
    }

    public void setCodigoPop(String codigoPop)
    {
        this.codigoPop = codigoPop;
    }

    public Boolean isActivo()
    {
        return activo;
    }

    public void setActivo(Boolean activo)
    {
        this.activo = activo;
    }

    public String getAcronimo() {
        return acronimo;
    }

    public void setAcronimo(String acronimo) {
        this.acronimo = acronimo;
    }
}
