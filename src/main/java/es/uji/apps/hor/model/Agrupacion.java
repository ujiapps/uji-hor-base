package es.uji.apps.hor.model;

import java.util.List;

import es.uji.apps.hor.AgrupacionNoPerteneceAEstudioException;

public class Agrupacion
{
    private Long id;
    private String nombre;
    private List<Asignatura> asignaturas;
    private List<Estudio> estudios;

    public Agrupacion()
    {

    }

    public Agrupacion(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public List<Asignatura> getAsignaturas()
    {
        return asignaturas;
    }

    public void setAsignaturas(List<Asignatura> asignaturas)
    {
        this.asignaturas = asignaturas;
    }

    public List<Estudio> getEstudios()
    {
        return estudios;
    }

    public void setEstudios(List<Estudio> estudios)
    {
        this.estudios = estudios;
    }

    public void compruebaEstudioEnAgrupacion(Long estudioId)
            throws AgrupacionNoPerteneceAEstudioException
    {
        Estudio estudio = new Estudio();
        estudio.setId(estudioId);

        if (!this.estudios.contains(estudio))
        {
            throw new AgrupacionNoPerteneceAEstudioException();
        }
    }
}
