package es.uji.apps.hor.model;

public class Subgrupo
{
    private String grupoId;
    private Long subgrupoId;
    private String tipoSubgrupoId;
    private String tipoSubgrupoNombre;

    public String getGrupoId()
    {
        return grupoId;
    }

    public void setGrupoId(String grupoId)
    {
        this.grupoId = grupoId;
    }

    public Long getSubgrupoId()
    {
        return subgrupoId;
    }

    public void setSubgrupoId(Long subgrupoId)
    {
        this.subgrupoId = subgrupoId;
    }

    public String getTipoSubgrupoId()
    {
        return tipoSubgrupoId;
    }

    public void setTipoSubgrupoId(String tipoSubgrupoId)
    {
        this.tipoSubgrupoId = tipoSubgrupoId;
    }

    public String getTipoSubgrupoNombre()
    {
        return tipoSubgrupoNombre;
    }

    public void setTipoSubgrupoNombre(String tipoSubgrupoNombre)
    {
        this.tipoSubgrupoNombre = tipoSubgrupoNombre;
    }
}
