package es.uji.apps.hor.model;

import java.util.ArrayList;
import java.util.List;

public class Cargo {
    private Long id;
    private String nombre;
    private Long estudio;
    private Long departamento;

    private Long centro;
    private Long tipoCargoId;

    public static final Long DIRECTOR_ESTUDIO = new Long(1);
    public static final Long COORDINADOR_CURSO = new Long(2);
    public static final Long DIRECTOR_CENTRO = new Long(3);
    public static final Long PAS_CENTRO = new Long(4);
    public static final Long PAS_DEPARTAMENTO = new Long(5);
    public static final Long DIRECTOR_DEPARTAMENTO = new Long(6);

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getEstudio() {
        return estudio;
    }

    public void setEstudio(Long estudio) {
        this.estudio = estudio;
    }

    public Long getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Long departamento) {
        this.departamento = departamento;
    }

    public Long getCentro()
    {
        return centro;
    }

    public void setCentro(Long centro)
    {
        this.centro = centro;
    }

    public Long getTipoCargoId() {
        return tipoCargoId;
    }

    public void setTipoCargoId(Long tipoCargoId) {
        this.tipoCargoId = tipoCargoId;
    }

    public boolean gestionaPOD()
    {
        List<Long> listaCargosGestinanPOD = new ArrayList<>();
        listaCargosGestinanPOD.add(DIRECTOR_CENTRO);
        listaCargosGestinanPOD.add(PAS_CENTRO);
        listaCargosGestinanPOD.add(PAS_DEPARTAMENTO);
        listaCargosGestinanPOD.add(DIRECTOR_DEPARTAMENTO);

        return listaCargosGestinanPOD.contains((long) id);
    }
}
