package es.uji.apps.hor.model;

import es.uji.commons.sso.exceptions.UnauthorizedUserException;

import java.util.ArrayList;
import java.util.List;

public class Persona
{
    private Long id;
    private String nombre;
    private String nombreBuscar;
    private String email;
    private String actividadId;
    private Departamento departamento;
    private Centro centroAutorizado;
    private List<Estudio> estudiosAutorizados = new ArrayList<Estudio>();
    private List<Cargo> cargos = new ArrayList<Cargo>();
    private List<Departamento> departamentosAutorizados = new ArrayList<Departamento>();
    private List<Area> areasAutorizadas = new ArrayList<Area>();

    public Persona()
    {
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getNombreBuscar()
    {
        return this.nombreBuscar;
    }

    public void setNombreBuscar(String nombreBuscar)
    {
        this.nombreBuscar = nombreBuscar;
    }

    public String getActividadId()
    {
        return actividadId;
    }

    public void setActividadId(String actividadId)
    {
        this.actividadId = actividadId;
    }

    public Departamento getDepartamento()
    {
        return departamento;
    }

    public void setDepartamento(Departamento departamento)
    {
        this.departamento = departamento;
    }

    public List<Estudio> getEstudiosAutorizados()
    {
        return estudiosAutorizados;
    }

    public void setEstudiosAutorizados(List<Estudio> estudios)
    {
        this.estudiosAutorizados = estudios;
    }

    public List<Departamento> getDepartamentosAutorizados()
    {
        return departamentosAutorizados;
    }

    public void setDepartamentosAutorizados(List<Departamento> departamentosAutorizados)
    {
        this.departamentosAutorizados = departamentosAutorizados;
    }

    public List<Area> getAreasAutorizadas()
    {
        return areasAutorizadas;
    }

    public void setAreasAutorizadas(List<Area> areasAutorizadas)
    {
        this.areasAutorizadas = areasAutorizadas;
    }

    public boolean puedeAccederACentro(Centro centro)
    {
        return puedeAccederACentro(centro.getId());
    }

    public boolean puedeAccederACentro(Long centroId)
    {
        Centro centroUsuario = getCentroAutorizado();
        return centroUsuario != null && centroId.equals(centroUsuario.getId());

    }

    public boolean puedeAccederAEstudio(Estudio estudio)
    {
        return puedeAccederAEstudio(estudio.getId());
    }

    public boolean puedeAccederAEstudio(Long estudioId)
    {
        for (Estudio estudioUsuario : getEstudiosAutorizados())
        {
            if (estudioId.equals(estudioUsuario.getId()))
            {
                return true;
            }
        }
        return false;
    }

    public boolean puedeAccederADepartamento(Long departamentoId)
    {
        for (Departamento departamentoUsuario : getDepartamentosAutorizados())
        {
            if (departamentoId.equals(departamentoUsuario.getId()))
            {
                return true;
            }
        }
        return false;
    }

    public Centro getCentroAutorizado()
    {
        return centroAutorizado;
    }

    public void setCentroAutorizado(Centro centro)
    {
        this.centroAutorizado = centro;
    }

    public void compruebaAccesoACentro(Centro centro) throws UnauthorizedUserException
    {
        compruebaAccesoACentro(centro.getId());
    }

    public void compruebaAccesoACentro(Long centroId) throws UnauthorizedUserException
    {
        if (!puedeAccederACentro(centroId))
        {
            throw new UnauthorizedUserException();
        }

    }

    public void compruebaAccesoAEstudio(Estudio estudio) throws UnauthorizedUserException
    {
        compruebaAccesoAEstudio(estudio.getId());
    }

    public void compruebaAccesoAEstudio(Long estudioId) throws UnauthorizedUserException
    {
        if (!puedeAccederAEstudio(estudioId))
        {
            throw new UnauthorizedUserException();
        }

    }

    public void compruebaAccesoAAsignatura(Asignatura asignatura) throws UnauthorizedUserException
    {
        if (!puedeAccederAAsignatura(asignatura))
        {
            throw new UnauthorizedUserException();
        }

    }

    private boolean puedeAccederAAsignatura(Asignatura asignatura)
    {
        for (Area area : this.getDepartamento().getAreas())
        {
            if (area.getId().equals(asignatura.getArea().getId()))
            {
                return true;
            }
        }
        return false;
    }

    public void compruebaAccesoAEvento(Evento evento) throws UnauthorizedUserException
    {
        if (!puedeAccederAEvento(evento))
        {
            throw new UnauthorizedUserException();
        }

    }

    public boolean puedeAccederAEvento(Evento evento)
    {
        for (Asignatura asignatura : evento.getAsignaturas())
        {
            if (puedeAccederAEstudio(asignatura.getEstudio()))
            {
                return true;
            }
        }

        return false;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public List<Cargo> getCargos()
    {
        return cargos;
    }

    public void setCargos(List<Cargo> cargos)
    {
        this.cargos = cargos;
    }

    public void compruebaAsignacionPermisoEstudioId(Long estudioId, Long tipoCargoId)
            throws UnauthorizedUserException
    {
        List<Estudio> listaEstudios = getEstudiosAutorizadosConTipoCargo(tipoCargoId);

        for (Estudio estudio : listaEstudios)
        {
            if (estudio.getId().equals(estudioId))
            {
                return;
            }
        }

        throw new UnauthorizedUserException();
    }

    private List<Estudio> getEstudiosAutorizadosConTipoCargo(Long tipoCargoId)
    {
        // TODO: Comprobar los cargos de los estudios
        return estudiosAutorizados;
    }

    public boolean esGestorDeCentro()
    {
        for (Cargo cargo : cargos)
        {
            if (cargo.getId().equals(Cargo.PAS_CENTRO)
                    || cargo.getId().equals(Cargo.DIRECTOR_CENTRO))
            {
                return true;
            }
        }
        return false;
    }

    public boolean esGestorDeCentro(Long centroId)
    {
        for (Cargo cargo : cargos)
        {
            if (cargo.getId().equals(Cargo.PAS_CENTRO)
                    || cargo.getId().equals(Cargo.DIRECTOR_CENTRO))
            {
                if (this.centroAutorizado.getId().equals(centroId))
                {
                    return true;
                }
            }
        }
        return false;
    }

    public void compruebaPermiso(PermisoExtra permisoExtra) throws UnauthorizedUserException
    {
        if (permisoExtra.getEstudio() != null)
        {
            compruebaAccesoAEstudio(permisoExtra.getEstudio().getId());
        }

        if (permisoExtra.getDepartamento() != null && permisoExtra.getArea() == null)
        {
            compruebaAccesoADepartamentoCompleto(permisoExtra.getDepartamento().getId());
        }

        if (permisoExtra.getDepartamento() != null && permisoExtra.getArea() != null)
        {
            compruebaAccesoADepartamentoArea(permisoExtra.getDepartamento().getId(), permisoExtra
                    .getArea().getId());
        }
    }

    private void compruebaAccesoADepartamentoArea(Long departamentoId, Long areaId)
            throws UnauthorizedUserException
    {
        if (!puedeAccederADepartamento(departamentoId))
        {
            throw new UnauthorizedUserException();
        }

        // TODO : Comprobar el acceso a areas, para ello la vista HOR_EXT_CARGOS_PERSONAS debería
        // mostrar las areas a las que tiene permiso un usuario
    }

    private void compruebaAccesoADepartamentoCompleto(Long departamentoId)
            throws UnauthorizedUserException
    {
        if (!puedeAccederADepartamento(departamentoId))
        {
            throw new UnauthorizedUserException();
        }
    }
}
