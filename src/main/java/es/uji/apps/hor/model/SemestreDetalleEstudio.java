package es.uji.apps.hor.model;

import java.util.Date;

public class SemestreDetalleEstudio
{
    private Long id;
    private SemestreDetalle semestreDetalle;
    private Estudio estudio;
    private Long cursoAcademico;
    private Date fechaInicio = null;
    private Date fechaFin = null;
    private Date fechaExamenesInicio = null;
    private Date fechaExamenesInicioC2 = null;
    private Date fechaExamenesFinC2 = null;
    private Date fechaExamenesFin = null;
    private Long numeroSemanas;

    public SemestreDetalleEstudio(Long id, SemestreDetalle semestreDetalle, Estudio estudio, Long cursoAcademico,
            Date fechaInicio, Date fechaFin, Date fechaExamenesInicio, Date fechaExamenesFin,
            Long numeroSemanas)
    {
        super();
        this.id = id;
        this.semestreDetalle = semestreDetalle;
        this.estudio = estudio;
        this.cursoAcademico = cursoAcademico;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.fechaExamenesInicio = fechaExamenesInicio;
        this.fechaExamenesFin = fechaExamenesFin;
        this.numeroSemanas = numeroSemanas;
    }

    public SemestreDetalleEstudio()
    {
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public SemestreDetalle getSemestreDetalle()
    {
        return semestreDetalle;
    }

    public void setSemestreDetalle(SemestreDetalle semestreDetalle)
    {
        this.semestreDetalle = semestreDetalle;
    }

    public Estudio getEstudio()
    {
        return estudio;
    }

    public void setEstudio(Estudio estudio)
    {
        this.estudio = estudio;
    }

    public Long getCursoAcademico()
    {
        return cursoAcademico;
    }

    public void setCursoAcademico(Long cursoAcademico)
    {
        this.cursoAcademico = cursoAcademico;
    }

    public Date getFechaInicio()
    {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio)
    {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin()
    {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin)
    {
        this.fechaFin = fechaFin;
    }

    public Date getFechaExamenesInicio()
    {
        return fechaExamenesInicio;
    }

    public void setFechaExamenesInicio(Date fechaExamenesInicio)
    {
        this.fechaExamenesInicio = fechaExamenesInicio;
    }

    public Date getFechaExamenesInicioC2()
    {
        return fechaExamenesInicioC2;
    }

    public void setFechaExamenesInicioC2(Date fechaExamenesInicioC2)
    {
        this.fechaExamenesInicioC2 = fechaExamenesInicioC2;
    }

    public Date getFechaExamenesFinC2()
    {
        return fechaExamenesFinC2;
    }

    public void setFechaExamenesFinC2(Date fechaExamenesFinC2)
    {
        this.fechaExamenesFinC2 = fechaExamenesFinC2;
    }

    public Date getFechaExamenesFin()
    {
        return fechaExamenesFin;
    }

    public void setFechaExamenesFin(Date fechaExamenesFin)
    {
        this.fechaExamenesFin = fechaExamenesFin;
    }

    public Long getNumeroSemanas()
    {
        return numeroSemanas;
    }

    public void setNumeroSemanas(Long numeroSemanas)
    {
        this.numeroSemanas = numeroSemanas;
    }
}
