package es.uji.apps.hor.model;

public enum TipoCalendarioAsignatura {
    SP(1L), CP(2L), MP(3L), OP(4L), RA(5L), MPSS(6L);

    private static final int SIN_PROFESOR_ASIGNADO = 1;
    private static final int PROFESORES_ASIGNADOS = 2;
    private static final int MISMO_PROFESOR_ASIGNADO = 3;
    private static final int OCUPACION_PROFESOR = 4;
    private static final int RESTO_AREA = 5;
    private static final int MISMO_PROFESOR_ASIGNADO_SIN_SESIONES = 6;

    private static final int COLOR_SIN_PROFESOR_ASIGNADO = 2;
    private static final int COLOR_PROFESORES_ASIGNADOS = 20;
    private static final int COLOR_MISMO_PROFESOR_ASIGNADO = 17;
    private static final int COLOR_OCUPACION_PROFESOR = 18;
    private static final int COLOR_RESTO_AREA = 32;
    private static final int COLOR_MISMO_PROFESOR_ASIGNADO_SIN_SESIONES = 16;

    private final Long calendarioId;

    TipoCalendarioAsignatura(Long calendarioId)
    {
        this.calendarioId = calendarioId;
    }

    public Long getCalendarioId()
    {
        return this.calendarioId;
    }

    public String getNombre()
    {
        switch (calendarioId.intValue())
        {
        case SIN_PROFESOR_ASIGNADO:
            return "Sense professor assignat";
        case PROFESORES_ASIGNADOS:
            return "Amb professors assignats";
        case MISMO_PROFESOR_ASIGNADO:
            return "Assignat al professor";
        case MISMO_PROFESOR_ASIGNADO_SIN_SESIONES:
            return "Assignat al professor pero sense sessions";
        case OCUPACION_PROFESOR:
            return "Ocupació professor";
        case RESTO_AREA:
            return "Resta de l'àrea";
        default:
            return "";
        }
    }

    public int getColor()
    {
        switch (calendarioId.intValue())
        {
        case SIN_PROFESOR_ASIGNADO:
            return COLOR_SIN_PROFESOR_ASIGNADO;
        case PROFESORES_ASIGNADOS:
            return COLOR_PROFESORES_ASIGNADOS;
        case MISMO_PROFESOR_ASIGNADO:
            return COLOR_MISMO_PROFESOR_ASIGNADO;
        case MISMO_PROFESOR_ASIGNADO_SIN_SESIONES:
            return COLOR_MISMO_PROFESOR_ASIGNADO_SIN_SESIONES;
        case OCUPACION_PROFESOR:
            return COLOR_OCUPACION_PROFESOR;
        case RESTO_AREA:
            return COLOR_RESTO_AREA;
        default:
            return COLOR_MISMO_PROFESOR_ASIGNADO;
        }
    }
}
