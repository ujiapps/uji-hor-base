package es.uji.apps.hor.model;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Examen
{
    private static final int HORA_INICIO_POR_DEFECTO = 8;
    private static final int MINUTO_INICIO_POR_DEFECTO = 0;
    private static final int SEGUNDO_INICIO_POR_DEFECTO = 0;

    private static final int HORA_FIN_POR_DEFECTO = 12;
    private static final int MINUTO_FIN_POR_DEFECTO = 0;
    private static final int SEGUNDO_FIN_POR_DEFECTO = 0;

    private Long id;
    private String nombre;
    private Date fecha;
    private Date horaInicio;
    private Date horaFin;
    private Boolean comun;
    private Boolean permiteSabados;
    private Boolean definitivo;
    private String asignaturasComunes;
    private Convocatoria convocatoria;
    private TipoExamen tipoExamen;
    private String comentario;
    private String comentarioES;
    private String comentarioUK;
    private List<Asignatura> asignaturasExamen;
    private List<Aula> aulasExamen;

    public Examen()
    {
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Date getFecha()
    {
        return fecha;
    }

    public void setFecha(Date fecha)
    {
        this.fecha = fecha;
    }

    public Date getHoraInicio()
    {
        return horaInicio;
    }

    public void setHoraInicio(Date horaInicio)
    {
        this.horaInicio = horaInicio;
    }

    public Date getHoraFin()
    {
        return horaFin;
    }

    public void setHoraFin(Date horaFin)
    {
        this.horaFin = horaFin;
    }

    public Boolean isComun()
    {
        return comun;
    }

    public void setComun(Boolean comun)
    {
        this.comun = comun;
    }

    public Boolean getPermiteSabados()
    {
        return permiteSabados;
    }

    public void setPermiteSabados(Boolean permiteSabados)
    {
        this.permiteSabados = permiteSabados;
    }

    public String getAsignaturasComunes()
    {
        return asignaturasComunes;
    }

    public void setAsignaturasComunes(String asignaturasComunes)
    {
        this.asignaturasComunes = asignaturasComunes;
    }

    public Convocatoria getConvocatoria()
    {
        return convocatoria;
    }

    public void setConvocatoria(Convocatoria convocatoria)
    {
        this.convocatoria = convocatoria;
    }

    public TipoExamen getTipoExamen()
    {
        return tipoExamen;
    }

    public void setTipoExamen(TipoExamen tipoExamen)
    {
        this.tipoExamen = tipoExamen;
    }

    public String getComentario()
    {
        return comentario;
    }

    public void setComentario(String comentario)
    {
        this.comentario = comentario;
    }

    public Boolean getComun()
    {
        return comun;
    }

    public Boolean isDefinitivo()
    {
        return definitivo;
    }

    public void setDefinitivo(Boolean definitivo)
    {
        this.definitivo = definitivo;
    }

    public List<Asignatura> getAsignaturasExamen()
    {
        return asignaturasExamen;
    }

    public void setAsignaturasExamen(List<Asignatura> asignaturasExamen)
    {
        this.asignaturasExamen = asignaturasExamen;
    }

    public List<Aula> getAulasExamen()
    {
        return aulasExamen;
    }

    public void setAulasExamen(List<Aula> aulasExamen)
    {
        this.aulasExamen = aulasExamen;
    }

    public String getComentarioES()
    {
        return comentarioES;
    }

    public void setComentarioES(String comentarioES)
    {
        this.comentarioES = comentarioES;
    }

    public String getComentarioUK()
    {
        return comentarioUK;
    }

    public void setComentarioUK(String comentarioUK)
    {
        this.comentarioUK = comentarioUK;
    }

    public String getNombreCompleto()
    {
        String tieneAulas = (!getAulasExamen().isEmpty()) ? " [Te aules]" : "";
        if (tipoExamen == null)
        {
            return nombre + tieneAulas;
        }
        return nombre + " - " + tipoExamen.getNombre() + tieneAulas;
    }

    public void planifica(Date fecha)
    {
        Calendar calInicio = Calendar.getInstance();
        Calendar calFin = Calendar.getInstance();
        calInicio.set(Calendar.HOUR_OF_DAY, HORA_INICIO_POR_DEFECTO);
        calInicio.set(Calendar.MINUTE, MINUTO_INICIO_POR_DEFECTO);
        calInicio.set(Calendar.SECOND, SEGUNDO_INICIO_POR_DEFECTO);
        calFin.set(Calendar.HOUR_OF_DAY, HORA_FIN_POR_DEFECTO);
        calFin.set(Calendar.MINUTE, MINUTO_FIN_POR_DEFECTO);
        calFin.set(Calendar.SECOND, SEGUNDO_FIN_POR_DEFECTO);

        this.horaInicio = calInicio.getTime();
        this.horaFin = calFin.getTime();
        this.fecha = fecha;
    }

    public void desplanifica()
    {
        this.horaInicio = null;
        this.horaFin = null;
        this.fecha = null;
    }

    public Examen divide()
    {
        Examen nuevoExamen = this.clonar();
        return nuevoExamen;
    }

    private Examen clonar()
    {
        Examen nuevo = new Examen();

        nuevo.setNombre(this.getNombre());
        nuevo.setFecha(this.getFecha());
        nuevo.setHoraInicio(this.getHoraInicio());
        nuevo.setHoraFin(this.getHoraFin());
        nuevo.setComun(this.isComun());
        nuevo.setPermiteSabados(this.getPermiteSabados());
        nuevo.setAsignaturasComunes(this.getAsignaturasComunes());
        nuevo.setConvocatoria(this.getConvocatoria());
        nuevo.setTipoExamen(this.getTipoExamen());
        nuevo.setComentario(this.getComentario());
        nuevo.setComentarioES(this.getComentarioES());
        nuevo.setComentarioUK(this.getComentarioUK());
        nuevo.setDefinitivo(this.isDefinitivo());

        return nuevo;
    }

    public Long getCursoId()
    {
        if (asignaturasExamen == null || asignaturasExamen.isEmpty())
        {
            return null;
        }

        return asignaturasExamen.get(0).getCursoId();
    }
}
