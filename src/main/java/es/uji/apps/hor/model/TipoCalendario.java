package es.uji.apps.hor.model;

public enum TipoCalendario
{
    EC(1L), FC(2L), F(100L);

    private static final int EN_CIRCUITO = 1;
    private static final int FUERA_CIRCUITO = 2;
    private static final int FESTIVOS = 100;

    private static final int COLOR_EN_CIRCUITO = 20;
    private static final int COLOR_FUERA_CIRCUITO = 17;
    private static final int COLOR_FESTIVOS = 2;

    private final Long calendarioId;

    TipoCalendario(Long calendarioId)
    {
        this.calendarioId = calendarioId;
    }

    public Long getCalendarioId()
    {
        return this.calendarioId;
    }

    public String getNombre()
    {
        switch (calendarioId.intValue())
        {
        case EN_CIRCUITO:
            return "En Circuit";
        case FUERA_CIRCUITO:
            return "Fora Circuit";
        case FESTIVOS:
            return "Festius";
        default:
            return "";
        }
    }

    public int getColor()
    {
        switch (calendarioId.intValue())
        {
        case EN_CIRCUITO:
            return COLOR_EN_CIRCUITO;
        case FUERA_CIRCUITO:
            return COLOR_FUERA_CIRCUITO;
        default:
            return COLOR_FESTIVOS;
        }
    }
}
