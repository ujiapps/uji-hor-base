package es.uji.apps.hor.model;

import java.io.Serializable;

public class Resource implements Serializable {
    private int id;
    private String name;
    public String eventColor;
    public String tipoSubrupodId;

    public Resource(int id, String name, String color, String tipoSubrupodId) {
        this.id = id;
        this.name = name;
        this.eventColor = color;
        this.tipoSubrupodId = tipoSubrupodId;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getEventColor() {
        return eventColor;
    }
    public void setEventColor(String color) {
        this.eventColor = color;
    }

    public String getTipoSubrupodId() {
        return tipoSubrupodId;
    }

    public void setTipoSubrupodId(String tipoSubrupodId) {
        this.tipoSubrupodId = tipoSubrupodId;
    }
}







