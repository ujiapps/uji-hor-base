package es.uji.apps.hor.model;

import java.util.ArrayList;
import java.util.List;

public class CalendarioCircuito
{
    private Long id;
    private String titulo;
    private Integer color;

    public CalendarioCircuito()
    {

    }

    public CalendarioCircuito(Long id, String titulo, Integer color)
    {
        this.id = id;
        this.titulo = titulo;
        this.color = color;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getTitulo()
    {
        return titulo;
    }

    public void setTitulo(String titulo)
    {
        this.titulo = titulo;
    }

    public Integer getColor()
    {
        return color;
    }

    public void setColor(Integer color)
    {
        this.color = color;
    }

    public static List<CalendarioCircuito> getCalendariosCircuitos()
    {
        List<CalendarioCircuito> calendarios = new ArrayList<CalendarioCircuito>();

        calendarios.add(CalendarioCircuito.getCalendarioCircuitoFC());
        calendarios.add(CalendarioCircuito.getCalendarioCircuitoEC());

        return calendarios;
    }

    public static CalendarioCircuito getCalendarioCircuitoFC()
    {
        return new CalendarioCircuito(TipoCalendario.FC.getCalendarioId(),
                TipoCalendario.FC.getNombre(), TipoCalendario.FC.getColor());
    }

    public static CalendarioCircuito getCalendarioCircuitoEC()
    {
        return new CalendarioCircuito(TipoCalendario.EC.getCalendarioId(),
                TipoCalendario.EC.getNombre(), TipoCalendario.EC.getColor());
    }
}
