package es.uji.apps.hor.model;

import java.io.Serializable;

public class Assignment implements Serializable {
    private Long id;
    private Long event;
    public Long resource;
    public Assignment(Long id, Long event, Long resource) {
        this.id = id;
        this.event = event;
        this.resource = resource;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getEvent() {
        return event;
    }

    public void setEvent(Long event) {
        this.event = event;
    }

    public Long getResource() {
        return resource;
    }

    public void setResource(Long resource) {
        this.resource = resource;
    }
}
