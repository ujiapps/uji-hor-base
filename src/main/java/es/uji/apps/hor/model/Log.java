package es.uji.apps.hor.model;

import java.util.Calendar;
import java.util.Date;

public class Log
{
    public static final int ACCION_MOVER_ITEM = 1;
    public static final int ACCION_MOVER_ITEM_SEMANA_DETALLE = 2;
    public static final int ACCION_MOVER_ITEM_DETALLE_MANUAL = 3;
    public static final int ACCION_PLANIFICAR_ITEM = 4;
    public static final int ACCION_DIVIDIR_ITEM = 5;
    public static final int ACCION_BORRAR_ITEM = 6;
    public static final int ACCION_DESPLANIFICAR_ITEM = 7;
    public static final int ACCION_ASIGNAR_AULA_ITEM = 8;
    public static final int ACCION_EDICION_DETALLE_ITEM = 9;

    private Long id;
    private Date fecha;
    private Evento evento;
    private String usuario;
    private String descripcion;
    private Long tipoAccion;
    private String tipoAccionNombre;
    private Long diaSemana;
    private Date horaFin;
    private Date horaInicio;
    private Long aulaId;
    private Boolean deshecho;

    public Log()
    {
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Date getFecha()
    {
        return fecha;
    }

    public void setFecha(Date fecha)
    {
        this.fecha = fecha;
    }

    public Evento getEvento()
    {
        return evento;
    }

    public void setEvento(Evento evento)
    {
        this.evento = evento;
    }

    public String getDescripcion()
    {
        return descripcion;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public String getUsuario()
    {
        return usuario;
    }

    public void setUsuario(String usuario)
    {
        this.usuario = usuario;
    }

    public static String getNombreDiaSemana(int diaSemana)
    {
        switch (diaSemana)
        {
            case Calendar.SUNDAY:
                return "Diumenge";
            case Calendar.MONDAY:
                return "Dilluns";
            case Calendar.TUESDAY:
                return "Dimarts";
            case Calendar.WEDNESDAY:
                return "Dimecres";
            case Calendar.THURSDAY:
                return "Dijous";
            case Calendar.FRIDAY:
                return "Divendres";
            case Calendar.SATURDAY:
                return "Dissabte";
            default:
                return "";
        }
    }

    public String getTipoAccionNombre()
    {
        switch (tipoAccion.intValue())
        {
            case ACCION_MOVER_ITEM:
                return "Moure subgrup";
            case ACCION_MOVER_ITEM_SEMANA_DETALLE:
                return "Moure subgrup setmana detall";
            case ACCION_PLANIFICAR_ITEM:
                return "Planificar subgrup";
            case ACCION_DIVIDIR_ITEM:
                return "Dividir subgrup";
            case ACCION_BORRAR_ITEM:
                return "Esborrar subgrup";
            case ACCION_DESPLANIFICAR_ITEM:
                return "Desplanificar subgrup";
            case ACCION_ASIGNAR_AULA_ITEM:
                return "Assignar aula";
            case ACCION_EDICION_DETALLE_ITEM:
                return "Editar detalls";
            case ACCION_MOVER_ITEM_DETALLE_MANUAL:
                return "Moure subgrup amb detall manual";
            default:
                return "";
        }
    }

    public Long getTipoAccion()
    {
        return tipoAccion;
    }

    public void setTipoAccion(Long tipoAccion)
    {
        this.tipoAccion = tipoAccion;
    }

    public Long getDiaSemana()
    {
        return diaSemana;
    }

    public void setDiaSemana(Long diaSemana)
    {
        this.diaSemana = diaSemana;
    }

    public Date getHoraFin()
    {
        return horaFin;
    }

    public void setHoraFin(Date horaFin)
    {
        this.horaFin = horaFin;
    }

    public Date getHoraInicio()
    {
        return horaInicio;
    }

    public void setHoraInicio(Date horaInicio)
    {
        this.horaInicio = horaInicio;
    }

    public Long getAulaId()
    {
        return aulaId;
    }

    public void setAulaId(Long aulaId)
    {
        this.aulaId = aulaId;
    }

    public Boolean sePuedeDeshacer()
    {
        return (tipoAccion != null) && (tipoAccion.intValue() != ACCION_MOVER_ITEM_SEMANA_DETALLE) &&
                (tipoAccion.intValue() != ACCION_MOVER_ITEM_DETALLE_MANUAL) &&
                (tipoAccion.intValue() != ACCION_DIVIDIR_ITEM) &&
                (tipoAccion.intValue() != ACCION_EDICION_DETALLE_ITEM) &&
                (tipoAccion.intValue() != ACCION_BORRAR_ITEM) &&
                (!deshecho);
    }

    public Boolean isDeshecho()
    {
        return deshecho;
    }

    public void setDeshecho(Boolean deshecho)
    {
        this.deshecho = deshecho;
    }
}
