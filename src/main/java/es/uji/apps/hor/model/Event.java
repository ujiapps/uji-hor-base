package es.uji.apps.hor.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

public class Event implements Serializable {
    private Long id;
    private String name;
    private String startDate;
    private String endDate;
    private String description;
    private String horaIni;
    private Long resourceId;
    private Long plazasAula;
    private String fechasDetalles;
    private String nombreAsignatura;
    private String comentarios;
    private String comunes;
    private Long cid;
    private String diaSemana;
    private String notes;
    private Integer repetirCada;
    private Date startDateRep;
    private Date endDateRepComp;
    private Integer endRepNumberComp;
    private Boolean detalleManual;
    private BigDecimal creditos;
    private BigDecimal creditosComputables;
    private Semestre semestre;
    private String profesores;
    private Boolean AllDay;
    private Long subgrupoId;
    private Long eventoId;

    public Event() {
    }

    public Event(Long id, String name, Date startDate, Date endDate, String description, Long horaIni, Long resourceId,
                 Long plazasAula, String fechasDetalles, String nombreAsignatura, String comentarios, String comunes,
                 Long cid, String diaSemana, String notes, Integer repetirCada,  Date startDateRep,Date endDateRepComp,
                    Integer endRepNumberComp, Boolean detalleManual, BigDecimal creditos, BigDecimal creditosComputables, Semestre semestre, Long subgrupoId,
                 Long eventoId) {
        this.id = id;
        this.name = name;
        setStartDate(startDate);
        setEndDate(endDate);
        this.description = description;
        setHoraIni(horaIni);
        this.resourceId = resourceId;
        this.plazasAula = plazasAula;
        this.fechasDetalles = fechasDetalles;
        this.nombreAsignatura = nombreAsignatura;
        this.comentarios = comentarios;
        this.comunes = comunes;
        this.cid = cid;
        this.diaSemana = diaSemana;
        this.notes = notes;
        this.repetirCada = repetirCada;
        this.startDateRep = startDateRep;
        this.endDateRepComp = endDateRepComp;
        this.endRepNumberComp = endRepNumberComp;
        this.detalleManual = detalleManual;
        this.creditos = creditos;
        this.creditosComputables = creditosComputables;
        this.semestre = semestre;
        this.subgrupoId = subgrupoId;
        this.eventoId = eventoId;
    }

    public Event(Long id, String profesores, Long plazasAula, Long cid, String name, Date startDate, Date endDate, Boolean AllDay, Long horaIni) {
        this.id = id;
        this.profesores = profesores;
        this.plazasAula = plazasAula;
        this.cid = cid;
        this.name = name;
        setStartDate(startDate);
        setEndDate(endDate);
        this.AllDay = AllDay;
        this.resourceId = cid;
        setHoraIni2(horaIni);
        this.description = name;
        this.nombreAsignatura = "";
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        this.startDate = formatter.format(startDate);
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        this.endDate = formatter.format(endDate);
    }

    public void setHoraIni(Long horaIni) {
        Format formatter = new SimpleDateFormat("HH:mm");
        this.horaIni = formatter.format(horaIni);
    }

    public void setHoraIni2(Long horaIni) {
        this.horaIni = String.format("%02d:00", horaIni);
    }

    public Long getResourceId() {
        return resourceId;
    }

    public void setResourceId(Long resourceId) {
        this.resourceId = resourceId;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHoraIni() {
        return horaIni;
    }

    public void setHoraIni(String horaIni) {
        this.horaIni = horaIni;
    }

    public Long getPlazasAula() {
        return plazasAula;
    }

    public void setPlazasAula(Long plazas) {
        this.plazasAula = plazas;
    }

    public String getFechasDetalles() {
        return fechasDetalles;
    }

    public void setFechasDetalles(String fechasDetalles) {
        this.fechasDetalles = fechasDetalles;
    }

    public String getNombreAsignatura() {
        return nombreAsignatura;
    }

    public void setNombreAsignatura(String nombreAsignatura) {
        this.nombreAsignatura = nombreAsignatura;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

    public String getComunes() {
        return comunes;
    }

    public void setComunes(String comunes) {
        this.comunes = comunes;
    }

    public Long getCid() {
        return cid;
    }

    public void setCid(Long cid) {
        this.cid = cid;
    }

    public String getDiaSemana() {
        return diaSemana;
    }

    public void setDiaSemana(String diaSemana) {
        this.diaSemana = diaSemana;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Integer getRepetirCada() {
        return repetirCada;
    }

    public void setRepetirCada(Integer repetirCada) {
        this.repetirCada = repetirCada;
    }

    public Date getStartDateRep() {
        return startDateRep;
    }

    public void setStartDateRep(Date startDateRep) {
        this.startDateRep = startDateRep;
    }

    public Date getEndDateRepComp() {
        return endDateRepComp;
    }

    public void setEndDateRepComp(Date endDateRepComp) {
        this.endDateRepComp = endDateRepComp;
    }

    public Integer getEndRepNumberComp() {
        return endRepNumberComp;
    }

    public void setEndRepNumberComp(Integer endRepNumberComp) {
        this.endRepNumberComp = endRepNumberComp;
    }

    public Boolean getDetalleManual() {
        return detalleManual;
    }

    public void setDetalleManual(Boolean detalleManual) {
        this.detalleManual = detalleManual;
    }

    public BigDecimal getCreditos() {
        return creditos;
    }

    public void setCreditos(BigDecimal creditos) {
        this.creditos = creditos;
    }

    public BigDecimal getCreditosComputables() {
        return creditosComputables;
    }

    public void setCreditosComputables(BigDecimal creditosComputables) {
        this.creditosComputables = creditosComputables;
    }

    public Semestre getSemestre() {
        return semestre;
    }

    public void setSemestre(Semestre semestre) {
        this.semestre = semestre;
    }

    public String getProfesores() {
        return profesores;
    }

    public void setProfesores(String profesores) {
        this.profesores = profesores;
    }

    public Boolean getAllDay() {
        return AllDay;
    }

    public void setAllDay(Boolean allDay) {
        AllDay = allDay;
    }

    public Long getSubgrupoId() {
        return subgrupoId;
    }

    public void setSubgrupoId(Long subgrupoId) {
        this.subgrupoId = subgrupoId;
    }

    public Long getEventoId() {
        return eventoId;
    }

    public void setEventoId(Long eventoId) {
        this.eventoId = eventoId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Event event = (Event) o;
        return Objects.equals(id, event.id) &&
                Objects.equals(startDate, event.startDate) &&
                Objects.equals(endDate, event.endDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, startDate, endDate);
    }
}