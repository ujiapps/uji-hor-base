package es.uji.apps.hor.model;

public class Grupo
{
    private String grupo;

    public Grupo(String grupo)
    {
        this.grupo = grupo;
    }

    public String getGrupo()
    {
        return grupo;
    }

    public void setGrupo(String grupo)
    {
        this.grupo = grupo;
    }

}
