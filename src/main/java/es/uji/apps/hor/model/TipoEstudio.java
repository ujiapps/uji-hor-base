package es.uji.apps.hor.model;

public class TipoEstudio
{
    public static String GRADO = "G";
    public static String MASTER = "M";

    private String id;
    private String nombre;
    private Integer orden = 0;

    public TipoEstudio()
    {

    }

    public TipoEstudio(String id)
    {
        super();
        this.id = id;
    }

    public TipoEstudio(String id, String nombre)
    {
        super();
        this.id = id;
        this.nombre = nombre;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Integer getOrden()
    {
        return orden;
    }

    public void setOrden(Integer orden)
    {
        this.orden = orden;
    }

    @Override
    public String toString()
    {
        return id;
    }

    public static String getTipoEstudioId(Long estudioId)
    {
        Long MAX_RANGO_GRADO = 1000L;
        return (estudioId < MAX_RANGO_GRADO) ? GRADO : MASTER;
    }

}
