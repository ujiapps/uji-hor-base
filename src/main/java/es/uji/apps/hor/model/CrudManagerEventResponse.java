package es.uji.apps.hor.model;

import java.util.List;

public class CrudManagerEventResponse {
    private EventSection events;
    private ResourceSection resources;
    private AssignmentSection assignments;
    private Boolean success;

    public static class EventSection {
        private List<Event> rows;

        public EventSection(List<Event> rows) {
            this.rows = rows;
        }

        public List<Event> getRows() {
            return rows;
        }

        public void setRows(List<Event> rows) {
            this.rows = rows;
        }
    }

    public static class ResourceSection {
        private List<Resource> rows;

        public ResourceSection(List<Resource> rows) {
            this.rows = rows;
        }

        public List<Resource> getRows() {
            return rows;
        }

        public void setRows(List<Resource> rows) {
            this.rows = rows;
        }
    }

    public static class AssignmentSection {
        private List<Assignment> rows;

        public AssignmentSection(List<Assignment> rows) {
            this.rows = rows;
        }

        public List<Assignment> getRows() {
            return rows;
        }

        public void setRows(List<Assignment> rows) {
            this.rows = rows;
        }
    }

    public EventSection getEvents() {
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = new EventSection(events);
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public ResourceSection getResources() {
        return resources;
    }

    public void setResources(List<Resource> resources) {
        this.resources = new ResourceSection(resources);
    }

    public AssignmentSection getAssignments() {
        return assignments;
    }

    public void setAssignments(List<Assignment> assignments) {
        this.assignments = new AssignmentSection(assignments);
    }
}