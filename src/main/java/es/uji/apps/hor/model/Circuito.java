package es.uji.apps.hor.model;

import java.util.List;

import es.uji.apps.hor.CircuitoNoPerteneceAEstudioException;

public class Circuito
{
    private Long id;
    private String nombre;
    private String grupo;
    private List<Estudio> estudios;
    private List<Evento> eventos;
    private Long plazas;

    private static final Long CURSO_CIRCUITO = 1L;

    public Circuito()
    {

    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getPlazas()
    {
        return plazas;
    }

    public void setPlazas(Long plazas)
    {
        this.plazas = plazas;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getGrupo()
    {
        return grupo;
    }

    public void setGrupo(String grupo)
    {
        this.grupo = grupo;
    }

    public void compruebaEstudioEnCircuito(Evento evento)
            throws CircuitoNoPerteneceAEstudioException
    {
        boolean found = false;

        if (evento.getGrupoId().equals(this.grupo))
        {
            for (Asignatura asignatura : evento.getAsignaturas())
            {
                Estudio estudioAsignatura = asignatura.getEstudio();
                for (Estudio estudio : this.getEstudios())
                {
                    if (estudioAsignatura.equals(estudio)
                            && asignatura.getCursoId().equals(CURSO_CIRCUITO))
                    {
                        found = true;
                        break;
                    }
                }
            }
        }

        if (!found)
        {
            throw new CircuitoNoPerteneceAEstudioException();
        }
    }

    public void compruebaEstudioEnCircuito(Long estudioId)
            throws CircuitoNoPerteneceAEstudioException
    {
        Estudio estudio = new Estudio();
        estudio.setId(estudioId);

        if (!this.estudios.contains(estudio))
        {
            throw new CircuitoNoPerteneceAEstudioException();
        }
    }

    public List<Estudio> getEstudios()
    {
        return estudios;
    }

    public void setEstudios(List<Estudio> estudios)
    {
        this.estudios = estudios;
    }

    public List<Evento> getEventos()
    {
        return eventos;
    }

    public void setEventos(List<Evento> eventos)
    {
        this.eventos = eventos;
    }

    public boolean tieneItemsAsignados()
    {
        return this.getEventos().size() > 0;
    }

    public void update(String nombre, Long plazas, List<Estudio> estudios)
    {
        this.nombre = nombre;
        this.plazas = plazas;
        this.estudios = estudios;
    }
}
