package es.uji.apps.hor.model;

import es.uji.apps.hor.DiaNoValidoExamenException;

import java.util.Calendar;
import java.util.Date;

public class Convocatoria
{
    private Long id;
    private String nombre;
    private Date inicio;
    private Date fin;
    private Long cursoId;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Date getInicio()
    {
        return inicio;
    }

    public void setInicio(Date inicio)
    {
        this.inicio = inicio;
    }

    public Date getFin()
    {
        return fin;
    }

    public void setFin(Date fin)
    {
        this.fin = fin;
    }

    public Long getCursoId()
    {
        return cursoId;
    }

    public void setCursoId(Long cursoId)
    {
        this.cursoId = cursoId;
    }

    public void compruebaFechaDentroConvocatoria(Date fecha) throws DiaNoValidoExamenException
    {
        fecha = removeTime(fecha);
        if (fecha.before(inicio) || fecha.after(fin))
        {
            throw new DiaNoValidoExamenException();
        }
    }

    private Date removeTime(Date date)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }
}
