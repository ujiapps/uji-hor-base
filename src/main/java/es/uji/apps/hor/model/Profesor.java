package es.uji.apps.hor.model;

import java.math.BigDecimal;
import java.util.List;

public class Profesor
{
    private Long id;
    private String nombre;
    private String email;
    private Departamento departamento;
    private Area area;
    private Boolean pendienteContratacion;
    private BigDecimal creditos;
    private BigDecimal creditosComputables;
    private BigDecimal creditosMaximos;
    private BigDecimal creditosReduccion;
    
    private List<Evento> eventos;
    private List<EventoDetalle> eventosDetalle;

    private String categoriaId;

    private String nombreCategoria;

    private BigDecimal creditosAsignados;
    private BigDecimal creditosPendientes;

    public Profesor()
    {

    }

    public Profesor(Long id, String nombre)
    {
        this.id = id;
        this.nombre = nombre;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public Departamento getDepartamento()
    {
        return departamento;
    }

    public void setDepartamento(Departamento departamento)
    {
        this.departamento = departamento;
    }

    public Area getArea()
    {
        return area;
    }

    public void setArea(Area area)
    {
        this.area = area;
    }

    public Boolean getPendienteContratacion()
    {
        return pendienteContratacion;
    }

    public void setPendienteContratacion(Boolean pendienteContratacion)
    {
        this.pendienteContratacion = pendienteContratacion;
    }

    public List<Evento> getEventos() {
        return eventos;
    }

    public void setEventos(List<Evento> eventos) {
        this.eventos = eventos;
    }

    public List<EventoDetalle> getEventosDetalle() {
        return eventosDetalle;
    }

    public void setEventosDetalle(List<EventoDetalle> eventosDetalle) {
        this.eventosDetalle = eventosDetalle;
    }

    public BigDecimal getCreditos() {
        return creditos;
    }

    public void setCreditos(BigDecimal creditos) {
        this.creditos = creditos;
    }

    public BigDecimal getCreditosComputables() {
        return creditosComputables;
    }

    public void setCreditosComputables(BigDecimal creditosComputables) {
        this.creditosComputables = creditosComputables;
    }

    public BigDecimal getCreditosMaximos() {
        return creditosMaximos;
    }

    public void setCreditosMaximos(BigDecimal creditosMaximos) {
        this.creditosMaximos = creditosMaximos;
    }

    public BigDecimal getCreditosReduccion() {
        return creditosReduccion;
    }

    public void setCreditosReduccion(BigDecimal creditosReduccion) {
        this.creditosReduccion = creditosReduccion;
    }

    public String getCategoriaId() {
        return categoriaId;
    }

    public void setCategoriaId(String categoriaId) {
        this.categoriaId = categoriaId;
    }

    public String getNombreCategoria() {
        return nombreCategoria;
    }

    public void setNombreCategoria(String nombreCategoria) {
        this.nombreCategoria = nombreCategoria;
    }

    public BigDecimal getCreditosAsignados() {
        return creditosAsignados;
    }

    public void setCreditosAsignados(BigDecimal creditosAsignados) {
        this.creditosAsignados = creditosAsignados;
    }

    public BigDecimal getCreditosPendientes() {
        return creditosPendientes;
    }

    public void setCreditosPendientes(BigDecimal creditosPendientes) {
        this.creditosPendientes = creditosPendientes;
    }
}
