package es.uji.apps.hor.model;

import es.uji.commons.rest.annotations.DataTag;

import java.util.Date;
import java.util.List;

public class SemestreDetalle
{
    private Long id;
    @DataTag
    private Semestre semestre;
    @DataTag
    private TipoEstudio tipoEstudio;
    private Date fechaInicio = null;
    private Date fechaFin = null;
    private Date fechaExamenesInicio = null;
    private Date fechaExamenesInicioC2 = null;
    private Date fechaExamenesFinC2 = null;
    private Date fechaExamenesFin = null;
    private Long numeroSemanas;

    public SemestreDetalle(Long id, Semestre semestre, TipoEstudio tipoEstudio, Date fechaInicio,
            Date fechaFin, Long numeroSemanas)
    {
        super();
        this.id = id;
        this.semestre = semestre;
        this.tipoEstudio = tipoEstudio;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.numeroSemanas = numeroSemanas;
    }

    public SemestreDetalle(Long id, Semestre semestre, TipoEstudio tipoEstudio, Date fechaInicio,
            Date fechaFin, Date fechaExamenesInicio, Date fechaExamenesFin, Long numeroSemanas)
    {
        super();
        this.id = id;
        this.semestre = semestre;
        this.tipoEstudio = tipoEstudio;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.fechaExamenesInicio = fechaExamenesInicio;
        this.fechaExamenesFin = fechaExamenesFin;
        this.numeroSemanas = numeroSemanas;
    }
    
    public SemestreDetalle()
    {  
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Semestre getSemestre()
    {
        return semestre;
    }

    public void setSemestre(Semestre semestre)
    {
        this.semestre = semestre;
    }

    public TipoEstudio getTipoEstudio()
    {
        return tipoEstudio;
    }

    public void setTipoEstudio(TipoEstudio tipoEstudio)
    {
        this.tipoEstudio = tipoEstudio;
    }

    public Date getFechaInicio()
    {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio)
    {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin()
    {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin)
    {
        this.fechaFin = fechaFin;
    }

    public Date getFechaExamenesInicio()
    {
        return fechaExamenesInicio;
    }

    public void setFechaExamenesInicio(Date fechaExamenesInicio)
    {
        this.fechaExamenesInicio = fechaExamenesInicio;
    }

    public Date getFechaExamenesFin()
    {
        return fechaExamenesFin;
    }

    public void setFechaExamenesFin(Date fechaExamenesFin)
    {
        this.fechaExamenesFin = fechaExamenesFin;
    }

    public Long getNumeroSemanas()
    {
        return numeroSemanas;
    }

    public void setNumeroSemanas(Long numeroSemanas)
    {
        this.numeroSemanas = numeroSemanas;
    }

    public Date getFechaExamenesFinC2() {
        return fechaExamenesFinC2;
    }

    public void setFechaExamenesFinC2(Date fechaExamenesFinC2) {
        this.fechaExamenesFinC2 = fechaExamenesFinC2;
    }

    public Date getFechaExamenesInicioC2() {
        return fechaExamenesInicioC2;
    }

    public void setFechaExamenesInicioC2(Date fechaExamenesInicioC2) {
        this.fechaExamenesInicioC2 = fechaExamenesInicioC2;
    }

    public static SemestreDetalle creaSemestreDetalleGeneralDesdeSemestresDetalles(List<SemestreDetalle> semestresDetalle) {
        SemestreDetalle semestreDetalle = new SemestreDetalle();
        SemestreDetalle muestra = semestresDetalle.get(0);

        semestreDetalle.setTipoEstudio(muestra.getTipoEstudio());
        semestreDetalle.setSemestre(muestra.getSemestre());
        semestreDetalle.setId(muestra.getId());
        semestreDetalle.setFechaExamenesFin(muestra.getFechaExamenesFin());
        semestreDetalle.setFechaExamenesFinC2(muestra.getFechaExamenesFinC2());
        semestreDetalle.setFechaExamenesInicio(muestra.getFechaExamenesInicio());
        semestreDetalle.setFechaExamenesInicioC2(muestra.getFechaExamenesInicioC2());
        semestreDetalle.setFechaInicio(muestra.getFechaInicio());
        semestreDetalle.setFechaFin(muestra.getFechaFin());

        for (SemestreDetalle sd: semestresDetalle) {
            if (sd.getFechaInicio().before(semestreDetalle.getFechaInicio())) {
                semestreDetalle.setFechaInicio(sd.getFechaInicio());
            }

            if (sd.getFechaExamenesInicio().before(semestreDetalle.getFechaExamenesInicio())) {
                semestreDetalle.setFechaExamenesInicio(sd.getFechaExamenesInicio());
            }

            if (sd.getFechaExamenesInicioC2().before(semestreDetalle.getFechaExamenesInicioC2())) {
                semestreDetalle.setFechaExamenesInicioC2(sd.getFechaExamenesInicioC2());
            }

            if (sd.getFechaFin().after(semestreDetalle.getFechaFin())) {
                semestreDetalle.setFechaFin(sd.getFechaFin());
            }

            if (sd.getFechaExamenesFin().after(semestreDetalle.getFechaExamenesFin())) {
                semestreDetalle.setFechaExamenesFin(sd.getFechaExamenesFin());
            }

            if (sd.getFechaExamenesFinC2().after(semestreDetalle.getFechaExamenesFinC2())) {
                semestreDetalle.setFechaExamenesFinC2(sd.getFechaExamenesFinC2());
            }
        }
        return semestreDetalle;
    }
}
