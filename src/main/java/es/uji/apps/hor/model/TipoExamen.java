package es.uji.apps.hor.model;

public class TipoExamen
{
    public static final Long TEORIA = 1L;
    public static final Long PROBLEMAS = 2L;
    public static final Long LABORATORIO = 3L;
    public static final Long ORAL = 4L;
    public static final Long PRACTICAS = 5L;

    private Long id;
    private String nombre;
    private Boolean porDefecto;
    private String codigo;

    public TipoExamen()
    {

    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Boolean getPorDefecto()
    {
        return porDefecto;
    }

    public void setPorDefecto(Boolean porDefecto)
    {
        this.porDefecto = porDefecto;
    }

    public String getCodigo()
    {
        return codigo;
    }

    public void setCodigo(String codigo)
    {
        this.codigo = codigo;
    }
}
