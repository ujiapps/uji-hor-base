package es.uji.apps.hor.model;



public class Area
{
    private Long id;
    private String nombre;
    private Departamento departamento;
    private Long activa;

    public Area() {
    }
    
    public Area(Long id, String nombre)
    {
        this.id = id;
        this.nombre = nombre;
    }
    
    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Departamento getDepartamento()
    {
        return departamento;
    }

    public void setDepartamento(Departamento departamento)
    {
        this.departamento = departamento;
    }

    public Long getActiva()
    {
        return activa;
    }

    public void setActiva(Long activa)
    {
        this.activa = activa;
    }
}
