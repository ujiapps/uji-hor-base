package es.uji.apps.hor.model;

import java.util.ArrayList;
import java.util.List;

public class CalendarioAsignatura
{
    private Long id;
    private String titulo;
    private Integer color;

    public CalendarioAsignatura()
    {

    }

    public CalendarioAsignatura(Long id, String titulo, Integer color)
    {
        this.id = id;
        this.titulo = titulo;
        this.color = color;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getTitulo()
    {
        return titulo;
    }

    public void setTitulo(String titulo)
    {
        this.titulo = titulo;
    }

    public Integer getColor()
    {
        return color;
    }

    public void setColor(Integer color)
    {
        this.color = color;
    }

    public static List<CalendarioAsignatura> getCalendariosProfesor()
    {
        List<CalendarioAsignatura> calendarios = new ArrayList<CalendarioAsignatura>();

        calendarios.add(CalendarioAsignatura.getCalendarioSinProfesoresAsignados());
        calendarios.add(CalendarioAsignatura.getCalendarioConProfesoresAsignados());
        calendarios.add(CalendarioAsignatura.getCalendarioConMismoProfesorAsignado());

        return calendarios;
    }

    public static CalendarioAsignatura getCalendarioSinProfesoresAsignados()
    {
        return new CalendarioAsignatura(TipoCalendarioAsignatura.SP.getCalendarioId(),
                TipoCalendarioAsignatura.SP.getNombre(), TipoCalendarioAsignatura.SP.getColor());
    }

    public static CalendarioAsignatura getCalendarioConProfesoresAsignados()
    {
        return new CalendarioAsignatura(TipoCalendarioAsignatura.CP.getCalendarioId(),
                TipoCalendarioAsignatura.CP.getNombre(), TipoCalendarioAsignatura.CP.getColor());
    }

    public static CalendarioAsignatura getCalendarioConMismoProfesorAsignado()
    {
        return new CalendarioAsignatura(TipoCalendarioAsignatura.MP.getCalendarioId(),
                TipoCalendarioAsignatura.MP.getNombre(), TipoCalendarioAsignatura.MP.getColor());
    }

    public static CalendarioAsignatura getCalendarioConMismoProfesorAsignadoSinSesiones()
    {
        return new CalendarioAsignatura(TipoCalendarioAsignatura.MPSS.getCalendarioId(),
                TipoCalendarioAsignatura.MPSS.getNombre(), TipoCalendarioAsignatura.MPSS.getColor());
    }

    public static CalendarioAsignatura getCalendarioConOcupacionProfesor()
    {
        return new CalendarioAsignatura(TipoCalendarioAsignatura.OP.getCalendarioId(),
                TipoCalendarioAsignatura.OP.getNombre(), TipoCalendarioAsignatura.OP.getColor());
    }

    public static CalendarioAsignatura getCalendarioRestoArea()
    {
        return new CalendarioAsignatura(TipoCalendarioAsignatura.RA.getCalendarioId(),
                TipoCalendarioAsignatura.RA.getNombre(), TipoCalendarioAsignatura.RA.getColor());
    }

}
