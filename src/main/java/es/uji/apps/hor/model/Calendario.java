package es.uji.apps.hor.model;

public class Calendario
{
    private Long id;
    private String nombre;

    public Calendario()
    {

    }

    public Calendario(Long id)
    {
        this.id = id;
    }

    public Calendario(Long id, String nombre)
    {
        this.id = id;
        this.nombre = nombre;
    }

    public Calendario(String tipoSubgrupoId)
    {
        switch (tipoSubgrupoId)
        {
        case "TE":
            id = 1L;
            nombre = "Teoria";
            break;
        case "PR":
            id = 2L;
            nombre = "Problemes";
            break;
        case "LA":
            id = 3L;
            nombre = "Laboratoris";
            break;
        case "SE":
            id = 4L;
            nombre = "Seminaris";
            break;
        case "TU":
            id = 5L;
            nombre = "Tutories";
            break;
        case "AV":
            id = 6L;
            nombre = "Avaluació";
            break;
        case "TP":
            id = 7L;
            nombre = "Teoria i problemes";
            break;
        }
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getLetraId()
    {
        return TipoSubgrupo.getTipoSubgrupo(id);
    }
}
