package es.uji.apps.hor.model;

import java.util.List;

import es.uji.apps.hor.EstudioNoCompartidoException;
import es.uji.commons.rest.annotations.DataTag;

public class Estudio
{
    private Long id;

    @DataTag
    private String nombre;
    private TipoEstudio tipoEstudio;
    private Centro centro;
    private Boolean oficial;
    private Boolean abiertoMatricula;
    private Boolean abiertoConsulta;
    private Boolean abiertoSesionesPodPdi;
    private Integer numeroCursos;
    private List<Estudio> estudiosCompartidos;

    public Estudio(Long id, String nombre)
    {
        this.id = id;
        this.nombre = nombre;
    }

    public Estudio(Long id)
    {
        this.id = id;
    }

    public Estudio()
    {
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public TipoEstudio getTipoEstudio()
    {
        return tipoEstudio;
    }

    public void setTipoEstudio(TipoEstudio tipoEstudio)
    {
        this.tipoEstudio = tipoEstudio;
    }

    public Centro getCentro()
    {
        return centro;
    }

    public void setCentro(Centro centro)
    {
        this.centro = centro;
    }

    public Boolean getOficial()
    {
        return oficial;
    }

    public void setOficial(Boolean oficial)
    {
        this.oficial = oficial;
    }

    public Boolean getAbiertoMatricula()
    {
        return abiertoMatricula;
    }

    public void setAbiertoMatricula(Boolean abiertoMatricula)
    {
        this.abiertoMatricula = abiertoMatricula;
    }

    public Boolean getAbiertoSesionesPodPdi()
    {
        return abiertoSesionesPodPdi;
    }

    public void setAbiertoSesionesPodPdi(Boolean abiertoSesionesPodPdi)
    {
        this.abiertoSesionesPodPdi = abiertoSesionesPodPdi;
    }

    public Integer getNumeroCursos()
    {
        return numeroCursos;
    }

    public void setNumeroCursos(Integer numeroCursos)
    {
        this.numeroCursos = numeroCursos;
    }

    public List<Estudio> getEstudiosCompartidos()
    {
        return estudiosCompartidos;
    }

    public void setEstudiosCompartidos(List<Estudio> estudiosCompartidos)
    {
        this.estudiosCompartidos = estudiosCompartidos;
    }

    public Boolean getAbiertoConsulta()
    {
        return abiertoConsulta;
    }

    public void setAbiertoConsulta(Boolean abiertoConsulta)
    {
        this.abiertoConsulta = abiertoConsulta;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        Estudio other = (Estudio) obj;
        if (id == null)
        {
            if (other.id != null)
            {
                return false;
            }
        }
        else if (!id.equals(other.id))
        {
            return false;
        }
        return true;
    }

    public void compruebaEstudioCompartido(Estudio estudioCompartido)
            throws EstudioNoCompartidoException
    {
        if (!this.estudiosCompartidos.contains(estudioCompartido))
        {
            throw new EstudioNoCompartidoException(this.nombre);
        }
    }

}
