package es.uji.apps.hor.model;

import es.uji.apps.hor.*;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.*;

@Component
public class Evento
{
    private static final int LUNES = 1;
    private static final int MARTES = 2;
    private static final int MIERCOLES = 3;
    private static final int JUEVES = 4;
    private static final int VIERNES = 5;
    private static final int SABADO = 6;
    private static final int DOMINGO = 7;

    private static final Long UNA_HORA_EN_MILISEGUNDOS = (long) 3600000;

    private Long id;
    private Calendario calendario;
    private String titulo;
    private String observaciones;
    private Date inicio;
    private Date fin;
    private Boolean detalleManual;
    private Integer numeroIteraciones;
    private Integer repetirCadaSemanas;
    private Date desdeElDia;
    private Date hastaElDia;
    private List<Asignatura> asignaturas = new ArrayList<Asignatura>();
    private Semestre semestre;
    private String grupoId;
    private Long subgrupoId;
    private Long plazas;
    private BigDecimal creditos;
    private BigDecimal creditosComputables;
    private Circuito circuito;
    private List<EventoDetalle> eventosDetalle = new ArrayList<EventoDetalle>();
    private CalendarioCircuito calendarioCircuito;
    private CalendarioAsignatura calendarioAsignatura;
    private String textoAsignaturasComunes;
    private String comentarios;

    private String tipoEstudioId;

    private List<Profesor> profesores = new ArrayList<Profesor>();
    private List<ProfesorDetalle> profesoresDetalle = new ArrayList<ProfesorDetalle>();

    private Aula aula;

    public Evento(Long id, Calendario calendario, String titulo, Date inicio, Date fin)
    {
        this.id = id;
        this.calendario = calendario;
        this.titulo = titulo;
        this.inicio = inicio;
        this.fin = fin;
    }

    public Evento()
    {
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Calendario getCalendario()
    {
        return calendario;
    }

    public void setCalendario(Calendario calendario)
    {
        this.calendario = calendario;
    }

    public String getDescripcionParaUnEstudio(Long estudioId)
    {
        Asignatura asignatura = getAsignaturaDelEstudio(estudioId);
        if (asignatura == null)
        {
            return "";
        }

        String texto = MessageFormat.format("{0} {1} {2}{3}", asignatura.getId(), grupoId,
                getCalendario().getLetraId(), subgrupoId);

        if (tieneComunes())
        {
            texto = MessageFormat.format("{0} - C", texto);
        }

        if (getAula() != null)
        {
            texto = MessageFormat.format("{0} {1}", texto, getAula().getCodigo());
        }

        return texto;
    }

    public String getDescripcionCompletaParaUnEstudio(Long estudioId)
    {
        Asignatura asignatura = getAsignaturaDelEstudio(estudioId);
        if (asignatura == null)
        {
            return "";
        }

        String texto = MessageFormat.format("{0} {1} Grup {2} {3}{4}", asignatura.getId(),
                asignatura.getNombre(), grupoId, getCalendario().getLetraId(), subgrupoId);

        if (tieneComunes())
        {
            texto = MessageFormat.format("{0} - Compartida", texto);
        }

        if (getAula() != null)
        {
            texto = MessageFormat.format("{0} - {1}", texto, getAula().getNombre());
        }

        return texto;
    }

    public boolean tieneComunes()
    {
        return (textoAsignaturasComunes != null && !textoAsignaturasComunes.isEmpty());
    }

    public String getTitulo()
    {
        if (titulo != null && !titulo.isEmpty())
        {
            return titulo;
        }
        else
        {
            return this.toString();
        }
    }

    public void setTitulo(String titulo)
    {
        this.titulo = titulo;
    }

    public String getObservaciones()
    {
        return observaciones;
    }

    public void setObservaciones(String observaciones)
    {
        this.observaciones = observaciones;
    }

    public Date getInicio()
    {
        return inicio;
    }

    public void setInicio(Date inicio)
    {
        this.inicio = inicio;
    }

    public Date getFin()
    {
        return fin;
    }

    public void setFin(Date fin)
    {
        this.fin = fin;
    }

    public void setFechaInicioYFin(Date inicio, Date fin) throws DuracionEventoIncorrectaException
    {
        if (fechasEnElMismoDiaYEnSemanaLaboral(inicio, fin))
        {
            this.setInicio(inicio);
            this.setFin(fin);
        }
        else
        {
            throw new DuracionEventoIncorrectaException();
        }
    }

    private boolean nuevaFechaCambiaDeDiaSemana(Date nuevaFechaInicio)
    {

        return !mismoDiaSemana(this.inicio, nuevaFechaInicio);
    }

    private boolean mismoDiaSemana(Date unDia, Date otroDia)
    {
        Calendar calUnDia = Calendar.getInstance();
        Calendar calOtroDia = Calendar.getInstance();

        calUnDia.setTime(unDia);
        calOtroDia.setTime(otroDia);

        return calUnDia.get(Calendar.DAY_OF_WEEK) == calOtroDia.get(Calendar.DAY_OF_WEEK);
    }

    private boolean fechasEnElMismoDiaYEnSemanaLaboral(Date inicio, Date fin)
    {
        Calendar calInicio = Calendar.getInstance();
        Calendar calFin = Calendar.getInstance();

        calInicio.setTime(inicio);
        calFin.setTime(fin);

        return mismoDia(calInicio, calFin) && noEsDomingo(calInicio);
    }

    private boolean mismoDia(Calendar calInicio, Calendar calFin)
    {
        return calInicio.get(Calendar.YEAR) == calFin.get(Calendar.YEAR)
                && calInicio.get(Calendar.MONTH) == calFin.get(Calendar.MONTH)
                && calInicio.get(Calendar.DAY_OF_MONTH) == calFin.get(Calendar.DAY_OF_MONTH);
    }

    private boolean noEsDomingo(Calendar calInicio)
    {
        return calInicio.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY;
    }

    public Boolean hasDetalleManual()
    {
        return detalleManual;
    }

    public void setDetalleManual(Boolean detalleManual)
    {
        this.detalleManual = detalleManual;
    }

    public Integer getNumeroIteraciones()
    {
        return numeroIteraciones;
    }

    public void setNumeroIteraciones(Integer numeroIteraciones)
    {
        this.numeroIteraciones = numeroIteraciones;
    }

    public Integer getRepetirCadaSemanas()
    {
        return repetirCadaSemanas;
    }

    public void setRepetirCadaSemanas(Integer repetirCadaSemanas)
    {
        this.repetirCadaSemanas = repetirCadaSemanas;
    }

    public Date getDesdeElDia()
    {
        return desdeElDia;
    }

    public void setDesdeElDia(Date desdeElDia)
    {
        this.desdeElDia = desdeElDia;
    }

    public Date getHastaElDia()
    {
        return hastaElDia;
    }

    public void setHastaElDia(Date hastaElDia)
    {
        this.hastaElDia = hastaElDia;
    }

    public Evento divide() throws EventoNoDivisibleException
    {
        if (duraMenosDeUnaHora())
        {
            throw new EventoNoDivisibleException();
        }

        Evento nuevoEvento = this.clonar();
        nuevoEvento.retrasaHoraInicioAMitadDuracion();
        this.reduceDuracionALaMitad();

        nuevoEvento.propagaRangoHorarioAEventosDetalle();
        this.propagaRangoHorarioAEventosDetalle();
        return nuevoEvento;
    }

    private boolean duraMenosDeUnaHora()
    {
        return getDuracionEnMilisegundos() < UNA_HORA_EN_MILISEGUNDOS;
    }

    private void propagaRangoHorarioAEventosDetalle()
    {
        for (EventoDetalle eventoDetalle : this.getEventosDetalle())
        {
            eventoDetalle.estableceHoraYMinutosInicio(this.getInicio());
            eventoDetalle.estableceHoraYMinutosFin(this.getFin());
        }
    }

    private void reduceDuracionALaMitad()
    {
        Date nuevaHoraFin = new Date(this.getFin().getTime() - this.getDuracionEnMilisegundos() / 2);
        this.setFin(nuevaHoraFin);
    }

    private Long getDuracionEnMilisegundos()
    {
        return this.getFin().getTime() - this.getInicio().getTime();
    }

    private void retrasaHoraInicioAMitadDuracion()
    {
        Date nuevaHoraInicio = new Date(this.getInicio().getTime()
                + this.getDuracionEnMilisegundos() / 2);
        this.setInicio(nuevaHoraInicio);
    }

    private Evento clonar()
    {
        Evento nuevo = new Evento();
        nuevo.setCalendario(this.getCalendario());
        nuevo.setTitulo(this.getTitulo());
        nuevo.setObservaciones(this.getObservaciones());
        nuevo.setInicio(this.getInicio());
        nuevo.setFin(this.getFin());
        nuevo.setDetalleManual(this.hasDetalleManual());
        nuevo.setNumeroIteraciones(this.getNumeroIteraciones());
        nuevo.setRepetirCadaSemanas(this.getRepetirCadaSemanas());
        nuevo.setDesdeElDia(this.getDesdeElDia());
        nuevo.setHastaElDia(this.getHastaElDia());
        nuevo.setAula(this.getAula());
        nuevo.setAsignaturas(this.getAsignaturas());
        nuevo.setSemestre(this.getSemestre());
        nuevo.setGrupoId(this.getGrupoId());
        nuevo.setSubgrupoId(this.getSubgrupoId());
        nuevo.setPlazas(this.getPlazas());
        nuevo.setCreditos(this.getCreditos());
        nuevo.setCreditosComputables(this.getCreditosComputables());
        nuevo.setTextoAsignaturasComunes(this.getTextoAsignaturasComunes());
        nuevo.setComentarios(this.getComentarios());

        for (EventoDetalle eventoDetalle : this.getEventosDetalle())
        {
            EventoDetalle nuevoEventoDetalle = eventoDetalle.clonar();
            nuevoEventoDetalle.setEvento(nuevo);
        }

        for (ProfesorDetalle profesorDetalle : this.getProfesoresDetalle())
        {
            ProfesorDetalle nuevoProfesorDetalle = profesorDetalle.clonar();
            nuevoProfesorDetalle.setEvento(nuevo);
            nuevo.addProfesorDetalle(nuevoProfesorDetalle);
        }

        return nuevo;
    }

    public void addEventoDetalle(EventoDetalle nuevoEventoDetalle)
    {
        this.eventosDetalle.add(nuevoEventoDetalle);
    }

    public void addProfesorDetalle(ProfesorDetalle profesorDetalle)
    {
        this.profesoresDetalle.add(profesorDetalle);
    }

    public EventoDetalle creaDetalleEnFecha(Date fecha)
    {
        EventoDetalle detalle = new EventoDetalle(this, fecha);
        return detalle;
    }

    public List<Asignatura> getAsignaturas()
    {
        return asignaturas;
    }

    public void setAsignaturas(List<Asignatura> asignaturas)
    {
        Collections.sort(asignaturas, (a1, a2) -> a1.getId().compareTo(a2.getId()));
        this.asignaturas = asignaturas;
    }

    public Asignatura getAsignaturaDelEstudio(Long estudioId)
    {
        for (Asignatura asig : asignaturas)
        {
            if (asig.getEstudio().getId().equals(estudioId))
            {
                return asig;
            }
        }

        return null;
    }

    public Semestre getSemestre()
    {
        return semestre;
    }

    public void setSemestre(Semestre semestre)
    {
        this.semestre = semestre;
    }

    public String getGrupoId()
    {
        return grupoId;
    }

    public void setGrupoId(String grupoId)
    {
        this.grupoId = grupoId;
    }

    public Long getSubgrupoId()
    {
        return subgrupoId;
    }

    public void setSubgrupoId(Long subgrupoId)
    {
        this.subgrupoId = subgrupoId;
    }

    private Integer convierteDiaSemaanDeCalendar(Integer diaCalendario)
    {
        switch (diaCalendario)
        {
        case Calendar.MONDAY:
            return LUNES;
        case Calendar.TUESDAY:
            return MARTES;
        case Calendar.WEDNESDAY:
            return MIERCOLES;
        case Calendar.THURSDAY:
            return JUEVES;
        case Calendar.FRIDAY:
            return VIERNES;
        case Calendar.SATURDAY:
            return SABADO;
        case Calendar.SUNDAY:
            return DOMINGO;
        }

        return 0;
    }

    public Integer getDia()
    {
        if (this.getInicio() == null)
        {
            return null;
        }

        Calendar actual = Calendar.getInstance(new Locale("es", "ES"));
        actual.setTime(this.getInicio());

        return convierteDiaSemaanDeCalendar(actual.get(Calendar.DAY_OF_WEEK));
    }

    public String getDiaAsString()
    {
        if (this.getInicio() == null)
        {
            return null;
        }

        Integer dia = getDia();
        switch (dia)
        {
        case LUNES:
            return "Dilluns";
        case MARTES:
            return "Dimarts";
        case MIERCOLES:
            return "Dimecres";
        case JUEVES:
            return "Dijous";
        case VIERNES:
            return "Divendres";
        case SABADO:
            return "Dissabte";
        case DOMINGO:
            return "Diumenge";
        }

        return "";
    }

    public Long getPlazas()
    {
        return plazas;
    }

    public void setPlazas(Long plazas)
    {
        this.plazas = plazas;
    }

    public List<EventoDetalle> getEventosDetalle()
    {
        return eventosDetalle;
    }

    public void setEventosDetalle(List<EventoDetalle> eventosDetalle)
    {
        this.eventosDetalle = eventosDetalle;
    }

    public void desplanificar()
    {
        this.setInicio(null);
        this.setFin(null);
        this.setAula(null);
        this.setDetalleManual(false);
    }

    public void actualizaAula(Aula aula) throws AulaNoAsignadaAEstudioDelEventoException
    {
        if (!aulaAsignadaAAlgunEstudioDelEvento(aula))
        {
            throw new AulaNoAsignadaAEstudioDelEventoException();
        }

        this.aula = aula;
    }

    public void desasignaAula()
    {
        aula = null;
    }

    private boolean aulaAsignadaAAlgunEstudioDelEvento(Aula aula)
    {

        List<Long> estudiosIds = new ArrayList<Long>();

        for (Asignatura asignatura : asignaturas)
        {
            estudiosIds.add(asignatura.getEstudio().getId());
        }

        for (AulaPlanificacion planificacion : aula.getPlanificacion())
        {
            Estudio estudio = planificacion.getEstudio();

            if (estudiosIds.contains(estudio.getId())
                    && semestre.getSemestre().equals(planificacion.getSemestre().getSemestre()))
            {
                return true;
            }
        }
        return false;
    }

    public void vaciaEventosDetalle()
    {
        getEventosDetalle().clear();
    }

    public String getAsignaturasComunes(Long estudioId)
    {
        if (!tieneComunes())
        {
            return "";
        }

        String comunes = "";

        for (Asignatura asignatura : asignaturas)
        {
            if (!asignatura.getEstudio().getId().equals(estudioId))
            {
                comunes = comunes + ", " + asignatura.getId();
            }
        }

        if (comunes.length() > 2)
        {
            comunes = comunes.substring(2);
        }

        return comunes;
    }

    public void compruebaDentroDeLosRangosHorarios(List<RangoHorario> rangosHorarios)
            throws EventoFueraDeRangoException
    {
        for (RangoHorario rangoHorario : rangosHorarios)
        {
            try
            {
                rangoHorario.compruebaEventoDentroDeRango(this);
            }
            catch (RangoHorarioFueradeLimites e)
            {
                throw new EventoFueraDeRangoException(grupoId,
                        rangoHorario.getRangoHorarioAsString());
            }
        }

    }

    public void compruebaDentroFechasSemestre(Date fechaInicioSemestre, Date fechaFinSemestre)
            throws EventoFueraDeFechasSemestreException
    {
        if (!dentroDelRangoDeFechas(fechaInicioSemestre, fechaFinSemestre))
        {
            throw new EventoFueraDeFechasSemestreException();
        }

    }

    private boolean dentroDelRangoDeFechas(Date fechaInicio, Date fechaFin)
    {
        return getInicio().after(fechaInicio) && getFin().before(fechaFin);

    }

    public String getDescripcionConGrupoYComunes()
    {
        String tituloEvento = "";

        for (Asignatura asignatura : this.asignaturas)
        {
            tituloEvento = tituloEvento + " " + asignatura.getId();
        }

        tituloEvento += " " + this.grupoId + " " + this.calendario.getLetraId() + this.subgrupoId;

        return tituloEvento;
    }

    public Aula getAula()
    {
        return aula;
    }

    public void setAula(Aula aula)
    {
        this.aula = aula;
    }

    public Circuito getCircuito()
    {
        return circuito;
    }

    public void setCircuito(Circuito circuito)
    {
        this.circuito = circuito;
    }

    public String getComentarios()
    {
        return comentarios;
    }

    public void setComentarios(String comentarios)
    {
        this.comentarios = comentarios;
    }

    public String getTipoEstudioId()
    {
        return tipoEstudioId;
    }

    public void setTipoEstudioId(String tipoEstudioId)
    {
        this.tipoEstudioId = tipoEstudioId;
    }

    public CalendarioCircuito getCalendarioCircuito()
    {
        return calendarioCircuito;
    }

    public void setCalendarioCircuito(CalendarioCircuito calendarioCircuito)
    {
        this.calendarioCircuito = calendarioCircuito;
    }

    public void unsetCircuito()
    {
        this.circuito = null;
    }

    public String getListaClases()
    {
        String listaClases = "";
        for (Asignatura asignatura : this.getAsignaturas())
        {
            Estudio estudio = asignatura.getEstudio();
            listaClases += this.getDescripcionParaUnEstudio(estudio.getId()) + ", ";
        }

        if (!listaClases.isEmpty())
        {
            return listaClases.substring(0, listaClases.length() - 2);
        }
        else
        {
            return "";
        }
    }

    public String getDescripcionParaUnaAsignatura()
    {
        if (this.getAsignaturas() == null || this.getAsignaturas().size() == 0)
        {
            return "";
        }
        Asignatura asignatura = this.getAsignaturas().get(0);

        String texto;

        if (tieneComunes())
        {
            texto = MessageFormat.format("{0} {1} {2}{3} - C", asignatura.getCodigoComun(),
                    grupoId, getCalendario().getLetraId(), subgrupoId);
        }
        else
        {
            texto = MessageFormat.format("{0} {1} {2}{3}", asignatura.getId(), grupoId,
                    getCalendario().getLetraId(), subgrupoId);
        }

        if (getAula() != null)
        {
            texto = MessageFormat.format("{0} {1}", texto, getAula().getCodigo());
        }

        return texto;
    }

    public List<Profesor> getProfesores()
    {
        return profesores;
    }

    public void setProfesores(List<Profesor> profesores)
    {
        this.profesores = profesores;
    }

    public List<ProfesorDetalle> getProfesoresDetalle()
    {
        return profesoresDetalle;
    }

    public void setProfesoresDetalle(List<ProfesorDetalle> profesoresDetalle)
    {
        this.profesoresDetalle = profesoresDetalle;
    }

    public CalendarioAsignatura getCalendarioAsignatura()
    {
        return calendarioAsignatura;
    }

    public void setCalendarioAsignatura(CalendarioAsignatura calendarioAsignatura)
    {
        this.calendarioAsignatura = calendarioAsignatura;
    }

    public BigDecimal getCreditos()
    {
        return creditos;
    }

    public void setCreditos(BigDecimal creditos)
    {
        this.creditos = creditos;
    }

    public BigDecimal getCreditosComputables()
    {
        return creditosComputables;
    }

    public void setCreditosComputables(BigDecimal creditosComputables)
    {
        this.creditosComputables = creditosComputables;
    }

    public String getTextoAsignaturasComunes()
    {
        return textoAsignaturasComunes;
    }

    public void setTextoAsignaturasComunes(String textoAsignaturasComunes)
    {
        this.textoAsignaturasComunes = textoAsignaturasComunes;
    }

    public Boolean tieneDetalleEnFecha(Date fecha)
    {
        for (EventoDetalle eventoDetalle : eventosDetalle)
        {
            if (DateUtils.isSameDay(eventoDetalle.getInicio(), fecha))
            {
                return true;
            }
        }
        return false;
    }
}
