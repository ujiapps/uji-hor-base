package es.uji.apps.hor.model;

public class AsignaturaAgrupacion
{
    private Long id;
    private Asignatura asignatura;
    private Agrupacion agrupacion;
    private String grupoId;
    private Long subgrupoId;
    private String tipoSubgrupo;

    public AsignaturaAgrupacion()
    {
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Asignatura getAsignatura()
    {
        return asignatura;
    }

    public void setAsignatura(Asignatura asignatura)
    {
        this.asignatura = asignatura;
    }

    public Agrupacion getAgrupacion()
    {
        return agrupacion;
    }

    public void setAgrupacion(Agrupacion agrupacion)
    {
        this.agrupacion = agrupacion;
    }

    public String getGrupoId()
    {
        return grupoId;
    }

    public void setGrupoId(String grupoId)
    {
        this.grupoId = grupoId;
    }

    public Long getSubgrupoId()
    {
        return subgrupoId;
    }

    public void setSubgrupoId(Long subgrupoId)
    {
        this.subgrupoId = subgrupoId;
    }

    public String getTipoSubgrupo()
    {
        return tipoSubgrupo;
    }

    public void setTipoSubGrupo(String tipoSubgrupo)
    {
        this.tipoSubgrupo = tipoSubgrupo;
    }

    public AsignaturaAgrupacion clonar()
    {
        AsignaturaAgrupacion nuevo = new AsignaturaAgrupacion();
        nuevo.setId(id);
        nuevo.setAsignatura(new Asignatura(asignatura.getId()));
        nuevo.setAgrupacion(new Agrupacion(agrupacion.getId()));
        nuevo.setGrupoId(grupoId);
        nuevo.setTipoSubGrupo(tipoSubgrupo);
        nuevo.setSubgrupoId(subgrupoId);
        return nuevo;
    }
}