package es.uji.apps.hor.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.hor.db.DetalleSemestreDTO;
import es.uji.apps.hor.db.FechaEstudioDTO;
import es.uji.apps.hor.db.QDetalleSemestreDTO;
import es.uji.apps.hor.db.QEstudioDTO;
import es.uji.apps.hor.db.QFechaEstudioDTO;
import es.uji.apps.hor.db.QSemestreDTO;
import es.uji.apps.hor.db.QTipoEstudioDTO;
import es.uji.apps.hor.db.SemestreDTO;
import es.uji.apps.hor.db.TipoEstudioDTO;
import es.uji.apps.hor.model.Semestre;
import es.uji.apps.hor.model.SemestreDetalle;
import es.uji.apps.hor.model.TipoEstudio;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class SemestresDetalleDAODatabaseImpl extends BaseDAODatabaseImpl implements
        SemestresDetalleDAO
{

    @Override
    public List<SemestreDetalle> getSemestresDetalleTodos()
    {
        JPAQuery query = new JPAQuery(entityManager);
        QDetalleSemestreDTO detalleSemestre = QDetalleSemestreDTO.detalleSemestreDTO;
        QSemestreDTO semestre = QSemestreDTO.semestreDTO;
        QTipoEstudioDTO tipoEstudio = QTipoEstudioDTO.tipoEstudioDTO;

        List<DetalleSemestreDTO> queryResult = query.from(detalleSemestre)
                .join(detalleSemestre.semestre, semestre).fetch()
                .join(detalleSemestre.tiposEstudio, tipoEstudio).fetch().list(detalleSemestre);

        List<SemestreDetalle> semestresDetalle = new ArrayList<SemestreDetalle>();
        for (DetalleSemestreDTO detalle : queryResult)
        {
            semestresDetalle.add(convierteDetalleDTOEnDetalleModelo(detalle));
        }
        return semestresDetalle;
    }

    private SemestreDetalle convierteDetalleDTOEnDetalleModelo(DetalleSemestreDTO detalleDTO)
    {
        SemestreDetalle detalleModelo = new SemestreDetalle(detalleDTO.getId(),
                convierteSemestreDTOenSemesetreModelo(detalleDTO.getSemestre()),
                convierteTiposEstudioDTOenTiposEstudioModelo(detalleDTO.getTiposEstudio()),
                detalleDTO.getFechaInicio(), detalleDTO.getFechaFin(),
                detalleDTO.getNumeroSemanas());
        detalleModelo.setFechaExamenesInicio(detalleDTO.getFechaExamenesInicio());
        detalleModelo.setFechaExamenesInicioC2(detalleDTO.getFechaExamenesInicioC2());
        detalleModelo.setFechaExamenesFinC2(detalleDTO.getFechaExamenesFinC2());
        detalleModelo.setFechaExamenesFin(detalleDTO.getFechaExamenesFin());
        return detalleModelo;
    }

    private Semestre convierteSemestreDTOenSemesetreModelo(SemestreDTO semestreDTO)
    {
        return new Semestre(semestreDTO.getId(), semestreDTO.getNombre());
    }

    private TipoEstudio convierteTiposEstudioDTOenTiposEstudioModelo(TipoEstudioDTO tipoEstudioDTO)
    {
        return new TipoEstudio(tipoEstudioDTO.getId(), tipoEstudioDTO.getNombre());
    }

    @Override
    public List<SemestreDetalle> getSemestresDetallesPorEstudioIdYSemestreId(Long estudioId,
            Long semestreId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QDetalleSemestreDTO detalleSemestre = QDetalleSemestreDTO.detalleSemestreDTO;
        QTipoEstudioDTO tipoEstudio = QTipoEstudioDTO.tipoEstudioDTO;
        QEstudioDTO estudio = QEstudioDTO.estudioDTO;

        List<DetalleSemestreDTO> queryResult = query
                .from(detalleSemestre, estudio)
                .join(estudio.tipoEstudio, tipoEstudio)
                .join(detalleSemestre.tiposEstudio, tipoEstudio)
                .fetch()
                .where(estudio.id.eq(estudioId).and(
                        estudio.tipoEstudio.eq(tipoEstudio).and(
                                detalleSemestre.semestre.id.eq(semestreId)))).list(detalleSemestre);

        List<SemestreDetalle> semestresDetalle = new ArrayList<SemestreDetalle>();
        for (DetalleSemestreDTO detalle : queryResult)
        {
            semestresDetalle.add(convierteDetalleDTOEnDetalleModelo(detalle));
        }
        return semestresDetalle;
    }

    @Override
    public List<SemestreDetalle> getSemestresDetallesPorEstudioIdYSemestreIdYCurso(Long estudioId,
            Long semestreId, Long cursoAcademico)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QFechaEstudioDTO qFechaEstudioDTO = QFechaEstudioDTO.fechaEstudioDTO;
        QTipoEstudioDTO qTipoEstudioDTO = QTipoEstudioDTO.tipoEstudioDTO;
        QSemestreDTO qSemestreDTO = QSemestreDTO.semestreDTO;

        List<FechaEstudioDTO> result = query
                .from(qFechaEstudioDTO)
                .join(qFechaEstudioDTO.tiposEstudio, qTipoEstudioDTO)
                .fetch()
                .join(qFechaEstudioDTO.semestre, qSemestreDTO)
                .fetch()
                .where(qFechaEstudioDTO.estudioId.eq(estudioId)
                        .and(qFechaEstudioDTO.semestre.id.eq(semestreId))
                        .and(qFechaEstudioDTO.cursoAcademicoId.eq(cursoAcademico)))
                .list(qFechaEstudioDTO);

        List<SemestreDetalle> semestresDetalle = new ArrayList<SemestreDetalle>();

        for (FechaEstudioDTO detalle : result)
        {
            semestresDetalle.add(convierteFechaEstudioDTOEnDetalleModelo(detalle));
        }

        return semestresDetalle;
    }

    private SemestreDetalle convierteFechaEstudioDTOEnDetalleModelo(FechaEstudioDTO fechaEstudioDTO)
    {
        SemestreDetalle detalleModelo = new SemestreDetalle(fechaEstudioDTO.getId(),
                convierteSemestreDTOenSemesetreModelo(fechaEstudioDTO.getSemestre()),
                convierteTiposEstudioDTOenTiposEstudioModelo(fechaEstudioDTO.getTiposEstudio()),
                fechaEstudioDTO.getFechaInicio(), fechaEstudioDTO.getFechaFin(),
                fechaEstudioDTO.getNumeroSemanas());
        detalleModelo.setFechaExamenesInicio(fechaEstudioDTO.getFechaExamenesInicio());
        detalleModelo.setFechaExamenesInicioC2(fechaEstudioDTO.getFechaExamenesInicioC2());
        detalleModelo.setFechaExamenesFinC2(fechaEstudioDTO.getFechaExamenesFinC2());
        detalleModelo.setFechaExamenesFin(fechaEstudioDTO.getFechaExamenesFin());
        return detalleModelo;
    }

    private DetalleSemestreDTO convierteSemestreDetalleADetalleSemestreDTO(
            SemestreDetalle semestreDetalle)
    {
        SemestreDTO semestreDTO = new SemestreDTO();
        semestreDTO.setId(semestreDetalle.getSemestre().getSemestre());
        semestreDTO.setNombre(semestreDetalle.getSemestre().getNombre());

        TipoEstudioDTO tipoEstudioDTO = new TipoEstudioDTO();
        tipoEstudioDTO.setId(semestreDetalle.getTipoEstudio().getId());
        tipoEstudioDTO.setNombre(semestreDetalle.getTipoEstudio().getNombre());
        tipoEstudioDTO.setOrden(new Long(semestreDetalle.getTipoEstudio().getOrden()));

        DetalleSemestreDTO detalleSemestreDTO = new DetalleSemestreDTO();
        detalleSemestreDTO.setFechaExamenesFin(semestreDetalle.getFechaExamenesFin());
        detalleSemestreDTO.setFechaExamenesInicio(semestreDetalle.getFechaExamenesInicio());
        detalleSemestreDTO.setFechaFin(semestreDetalle.getFechaFin());
        detalleSemestreDTO.setFechaInicio(semestreDetalle.getFechaInicio());
        detalleSemestreDTO.setNumeroSemanas(semestreDetalle.getNumeroSemanas());
        detalleSemestreDTO.setSemestre(semestreDTO);
        detalleSemestreDTO.setTiposEstudio(tipoEstudioDTO);

        return detalleSemestreDTO;
    }

    @Override
    public SemestreDetalle insert(SemestreDetalle semestreDetalle)
    {
        DetalleSemestreDTO detalleSemestreDTO = convierteSemestreDetalleADetalleSemestreDTO(semestreDetalle);
        detalleSemestreDTO = this.insert(detalleSemestreDTO);
        return convierteDetalleDTOEnDetalleModelo(detalleSemestreDTO);
    }

    @Override
    public List<SemestreDetalle> getRangoFechasSemestresDetallesPorEstudioIdYSemestreId(
            Long estudioId, Long semestreId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QFechaEstudioDTO qFechaEstudioDTO = QFechaEstudioDTO.fechaEstudioDTO;

        query.from(qFechaEstudioDTO).where(
                qFechaEstudioDTO.semestre.id.eq(semestreId).and(
                        qFechaEstudioDTO.estudioId.eq(estudioId)));

        List<FechaEstudioDTO> fechaEstudioDTOs = query.list(qFechaEstudioDTO);
        List<SemestreDetalle> semestreDetalles = new ArrayList<>();

        for (FechaEstudioDTO fechaEstudioDTO: fechaEstudioDTOs) {
            semestreDetalles.add(convierteFechaEstudioDTOEnDetalleModelo(fechaEstudioDTO));
        }

        return semestreDetalles;
    }

}
