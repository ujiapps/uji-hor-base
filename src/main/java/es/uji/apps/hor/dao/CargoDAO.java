package es.uji.apps.hor.dao;

import es.uji.apps.hor.model.Cargo;
import es.uji.commons.db.BaseDAO;

import java.util.List;

public interface CargoDAO extends BaseDAO
{
    List<Cargo> getCargos(Long connectedUserId);
}
