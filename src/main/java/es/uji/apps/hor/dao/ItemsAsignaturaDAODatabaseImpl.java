package es.uji.apps.hor.dao;

import es.uji.apps.hor.db.ItemDTO;
import es.uji.apps.hor.db.ItemsAsignaturaDTO;
import es.uji.apps.hor.model.Asignatura;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class ItemsAsignaturaDAODatabaseImpl extends BaseDAODatabaseImpl implements ItemsAsignaturaDAO
{
    @Override
    @Transactional
    public Asignatura insertItemAsignatura(Asignatura asignatura, Long itemId) {
        // Creamos una nueva asignatura
        ItemsAsignaturaDTO asignaturaDTO = new ItemsAsignaturaDTO();

        asignaturaDTO.setAsignaturaId(asignatura.getId());
        asignaturaDTO.setNombreAsignatura(asignatura.getNombre());
        asignaturaDTO.setEstudio(asignatura.getEstudio().getNombre());
        
        asignaturaDTO.setEstudioId(asignatura.getEstudio().getId());

        ItemDTO itemDTO = new ItemDTO();
        itemDTO.setId(itemId);
        asignaturaDTO.setItem(itemDTO);

        insert(asignaturaDTO);

        return asignatura;

    }
}
