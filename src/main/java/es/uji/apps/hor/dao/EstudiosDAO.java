package es.uji.apps.hor.dao;

import java.util.List;

import es.uji.apps.hor.model.Estudio;
import es.uji.commons.db.BaseDAO;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

public interface EstudiosDAO extends BaseDAO
{
    List<Estudio> getEstudios();

    List<Estudio> getEstudiosVisiblesPorUsuario(Long userId);

    List<Estudio> getEstudiosByCentroId(Long centroId);

    Estudio insert(Estudio estudio);

    Estudio getEstudioById(Long estudioId) throws RegistroNoEncontradoException;

    List<Estudio> getEstudiosByAgrupacionId(Long agrupacionId);

    Estudio getEstudioByAsignaturaId(String asignaturaId);

    List<Estudio> getEstudiosByEvento(Long itemId);

    List<Estudio> getEstudiosAbiertosConsulta();

    Boolean isEstudioAbiertoConsulta(Long estudioId);
}
