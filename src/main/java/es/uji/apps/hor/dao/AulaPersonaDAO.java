package es.uji.apps.hor.dao;

import es.uji.apps.hor.model.Aula;
import es.uji.apps.hor.model.Centro;
import es.uji.commons.db.BaseDAO;

public interface AulaPersonaDAO extends BaseDAO
{
    Aula insertAulaPersona(Long personaId, Aula aula, Centro centro);
}
