package es.uji.apps.hor.dao;

import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import com.mysema.query.types.QTuple;

import es.uji.apps.hor.EventoDetalleSinEventoException;
import es.uji.apps.hor.db.*;
import es.uji.apps.hor.model.*;
import es.uji.apps.hor.services.FechaService;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Repository
public class EventosDAODatabaseImpl extends BaseDAODatabaseImpl implements EventosDAO
{

    private Calendario obtenerCalendarioAsociadoPorTipoSubgrupo(ItemDTO itemDTO)
    {
        String tipoSubgrupoId = itemDTO.getTipoSubgrupoId();
        TipoSubgrupo tipoSubgrupo = TipoSubgrupo.valueOf(tipoSubgrupoId);

        return new Calendario(tipoSubgrupo.getCalendarioAsociado(), tipoSubgrupo.getNombre());
    }

    @Override
    public List<Evento> getEventosSemanaGenerica(Long estudioId, Long cursoId, Long semestreId,
                                                 List<String> gruposIds, List<String> asignaturasIds, List<Long> calendariosIds)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QItemDTO item = QItemDTO.itemDTO;
        QItemsAsignaturaDTO asignatura = QItemsAsignaturaDTO.itemsAsignaturaDTO;

        List<String> tiposCalendarios = TipoSubgrupo.getTiposSubgrupos(calendariosIds);

        query.from(item).join(item.itemsAsignaturas, asignatura)
                .where(asignatura.estudioId.eq(estudioId)
                        .and(asignatura.cursoId.eq(cursoId).and(item.semestre.id.eq(semestreId))
                                .and(item.grupoId.in(gruposIds)).and(item.diaSemana.isNotNull())
                                .and(item.tipoSubgrupoId.in(tiposCalendarios))));

        if (!asignaturasIds.isEmpty())
        {
            query.where(asignatura.asignaturaId.in(asignaturasIds));
        }

        return getEventosConTodasLasAsignaturasByListaEventos(query.list(item));
    }

    @Override
    public List<Evento> getEventosEditablesEnSemanaDetalleByEventoIds(List<Long> eventoIds)
    {
        if (eventoIds.isEmpty())
        {
            return new ArrayList<>();
        }
        JPAQuery query = new JPAQuery(entityManager);

        QItemDTO item = QItemDTO.itemDTO;
        QItemDetalleDTO itemsDetalle = QItemDetalleDTO.itemDetalleDTO;

        query.from(item).join(item.itemsDetalles, itemsDetalle).fetch()
                .where(item.id.in(eventoIds));

        List<Evento> listaEventos = new ArrayList<>();
        for (ItemDTO itemDTO : query.list(item))
        {
            Evento evento = creaEventoDesdeItemDTO(itemDTO);
            if (evento.getEventosDetalle().size() == 1)
            {
                listaEventos.add(evento);
            }
        }
        return listaEventos;
    }

    @Override
    public List<Evento> getEventosByListaAsignaturasYSemestre(List<String> asignaturasIds,
                                                              Long semestreId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QItemDTO item = QItemDTO.itemDTO;
        QItemsAsignaturaDTO asignatura = QItemsAsignaturaDTO.itemsAsignaturaDTO;
        QItemProfesorDTO itemProfesor = QItemProfesorDTO.itemProfesorDTO;

        query.from(item).join(item.itemsAsignaturas, asignatura).fetch()
                .leftJoin(item.itemsProfesores, itemProfesor).fetch()
                .where(asignatura.asignaturaId.in(asignaturasIds)
                        .and(item.semestre.id.eq(semestreId)).and(item.diaSemana.isNotNull()));

        return query.distinct().list(item).stream().map(itemDTO -> creaEventoDesdeItemDTO(itemDTO))
                .collect(Collectors.toList());
    }

    private List<Evento> getEventosConTodasLasAsignaturasByListaEventos(List<ItemDTO> itemsDTO)
    {
        if (itemsDTO.isEmpty())
        {
            return new ArrayList<>();
        }

        List<Long> itemIds = itemsDTO.stream().map(itemDTO -> itemDTO.getId())
                .collect(Collectors.toList());

        JPAQuery query = new JPAQuery(entityManager);
        QItemDTO item = QItemDTO.itemDTO;
        QItemsAsignaturaDTO asignatura = QItemsAsignaturaDTO.itemsAsignaturaDTO;
        QItemProfesorDTO itemProfesor = QItemProfesorDTO.itemProfesorDTO;

        query.from(item).join(item.itemsAsignaturas, asignatura).fetch()
                .leftJoin(item.itemsProfesores, itemProfesor).fetch().where(item.id.in(itemIds));

        List<Evento> eventos = query.list(item).stream()
                .map(itemDTO -> creaEventoDesdeItemDTO(itemDTO)).collect(Collectors.toList());

        List<EventoDetalle> eventosDetalle = getEventosDetalleByListaEventos(itemIds);
        for (Evento evento : eventos)
        {
            List<EventoDetalle> detalles = eventosDetalle.stream()
                    .filter(ed -> ed.getId().equals(evento.getId())).collect(Collectors.toList());
            evento.setEventosDetalle(detalles);
        }

        return eventos;
    }

    private List<EventoDetalle> getEventosDetalleByListaEventos(List<Long> itemIds)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QItemDetalleCompletoDTO itemDetalleCompleto = QItemDetalleCompletoDTO.itemDetalleCompletoDTO;
        query.from(itemDetalleCompleto).where(
                itemDetalleCompleto.id.in(itemIds).and(itemDetalleCompleto.docencia.eq("S")));

        return query.list(itemDetalleCompleto).stream()
                .map(dcDto -> creaEventoDetalleDesdeItemDetalleCompletoDTO(dcDto))
                .collect(Collectors.toList());
    }

    private EventoDetalle creaEventoDetalleDesdeItemDetalleCompletoDTO(
            ItemDetalleCompletoDTO itemDetalleCompletoDTO)
    {
        EventoDetalle eventoDetalle = new EventoDetalle();
        eventoDetalle.setId(itemDetalleCompletoDTO.getId());
        eventoDetalle.setInicio(itemDetalleCompletoDTO.getFecha());
        eventoDetalle.setFin(itemDetalleCompletoDTO.getFecha());
        return eventoDetalle;
    }

    private EventoDetalle creaEventoDetalleDesdeItemDetalleDTO(ItemDetalleDTO itemDetalleDTO)
    {
        EventoDetalle eventoDetalle = new EventoDetalle();

        eventoDetalle.setId(itemDetalleDTO.getId());
        eventoDetalle.setDescripcion(itemDetalleDTO.getDescripcion());
        eventoDetalle.setInicio(itemDetalleDTO.getInicio());

        eventoDetalle.setFin(itemDetalleDTO.getFin());

        List<Profesor> profesores = new ArrayList<>();
        for (ItemProfesorDTO itemProfesor : itemDetalleDTO.getItem().getItemsProfesores())
        {
            Profesor profesor = new Profesor();
            profesor.setId(itemProfesor.getProfesor().getId());
            profesor.setNombre(itemProfesor.getProfesor().getNombre());
            profesores.add(profesor);
        }
        eventoDetalle.setProfesores(profesores);

        return eventoDetalle;
    }

    public Asignatura creaAsignaturasDesdeItemAsignaturaDTO(ItemsAsignaturaDTO asig,
                                                            ItemDTO itemDTO)
    {

        Estudio estudio = new Estudio();
        TipoEstudio tipoEstudio = new TipoEstudio();
        tipoEstudio.setNombre(itemDTO.getTipoEstudio());
        tipoEstudio.setId(itemDTO.getTipoEstudioId());

        estudio.setId(asig.getEstudioId());
        estudio.setNombre(asig.getEstudio());
        estudio.setTipoEstudio(tipoEstudio);

        Asignatura asignatura = new Asignatura();

        asignatura.setComun(itemDTO.getComun() == 1);
        if (itemDTO.getComun() > 0)
        {
            asignatura.setComunes(itemDTO.getComunes());
        }

        asignatura.setNombre(asig.getNombreAsignatura());
        asignatura.setId(asig.getAsignaturaId());
        asignatura.setCursoId(asig.getCursoId());
        asignatura.setCaracter(asig.getCaracter());
        asignatura.setCaracterId(asig.getCaracterId());
        asignatura.setEstudio(estudio);
        asignatura.setPorcentajeComun(itemDTO.getPorcentajeComun());
        asignatura.setTipoAsignatura(itemDTO.getTipoAsignatura());
        asignatura.setTipoAsignaturaId(itemDTO.getTipoAsignaturaId());
        return asignatura;
    }

    private Evento creaEventoDesdeItemDTO(ItemDTO itemDTO)
    {
        Calendario calendario = obtenerCalendarioAsociadoPorTipoSubgrupo(itemDTO);

        Evento evento = new Evento();
        evento.setId(itemDTO.getId());
        evento.setCalendario(calendario);
        evento.setTextoAsignaturasComunes(itemDTO.getComunes());
        evento.setComentarios(itemDTO.getComentarios());
        evento.setTipoEstudioId(itemDTO.getTipoEstudioId());
        evento.setTitulo(itemDTO.getNombreItem());

        if (itemDTO.getHoraInicio() != null && itemDTO.getHoraFin() != null)
        {
            evento.setInicio(FechaService.getFechaSemanaGenerica(itemDTO.getDiaSemana().getId(), itemDTO.getHoraInicio()));
            evento.setFin(FechaService.getFechaSemanaGenerica(itemDTO.getDiaSemana().getId(), itemDTO.getHoraFin()));
        }

        if (itemDTO.getItemsProfesores() != null)
        {
            for (ItemProfesorDTO itemProfesorDTO : itemDTO.getItemsProfesores())
            {
                ProfesorDTO profesorDTO = itemProfesorDTO.getProfesor();

                Profesor profesor = new Profesor();
                profesor.setId(profesorDTO.getId());
                profesor.setNombre(profesorDTO.getNombre());

                if (evento.getProfesores() == null)
                {
                    evento.setProfesores(new ArrayList<Profesor>());
                }
                evento.getProfesores().add(profesor);

                ProfesorDetalle profesorDetalle = new ProfesorDetalle();
                profesorDetalle.setId(itemProfesorDTO.getId());

                if (itemProfesorDTO.getCreditos() != null)
                {
                    profesorDetalle.setCreditos(
                            itemProfesorDTO.getCreditos().setScale(2, BigDecimal.ROUND_HALF_UP));
                }

                if (itemProfesorDTO.getCreditosComputables() != null)
                {
                    profesorDetalle.setCreditosComputables(itemProfesorDTO.getCreditosComputables()
                            .setScale(2, BigDecimal.ROUND_HALF_UP));
                }

                profesorDetalle.setDetalleManual(itemProfesorDTO.getDetalleManual());
                profesorDetalle.setProfesor(profesor);

                if (evento.getProfesoresDetalle() == null)
                {
                    evento.setProfesoresDetalle(new ArrayList<ProfesorDetalle>());
                }
                evento.getProfesoresDetalle().add(profesorDetalle);

                // Créditos calculados por vista
                ItemsCreditosDetalleDTO itemsCreditosDetalleDTO = itemDTO.getItemsCreditosDetalle().stream().filter(cd -> cd.getId().equals(profesor.getId())).findFirst()
                        .orElse(null);
                if (itemsCreditosDetalleDTO != null)
                {
                    profesorDetalle.setCreditosCalculados(itemsCreditosDetalleDTO.getCrdFinal());
                }

                List<EventoDetalle> eventosDetalle = new ArrayList<>();
                if (itemProfesorDTO.getItemsProfesor() != null)
                {
                    for (ItemDetalleProfesorDTO itemDetalleProfesorDTO : itemProfesorDTO
                            .getItemsProfesor())
                    {
                        EventoDetalle eventoDetalle = new EventoDetalle();
                        eventoDetalle.setEvento(evento);
                        eventoDetalle.setId(itemDetalleProfesorDTO.getId());
                        eventoDetalle
                                .setInicio(itemDetalleProfesorDTO.getItemDetalle().getInicio());
                        eventoDetalle.setFin(itemDetalleProfesorDTO.getItemDetalle().getFin());
                        eventosDetalle.add(eventoDetalle);
                    }
                }
                profesorDetalle.setEventosDetalle(eventosDetalle);
            }

            estableceCreditosAsignadosAProfesores(evento, itemDTO);
        }

        if (itemDTO.getCreditos() != null)
        {
            evento.setCreditos(itemDTO.getCreditos().setScale(2, BigDecimal.ROUND_HALF_UP));
        }

        if (itemDTO.getCreditosComputables() != null)
        {
            evento.setCreditosComputables(
                    itemDTO.getCreditosComputables().setScale(2, BigDecimal.ROUND_HALF_UP));
        }

        evento.setDetalleManual(itemDTO.getDetalleManual());
        evento.setNumeroIteraciones(itemDTO.getNumeroIteraciones());
        evento.setRepetirCadaSemanas(itemDTO.getRepetirCadaSemanas());
        evento.setDesdeElDia(itemDTO.getDesdeElDia());
        evento.setHastaElDia(itemDTO.getHastaElDia());
        evento.setGrupoId(itemDTO.getGrupoId());
        evento.setSubgrupoId(itemDTO.getSubgrupoId());
        evento.setPlazas(itemDTO.getPlazas());

        Semestre semestre = new Semestre();
        semestre.setSemestre(itemDTO.getSemestre().getId());
        semestre.setNombre(itemDTO.getSemestre().getNombre());
        evento.setSemestre(semestre);

        if (itemDTO.getAula() != null)
        {
            Aula aula = new Aula();
            aula.setId(itemDTO.getAula().getId());
            aula.setNombre(itemDTO.getAula().getNombre());
            aula.setCodigo(itemDTO.getAula().getCodigo());
            aula.setPlazas(itemDTO.getAula().getPlazas());
            evento.setAula(aula);
        }

        if (!itemDTO.getItemsAsignaturas().isEmpty())
        {
            List<Asignatura> listaAsignaturas = new ArrayList<Asignatura>();
            for (ItemsAsignaturaDTO itemAsignatura : itemDTO.getItemsAsignaturas())
            {
                listaAsignaturas
                        .add(creaAsignaturasDesdeItemAsignaturaDTO(itemAsignatura, itemDTO));
            }
            evento.setAsignaturas(listaAsignaturas);
        }

        if (!itemDTO.getItemsDetalles().isEmpty())
        {
            List<EventoDetalle> listaDetalles = new ArrayList<>();
            for (ItemDetalleDTO itemDetalle : itemDTO.getItemsDetalles())
            {
                EventoDetalle eventoDetalle = creaEventoDetalleDesdeItemDetalleDTO(itemDetalle);
                eventoDetalle.setEvento(evento);
                listaDetalles.add(eventoDetalle);
            }
            evento.setEventosDetalle(listaDetalles);
        }

        return evento;
    }

    private void estableceCreditosAsignadosAProfesores(Evento evento, ItemDTO itemDTO)
    {
        if (evento.getProfesores() != null)
        {
            List<Long> profesoresIds = new ArrayList<>();

            for (Profesor profesor : evento.getProfesores())
            {
                profesoresIds.add(profesor.getId());
            }

            Map<Long, BigDecimal> creditosProfesoresMap = new HashMap<>();
            Map<Long, BigDecimal> creditosComputablesProfesoresMap = new HashMap<>();

            for (ItemsCreditosDetalleDTO creditosDetalle : itemDTO.getItemsCreditosDetalle())
            {
                creditosProfesoresMap.put(creditosDetalle.getId(), creditosDetalle.getCrdFinal());
                creditosComputablesProfesoresMap.put(creditosDetalle.getId(), creditosDetalle.getCrdComputablesFinal());
            }

            for (Profesor profesor : evento.getProfesores())
            {
                if (creditosProfesoresMap.get(profesor.getId()) != null)
                {
                    profesor.setCreditos(creditosProfesoresMap.get(profesor.getId()).setScale(2,
                            BigDecimal.ROUND_HALF_UP));
                }
                if (creditosComputablesProfesoresMap.get(profesor.getId()) != null)
                {
                    profesor.setCreditosComputables(creditosComputablesProfesoresMap.get(profesor.getId()).setScale(2,
                            BigDecimal.ROUND_HALF_UP));
                }
            }
        }
    }

    private String getNombreDiaSemana(Integer diaSemana)
    {
        HashMap<Integer, String> semana = new HashMap<>();
        semana.put(Calendar.SUNDAY, "Diumenge");
        semana.put(Calendar.MONDAY, "Dilluns");
        semana.put(Calendar.TUESDAY, "Dimarts");
        semana.put(Calendar.WEDNESDAY, "Dimecres");
        semana.put(Calendar.THURSDAY, "Dijous");
        semana.put(Calendar.FRIDAY, "Divendres");
        semana.put(Calendar.SATURDAY, "Dissabte");

        return semana.get(diaSemana);
    }

    @Override
    public List<Evento> getEventosDeUnCurso(Long estudioId, Long cursoId, Long semestreId,
                                            String grupoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QItemDTO item = QItemDTO.itemDTO;
        QItemsAsignaturaDTO asignatura = QItemsAsignaturaDTO.itemsAsignaturaDTO;

        List<ItemDTO> listaItemsDTO = query.from(asignatura).join(asignatura.item, item)
                .where(asignatura.estudioId.eq(estudioId)
                        .and(asignatura.cursoId.eq(cursoId).and(item.semestre.id.eq(semestreId))
                                .and(item.grupoId.eq(grupoId)).and(item.diaSemana.isNotNull())))
                .list(item);

        return listaItemsDTO.stream().map(itemDTO -> creaEventoDesdeItemDTO(itemDTO))
                .collect(Collectors.toList());
    }

    @Override
    public void deleteEventoDetalle(EventoDetalle detalle)
    {
        delete(ItemDetalleDTO.class, detalle.getId());
    }

    @Override
    public void deleteDetallesDeEvento(Evento evento)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QItemDetalleDTO qItemDetalle = QItemDetalleDTO.itemDetalleDTO;

        List<ItemDetalleDTO> itemsDetalle = query.from(qItemDetalle)
                .where(qItemDetalle.item.id.eq(evento.getId())).list(qItemDetalle);

        for (ItemDetalleDTO itemDetalle : itemsDetalle)
        {
            delete(ItemDetalleDTO.class, itemDetalle.getId());
        }
    }

    @Override
    public void deleteEventoSemanaGenerica(Long eventoId) throws RegistroNoEncontradoException
    {
        // Los comunes no hará falta tratarlos
        deleteCircuitosDeItem(eventoId);

        try
        {
            delete(ItemDTO.class, eventoId);
        }
        catch (Exception e)
        {
            throw new RegistroNoEncontradoException();
        }
    }

    private void deleteCircuitosDeItem(Long itemId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QItemCircuitoDTO itemCircuito = QItemCircuitoDTO.itemCircuitoDTO;

        List<ItemCircuitoDTO> listaItemsCircuitosDTO = query.from(itemCircuito)
                .where(itemCircuito.item.id.eq(itemId)).list(itemCircuito);

        for (ItemCircuitoDTO itemCircuitoDTO : listaItemsCircuitosDTO)
        {
            delete(ItemCircuitoDTO.class, itemCircuitoDTO.getId());
        }
    }

    @Override
    public Evento modificaDetallesGrupoAsignatura(Evento evento)
    {
        ItemDTO item = creaItemDTODesde(evento);

        DiaSemanaDTO diaSemanaDTO = getDiaSemanaDTOParaFecha(evento.getInicio());

        item.setHoraInicio(evento.getInicio());
        item.setHoraFin(evento.getFin());
        item.setDiaSemana(diaSemanaDTO);
        item.setDesdeElDia(evento.getDesdeElDia());
        item.setNumeroIteraciones(evento.getNumeroIteraciones());
        item.setRepetirCadaSemanas(evento.getRepetirCadaSemanas());
        item.setHastaElDia(evento.getHastaElDia());
        item.setDetalleManual(evento.hasDetalleManual());
        update(item);

        return creaEventoDesdeItemDTO(item);
    }

    DiaSemanaDTO getDiaSemanaDTOParaFecha(Date fecha)
    {
        if (fecha == null)
        {
            return null;
        }

        Calendar calInicio = Calendar.getInstance();
        calInicio.setTime(fecha);
        String diaSemana = getNombreDiaSemana(calInicio.get(Calendar.DAY_OF_WEEK));

        QDiaSemanaDTO qDiaSemana = QDiaSemanaDTO.diaSemanaDTO;
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qDiaSemana).where(qDiaSemana.nombre.eq(diaSemana));
        return query.list(qDiaSemana).get(0);
    }

    @Override
    public List<EventoDocencia> getDiasDocenciaDeUnEventoByEventoId(Long eventoId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QItemDetalleCompletoDTO item = QItemDetalleCompletoDTO.itemDetalleCompletoDTO;

        List<ItemDetalleCompletoDTO> listaItemsDetalleCompletoDTO = query.from(item)
                .where(item.id.eq(eventoId)).orderBy(item.fecha.asc()).list(item);

        List<EventoDocencia> eventosDocencia = new ArrayList<EventoDocencia>();

        for (ItemDetalleCompletoDTO itemDetalleCompletoDTO : listaItemsDetalleCompletoDTO)
        {
            eventosDocencia.add(creaEventoDocenciaDesde(itemDetalleCompletoDTO));
        }

        return eventosDocencia;
    }

    private EventoDocencia creaEventoDocenciaDesde(ItemDetalleCompletoDTO itemDetalleCompletoDTO)
    {
        EventoDocencia eventoDocencia = new EventoDocencia();

        eventoDocencia.setEventoId(itemDetalleCompletoDTO.getId());
        eventoDocencia.setFecha(itemDetalleCompletoDTO.getFecha());
        eventoDocencia.setDocencia(itemDetalleCompletoDTO.getDocencia());
        eventoDocencia.setTipoDia(itemDetalleCompletoDTO.getTipoDia());

        return eventoDocencia;
    }

    @Override
    public List<EventoDocencia> getDiasFestivosDeUnEventoByEventoId(Long eventoId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QItemDetalleCompletoDTO item = QItemDetalleCompletoDTO.itemDetalleCompletoDTO;

        List<ItemDetalleCompletoDTO> listaItemsDetalleCompletoDTO = query.from(item)
                .where(item.id.eq(eventoId).and(item.tipoDia.eq("F"))).orderBy(item.fecha.asc())
                .list(item);

        List<EventoDocencia> eventosDocencia = new ArrayList<EventoDocencia>();

        for (ItemDetalleCompletoDTO itemDetalleCompletoDTO : listaItemsDetalleCompletoDTO)
        {
            EventoDocencia eventoDocencia = new EventoDocencia();
            eventoDocencia.setFecha(itemDetalleCompletoDTO.getFecha());
            eventoDocencia.setTipoDia(itemDetalleCompletoDTO.getTipoDia());
            eventoDocencia.setDocencia("N");
            eventosDocencia.add(eventoDocencia);
        }

        return eventosDocencia;
    }

    @Override
    public List<Evento> getEventosSemanaGenericaByAgrupacion(Long estudioId, Long agrupacionId,
                                                             Long semestreId, List<String> gruposIds, List<String> asignaturasIds,
                                                             List<Long> calendariosIds)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QItemDTO item = QItemDTO.itemDTO;
        QItemAgrupacionDTO itemAgrupacion = QItemAgrupacionDTO.itemAgrupacionDTO;
        QItemsAsignaturaDTO asignatura = QItemsAsignaturaDTO.itemsAsignaturaDTO;

        List<String> tiposCalendarios = TipoSubgrupo.getTiposSubgrupos(calendariosIds);

        query.from(item).join(item.itemsAsignaturas, asignatura)
                .join(item.itemsAgrupacion, itemAgrupacion)
                .where(asignatura.estudioId.eq(estudioId)
                        .and(itemAgrupacion.agrupacion.id.eq(agrupacionId)
                                .and(item.semestre.id.eq(semestreId))
                                .and(item.grupoId.in(gruposIds)).and(item.diaSemana.isNotNull())
                                .and(item.tipoSubgrupoId.in(tiposCalendarios))));

        if (!asignaturasIds.isEmpty())
        {
            query.where(asignatura.asignaturaId.in(asignaturasIds));
        }

        return getEventosConTodasLasAsignaturasByListaEventos(query.list(item));
    }

    @Override
    public List<EventoDetalle> getEventosDetallePorAgrupacion(Long estudioId, Long agrupacionId,
                                                              Long semestreId, List<String> gruposIds, List<String> asignaturasIds,
                                                              List<Long> calendariosIds, Date rangoFechaInicio, Date rangoFechaFin)
    {

        JPAQuery query = new JPAQuery(entityManager);
        List<String> tiposCalendarios = TipoSubgrupo.getTiposSubgrupos(calendariosIds);

        QItemDTO item = QItemDTO.itemDTO;
        QItemAgrupacionDTO itemAgrupacion = QItemAgrupacionDTO.itemAgrupacionDTO;
        QItemsAsignaturaDTO itemsAsignatura = QItemsAsignaturaDTO.itemsAsignaturaDTO;
        QItemDetalleDTO itemsDetalle = QItemDetalleDTO.itemDetalleDTO;

        query.from(itemsDetalle).join(itemsDetalle.item, item).fetch()
                .join(item.itemsAsignaturas, itemsAsignatura).fetch()
                .join(item.itemsAgrupacion, itemAgrupacion)
                .where(itemsAsignatura.estudioId.eq(estudioId)
                        .and(itemAgrupacion.agrupacion.id.eq(agrupacionId)
                                .and(item.semestre.id.eq(semestreId))
                                .and(itemsDetalle.inicio.goe(rangoFechaInicio))
                                .and(itemsDetalle.fin.loe(rangoFechaFin))
                                .and(item.grupoId.in(gruposIds)).and(item.diaSemana.isNotNull())
                                .and(item.tipoSubgrupoId.in(tiposCalendarios))));

        if (!asignaturasIds.isEmpty())
        {
            query.where(itemsAsignatura.asignaturaId.in(asignaturasIds));
        }

        List<EventoDetalle> listaEventosDetalle = new ArrayList<EventoDetalle>();
        for (ItemDetalleDTO itemDetalleDTO : query.list(itemsDetalle))
        {
            EventoDetalle eventoDetalle = creaEventoDetalleDesdeItemDetalleDTO(itemDetalleDTO);
            eventoDetalle.setEvento(creaEventoDesdeItemDTO(itemDetalleDTO.getItem()));
            listaEventosDetalle.add(eventoDetalle);
        }
        return listaEventosDetalle;
    }

    @Override
    public List<Subgrupo> subgruposPorAsignaturaEstudioYCurso(String asignaturaId, Long estudioId,
                                                              Long cursoId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QItemDTO item = QItemDTO.itemDTO;
        QItemsAsignaturaDTO itemsAsignatura = QItemsAsignaturaDTO.itemsAsignaturaDTO;

        query.from(item).join(item.itemsAsignaturas, itemsAsignatura)
                .where(itemsAsignatura.estudioId.eq(estudioId).and(itemsAsignatura.asignaturaId
                        .eq(asignaturaId).and(itemsAsignatura.cursoId.eq(cursoId))));

        List<Tuple> listaSubgrupos = query.distinct()
                .list(new QTuple(item.subgrupoId, item.tipoSubgrupoId, item.grupoId));

        List<Subgrupo> subgrupos = new ArrayList<>();
        for (Tuple tuple : listaSubgrupos)
        {
            Subgrupo subgrupo = new Subgrupo();
            subgrupo.setSubgrupoId(tuple.get(item.subgrupoId));
            subgrupo.setTipoSubgrupoId(tuple.get(item.tipoSubgrupoId));
            subgrupo.setTipoSubgrupoNombre(tuple.get(item.tipoSubgrupoId));
            subgrupo.setGrupoId(tuple.get(item.grupoId));
            subgrupos.add(subgrupo);
        }

        return subgrupos;
    }

    @Override
    public List<EventoDetalle> getEventosDetalle(Long estudioId, Long cursoId, Long semestreId,
                                                 List<String> gruposIds, List<String> asignaturasIds, List<Long> calendariosIds,
                                                 Date rangoFechaInicio, Date rangoFechaFin)
    {
        JPAQuery query = new JPAQuery(entityManager);
        List<String> tiposCalendarios = TipoSubgrupo.getTiposSubgrupos(calendariosIds);

        QItemDTO item = QItemDTO.itemDTO;
        QItemsAsignaturaDTO itemsAsignatura = QItemsAsignaturaDTO.itemsAsignaturaDTO;
        QItemDetalleDTO itemsDetalle = QItemDetalleDTO.itemDetalleDTO;

        query.from(itemsDetalle).join(itemsDetalle.item, item).fetch()
                .join(item.itemsAsignaturas, itemsAsignatura).fetch()
                .where(itemsAsignatura.estudioId.eq(estudioId)
                        .and(itemsAsignatura.cursoId.eq(cursoId)
                                .and(item.semestre.id.eq(semestreId))
                                .and(itemsDetalle.inicio.goe(rangoFechaInicio))
                                .and(itemsDetalle.fin.loe(rangoFechaFin))
                                .and(item.grupoId.in(gruposIds)).and(item.diaSemana.isNotNull())
                                .and(item.tipoSubgrupoId.in(tiposCalendarios))));

        if (!asignaturasIds.isEmpty())
        {
            query.where(itemsAsignatura.asignaturaId.in(asignaturasIds));
        }

        List<EventoDetalle> listaEventosDetalle = new ArrayList<EventoDetalle>();
        for (ItemDetalleDTO itemDetalleDTO : query.list(itemsDetalle))
        {
            EventoDetalle eventoDetalle = creaEventoDetalleDesdeItemDetalleDTO(itemDetalleDTO);
            eventoDetalle.setEvento(creaEventoDesdeItemDTO(itemDetalleDTO.getItem()));
            listaEventosDetalle.add(eventoDetalle);
        }
        return listaEventosDetalle;
    }

    @Override
    public Evento getEventoByEventoDetalleId(Long eventoDetalleId)
            throws RegistroNoEncontradoException
    {
        JPAQuery query = new JPAQuery(entityManager);

        QItemDTO item = QItemDTO.itemDTO;
        QItemDetalleDTO itemDetalle = QItemDetalleDTO.itemDetalleDTO;

        List<ItemDTO> listaItemsDTO = query.from(itemDetalle).join(itemDetalle.item, item)
                .where(itemDetalle.id.eq(eventoDetalleId)).list(item);

        if (listaItemsDTO.size() == 1)
        {
            Evento evento = creaEventoDesdeItemDTO(listaItemsDTO.get(0));
            evento.setEventosDetalle(getEventosDetalleDeEvento(evento));

            return evento;
        }
        else
        {
            throw new RegistroNoEncontradoException();
        }
    }

    @Override
    @Transactional
    public void actualizaAulaAsignadaAEvento(Long eventoId, Long aulaId)
            throws RegistroNoEncontradoException
    {
        ItemDTO item;
        AulaDTO aula;
        try
        {
            item = get(ItemDTO.class, eventoId).get(0);
            aula = get(AulaDTO.class, aulaId).get(0);
        }
        catch (Exception e)
        {
            throw new RegistroNoEncontradoException();
        }

        item.setAula(aula);

        update(item);
    }

    @Override
    public Evento getEventoById(Long eventoId) throws RegistroNoEncontradoException
    {
        JPAQuery query = new JPAQuery(entityManager);

        QItemDTO item = QItemDTO.itemDTO;
        QItemsAsignaturaDTO itemsAsignatura = QItemsAsignaturaDTO.itemsAsignaturaDTO;
        QItemDetalleDTO itemDetalleDTO = QItemDetalleDTO.itemDetalleDTO;
        QItemProfesorDTO itemProfesorDTO = QItemProfesorDTO.itemProfesorDTO;

        List<ItemDTO> listaItemsDTO = query.from(item).join(item.itemsAsignaturas, itemsAsignatura)
                .fetch().leftJoin(item.itemsDetalles, itemDetalleDTO).fetch()
                .leftJoin(item.itemsProfesores, itemProfesorDTO).fetch().where(item.id.eq(eventoId))
                .distinct().list(item);

        if (listaItemsDTO.size() == 1)
        {
            return creaEventoDesdeItemDTO(listaItemsDTO.get(0));
        }
        else
        {
            throw new RegistroNoEncontradoException();
        }
    }

    @Override
    public Evento getEventoByIdConDetalles(Long eventoId) throws RegistroNoEncontradoException
    {
        Evento evento = getEventoById(eventoId);
        return evento;
    }

    private List<EventoDetalle> getEventosDetalleDeEvento(Evento evento)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QItemDetalleDTO itemDetalle = QItemDetalleDTO.itemDetalleDTO;

        List<ItemDetalleDTO> listaItemsDTO = query.from(itemDetalle)
                .where(itemDetalle.item.id.eq(evento.getId())).list(itemDetalle);

        List<EventoDetalle> listaDetalles = new ArrayList<EventoDetalle>();

        EventoDetalle eventoDetalle;
        for (ItemDetalleDTO detalleDTO : listaItemsDTO)
        {
            eventoDetalle = creaEventoDetalleDesdeItemDetalleDTO(detalleDTO);
            eventoDetalle.setEvento(evento);
            listaDetalles.add(eventoDetalle);
        }

        return listaDetalles;
    }

    @Override
    public Evento updateEvento(Evento evento)
    {
        ItemDTO itemDTO = creaItemDTODesde(evento);
        itemDTO = update(itemDTO);

        return creaEventoDesdeItemDTO(itemDTO);
    }

    @Override
    public Evento insertEvento(Evento evento)
    {
        ItemDTO itemDTO = creaItemDTODesde(evento);
        itemDTO = insert(itemDTO);

        if (!evento.getAsignaturas().isEmpty())
        {
            for (Asignatura asignatura : evento.getAsignaturas())
            {
                asignaLaAsignaturaSiEsNecesario(itemDTO, asignatura);
            }
        }

        for (EventoDetalle eventoDetalle : evento.getEventosDetalle())
        {
            ItemDetalleDTO itemDetalleDTO = new ItemDetalleDTO();
            itemDetalleDTO.setDescripcion(eventoDetalle.getDescripcion());
            itemDetalleDTO.setInicio(eventoDetalle.getInicio());
            itemDetalleDTO.setFin(eventoDetalle.getFin());
            itemDetalleDTO.setItem(itemDTO);
            insert(itemDetalleDTO);
        }

        if (evento.getProfesoresDetalle() != null)
        {
            for (ProfesorDetalle profesorDetalle : evento.getProfesoresDetalle())
            {
                ItemProfesorDTO itemProfesoDTO = creaItemProfesorDTODesde(profesorDetalle);
                itemProfesoDTO.setItem(itemDTO);
                insert(itemProfesoDTO);
            }
        }

        return creaEventoDesdeItemDTO(itemDTO);

    }

    private ItemProfesorDTO creaItemProfesorDTODesde(ProfesorDetalle profesorDetalle)
    {
        ItemProfesorDTO itemProfesorDTO = new ItemProfesorDTO();
        itemProfesorDTO.setId(profesorDetalle.getId());
        itemProfesorDTO.setDetalleManual(profesorDetalle.getDetalleManual());

        if (profesorDetalle.getCreditos() != null)
        {
            itemProfesorDTO.setCreditos(
                    profesorDetalle.getCreditos().setScale(2, BigDecimal.ROUND_HALF_UP));
        }

        if (profesorDetalle.getCreditosComputables() != null)
        {
            itemProfesorDTO.setCreditosComputables(
                    profesorDetalle.getCreditosComputables().setScale(2, BigDecimal.ROUND_HALF_UP));
        }

        ItemDTO itemDTO = new ItemDTO();
        itemDTO.setId(profesorDetalle.getEvento().getId());
        itemProfesorDTO.setItem(itemDTO);

        ProfesorDTO profesorDTO = new ProfesorDTO();
        profesorDTO.setId(profesorDetalle.getProfesor().getId());
        itemProfesorDTO.setProfesor(profesorDTO);

        return itemProfesorDTO;
    }

    private ItemDTO asignaLaAsignaturaSiEsNecesario(ItemDTO itemDTO, Asignatura asignatura)
    {
        if (!tieneAsignadaLaAsignatura(itemDTO, asignatura))
        {
            ItemsAsignaturaDTO asignaturaDTO = creaItemAsignaturaDeAsignatura(itemDTO, asignatura);
            insert(asignaturaDTO);
        }

        return itemDTO;
    }

    private ItemsAsignaturaDTO creaItemAsignaturaDeAsignatura(ItemDTO itemDTO,
                                                              Asignatura asignatura)
    {
        ItemsAsignaturaDTO asignaturaDTO = new ItemsAsignaturaDTO();
        asignaturaDTO.setAsignaturaId(asignatura.getId());
        asignaturaDTO.setNombreAsignatura(asignatura.getNombre());
        asignaturaDTO.setItem(itemDTO);
        asignaturaDTO.setEstudioId(asignatura.getEstudio().getId());
        asignaturaDTO.setEstudio(asignatura.getEstudio().getNombre());
        asignaturaDTO.setCursoId(asignatura.getCursoId());
        asignaturaDTO.setCaracter(asignatura.getCaracter());
        asignaturaDTO.setCaracterId(asignatura.getCaracterId());

        return asignaturaDTO;
    }

    private boolean tieneAsignadaLaAsignatura(ItemDTO itemDTO, Asignatura asignatura)
    {
        String asignaturaId = asignatura.getId();
        for (ItemsAsignaturaDTO asig : itemDTO.getItemsAsignaturas())
        {
            if (String.valueOf(asig.getId()).equals(asignaturaId))
            {
                return true;
            }
        }
        return false;
    }

    private ItemDTO creaItemDTODesde(Evento evento)
    {
        // Creamos el nuevo evento
        ItemDTO itemDTO = new ItemDTO();
        itemDTO.setId(evento.getId());

        SemestreDTO semestreDTO = new SemestreDTO();
        semestreDTO.setId(evento.getSemestre().getSemestre());
        semestreDTO.setNombre(evento.getSemestre().getNombre());

        itemDTO.setPlazas(evento.getPlazas());
        itemDTO.setComun(evento.tieneComunes() ? new Long(1) : new Long(0));
        itemDTO.setComunes(evento.getTextoAsignaturasComunes());
        if (!evento.getAsignaturas().isEmpty())
        {
            Asignatura unaAsignatura = evento.getAsignaturas().get(0);
            itemDTO.setPorcentajeComun(unaAsignatura.getPorcentajeComun());
            itemDTO.setTipoAsignaturaId(unaAsignatura.getTipoAsignaturaId());
            itemDTO.setTipoAsignatura(unaAsignatura.getTipoAsignatura());

            itemDTO.setTipoEstudioId(unaAsignatura.getEstudio().getTipoEstudio().getId());
            itemDTO.setTipoEstudio(unaAsignatura.getEstudio().getTipoEstudio().getNombre());
        }

        if (evento.getDia() != null)
        {
            DiaSemanaDTO diaSemanaDTO = new DiaSemanaDTO();
            diaSemanaDTO.setId(new Long(evento.getDia()));
            itemDTO.setDiaSemana(diaSemanaDTO);
        }

        itemDTO.setSemestre(semestreDTO);
        itemDTO.setGrupoId(evento.getGrupoId());
        itemDTO.setSubgrupoId(evento.getSubgrupoId());

        if (evento.getAula() != null)
        {
            AulaDTO aulaDTO = new AulaDTO();
            aulaDTO.setId(evento.getAula().getId());
            itemDTO.setAula(aulaDTO);
        }

        itemDTO.setHastaElDia(evento.getHastaElDia());
        itemDTO.setHoraFin(evento.getFin());
        itemDTO.setHoraInicio(evento.getInicio());
        itemDTO.setDesdeElDia(evento.getDesdeElDia());
        itemDTO.setHastaElDia(evento.getHastaElDia());
        itemDTO.setRepetirCadaSemanas(evento.getRepetirCadaSemanas());
        itemDTO.setNumeroIteraciones(evento.getNumeroIteraciones());
        itemDTO.setTipoSubgrupoId(TipoSubgrupo.getTipoSubgrupo(evento.getCalendario().getId()));
        itemDTO.setTipoSubgrupo(evento.getCalendario().getNombre());

        itemDTO.setDetalleManual(evento.hasDetalleManual());
        if (evento.getCreditos() != null)
        {
            itemDTO.setCreditos(evento.getCreditos().setScale(2, BigDecimal.ROUND_HALF_UP));
        }

        if (evento.getCreditosComputables() != null)
        {
            itemDTO.setCreditosComputables(
                    evento.getCreditosComputables().setScale(2, BigDecimal.ROUND_HALF_UP));
        }

        itemDTO.setComentarios(evento.getComentarios());
        return itemDTO;
    }

    @Override
    @Transactional
    public void updateHorasEventoYSusDetalles(Evento evento)
    {
        DiaSemanaDTO diaSemanaDTO = getDiaSemanaDTOParaFecha(evento.getInicio());

        QItemDTO qItem = QItemDTO.itemDTO;
        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, qItem);
        updateClause.where(qItem.id.eq(evento.getId())).set(qItem.horaInicio, evento.getInicio())
                .set(qItem.horaFin, evento.getFin()).set(qItem.diaSemana, diaSemanaDTO)
                .set(qItem.detalleManual, evento.hasDetalleManual());

        // En los masters se permite el cambio de tipo subgrupo
        if (evento.getTipoEstudioId().equals("M"))
        {
            updateClause.set(qItem.tipoSubgrupoId, evento.getCalendario().getLetraId())
                .set(qItem.tipoSubgrupo, evento.getCalendario().getNombre());
        }
        updateClause.execute();
    }

    @Override
    @Transactional
    public void updateHorasEventoDetalle(EventoDetalle eventoDetalle)
    {
        QItemDetalleDTO qItemDetalle = QItemDetalleDTO.itemDetalleDTO;

        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, qItemDetalle);
        updateClause.where(qItemDetalle.id.eq(eventoDetalle.getId()))
                .set(qItemDetalle.inicio, eventoDetalle.getInicio())
                .set(qItemDetalle.fin, eventoDetalle.getFin()).execute();

    }

    @Override
    public EventoDetalle insertEventoDetalle(EventoDetalle eventoDetalle)
            throws EventoDetalleSinEventoException
    {
        ItemDetalleDTO itemDetalle = new ItemDetalleDTO();
        itemDetalle.setDescripcion(eventoDetalle.getDescripcion());
        itemDetalle.setInicio(eventoDetalle.getInicio());
        itemDetalle.setFin(eventoDetalle.getFin());

        ItemDTO item = new ItemDTO();
        item.setId(eventoDetalle.getEvento().getId());
        itemDetalle.setItem(item);

        itemDetalle = this.insert(itemDetalle);

        return creaItemDetalleEnEventoDetalle(itemDetalle);
    }

    private EventoDetalle creaItemDetalleEnEventoDetalle(ItemDetalleDTO itemDetalle)
            throws EventoDetalleSinEventoException
    {
        EventoDetalle eventoDetalle = new EventoDetalle();

        eventoDetalle.setDescripcion(itemDetalle.getDescripcion());
        eventoDetalle.setId(itemDetalle.getId());
        eventoDetalle.setFin(itemDetalle.getFin());
        eventoDetalle.setInicio(itemDetalle.getInicio());
        try
        {
            eventoDetalle.setEvento(getEventoById(itemDetalle.getItem().getId()));
        }
        catch (RegistroNoEncontradoException e)
        {
            throw new EventoDetalleSinEventoException();
        }

        return eventoDetalle;
    }

    @Override
    public void desasignaAula(Long eventoId) throws RegistroNoEncontradoException
    {
        ItemDTO item;

        try
        {
            item = get(ItemDTO.class, eventoId).get(0);
        }
        catch (Exception e)
        {
            throw new RegistroNoEncontradoException();
        }

        item.setAula(null);
        item.setAulaNombre("");

        update(item);
    }

    @Override
    @Transactional
    public void desplanificaEvento(Evento evento)
    {
        DiaSemanaDTO diaSemanaDTO = getDiaSemanaDTOParaFecha(evento.getInicio());

        QItemDTO qItem = QItemDTO.itemDTO;
        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, qItem);
        updateClause.where(qItem.id.eq(evento.getId())).set(qItem.horaInicio, evento.getInicio())
                .set(qItem.horaFin, evento.getFin()).set(qItem.diaSemana, diaSemanaDTO)
                .set(qItem.detalleManual, false).setNull(qItem.aula).set(qItem.aulaNombre, "")
                .execute();
    }

    @Override
    public List<EventoDetalle> getEventosDetallePorAula(Long aulaId, Long semestreId,
                                                        List<Long> calendariosIds, Date rangoFechaInicio, Date rangoFechaFin)
    {
        JPAQuery query = new JPAQuery(entityManager);
        List<String> tiposCalendarios = TipoSubgrupo.getTiposSubgrupos(calendariosIds);

        QItemDTO item = QItemDTO.itemDTO;
        QItemsAsignaturaDTO itemsAsignatura = QItemsAsignaturaDTO.itemsAsignaturaDTO;
        QItemDetalleDTO itemsDetalle = QItemDetalleDTO.itemDetalleDTO;
        QAulaDTO aula = QAulaDTO.aulaDTO;

        List<ItemDetalleDTO> listaItemsDetalleDTO = query.from(itemsDetalle)
                .join(itemsDetalle.item, item).fetch().join(item.itemsAsignaturas, itemsAsignatura)
                .fetch().join(item.aula, aula)
                .where(aula.id.eq(aulaId)//.and(item.semestre.id.eq(semestreId))
                        .and(itemsDetalle.inicio.goe(rangoFechaInicio))
                        .and(itemsDetalle.fin.loe(rangoFechaFin)).and(item.diaSemana.isNotNull())
                        .and(item.tipoSubgrupoId.in(tiposCalendarios)))
                .list(itemsDetalle);

        List<EventoDetalle> listaEventosDetalle = new ArrayList<EventoDetalle>();
        for (ItemDetalleDTO itemDetalleDTO : listaItemsDetalleDTO)
        {

            EventoDetalle eventoDetalle = creaEventoDetalleDesdeItemDetalleDTO(itemDetalleDTO);
            eventoDetalle.setEvento(creaEventoDesdeItemDTO(itemDetalleDTO.getItem()));

            listaEventosDetalle.add(eventoDetalle);
        }
        return listaEventosDetalle;
    }

    @Override
    public List<Evento> getEventosSemanaGenericaPorAula(Long aulaId, Long semestreId,
                                                        List<Long> calendariosIds)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QItemDTO item = QItemDTO.itemDTO;
        QItemsAsignaturaDTO asignatura = QItemsAsignaturaDTO.itemsAsignaturaDTO;
        QAulaDTO aula = QAulaDTO.aulaDTO;

        List<String> tiposCalendarios = TipoSubgrupo.getTiposSubgrupos(calendariosIds);

        List<ItemDTO> listaItemsDTO = query.from(item).join(item.itemsAsignaturas, asignatura)
                .fetch().join(item.aula, aula)
                .where(aula.id.eq(aulaId).and(item.semestre.id.eq(semestreId))
                        .and(item.diaSemana.isNotNull())
                        .and(item.tipoSubgrupoId.in(tiposCalendarios)))
                .list(item);

        List<Evento> eventos = new ArrayList<Evento>();

        for (ItemDTO itemDTO : listaItemsDTO)
        {
            eventos.add(creaEventoDesdeItemDTO(itemDTO));
        }

        return eventos;
    }

    @Override
    public List<Evento> getEventosDelMismoGrupo(Evento evento)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QItemDTO qItem = QItemDTO.itemDTO;
        QItemsAsignaturaDTO qAsignatura = QItemsAsignaturaDTO.itemsAsignaturaDTO;

        String asignaturaId = evento.getAsignaturas().get(0).getId();
        Long estudioId = evento.getAsignaturas().get(0).getEstudio().getId();
        Long cursoId = evento.getAsignaturas().get(0).getCursoId();

        List<ItemDTO> listaItems = query.from(qItem).innerJoin(qItem.itemsAsignaturas, qAsignatura)
                .fetch()
                .where(qAsignatura.asignaturaId.eq(asignaturaId)
                        .and(qAsignatura.estudioId.eq(estudioId))
                        .and(qAsignatura.cursoId.eq(cursoId))
                        .and(qItem.semestre.id.eq(evento.getSemestre().getSemestre()))
                        .and(qItem.grupoId.eq(evento.getGrupoId()))
                        .and(qItem.tipoSubgrupoId.eq(evento.getCalendario().getLetraId()))
                        .and(qItem.subgrupoId.eq(evento.getSubgrupoId()))
                        .and(qItem.id.ne(evento.getId())))
                .distinct().list(qItem);

        List<Evento> listaEventos = new ArrayList<Evento>();
        for (ItemDTO itemDTO : listaItems)
        {
            listaEventos.add(creaEventoDesdeItemDTO(itemDTO));
        }

        return listaEventos;
    }

    @Override
    public List<Evento> getEventosMismoGrupoConDetalleProfesor(Evento evento, Long profesorId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QItemDTO qItem = QItemDTO.itemDTO;
        QItemsAsignaturaDTO qAsignatura = QItemsAsignaturaDTO.itemsAsignaturaDTO;
        QItemProfesorDTO qItemProfesorDTO = QItemProfesorDTO.itemProfesorDTO;
        QDiaSemanaDTO qDiaSemanaDTO = QDiaSemanaDTO.diaSemanaDTO;

        String asignaturaId = evento.getAsignaturas().get(0).getId();

        List<ItemDTO> listaItems = query.from(qItem).innerJoin(qItem.itemsAsignaturas, qAsignatura)
                .join(qItem.diaSemana, qDiaSemanaDTO).join(qItem.itemsProfesores, qItemProfesorDTO)
                .fetch()
                .where((qAsignatura.asignaturaId.eq(asignaturaId)
                        .or(qItem.comunes.like("%" + asignaturaId + "%")))
                        .and(qItem.grupoId.eq(evento.getGrupoId()))
                        .and(qItem.tipoSubgrupoId.eq(evento.getCalendario().getLetraId()))
                        .and(qItem.subgrupoId.eq(evento.getSubgrupoId()))
                        .and(qItemProfesorDTO.profesor.id.eq(profesorId)))
                .orderBy(qItem.semestre.id.asc(), qItem.diaSemana.id.asc(), qItem.horaInicio.asc())
                .list(qItem);

        return listaItems.stream().distinct().map(i -> creaEventoDesdeItemDTO(i))
                .collect(Collectors.toList());
    }

    @Override
    public List<Evento> getEventosDelMismoGrupoIncluyendoAnuales(Evento evento)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QItemDTO qItem = QItemDTO.itemDTO;
        QItemsAsignaturaDTO qAsignatura = QItemsAsignaturaDTO.itemsAsignaturaDTO;

        String asignaturaId = evento.getAsignaturas().get(0).getId();

        List<ItemDTO> listaItems = query.from(qItem).innerJoin(qItem.itemsAsignaturas, qAsignatura)
                .where((qAsignatura.asignaturaId.eq(asignaturaId)
                        .or(qItem.comunes.like("%" + asignaturaId + "%")))
                        .and(qItem.semestre.id.eq(evento.getSemestre().getSemestre())
                                .or(qItem.tipoAsignaturaId.eq("A")))
                        .and(qItem.grupoId.eq(evento.getGrupoId()))
                        .and(qItem.tipoSubgrupoId.eq(evento.getCalendario().getLetraId()))
                        .and(qItem.subgrupoId.eq(evento.getSubgrupoId())))
                .distinct().list(qItem);

        List<Evento> listaEventos = new ArrayList<Evento>();
        for (ItemDTO itemDTO : listaItems)
        {
            listaEventos.add(creaEventoDesdeItemDTO(itemDTO));
        }

        return listaEventos;
    }

    @Override
    public List<Evento> getTodosLosEventosSemanaGenericaCircuito(Long estudioId, Long semestreId,
                                                                 String grupoId, Long circuitoId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QItemDTO item = QItemDTO.itemDTO;
        QItemsAsignaturaDTO asignatura = QItemsAsignaturaDTO.itemsAsignaturaDTO;
        QItemCircuitoDTO itemCircuito = QItemCircuitoDTO.itemCircuitoDTO;

        List<ItemDTO> listaItemsDTO = query.from(item).join(item.itemsAsignaturas, asignatura)
                .fetch().leftJoin(item.itemsCircuitos, itemCircuito).fetch()
                .where(asignatura.estudioId.eq(estudioId)
                        .and(asignatura.cursoId.eq(new Long(1)).and(item.semestre.id.eq(semestreId))
                                .and(item.grupoId.eq(grupoId)).and(item.diaSemana.isNotNull())))
                .list(item);

        List<Evento> eventos = new ArrayList<Evento>();

        for (ItemDTO itemDTO : listaItemsDTO)
        {
            eventos.add(creaEventoParaCircuitoDesdeItemDTO(itemDTO, circuitoId));
        }

        return eventos;
    }

    @Override
    public List<Evento> getEventosNoPlanificadosSemanaGenericaCircuito(Long estudioId,
                                                                       Long semestreId, String grupoId, Long circuitoId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QItemDTO item = QItemDTO.itemDTO;
        QItemsAsignaturaDTO asignatura = QItemsAsignaturaDTO.itemsAsignaturaDTO;
        QItemCircuitoDTO itemCircuito = QItemCircuitoDTO.itemCircuitoDTO;

        List<ItemDTO> listaItemsDTO = query.from(item).join(item.itemsAsignaturas, asignatura)
                .fetch().leftJoin(item.itemsCircuitos, itemCircuito).fetch()
                .where(asignatura.estudioId.eq(estudioId)
                        .and(asignatura.cursoId.eq(new Long(1)).and(item.semestre.id.eq(semestreId))
                                .and(item.grupoId.eq(grupoId)).and(item.diaSemana.isNull())))
                .distinct().list(item);

        return listaItemsDTO.stream()
                .map(itemDTO -> creaEventoParaCircuitoDesdeItemDTO(itemDTO, circuitoId))
                .collect(Collectors.toList());
    }

    private Evento creaEventoParaCircuitoDesdeItemDTO(ItemDTO itemDTO, Long circuitoId)
    {
        Calendario calendario = obtenerCalendarioAsociadoPorTipoSubgrupo(itemDTO);

        Evento evento = new Evento();
        evento.setCalendario(calendario);
        evento.setTextoAsignaturasComunes(itemDTO.getComunes());

        if (itemDTO.getHoraInicio() != null && itemDTO.getHoraFin() != null)
        {
            evento.setInicio(FechaService.getFechaSemanaGenerica(itemDTO.getDiaSemana().getId(), itemDTO.getHoraInicio()));
            evento.setFin(FechaService.getFechaSemanaGenerica(itemDTO.getDiaSemana().getId(), itemDTO.getHoraFin()));
        }

        for (ItemCircuitoDTO itemCircuitoDTO : itemDTO.getItemsCircuitos())
        {
            if (itemCircuitoDTO.getCircuito().getId().equals(circuitoId))
            {
                Circuito circuito = new Circuito();
                circuito.setId(circuitoId);
                evento.setCircuito(circuito);
                break;
            }
        }

        evento.setGrupoId(itemDTO.getGrupoId());
        evento.setSubgrupoId(itemDTO.getSubgrupoId());
        evento.setPlazas(itemDTO.getPlazas());
        evento.setId(itemDTO.getId());

        Semestre semestre = new Semestre();
        semestre.setSemestre(itemDTO.getSemestre().getId());
        semestre.setNombre(itemDTO.getSemestre().getNombre());
        evento.setSemestre(semestre);

        if (itemDTO.getAula() != null)
        {
            Aula aula = new Aula();
            aula.setId(itemDTO.getAula().getId());
            aula.setNombre(itemDTO.getAula().getNombre());
            aula.setCodigo(itemDTO.getAula().getCodigo());
            aula.setPlazas(itemDTO.getAula().getPlazas());
            evento.setAula(aula);
        }

        if (!itemDTO.getItemsAsignaturas().isEmpty())
        {
            List<Asignatura> listaAsignaturas = new ArrayList<Asignatura>();
            for (ItemsAsignaturaDTO itemAsignatura : itemDTO.getItemsAsignaturas())
            {
                listaAsignaturas
                        .add(creaAsignaturasDesdeItemAsignaturaDTO(itemAsignatura, itemDTO));
            }
            evento.setAsignaturas(listaAsignaturas);
        }

        return evento;
    }

    @Override
    @Transactional
    public void asignaCircuitoAEventos(List<Long> eventosIds, Long circuitoId)
            throws RegistroNoEncontradoException
    {
        for (Long eventoId : eventosIds)
        {
            try
            {
                ItemCircuitoDTO itemCircuitoDTO = new ItemCircuitoDTO();
                itemCircuitoDTO.setItem(get(ItemDTO.class, eventoId).get(0));
                itemCircuitoDTO.setCircuito(get(CircuitoDTO.class, circuitoId).get(0));

                insert(itemCircuitoDTO);
            }
            catch (Exception e)
            {
                throw new RegistroNoEncontradoException();
            }
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void desasignaCircuitoDeEventos(List<Long> eventosIds, Long circuitoId)
            throws RegistroNoEncontradoException
    {
        QItemCircuitoDTO qItemCircuito = QItemCircuitoDTO.itemCircuitoDTO;

        for (Long eventoId : eventosIds)
        {
            try
            {
                JPAQuery query = new JPAQuery(entityManager);

                ItemCircuitoDTO itemCircuito = query.from(qItemCircuito)
                        .where(qItemCircuito.item.id.eq(eventoId)
                                .and(qItemCircuito.circuito.id.eq(circuitoId)))
                        .singleResult(qItemCircuito);
                if (itemCircuito != null)
                {
                    delete(itemCircuito);
                }
            }
            catch (Exception e)
            {
                throw new RegistroNoEncontradoException();
            }
        }
    }

    @Override
    public List<EventoDetalle> getTodosLosEventosSemanaDetalleCircuito(Long estudioId,
                                                                       Long semestreId, String grupoId, Long circuitoId, Date rangoFechaInicio,
                                                                       Date rangoFechaFin)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QItemDTO item = QItemDTO.itemDTO;
        QItemsAsignaturaDTO itemsAsignatura = QItemsAsignaturaDTO.itemsAsignaturaDTO;
        QItemDetalleDTO itemsDetalle = QItemDetalleDTO.itemDetalleDTO;
        QItemCircuitoDTO itemCircuito = QItemCircuitoDTO.itemCircuitoDTO;

        List<ItemDetalleDTO> listaItemsDetalleDTO = query.from(itemsDetalle)
                .join(itemsDetalle.item, item).fetch().join(item.itemsAsignaturas, itemsAsignatura)
                .fetch().join(item.itemsCircuitos, itemCircuito).fetch()
                .where(itemsAsignatura.estudioId.eq(estudioId)
                        .and(itemsAsignatura.cursoId.eq(new Long(1))
                                .and(item.semestre.id.eq(semestreId))
                                .and(itemsDetalle.inicio.goe(rangoFechaInicio))
                                .and(itemsDetalle.fin.loe(rangoFechaFin))
                                .and(item.grupoId.eq(grupoId)).and(item.diaSemana.isNotNull())))
                .distinct().list(itemsDetalle);

        List<EventoDetalle> listaEventosDetalle = new ArrayList<EventoDetalle>();
        for (ItemDetalleDTO itemDetalleDTO : listaItemsDetalleDTO)
        {
            EventoDetalle eventoDetalle = creaEventoDetalleDesdeItemDetalleDTO(itemDetalleDTO);

            eventoDetalle.setEvento(
                    creaEventoParaCircuitoDesdeItemDTO(itemDetalleDTO.getItem(), circuitoId));
            listaEventosDetalle.add(eventoDetalle);
        }
        return listaEventosDetalle;
    }

    @Override
    public List<Evento> getEventosSemanaGenericaPlanificadosPorAsignaturaIdYSemestreId(
            String asignaturaId, Long semestreId, List<String> gruposList,
            List<String> tipoSubgruposList)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QItemDTO qItem = QItemDTO.itemDTO;
        QItemProfesorDTO qItemProfesor = QItemProfesorDTO.itemProfesorDTO;
        QItemsAsignaturaDTO qAsignatura = QItemsAsignaturaDTO.itemsAsignaturaDTO;
        QProfesorDTO qProfesorDTO = QProfesorDTO.profesorDTO;
        QItemsCreditosDetalleDTO qItemsCreditosDetalleDTO = QItemsCreditosDetalleDTO.itemsCreditosDetalleDTO;
        QItemDetalleProfesorDTO qItemDetalleProfesorDTO = QItemDetalleProfesorDTO.itemDetalleProfesorDTO;
        QItemDetalleDTO qItemDetalleDTO = QItemDetalleDTO.itemDetalleDTO;

        query.from(qItem).join(qItem.itemsAsignaturas, qAsignatura).fetch()
                .leftJoin(qItem.itemsProfesores, qItemProfesor).fetch()
                .leftJoin(qItemProfesor.itemsProfesor, qItemDetalleProfesorDTO).fetch()
                .leftJoin(qItemDetalleProfesorDTO.itemDetalle, qItemDetalleDTO).fetch()
                .leftJoin(qItemProfesor.profesor, qProfesorDTO).fetch()
                .leftJoin(qItem.itemsCreditosDetalle, qItemsCreditosDetalleDTO).fetch()
                .where((qAsignatura.asignaturaId.eq(asignaturaId)
                        .or(qItem.comunes.like("%" + asignaturaId + "%")))
                        .and(qItem.semestre.id.eq(semestreId))
                        .and(qItem.diaSemana.isNotNull()))
                .orderBy(qItem.id.asc());

        if (gruposList.size() > 0)
        {
            query.where(qItem.grupoId.in(gruposList));
        }

        if (tipoSubgruposList.size() > 0)
        {
            query.where(qItem.tipoSubgrupoId.in(tipoSubgruposList));
        }

        List<Evento> eventos = new ArrayList<Evento>();

        for (ItemDTO itemDTO : query.distinct().list(qItem))
        {
            eventos.add(creaEventoDesdeItemDTO(itemDTO));
        }

        return eventos;
    }

    @Override
    public List<Evento> getEventosByAsignatura(String asignaturaId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QItemDTO qItem = QItemDTO.itemDTO;
        QItemsAsignaturaDTO qAsignatura = QItemsAsignaturaDTO.itemsAsignaturaDTO;

        query.from(qItem).join(qItem.itemsAsignaturas, qAsignatura).fetch()
                .where(qAsignatura.asignaturaId.eq(asignaturaId)
                        .or(qItem.comunes.like("%" + asignaturaId + "%")));

        List<Evento> eventos = new ArrayList<>();
        for (ItemDTO itemDTO : query.distinct().list(qItem))
        {
            eventos.add(creaEventoDesdeItemDTO(itemDTO));
        }
        return eventos;
    }

    @Override
    public List<EventoDetalle> getEventosSemanaDetallePlanificadosPorAsignaturaIdYSemestreId(
            String asignaturaId, Long semestreId, List<String> gruposList,
            List<String> tipoSubgruposList)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QItemDTO qItemDTO = QItemDTO.itemDTO;
        QItemsAsignaturaDTO qItemsAsignaturaDTO = QItemsAsignaturaDTO.itemsAsignaturaDTO;
        QItemProfesorDTO qItemProfesorDTO = QItemProfesorDTO.itemProfesorDTO;
        QProfesorDTO qProfesor = QProfesorDTO.profesorDTO;
        QItemDetalleDTO qItemDetalleDTO = QItemDetalleDTO.itemDetalleDTO;

        query.from(qItemDetalleDTO).join(qItemDetalleDTO.item, qItemDTO).fetch()
                .join(qItemDTO.itemsAsignaturas, qItemsAsignaturaDTO).fetch()
                .leftJoin(qItemDTO.itemsProfesores, qItemProfesorDTO).fetch()
                .leftJoin(qItemProfesorDTO.profesor, qProfesor).fetch()
                .where((qItemsAsignaturaDTO.asignaturaId.eq(asignaturaId)
                        .or(qItemDTO.comunes.like("%" + asignaturaId + "%")))
                        .and(qItemDTO.semestre.id.eq(semestreId)))
                .orderBy(qItemDetalleDTO.inicio.asc());

        if (gruposList.size() > 0)
        {
            query.where(qItemDTO.grupoId.in(gruposList));
        }

        if (tipoSubgruposList.size() > 0)
        {
            query.where(qItemDTO.tipoSubgrupoId.in(tipoSubgruposList));
        }

        List<ItemDetalleDTO> listaItemsDetalleDTO = query.distinct().list(qItemDetalleDTO);

        List<EventoDetalle> listaEventosDetalle = new ArrayList<>();
        for (ItemDetalleDTO itemDetalleDTO : listaItemsDetalleDTO)
        {
            EventoDetalle eventoDetalle = creaEventoDetalleDesdeItemDetalleDTO(itemDetalleDTO);
            eventoDetalle.setEvento(creaEventoDesdeItemDTO(itemDetalleDTO.getItem()));
            listaEventosDetalle.add(eventoDetalle);
        }
        return listaEventosDetalle;
    }

    @Override
    public List<Evento> getEventosSemanaGenericaNoPlanificadosPorAsignaturaIdYSemestreIdConProfesores(
            String asignaturaId, Long semestreId, List<String> gruposList,
            List<String> tipoSubgruposList)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QItemsAsignaturaDTO qAsignatura = QItemsAsignaturaDTO.itemsAsignaturaDTO;
        QItemProfesorDTO qItemProfesor = QItemProfesorDTO.itemProfesorDTO;
        QProfesorDTO qProfesorDTO = QProfesorDTO.profesorDTO;
        QItemDTO qItem = QItemDTO.itemDTO;
        QItemsCreditosDetalleDTO qItemsCreditosDetalleDTO = QItemsCreditosDetalleDTO.itemsCreditosDetalleDTO;

        query.from(qItem).join(qItem.itemsAsignaturas, qAsignatura).fetch()
                .leftJoin(qItem.itemsProfesores, qItemProfesor).fetch()
                .leftJoin(qItemProfesor.profesor, qProfesorDTO).fetch()
                .leftJoin(qItem.itemsCreditosDetalle, qItemsCreditosDetalleDTO).fetch()
                .where((qAsignatura.asignaturaId.eq(asignaturaId)
                        .or(qItem.comunes.like("%" + asignaturaId + "%")))
                        .and(qItem.semestre.id.eq(semestreId))
                        .and(qItem.diaSemana.isNull()))
                .orderBy(qItem.id.asc());

        if (gruposList.size() > 0)
        {
            query.where(qItem.grupoId.in(gruposList));
        }

        if (tipoSubgruposList.size() > 0)
        {
            query.where(qItem.tipoSubgrupoId.in(tipoSubgruposList));
        }

        return query.distinct().list(qItem).stream().map(itemDTO -> creaEventoDesdeItemDTO(itemDTO))
                .collect(Collectors.toList());

    }

    @Override
    public List<EventoDocencia> getFechasEventoByProfesor(Long profesorId, Long eventoId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QItemProfesorDetalleVistaDTO qItemProfesorDetalleVistaDTO = QItemProfesorDetalleVistaDTO.itemProfesorDetalleVistaDTO;
        QCalendarioDTO qCalendarioDTO = QCalendarioDTO.calendarioDTO;

        query.from(qItemProfesorDetalleVistaDTO, qCalendarioDTO)
                .where(qItemProfesorDetalleVistaDTO.itemId.eq(eventoId)
                        .and(qItemProfesorDetalleVistaDTO.profesorId.eq(profesorId))
                        .and(qCalendarioDTO.fecha.year()
                                .eq(qItemProfesorDetalleVistaDTO.inicio.year()))
                        .and(qCalendarioDTO.fecha.month()
                                .eq(qItemProfesorDetalleVistaDTO.inicio.month()))
                        .and(qCalendarioDTO.fecha.dayOfMonth()
                                .eq(qItemProfesorDetalleVistaDTO.inicio.dayOfMonth())))
                .orderBy(qItemProfesorDetalleVistaDTO.inicio.asc());

        List<EventoDocencia> fechas = new ArrayList<>();

        List<Tuple> listaDetalleClase = query.distinct()
                .list(new QTuple(qItemProfesorDetalleVistaDTO.detalleId,
                        qItemProfesorDetalleVistaDTO.inicio,
                        qItemProfesorDetalleVistaDTO.seleccionado, qCalendarioDTO.tipoDia));

        for (Tuple detalleClase : listaDetalleClase)
        {
            EventoDocencia eventoDocencia = new EventoDocencia();
            eventoDocencia.setEventoId(detalleClase.get(qItemProfesorDetalleVistaDTO.detalleId));
            eventoDocencia.setFecha(detalleClase.get(qItemProfesorDetalleVistaDTO.inicio));
            eventoDocencia.setDocencia(detalleClase.get(qItemProfesorDetalleVistaDTO.seleccionado));
            eventoDocencia.setTipoDia(detalleClase.get(qCalendarioDTO.tipoDia));

            fechas.add(eventoDocencia);
        }
        return fechas;
    }

    @Override
    public List<EventoDocencia> getFechasEventoSeleccionadasDetalleManualOtrosProfesores(Long profesorId, Long eventoId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QItemProfesorDetalleVistaDTO qItemProfesorDetalleVistaDTO = QItemProfesorDetalleVistaDTO.itemProfesorDetalleVistaDTO;
        QCalendarioDTO qCalendarioDTO = QCalendarioDTO.calendarioDTO;

        query.from(qItemProfesorDetalleVistaDTO, qCalendarioDTO)
                .where(qItemProfesorDetalleVistaDTO.itemId.eq(eventoId)
                        .and(qItemProfesorDetalleVistaDTO.profesorId.ne(profesorId))
                        .and(qItemProfesorDetalleVistaDTO.detalleManual.eq(true))
                        .and(qItemProfesorDetalleVistaDTO.seleccionado.eq("S"))
                        .and(qCalendarioDTO.fecha.year()
                                .eq(qItemProfesorDetalleVistaDTO.inicio.year()))
                        .and(qCalendarioDTO.fecha.month()
                                .eq(qItemProfesorDetalleVistaDTO.inicio.month()))
                        .and(qCalendarioDTO.fecha.dayOfMonth()
                                .eq(qItemProfesorDetalleVistaDTO.inicio.dayOfMonth())))
                .orderBy(qItemProfesorDetalleVistaDTO.inicio.asc());

        List<EventoDocencia> fechas = new ArrayList<>();

        List<Tuple> listaDetalleClase = query.distinct()
                .list(new QTuple(qItemProfesorDetalleVistaDTO.detalleId,
                        qItemProfesorDetalleVistaDTO.inicio,
                        qItemProfesorDetalleVistaDTO.seleccionado, qCalendarioDTO.tipoDia));

        for (Tuple detalleClase : listaDetalleClase)
        {
            EventoDocencia eventoDocencia = new EventoDocencia();
            eventoDocencia.setEventoId(detalleClase.get(qItemProfesorDetalleVistaDTO.detalleId));
            eventoDocencia.setFecha(detalleClase.get(qItemProfesorDetalleVistaDTO.inicio));
            eventoDocencia.setDocencia(detalleClase.get(qItemProfesorDetalleVistaDTO.seleccionado));
            eventoDocencia.setTipoDia(detalleClase.get(qCalendarioDTO.tipoDia));

            fechas.add(eventoDocencia);
        }
        return fechas;
    }

    @Override
    public List<Evento> getEventosProfesor(Long profesorId, String asignaturaId, Long semestreId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QItemDTO qItem = QItemDTO.itemDTO;
        QItemsAsignaturaDTO qAsignatura = QItemsAsignaturaDTO.itemsAsignaturaDTO;
        QItemProfesorDTO qItemProfesor = QItemProfesorDTO.itemProfesorDTO;
        QItemsCreditosDetalleDTO qItemsCreditosDetalleDTO = QItemsCreditosDetalleDTO.itemsCreditosDetalleDTO;

        query.from(qItem).join(qItem.itemsAsignaturas, qAsignatura).fetch()
                .join(qItem.itemsProfesores, qItemProfesor).fetch()
                .leftJoin(qItem.itemsCreditosDetalle, qItemsCreditosDetalleDTO).fetch()
                .where(qItemProfesor.profesor.id.eq(profesorId)
                        .and(qItem.semestre.id.eq(semestreId))
                        .and(qItem.comunes.notLike('%' + asignaturaId + '%')
                                .or(qItem.comunes.isNull()))
                        .and(qItem.horaInicio.isNotNull()))
                .orderBy(qItem.id.asc());

        List<Evento> eventos = query.distinct().list(qItem).stream()
                .map(itemDTO -> creaEventoDesdeItemDTO(itemDTO)).collect(Collectors.toList());
        actualizaCodigoComunAsignaturas(eventos);
        return eventos;
    }

    @Override
    public List<Evento> getEventosProfesorByAsignaturaAndSemestre(Long profesorId, String asignaturaId, Long semestreId) {
        JPAQuery query = new JPAQuery(entityManager);

        QItemDTO qItem = QItemDTO.itemDTO;
        QItemsAsignaturaDTO qAsignatura = QItemsAsignaturaDTO.itemsAsignaturaDTO;
        QItemProfesorDTO qItemProfesor = QItemProfesorDTO.itemProfesorDTO;
        QItemsCreditosDetalleDTO qItemsCreditosDetalleDTO = QItemsCreditosDetalleDTO.itemsCreditosDetalleDTO;

        query.from(qItem).join(qItem.itemsAsignaturas, qAsignatura).fetch()
                .join(qItem.itemsProfesores, qItemProfesor).fetch()
                .leftJoin(qItem.itemsCreditosDetalle, qItemsCreditosDetalleDTO).fetch()
                .where(qItemProfesor.profesor.id.eq(profesorId)
                        .and(qItem.semestre.id.eq(semestreId))
                        .and(qItem.horaInicio.isNotNull()))
                .orderBy(qItem.id.asc());

        if (asignaturaId != null && !asignaturaId.isEmpty()) {
            query.where(qAsignatura.asignaturaId.eq(asignaturaId));
        }

        List<Evento> eventos = query.distinct().list(qItem).stream()
                .map(itemDTO -> creaEventoDesdeItemDTO(itemDTO)).collect(Collectors.toList());
        actualizaCodigoComunAsignaturas(eventos);
        return eventos;
    }

    protected void actualizaCodigoComunAsignaturas(List<Evento> eventos)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QAsignaturaComunDTO qAsignaturaComunDTO = QAsignaturaComunDTO.asignaturaComunDTO;

        List<AsignaturaComunDTO> asignaturas = query.from(qAsignaturaComunDTO)
                .list(qAsignaturaComunDTO);

        for (Evento evento : eventos)
        {
            for (Asignatura asignatura : evento.getAsignaturas())
            {
                if (asignatura.getComun())
                {
                    Optional<AsignaturaComunDTO> asignaturaComunDTO = asignaturas.stream()
                            .filter(ac -> ac.getAsignaturaId().equals(asignatura.getId()))
                            .findFirst();
                    if (asignaturaComunDTO.isPresent())
                    {
                        asignatura.setCodigoComun(asignaturaComunDTO.get().getCodigoComun());
                    }
                }
            }
        }
    }

    @Override
    public List<Evento> getEventosProfesorNoPlanificadosQueNoPertenezcanAsignatura(Long profesorId,
                                                                                   String asignaturaId, Long semestreId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QItemDTO qItem = QItemDTO.itemDTO;
        QItemProfesorDTO qItemProfesor = QItemProfesorDTO.itemProfesorDTO;
        QItemsAsignaturaDTO qAsignatura = QItemsAsignaturaDTO.itemsAsignaturaDTO;

        query.from(qItem).join(qItem.itemsAsignaturas, qAsignatura).fetch()
                .join(qItem.itemsProfesores, qItemProfesor).fetch()
                .where(qItemProfesor.profesor.id.eq(profesorId)
                        .and(qItem.semestre.id.eq(semestreId))
                        .and(qItem.comunes.notLike('%' + asignaturaId + '%')
                                .or(qItem.comunes.isNull()))
                        .and(qItem.horaInicio.isNull()))
                .orderBy(qItem.id.asc());

        return query.distinct().list(qItem).stream().map(itemDTO -> creaEventoDesdeItemDTO(itemDTO))
                .collect(Collectors.toList());
    }

    @Override
    public Evento undo(Log log)
    {
        ItemDTO item = get(ItemDTO.class, log.getEvento().getId()).get(0);
        item.setDiaSemana(log.getDiaSemana() != null ? new DiaSemanaDTO(log.getDiaSemana()) : null);
        item.setHoraInicio(log.getHoraInicio());
        item.setHoraFin(log.getHoraFin());
        item.setAula(log.getAulaId() != null ? new AulaDTO(log.getAulaId()) : null);
        item = update(item);
        return creaEventoDesdeItemDTO(item);
    }
}
