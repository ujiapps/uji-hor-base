package es.uji.apps.hor.dao;

import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.QTuple;

import es.uji.apps.hor.db.*;
import es.uji.apps.hor.model.Agrupacion;
import es.uji.apps.hor.model.Asignatura;
import es.uji.apps.hor.model.AsignaturaAgrupacion;
import es.uji.commons.db.BaseDAODatabaseImpl;

import org.hibernate.type.BigDecimalType;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.*;
import java.util.stream.Collectors;

@Repository
public class AsignaturaDAODatabaseImpl extends BaseDAODatabaseImpl implements AsignaturaDAO
{

    public static final long ACTIVA = 1L;

    @Override
    public List<Asignatura> getAsignaturasByAreaIdAndSemestreId(Long areaId, Long semestreId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QAsignaturaAreaDTO qAsignaturaAreaDTO = QAsignaturaAreaDTO.asignaturaAreaDTO;
        QAsignaturaComunDTO qAsignaturaComunDTO = QAsignaturaComunDTO.asignaturaComunDTO;
        QItemsAsignaturaDTO qItemsAsignaturaDTO = QItemsAsignaturaDTO.itemsAsignaturaDTO;
        QItemDTO qItems = QItemDTO.itemDTO;

        query.from(qAsignaturaAreaDTO, qItemsAsignaturaDTO).join(qItemsAsignaturaDTO.item, qItems)
                .fetch()
                .where(qAsignaturaAreaDTO.asignaturaId.eq(qItemsAsignaturaDTO.asignaturaId)
                        .and(qAsignaturaAreaDTO.areaId.eq(areaId))
                        .and(qItems.semestre.id.eq(semestreId)).and(qItems.comun.eq(0L)));

        List<ItemsAsignaturaDTO> listaAsignaturasNoComunesDTO = query.distinct()
                .list(qItemsAsignaturaDTO);

        listaAsignaturasNoComunesDTO = eliminaAsignaturasDuplicadas(listaAsignaturasNoComunesDTO);

        query = new JPAQuery(entityManager);
        query.from(qAsignaturaComunDTO, qAsignaturaAreaDTO, qItemsAsignaturaDTO, qItems)
                .where(qAsignaturaAreaDTO.asignaturaId.eq(qAsignaturaComunDTO.asignaturaId)
                        .and(qItems.id.eq(qItemsAsignaturaDTO.item.id))
                        .and(qItemsAsignaturaDTO.asignaturaId.eq(qAsignaturaAreaDTO.asignaturaId))
                        .and(qAsignaturaAreaDTO.areaId.eq(areaId))
                        .and(qItems.semestre.id.eq(semestreId)));

        List<Tuple> listaTuplasAsignaturasComunesDTO = query.distinct()
                .list(new QTuple(QAsignaturaComunDTO.asignaturaComunDTO.grupoComunId,
                        QItemsAsignaturaDTO.itemsAsignaturaDTO.asignaturaId,
                        QAsignaturaComunDTO.asignaturaComunDTO.nombre,
                        QAsignaturaComunDTO.asignaturaComunDTO.prefijo,
                        QItemsAsignaturaDTO.itemsAsignaturaDTO.nombreAsignatura,
                        QItemDTO.itemDTO.tipoAsignaturaId));

        List<Asignatura> listaAsignaturas = new ArrayList<>();

        for (ItemsAsignaturaDTO itemsAsignaturaDTO : listaAsignaturasNoComunesDTO)
        {
            listaAsignaturas.add(creaAsignaturaDesdeItemsAsignaturaNoComunDTO(itemsAsignaturaDTO));
        }

        for (Tuple tupla : listaTuplasAsignaturasComunesDTO)
        {
            listaAsignaturas.add(creaAsignaturaDesdeItemsAsignaturaDTO(tupla));
        }
        return listaAsignaturas;
    }

    @Override
    public List<Asignatura> getAsignaturasByAreaId(Long areaId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QAsignaturaAreaDTO qAsignaturaAreaDTO = QAsignaturaAreaDTO.asignaturaAreaDTO;
        QAsignaturaComunDTO qAsignaturaComunDTO = QAsignaturaComunDTO.asignaturaComunDTO;
        QItemsAsignaturaDTO qItemsAsignaturaDTO = QItemsAsignaturaDTO.itemsAsignaturaDTO;
        QItemDTO qItems = QItemDTO.itemDTO;

        query.from(qAsignaturaAreaDTO, qItemsAsignaturaDTO).join(qItemsAsignaturaDTO.item, qItems)
                .fetch()
                .where(qAsignaturaAreaDTO.asignaturaId.eq(qItemsAsignaturaDTO.asignaturaId)
                        .and(qAsignaturaAreaDTO.areaId.eq(areaId)).and(qItems.comun.eq(0L))
                        .and(qAsignaturaAreaDTO.porcentaje.gt(0L)));

        List<ItemsAsignaturaDTO> listaAsignaturasNoComunesDTO = query.distinct()
                .list(qItemsAsignaturaDTO);

        listaAsignaturasNoComunesDTO = eliminaAsignaturasDuplicadas(listaAsignaturasNoComunesDTO);

        query = new JPAQuery(entityManager);
        query.from(qAsignaturaComunDTO, qAsignaturaAreaDTO, qItemsAsignaturaDTO, qItems)
                .where(qAsignaturaAreaDTO.asignaturaId.eq(qAsignaturaComunDTO.asignaturaId)
                        .and(qItems.id.eq(qItemsAsignaturaDTO.item.id))
                        .and(qItemsAsignaturaDTO.asignaturaId.eq(qAsignaturaAreaDTO.asignaturaId))
                        .and(qAsignaturaAreaDTO.areaId.eq(areaId))
                        .and(qAsignaturaAreaDTO.porcentaje.gt(0L)));

        List<Tuple> listaTuplasAsignaturasComunesDTO = query.distinct()
                .list(new QTuple(QAsignaturaComunDTO.asignaturaComunDTO.grupoComunId,
                        QItemsAsignaturaDTO.itemsAsignaturaDTO.asignaturaId,
                        QAsignaturaComunDTO.asignaturaComunDTO.nombre,
                        QAsignaturaComunDTO.asignaturaComunDTO.prefijo,
                        QItemsAsignaturaDTO.itemsAsignaturaDTO.nombreAsignatura,
                        QItemDTO.itemDTO.tipoAsignaturaId));

        List<Asignatura> listaAsignaturas = new ArrayList<>();

        for (ItemsAsignaturaDTO itemsAsignaturaDTO : listaAsignaturasNoComunesDTO)
        {
            listaAsignaturas.add(creaAsignaturaDesdeItemsAsignaturaNoComunDTO(itemsAsignaturaDTO));
        }

        for (Tuple tupla : listaTuplasAsignaturasComunesDTO)
        {
            listaAsignaturas.add(creaAsignaturaDesdeItemsAsignaturaDTO(tupla));
        }
        return listaAsignaturas;
    }

    @Override
    public List<Asignatura> getAsignaturasByProfesorId(Long profesorId, Long semestreId)
    {
        List<Long> estudiosAbiertosSesiones = getTitulacionesIdsAbiertasSesionesPod();

        if (estudiosAbiertosSesiones.isEmpty()) {
            return new ArrayList<>();
        }

        JPAQuery query = new JPAQuery(entityManager);

        QItemsAsignaturaDTO qItemsAsignaturaDTO = QItemsAsignaturaDTO.itemsAsignaturaDTO;
        QItemDTO qItems = QItemDTO.itemDTO;
        QItemProfesorDTO qItemProfesorDTO = QItemProfesorDTO.itemProfesorDTO;

        query.from(qItemsAsignaturaDTO)
                .join(qItemsAsignaturaDTO.item, qItems).fetch()
                .join(qItems.itemsProfesores, qItemProfesorDTO).fetch()
                .where(qItemProfesorDTO.profesor.id.eq(profesorId).and(qItemsAsignaturaDTO.estudioId.in(estudiosAbiertosSesiones)));

        if (semestreId != null)
        {
            query.where(qItems.semestre.id.eq(semestreId));
        }

        List<Asignatura> listaAsignaturas = new ArrayList<>();

        for (ItemsAsignaturaDTO itemsAsignaturaDTO : query.distinct().list(qItemsAsignaturaDTO))
        {
            if (listaAsignaturas.stream().filter(a -> a.getId().equals(itemsAsignaturaDTO.getAsignaturaId())).count() == 0)
            {
                listaAsignaturas.add(creaAsignaturaDesdeItemsAsignaturaNoComunDTO(itemsAsignaturaDTO));
            }
        }

        return listaAsignaturas;
    }

    private List<Long> getTitulacionesIdsAbiertasSesionesPod()
    {
        JPAQuery query = new JPAQuery(entityManager);
        QEstudioDTO qEstudio = QEstudioDTO.estudioDTO;
        List<EstudioDTO> estudios = query.from(qEstudio).where(qEstudio.abiertoSesionesPodPdi.isTrue()).distinct().list(qEstudio);
        return estudios.stream().map(e -> e.getId()).collect(Collectors.toList());
    }

    @Override
    public List<Asignatura> getAsignaturasByTitulacionAndCursoAndSemestre(Long estudioId,
                                                                          Long cursoId, Long semestreId, List<String> gruposIds)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QItemsAsignaturaDTO qItemsAsignaturaDTO = QItemsAsignaturaDTO.itemsAsignaturaDTO;
        QItemDTO qItemDTO = QItemDTO.itemDTO;

        query.from(qItemsAsignaturaDTO).join(qItemsAsignaturaDTO.item, qItemDTO).fetch()
                .where(qItemsAsignaturaDTO.estudioId.eq(estudioId).and(qItemsAsignaturaDTO.cursoId
                        .eq(cursoId).and(qItemDTO.semestre.id.eq(semestreId))));

        if (!gruposIds.isEmpty())
        {
            query.where(qItemDTO.grupoId.in(gruposIds));
        }

        List<ItemsAsignaturaDTO> asignaturas = query.distinct().list(qItemsAsignaturaDTO);

        asignaturas = eliminaAsignaturasDuplicadas(asignaturas);

        List<Asignatura> listaAsignaturas = new ArrayList<>();

        for (ItemsAsignaturaDTO itemsAsignaturaDTO : asignaturas)
        {
            listaAsignaturas.add(creaAsignaturaDesdeItemsAsignaturaNoComunDTO(itemsAsignaturaDTO));
        }

        return listaAsignaturas;
    }

    @Override
    public List<Asignatura> getAsignaturasByTitulacionAndCurso(Long estudioId, Long cursoId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QAsignaturaEstudioDTO qAsignaturaEstudioDTO = QAsignaturaEstudioDTO.asignaturaEstudioDTO;

        query.from(qAsignaturaEstudioDTO)
                .where(qAsignaturaEstudioDTO.estudioId.eq(estudioId)
                        .and(qAsignaturaEstudioDTO.cursoId.eq(cursoId)))
                .orderBy(qAsignaturaEstudioDTO.asignaturaId.asc()).distinct();

        List<Asignatura> listaAsignaturas = new ArrayList<>();
        for (AsignaturaEstudioDTO asignaturaEstudioDTO : query.list(qAsignaturaEstudioDTO))
        {
            listaAsignaturas.add(creaAsignaturaDesdeAsignaturaEstudioDTO(asignaturaEstudioDTO));
        }

        return listaAsignaturas;
    }

    @Override
    public List<AsignaturaAgrupacion> getAsignaturasByAgrupacionId(Long agrupacionId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QAsignaturaEstudioDTO qAsignaturaEstudioDTO = QAsignaturaEstudioDTO.asignaturaEstudioDTO;
        QAgrupacionAsignaturaDTO qAgrupacionAsignaturaDTO = QAgrupacionAsignaturaDTO.agrupacionAsignaturaDTO;

        query.from(qAgrupacionAsignaturaDTO)
                .join(qAgrupacionAsignaturaDTO.asignatura, qAsignaturaEstudioDTO).fetch()
                .where(qAgrupacionAsignaturaDTO.agrupacion.id.eq(agrupacionId))
                .orderBy(qAsignaturaEstudioDTO.asignaturaId.asc()).distinct();

        List<AsignaturaAgrupacion> listaAsignaturas = new ArrayList<>();
        for (AgrupacionAsignaturaDTO agrupacionAsignaturaDTO : query.list(qAgrupacionAsignaturaDTO))
        {
            listaAsignaturas.add(creaAsignaturaAgrupacionDesde(agrupacionAsignaturaDTO));
        }

        return listaAsignaturas;
    }

    private AsignaturaAgrupacion creaAsignaturaAgrupacionDesde(
            AgrupacionAsignaturaDTO agrupacionAsignaturaDTO)
    {
        AsignaturaAgrupacion asignaturaAgrupacion = new AsignaturaAgrupacion();
        asignaturaAgrupacion.setId(agrupacionAsignaturaDTO.getId());
        asignaturaAgrupacion.setSubgrupoId(agrupacionAsignaturaDTO.getSubgrupoId());
        asignaturaAgrupacion.setTipoSubGrupo(agrupacionAsignaturaDTO.getTipoSubgrupoId());

        Asignatura asignatura = new Asignatura();
        asignatura.setId(agrupacionAsignaturaDTO.getAsignatura().getAsignaturaId());
        asignatura.setNombre(agrupacionAsignaturaDTO.getAsignatura().getAsignaturaNombre());
        asignatura.setCursoId(agrupacionAsignaturaDTO.getAsignatura().getCursoId());
        asignaturaAgrupacion.setAsignatura(asignatura);

        Agrupacion agrupacion = new Agrupacion();
        agrupacion.setId(agrupacionAsignaturaDTO.getAgrupacion().getId());
        asignaturaAgrupacion.setAgrupacion(agrupacion);

        return asignaturaAgrupacion;
    }

    private List<AsignaturaComunDTO> extraeAsignaturasComunes(Long grupoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QAsignaturaComunDTO qAsignaturaComunDTO = QAsignaturaComunDTO.asignaturaComunDTO;

        query.from(qAsignaturaComunDTO).where(qAsignaturaComunDTO.grupoComunId.eq(grupoId));

        return query.list(qAsignaturaComunDTO);
    }

    private AsignaturaEstudioDTO getAsignaturaEstudioDTOByAsignaturaId(String asignaturaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QAsignaturaEstudioDTO qAsignaturaEstudioDTO = QAsignaturaEstudioDTO.asignaturaEstudioDTO;

        query.from(qAsignaturaEstudioDTO)
                .where(qAsignaturaEstudioDTO.asignaturaId.eq(asignaturaId));

        List<AsignaturaEstudioDTO> res = query.list(qAsignaturaEstudioDTO);

        if (res.size() > 0)
        {
            return res.get(0);
        }
        else
        {
            return null;
        }
    }

    @Override
    public List<Asignatura> getAsignaturasCompartidaByEstudioYAsignaturaId(String asignaturaId,
                                                                           List<Long> estudioCompartidoIds)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QAsignaturaComunDTO qAsignaturaComunDTO = QAsignaturaComunDTO.asignaturaComunDTO;

        query.from(qAsignaturaComunDTO).where(qAsignaturaComunDTO.asignaturaId.eq(asignaturaId));

        List<AsignaturaComunDTO> asignaturaComunDTOs = query.list(qAsignaturaComunDTO);

        List<Asignatura> asignaturasCompartidas = new ArrayList<>();

        if (asignaturaComunDTOs.size() > 0)
        {
            AsignaturaComunDTO asignaturaComunDTO = asignaturaComunDTOs.get(0);

            List<AsignaturaComunDTO> asignaturasComunesDTO = extraeAsignaturasComunes(
                    asignaturaComunDTO.getGrupoComunId());
            for (AsignaturaComunDTO asignaturaComunDTO2 : asignaturasComunesDTO)
            {
                AsignaturaEstudioDTO asignaturaEstudioDTO = getAsignaturaEstudioDTOByAsignaturaId(
                        asignaturaComunDTO2.getAsignaturaId());
                if (estudioCompartidoIds.contains(asignaturaEstudioDTO.getEstudioId()))
                {
                    asignaturasCompartidas
                            .add(creaAsignaturaDesdeAsignaturaEstudioDTO(asignaturaEstudioDTO));
                }
            }
        }

        return asignaturasCompartidas;
    }

    @Override
    public List<String> getCodigosAsignaturasCompartidas(String asignaturaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QAsignaturaComunDTO qAsignaturaComunDTO = QAsignaturaComunDTO.asignaturaComunDTO;
        query.from(qAsignaturaComunDTO).where(qAsignaturaComunDTO.asignaturaId.eq(asignaturaId));
        List<AsignaturaComunDTO> asignaturaComunDTOs = query.list(qAsignaturaComunDTO);
        List<String> codigosAsignaturasCompartidas = new ArrayList<>();

        if (asignaturaComunDTOs.size() > 0)
        {
            AsignaturaComunDTO asignaturaComunDTO = asignaturaComunDTOs.get(0);

            List<AsignaturaComunDTO> asignaturasComunesDTO = extraeAsignaturasComunes(
                    asignaturaComunDTO.getGrupoComunId());
            for (AsignaturaComunDTO asignaturaComunDTO2 : asignaturasComunesDTO)
            {
                codigosAsignaturasCompartidas.add(asignaturaComunDTO2.getAsignaturaId());

            }
        }

        return codigosAsignaturasCompartidas;
    }

    @Override
    public List<Asignatura> getAsignaturasByAgrupacionAndSemestreAndGrupos(Long estudioId,
                                                                           Long agrupacionId, Long semestreId, List<String> gruposAsList)
    {
        if (gruposAsList.isEmpty())
        {
            return new ArrayList<>();
        }

        JPAQuery query = new JPAQuery(entityManager);

        QItemsAsignaturaDTO qItemsAsignaturaDTO = QItemsAsignaturaDTO.itemsAsignaturaDTO;
        QItemDTO qItemDTO = QItemDTO.itemDTO;
        QItemAgrupacionDTO qItemAgrupacionDTO = QItemAgrupacionDTO.itemAgrupacionDTO;

        query.from(qItemsAsignaturaDTO).join(qItemsAsignaturaDTO.item, qItemDTO).fetch()
                .join(qItemDTO.itemsAgrupacion, qItemAgrupacionDTO)
                .where(qItemAgrupacionDTO.agrupacion.id.eq(agrupacionId).and(
                        qItemDTO.semestre.id.eq(semestreId).and(qItemDTO.grupoId.in(gruposAsList)
                                .and(qItemsAsignaturaDTO.estudioId.eq(estudioId)))));

        List<ItemsAsignaturaDTO> asignaturas = query.distinct().list(qItemsAsignaturaDTO);

        asignaturas = eliminaAsignaturasDuplicadas(asignaturas);

        List<Asignatura> listaAsignaturas = new ArrayList<>();

        for (ItemsAsignaturaDTO itemsAsignaturaDTO : asignaturas)
        {
            listaAsignaturas.add(creaAsignaturaDesdeItemsAsignaturaNoComunDTO(itemsAsignaturaDTO));
        }

        return listaAsignaturas;
    }

    @Override
    public String getCodigoComunByAsignaturaId(String asignaturaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QAsignaturaComunDTO qAsignaturaComunDTO = QAsignaturaComunDTO.asignaturaComunDTO;

        query.from(qAsignaturaComunDTO).where(qAsignaturaComunDTO.asignaturaId.eq(asignaturaId));

        List<AsignaturaComunDTO> asignaturaComunDTOs = query.list(qAsignaturaComunDTO);
        if (asignaturaComunDTOs.isEmpty())
        {
            return null;
        }

        AsignaturaComunDTO asignaturaComunDTO = asignaturaComunDTOs.get(0);
        return asignaturaComunDTO.getPrefijo() + asignaturaComunDTO.getAsignaturaId().substring(2);
    }

    @Override
    public Map<String, BigDecimal> getCreditosPodAsignatura(List<String> asignaturasId,
                                                            boolean asignaturaComparteItems)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QAsignaturaDetalleDTO qAsignaturaDetalleDTO = QAsignaturaDetalleDTO.asignaturaDetalleDTO;
        query.from(qAsignaturaDetalleDTO)
                .where(qAsignaturaDetalleDTO.asignaturaId.in(asignaturasId));

        List<AsignaturaDetalleDTO> asignaturaDetalleDTOs = query.list(qAsignaturaDetalleDTO);
        if (asignaturaDetalleDTOs.isEmpty())
        {
            return null;
        }

        Map<String, BigDecimal> mapaCreditos = new HashMap<>();
        mapaCreditos.put("creditos", asignaturaComparteItems ? getMaxCreditos(asignaturaDetalleDTOs)
                : getSumaCreditos(asignaturaDetalleDTOs));
        mapaCreditos.put("creditosComputables", asignaturaComparteItems ? getMaxCreditosComputables(asignaturaDetalleDTOs)
                : getSumaCreditosComputables(asignaturaDetalleDTOs));

        return mapaCreditos;

    }
    @Override
    public Map<String, BigDecimal> getCreditosPodAsignatura(String asignaturasId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCreditosAsignaturaDTO qCreditosAsignaturaDTO = QCreditosAsignaturaDTO.creditosAsignaturaDTO;
        query.from(qCreditosAsignaturaDTO)
                .where(qCreditosAsignaturaDTO.asignaturaId.eq(asignaturasId));

        List<CreditosAsignaturaDTO> asignaturaDetalleDTOs = query.list(qCreditosAsignaturaDTO);
        if (asignaturaDetalleDTOs.isEmpty())
        {
            return null;
        }

        CreditosAsignaturaDTO creditosAsignaturaDTO = asignaturaDetalleDTOs.get(0);

        Map<String, BigDecimal> mapaCreditos = new HashMap<>();
        mapaCreditos.put("creditos", creditosAsignaturaDTO.getCreditosPod());
        mapaCreditos.put("creditosComputables", creditosAsignaturaDTO.getCreditosPod());

        return mapaCreditos;
    }

    private BigDecimal getMaxCreditos(List<AsignaturaDetalleDTO> asignaturaDetalleDTOs)
    {
        return asignaturaDetalleDTOs.stream().map(ad -> ad.getCreditosPod())
                .max((c1, c2) -> c1.compareTo(c2)).get();
    }

    private BigDecimal getMaxCreditosComputables(List<AsignaturaDetalleDTO> asignaturaDetalleDTOs)
    {
        return asignaturaDetalleDTOs.stream().map(ad -> ad.getCreditosPodComputables())
                .max((c1, c2) -> c1.compareTo(c2)).get();
    }

    private BigDecimal getSumaCreditos(List<AsignaturaDetalleDTO> asignaturaDetalleDTOs)
    {
        return asignaturaDetalleDTOs.stream().map(ad -> ad.getCreditosPod())
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    private BigDecimal getSumaCreditosComputables(List<AsignaturaDetalleDTO> asignaturaDetalleDTOs)
    {
        return asignaturaDetalleDTOs.stream().map(ad -> ad.getCreditosPodComputables())
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    @Override
    public Map<String, BigDecimal> getCreditosPodAsignados(List<String> asignaturasId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QItemsCreditosDetalleDTO qItemsCreditosDetalleDTO = QItemsCreditosDetalleDTO.itemsCreditosDetalleDTO;
        query.from(qItemsCreditosDetalleDTO)
                .where(qItemsCreditosDetalleDTO.asignaturaId.in(asignaturasId));

        List<ItemsCreditosDetalleDTO> itemsCreditosDetalleDTOs = query
                .list(qItemsCreditosDetalleDTO);

        BigDecimal creditosTotales = new BigDecimal(0);
        BigDecimal creditosComputablesTotales = new BigDecimal(0);

        HashSet<String> subgruposProfesor = new HashSet<>();
        for (ItemsCreditosDetalleDTO itemsCreditosDetalleDTO : itemsCreditosDetalleDTOs)
        {
            String hashSubgrupoProfesor = MessageFormat.format("{0}-{1}-{2}",
                    itemsCreditosDetalleDTO.getGrupoId(), itemsCreditosDetalleDTO.getTipo(),
                    itemsCreditosDetalleDTO.getId());
            if (!subgruposProfesor.contains(hashSubgrupoProfesor))
            {
                creditosTotales = creditosTotales.add(itemsCreditosDetalleDTO.getCrdFinal());
                creditosComputablesTotales = creditosComputablesTotales.add(itemsCreditosDetalleDTO.getCrdComputablesFinal());
                subgruposProfesor.add(hashSubgrupoProfesor);
            }
        }

        Map<String, BigDecimal> mapaCreditos = new HashMap<>();
        mapaCreditos.put("creditos", creditosTotales);
        mapaCreditos.put("creditosComputables", creditosComputablesTotales);

        return mapaCreditos;
    }

    private Asignatura creaAsignaturaDesdeAsignaturaEstudioDTO(
            AsignaturaEstudioDTO asignaturaEstudioDTO)
    {
        Asignatura asignatura = new Asignatura();
        asignatura.setId(asignaturaEstudioDTO.getAsignaturaId());
        asignatura.setNombre(asignaturaEstudioDTO.getAsignaturaNombre());
        asignatura.setCaracter(asignaturaEstudioDTO.getCaracterNombre());
        asignatura.setCursoId(asignaturaEstudioDTO.getCursoId());
        return asignatura;

    }

    private List<ItemsAsignaturaDTO> eliminaAsignaturasDuplicadas(
            List<ItemsAsignaturaDTO> listaAsignaturasNoComunesDTO)
    {
        List<String> asignaturasIds = new ArrayList<>();
        List<ItemsAsignaturaDTO> listaAsignaturasNoDuplicadas = new ArrayList<>();

        for (ItemsAsignaturaDTO itemsAsignaturaDTO : listaAsignaturasNoComunesDTO)
        {
            if (!asignaturasIds.contains(itemsAsignaturaDTO.getAsignaturaId()))
            {
                asignaturasIds.add(itemsAsignaturaDTO.getAsignaturaId());
                listaAsignaturasNoDuplicadas.add(itemsAsignaturaDTO);
            }
        }

        return listaAsignaturasNoDuplicadas;
    }

    private List<Asignatura> generaListaAsignaturasNoComunes(Long semestreId,
                                                             List<String> listaAsignaturasIds)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QItemsAsignaturaDTO qItemsAsignaturaDTO = QItemsAsignaturaDTO.itemsAsignaturaDTO;
        QItemDTO qItems = QItemDTO.itemDTO;

        List<Asignatura> listaAsignaturas = new ArrayList<Asignatura>();
        query.from(qItemsAsignaturaDTO, qItems)
                .where(qItems.id.eq(qItemsAsignaturaDTO.item.id)
                        .and(qItemsAsignaturaDTO.asignaturaId.in(listaAsignaturasIds))
                        .and(qItems.semestre.id.eq(semestreId)));

        for (Tuple asignatura : query.distinct().list(new QTuple(qItemsAsignaturaDTO.asignaturaId,
                qItemsAsignaturaDTO.nombreAsignatura, qItems.tipoAsignaturaId)))
        {
            listaAsignaturas.add(creaAsignaturaDesdeItemsAsignaturaDTO(asignatura));
        }

        return listaAsignaturas;
    }

    private Asignatura creaAsignaturaDesdeItemsAsignaturaNoComunDTO(
            ItemsAsignaturaDTO itemsAsignaturaDTO)
    {
        Asignatura asignatura = new Asignatura();

        asignatura.setId(itemsAsignaturaDTO.getAsignaturaId());
        asignatura.setNombre(itemsAsignaturaDTO.getNombreAsignatura());
        ItemDTO itemDTO = itemsAsignaturaDTO.getItem();
        String tipoAsignatura = itemDTO.getTipoAsignaturaId();
        asignatura.setComun(false);

        if (tipoAsignatura.equals("A"))
        {
            asignatura.setNombre(asignatura.getNombre() + " -- Anual -- ");
        }

        return asignatura;
    }

    private Asignatura creaAsignaturaDesdeItemsAsignaturaDTO(Tuple asignaturaTuple)
    {
        Asignatura asignatura = new Asignatura();

        asignatura.setId(asignaturaTuple.get(QItemsAsignaturaDTO.itemsAsignaturaDTO.asignaturaId));
        String nombreAsignaturaComun = asignaturaTuple
                .get(QAsignaturaComunDTO.asignaturaComunDTO.nombre);
        asignatura.setNombre(
                asignaturaTuple.get(QItemsAsignaturaDTO.itemsAsignaturaDTO.nombreAsignatura));
        String tipoAsignatura = asignaturaTuple.get(QItemDTO.itemDTO.tipoAsignaturaId);

        if (tipoAsignatura.equals("A"))
        {
            asignatura.setNombre(asignatura.getNombre() + " -- Anual -- ");
        }

        if (nombreAsignaturaComun != null)
        {
            asignatura.setComun(true);
            asignatura.setNombre(asignatura.getNombre() + " -- Comuns -- " + nombreAsignaturaComun);
            asignatura.setComunes(nombreAsignaturaComun);
        }
        else
        {
            asignatura.setComun(false);
        }
        asignatura.setCodigoComun(getCodigoAsignatura(
                asignaturaTuple.get(QAsignaturaComunDTO.asignaturaComunDTO.prefijo),
                asignaturaTuple.get(QItemsAsignaturaDTO.itemsAsignaturaDTO.asignaturaId)));

        return asignatura;
    }

    private String getCodigoAsignatura(String prefijo, String asignaturaId)
    {
        if (prefijo == null || prefijo.isEmpty())
        {
            return asignaturaId;
        }
        return prefijo + asignaturaId.substring(2);
    }

}
