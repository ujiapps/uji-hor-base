package es.uji.apps.hor.dao;

import es.uji.apps.hor.model.TipoSubgrupoUI;
import es.uji.commons.db.BaseDAO;

import java.util.List;

public interface TipoSubgruposDAO extends BaseDAO
{
    List<TipoSubgrupoUI> getSubGruposAsignatura(String asignaturaId, Long semestreId);
}
