package es.uji.apps.hor.dao;

import java.util.ArrayList;
import java.util.List;

import es.uji.apps.hor.model.Centro;
import org.springframework.stereotype.Repository;

import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.QTuple;

import es.uji.apps.hor.db.*;
import es.uji.apps.hor.model.Estudio;
import es.uji.apps.hor.model.TipoEstudio;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

@Repository
public class EstudiosDAODatabaseImpl extends BaseDAODatabaseImpl implements EstudiosDAO
{
    @Override
    public List<Estudio> getEstudios()
    {
        JPAQuery query = new JPAQuery(entityManager);
        QEstudioDTO qEstudio = QEstudioDTO.estudioDTO;

        List<EstudioDTO> listaEstudios = query.from(qEstudio).orderBy(qEstudio.nombre.asc())
                .list(qEstudio);

        List<Estudio> estudios = new ArrayList<Estudio>();

        for (EstudioDTO estudioDTO : listaEstudios)
        {
            estudios.add(creaEstudioDesdeEstudioDTO(estudioDTO));
        }

        return estudios;
    }


    @Override
    public List<Estudio> getEstudiosAbiertosConsulta()
    {
        JPAQuery query = new JPAQuery(entityManager);
        QEstudioDTO qEstudio = QEstudioDTO.estudioDTO;

        List<EstudioDTO> listaEstudios = query.from(qEstudio).where(qEstudio.abiertoConsulta.eq(true)).orderBy(qEstudio.nombre.asc())
                .list(qEstudio);

        List<Estudio> estudios = new ArrayList<Estudio>();

        for (EstudioDTO estudioDTO : listaEstudios)
        {
            estudios.add(creaEstudioDesdeEstudioDTO(estudioDTO));
        }

        return estudios;
    }

    @Override
    public Boolean isEstudioAbiertoConsulta(Long estudioId)
    {
        try
        {
            Estudio estudio = getEstudioById(estudioId);
            return estudio.getAbiertoConsulta();
        }
        catch (RegistroNoEncontradoException e)
        {
            return false;
        }
    }

    @Override
    public List<Estudio> getEstudiosVisiblesPorUsuario(Long userId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QEstudioDTO qEstudio = QEstudioDTO.estudioDTO;
        QCargoPersonaDTO qCargo = QCargoPersonaDTO.cargoPersonaDTO;

        List<EstudioDTO> listaEstudios = query.from(qEstudio)
                .innerJoin(qEstudio.cargosPersona, qCargo).where(qCargo.persona.id.eq(userId))
                .orderBy(qEstudio.nombre.asc()).list(qEstudio);

        List<Estudio> estudios = new ArrayList<Estudio>();

        for (EstudioDTO estudioDTO : listaEstudios)
        {
            estudios.add(creaEstudioDesdeEstudioDTO(estudioDTO));
        }

        return estudios;
    }

    @Override
    public List<Estudio> getEstudiosByCentroId(Long centroId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QEstudioDTO qEstudio = QEstudioDTO.estudioDTO;

        query.from(qEstudio).where(qEstudio.centro.id.eq(centroId)).orderBy(qEstudio.nombre.asc());

        List<Estudio> listaEstudios = new ArrayList<Estudio>();

        for (EstudioDTO estudioDTO : query.list(qEstudio))
        {
            listaEstudios.add(creaEstudioDesdeEstudioDTO(estudioDTO));
        }

        return listaEstudios;
    }

    private Estudio creaEstudioDesdeEstudioDTO(EstudioDTO estudioDTO)
    {
        Estudio estudio = new Estudio(estudioDTO.getId(), estudioDTO.getNombre());
        TipoEstudio tipoEstudio = new TipoEstudio(estudioDTO.getTipoEstudio().getId(), estudioDTO
                .getTipoEstudio().getNombre());

        estudio.setTipoEstudio(tipoEstudio);
        estudio.setId(estudioDTO.getId());
        estudio.setCentro(new Centro(estudioDTO.getCentro().getId(), estudioDTO.getCentro().getNombre()));
        return estudio;
    }

    private Estudio creaEstudioDesdeEstudioDTOConEstudiosCompartidos(EstudioDTO estudioDTO,
                                                                     List<EstudiosCompartidosDTO> estudiosCompartidosDTO)
    {
        Estudio estudio = new Estudio(estudioDTO.getId(), estudioDTO.getNombre());
        TipoEstudio tipoEstudio = new TipoEstudio(estudioDTO.getTipoEstudio().getId(), estudioDTO
                .getTipoEstudio().getNombre());

        List<Estudio> listaEstudiosCompartidos = new ArrayList<Estudio>();

        for (EstudiosCompartidosDTO estudioCompartidoDTO : estudiosCompartidosDTO)
        {
            Estudio estudioCompartido = new Estudio();
            estudioCompartido.setId(estudioCompartidoDTO.getEstudioCompartido().getId());
            estudioCompartido.setNombre(estudioCompartidoDTO.getEstudioCompartido().getNombre());
            listaEstudiosCompartidos.add(estudioCompartido);
        }
        estudio.setEstudiosCompartidos(listaEstudiosCompartidos);
        estudio.setTipoEstudio(tipoEstudio);
        estudio.setId(estudioDTO.getId());
        estudio.setAbiertoConsulta(estudioDTO.getAbiertoConsulta());
        estudio.setAbiertoSesionesPodPdi(estudioDTO.getAbiertoSesionesPodPdi());
        return estudio;
    }

    private EstudioDTO convierteEstudioAEstudioDTO(Estudio estudio)
    {
        TipoEstudioDTO tipoEstudioDTO = new TipoEstudioDTO();
        tipoEstudioDTO.setId(estudio.getTipoEstudio().getId());
        tipoEstudioDTO.setNombre(estudio.getTipoEstudio().getNombre());

        EstudioDTO estudioDTO = new EstudioDTO();
        estudioDTO.setId(estudio.getId());
        estudioDTO.setNombre(estudio.getNombre());
        estudioDTO.setTipoEstudio(tipoEstudioDTO);
        return estudioDTO;
    }

    @Override
    public Estudio insert(Estudio estudio)
    {
        EstudioDTO estudioDTO = convierteEstudioAEstudioDTO(estudio);
        EstudioDTO nuevoEstudioDTO = insert(estudioDTO);
        return creaEstudioDesdeEstudioDTO(nuevoEstudioDTO);
    }

    @Override
    public Estudio getEstudioById(Long estudioId) throws RegistroNoEncontradoException
    {
        JPAQuery query = new JPAQuery(entityManager);
        QEstudioDTO qEstudio = QEstudioDTO.estudioDTO;
        QEstudiosCompartidosDTO qEstudiosCompartidos = QEstudiosCompartidosDTO.estudiosCompartidosDTO;

        query.from(qEstudio).where(qEstudio.id.eq(estudioId));

        List<EstudioDTO> listaEstudios = query.list(qEstudio);

        if (listaEstudios.size() == 1)
        {
            EstudioDTO estudioDTO = listaEstudios.get(0);
            query.from(qEstudiosCompartidos).where(qEstudiosCompartidos.estudio.id.eq(estudioId));
            List<EstudiosCompartidosDTO> estudiosCompartidosDTO = query.list(qEstudiosCompartidos);
            return creaEstudioDesdeEstudioDTOConEstudiosCompartidos(estudioDTO,
                    estudiosCompartidosDTO);
        }
        else
        {
            throw new RegistroNoEncontradoException();
        }
    }

    @Override
    public List<Estudio> getEstudiosByAgrupacionId(Long agrupacionId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QEstudioDTO qEstudio = QEstudioDTO.estudioDTO;
        QAgrupacionEstudioDTO qAgrupacionEstudioDTO = QAgrupacionEstudioDTO.agrupacionEstudioDTO;

        query.from(qEstudio).join(qEstudio.agrupacionesEstudio, qAgrupacionEstudioDTO)
                .where(qAgrupacionEstudioDTO.agrupacion.id.eq(agrupacionId));

        List<Estudio> listaEstudios = new ArrayList<Estudio>();

        for (EstudioDTO estudioDTO : query.list(qEstudio))
        {
            listaEstudios.add(creaEstudioDesdeEstudioDTO(estudioDTO));
        }

        return listaEstudios;

    }

    @Override
    public Estudio getEstudioByAsignaturaId(String asignaturaId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QAsignaturaEstudioDTO qAsignaturaEstudioDTO = QAsignaturaEstudioDTO.asignaturaEstudioDTO;
        query.from(qAsignaturaEstudioDTO)
                .where(qAsignaturaEstudioDTO.asignaturaId.eq(asignaturaId));

        AsignaturaEstudioDTO asignaturaEstudioDTO = query.list(qAsignaturaEstudioDTO).get(0);

        return creaEstudioDesdeAsignaturaEstudioDTO(asignaturaEstudioDTO);
    }

    private Estudio creaEstudioDesdeAsignaturaEstudioDTO(AsignaturaEstudioDTO asignaturaEstudioDTO)
    {
        Estudio estudio = new Estudio(asignaturaEstudioDTO.getEstudioId(),
                asignaturaEstudioDTO.getEstudioNombre());
        return estudio;
    }

    @Override
    public List<Estudio> getEstudiosByEvento(Long itemId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QItemsAsignaturaDTO qItemsAsignaturaDTO = QItemsAsignaturaDTO.itemsAsignaturaDTO;
        List<Tuple> listaEstudiosTuples = query.from(qItemsAsignaturaDTO)
                .where(qItemsAsignaturaDTO.item.id.eq(itemId)).distinct()
                .list(new QTuple(qItemsAsignaturaDTO.estudioId, qItemsAsignaturaDTO.estudio));

        List<Estudio> estudios = new ArrayList<Estudio>();

        for (Tuple tuple : listaEstudiosTuples)
        {
            Estudio estudio = new Estudio();
            estudio.setId(tuple.get(qItemsAsignaturaDTO.estudioId));
            estudio.setNombre(tuple.get(qItemsAsignaturaDTO.estudio));
            estudios.add(estudio);
        }

        return estudios;
    }
}