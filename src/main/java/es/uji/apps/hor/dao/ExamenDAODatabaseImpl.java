package es.uji.apps.hor.dao;

import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.QTuple;

import es.uji.apps.hor.db.*;
import es.uji.apps.hor.model.*;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Repository
public class ExamenDAODatabaseImpl extends BaseDAODatabaseImpl implements ExamenDAO
{

    @Override
    public List<Examen> getExamenesNoPlanificados(Long estudioId, Long convocatoriaId,
                                                  List<Long> cursosList)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QExamenDTO qExamenDTO = QExamenDTO.examenDTO;
        QExamenAsignaturaDTO qExamenAsignaturaDTO = QExamenAsignaturaDTO.examenAsignaturaDTO;
        QEstudioDTO qEstudioDTO = QEstudioDTO.estudioDTO;
        QExamenAulaDTO qExamenAulaDTO = QExamenAulaDTO.examenAulaDTO;
        QAulaDTO qAulaDTO = QAulaDTO.aulaDTO;

        query.from(qExamenDTO)
                .join(qExamenDTO.asignaturasExamen, qExamenAsignaturaDTO)
                .fetch()
                .join(qExamenAsignaturaDTO.estudio, qEstudioDTO)
                .leftJoin(qExamenDTO.aulasExamen, qExamenAulaDTO)
                .fetch()
                .leftJoin(qExamenAulaDTO.aula, qAulaDTO)
                .fetch()
                .where(qExamenDTO.fecha.isNull().and(qExamenDTO.convocatoriaId.eq(convocatoriaId))
                        .and(qExamenAsignaturaDTO.estudio.id.eq(estudioId)));

        if (cursosList.size() > 0)
        {
            query.where(qExamenAsignaturaDTO.cursoId.in(cursosList));
        }

        List<Examen> examenes = new ArrayList<>();
        for (ExamenDTO examenDTO : query.list(qExamenDTO))
        {
            examenes.add(creaExamenDesde(examenDTO));
        }
        return examenes;
    }

    @Override
    public List<Examen> getExamenesPlanificados(Long estudioId, Long convocatoriaId,
                                                List<Long> cursosList)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QExamenDTO qExamenDTO = QExamenDTO.examenDTO;
        QExamenAsignaturaDTO qExamenAsignaturaDTO = QExamenAsignaturaDTO.examenAsignaturaDTO;
        QEstudioDTO qEstudioDTO = QEstudioDTO.estudioDTO;
        QExamenAulaDTO qExamenAulaDTO = QExamenAulaDTO.examenAulaDTO;
        QAulaDTO qAulaDTO = QAulaDTO.aulaDTO;

        query.from(qExamenDTO)
                .join(qExamenDTO.asignaturasExamen, qExamenAsignaturaDTO)
                .fetch()
                .join(qExamenAsignaturaDTO.estudio, qEstudioDTO)
                .leftJoin(qExamenDTO.aulasExamen, qExamenAulaDTO)
                .fetch()
                .leftJoin(qExamenAulaDTO.aula, qAulaDTO)
                .fetch()
                .where(qExamenDTO.fecha.isNotNull()
                        .and(qExamenDTO.convocatoriaId.eq(convocatoriaId))
                        .and(qExamenAsignaturaDTO.estudio.id.eq(estudioId)));

        if (cursosList.size() > 0)
        {
            query.where(qExamenAsignaturaDTO.cursoId.in(cursosList));
        }

        List<Examen> examenes = new ArrayList<>();
        for (ExamenDTO examenDTO : query.distinct().list(qExamenDTO))
        {
            examenes.add(creaExamenDesde(examenDTO));
        }
        return examenes;
    }

    @Override
    @Transactional
    public void updateExamen(Examen examen)
    {
        ExamenDTO examenDTO = creaExamenDTODesdeExamen(examen);
        update(examenDTO);
    }

    @Override
    public Examen getExamenById(Long examenId) throws RegistroNoEncontradoException
    {
        JPAQuery query = new JPAQuery(entityManager);

        QExamenDTO qExamenDTO = QExamenDTO.examenDTO;
        QExamenAsignaturaDTO qExamenAsignaturaDTO = QExamenAsignaturaDTO.examenAsignaturaDTO;
        QExamenAulaDTO qExamenAulaDTO = QExamenAulaDTO.examenAulaDTO;
        QAulaDTO qAulaDTO = QAulaDTO.aulaDTO;

        query.from(qExamenDTO).leftJoin(qExamenDTO.asignaturasExamen, qExamenAsignaturaDTO).fetch()
                .leftJoin(qExamenDTO.aulasExamen, qExamenAulaDTO).fetch()
                .leftJoin(qExamenAulaDTO.aula, qAulaDTO).fetch().where(qExamenDTO.id.eq(examenId))
                .distinct();

        List<ExamenDTO> examenDTO = query.list(qExamenDTO);

        if (examenDTO.size() == 1)
        {
            return creaExamenDesde(examenDTO.get(0));
        }
        else
        {
            throw new RegistroNoEncontradoException();
        }
    }

    private ExamenDTO creaExamenDTODesdeExamen(Examen examen)
    {
        ExamenDTO examenDTO = new ExamenDTO();
        examenDTO.setId(examen.getId());
        examenDTO.setNombre(examen.getNombre());
        examenDTO.setFecha(examen.getFecha());
        examenDTO.setHoraInicio(examen.getHoraInicio());
        examenDTO.setHoraFin(examen.getHoraFin());
        examenDTO.setComun(examen.isComun());
        examenDTO.setPermiteSabados(examen.getPermiteSabados());
        examenDTO.setComunTexto(examen.getAsignaturasComunes());
        examenDTO.setConvocatoriaId(examen.getConvocatoria().getId());
        examenDTO.setConvocatoriaNombre(examen.getConvocatoria().getNombre());
        examenDTO.setTipoExamen(creaTipoExamenDTODesde(examen.getTipoExamen()));
        examenDTO.setComentario(examen.getComentario());
        examenDTO.setComentarioES(examen.getComentarioES());
        examenDTO.setComentarioUK(examen.getComentarioUK());
        examenDTO.setDefinitivo(examen.isDefinitivo());
        return examenDTO;
    }

    private TipoExamenDTO creaTipoExamenDTODesde(TipoExamen tipoExamen)
    {
        TipoExamenDTO tipoExamenDTO = new TipoExamenDTO();
        tipoExamenDTO.setId(tipoExamen.getId());
        tipoExamenDTO.setNombre(tipoExamen.getNombre());
        tipoExamenDTO.setPorDefecto(tipoExamen.getPorDefecto());
        tipoExamenDTO.setCodigo(tipoExamen.getCodigo());
        return tipoExamenDTO;
    }

    private Examen creaExamenDesde(ExamenDTO examenDTO)
    {
        Examen examen = new Examen();
        examen.setId(examenDTO.getId());

        if (examenDTO.getComunTexto() != null)
        {
            examen.setNombre(examenDTO.getComunTexto());
        }
        else
        {
            examen.setNombre(examenDTO.getNombre());
        }

        examen.setFecha(examenDTO.getFecha());
        examen.setHoraInicio(getHoraEnFecha(examenDTO.getHoraInicio(), examenDTO.getFecha()));
        examen.setHoraFin(getHoraEnFecha(examenDTO.getHoraFin(), examenDTO.getFecha()));
        examen.setComun(examenDTO.isComun());
        examen.setPermiteSabados(examenDTO.getPermiteSabados());
        examen.setAsignaturasComunes(examenDTO.getComunTexto());
        examen.setConvocatoria(creaConvocatoriaDesde(examenDTO));
        examen.setComentario(examenDTO.getComentario());
        examen.setComentarioES(examenDTO.getComentarioES());
        examen.setComentarioUK(examenDTO.getComentarioUK());
        examen.setTipoExamen(creaTipoExamenDesde(examenDTO.getTipoExamen()));
        examen.setDefinitivo(examenDTO.getDefinitivo());
        if (examenDTO.getAsignaturasExamen() != null)
        {
            List<Asignatura> asignaturas = new ArrayList<>();
            for (ExamenAsignaturaDTO examenAsignaturaDTO : examenDTO.getAsignaturasExamen())
            {
                asignaturas.add(crearDTODesde(examenAsignaturaDTO));
            }
            examen.setAsignaturasExamen(asignaturas);
        }
        if (examenDTO.getAulasExamen() != null)
        {
            List<Aula> aulas = new ArrayList<>();
            for (ExamenAulaDTO examenAulaDTO : examenDTO.getAulasExamen())
            {
                aulas.add(crearDTODesde(examenAulaDTO));
            }
            examen.setAulasExamen(aulas);
        }
        return examen;
    }

    private Aula crearDTODesde(ExamenAulaDTO examenAulaDTO)
    {
        Aula aula = new Aula();
        aula.setId(examenAulaDTO.getAula().getId());
        aula.setCodigo(examenAulaDTO.getAula().getCodigo());
        aula.setNombre(examenAulaDTO.getAula().getNombre());
        return aula;
    }

    private Asignatura crearDTODesde(ExamenAsignaturaDTO examenAsignaturaDTO)
    {
        Asignatura asignatura = new Asignatura();
        asignatura.setId(examenAsignaturaDTO.getAsignaturaId());
        asignatura.setNombre(examenAsignaturaDTO.getAsignaturaNombre());
        asignatura.setCursoId(examenAsignaturaDTO.getCursoId());
        asignatura.setTipoAsignatura(examenAsignaturaDTO.getTipoAsignatura());
        return asignatura;
    }

    private Date getHoraEnFecha(Date hora, Date fecha)
    {
        if (fecha == null || hora == null)
        {
            return null;
        }

        Calendar calendarioFecha = Calendar.getInstance();
        calendarioFecha.setTime(fecha);

        Calendar calendarioHora = Calendar.getInstance();
        calendarioHora.setTime(hora);

        calendarioFecha.set(Calendar.HOUR_OF_DAY, calendarioHora.get(Calendar.HOUR_OF_DAY));
        calendarioFecha.set(Calendar.MINUTE, calendarioHora.get(Calendar.MINUTE));
        calendarioFecha.set(Calendar.SECOND, 0);

        return calendarioFecha.getTime();
    }

    @Override
    public List<Convocatoria> getConvocatorias()
    {
        JPAQuery query = new JPAQuery(entityManager);
        QFechaConvocatoriaDTO qFechaConvocatoriaDTO = QFechaConvocatoriaDTO.fechaConvocatoriaDTO;
        query.from(qFechaConvocatoriaDTO).orderBy(qFechaConvocatoriaDTO.id.asc());

        List<Convocatoria> convocatorias = new ArrayList<>();
        for (FechaConvocatoriaDTO convocatoria : query.list(qFechaConvocatoriaDTO))
        {
            convocatorias.add(creaConvocatoriaDesde(convocatoria));
        }
        return convocatorias;
    }

    @Override
    public Convocatoria getConvocatoriaById(Long convocatoriaId)
            throws RegistroNoEncontradoException
    {
        List<FechaConvocatoriaDTO> convocatoriaDTO = get(FechaConvocatoriaDTO.class, convocatoriaId);

        if (convocatoriaDTO.size() > 0)
        {
            return creaConvocatoriaDesde(convocatoriaDTO.get(0));
        }
        else
        {
            throw new RegistroNoEncontradoException();
        }
    }

    private Convocatoria creaConvocatoriaDesde(FechaConvocatoriaDTO convocatoriaDTO)
    {
        Convocatoria convocatoria = new Convocatoria();
        convocatoria.setId(convocatoriaDTO.getId());
        convocatoria.setNombre(convocatoriaDTO.getNombre());
        convocatoria.setInicio(convocatoriaDTO.getFechaExamenesInicio());
        convocatoria.setFin(convocatoriaDTO.getFechaExamenesFin());
        return convocatoria;
    }

    private Convocatoria creaConvocatoriaDesde(ExamenDTO examenDTO)
    {
        Convocatoria convocatoria = new Convocatoria();
        convocatoria.setId(examenDTO.getConvocatoriaId());
        convocatoria.setNombre(examenDTO.getConvocatoriaNombre());
        return convocatoria;
    }

    @Override
    public Convocatoria getConvocatoriaEstudioById(Long convocatoriaId, Long estudioId, Long cursoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QFechaConvocatoriaEstudioDTO qFechaConvocatoriaEstudioDTO = QFechaConvocatoriaEstudioDTO.fechaConvocatoriaEstudioDTO;
        query.from(qFechaConvocatoriaEstudioDTO).where(
                qFechaConvocatoriaEstudioDTO.estudio.id.eq(estudioId).and(qFechaConvocatoriaEstudioDTO.cursoId.eq(cursoId)).and(
                        qFechaConvocatoriaEstudioDTO.convocatoriaId.eq(convocatoriaId)));

        FechaConvocatoriaEstudioDTO convocatoria = query.list(qFechaConvocatoriaEstudioDTO).get(0);
        return creaConvocatoriaDesde(convocatoria);
    }

    @Override
    public List<TipoExamen> getTiposExamen()
    {
        JPAQuery query = new JPAQuery(entityManager);
        QTipoExamenDTO qTipoExamenDTO = QTipoExamenDTO.tipoExamenDTO;
        query.from(qTipoExamenDTO).orderBy(qTipoExamenDTO.porDefecto.desc());

        List<TipoExamen> tipoExamenes = new ArrayList<>();
        for (TipoExamenDTO tipoExamen : query.list(qTipoExamenDTO))
        {
            tipoExamenes.add(creaTipoExamenDesde(tipoExamen));
        }
        return tipoExamenes;
    }

    @Override
    public List<Curso> getCursosEstudio(Long estudioId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QExamenAsignaturaDTO qExamenAsignaturaDTO = QExamenAsignaturaDTO.examenAsignaturaDTO;

        List<Tuple> listaSubGruposTuples = query.from(qExamenAsignaturaDTO)
                .where(qExamenAsignaturaDTO.estudio.id.eq(estudioId))
                .orderBy(qExamenAsignaturaDTO.cursoId.asc()).distinct()
                .list(new QTuple(qExamenAsignaturaDTO.cursoId));

        List<Curso> cursos = new ArrayList<Curso>();

        for (Tuple tuple : listaSubGruposTuples)
        {
            cursos.add(new Curso(tuple.get(qExamenAsignaturaDTO.cursoId)));
        }

        return cursos;
    }

    @Override
    @Transactional
    public void deleteExamenById(Long id)
    {
        delete(ExamenDTO.class, id);
    }

    @Override
    public List<Examen> getExamenesPlanificadosByAsignaturaAndConvocatoria(String asignaturaId,
                                                                           Long convocatoriaId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QExamenDTO qExamenDTO = QExamenDTO.examenDTO;
        QExamenAsignaturaDTO qExamenAsignaturaDTO = QExamenAsignaturaDTO.examenAsignaturaDTO;
        QExamenAulaDTO qExamenAulaDTO = QExamenAulaDTO.examenAulaDTO;
        QAulaDTO qAulaDTO = QAulaDTO.aulaDTO;

        query.from(qExamenDTO)
                .join(qExamenDTO.asignaturasExamen, qExamenAsignaturaDTO)
                .fetch()
                .leftJoin(qExamenDTO.aulasExamen, qExamenAulaDTO)
                .fetch()
                .leftJoin(qExamenAulaDTO.aula, qAulaDTO)
                .fetch()
                .where(qExamenDTO.fecha.isNotNull().and(qExamenDTO.nombre.eq(asignaturaId))
                        .and(qExamenDTO.convocatoriaId.eq(convocatoriaId)));

        List<Examen> examenes = new ArrayList<>();
        for (ExamenDTO examenDTO : query.distinct().list(qExamenDTO))
        {
            examenes.add(creaExamenDesde(examenDTO));
        }
        return examenes;
    }

    @Override
    @Transactional
    public void insertaExamen(Examen nuevoExamen, Long examenOriginalId)
    {
        ExamenDTO examenDTO = creaExamenDTODesdeExamen(nuevoExamen);
        examenDTO = insert(examenDTO);

        insertaExamenAsignaturaNuevoExamen(examenOriginalId, examenDTO);
    }

    @Override
    @Transactional
    public void deleteExamenAsignaturaByExamenId(Long examenId)
    {
        QExamenAsignaturaDTO qExamenAsignaturaDTO = QExamenAsignaturaDTO.examenAsignaturaDTO;
        JPADeleteClause deleteClause = new JPADeleteClause(entityManager, qExamenAsignaturaDTO);

        deleteClause.where(qExamenAsignaturaDTO.examen.id.eq(examenId)).execute();
    }

    @Override
    @Transactional
    public void deleteExamenAulaByExamenId(Long examenId)
    {
        QExamenAulaDTO qExamenAulaDTO = QExamenAulaDTO.examenAulaDTO;
        JPADeleteClause deleteClause = new JPADeleteClause(entityManager, qExamenAulaDTO);

        deleteClause.where(qExamenAulaDTO.examen.id.eq(examenId)).execute();
    }

    @Override
    public List<Convocatoria> getConvocatoriasByEstudio(Long estudioId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QFechaConvocatoriaEstudioDTO qFechaConvocatoriaEstudioDTO = QFechaConvocatoriaEstudioDTO.fechaConvocatoriaEstudioDTO;
        query.from(qFechaConvocatoriaEstudioDTO)
                .where(qFechaConvocatoriaEstudioDTO.estudio.id.eq(estudioId))
                .orderBy(qFechaConvocatoriaEstudioDTO.id.asc());

        List<Convocatoria> convocatorias = new ArrayList<>();
        for (FechaConvocatoriaEstudioDTO convocatoriaEstudioDTO : query
                .list(qFechaConvocatoriaEstudioDTO))
        {
            convocatorias.add(creaConvocatoriaDesde(convocatoriaEstudioDTO));
        }
        return convocatorias;
    }

    @Override
    @Transactional
    public void borraAulasExamen(Long examenId)
    {
        QExamenAulaDTO qExamenAulaDTO = QExamenAulaDTO.examenAulaDTO;
        JPADeleteClause deleteClause = new JPADeleteClause(entityManager, qExamenAulaDTO);
        deleteClause.where(qExamenAulaDTO.examen.id.eq(examenId)).execute();
    }

    @Override
    @Transactional
    public void insertaAulasExamen(Long examenId, Long aulaId)
    {
        ExamenAulaDTO examenAulaDTO = new ExamenAulaDTO();

        AulaDTO aulaDTO = new AulaDTO();
        aulaDTO.setId(aulaId);
        examenAulaDTO.setAula(aulaDTO);

        ExamenDTO examenDTO = new ExamenDTO();
        examenDTO.setId(examenId);
        examenAulaDTO.setExamen(examenDTO);

        insert(examenAulaDTO);
    }

    private Convocatoria creaConvocatoriaDesde(
            FechaConvocatoriaEstudioDTO fechaConvocatoriaEstudioDTO)
    {
        Convocatoria convocatoria = new Convocatoria();
        convocatoria.setId(fechaConvocatoriaEstudioDTO.getConvocatoriaId());
        convocatoria.setNombre(fechaConvocatoriaEstudioDTO.getNombre());
        convocatoria.setInicio(fechaConvocatoriaEstudioDTO.getFechaExamenesInicio());
        convocatoria.setFin(fechaConvocatoriaEstudioDTO.getFechaExamenesFin());
        return convocatoria;
    }

    private void insertaExamenAsignaturaNuevoExamen(Long examenOriginalId, ExamenDTO examenDTO)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QExamenAsignaturaDTO qExamenAsignaturaDTO = QExamenAsignaturaDTO.examenAsignaturaDTO;

        query.from(qExamenAsignaturaDTO).where(qExamenAsignaturaDTO.examen.id.eq(examenOriginalId));

        for (ExamenAsignaturaDTO examenAsignaturaDTO : query.list(qExamenAsignaturaDTO))
        {
            ExamenAsignaturaDTO nuevoExamenAsignaturaDTO = new ExamenAsignaturaDTO();
            nuevoExamenAsignaturaDTO.setAsignaturaId(examenAsignaturaDTO.getAsignaturaId());
            nuevoExamenAsignaturaDTO.setAsignaturaNombre(examenAsignaturaDTO.getAsignaturaNombre());
            nuevoExamenAsignaturaDTO.setEstudio(examenAsignaturaDTO.getEstudio());
            nuevoExamenAsignaturaDTO.setEstudioNombre(examenAsignaturaDTO.getEstudioNombre());
            nuevoExamenAsignaturaDTO.setExamen(examenDTO);
            nuevoExamenAsignaturaDTO.setSemestre(examenAsignaturaDTO.getSemestre());
            nuevoExamenAsignaturaDTO.setTipoAsignatura(examenAsignaturaDTO.getTipoAsignatura());
            nuevoExamenAsignaturaDTO.setCentroId(examenAsignaturaDTO.getCentroId());
            nuevoExamenAsignaturaDTO.setCursoId(examenAsignaturaDTO.getCursoId());
            insert(nuevoExamenAsignaturaDTO);
        }
    }

    private TipoExamen creaTipoExamenDesde(TipoExamenDTO tipoExamenDTO)
    {
        TipoExamen tipoExamen = new TipoExamen();
        tipoExamen.setId(tipoExamenDTO.getId());
        tipoExamen.setNombre(tipoExamenDTO.getNombre());
        tipoExamen.setPorDefecto(tipoExamenDTO.getPorDefecto());
        tipoExamen.setCodigo(tipoExamenDTO.getCodigo());
        return tipoExamen;
    }
}
