package es.uji.apps.hor.dao;

import java.util.List;

import es.uji.apps.hor.model.Circuito;
import es.uji.commons.db.BaseDAO;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

public interface CircuitoDAO extends BaseDAO
{

    List<Circuito> getCircuitosByEstudioIdAndGrupoId(Long estudioId, String grupoId);

    void deleteCircuitoById(Long circuitoId) throws RegistroConHijosException;

    Circuito getCircuitoById(Long circuitoId);

    Circuito insertNuevoCircuitoEnEstudio(Long circuitoId, Long estudioId);

    void borraCircuitoDeEstudio(Long circuitoId, Long estudioId)
            throws RegistroNoEncontradoException, RegistroConHijosException;

    Circuito getCircuitoCompletoById(Long circuitoId);

    Circuito insertNuevoCircuito(Circuito circuito);

    void updateCircuito(Circuito circuito);
}
