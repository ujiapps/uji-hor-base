package es.uji.apps.hor.dao;

import es.uji.apps.hor.EventoDetalleSinEventoException;
import es.uji.apps.hor.db.ItemDTO;
import es.uji.apps.hor.db.ItemsAsignaturaDTO;
import es.uji.apps.hor.model.*;
import es.uji.commons.db.BaseDAO;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

import java.util.Date;
import java.util.List;

public interface EventosDAO extends BaseDAO
{
    List<Evento> getEventosSemanaGenerica(Long estudioId, Long cursoId, Long semestreId,
            List<String> gruposIds, List<String> asignaturasIds, List<Long> calendariosIds);

    List<Evento> getEventosEditablesEnSemanaDetalleByEventoIds(List<Long> eventoIds);

    List<Evento> getEventosDeUnCurso(Long estudioId, Long cursoId, Long semestreId, String grupoId);

    void deleteEventoSemanaGenerica(Long eventoId) throws RegistroNoEncontradoException;

    List<EventoDocencia> getDiasDocenciaDeUnEventoByEventoId(Long eventoId);

    List<EventoDetalle> getEventosDetalle(Long estudioId, Long cursoId, Long semestreId,
            List<String> gruposIds, List<String> asignaturasIds, List<Long> calendariosIds,
            Date rangoFechaInicio, Date rangoFechaFin);

    void actualizaAulaAsignadaAEvento(Long eventoId, Long aulaId)
            throws RegistroNoEncontradoException;

    Evento getEventoById(Long eventoId) throws RegistroNoEncontradoException;

    Evento insertEvento(Evento eventoDividido);

    void updateHorasEventoYSusDetalles(Evento evento);

    void updateHorasEventoDetalle(EventoDetalle eventoDetalle);

    EventoDetalle insertEventoDetalle(EventoDetalle eventoDetalle)
            throws EventoDetalleSinEventoException;

    void deleteEventoDetalle(EventoDetalle detalle);

    void deleteDetallesDeEvento(Evento evento);

    Evento updateEvento(Evento evento);

    Evento modificaDetallesGrupoAsignatura(Evento evento);

    Asignatura creaAsignaturasDesdeItemAsignaturaDTO(ItemsAsignaturaDTO asig, ItemDTO itemDTO);

    void desasignaAula(Long eventoId) throws RegistroNoEncontradoException;

    void desplanificaEvento(Evento evento);

    List<EventoDetalle> getEventosDetallePorAula(Long aulaId, Long semestreId,
            List<Long> calendariosIds, Date rangoFechaInicio, Date rangoFechaFin);

    List<Evento> getEventosSemanaGenericaPorAula(Long aulaId, Long semestreId,
            List<Long> calendariosIds);

    List<Evento> getEventosDelMismoGrupo(Evento evento);

    List<Evento> getEventosMismoGrupoConDetalleProfesor(Evento evento, Long profesorId);

    List<Evento> getEventosDelMismoGrupoIncluyendoAnuales(Evento evento);

    Evento getEventoByIdConDetalles(Long eventoId) throws RegistroNoEncontradoException;

    Evento getEventoByEventoDetalleId(Long eventoDetalleId) throws RegistroNoEncontradoException;

    List<Evento> getTodosLosEventosSemanaGenericaCircuito(Long estudioId, Long semestreId,
            String grupoId, Long circuitoId);

    List<Evento> getEventosNoPlanificadosSemanaGenericaCircuito(Long estudioId, Long semestreId,
            String grupoId, Long circuitoId);

    void asignaCircuitoAEventos(List<Long> eventosIds, Long circuitoId)
            throws RegistroNoEncontradoException;

    void desasignaCircuitoDeEventos(List<Long> eventosIds, Long circuitoId)
            throws RegistroNoEncontradoException;

    List<EventoDetalle> getTodosLosEventosSemanaDetalleCircuito(Long estudioId, Long semestreId,
            String grupoId, Long circuitoId, Date rangoFechaInicio, Date rangoFechaFin);

    List<Evento> getEventosSemanaGenericaPlanificadosPorAsignaturaIdYSemestreId(
            String asignaturaId, Long semestreId, List<String> gruposList,
            List<String> tipoSubgruposList);

    List<Evento> getEventosByAsignatura(String asignaturaId);

    List<EventoDetalle> getEventosSemanaDetallePlanificadosPorAsignaturaIdYSemestreId(
            String asignaturaId, Long semestreId, List<String> gruposList,
            List<String> tipoSubgruposList);

    List<Evento> getEventosSemanaGenericaNoPlanificadosPorAsignaturaIdYSemestreIdConProfesores(
            String asignaturaId, Long semestreId, List<String> gruposList,
            List<String> tipoSubgruposList);

    List<EventoDocencia> getFechasEventoByProfesor(Long profesorId, Long eventoId);

    List<EventoDocencia> getFechasEventoSeleccionadasDetalleManualOtrosProfesores(Long profesorId, Long eventoId);

    List<Evento> getEventosProfesor(Long profesorId, String asignaturaId, Long semestreId);

    List<Evento> getEventosProfesorNoPlanificadosQueNoPertenezcanAsignatura(Long profesorId,
            String asignaturaId, Long semestreId);

    List<EventoDocencia> getDiasFestivosDeUnEventoByEventoId(Long eventoId);

    List<Evento> getEventosSemanaGenericaByAgrupacion(Long estudioId, Long agrupacionId,
            Long semestreId, List<String> gruposIds, List<String> asignaturasIds,
            List<Long> calendariosIds);

    List<EventoDetalle> getEventosDetallePorAgrupacion(Long estudioId, Long agrupacionId,
            Long semestreId, List<String> gruposIds, List<String> asignaturasIds,
            List<Long> calendariosIds, Date rangoFechaInicio, Date rangoFechaFin);

    List<Subgrupo> subgruposPorAsignaturaEstudioYCurso(String asignaturaId, Long estudioId,
            Long cursoId);

    List<Evento> getEventosByListaAsignaturasYSemestre(List<String> asignaturasIds, Long semestreId);

    Evento undo(Log log);

    List<Evento> getEventosProfesorByAsignaturaAndSemestre(Long profesorId, String asignaturaId, Long semestreId);
}
