package es.uji.apps.hor.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;

import es.uji.apps.hor.db.LogDTO;
import es.uji.apps.hor.db.QLogDTO;
import es.uji.apps.hor.model.Evento;
import es.uji.apps.hor.model.Log;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class LogDAODatabaseImpl extends BaseDAODatabaseImpl implements LogDAO
{
    @Override
    @Transactional
    public void register(int tipoAccion, String descripcion, Evento evento, String usuario)
    {
        Date fecha = getFechaActual();
        LogDTO logDTO = new LogDTO();
        logDTO.setTipoAccion(new Long(tipoAccion));
        logDTO.setDescripcion(descripcion);
        logDTO.setFecha(fecha);
        logDTO.setItemId(evento.getId());
        logDTO.setUsuario(usuario);

        logDTO.setDiaSemana(evento.getDia() != null ? evento.getDia().longValue() : null);
        logDTO.setHoraInicio(evento.getInicio());
        logDTO.setHoraFin(evento.getFin());
        logDTO.setAulaId(evento.getAula() != null ? evento.getAula().getId() : null);
        logDTO.setDeshecho(false);

        insert(logDTO);
    }

    private Date getFechaActual()
    {
        Calendar calendar = Calendar.getInstance();
        return calendar.getTime();
    }

    private Log convierteLogDTOALog(LogDTO logDTO)
    {
        Log log = new Log();
        log.setId(logDTO.getId());
        Evento evento = new Evento();
        evento.setId(logDTO.getItemId());

        log.setEvento(evento);
        log.setFecha(logDTO.getFecha());
        log.setDescripcion(logDTO.getDescripcion());
        log.setUsuario(logDTO.getUsuario());
        log.setHoraInicio(logDTO.getHoraInicio());
        log.setHoraFin(logDTO.getHoraFin());
        log.setDiaSemana(logDTO.getDiaSemana());
        log.setAulaId(logDTO.getAulaId());
        log.setTipoAccion(logDTO.getTipoAccion());
        log.setDeshecho(logDTO.isDeshecho());

        return log;
    }

    @Override
    public List<Log> getLogsByFecha(Date fecha)
    {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(fecha);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        JPAQuery query = new JPAQuery(entityManager);
        QLogDTO qLogDTO = QLogDTO.logDTO;
        query.from(qLogDTO).where(qLogDTO.fecha.year().eq(year).and(qLogDTO.fecha.month().eq(month)).and(qLogDTO.fecha.dayOfMonth().eq(day)).and(qLogDTO.deshecho.eq(false))).orderBy(qLogDTO.fecha.desc());

        List<Log> listaLog = new ArrayList<>();
        for (LogDTO logDTO : query.list(qLogDTO))
        {
            listaLog.add(convierteLogDTOALog(logDTO));
        }
        return listaLog;
    }

    @Override
    public List<Log> getLogsByFechaYUsuario(Date fecha, String usuario)
    {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(fecha);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        JPAQuery query = new JPAQuery(entityManager);

        QLogDTO qLogDTO = QLogDTO.logDTO;
        query.from(qLogDTO).where(qLogDTO.fecha.year().eq(year).and(qLogDTO.fecha.month().eq(month)).and(qLogDTO.fecha.dayOfMonth().eq(day)).and(qLogDTO.usuario.eq(usuario)).and(qLogDTO.deshecho.eq(false)));

        List<Log> listaLog = new ArrayList<>();
        for (LogDTO logDTO : query.list(qLogDTO))
        {
            listaLog.add(convierteLogDTOALog(logDTO));
        }
        return listaLog;
    }

    @Override
    public List<Log> getLogsByFechaYItemId(Date fecha, Long itemId)
    {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(fecha);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        JPAQuery query = new JPAQuery(entityManager);

        QLogDTO qLogDTO = QLogDTO.logDTO;
        query.from(qLogDTO).where(qLogDTO.fecha.year().eq(year).and(qLogDTO.fecha.month().eq(month)).and(qLogDTO.fecha.dayOfMonth().eq(day)).and(qLogDTO.itemId.eq(itemId)).and(qLogDTO.deshecho.eq(false)));

        List<Log> listaLog = new ArrayList<Log>();
        for (LogDTO logDTO : query.list(qLogDTO))
        {
            listaLog.add(convierteLogDTOALog(logDTO));
        }
        return listaLog;
    }

    @Override
    public List<Log> getLogsByFechaYItemIdYUsuario(Date fecha, Long itemId, String usuario)
    {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(fecha);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        JPAQuery query = new JPAQuery(entityManager);

        QLogDTO qLogDTO = QLogDTO.logDTO;
        query.from(qLogDTO).where(
                qLogDTO.fecha.year().eq(year).and(qLogDTO.fecha.month().eq(month)).and(qLogDTO.fecha.dayOfMonth().eq(day)).and(
                        qLogDTO.usuario.eq(usuario).and(qLogDTO.itemId.eq(itemId))).and(qLogDTO.deshecho.eq(false)));

        List<Log> listaLog = new ArrayList<Log>();
        for (LogDTO logDTO : query.list(qLogDTO))
        {
            listaLog.add(convierteLogDTOALog(logDTO));
        }
        return listaLog;
    }

    @Override
    public List<Log> getLogsByItemId(Long itemId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QLogDTO qLogDTO = QLogDTO.logDTO;
        query.from(qLogDTO).where(qLogDTO.itemId.eq(itemId).and(qLogDTO.deshecho.eq(false))).orderBy(qLogDTO.fecha.desc());

        List<Log> listaLog = new ArrayList<Log>();
        for (LogDTO logDTO : query.list(qLogDTO))
        {
            listaLog.add(convierteLogDTOALog(logDTO));
        }
        return listaLog;
    }

    @Override
    public Log getLogById(Long id)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QLogDTO qLogDTO = QLogDTO.logDTO;
        List<LogDTO> results = query.from(qLogDTO).where(qLogDTO.id.eq(id)).list(qLogDTO);

        if (results.isEmpty())
        {
            return null;
        }
        return convierteLogDTOALog(results.get(0));
    }

    @Transactional
    @Override
    public void invalidaLog(Long id)
    {
        QLogDTO qLogDTO = QLogDTO.logDTO;
        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, qLogDTO);
        updateClause.where(qLogDTO.id.eq(id))
                .set(qLogDTO.deshecho, true)
                .execute();
    }
}
