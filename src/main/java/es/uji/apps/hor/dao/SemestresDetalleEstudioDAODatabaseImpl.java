package es.uji.apps.hor.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.hor.db.*;
import es.uji.apps.hor.model.*;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class SemestresDetalleEstudioDAODatabaseImpl extends BaseDAODatabaseImpl implements
        SemestresDetalleEstudioDAO
{

    @Override
    public List<SemestreDetalleEstudio> getSemestresDetalleEstudios()
    {
        JPAQuery query = new JPAQuery(entityManager);

        QDetalleSemestreEstudioDTO detalleSemestreEstudio = QDetalleSemestreEstudioDTO.detalleSemestreEstudioDTO;
        QDetalleSemestreDTO detalleDemestre = QDetalleSemestreDTO.detalleSemestreDTO;
        QEstudioDTO estudio = QEstudioDTO.estudioDTO;

        List<DetalleSemestreEstudioDTO> queryResult = query.from(detalleSemestreEstudio)
                .join(detalleSemestreEstudio.detalleSemestre, detalleDemestre).fetch()
                .join(detalleSemestreEstudio.estudio, estudio).fetch().list(detalleSemestreEstudio);

        List<SemestreDetalleEstudio> semestreDetalleEstudios = new ArrayList<SemestreDetalleEstudio>();
        for (DetalleSemestreEstudioDTO detalle : queryResult)
        {
            semestreDetalleEstudios.add(convierteDetalleDTOEnDetalleModelo(detalle));
        }
        return semestreDetalleEstudios;
    }

    @Override
    public SemestreDetalleEstudio getSemestresDetalleEstudioById(Long semestreDetalleEstudioId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QDetalleSemestreEstudioDTO detalleSemestreEstudio = QDetalleSemestreEstudioDTO.detalleSemestreEstudioDTO;
        QDetalleSemestreDTO detalleDemestre = QDetalleSemestreDTO.detalleSemestreDTO;
        QEstudioDTO estudio = QEstudioDTO.estudioDTO;

        List<DetalleSemestreEstudioDTO> queryResult = query.from(detalleSemestreEstudio)
                .join(detalleSemestreEstudio.detalleSemestre, detalleDemestre).fetch()
                .join(detalleSemestreEstudio.estudio, estudio).fetch()
                .where(detalleSemestreEstudio.id.eq(semestreDetalleEstudioId))
                .list(detalleSemestreEstudio);

        if (queryResult.size() > 0)
        {
            return convierteDetalleDTOEnDetalleModelo(queryResult.get(0));
        }
        return null;
    }

    private SemestreDetalleEstudio convierteDetalleDTOEnDetalleModelo(
            DetalleSemestreEstudioDTO detalleSemestreEstudioDTO)
    {
        SemestreDetalleEstudio detalleSemestreEstudio = new SemestreDetalleEstudio();
        detalleSemestreEstudio.setId(detalleSemestreEstudioDTO.getId());
        detalleSemestreEstudio.setCursoAcademico(detalleSemestreEstudioDTO.getCursoAcademicoId());
        detalleSemestreEstudio.setEstudio(creaEstudioDesdeDTO(detalleSemestreEstudioDTO
                .getEstudio()));
        detalleSemestreEstudio.setSemestreDetalle(creaModeloDesdeDTO(detalleSemestreEstudioDTO
                .getDetalleSemestre()));
        detalleSemestreEstudio.setFechaInicio(detalleSemestreEstudioDTO.getFechaInicio());
        detalleSemestreEstudio.setFechaFin(detalleSemestreEstudioDTO.getFechaFin());
        detalleSemestreEstudio.setFechaExamenesInicio(detalleSemestreEstudioDTO
                .getFechaExamenesInicio());
        detalleSemestreEstudio.setFechaExamenesFin(detalleSemestreEstudioDTO.getFechaExamenesFin());
        detalleSemestreEstudio.setFechaExamenesInicioC2(detalleSemestreEstudioDTO
                .getFechaExamenesInicioC2());
        detalleSemestreEstudio.setFechaExamenesFinC2(detalleSemestreEstudioDTO
                .getFechaExamenesFinC2());
        detalleSemestreEstudio.setNumeroSemanas(detalleSemestreEstudioDTO.getNumeroSemanas());

        return detalleSemestreEstudio;
    }

    private Estudio creaEstudioDesdeDTO(EstudioDTO estudioDTO)
    {
        Estudio estudio = new Estudio(estudioDTO.getId(), estudioDTO.getNombre());
        TipoEstudio tipoEstudio = new TipoEstudio(estudioDTO.getTipoEstudio().getId(), estudioDTO
                .getTipoEstudio().getNombre());

        estudio.setTipoEstudio(tipoEstudio);
        estudio.setId(estudioDTO.getId());
        return estudio;
    }

    private SemestreDetalle creaModeloDesdeDTO(DetalleSemestreDTO semestreDetalleDTO)
    {
        Semestre semestre = new Semestre(semestreDetalleDTO.getSemestre().getId());
        TipoEstudio tipoEstudio = new TipoEstudio(semestreDetalleDTO.getTiposEstudio().getId());

        SemestreDetalle detalleModelo = new SemestreDetalle(semestreDetalleDTO.getId(), semestre,
                tipoEstudio, semestreDetalleDTO.getFechaInicio(), semestreDetalleDTO.getFechaFin(),
                semestreDetalleDTO.getNumeroSemanas());
        detalleModelo.setFechaExamenesInicio(semestreDetalleDTO.getFechaExamenesInicio());
        detalleModelo.setFechaExamenesInicioC2(semestreDetalleDTO.getFechaExamenesInicioC2());
        detalleModelo.setFechaExamenesFinC2(semestreDetalleDTO.getFechaExamenesFinC2());
        detalleModelo.setFechaExamenesFin(semestreDetalleDTO.getFechaExamenesFin());
        return detalleModelo;
    }

    public SemestreDetalleEstudio insert(SemestreDetalleEstudio semestreDetalleEstudio)
    {
        DetalleSemestreEstudioDTO detalleSemestreDTO = convierteSemestreDetalleADetalleSemestreDTO(semestreDetalleEstudio);
        detalleSemestreDTO = insert(detalleSemestreDTO);
        SemestreDetalleEstudio semestreDetalleEstudioBD = getSemestresDetalleEstudioById(detalleSemestreDTO.getId());
        return semestreDetalleEstudioBD;
    }

    public SemestreDetalleEstudio update(SemestreDetalleEstudio semestreDetalleEstudio)
    {
        DetalleSemestreEstudioDTO detalleSemestreDTO = convierteSemestreDetalleADetalleSemestreDTO(semestreDetalleEstudio);
        detalleSemestreDTO = update(detalleSemestreDTO);
        return convierteDetalleDTOEnDetalleModelo(detalleSemestreDTO);
    }

    @Override
    public void delete(Long semestreDetalleEstudioId)
    {
        delete(DetalleSemestreEstudioDTO.class, semestreDetalleEstudioId);
    }

    private DetalleSemestreEstudioDTO convierteSemestreDetalleADetalleSemestreDTO(
            SemestreDetalleEstudio detalleSemestreEstudio)
    {
        DetalleSemestreEstudioDTO detalleSemestreEstudioDTO = new DetalleSemestreEstudioDTO();

        EstudioDTO estudioDTO = new EstudioDTO();
        estudioDTO.setId(detalleSemestreEstudio.getEstudio().getId());
        detalleSemestreEstudioDTO.setEstudio(estudioDTO);

        DetalleSemestreDTO detalleSemestreDTO = new DetalleSemestreDTO();
        detalleSemestreDTO.setId(detalleSemestreEstudio.getSemestreDetalle().getId());
        detalleSemestreEstudioDTO.setDetalleSemestre(detalleSemestreDTO);

        detalleSemestreEstudioDTO.setId(detalleSemestreEstudio.getId());
        detalleSemestreEstudioDTO.setCursoAcademicoId(detalleSemestreEstudio.getCursoAcademico());
        detalleSemestreEstudioDTO.setFechaInicio(detalleSemestreEstudio.getFechaInicio());
        detalleSemestreEstudioDTO.setFechaFin(detalleSemestreEstudio.getFechaFin());
        detalleSemestreEstudioDTO.setFechaExamenesInicio(detalleSemestreEstudio
                .getFechaExamenesInicio());
        detalleSemestreEstudioDTO.setFechaExamenesFin(detalleSemestreEstudio.getFechaExamenesFin());
        detalleSemestreEstudioDTO.setFechaExamenesInicioC2(detalleSemestreEstudio
                .getFechaExamenesInicioC2());
        detalleSemestreEstudioDTO.setFechaExamenesFinC2(detalleSemestreEstudio
                .getFechaExamenesFinC2());
        detalleSemestreEstudioDTO.setNumeroSemanas(detalleSemestreEstudio.getNumeroSemanas());

        return detalleSemestreEstudioDTO;
    }

}
