package es.uji.apps.hor.dao;

import java.util.List;

import es.uji.apps.hor.model.Grupo;
import es.uji.commons.db.BaseDAO;

public interface GruposDAO extends BaseDAO
{
    List<Grupo> getGrupos(Long semestreId, Long cursoId, Long estudioId);

    List<Grupo> getGruposCircuitos(Long estudioId, Long cursoId);

    List<Grupo> getGruposAsignatura(String asignaturaId, Long semestreId);

    List<Grupo> getGruposAgrupacion(Long agrupacionId, Long semestreId);
}
