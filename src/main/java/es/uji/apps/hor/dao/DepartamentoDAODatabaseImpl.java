package es.uji.apps.hor.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.hor.db.CargoPersonaDTO;
import es.uji.apps.hor.db.CentroDTO;
import es.uji.apps.hor.db.DepartamentoDTO;
import es.uji.apps.hor.db.QCargoPersonaDTO;
import es.uji.apps.hor.db.QCentroDTO;
import es.uji.apps.hor.db.QDepartamentoDTO;
import es.uji.apps.hor.db.QProfesorDTO;
import es.uji.apps.hor.model.Centro;
import es.uji.apps.hor.model.Departamento;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class DepartamentoDAODatabaseImpl extends BaseDAODatabaseImpl implements DepartamentoDAO
{

    public static final long ACTIVO = 1L;

    @Override
    @Transactional
    public Departamento insertDepartamento(Departamento departamento)
    {
        CentroDTO centroDTO = new CentroDTO();
        centroDTO.setId(departamento.getCentro().getId());

        DepartamentoDTO departamentoDTO = new DepartamentoDTO();
        departamentoDTO.setActivo(departamento.getActivo());
        departamentoDTO.setCentro(centroDTO);

        insert(departamentoDTO);

        return creaDepartamentoDesdeDepartamentoDTO(departamentoDTO);
    }

    @Override
    public List<Departamento> getDepartamentos()
    {
        JPAQuery query = new JPAQuery(entityManager);

        QDepartamentoDTO qDepartamento = QDepartamentoDTO.departamentoDTO;
        QCentroDTO qCentro = QCentroDTO.centroDTO;

        query.from(qDepartamento).join(qDepartamento.centro, qCentro).fetch()
                .where(qDepartamento.activo.eq(ACTIVO))
                .orderBy(qCentro.nombre.asc(), qDepartamento.nombre.asc());

        List<Departamento> listaDepartamentos = new ArrayList<Departamento>();

        for (DepartamentoDTO departamentoDTO : query.list(qDepartamento))
        {
            listaDepartamentos.add(creaDepartamentoDesdeDepartamentoDTO(departamentoDTO));
        }

        return listaDepartamentos;

    }

    @Override
    public List<Departamento> getDepartamentosByPersonaId(Long connectedUserId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QCargoPersonaDTO qCargoPersonaDTO = QCargoPersonaDTO.cargoPersonaDTO;

        query.from(qCargoPersonaDTO)
                .where(qCargoPersonaDTO.persona.id.eq(connectedUserId))
                .orderBy(qCargoPersonaDTO.nombreCentro.asc(),
                        qCargoPersonaDTO.nombreDepartamento.asc());

        Map<Long, Departamento> mapDepartamentos = new HashMap<>();

        for (CargoPersonaDTO cargoPersonaDTO : query.list(qCargoPersonaDTO))
        {
            if (cargoPersonaDTO.getDepartamento() != null
                    && !mapDepartamentos.containsKey(cargoPersonaDTO.getDepartamento().getId()))
            {
                mapDepartamentos.put(cargoPersonaDTO.getDepartamento().getId(),
                        creaDepartamentoDesdeCargoPersonaDTO(cargoPersonaDTO));
            }
        }

        return new ArrayList(mapDepartamentos.values());
    }

    @Override
    public List<Departamento> getMiDepartamentoByPersonaId(Long connectedUserId)
            throws RegistroNoEncontradoException
    {
        JPAQuery query = new JPAQuery(entityManager);

        QDepartamentoDTO qDepartamentoDTO = QDepartamentoDTO.departamentoDTO;
        QProfesorDTO qProfesorDTO = QProfesorDTO.profesorDTO;

        query.from(qDepartamentoDTO, qProfesorDTO).where(
                qProfesorDTO.id.eq(connectedUserId).and(
                        qProfesorDTO.departamentoId.eq(qDepartamentoDTO.id)));

        List<DepartamentoDTO> listaDepartamentosDTO = query.list(qDepartamentoDTO);

        if (listaDepartamentosDTO.size() == 1)
        {
            List<Departamento> listaDepartamento = new ArrayList<Departamento>();
            listaDepartamento
                    .add(creaDepartamentoDesdeDepartamentoDTO(listaDepartamentosDTO.get(0)));

            return listaDepartamento;
        }
        else
        {
            throw new RegistroNoEncontradoException();
        }
    }

    private Departamento creaDepartamentoDesdeCargoPersonaDTO(CargoPersonaDTO cargoPersonaDTO)
    {
        Departamento departamento = new Departamento();
        departamento.setId(cargoPersonaDTO.getDepartamento().getId());
        departamento.setNombre(cargoPersonaDTO.getNombreDepartamento());
        departamento.setActivo(1L);

        if (cargoPersonaDTO.getCentro() != null)
        {
            Centro centro = new Centro();
            centro.setId(cargoPersonaDTO.getCentro().getId());
            centro.setNombre(cargoPersonaDTO.getNombreCentro());
            departamento.setCentro(centro);
        }

        return departamento;
    }

    private Departamento creaDepartamentoDesdeDepartamentoDTO(DepartamentoDTO departamentoDTO)
    {
        Departamento departamento = new Departamento();
        departamento.setId(departamentoDTO.getId());
        departamento.setNombre(departamentoDTO.getNombre());
        departamento.setActivo(departamentoDTO.getActivo());

        Centro centro = new Centro();
        centro.setId(departamentoDTO.getCentro().getId());
        centro.setNombre(departamentoDTO.getCentro().getNombre());
        departamento.setCentro(centro);

        return departamento;

    }
}
