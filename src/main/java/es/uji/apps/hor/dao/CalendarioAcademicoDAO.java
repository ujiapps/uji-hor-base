package es.uji.apps.hor.dao;

import java.util.Date;
import java.util.List;

import es.uji.apps.hor.model.CalendarioAcademico;
import es.uji.commons.db.BaseDAO;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

public interface CalendarioAcademicoDAO extends BaseDAO
{
    public CalendarioAcademico getCalendarioAcademicoByFecha(String tipoEstudioId, Date fecha)
            throws RegistroNoEncontradoException;

    public List<CalendarioAcademico> getCalendarioAcademicoNoLectivos(String tipoEstudioId, Date fechaInicio, Date fechaFin);

    public List<CalendarioAcademico> getCalendarioAcademicoFestivos(String tipoEstudioId, Date fechaInicio, Date fechaFin);

}
