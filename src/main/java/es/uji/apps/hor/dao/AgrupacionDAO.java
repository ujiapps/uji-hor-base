package es.uji.apps.hor.dao;

import java.util.List;

import es.uji.apps.hor.model.Agrupacion;
import es.uji.apps.hor.model.AsignaturaAgrupacion;
import es.uji.commons.db.BaseDAO;

public interface AgrupacionDAO extends BaseDAO
{
    List<Agrupacion> getAgrupacionesByEstudioId(Long estudioId);

    Agrupacion getAgrupacionCompletaByIdAndEstudioId(Long agrupacionId, Long estudioId);

    void delete(Long agrupacionId);

    void deleteAsignaturaDeAgrupacion(Long agrupacionId, String asignaturaId);

    void deleteAsignaturaDeAgrupacionConCompartidas(AsignaturaAgrupacion asignaturaAgrupacion);

    void addAsignaturaDeAgrupacion(Long agrupacionId, String asignaturaId);

    Agrupacion getAgrupacionConEstudios(Long agrupacionId);

    Agrupacion insertAgrupacion(Agrupacion agrupacion);

    void updateAgrupacion(Agrupacion agrupacion);

    Agrupacion getAgrupacionConEstudio(Long agrupacionId, Long estudioId);

    void addAsignaturaAgrupacion(AsignaturaAgrupacion asignaturaAgrupacion);

    AsignaturaAgrupacion getAsignaturaAgrupacionById(Long asignaturaAgrupacionId);

    void deleteAsignaturaAgrupacion(Long asignaturaAgrupacionId);

    boolean existeAsignaturaAgrupacion(AsignaturaAgrupacion asignaturaAgrupacion);
}