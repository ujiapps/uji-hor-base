package es.uji.apps.hor.dao;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.hor.db.*;
import es.uji.apps.hor.model.Agrupacion;
import es.uji.apps.hor.model.Asignatura;
import es.uji.apps.hor.model.AsignaturaAgrupacion;
import es.uji.apps.hor.model.Estudio;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class AgrupacionDAODatabaseImpl extends BaseDAODatabaseImpl implements AgrupacionDAO
{
    @Override
    public List<Agrupacion> getAgrupacionesByEstudioId(Long estudioId)
    {
        JPAQuery queryEstudios = new JPAQuery(entityManager);
        QAgrupacionEstudioDTO qAgrupacionEstudioDTO = QAgrupacionEstudioDTO.agrupacionEstudioDTO;

        List<AgrupacionEstudioDTO> agrupacionesEstudioDTO = queryEstudios
                .from(qAgrupacionEstudioDTO).where(qAgrupacionEstudioDTO.estudio.id.eq(estudioId))
                .list(qAgrupacionEstudioDTO);

        List<Long> agrupacionIds = new ArrayList<>();
        for (AgrupacionEstudioDTO agrupacionEstudioDTO : agrupacionesEstudioDTO)
        {
            agrupacionIds.add(agrupacionEstudioDTO.getAgrupacion().getId());
        }

        if (agrupacionIds.isEmpty())
        {
            return new ArrayList<>();
        }

        JPAQuery query = new JPAQuery(entityManager);
        QAgrupacionDTO qAgrupacionDTO = QAgrupacionDTO.agrupacionDTO;
        query.from(qAgrupacionDTO).join(qAgrupacionDTO.agrupacionesEstudio, qAgrupacionEstudioDTO)
                .fetch().where(qAgrupacionEstudioDTO.agrupacion.id.in(agrupacionIds)).distinct().orderBy(qAgrupacionDTO.nombre.asc());

        List<Agrupacion> listaAgrupaciones = new ArrayList<>();

        for (AgrupacionDTO agrupacionDTO : query.list(qAgrupacionDTO))
        {
            listaAgrupaciones.add(creaAgrupacionDesdeAgrupacionDTO(agrupacionDTO));
        }

        return listaAgrupaciones;
    }

    private Agrupacion creaAgrupacionDesdeAgrupacionDTO(AgrupacionDTO agrupacionDTO)
    {
        Agrupacion agrupacion = new Agrupacion();
        agrupacion.setId(agrupacionDTO.getId());
        agrupacion.setNombre(agrupacionDTO.getNombre());

        List<Estudio> estudios = new ArrayList<>();

        for (AgrupacionEstudioDTO agrupacionEstudioDTO : agrupacionDTO.getAgrupacionesEstudio())
        {
            Estudio estudio = new Estudio();
            estudio.setId(agrupacionEstudioDTO.getEstudio().getId());
            estudio.setNombre(agrupacionEstudioDTO.getEstudio().getNombre());
            if (agrupacionEstudioDTO.getEstudio().getNumeroCursos() != null)
            {
                estudio.setNumeroCursos(agrupacionEstudioDTO.getEstudio().getNumeroCursos()
                        .intValue());
            }
            estudios.add(estudio);
        }

        agrupacion.setEstudios(estudios);
        return agrupacion;
    }

    @Override
    public Agrupacion getAgrupacionCompletaByIdAndEstudioId(Long agrupacionId, Long estudioId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QAgrupacionDTO qAgrupacionDTO = QAgrupacionDTO.agrupacionDTO;
        QAgrupacionEstudioDTO qAgrupacionEstudioDTO = QAgrupacionEstudioDTO.agrupacionEstudioDTO;
        QEstudioDTO qEstudioDTO = QEstudioDTO.estudioDTO;

        query.from(qAgrupacionDTO)
                .join(qAgrupacionDTO.agrupacionesEstudio, qAgrupacionEstudioDTO)
                .fetch()
                .join(qAgrupacionEstudioDTO.estudio, qEstudioDTO)
                .fetch()
                .where(qAgrupacionEstudioDTO.estudio.id.eq(estudioId).and(
                        qAgrupacionDTO.id.eq(agrupacionId)));

        AgrupacionDTO agrupacionDTO = query.list(qAgrupacionDTO).get(0);

        return creaAgrupacionDesdeAgrupacionDTO(agrupacionDTO);
    }

    @Override
    @Transactional
    public void delete(Long agrupacionId)
    {
        delete(AgrupacionDTO.class, agrupacionId);
    }

    @Override
    @Transactional
    public void deleteAsignaturaDeAgrupacion(Long agrupacionId, String asignaturaId)
    {
        QAgrupacionAsignaturaDTO qAgrupacionAsignaturaDTO = QAgrupacionAsignaturaDTO.agrupacionAsignaturaDTO;
        JPADeleteClause deleteClause = new JPADeleteClause(entityManager, qAgrupacionAsignaturaDTO);

        deleteClause.where(
                qAgrupacionAsignaturaDTO.agrupacion.id
                        .eq(agrupacionId)
                        .and(qAgrupacionAsignaturaDTO.asignatura.asignaturaId.eq(asignaturaId))
                        .and(qAgrupacionAsignaturaDTO.subgrupoId.isNull().and(
                                qAgrupacionAsignaturaDTO.tipoSubgrupoId.isNull()))).execute();
    }

    @Override
    @Transactional
    public void deleteAsignaturaDeAgrupacionConCompartidas(AsignaturaAgrupacion asignaturaAgrupacion)
    {
        QAgrupacionAsignaturaDTO qAgrupacionAsignaturaDTO = QAgrupacionAsignaturaDTO.agrupacionAsignaturaDTO;
        JPADeleteClause deleteClause = new JPADeleteClause(entityManager, qAgrupacionAsignaturaDTO);

        deleteClause.where(
                qAgrupacionAsignaturaDTO.agrupacion.id
                        .eq(asignaturaAgrupacion.getAgrupacion().getId())
                        .and(qAgrupacionAsignaturaDTO.asignatura.asignaturaId
                                .eq(asignaturaAgrupacion.getAsignatura().getId()))
                        .and(qAgrupacionAsignaturaDTO.subgrupoId.eq(
                                asignaturaAgrupacion.getSubgrupoId()).and(
                                qAgrupacionAsignaturaDTO.tipoSubgrupoId.eq(asignaturaAgrupacion
                                        .getTipoSubgrupo())))).execute();
    }

    @Override
    public void addAsignaturaDeAgrupacion(Long agrupacionId, String asignaturaId)
    {
        AgrupacionAsignaturaDTO agrupacionAsignaturaDTO = new AgrupacionAsignaturaDTO();

        AgrupacionDTO agrupacionDTO = new AgrupacionDTO();
        agrupacionDTO.setId(agrupacionId);

        AsignaturaEstudioDTO asignaturaEstudioDTO = new AsignaturaEstudioDTO();
        asignaturaEstudioDTO.setAsignaturaId(asignaturaId);
        agrupacionAsignaturaDTO.setAsignatura(asignaturaEstudioDTO);
        agrupacionAsignaturaDTO.setAgrupacion(agrupacionDTO);
        insert(agrupacionAsignaturaDTO);

    }

    @Override
    public Agrupacion getAgrupacionConEstudios(Long agrupacionId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QAgrupacionDTO qAgrupacionDTO = QAgrupacionDTO.agrupacionDTO;
        QAgrupacionEstudioDTO qAgrupacionEstudioDTO = QAgrupacionEstudioDTO.agrupacionEstudioDTO;
        QEstudioDTO qEstudioDTO = QEstudioDTO.estudioDTO;

        query.from(qAgrupacionDTO).join(qAgrupacionDTO.agrupacionesEstudio, qAgrupacionEstudioDTO)
                .fetch().join(qAgrupacionEstudioDTO.estudio, qEstudioDTO).fetch()
                .where(qAgrupacionDTO.id.eq(agrupacionId));

        AgrupacionDTO agrupacionDTO = query.list(qAgrupacionDTO).get(0);

        return creaAgrupacionDesdeAgrupacionDTO(agrupacionDTO);
    }

    @Override
    public Agrupacion getAgrupacionConEstudio(Long agrupacionId, Long estudioId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QAgrupacionDTO qAgrupacionDTO = QAgrupacionDTO.agrupacionDTO;
        QAgrupacionEstudioDTO qAgrupacionEstudioDTO = QAgrupacionEstudioDTO.agrupacionEstudioDTO;
        QEstudioDTO qEstudioDTO = QEstudioDTO.estudioDTO;

        query.from(qAgrupacionDTO).join(qAgrupacionDTO.agrupacionesEstudio, qAgrupacionEstudioDTO)
                .fetch().join(qAgrupacionEstudioDTO.estudio, qEstudioDTO).fetch()
                .where(qAgrupacionDTO.id.eq(agrupacionId).and(qEstudioDTO.id.eq(estudioId)));

        AgrupacionDTO agrupacionDTO = query.list(qAgrupacionDTO).get(0);

        return creaAgrupacionDesdeAgrupacionDTO(agrupacionDTO);
    }

    @Override
    public void addAsignaturaAgrupacion(AsignaturaAgrupacion asignaturaAgrupacion)
    {
        AgrupacionAsignaturaDTO agrupacionAsignaturaDTO = creaAgrupacionAsignaturaDTODesdeAsignaturaAgrupacion(asignaturaAgrupacion);
        insert(agrupacionAsignaturaDTO);
    }

    private AgrupacionAsignaturaDTO creaAgrupacionAsignaturaDTODesdeAsignaturaAgrupacion(
            AsignaturaAgrupacion asignaturaAgrupacion)
    {
        AgrupacionAsignaturaDTO agrupacionAsignaturaDTO = new AgrupacionAsignaturaDTO();
        agrupacionAsignaturaDTO.setId(asignaturaAgrupacion.getId());
        agrupacionAsignaturaDTO.setAsignatura(new AsignaturaEstudioDTO(asignaturaAgrupacion
                .getAsignatura().getId()));
        agrupacionAsignaturaDTO.setAgrupacion(new AgrupacionDTO(asignaturaAgrupacion
                .getAgrupacion().getId()));
        agrupacionAsignaturaDTO.setSubgrupoId(asignaturaAgrupacion.getSubgrupoId());
        agrupacionAsignaturaDTO.setTipoSubgrupoId(asignaturaAgrupacion.getTipoSubgrupo());
        return agrupacionAsignaturaDTO;
    }

    @Override
    public AsignaturaAgrupacion getAsignaturaAgrupacionById(Long asignaturaAgrupacionId)
    {
        AgrupacionAsignaturaDTO agrupacionAsignaturaDTO = get(AgrupacionAsignaturaDTO.class,
                asignaturaAgrupacionId).get(0);
        return creaAsignaturaAgrupacionDesdeDTO(agrupacionAsignaturaDTO);
    }

    @Override
    public void deleteAsignaturaAgrupacion(Long asignaturaAgrupacionId)
    {
        delete(AgrupacionAsignaturaDTO.class, asignaturaAgrupacionId);
    }

    @Override
    public boolean existeAsignaturaAgrupacion(AsignaturaAgrupacion asignaturaAgrupacion)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QAgrupacionAsignaturaDTO agrupacionAsignaturaDTO = QAgrupacionAsignaturaDTO.agrupacionAsignaturaDTO;

        query.from(agrupacionAsignaturaDTO).where(
                agrupacionAsignaturaDTO.asignatura.asignaturaId.eq(
                        asignaturaAgrupacion.getAsignatura().getId()).and(
                        agrupacionAsignaturaDTO.agrupacion.id.eq(asignaturaAgrupacion
                                .getAgrupacion().getId())));

        if (asignaturaAgrupacion.getTipoSubgrupo() != null
                && !asignaturaAgrupacion.getTipoSubgrupo().isEmpty())
        {
            query.where(agrupacionAsignaturaDTO.tipoSubgrupoId.eq(asignaturaAgrupacion
                    .getTipoSubgrupo()));
        }
        else
        {
            query.where(agrupacionAsignaturaDTO.tipoSubgrupoId.isNull());
        }

        if (asignaturaAgrupacion.getSubgrupoId() != null)
        {
            query.where(agrupacionAsignaturaDTO.subgrupoId.eq(asignaturaAgrupacion.getSubgrupoId()));
        }
        else
        {
            query.where(agrupacionAsignaturaDTO.subgrupoId.isNull());
        }

        return query.list(agrupacionAsignaturaDTO).size() > 0;
    }

    private AsignaturaAgrupacion creaAsignaturaAgrupacionDesdeDTO(
            AgrupacionAsignaturaDTO agrupacionAsignaturaDTO)
    {
        AsignaturaAgrupacion asignaturaAgrupacion = new AsignaturaAgrupacion();
        asignaturaAgrupacion.setId(agrupacionAsignaturaDTO.getId());
        asignaturaAgrupacion.setTipoSubGrupo(agrupacionAsignaturaDTO.getTipoSubgrupoId());
        asignaturaAgrupacion.setSubgrupoId(agrupacionAsignaturaDTO.getSubgrupoId());
        asignaturaAgrupacion.setAsignatura(new Asignatura(agrupacionAsignaturaDTO.getAsignatura()
                .getAsignaturaId()));
        asignaturaAgrupacion.setAgrupacion(new Agrupacion(agrupacionAsignaturaDTO.getAgrupacion()
                .getId()));
        return asignaturaAgrupacion;
    }

    @Override
    public Agrupacion insertAgrupacion(Agrupacion agrupacion)
    {
        AgrupacionDTO agrupacionDTO = creaAgrupacionDTODesdeAgrupacion(agrupacion);
        agrupacionDTO = insert(agrupacionDTO);

        return creaAgrupacionDesdeAgrupacionDTO(agrupacionDTO);
    }

    private AgrupacionDTO creaAgrupacionDTODesdeAgrupacion(Agrupacion agrupacion)
    {
        AgrupacionDTO agrupacionDTO = new AgrupacionDTO();
        agrupacionDTO.setId(agrupacion.getId());
        agrupacionDTO.setNombre(agrupacion.getNombre());

        List<AgrupacionEstudioDTO> agrupacionEstudios = new ArrayList<>();

        for (Estudio estudio : agrupacion.getEstudios())
        {
            AgrupacionEstudioDTO agrupacionEstudioDTO = new AgrupacionEstudioDTO();
            EstudioDTO estudioDTO = new EstudioDTO();
            estudioDTO.setId(estudio.getId());
            estudioDTO.setNombre(estudio.getNombre());
            agrupacionEstudioDTO.setEstudio(estudioDTO);
            agrupacionEstudioDTO.setAgrupacion(agrupacionDTO);
            agrupacionEstudios.add(agrupacionEstudioDTO);
        }

        agrupacionDTO.setAgrupacionesEstudio(new HashSet(agrupacionEstudios));
        return agrupacionDTO;
    }

    @Override
    @Transactional
    public void updateAgrupacion(Agrupacion agrupacion)
    {
        limpiaEstudios(agrupacion.getId());
        AgrupacionDTO agrupacionDTO = creaAgrupacionDTODesdeAgrupacion(agrupacion);
        update(agrupacionDTO);
    }

    private void limpiaEstudios(Long agrupacionId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QAgrupacionEstudioDTO qAgrupacionEstudioDTO = QAgrupacionEstudioDTO.agrupacionEstudioDTO;

        // Borramos los estudios asignados anteriormente
        List<AgrupacionEstudioDTO> agrupacionEstudioDTOs = query.from(qAgrupacionEstudioDTO)
                .where(qAgrupacionEstudioDTO.agrupacion.id.eq(agrupacionId))
                .list(qAgrupacionEstudioDTO);

        for (AgrupacionEstudioDTO agrupacionEstudioDTO : agrupacionEstudioDTOs)
        {
            delete(agrupacionEstudioDTO);
        }
    }

}