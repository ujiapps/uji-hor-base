package es.uji.apps.hor.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.hor.db.CircuitoDTO;
import es.uji.apps.hor.db.CircuitoEstudioDTO;
import es.uji.apps.hor.db.EstudioDTO;
import es.uji.apps.hor.db.ItemCircuitoDTO;
import es.uji.apps.hor.db.QCircuitoDTO;
import es.uji.apps.hor.db.QCircuitoEstudioDTO;
import es.uji.apps.hor.db.QItemCircuitoDTO;
import es.uji.apps.hor.model.Circuito;
import es.uji.apps.hor.model.Estudio;
import es.uji.apps.hor.model.Evento;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

@Repository
public class CircuitoDAODatabaseImpl extends BaseDAODatabaseImpl implements CircuitoDAO
{

    @Override
    public List<Circuito> getCircuitosByEstudioIdAndGrupoId(Long estudioId, String grupoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        JPAQuery query2 = new JPAQuery(entityManager);
        QCircuitoDTO qCircuito = QCircuitoDTO.circuitoDTO;
        QCircuitoEstudioDTO qCircuitoEstudio = QCircuitoEstudioDTO.circuitoEstudioDTO;

        List<Long> circuitosIds = query
                .from(qCircuito)
                .innerJoin(qCircuito.circuitosEstudios, qCircuitoEstudio)
                .where(qCircuitoEstudio.estudio.id.eq(estudioId).and(qCircuito.grupoId.eq(grupoId)))
                .distinct().list(qCircuito.id);

        Map<Long, Circuito> mapaCircuitos = new HashMap<Long, Circuito>();

        if (circuitosIds.size() > 0)
        {
            query2.from(qCircuito).innerJoin(qCircuito.circuitosEstudios, qCircuitoEstudio).fetch()
                    .where(qCircuito.id.in(circuitosIds));

            for (CircuitoDTO circuitoDTO : query2.list(qCircuito))
            {
                if (!mapaCircuitos.containsKey(circuitoDTO.getId())) {
                    mapaCircuitos.put(circuitoDTO.getId(), creaCircuitoDesdeCircuitoDTO(circuitoDTO));
                }
            }
        }

        return new ArrayList(mapaCircuitos.values());
    }

    private Circuito creaCircuitoDesdeCircuitoDTO(CircuitoDTO circuitoDTO)
    {
        Circuito circuito = new Circuito();
        circuito.setId(circuitoDTO.getId());
        circuito.setNombre(circuitoDTO.getNombre());
        circuito.setGrupo(circuitoDTO.getGrupoId());
        circuito.setPlazas(circuitoDTO.getPlazas());

        List<Estudio> listaEstudios = new ArrayList<Estudio>();
        for (CircuitoEstudioDTO circuitoEstudioDTO : circuitoDTO.getCircuitosEstudios())
        {
            Estudio estudio = new Estudio();
            estudio.setId(circuitoEstudioDTO.getEstudio().getId());
            listaEstudios.add(estudio);
        }
        circuito.setEstudios(listaEstudios);

        return circuito;
    }

    @Override
    @Transactional
    public void deleteCircuitoById(Long circuitoId) throws RegistroConHijosException
    {
        delete(CircuitoDTO.class, circuitoId);
    }

    @Override
    public Circuito getCircuitoById(Long circuitoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCircuitoDTO qCircuito = QCircuitoDTO.circuitoDTO;
        QCircuitoEstudioDTO qCircuitoEstudio = QCircuitoEstudioDTO.circuitoEstudioDTO;

        query.from(qCircuito).innerJoin(qCircuito.circuitosEstudios, qCircuitoEstudio).fetch()
                .where(qCircuito.id.eq(circuitoId));

        List<CircuitoDTO> listaCircuitos = query.list(qCircuito);

        if (listaCircuitos.size() > 0)
        {
            return creaCircuitoDesdeCircuitoDTO(listaCircuitos.get(0));
        }
        else
        {
            return new Circuito();
        }
    }

    @Override
    public Circuito insertNuevoCircuito(Circuito circuito)
    {
        CircuitoDTO circuitoDTO = new CircuitoDTO();
        circuitoDTO.setId(circuito.getId());
        circuitoDTO.setNombre(circuito.getNombre());
        circuitoDTO.setPlazas(circuito.getPlazas());
        circuitoDTO.setGrupoId(circuito.getGrupo());

        circuitoDTO = insert(circuitoDTO);

        List<CircuitoEstudioDTO> listaCircuitoEstudiosDTO = new ArrayList<CircuitoEstudioDTO>();
        for (Estudio estudio : circuito.getEstudios())
        {
            CircuitoEstudioDTO circuitoEstudioDTO = new CircuitoEstudioDTO();
            EstudioDTO estudioDTO = new EstudioDTO();
            estudioDTO.setId(estudio.getId());

            circuitoEstudioDTO.setCircuito(circuitoDTO);
            circuitoEstudioDTO.setEstudio(estudioDTO);

            listaCircuitoEstudiosDTO.add(circuitoEstudioDTO);
            insert(circuitoEstudioDTO);
        }

        circuitoDTO.setCircuitosEstudios(new HashSet<CircuitoEstudioDTO>(listaCircuitoEstudiosDTO));
        return creaCircuitoDesdeCircuitoDTO(circuitoDTO);
    }

    @Override
    @Transactional
    public void borraCircuitoDeEstudio(Long circuitoId, Long estudioId)
            throws RegistroNoEncontradoException, RegistroConHijosException
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCircuitoEstudioDTO qCircuitoEstudio = QCircuitoEstudioDTO.circuitoEstudioDTO;
        query.from(qCircuitoEstudio).where(
                qCircuitoEstudio.estudio.id.eq(estudioId).and(
                        qCircuitoEstudio.circuito.id.eq(circuitoId)));

        List<CircuitoEstudioDTO> resultado = query.list(qCircuitoEstudio);

        if (resultado.size() > 0)
        {
            CircuitoEstudioDTO circuitoEstudioDTO = resultado.get(0);

            try
            {
                delete(CircuitoEstudioDTO.class, circuitoEstudioDTO.getId());
            }
            catch (DataIntegrityViolationException e)
            {
                throw new RegistroConHijosException(
                        "No es pot borrar el circuit perque té events assignats");
            }
        }
    }

    @Override
    public Circuito getCircuitoCompletoById(Long circuitoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCircuitoDTO qCircuito = QCircuitoDTO.circuitoDTO;
        QCircuitoEstudioDTO qCircuitoEstudio = QCircuitoEstudioDTO.circuitoEstudioDTO;
        QItemCircuitoDTO qItemCircuitoDTO = QItemCircuitoDTO.itemCircuitoDTO;

        query.from(qCircuito).innerJoin(qCircuito.circuitosEstudios, qCircuitoEstudio).fetch()
                .where(qCircuito.id.eq(circuitoId));

        List<CircuitoDTO> listaCircuitos = query.list(qCircuito);

        if (listaCircuitos.size() > 0)
        {
            JPAQuery query2 = new JPAQuery(entityManager);
            CircuitoDTO circuitoDTO = listaCircuitos.get(0);
            query2.from(qItemCircuitoDTO).where(qItemCircuitoDTO.circuito.id.eq(circuitoId));
            List<ItemCircuitoDTO> itemCircuitoDTO = query2.list(qItemCircuitoDTO);
            return creaCircuitoConEventosDesdeCircuitoDTO(circuitoDTO, itemCircuitoDTO);
        }
        else
        {
            return new Circuito();
        }
    }

    private Circuito creaCircuitoConEventosDesdeCircuitoDTO(CircuitoDTO circuitoDTO,
            List<ItemCircuitoDTO> listaItemsCircuito)
    {
        Circuito circuito = creaCircuitoDesdeCircuitoDTO(circuitoDTO);

        List<Evento> listaEventos = new ArrayList<Evento>();
        for (ItemCircuitoDTO itemCircuitoDTO : listaItemsCircuito)
        {
            Evento evento = new Evento();
            evento.setId(itemCircuitoDTO.getItem().getId());
            listaEventos.add(evento);
        }

        circuito.setEventos(listaEventos);

        return circuito;
    }

    @Override
    public Circuito insertNuevoCircuitoEnEstudio(Long circuitoId, Long estudioId)
    {
        CircuitoDTO circuitoDTO = new CircuitoDTO();
        circuitoDTO.setId(circuitoId);

        EstudioDTO estudioDTO = new EstudioDTO();
        estudioDTO.setId(estudioId);

        CircuitoEstudioDTO circuitoEstudioDTO = new CircuitoEstudioDTO();
        circuitoEstudioDTO.setCircuito(circuitoDTO);
        circuitoEstudioDTO.setEstudio(estudioDTO);
        insert(circuitoEstudioDTO);

        return getCircuitoById(circuitoId);
    }

    @Override
    @Transactional
    public void updateCircuito(Circuito circuito)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCircuitoEstudioDTO qCircuitoEstudio = QCircuitoEstudioDTO.circuitoEstudioDTO;

        CircuitoDTO circuitoDTO = new CircuitoDTO();
        circuitoDTO.setId(circuito.getId());
        circuitoDTO.setNombre(circuito.getNombre());
        circuitoDTO.setPlazas(circuito.getPlazas());
        circuitoDTO.setGrupoId(circuito.getGrupo());
        this.update(circuitoDTO);

        // Borramos los estudios asignados anteriormente
        List<CircuitoEstudioDTO> circuitosEstudios = query.from(qCircuitoEstudio)
                .where(qCircuitoEstudio.circuito.id.eq(circuito.getId())).list(qCircuitoEstudio);

        for (CircuitoEstudioDTO circuitoEstudioDTO : circuitosEstudios)
        {
            delete(CircuitoEstudioDTO.class, circuitoEstudioDTO.getId());
        }

        // E insertamos los nuevos

        List<CircuitoEstudioDTO> listaCircuitoEstudiosDTO = new ArrayList<CircuitoEstudioDTO>();
        for (Estudio estudio : circuito.getEstudios())
        {
            CircuitoEstudioDTO circuitoEstudioDTO = new CircuitoEstudioDTO();
            EstudioDTO estudioDTO = new EstudioDTO();
            estudioDTO.setId(estudio.getId());

            circuitoEstudioDTO.setCircuito(circuitoDTO);
            circuitoEstudioDTO.setEstudio(estudioDTO);

            listaCircuitoEstudiosDTO.add(circuitoEstudioDTO);
            insert(circuitoEstudioDTO);
        }
    }

}
