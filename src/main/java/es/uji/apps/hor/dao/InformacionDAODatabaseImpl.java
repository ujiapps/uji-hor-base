package es.uji.apps.hor.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.hor.db.InformacionDTO;
import es.uji.apps.hor.db.QInformacionDTO;
import es.uji.apps.hor.model.Informacion;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class InformacionDAODatabaseImpl extends BaseDAODatabaseImpl implements InformacionDAO
{
    @Override
    public List<Informacion> getInformacion(Long connectedUserId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QInformacionDTO qInformacion = QInformacionDTO.informacionDTO;
        query.from(qInformacion)
                .where(qInformacion.fechaInicio.currentDate().between(qInformacion.fechaInicio,
                        qInformacion.fechaFin)).orderBy(qInformacion.orden.asc());

        List<Informacion> listaInformacion = new ArrayList<Informacion>();
        for (InformacionDTO informacionDTO : query.list(qInformacion))
        {
            listaInformacion.add(convierteInformacionDTOAInformacion(informacionDTO));
        }
        return listaInformacion;
    }


    private Informacion convierteInformacionDTOAInformacion(InformacionDTO informacionDTO)
    {
        Informacion informacion = new Informacion();
        informacion.setId(informacionDTO.getId());
        informacion.setTexto(informacionDTO.getTexto());
        informacion.setOrden(informacionDTO.getOrden());
        informacion.setFechaFin(informacionDTO.getFechaFin());
        informacion.setFechaInicio(informacionDTO.getFechaInicio());
        return informacion;
    }
}
