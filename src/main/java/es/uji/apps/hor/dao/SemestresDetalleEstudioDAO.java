package es.uji.apps.hor.dao;

import java.util.List;

import es.uji.apps.hor.model.SemestreDetalle;
import es.uji.apps.hor.model.SemestreDetalleEstudio;
import es.uji.commons.db.BaseDAO;

public interface SemestresDetalleEstudioDAO extends BaseDAO
{
    List<SemestreDetalleEstudio> getSemestresDetalleEstudios();

    SemestreDetalleEstudio getSemestresDetalleEstudioById(Long semestreDetalleEstudioId);

    SemestreDetalleEstudio insert(SemestreDetalleEstudio semestreDetalleEstudio);

    SemestreDetalleEstudio update(SemestreDetalleEstudio semestreDetalleEstudio);

    void delete(Long semestreDetalleEstudioId);
}
