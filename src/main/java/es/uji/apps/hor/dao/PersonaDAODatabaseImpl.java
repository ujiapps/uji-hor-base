package es.uji.apps.hor.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.hor.db.*;
import es.uji.apps.hor.model.*;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.json.lookup.LookupItem;
import es.uji.commons.rest.StringUtils;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.dao.ApaDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

@Repository
public class PersonaDAODatabaseImpl extends BaseDAODatabaseImpl implements PersonaDAO
{
    @Override
    @Transactional
    public Persona insertaPersonasYCargos(Persona persona)
    {
        PersonaDTO personaDTO = new PersonaDTO();
        personaDTO.setId(persona.getId());

        DepartamentoDTO departamentoDTO = new DepartamentoDTO();
        departamentoDTO.setId(persona.getDepartamento().getId());

        personaDTO.setNombre(persona.getNombre());
        personaDTO.setNombreBuscar(persona.getNombreBuscar());
        personaDTO.setEmail(persona.getEmail());
        personaDTO.setActividadId(persona.getActividadId());
        personaDTO.setCargosPersona(new HashSet<CargoPersonaDTO>());

        CentroDTO centroDTO = new CentroDTO();
        centroDTO.setId(persona.getCentroAutorizado().getId());

        if (persona.getEstudiosAutorizados().size() > 0)
        {
            for (Estudio estudio : persona.getEstudiosAutorizados())
            {
                EstudioDTO estudioDTO = new EstudioDTO();
                estudioDTO.setId(estudio.getId());

                CargoPersonaDTO cargoPersonaDTO = new CargoPersonaDTO();
                cargoPersonaDTO.setPersona(personaDTO);
                cargoPersonaDTO.setEstudio(estudioDTO);
                cargoPersonaDTO.setCentro(centroDTO);

                personaDTO.getCargosPersona().add(cargoPersonaDTO);
            }
        }
        else
        {
            CargoPersonaDTO cargoPersonaDTO = new CargoPersonaDTO();
            cargoPersonaDTO.setPersona(personaDTO);
            cargoPersonaDTO.setCentro(centroDTO);

            personaDTO.getCargosPersona().add(cargoPersonaDTO);
        }

        update(personaDTO);

        return persona;
    }

    @Override
    public List<Cargo> getCargosByPersonaId(Long connectedUserId) {
        JPAQuery query = new JPAQuery(entityManager);

        QCargoPersonaDTO qCargoPersonaDTO = QCargoPersonaDTO.cargoPersonaDTO;

        query.from(qCargoPersonaDTO).where(qCargoPersonaDTO.persona.id.eq(connectedUserId));

        List<CargoPersonaDTO> listaCargoPersona = query.list(qCargoPersonaDTO);

        List<Cargo> listaCargos = new ArrayList<>();
        for (CargoPersonaDTO cargoPersonaDTO: listaCargoPersona) {
            Cargo cargo = new Cargo();
            cargo.setNombre(cargoPersonaDTO.getNombreCargo());
            cargo.setId(cargoPersonaDTO.getCargo().getId());
            listaCargos.add(cargo);

        }

        return listaCargos;
    }

    @Override
    public Persona getPersonaConTitulacionesYCentrosById(Long personaId)
            throws RegistroNoEncontradoException
    {
        Persona persona = null;

        JPAQuery query = new JPAQuery(entityManager);

        QCargoPersonaDTO qCargoPersonaDTO = QCargoPersonaDTO.cargoPersonaDTO;

        query.from(qCargoPersonaDTO).where(qCargoPersonaDTO.persona.id.eq(personaId));

        List<CargoPersonaDTO> listaCargoPersona = query.list(qCargoPersonaDTO);

        if (listaCargoPersona.size() == 0)
        {
            throw new RegistroNoEncontradoException();
        }

        persona = new Persona();
        persona.setId(listaCargoPersona.get(0).getId());
        persona.setNombre(listaCargoPersona.get(0).getNombre());
        persona.setCentroAutorizado(creaCentroDesdeCargoDTO(listaCargoPersona.get(0)));

        Map<Long, TipoCargoDTO> mapTipoCargos = new HashMap<Long, TipoCargoDTO>();
        Map<Long, EstudioDTO> mapEstudios = new HashMap<Long, EstudioDTO>();
        Map<Long, DepartamentoDTO> mapDepartamentos = new HashMap<Long, DepartamentoDTO>();
        Map<Long, AreaDTO> mapAreas = new HashMap<Long, AreaDTO>();

        for (CargoPersonaDTO cargoPersona : listaCargoPersona)
        {
            if (cargoPersona.getEstudio() != null && !mapEstudios.keySet().contains(cargoPersona.getEstudio().getId()))
            {
                mapEstudios.put(cargoPersona.getEstudio().getId(), cargoPersona.getEstudio());
            }

            if (!mapTipoCargos.values().contains(cargoPersona.getNombreCargo()))
            {
                mapTipoCargos
                        .put(cargoPersona.getCargo().getId(), cargoPersona.getCargo());
            }

            if (cargoPersona.getDepartamento() != null && !mapDepartamentos.keySet().contains(cargoPersona.getDepartamento().getId()))
            {
                mapDepartamentos.put(cargoPersona.getDepartamento().getId(), cargoPersona.getDepartamento());
            }

            if (cargoPersona.getArea() != null && !mapAreas.keySet().contains(cargoPersona.getArea().getId()))
            {
                mapAreas.put(cargoPersona.getArea().getId(), cargoPersona.getArea());
            }

        }

        for (EstudioDTO estudioDTO : mapEstudios.values())
        {
            Estudio estudio = new Estudio();
            estudio.setNombre(estudioDTO.getNombre());
            estudio.setId(estudioDTO.getId());
            persona.getEstudiosAutorizados().add(estudio);
        }

        for (TipoCargoDTO tipoCargoDTO : mapTipoCargos.values())
        {
            Cargo cargo = new Cargo();
            cargo.setId(tipoCargoDTO.getId());
            cargo.setNombre(tipoCargoDTO.getNombre());
            cargo.setDepartamento(tipoCargoDTO.getDepartamento());
            cargo.setEstudio(tipoCargoDTO.getEstudio());
            cargo.setCentro(tipoCargoDTO.getCentro());
            persona.getCargos().add(cargo);
        }

        for (DepartamentoDTO departamentoDTO : mapDepartamentos.values())
        {
            Departamento departamento = new Departamento();
            departamento.setNombre(departamentoDTO.getNombre());
            departamento.setId(departamentoDTO.getId());
            persona.getDepartamentosAutorizados().add(departamento);
        }

        for (AreaDTO areaDTO : mapAreas.values())
        {
            Area area = new Area();
            area.setNombre(areaDTO.getNombre());
            area.setId(areaDTO.getId());
            persona.getAreasAutorizadas().add(area);
        }

        return persona;
    }

    private Estudio creaEstudioDeCargoDTO(CargoPersonaDTO cargoDTO)
    {
        Estudio estudioNuevo = new Estudio();
        estudioNuevo.setId(cargoDTO.getEstudio().getId());
        estudioNuevo.setNombre(cargoDTO.getEstudio().getNombre());
        return estudioNuevo;
    }

    private Centro creaCentroDesdeCargoDTO(CargoPersonaDTO cargoDTO)
    {
        Centro centroNuevo = new Centro();
        centroNuevo.setId(cargoDTO.getCentro().getId());
        centroNuevo.setNombre(cargoDTO.getCentro().getNombre());
        return centroNuevo;
    }

    @Autowired
    private ApaDAO apaDAO;

    @Override
    public boolean esAdmin(Long personaId)
    {
        return apaDAO.hasPerfil("HOR", "ADMIN", personaId);
    }

    @Override
    public List<LookupItem> search(String cadena)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QPersonaDTO qPersona = QPersonaDTO.personaDTO;

        List<LookupItem> result = new ArrayList<LookupItem>();
        if (cadena != null && !cadena.isEmpty())
        {
            String cadenaLimpia = StringUtils.limpiaAcentos(cadena);

            query.from(qPersona).where(
                    qPersona.nombreBuscar.lower().like("%" + cadenaLimpia.toLowerCase() + "%"));
            List<PersonaDTO> listaPersonas = query.list(qPersona);
            for (PersonaDTO personaDTO : listaPersonas)
            {
                LookupItem lookupItem = new LookupItem();
                lookupItem.setId(String.valueOf(personaDTO.getId()));
                lookupItem.setNombre(personaDTO.getNombre());
                result.add(lookupItem);
            }
        }
        return result;
    }

    @Override
    public List<Cargo> getTodosLosCargos()
    {
        JPAQuery query = new JPAQuery(entityManager);
        QTipoCargoDTO qTipoCargo = QTipoCargoDTO.tipoCargoDTO;

        query.from(qTipoCargo);

        return convierteCargoDTOaCargo(query.list(qTipoCargo));
    }

    private List<Cargo> convierteCargoDTOaCargo(List<TipoCargoDTO> listaCargosDTO)
    {
        List<Cargo> listaCargos = new ArrayList<Cargo>();

        for (TipoCargoDTO cargoDTO : listaCargosDTO)
        {
            Cargo cargo = new Cargo();
            cargo.setId(cargoDTO.getId());
            cargo.setNombre(cargoDTO.getNombre());
            cargo.setEstudio(cargoDTO.getEstudio());
            cargo.setDepartamento(cargoDTO.getDepartamento());
            cargo.setCentro(cargoDTO.getCentro());
            listaCargos.add(cargo);
        }

        return listaCargos;
    }

    @Override
    public boolean isAulaAutorizada(Long aulaId, Long personaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QAulaPersonaDTO qAulaPersona = QAulaPersonaDTO.aulaPersonaDTO;

        List<AulaPersonaDTO> listaAulasDTO = query.from(qAulaPersona)
                .where(qAulaPersona.personaId.eq(personaId).and(qAulaPersona.aulaId.eq(aulaId)))
                .distinct().list(qAulaPersona);

        if (listaAulasDTO.size() > 0)
        {
            return true;
        }

        return false;
    }
}
