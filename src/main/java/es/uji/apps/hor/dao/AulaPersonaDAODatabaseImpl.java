package es.uji.apps.hor.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.hor.db.AulaPersonaDTO;
import es.uji.apps.hor.model.Aula;
import es.uji.apps.hor.model.Centro;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class AulaPersonaDAODatabaseImpl extends BaseDAODatabaseImpl implements AulaPersonaDAO
{
    @Override
    @Transactional
    public Aula insertAulaPersona(Long personaId, Aula aula, Centro centro) {
        
        AulaPersonaDTO aulaPersonaDTO = new AulaPersonaDTO();

        aulaPersonaDTO.setCentroId(centro.getId());
        aulaPersonaDTO.setCentro(centro.getNombre());
        aulaPersonaDTO.setPersonaId(personaId);
        aulaPersonaDTO.setAulaId(aula.getId());
        aulaPersonaDTO.setCodigo(aula.getCodigo());
        aulaPersonaDTO.setTipo(aula.getTipo().getNombre());
        aulaPersonaDTO.setNombre(aula.getNombre());

        insert(aulaPersonaDTO);

        return aula;
    }
}
