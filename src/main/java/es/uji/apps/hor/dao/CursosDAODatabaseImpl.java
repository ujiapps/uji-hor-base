package es.uji.apps.hor.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.hor.db.CursoDTO;
import es.uji.apps.hor.db.QCursoDTO;
import es.uji.apps.hor.model.Curso;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class CursosDAODatabaseImpl extends BaseDAODatabaseImpl implements CursosDAO
{
    @Override
    public List<Curso> getCursos(Long estudioId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCursoDTO qCurso = QCursoDTO.cursoDTO;

        query.from(qCurso).where(qCurso.estudioId.eq(estudioId)).orderBy(qCurso.cursoId.asc());

        List<CursoDTO> cursosDTO = query.list(qCurso);
        return creaCursosDesdeCursosDTO(cursosDTO);
    }

    private List<Curso> creaCursosDesdeCursosDTO(List<CursoDTO> cursosDTO)
    {
        List<Curso> listaCursos = new ArrayList<Curso>();
        for (CursoDTO cursoDTO : cursosDTO)
        {
            Curso curso = new Curso(cursoDTO.getCursoId());
            listaCursos.add(curso);
        }
        return listaCursos;
    }
}