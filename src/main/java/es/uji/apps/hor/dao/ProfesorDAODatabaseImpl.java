package es.uji.apps.hor.dao;

import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import com.mysema.query.types.QTuple;

import es.uji.apps.hor.EventoYaAsignado;
import es.uji.apps.hor.db.*;
import es.uji.apps.hor.model.Area;
import es.uji.apps.hor.model.Evento;
import es.uji.apps.hor.model.Profesor;
import es.uji.apps.hor.model.ProfesorDetalle;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

import org.hibernate.JDBCException;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.sql.SQLIntegrityConstraintViolationException;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class ProfesorDAODatabaseImpl extends BaseDAODatabaseImpl implements ProfesorDAO
{
    public static final long TRUE = 1L;

    @Override
    public List<Profesor> getProfesoresByAreaId(Long areaId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QProfesorDTO qProfesor = QProfesorDTO.profesorDTO;
        QAreaDTO qArea = QAreaDTO.areaDTO;

        query.from(qProfesor).join(qProfesor.area, qArea).fetch()
                .where(qProfesor.area.id.eq(areaId).and(qArea.activa.eq(TRUE)))
                .orderBy(qProfesor.nombre.asc());

        List<Profesor> listaProfesores = new ArrayList<Profesor>();

        for (ProfesorDTO areaDTO : query.list(qProfesor))
        {
            listaProfesores.add(creaProfesorDesdeProfesorDTO(areaDTO));
        }

        return listaProfesores;
    }

    @Override
    public List<Profesor> getProfesoresByAreaIdAndPersonaId(Long areaId, Long connectedUserId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QProfesorDTO qProfesor = QProfesorDTO.profesorDTO;
        QAreaDTO qArea = QAreaDTO.areaDTO;
        QDepartamentoDTO qDepartamento = QDepartamentoDTO.departamentoDTO;

        query.from(qProfesor).join(qProfesor.area, qArea).fetch()
                .join(qArea.departamento, qDepartamento)
                .where(qProfesor.area.id.eq(areaId).and(qProfesor.id.eq(connectedUserId))
                        .and(qArea.activa.eq(TRUE)).and(qDepartamento.activo.eq(TRUE)))
                .orderBy(qProfesor.nombre.asc());

        List<Profesor> listaProfesors = new ArrayList<Profesor>();

        for (ProfesorDTO areaDTO : query.list(qProfesor))
        {
            listaProfesors.add(creaProfesorDesdeProfesorDTO(areaDTO));
        }

        return listaProfesors;
    }

    @Override
    @Transactional
    public void asociaClase(Long itemId, Long profesorId)
    {
        ItemProfesorDTO itemProfesorDTO = new ItemProfesorDTO();
        ProfesorDTO profesorDTO = new ProfesorDTO();
        profesorDTO.setId(profesorId);

        ItemDTO itemDTO = new ItemDTO();
        itemDTO.setId(itemId);

        itemProfesorDTO.setProfesor(profesorDTO);
        itemProfesorDTO.setItem(itemDTO);
        itemProfesorDTO.setDetalleManual(false);

        insert(itemProfesorDTO);
    }

    @Override
    @Transactional
    public void desasociaClase(Long itemId, Long profesorId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QItemProfesorDTO qItemProfesorDTO = QItemProfesorDTO.itemProfesorDTO;

        query.from(qItemProfesorDTO).where(qItemProfesorDTO.profesor.id.eq(profesorId)
                .and(qItemProfesorDTO.item.id.eq(itemId)));

        for (ItemProfesorDTO itemProfesorDTO : query.list(qItemProfesorDTO))
        {
            delete(itemProfesorDTO);
        }
    }

    private Profesor creaProfesorDesdeProfesorDTO(ProfesorDTO profesorDTO)
    {
        Profesor profesor = new Profesor();
        profesor.setId(profesorDTO.getId());
        profesor.setNombre(profesorDTO.getNombre());
        profesor.setEmail(profesorDTO.getEmail());
        profesor.setPendienteContratacion(profesorDTO.getPendienteContratacion() == TRUE);
        profesor.setCreditos(profesorDTO.getCreditos());
        profesor.setCreditosMaximos(profesorDTO.getCreditosMaximos());
        profesor.setCreditosReduccion(profesorDTO.getCreditosReduccion());
        profesor.setCategoriaId(profesorDTO.getCategoriaId());
        profesor.setNombreCategoria(profesorDTO.getNombreCategoria());

        Area area = new Area();
        area.setId(profesorDTO.getArea().getId());
        area.setNombre(profesorDTO.getArea().getNombre());
        area.setActiva(profesorDTO.getArea().getActiva());

        return profesor;
    }

    @Override
    @Transactional
    public void updateDetalleManualByItemsId(List<Long> itemsId, Long profesorId,
                                             Boolean detalleManual)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QItemProfesorDTO qItemProfesorDTO = QItemProfesorDTO.itemProfesorDTO;

        List<ItemProfesorDTO> itemsProfesor = query.from(qItemProfesorDTO)
                .where(qItemProfesorDTO.item.id.in(itemsId)
                        .and(qItemProfesorDTO.profesor.id.eq(profesorId)))
                .list(qItemProfesorDTO);

        for (ItemProfesorDTO itemProfesor : itemsProfesor)
        {
            itemProfesor.setDetalleManual(detalleManual);
            update(itemProfesor);
        }
    }

    @Override
    @Transactional
    public void updateCreditosByItemsId(List<Long> itemsId, Long profesorId, BigDecimal creditos,
                                        BigDecimal creditosComputables)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QItemProfesorDTO qItemProfesorDTO = QItemProfesorDTO.itemProfesorDTO;

        List<ItemProfesorDTO> itemsProfesor = query.from(qItemProfesorDTO)
                .where(qItemProfesorDTO.item.id.in(itemsId)
                        .and(qItemProfesorDTO.profesor.id.eq(profesorId)))
                .list(qItemProfesorDTO);

        for (ItemProfesorDTO itemProfesor : itemsProfesor)
        {
            itemProfesor.setCreditos(creditos);
            itemProfesor.setCreditosComputables(creditosComputables);
            update(itemProfesor);
        }
    }

    @Override
    public void insertaItemDetallesProfesor(HashMap<Long, List> fechasSeleccionadasPorEvento)
            throws EventoYaAsignado
    {
        for (Map.Entry<Long, List> entry : fechasSeleccionadasPorEvento.entrySet())
        {
            Long itemProfesorId = entry.getKey();
            List<Long> itemsDetalleId = entry.getValue();

            ItemProfesorDTO itemProfesorDTO = new ItemProfesorDTO();
            itemProfesorDTO.setId(itemProfesorId);

            for (Long itemDetalleId : itemsDetalleId)
            {
                ItemDetalleProfesorDTO itemDetalleProfesor = new ItemDetalleProfesorDTO();
                itemDetalleProfesor.setItemProfesor(itemProfesorDTO);

                ItemDetalleDTO itemDetalle = new ItemDetalleDTO();
                itemDetalle.setId(itemDetalleId);
                itemDetalleProfesor.setItemDetalle(itemDetalle);

                try
                {
                    insert(itemDetalleProfesor);
                }
                catch (Exception e)
                {
                    String mensajeError = getMensajeFechaYaAsignada(itemDetalleId, itemProfesorId);
                    throw new EventoYaAsignado(mensajeError);
                }
            }
        }
    }

    private String getMensajeFechaYaAsignada(Long itemDetalleId, Long itemProfesorId)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        ItemDetalleProfesorDTO itemDetalleProfesorDTO = getItemDetalleProfesorDTOByDetalleId(
                itemDetalleId, itemProfesorId);
        String fecha = dateFormat.format(itemDetalleProfesorDTO.getItemDetalle().getInicio());
        return MessageFormat.format("La classe del dia {0} ja està assignada al professor {1}",
                fecha, itemDetalleProfesorDTO.getItemProfesor().getProfesor().getNombre());
    }

    private ItemDetalleProfesorDTO getItemDetalleProfesorDTOByDetalleId(Long detalleId,
                                                                        Long itemProfesorId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QProfesorDTO qProfesorDTO = QProfesorDTO.profesorDTO;
        QItemDetalleProfesorDTO qItemDetalleProfesorDTO = QItemDetalleProfesorDTO.itemDetalleProfesorDTO;
        QItemProfesorDTO qItemProfesorDTO = QItemProfesorDTO.itemProfesorDTO;
        QItemDetalleDTO qItemDetalleDTO = QItemDetalleDTO.itemDetalleDTO;

        List<ItemDetalleProfesorDTO> itemsDetalleProfesorDTO = query.from(qItemDetalleProfesorDTO)
                .join(qItemDetalleProfesorDTO.itemDetalle, qItemDetalleDTO).fetch()
                .join(qItemDetalleProfesorDTO.itemProfesor, qItemProfesorDTO).fetch()
                .join(qItemProfesorDTO.profesor, qProfesorDTO).fetch()
                .where(qItemDetalleProfesorDTO.itemDetalle.id.eq(detalleId)
                        .and(qItemDetalleProfesorDTO.itemProfesor.id.ne(itemProfesorId)))
                .distinct().list(qItemDetalleProfesorDTO);

        if (itemsDetalleProfesorDTO.size() == 0)
        {
            return null;
        }

        return itemsDetalleProfesorDTO.get(0);
    }

    @Override
    @Transactional
    public void borraItemDetallesProfesorByEventosId(List<Long> eventoIds, Long profesorId)
    {
        if (eventoIds.isEmpty())
        {
            return;
        }

        JPAQuery query = new JPAQuery(entityManager);
        QItemDetalleProfesorDTO qItemDetalleProfesorDTO = QItemDetalleProfesorDTO.itemDetalleProfesorDTO;
        QItemProfesorDTO qItemProfesorDTO = QItemProfesorDTO.itemProfesorDTO;

        List<ItemDetalleProfesorDTO> itemsDetalleProfesorDTO = query.from(qItemDetalleProfesorDTO)
                .join(qItemDetalleProfesorDTO.itemProfesor, qItemProfesorDTO)
                .where(qItemProfesorDTO.item.id.in(eventoIds)
                        .and(qItemProfesorDTO.profesor.id.eq(profesorId)))
                .list(qItemDetalleProfesorDTO);

        for (ItemDetalleProfesorDTO itemDetalleProfesor : itemsDetalleProfesorDTO)
        {
            delete(ItemDetalleProfesorDTO.class, itemDetalleProfesor.getId());
        }
    }

    @Override
    public ProfesorDetalle getProfesorDetalleByItemIdYProfesorId(Long itemId, Long profesorId)
            throws RegistroNoEncontradoException
    {
        JPAQuery query = new JPAQuery(entityManager);
        QItemProfesorDTO qItemProfesor = QItemProfesorDTO.itemProfesorDTO;

        List<ItemProfesorDTO> itemProfesores = query.from(qItemProfesor).where(
                qItemProfesor.item.id.eq(itemId).and(qItemProfesor.profesor.id.eq(profesorId)))
                .list(qItemProfesor);

        if (itemProfesores.size() == 0)
        {
            throw new RegistroNoEncontradoException();
        }

        return creaProfesorDetalleDesde(itemProfesores.get(0));
    }

    @Override
    @Transactional
    public void updateCreditosEventoByProfesorId(Long itemId, Long profesorId, BigDecimal creditos,
                                                 BigDecimal creditosComputables)
    {
        QItemProfesorDTO qItemProfesorDTO = QItemProfesorDTO.itemProfesorDTO;
        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, qItemProfesorDTO);

        updateClause
                .where(qItemProfesorDTO.item.id.eq(itemId)
                        .and(qItemProfesorDTO.profesor.id.eq(profesorId)))
                .set(qItemProfesorDTO.creditos, creditos)
                .set(qItemProfesorDTO.creditosComputables, creditosComputables).execute();
    }

    @Override
    public List<Profesor> getProfesoresYCreditosRestantesByAreaIdAndPersonaId(Long areaId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QProfesorCreditosDTO qProfesorCreditosDTO = QProfesorCreditosDTO.profesorCreditosDTO;
        QProfesorDTO qProfesorDTO = QProfesorDTO.profesorDTO;

        query.from(qProfesorDTO, qProfesorCreditosDTO)
                .where(qProfesorDTO.id.eq(qProfesorCreditosDTO.id)
                        .and(qProfesorCreditosDTO.areaId.eq(areaId)))
                .orderBy(qProfesorCreditosDTO.nombre.asc());

        List<Profesor> listaProfesores = new ArrayList<Profesor>();

        for (Tuple tuplaProfesor : query.list(new QTuple(qProfesorDTO.id, qProfesorDTO.nombre,
                qProfesorDTO.nombreCategoria, qProfesorDTO.area, qProfesorDTO.categoriaId,
                qProfesorDTO.creditosMaximos, qProfesorDTO.email, qProfesorDTO.creditos,
                qProfesorDTO.creditosReduccion, qProfesorCreditosDTO.creditosPendientes,
                qProfesorCreditosDTO.creditosAsignados, qProfesorCreditosDTO.creditosComputables)))
        {
            listaProfesores.add(creaProfesorDesdeTupla(tuplaProfesor));
        }

        return listaProfesores;
    }

    @Override
    public Profesor getProfesorById(Long connectedUserId) throws RegistroNoEncontradoException
    {

        JPAQuery query = new JPAQuery(entityManager);

        QProfesorDTO qProfesorDTO = QProfesorDTO.profesorDTO;

        query.from(qProfesorDTO).where(qProfesorDTO.id.eq(connectedUserId));

        List<ProfesorDTO> listaProfesorDTO = query.list(qProfesorDTO);

        if (listaProfesorDTO.size() == 1)
        {
            return creaProfesorDesdeProfesorDTO(listaProfesorDTO.get(0));
        }
        else
        {
            throw new RegistroNoEncontradoException();
        }
    }

    private Profesor creaProfesorDesdeTupla(Tuple tuplaProfesor)
    {
        Profesor profesor = new Profesor();
        profesor.setId(tuplaProfesor.get(QProfesorDTO.profesorDTO.id));
        profesor.setNombre(tuplaProfesor.get(QProfesorDTO.profesorDTO.nombre));
        profesor.setCreditosPendientes(getRounded(
                tuplaProfesor.get(QProfesorCreditosDTO.profesorCreditosDTO.creditosPendientes)));
        profesor.setCreditosAsignados(getRounded(
                tuplaProfesor.get(QProfesorCreditosDTO.profesorCreditosDTO.creditosAsignados)));
        profesor.setCreditosComputables(getRounded(
                tuplaProfesor.get(QProfesorCreditosDTO.profesorCreditosDTO.creditosComputables)));
        profesor.setCreditosMaximos(
                getRounded(tuplaProfesor.get(QProfesorDTO.profesorDTO.creditosMaximos)));
        profesor.setCreditosReduccion(
                getRounded(tuplaProfesor.get(QProfesorDTO.profesorDTO.creditosReduccion)));
        profesor.setEmail(tuplaProfesor.get(QProfesorDTO.profesorDTO.email));
        profesor.setCreditos(getRounded(tuplaProfesor.get(QProfesorDTO.profesorDTO.creditos)));
        profesor.setNombreCategoria(tuplaProfesor.get(QProfesorDTO.profesorDTO.nombreCategoria));
        profesor.setCategoriaId(tuplaProfesor.get(QProfesorDTO.profesorDTO.categoriaId));

        Area area = new Area();
        AreaDTO areaDTO = tuplaProfesor.get(QProfesorDTO.profesorDTO.area);
        area.setId(areaDTO.getId());
        profesor.setArea(area);

        return profesor;
    }

    protected BigDecimal getRounded(BigDecimal data)
    {
        if (data == null)
        {
            return null;
        }

        return data.setScale(2, BigDecimal.ROUND_HALF_UP);
    }

    private ProfesorDetalle creaProfesorDetalleDesde(ItemProfesorDTO itemProfesorDTO)
    {
        ProfesorDetalle profesorDetalle = new ProfesorDetalle();
        profesorDetalle.setId(itemProfesorDTO.getId());
        profesorDetalle.setDetalleManual(itemProfesorDTO.getDetalleManual());
        profesorDetalle.setCreditos(itemProfesorDTO.getCreditos());
        profesorDetalle.setCreditosComputables(itemProfesorDTO.getCreditosComputables());

        Profesor profesor = new Profesor();
        profesor.setId(itemProfesorDTO.getProfesor().getId());
        profesorDetalle.setProfesor(profesor);

        Evento evento = new Evento();
        evento.setId(itemProfesorDTO.getItem().getId());
        profesorDetalle.setEvento(evento);

        return profesorDetalle;
    }

}
