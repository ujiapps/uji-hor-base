package es.uji.apps.hor.dao;

import java.util.List;

import es.uji.apps.hor.model.Idioma;
import es.uji.commons.db.BaseDAO;

public interface IdiomaDAO extends BaseDAO
{
    List<Idioma> getIdiomas();

    List<Idioma> getIdiomasProfesorItem(Long profesorId, Long itemId);

    void actualizaIdiomas(Long profesorId, Long itemId, List<Long> idiomaIds);
}
