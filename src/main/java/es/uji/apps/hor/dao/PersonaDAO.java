package es.uji.apps.hor.dao;

import es.uji.apps.hor.model.Cargo;
import es.uji.apps.hor.model.Persona;
import es.uji.commons.db.BaseDAO;
import es.uji.commons.db.LookupDAO;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

import java.util.List;

public interface PersonaDAO extends BaseDAO, LookupDAO
{
    Persona getPersonaConTitulacionesYCentrosById(Long personaId)
            throws RegistroNoEncontradoException;

    boolean esAdmin(Long personaId);

    List<Cargo> getTodosLosCargos();

    boolean isAulaAutorizada(Long aulaId, Long personaId);

    Persona insertaPersonasYCargos(Persona persona);

    List<Cargo> getCargosByPersonaId(Long connectedUserId);
}
