package es.uji.apps.hor.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.sso.db.QApaSesione;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;

@Repository("sesionDAO")
public class SesionDAO extends BaseDAODatabaseImpl {

    private QApaSesione qApaSesione = QApaSesione.apaSesione;

    public String getUserSession(BigDecimal personId) {

        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qApaSesione).where(qApaSesione.personaId.eq(personId)).singleResult(qApaSesione).getSesionId();
    }
}
