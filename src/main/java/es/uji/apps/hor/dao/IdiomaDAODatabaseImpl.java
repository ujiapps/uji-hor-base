package es.uji.apps.hor.dao;

import java.util.ArrayList;
import java.util.List;

import com.mysema.query.jpa.impl.JPADeleteClause;
import es.uji.apps.hor.model.*;
import org.springframework.stereotype.Repository;
import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.hor.db.*;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class IdiomaDAODatabaseImpl extends BaseDAODatabaseImpl implements IdiomaDAO
{
    @Override
    public List<Idioma> getIdiomas()
    {
        JPAQuery query = new JPAQuery(entityManager);
        QIdiomaDTO qIdioma = QIdiomaDTO.idiomaDTO;

        List<Idioma> idiomas = new ArrayList<>();
        for (IdiomaDTO idiomaDTO : query.from(qIdioma).list(qIdioma))
        {
            Idioma idioma = new Idioma();
            idioma.setId(idiomaDTO.getId());
            idioma.setNombre(idiomaDTO.getNombre());
            idioma.setCodigoPop(idiomaDTO.getCodigoPop());
            idioma.setAcronimo(idiomaDTO.getAcronimo());
            idiomas.add(idioma);
        }
        return idiomas;
    }

    @Override
    public List<Idioma> getIdiomasProfesorItem(Long profesorId, Long itemId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QIdiomaDTO qIdiomaDTO = QIdiomaDTO.idiomaDTO;
        QItemProfesorIdiomaDTO qItemProfesorIdiomaDTO = QItemProfesorIdiomaDTO.itemProfesorIdiomaDTO;

        List<Idioma> idiomas = new ArrayList<>();

        query.from(qItemProfesorIdiomaDTO).join(qItemProfesorIdiomaDTO.idioma, qIdiomaDTO)
                .where(qItemProfesorIdiomaDTO.itemProfesor.profesor.id.eq(profesorId)
                        .and(qItemProfesorIdiomaDTO.itemProfesor.item.id.eq(itemId)));

        for (ItemProfesorIdiomaDTO itemProfesorIdiomaDTO : query.list(qItemProfesorIdiomaDTO))
        {
            Idioma idioma = new Idioma();
            idioma.setId(itemProfesorIdiomaDTO.getIdioma().getId());
            idioma.setNombre(itemProfesorIdiomaDTO.getIdioma().getNombre());
            idioma.setCodigoPop(itemProfesorIdiomaDTO.getIdioma().getCodigoPop());
            idioma.setActivo(true);
            idiomas.add(idioma);
        }
        return idiomas;
    }

    @Override
    @Transactional
    public void actualizaIdiomas(Long profesorId, Long itemId, List<Long> idiomaIds) {
        Long itemProfesorId = getItemProfesorIdByProfesorIdAndItemId(profesorId, itemId);
        QItemProfesorIdiomaDTO qItemProfesorIdiomaDTO = QItemProfesorIdiomaDTO.itemProfesorIdiomaDTO;
        JPADeleteClause deleteClause = new JPADeleteClause(entityManager, qItemProfesorIdiomaDTO);

        deleteClause.where(qItemProfesorIdiomaDTO.itemProfesor.id.eq(itemProfesorId)).execute();

        for (Long idiomaId: idiomaIds) {
            ItemProfesorIdiomaDTO itemProfesorIdiomaDTO = new ItemProfesorIdiomaDTO();
            itemProfesorIdiomaDTO.setIdioma(new IdiomaDTO(idiomaId));
            itemProfesorIdiomaDTO.setItemProfesor(new ItemProfesorDTO(itemProfesorId));
            insert(itemProfesorIdiomaDTO);
        }

    }

    private Long getItemProfesorIdByProfesorIdAndItemId(Long profesorId, Long itemId) {
        JPAQuery query = new JPAQuery(entityManager);
        QItemProfesorDTO qItemProfesorDTO = QItemProfesorDTO.itemProfesorDTO;

        query.from(qItemProfesorDTO).where(qItemProfesorDTO.item.id.eq(itemId).and(qItemProfesorDTO.profesor.id.eq(profesorId)));

        List<ItemProfesorDTO> itemsProfesorDTO = query.list(qItemProfesorDTO);

        if (itemsProfesorDTO.size() > 0) {
            return itemsProfesorDTO.get(0).getId();
        }

        return null;
    }

}