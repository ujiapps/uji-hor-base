package es.uji.apps.hor.dao;

import java.util.Date;
import java.util.List;

import es.uji.apps.hor.model.Evento;
import es.uji.apps.hor.model.Log;
import es.uji.commons.db.BaseDAO;

public interface LogDAO extends BaseDAO
{
    void register(int tipoAccion, String descripcion, Evento evento, String usuario);

    List<Log> getLogsByFecha(Date fecha);

    List<Log> getLogsByFechaYUsuario(Date fecha, String usuario);

    List<Log> getLogsByFechaYItemId(Date fecha, Long itemId);

    List<Log> getLogsByFechaYItemIdYUsuario(Date fecha, Long itemId, String usuario);

    List<Log> getLogsByItemId(Long itemId);

    Log getLogById(Long id);

    void invalidaLog(Long id);
}
