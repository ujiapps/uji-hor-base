package es.uji.apps.hor.dao;

import es.uji.apps.hor.model.Asignatura;
import es.uji.commons.db.BaseDAO;

public interface ItemsAsignaturaDAO extends BaseDAO
{
    Asignatura insertItemAsignatura(Asignatura asignatura, Long itemId);
}
