package es.uji.apps.hor.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.hor.db.AreaDTO;
import es.uji.apps.hor.db.CargoPersonaDTO;
import es.uji.apps.hor.db.QAreaDTO;
import es.uji.apps.hor.db.QCargoPersonaDTO;
import es.uji.apps.hor.db.QDepartamentoDTO;
import es.uji.apps.hor.db.QProfesorDTO;
import es.uji.apps.hor.model.Area;
import es.uji.apps.hor.model.Departamento;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class AreaDAODatabaseImpl extends BaseDAODatabaseImpl implements AreaDAO
{

    public static final long ACTIVA = 1L;

    @Override
    public List<Area> getAreasByDepartamentoId(Long departamentoId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QAreaDTO qArea = QAreaDTO.areaDTO;
        QDepartamentoDTO qDepartamento = QDepartamentoDTO.departamentoDTO;

        query.from(qArea)
                .join(qArea.departamento, qDepartamento)
                .fetch()
                .where(qArea.departamento.id.eq(departamentoId)
                        .and(qDepartamento.activo.eq(ACTIVA)).and(qArea.activa.eq(ACTIVA)));

        List<Area> listaAreas = new ArrayList<Area>();

        for (AreaDTO areaDTO : query.list(qArea))
        {
            listaAreas.add(creaAreaDesdeAreaDTO(areaDTO));
        }

        return listaAreas;
    }

    @Override
    public List<Area> getAreasByDepartamentoIdAndPersonaId(Long departamentoId, Long connectedUserId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QCargoPersonaDTO qCargoPersonaDTO = QCargoPersonaDTO.cargoPersonaDTO;

        query.from(qCargoPersonaDTO)
                .where(qCargoPersonaDTO.persona.id.eq(connectedUserId).and(
                        qCargoPersonaDTO.departamento.id.eq(departamentoId)))
                .orderBy(qCargoPersonaDTO.nombreArea.asc());

        Map<Long, Area> mapAreas = new HashMap<>();
        for (CargoPersonaDTO cargoPersonaDTO : query.list(qCargoPersonaDTO))
        {
            if (cargoPersonaDTO.getArea() != null
                    && !mapAreas.containsKey(cargoPersonaDTO.getArea().getId()))
            {
                mapAreas.put(cargoPersonaDTO.getArea().getId(),
                        creaAreaDesdeCargoPersonaDTO(cargoPersonaDTO));
            }
        }
        return new ArrayList<>(mapAreas.values());
    }

    @Override
    public List<Area> getMiAreaByPersonaId(Long connectedUserId)
            throws RegistroNoEncontradoException
    {

        JPAQuery query = new JPAQuery(entityManager);

        QAreaDTO qAreaDTO = QAreaDTO.areaDTO;
        QProfesorDTO qProfesorDTO = QProfesorDTO.profesorDTO;

        query.from(qAreaDTO).join(qAreaDTO.profesores, qProfesorDTO)
                .where(qProfesorDTO.id.eq(connectedUserId));

        List<AreaDTO> listaAreasDTO = query.list(qAreaDTO);

        if (listaAreasDTO.size() == 1)
        {
            List<Area> listaArea = new ArrayList<Area>();
            listaArea.add(creaAreaDesdeAreaDTO(listaAreasDTO.get(0)));

            return listaArea;
        }
        else
        {
            throw new RegistroNoEncontradoException();
        }
    }

    private Area creaAreaDesdeCargoPersonaDTO(CargoPersonaDTO cargoPersonaDTO)
    {
        Area area = new Area();
        area.setId(cargoPersonaDTO.getArea().getId());
        area.setNombre(cargoPersonaDTO.getNombreArea());
        area.setActiva(1L);

        Departamento departamento = new Departamento();
        departamento.setId(cargoPersonaDTO.getDepartamento().getId());
        departamento.setNombre(cargoPersonaDTO.getNombreDepartamento());
        area.setDepartamento(departamento);

        return area;
    }

    private Area creaAreaDesdeAreaDTO(AreaDTO areaDTO)
    {
        Area area = new Area();
        area.setId(areaDTO.getId());
        area.setNombre(areaDTO.getNombre());
        area.setActiva(areaDTO.getActiva());

        Departamento departamento = new Departamento();
        departamento.setId(areaDTO.getDepartamento().getId());
        departamento.setNombre(areaDTO.getDepartamento().getNombre());
        area.setDepartamento(departamento);

        return area;
    }

}
