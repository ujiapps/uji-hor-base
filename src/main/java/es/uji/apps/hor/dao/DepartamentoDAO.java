package es.uji.apps.hor.dao;

import es.uji.apps.hor.model.Departamento;
import es.uji.commons.db.BaseDAO;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

import java.util.List;

public interface DepartamentoDAO extends BaseDAO
{
    Departamento insertDepartamento(Departamento departamento);

    List<Departamento> getDepartamentos();

    List<Departamento> getDepartamentosByPersonaId(Long connectedUserId);

    List<Departamento> getMiDepartamentoByPersonaId(Long connectedUserId) throws RegistroNoEncontradoException;
}
