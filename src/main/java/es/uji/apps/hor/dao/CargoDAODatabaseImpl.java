package es.uji.apps.hor.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.hor.db.CargoPersonaDTO;
import es.uji.apps.hor.db.QCargoPersonaDTO;
import es.uji.apps.hor.model.Cargo;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class CargoDAODatabaseImpl extends BaseDAODatabaseImpl implements CargoDAO
{
    @Override
    public List<Cargo> getCargos(Long connectedUserId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QCargoPersonaDTO qCargoPersonaDTO = QCargoPersonaDTO.cargoPersonaDTO;
        query.from(qCargoPersonaDTO).where(qCargoPersonaDTO.persona.id.eq(connectedUserId));

        Map<Long, Cargo> mapCargos = new HashMap<>();
        for (CargoPersonaDTO cargoPersonaDTO : query.list(qCargoPersonaDTO))
        {
            if (!mapCargos.containsKey(cargoPersonaDTO.getCargo().getId()))
            {
                mapCargos.put(cargoPersonaDTO.getCargo().getId(),
                        creaCargoDesdeTipoCargoDTO(cargoPersonaDTO));
            }
        }

        return new ArrayList<>(mapCargos.values());

    }

    private Cargo creaCargoDesdeTipoCargoDTO(CargoPersonaDTO cargoPersonaDTO)
    {
        Cargo cargo = new Cargo();
        cargo.setId(cargoPersonaDTO.getId());
        cargo.setTipoCargoId(cargoPersonaDTO.getCargo().getId());
        cargo.setNombre(cargoPersonaDTO.getCargo().getNombre());
        if (cargoPersonaDTO.getEstudio() != null)
        {
            cargo.setEstudio(cargoPersonaDTO.getEstudio().getId());
        }

        if (cargoPersonaDTO.getDepartamento() != null)
        {
            cargo.setDepartamento(cargoPersonaDTO.getDepartamento().getId());
        }

        if (cargoPersonaDTO.getCentro() != null)
        {
            cargo.setCentro(cargoPersonaDTO.getCentro().getId());
        }

        return cargo;
    }
}
