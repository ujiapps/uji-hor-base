package es.uji.apps.hor.dao;

import es.uji.apps.hor.db.ItemProfesorDetalleVistaDTO;
import es.uji.commons.db.BaseDAO;

import java.util.List;

public interface ItemProfesorDetalleVistaDAO extends BaseDAO
{
    List<ItemProfesorDetalleVistaDTO> getEventosProfesorSemanaDetallePlanificadosPorItemId(
            List<Long> listaItemIds);
}
