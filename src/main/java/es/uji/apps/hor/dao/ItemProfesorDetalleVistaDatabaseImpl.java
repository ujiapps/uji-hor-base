package es.uji.apps.hor.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.hor.db.ItemProfesorDetalleVistaDTO;
import es.uji.apps.hor.db.QItemProfesorDetalleVistaDTO;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ItemProfesorDetalleVistaDatabaseImpl extends BaseDAODatabaseImpl implements
        ItemProfesorDetalleVistaDAO
{
    @Override
    public List<ItemProfesorDetalleVistaDTO> getEventosProfesorSemanaDetallePlanificadosPorItemId(
            List<Long> listaItemIds)
    {
        if (listaItemIds.isEmpty())
        {
            return new ArrayList<>();
        }

        JPAQuery query = new JPAQuery(entityManager);
        QItemProfesorDetalleVistaDTO qItemProfesorDetalleVistaDTO = QItemProfesorDetalleVistaDTO.itemProfesorDetalleVistaDTO;
        query.from(qItemProfesorDetalleVistaDTO)
                .where(qItemProfesorDetalleVistaDTO.itemId.in(listaItemIds))
                .orderBy(qItemProfesorDetalleVistaDTO.inicio.asc());

        return query.list(qItemProfesorDetalleVistaDTO);
    }
}
