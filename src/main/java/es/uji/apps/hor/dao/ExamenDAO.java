package es.uji.apps.hor.dao;

import es.uji.apps.hor.model.Convocatoria;
import es.uji.apps.hor.model.Curso;
import es.uji.apps.hor.model.Examen;
import es.uji.apps.hor.model.TipoExamen;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

import java.util.List;

public interface ExamenDAO
{
    List<Convocatoria> getConvocatorias();

    List<Examen> getExamenesNoPlanificados(Long estudioId, Long convocatoriaId, List<Long> cursosList);

    List<Examen> getExamenesPlanificados(Long estudioId, Long convocatoriaId, List<Long> cursosList);

    void updateExamen(Examen examen);

    Examen getExamenById(Long examenId) throws RegistroNoEncontradoException;

    Convocatoria getConvocatoriaById(Long convocatoriaId) throws RegistroNoEncontradoException;

    Convocatoria getConvocatoriaEstudioById(Long convocatoriaId, Long estudioId, Long cursoId) throws RegistroNoEncontradoException;

    List<TipoExamen> getTiposExamen();

    List<Curso> getCursosEstudio(Long estudioId);

    void deleteExamenById(Long id);

    List<Examen> getExamenesPlanificadosByAsignaturaAndConvocatoria(String asignaturaId, Long convocatoriaId);

    void insertaExamen(Examen nuevoExamen, Long examenOriginalId);

    void deleteExamenAsignaturaByExamenId(Long examenId);

    void deleteExamenAulaByExamenId(Long examenId);

    List<Convocatoria> getConvocatoriasByEstudio(Long estudioId);

    void borraAulasExamen(Long examenId);

    void insertaAulasExamen(Long examenId, Long aulaId);
}
