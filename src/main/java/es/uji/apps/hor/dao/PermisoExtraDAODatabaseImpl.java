package es.uji.apps.hor.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.hor.db.*;
import es.uji.apps.hor.model.*;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Repository
public class PermisoExtraDAODatabaseImpl extends BaseDAODatabaseImpl implements PermisoExtraDAO
{

    @Override
    public List<PermisoExtra> getPermisosExtra()
    {
        JPAQuery query = new JPAQuery(entityManager);

        QPermisoExtraDTO qPermisoExtra = QPermisoExtraDTO.permisoExtraDTO;
        QEstudioDTO qEstudioDTO = QEstudioDTO.estudioDTO;
        QDepartamentoDTO qDepartamentoDTO = QDepartamentoDTO.departamentoDTO;
        QAreaDTO qAreaDTO = QAreaDTO.areaDTO;
        QCentroDTO qCentroDTO = QCentroDTO.centroDTO;
        QPersonaDTO qPersona = QPersonaDTO.personaDTO;
        QTipoCargoDTO qTipoCargo = QTipoCargoDTO.tipoCargoDTO;

        query.from(qPermisoExtra).leftJoin(qPermisoExtra.estudio, qEstudioDTO).fetch()
                .leftJoin(qPermisoExtra.departamento, qDepartamentoDTO).fetch()
                .leftJoin(qPermisoExtra.area, qAreaDTO).fetch()
                .leftJoin(qPermisoExtra.centro, qCentroDTO).fetch()
                .join(qPermisoExtra.persona, qPersona).fetch()
                .join(qPermisoExtra.tipoCargo, qTipoCargo).fetch();

        List<PermisoExtra> listaPermisosExtra = new ArrayList<PermisoExtra>();
        for (PermisoExtraDTO permisoExtraDTO : query.list(qPermisoExtra))
        {
            listaPermisosExtra.add(conviertePermisoExtraDTOAPermisoExtra(permisoExtraDTO));
        }

        return listaPermisosExtra;
    }

    private PermisoExtra conviertePermisoExtraDTOAPermisoExtra(PermisoExtraDTO permisoExtraDTO)
    {
        PermisoExtra permisoExtra = new PermisoExtra();
        permisoExtra.setId(permisoExtraDTO.getId());

        Persona persona = new Persona();
        persona.setNombre(permisoExtraDTO.getPersona().getNombre());
        persona.setId(permisoExtraDTO.getPersona().getId());
        permisoExtra.setPersona(persona);

        Cargo cargo = new Cargo();
        cargo.setNombre(permisoExtraDTO.getTipoCargo().getNombre());
        cargo.setId(permisoExtraDTO.getTipoCargo().getId());
        permisoExtra.setCargo(cargo);
        permisoExtra.setPersonaOtorgaId(permisoExtraDTO.getPersonaOtorgaId());

        if (permisoExtraDTO.getEstudio() != null)
        {
            Estudio estudio = new Estudio();
            estudio.setNombre(permisoExtraDTO.getEstudio().getNombre());
            estudio.setId(permisoExtraDTO.getEstudio().getId());
            permisoExtra.setEstudio(estudio);
        }
        if (permisoExtraDTO.getDepartamento() != null)
        {
            Departamento departamento = new Departamento();
            departamento.setId(permisoExtraDTO.getDepartamento().getId());
            departamento.setNombre(permisoExtraDTO.getDepartamento().getNombre());
            permisoExtra.setDepartamento(departamento);
        }
        if (permisoExtraDTO.getArea() != null)
        {
            Area area = new Area();
            area.setId(permisoExtraDTO.getArea().getId());
            area.setNombre(permisoExtraDTO.getArea().getNombre());
            permisoExtra.setArea(area);
        }
        if (permisoExtraDTO.getCentro() != null)
        {
            Centro centro = new Centro();
            centro.setId(permisoExtraDTO.getCentro().getId());
            centro.setNombre(permisoExtraDTO.getCentro().getNombre());
            permisoExtra.setCentro(centro);
        }
        return permisoExtra;
    }

    @Override
    public List<PermisoExtra> getPermisosExtraByPersonaId(Long connectedUserId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QPermisoExtraDTO qPermisoExtra = QPermisoExtraDTO.permisoExtraDTO;
        QEstudioDTO qEstudio = QEstudioDTO.estudioDTO;
        QPersonaDTO qPersona = QPersonaDTO.personaDTO;
        QCentroDTO qCentroDTO = QCentroDTO.centroDTO;
        QTipoCargoDTO qTipoCargo = QTipoCargoDTO.tipoCargoDTO;

        QDepartamentoDTO qDepartamentoDTO = QDepartamentoDTO.departamentoDTO;
        QAreaDTO qAreaDTO = QAreaDTO.areaDTO;

        query.from(qPermisoExtra).leftJoin(qPermisoExtra.estudio, qEstudio).fetch()
                .leftJoin(qPermisoExtra.departamento, qDepartamentoDTO).fetch()
                .leftJoin(qPermisoExtra.area, qAreaDTO).fetch()
                .leftJoin(qPermisoExtra.centro, qCentroDTO).fetch()
                .join(qPermisoExtra.persona, qPersona).fetch()
                .join(qPermisoExtra.tipoCargo, qTipoCargo).fetch().where(qPermisoExtra.persona.id
                        .eq(connectedUserId).or(qPermisoExtra.personaOtorgaId.eq(connectedUserId)));

        List<PermisoExtra> listaPermisosExtra = new ArrayList<PermisoExtra>();
        for (PermisoExtraDTO permisoExtraDTO : query.distinct().list(qPermisoExtra))
        {
            listaPermisosExtra.add(conviertePermisoExtraDTOAPermisoExtra(permisoExtraDTO));
        }

        return listaPermisosExtra;

    }

    @Override
    @Transactional
    public PermisoExtra addPermisoExtra(Long estudioId, Long personaId, Long tipoCargoId,
            Long connectedUserId)
    {
        PermisoExtraDTO permisoExtraDTO = new PermisoExtraDTO();
        PersonaDTO personaDTO = new PersonaDTO();
        EstudioDTO estudioDTO = new EstudioDTO();
        TipoCargoDTO tipoCargoDTO = new TipoCargoDTO();

        personaDTO.setId(personaId);
        estudioDTO.setId(estudioId);
        tipoCargoDTO.setId(tipoCargoId);

        permisoExtraDTO.setPersona(personaDTO);
        permisoExtraDTO.setEstudio(estudioDTO);
        permisoExtraDTO.setTipoCargo(tipoCargoDTO);
        permisoExtraDTO.setPersonaOtorgaId(connectedUserId);

        permisoExtraDTO = insert(permisoExtraDTO);
        return conviertePermisoExtraDTOAPermisoExtra(permisoExtraDTO);
    }

    @Override
    public PermisoExtra getPermisoExtraById(Long permisoExtraId)
            throws RegistroNoEncontradoException
    {
        JPAQuery query = new JPAQuery(entityManager);

        QPermisoExtraDTO qPermisoExtra = QPermisoExtraDTO.permisoExtraDTO;

        query.from(qPermisoExtra).where(qPermisoExtra.id.eq(permisoExtraId));

        if (query.list(qPermisoExtra).size() > 0)
        {
            return conviertePermisoExtraDTOAPermisoExtra(query.list(qPermisoExtra).get(0));
        }
        else
        {
            throw new RegistroNoEncontradoException();
        }
    }

    @Override
    @Transactional
    public PermisoExtra addPermisoExtra(PermisoExtra permisoExtra)
    {
        PermisoExtraDTO permisoExtraDTO = getPermisoExtraDTODesde(permisoExtra);
        permisoExtraDTO = insert(permisoExtraDTO);
        return conviertePermisoExtraDTOAPermisoExtra(permisoExtraDTO);
    }

    private PermisoExtraDTO getPermisoExtraDTODesde(PermisoExtra permisoExtra)
    {
        PermisoExtraDTO permisoExtraDTO = new PermisoExtraDTO();

        PersonaDTO personaDTO = new PersonaDTO();
        personaDTO.setId(permisoExtra.getPersona().getId());
        permisoExtraDTO.setPersona(personaDTO);

        TipoCargoDTO tipoCargoDTO = new TipoCargoDTO();
        tipoCargoDTO.setId(permisoExtra.getCargo().getId());
        permisoExtraDTO.setTipoCargo(tipoCargoDTO);
        permisoExtraDTO.setPersonaOtorgaId(permisoExtra.getPersonaOtorgaId());

        if (permisoExtra.getDepartamento() != null)
        {
            DepartamentoDTO departamentoDTO = new DepartamentoDTO();
            departamentoDTO.setId(permisoExtra.getDepartamento().getId());
            permisoExtraDTO.setDepartamento(departamentoDTO);
        }

        if (permisoExtra.getEstudio() != null)
        {
            EstudioDTO estudioDTO = new EstudioDTO();
            estudioDTO.setId(permisoExtra.getEstudio().getId());
            permisoExtraDTO.setEstudio(estudioDTO);
        }

        if (permisoExtra.getArea() != null)
        {
            AreaDTO areaDTO = new AreaDTO();
            areaDTO.setId(permisoExtra.getArea().getId());
            permisoExtraDTO.setArea(areaDTO);
        }

        if (permisoExtra.getCentro() != null)
        {
            CentroDTO centroDTO = new CentroDTO();
            centroDTO.setId(permisoExtra.getCentro().getId());
            permisoExtraDTO.setCentro(centroDTO);
        }

        return permisoExtraDTO;
    }
}