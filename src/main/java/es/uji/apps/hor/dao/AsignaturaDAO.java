package es.uji.apps.hor.dao;

import es.uji.apps.hor.model.Asignatura;
import es.uji.apps.hor.model.AsignaturaAgrupacion;
import es.uji.commons.db.BaseDAO;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public interface AsignaturaDAO extends BaseDAO
{
    List<Asignatura> getAsignaturasByAreaIdAndSemestreId(Long areaId, Long semestreId);

    List<Asignatura> getAsignaturasByAreaId(Long areaId);

    List<Asignatura> getAsignaturasByTitulacionAndCursoAndSemestre(Long estudioId, Long cursoId,
            Long semestreId, List<String> gruposIds);

    List<Asignatura> getAsignaturasByTitulacionAndCurso(Long estudioId, Long cursoId);

    List<AsignaturaAgrupacion> getAsignaturasByAgrupacionId(Long agrupacionId);

    List<Asignatura> getAsignaturasCompartidaByEstudioYAsignaturaId(String asignaturaId,
            List<Long> estudioCompartidoIds);

    List<Asignatura> getAsignaturasByAgrupacionAndSemestreAndGrupos(Long estudioId,
            Long agrupacionId, Long semestreId, List<String> gruposAsList);

    String getCodigoComunByAsignaturaId(String asignaturaId);

    Map<String, BigDecimal> getCreditosPodAsignatura(List<String> asignaturasId, boolean asignaturaComparteItems);

    Map<String, BigDecimal> getCreditosPodAsignatura(String asignaturasId);

    Map<String, BigDecimal>  getCreditosPodAsignados(List<String> asignaturasId);

    List<String> getCodigosAsignaturasCompartidas(String asignaturaId);

    List<Asignatura> getAsignaturasByProfesorId(Long profesorId, Long semestreId);
}
