package es.uji.apps.hor.dao;

import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.QTuple;
import es.uji.apps.hor.db.QItemDTO;
import es.uji.apps.hor.db.QItemsAsignaturaDTO;
import es.uji.apps.hor.model.TipoSubgrupoUI;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class TipoSubgruposDAODatabaseImpl extends BaseDAODatabaseImpl implements TipoSubgruposDAO
{
    @Override
    public List<TipoSubgrupoUI> getSubGruposAsignatura(String asignaturaId, Long semestreId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QItemDTO item = QItemDTO.itemDTO;
        QItemsAsignaturaDTO asignatura = QItemsAsignaturaDTO.itemsAsignaturaDTO;

        List<Tuple> listaSubGruposTuples = query
                .from(asignatura)
                .join(asignatura.item, item)
                .where((asignatura.asignaturaId.eq(asignaturaId).or(item.comunes.like("%"
                        + asignaturaId + "%"))).and(asignatura.item.semestre.id.eq(semestreId)))
                .orderBy(item.tipoSubgrupo.asc()).distinct()
                .list(new QTuple(item.tipoSubgrupoId, item.tipoSubgrupo));

        List<TipoSubgrupoUI> subGrupos = new ArrayList<TipoSubgrupoUI>();

        for (Tuple tuple : listaSubGruposTuples)
        {
            subGrupos.add(new TipoSubgrupoUI(tuple.get(item.tipoSubgrupoId), tuple
                    .get(item.tipoSubgrupo)));
        }

        return subGrupos;
    }
}