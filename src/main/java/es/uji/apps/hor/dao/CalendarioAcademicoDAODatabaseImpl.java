package es.uji.apps.hor.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.hor.db.CalendarioDTO;
import es.uji.apps.hor.db.QCalendarioDTO;
import es.uji.apps.hor.model.CalendarioAcademico;
import es.uji.apps.hor.model.TipoEstudio;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
public class CalendarioAcademicoDAODatabaseImpl extends BaseDAODatabaseImpl implements
        CalendarioAcademicoDAO {
    private static final long DIA_SEMANA_DOMINGO = 7L;
    private static final long DIA_SEMANA_SABADO = 6L;
    private static final String TIPO_DIA_LECTIVO = "L";
    private static final String TIPO_DIA_FESTIVO = "F";
    private static final String TIPO_DIA_EXAMEN = "E";
    private static final String TIPO_DIA_NO_LECTIVO = "N";

    @Override
    public CalendarioAcademico getCalendarioAcademicoByFecha(String tipoEstudioId, Date fecha)
            throws RegistroNoEncontradoException {
        JPAQuery query = new JPAQuery(entityManager);
        QCalendarioDTO qCalendario = QCalendarioDTO.calendarioDTO;

        CalendarioDTO calendarioDTO = query.from(qCalendario).where(qCalendario.fecha.eq(fecha))
                .uniqueResult(qCalendario);

        if (calendarioDTO == null) {
            throw new RegistroNoEncontradoException();
        }

        return creaCalendarioAcademicoFrom(calendarioDTO, tipoEstudioId);
    }

    private CalendarioAcademico creaCalendarioAcademicoFrom(CalendarioDTO calendarioDTO, String tipoEstudioId) {
        CalendarioAcademico calendario = new CalendarioAcademico();
        calendario.setId(calendarioDTO.getId());
        calendario.setAnyo(calendarioDTO.getAño());
        calendario.setDia(calendarioDTO.getDia());
        calendario.setDiaSemana(calendarioDTO.getDiaSemana());
        calendario.setDiaSemanaId(calendarioDTO.getDiaSemanaId());
        calendario.setFecha(calendarioDTO.getFecha());
        calendario.setMes(calendarioDTO.getMes());
        if (TipoEstudio.GRADO.equals(tipoEstudioId))
        {
            calendario.setTipoDia(calendarioDTO.getTipoDia());
        } else {
            calendario.setTipoDia(calendarioDTO.getTipoDiaMaster());
        }
        calendario.setVacaciones(calendarioDTO.getVacaciones());

        return calendario;
    }

    @Override
    public List<CalendarioAcademico> getCalendarioAcademicoNoLectivos(String tipoEstudioId, Date fechaInicio, Date fechaFin) {
        JPAQuery query = new JPAQuery(entityManager);
        QCalendarioDTO qCalendarioDTO = QCalendarioDTO.calendarioDTO;

        query.from(qCalendarioDTO).where(
                qCalendarioDTO.diaSemanaId.notIn(DIA_SEMANA_DOMINGO)
                        .and(qCalendarioDTO.fecha.between(fechaInicio, fechaFin)));

        if (tipoEstudioId.equals(TipoEstudio.GRADO)) {
            query.where(qCalendarioDTO.tipoDia.in(TIPO_DIA_FESTIVO, TIPO_DIA_NO_LECTIVO));
        } else {
            query.where(qCalendarioDTO.tipoDiaMaster.in(TIPO_DIA_FESTIVO, TIPO_DIA_NO_LECTIVO));
        }

        List<CalendarioAcademico> listaCalendarioAcademico = new ArrayList<CalendarioAcademico>();
        for (CalendarioDTO calendario : query.list(qCalendarioDTO)) {
            listaCalendarioAcademico.add(creaCalendarioAcademicoFrom(calendario, tipoEstudioId));
        }

        return listaCalendarioAcademico;
    }

    @Override
    public List<CalendarioAcademico> getCalendarioAcademicoFestivos(String tipoEstudioId, Date fechaInicio, Date fechaFin) {
        JPAQuery query = new JPAQuery(entityManager);
        QCalendarioDTO qCalendarioDTO = QCalendarioDTO.calendarioDTO;

        if (tipoEstudioId.equals(TipoEstudio.GRADO)) {
            query.from(qCalendarioDTO).where(
                    qCalendarioDTO.tipoDia.eq(TIPO_DIA_FESTIVO).and(
                            qCalendarioDTO.fecha.between(fechaInicio, fechaFin)));
        } else {
            query.from(qCalendarioDTO).where(
                    qCalendarioDTO.tipoDiaMaster.eq(TIPO_DIA_FESTIVO).and(
                            qCalendarioDTO.fecha.between(fechaInicio, fechaFin)));
        }

        List<CalendarioAcademico> listaCalendarioAcademico = new ArrayList<CalendarioAcademico>();
        for (CalendarioDTO calendario : query.list(qCalendarioDTO)) {
            listaCalendarioAcademico.add(creaCalendarioAcademicoFrom(calendario, tipoEstudioId));
        }

        return listaCalendarioAcademico;
    }
}
