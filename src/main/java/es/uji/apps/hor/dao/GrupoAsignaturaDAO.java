package es.uji.apps.hor.dao;

import java.util.List;

import es.uji.apps.hor.model.GrupoAsignatura;
import es.uji.commons.db.BaseDAO;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

public interface GrupoAsignaturaDAO extends BaseDAO
{
    List<GrupoAsignatura> getGruposAsignaturasSinAsignar(Long estudioId, Long cursoId,
            Long semestreId, List<String> gruposIds, List<Long> calendariosIds, List<String> asignaturasIds);

    GrupoAsignatura getGrupoAsignaturaById(Long grupoAsignaturaId, Long estudioId)
            throws RegistroNoEncontradoException;

    void updateGrupoAsignaturaPlanificado(GrupoAsignatura grupoAsignatura)
            throws RegistroNoEncontradoException;

    List<GrupoAsignatura> getGruposAsignaturasSinAsignarByAgrupacion(Long estudioId,
            Long agrupacionId, Long semestreId, List<String> gruposIds, List<Long> calendariosIds, List<String> asignaturasIds);
}
