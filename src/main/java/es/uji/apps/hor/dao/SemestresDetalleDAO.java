package es.uji.apps.hor.dao;

import es.uji.apps.hor.model.SemestreDetalle;
import es.uji.commons.db.BaseDAO;

import java.util.List;

public interface SemestresDetalleDAO extends BaseDAO
{
    List<SemestreDetalle> getSemestresDetalleTodos();

    List<SemestreDetalle> getSemestresDetallesPorEstudioIdYSemestreId(Long estudioId, Long semestreId);

    List<SemestreDetalle> getSemestresDetallesPorEstudioIdYSemestreIdYCurso(Long estudioId, Long semestreId, Long cursoAcademico);

    SemestreDetalle insert(SemestreDetalle semestreDetalle);

    List<SemestreDetalle> getRangoFechasSemestresDetallesPorEstudioIdYSemestreId(Long estudioId, Long semestreId);
}
