package es.uji.apps.hor.dao;

import es.uji.apps.hor.EventoYaAsignado;
import es.uji.apps.hor.model.Profesor;
import es.uji.apps.hor.model.ProfesorDetalle;
import es.uji.commons.db.BaseDAO;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

public interface ProfesorDAO extends BaseDAO
{
    List<Profesor> getProfesoresByAreaId(Long areaId);

    List<Profesor> getProfesoresByAreaIdAndPersonaId(Long areaId, Long connectedUserId);

    void asociaClase(Long itemId, Long profesorId);

    void desasociaClase(Long itemId, Long profesorId);

    void updateDetalleManualByItemsId(List<Long> itemsId, Long profesorId, Boolean detalleManual);

    void updateCreditosByItemsId(List<Long> itemsId, Long profesorId, BigDecimal creditos, BigDecimal creditosComputables);

    void insertaItemDetallesProfesor(HashMap<Long, List> fechasSeleccionadasPorEvento)
            throws EventoYaAsignado;

    void borraItemDetallesProfesorByEventosId(List<Long> eventoIds, Long profesorId);

    ProfesorDetalle getProfesorDetalleByItemIdYProfesorId(Long itemId, Long profesorId) throws RegistroNoEncontradoException;

    void updateCreditosEventoByProfesorId(Long itemId, Long profesorId, BigDecimal creditos, BigDecimal creditosComputables);

    List<Profesor> getProfesoresYCreditosRestantesByAreaIdAndPersonaId(Long areaId);

    Profesor getProfesorById(Long connectedUserId) throws RegistroNoEncontradoException;
}
