package es.uji.apps.hor.dao;

import java.util.List;

import es.uji.apps.hor.model.Informacion;
import es.uji.commons.db.BaseDAO;

public interface InformacionDAO extends BaseDAO
{
    List<Informacion> getInformacion(Long connectedUserId);
}
