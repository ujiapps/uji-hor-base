package es.uji.apps.hor.dao;

import java.util.List;

import es.uji.apps.hor.model.Area;
import es.uji.commons.db.BaseDAO;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

public interface AreaDAO extends BaseDAO
{
    List<Area> getAreasByDepartamentoId(Long departamentoId);

    List<Area> getAreasByDepartamentoIdAndPersonaId(Long departamentoId, Long connectedUserId);

    List<Area> getMiAreaByPersonaId(Long connectedUserId) throws RegistroNoEncontradoException;
}
