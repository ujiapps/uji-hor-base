package es.uji.apps.hor;

@SuppressWarnings("serial")
public class EventoYaAsignado extends GeneralHORException
{
    public EventoYaAsignado()
    {
        super("La classe ja està assignada a un altre professor");
    }

    public EventoYaAsignado(String message)
    {
        super(message);
    }
}
