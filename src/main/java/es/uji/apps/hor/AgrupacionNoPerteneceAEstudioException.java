package es.uji.apps.hor;


@SuppressWarnings("serial")
public class AgrupacionNoPerteneceAEstudioException extends GeneralHORException {
    public AgrupacionNoPerteneceAEstudioException() {
        super("El circuit no pertany a l'estudi del event seleccionat");
    }
}
