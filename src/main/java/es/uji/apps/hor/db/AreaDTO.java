package es.uji.apps.hor.db;

import org.hibernate.annotations.BatchSize;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the HOR_AREAS database table.
 *
 */
@Entity
@BatchSize(size=100)
@Table(name="HOR_AREAS")
public class AreaDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	private Long activa;

	private String nombre;

	//bi-directional many-to-one association to DepartamentoDTO
    @ManyToOne
	@JoinColumn(name="DEPARTAMENTO_ID")
	private DepartamentoDTO departamento;

	//bi-directional many-to-one association to ProfesorDTO
	@OneToMany(mappedBy="area")
	private Set<ProfesorDTO> profesores;

    // bi-directional many-to-one association to PermisoExtraDTO
    @OneToMany(mappedBy = "area")
    private Set<PermisoExtraDTO> permisosExtras;

    public AreaDTO() {
    }

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getActiva() {
		return this.activa;
	}

	public void setActiva(Long activa) {
		this.activa = activa;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public DepartamentoDTO getDepartamento() {
		return this.departamento;
	}

	public void setDepartamento(DepartamentoDTO departamento) {
		this.departamento = departamento;
	}

	public Set<ProfesorDTO> getProfesores() {
		return this.profesores;
	}

	public void setProfesores(Set<ProfesorDTO> profesores) {
		this.profesores = profesores;
	}

    public Set<PermisoExtraDTO> getPermisosExtras()
    {
        return permisosExtras;
    }

    public void setPermisosExtras(Set<PermisoExtraDTO> permisosExtras)
    {
        this.permisosExtras = permisosExtras;
    }
}