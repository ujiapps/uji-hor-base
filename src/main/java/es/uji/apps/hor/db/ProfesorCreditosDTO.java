package es.uji.apps.hor.db;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * The persistent class for the HOR_V_GRUPOS database table.
 */
@Entity
@Table(name = "HOR_V_PROFESOR_CREDITOS")
public class ProfesorCreditosDTO implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    private Long id;

    private String nombre;

    @Column(name = "CREDITOS_PROFESOR")
    private BigDecimal creditosProfesor;

    @Column(name = "CREDITOS_ASIGNADOS")
    private BigDecimal creditosAsignados;

    @Column(name = "CREDITOS_PENDIENTES")
    private BigDecimal creditosPendientes;

    @Column(name = "CREDITOS_PACTADOS")
    private BigDecimal creditosComputables;

    @Column(name = "AREA_ID")
    private Long areaId;

    @Column(name = "DEPARTAMENTO_ID")
    private Long departamentoId;

    public BigDecimal getCreditosProfesor()
    {
        return creditosProfesor;
    }

    public void setCreditosProfesor(BigDecimal creditosProfesor)
    {
        this.creditosProfesor = creditosProfesor;
    }

    public BigDecimal getCreditosAsignados()
    {
        return creditosAsignados;
    }

    public void setCreditosAsignados(BigDecimal creditosAsignados)
    {
        this.creditosAsignados = creditosAsignados;
    }

    public BigDecimal getCreditosComputables()
    {
        return creditosComputables;
    }

    public void setCreditosComputables(BigDecimal creditosComputables)
    {
        this.creditosComputables = creditosComputables;
    }

    public BigDecimal getCreditosPendientes()
    {
        return creditosPendientes;
    }

    public void setCreditosPendientes(BigDecimal creditosPendientes)
    {
        this.creditosPendientes = creditosPendientes;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Long getAreaId()
    {
        return areaId;
    }

    public void setAreaId(Long areaId)
    {
        this.areaId = areaId;
    }

    public Long getDepartamentoId()
    {
        return departamentoId;
    }

    public void setDepartamentoId(Long departamentoId)
    {
        this.departamentoId = departamentoId;
    }
}
