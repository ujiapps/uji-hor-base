package es.uji.apps.hor.db;

import org.hibernate.annotations.BatchSize;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.*;

/**
 * The persistent class for the HOR_ESTUDIOS database table.
 * 
 */
@Entity
@BatchSize(size=100)
@Table(name = "HOR_ESTUDIOS")
@SuppressWarnings("serial")
public class EstudioDTO implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String nombre;

    private Long oficial;

    @Column(name="ABIERTO_CONSULTA")
    private Boolean abiertoConsulta;

    @Column(name="SESIONES_POD_PDI")
    private Boolean abiertoSesionesPodPdi;

    // bi-directional many-to-one association to CircuitoEstudioDTO
    @OneToMany(mappedBy = "estudio")
    private Set<CircuitoEstudioDTO> circuitosEstudio;

    // bi-directional many-to-one association to EstudiosCompartidosDTO
    @OneToMany(mappedBy = "estudio")
    private Set<EstudiosCompartidosDTO> estudiosCompartidos;

    // bi-directional many-to-one association to CentroDTO
    @ManyToOne
    @JoinColumn(name = "CENTRO_ID")
    private CentroDTO centro;

    @OneToMany(mappedBy = "estudio")
    private Set<AgrupacionEstudioDTO> agrupacionesEstudio;

    // bi-directional many-to-one association to TipoEstudioDTO
    @ManyToOne
    @JoinColumn(name = "TIPO_ID")
    private TipoEstudioDTO tipoEstudio;

    // bi-directional many-to-one association to CargoPersonaDTO
    @OneToMany(mappedBy = "estudio")
    private Set<CargoPersonaDTO> cargosPersona;

    // bi-directional many-to-one association to PermisoExtraDTO
    @OneToMany(mappedBy = "estudio")
    private Set<PermisoExtraDTO> permisosExtras;

    // bi-directional many-to-one association to ItemAgrupacionDTO
    @OneToMany(mappedBy = "estudio")
    private Set<ItemAgrupacionDTO> itemsAgrupacion;

    // bi-directional many-to-one association to ItemAgrupacionDTO
    @OneToMany(mappedBy = "estudio")
    private Set<FechaConvocatoriaEstudioDTO> fechasConvocatoria;

    @Column(name = "NUMERO_CURSOS")
    private Long numeroCursos;

    public EstudioDTO()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Long getOficial()
    {
        return this.oficial;
    }

    public void setOficial(Long oficial)
    {
        this.oficial = oficial;
    }

    public Boolean getAbiertoConsulta()
    {
        return abiertoConsulta;
    }

    public void setAbiertoConsulta(Boolean abiertoConsulta)
    {
        this.abiertoConsulta = abiertoConsulta;
    }

    public Boolean getAbiertoSesionesPodPdi()
    {
        return abiertoSesionesPodPdi;
    }

    public void setAbiertoSesionesPodPdi(Boolean abiertoSesionesPodPdi)
    {
        this.abiertoSesionesPodPdi = abiertoSesionesPodPdi;
    }

    public CentroDTO getCentro()
    {
        return this.centro;
    }

    public void setCentro(CentroDTO centro)
    {
        this.centro = centro;
    }

    public TipoEstudioDTO getTipoEstudio()
    {
        return this.tipoEstudio;
    }

    public void setTipoEstudio(TipoEstudioDTO tipoEstudio)
    {
        this.tipoEstudio = tipoEstudio;
    }

    public Set<CargoPersonaDTO> getCargosPersona()
    {
        return this.cargosPersona;
    }

    public void setCargosPersona(Set<CargoPersonaDTO> cargosPersona)
    {
        this.cargosPersona = cargosPersona;
    }

    public Set<PermisoExtraDTO> getPermisosExtras()
    {
        return this.permisosExtras;
    }

    public void setPermisosExtras(Set<PermisoExtraDTO> permisosExtras)
    {
        this.permisosExtras = permisosExtras;
    }

    public Long getNumeroCursos()
    {
        return numeroCursos;
    }

    public void setNumeroCursos(Long numeroCursos)
    {
        this.numeroCursos = numeroCursos;
    }

    public Set<EstudiosCompartidosDTO> getEstudiosCompartidos()
    {
        return estudiosCompartidos;
    }

    public void setEstudiosCompartidos(Set<EstudiosCompartidosDTO> estudiosCompartidos)
    {
        this.estudiosCompartidos = estudiosCompartidos;
    }

    public Set<CircuitoEstudioDTO> getCircuitosEstudio()
    {
        return circuitosEstudio;
    }

    public void setCircuitosEstudio(Set<CircuitoEstudioDTO> circuitosEstudio)
    {
        this.circuitosEstudio = circuitosEstudio;
    }

    public Set<AgrupacionEstudioDTO> getAgrupacionesEstudio()
    {
        return agrupacionesEstudio;
    }

    public void setAgrupacionesEstudio(Set<AgrupacionEstudioDTO> agrupacionesEstudio)
    {
        this.agrupacionesEstudio = agrupacionesEstudio;
    }

    public Set<ItemAgrupacionDTO> getItemsAgrupacion()
    {
        return itemsAgrupacion;
    }

    public void setItemsAgrupacion(Set<ItemAgrupacionDTO> itemsAgrupacion)
    {
        this.itemsAgrupacion = itemsAgrupacion;
    }
}