package es.uji.apps.hor.db;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.*;

/**
 * The persistent class for the HOR_V_ASIGNATURAS_AREA database table.
 * 
 */
@Entity
@Table(name = "HOR_V_ASIGNATURAS")
public class AsignaturaEstudioDTO implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID")
    private String asignaturaId;

    @Column(name = "ASIGNATURA")
    private String asignaturaNombre;

    @Column(name = "ESTUDIO_ID")
    private Long estudioId;

    @Column(name = "ESTUDIO")
    private String estudioNombre;

    @Column(name = "CURSO_ID")
    private Long cursoId;

    @Column(name = "CARACTER_ID")
    private String caracterId;

    @Column(name = "CARACTER")
    private String caracterNombre;

    @OneToMany(mappedBy = "asignatura")
    private Set<AgrupacionAsignaturaDTO> agrupacionesAsignatura;

    public AsignaturaEstudioDTO()
    {
    }

    public AsignaturaEstudioDTO(String id)
    {
        asignaturaId = id;
    }

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId)
    {
        this.asignaturaId = asignaturaId;
    }

    public String getAsignaturaNombre()
    {
        return asignaturaNombre;
    }

    public void setAsignaturaNombre(String asignaturaNombre)
    {
        this.asignaturaNombre = asignaturaNombre;
    }

    public Long getEstudioId()
    {
        return estudioId;
    }

    public void setEstudioId(Long estudioId)
    {
        this.estudioId = estudioId;
    }

    public String getEstudioNombre()
    {
        return estudioNombre;
    }

    public void setEstudioNombre(String estudioNombre)
    {
        this.estudioNombre = estudioNombre;
    }

    public Long getCursoId()
    {
        return cursoId;
    }

    public void setCursoId(Long cursoId)
    {
        this.cursoId = cursoId;
    }

    public String getCaracterId()
    {
        return caracterId;
    }

    public void setCaracterId(String caracterId)
    {
        this.caracterId = caracterId;
    }

    public String getCaracterNombre()
    {
        return caracterNombre;
    }

    public void setCaracterNombre(String caracterNombre)
    {
        this.caracterNombre = caracterNombre;
    }

    public Set<AgrupacionAsignaturaDTO> getAgrupacionesAsignatura()
    {
        return agrupacionesAsignatura;
    }

    public void setAgrupacionesAsignatura(Set<AgrupacionAsignaturaDTO> agrupacionesAsignatura)
    {
        this.agrupacionesAsignatura = agrupacionesAsignatura;
    }
}
