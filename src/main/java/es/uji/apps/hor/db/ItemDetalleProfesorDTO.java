package es.uji.apps.hor.db;

import org.hibernate.annotations.BatchSize;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@BatchSize(size=100)
@Table(name = "HOR_ITEMS_DET_PROFESORES")
public class ItemDetalleProfesorDTO implements Serializable
{
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "DETALLE_ID")
    private ItemDetalleDTO itemDetalle;

    @ManyToOne
    @JoinColumn(name = "ITEM_PROFESOR_ID")
    private ItemProfesorDTO itemProfesor;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public ItemDetalleDTO getItemDetalle()
    {
        return itemDetalle;
    }

    public void setItemDetalle(ItemDetalleDTO itemDetalle)
    {
        this.itemDetalle = itemDetalle;
    }

    public ItemProfesorDTO getItemProfesor()
    {
        return itemProfesor;
    }

    public void setItemProfesor(ItemProfesorDTO itemProfesor)
    {
        this.itemProfesor = itemProfesor;
    }
}
