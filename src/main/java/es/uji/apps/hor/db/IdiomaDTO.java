package es.uji.apps.hor.db;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import org.hibernate.annotations.BatchSize;

@Entity
@BatchSize(size = 100)
@Table(name = "HOR_IDIOMAS")
public class IdiomaDTO implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String nombre;

    @Column(name = "ID_POP")
    private String codigoPop;

    @Column(name = "ACRONIMO")
    private String acronimo;

    @OneToMany(mappedBy = "idioma", cascade = CascadeType.ALL)
    private Set<ItemProfesorIdiomaDTO> itemsProfesorIdioma;

    public IdiomaDTO()
    {
    }

    public IdiomaDTO(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getCodigoPop()
    {
        return codigoPop;
    }

    public void setCodigoPop(String codigoPop)
    {
        this.codigoPop = codigoPop;
    }

    public String getAcronimo() {
        return acronimo;
    }

    public void setAcronimo(String acronimo) {
        this.acronimo = acronimo;
    }
}