package es.uji.apps.hor.db;

import org.hibernate.annotations.BatchSize;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * The persistent class for the HOR_V_GRUPOS database table.
 * 
 */
@Entity
@BatchSize(size=100)
@Table(name="HOR_V_GRUPOS")
public class GrupoDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private String especial;

	private String estudio;

	@Id
	@Column(name="ESTUDIO_ID")
	private Long estudioId;

	@Column(name="GRUPO_ID")
	private String grupoId;

    public GrupoDTO() {
    }

	public String getEspecial() {
		return this.especial;
	}

	public void setEspecial(String especial) {
		this.especial = especial;
	}

	public String getEstudio() {
		return this.estudio;
	}

	public void setEstudio(String estudio) {
		this.estudio = estudio;
	}

	public Long getEstudioId() {
		return this.estudioId;
	}

	public void setEstudioId(Long estudioId) {
		this.estudioId = estudioId;
	}

	public String getGrupoId() {
		return this.grupoId;
	}

	public void setGrupoId(String grupoId) {
		this.grupoId = grupoId;
	}

}