package es.uji.apps.hor.db;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * The persistent class for the HOR_DIAS_SEMANA database table.
 * 
 */
@Entity
@SuppressWarnings("serial")
@Table(name="HOR_V_FECHAS_CONVOCATORIAS")
public class FechaConvocatoriaDTO implements Serializable {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

    private String nombre;

    @Column(name = "FECHA_EXAMENES_INICIO")
    private Date fechaExamenesInicio;

    @Column(name = "FECHA_EXAMENES_FIN")
    private Date fechaExamenesFin;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFechaExamenesInicio() {
        return fechaExamenesInicio;
    }

    public void setFechaExamenesInicio(Date fechaExamenesInicio) {
        this.fechaExamenesInicio = fechaExamenesInicio;
    }

    public Date getFechaExamenesFin() {
        return fechaExamenesFin;
    }

    public void setFechaExamenesFin(Date fechaExamenesFin) {
        this.fechaExamenesFin = fechaExamenesFin;
    }
}
