package es.uji.apps.hor.db;

import org.hibernate.annotations.BatchSize;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.*;

/**
 * The persistent class for the HOR_AREAS database table.
 *
 */
@Entity
@BatchSize(size=100)
@Table(name = "HOR_AGRUPACIONES")
public class AgrupacionDTO implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String nombre;

    @OneToMany(mappedBy = "agrupacion", cascade = CascadeType.ALL)
    private Set<AgrupacionEstudioDTO> agrupacionesEstudio;

    @OneToMany(mappedBy = "agrupacion", cascade = CascadeType.ALL)
    private Set<AgrupacionAsignaturaDTO> agrupacionesAsignatura;

    // bi-directional many-to-one association to ItemAgrupacionDTO
    @OneToMany(mappedBy = "agrupacion")
    private Set<ItemAgrupacionDTO> itemsAgrupacion;

    public AgrupacionDTO()
    {
    }

    public AgrupacionDTO(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Set<AgrupacionEstudioDTO> getAgrupacionesEstudio()
    {
        return agrupacionesEstudio;
    }

    public void setAgrupacionesEstudio(Set<AgrupacionEstudioDTO> agrupacionesEstudio)
    {
        this.agrupacionesEstudio = agrupacionesEstudio;
    }

    public Set<AgrupacionAsignaturaDTO> getAgrupacionesAsignatura()
    {
        return agrupacionesAsignatura;
    }

    public void setAgrupacionesAsignatura(Set<AgrupacionAsignaturaDTO> agrupacionesAsignatura)
    {
        this.agrupacionesAsignatura = agrupacionesAsignatura;
    }

    public Set<ItemAgrupacionDTO> getItemsAgrupacion()
    {
        return itemsAgrupacion;
    }

    public void setItemsAgrupacion(Set<ItemAgrupacionDTO> itemsAgrupacion)
    {
        this.itemsAgrupacion = itemsAgrupacion;
    }
}