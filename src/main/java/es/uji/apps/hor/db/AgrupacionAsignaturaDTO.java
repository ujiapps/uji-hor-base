package es.uji.apps.hor.db;

import org.hibernate.annotations.BatchSize;

import java.io.Serializable;

import javax.persistence.*;

/**
 * The persistent class for the HOR_AGRUPACIONES_ASIGNATURAS database table.
 *
 */
@Entity
@BatchSize(size=100)
@Table(name = "HOR_AGRUPACIONES_ASIGNATURAS")
public class AgrupacionAsignaturaDTO implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "SUBGRUPO_ID")
    private Long subgrupoId;

    @Column(name = "TIPO_SUBGRUPO_ID")
    private String tipoSubgrupoId;

    @ManyToOne
    @JoinColumn(name = "AGRUPACION_ID")
    private AgrupacionDTO agrupacion;

    @ManyToOne
    @JoinColumn(name = "ASIGNATURA_ID")
    private AsignaturaEstudioDTO asignatura;

    public AgrupacionAsignaturaDTO()
    {
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public AsignaturaEstudioDTO getAsignatura()
    {
        return asignatura;
    }

    public void setAsignatura(AsignaturaEstudioDTO asignatura)
    {
        this.asignatura = asignatura;
    }

    public AgrupacionDTO getAgrupacion()
    {
        return agrupacion;
    }

    public void setAgrupacion(AgrupacionDTO agrupacion)
    {
        this.agrupacion = agrupacion;
    }

    public Long getSubgrupoId()
    {
        return subgrupoId;
    }

    public void setSubgrupoId(Long subgrupoId)
    {
        this.subgrupoId = subgrupoId;
    }

    public String getTipoSubgrupoId()
    {
        return tipoSubgrupoId;
    }

    public void setTipoSubgrupoId(String tipoSubgrupoId)
    {
        this.tipoSubgrupoId = tipoSubgrupoId;
    }
}