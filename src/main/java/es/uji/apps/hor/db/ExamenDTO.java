package es.uji.apps.hor.db;

import org.hibernate.annotations.BatchSize;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * The persistent class for the HOR_EXAMENES database table.
 * 
 */
@Entity
@BatchSize(size=100)
@Table(name = "HOR_EXAMENES")
@SuppressWarnings("serial")
public class ExamenDTO implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String nombre;

    @Column
    private Date fecha;

    @Column(name = "HORA_INICIO")
    private Date horaInicio;

    @Column(name = "HORA_FIN")
    private Date horaFin;

    @Column
    private Boolean comun;

    @Column(name = "PERMITE_SABADOS")
    private Boolean permiteSabados;

    @Column(name = "COMUN_TEXTO")
    private String comunTexto;

    @Column(name = "CONVOCATORIA_ID")
    private Long convocatoriaId;

    @Column(name = "CONVOCATORIA_NOMBRE")
    private String convocatoriaNombre;

    @Column(name = "COMENTARIO")
    private String comentario;

    @Column(name = "COMENTARIO_ES")
    private String comentarioES;

    @Column(name = "COMENTARIO_UK")
    private String comentarioUK;

    @Column(name = "DEFINITIVO")
    private Boolean definitivo;

    @ManyToOne
    @JoinColumn(name = "TIPO_EXAMEN_ID")
    private TipoExamenDTO tipoExamen;

    // bi-directional many-to-one association to ProfesorDTO
    @OneToMany(mappedBy = "examen")
    private Set<ExamenAsignaturaDTO> asignaturasExamen = new HashSet<ExamenAsignaturaDTO>();

    // bi-directional many-to-one association to ExamenAulaDTO
    @OneToMany(mappedBy = "examen")
    private Set<ExamenAulaDTO> aulasExamen = new HashSet<ExamenAulaDTO>();

    public ExamenDTO()
    {
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Date getFecha()
    {
        return fecha;
    }

    public void setFecha(Date fecha)
    {
        this.fecha = fecha;
    }

    public Date getHoraInicio()
    {
        return horaInicio;
    }

    public void setHoraInicio(Date horaInicio)
    {
        this.horaInicio = horaInicio;
    }

    public Date getHoraFin()
    {
        return horaFin;
    }

    public void setHoraFin(Date horaFin)
    {
        this.horaFin = horaFin;
    }

    public Boolean isComun()
    {
        return comun;
    }

    public void setComun(Boolean comun)
    {
        this.comun = comun;
    }

    public String getComunTexto()
    {
        return comunTexto;
    }

    public void setComunTexto(String comunTexto)
    {
        this.comunTexto = comunTexto;
    }

    public Long getConvocatoriaId()
    {
        return convocatoriaId;
    }

    public void setConvocatoriaId(Long convocatoriaId)
    {
        this.convocatoriaId = convocatoriaId;
    }

    public String getConvocatoriaNombre()
    {
        return convocatoriaNombre;
    }

    public void setConvocatoriaNombre(String convocatoriaTexto)
    {
        this.convocatoriaNombre = convocatoriaTexto;
    }

    public Set<ExamenAsignaturaDTO> getAsignaturasExamen()
    {
        return asignaturasExamen;
    }

    public void setAsignaturasExamen(Set<ExamenAsignaturaDTO> asignaturasExamen)
    {
        this.asignaturasExamen = asignaturasExamen;
    }

    public TipoExamenDTO getTipoExamen()
    {
        return tipoExamen;
    }

    public void setTipoExamen(TipoExamenDTO tipoExamen)
    {
        this.tipoExamen = tipoExamen;
    }

    public String getComentario()
    {
        return comentario;
    }

    public void setComentario(String comentario)
    {
        this.comentario = comentario;
    }

    public Boolean getComun()
    {
        return comun;
    }

    public Boolean getPermiteSabados()
    {
        return permiteSabados;
    }

    public void setPermiteSabados(Boolean permiteSabados)
    {
        this.permiteSabados = permiteSabados;
    }

    public Boolean getDefinitivo()
    {
        return definitivo;
    }

    public void setDefinitivo(Boolean definitivo)
    {
        this.definitivo = definitivo;
    }

    public Set<ExamenAulaDTO> getAulasExamen()
    {
        return aulasExamen;
    }

    public void setAulasExamen(Set<ExamenAulaDTO> aulasExamen)
    {
        this.aulasExamen = aulasExamen;
    }

    public String getComentarioES() {
        return comentarioES;
    }

    public void setComentarioES(String comentarioES) {
        this.comentarioES = comentarioES;
    }

    public String getComentarioUK() {
        return comentarioUK;
    }

    public void setComentarioUK(String comentarioUK) {
        this.comentarioUK = comentarioUK;
    }
}