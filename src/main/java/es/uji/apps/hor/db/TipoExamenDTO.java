package es.uji.apps.hor.db;

import org.hibernate.annotations.BatchSize;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

/**
 * The persistent class for the HOR_TIPOS_EXAMENES database table.
 * 
 */
@Entity
@BatchSize(size=100)
@Table(name = "HOR_TIPOS_EXAMENES")
public class TipoExamenDTO implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    private Long id;

    @Column
    private String nombre;

    @Column(name = "POR_DEFECTO")
    private Boolean porDefecto;

    @Column
    private String codigo;

    // bi-directional many-to-one association to EstudioDTO
    @OneToMany(mappedBy = "tipoExamen")
    private Set<ExamenDTO> examenes;

    public TipoExamenDTO()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Boolean getPorDefecto()
    {
        return porDefecto;
    }

    public void setPorDefecto(Boolean porDefecto)
    {
        this.porDefecto = porDefecto;
    }

    public String getCodigo()
    {
        return codigo;
    }

    public void setCodigo(String codigo)
    {
        this.codigo = codigo;
    }

    public Set<ExamenDTO> getExamenes()
    {
        return examenes;
    }

    public void setExamenes(Set<ExamenDTO> examenes)
    {
        this.examenes = examenes;
    }
}