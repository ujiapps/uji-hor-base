package es.uji.apps.hor.db;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the HOR_INFORMACIONES database table.
 * 
 */
@Entity
@Table(name = "HOR_INFORMACIONES")
public class InformacionDTO implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_FIN")
    private Date fechaFin;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_INICIO")
    private Date fechaInicio;

    private Integer orden;

    private String texto;

    public InformacionDTO()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Date getFechaFin()
    {
        return this.fechaFin;
    }

    public void setFechaFin(Date fechaFin)
    {
        this.fechaFin = fechaFin;
    }

    public Date getFechaInicio()
    {
        return this.fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio)
    {
        this.fechaInicio = fechaInicio;
    }

    public Integer getOrden()
    {
        return this.orden;
    }

    public void setOrden(Integer orden)
    {
        this.orden = orden;
    }

    public String getTexto()
    {
        return this.texto;
    }

    public void setTexto(String texto)
    {
        this.texto = texto;
    }
}