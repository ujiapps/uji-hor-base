package es.uji.apps.hor.db;

import java.io.Serializable;

import javax.persistence.*;

/**
 * The persistent class for the HOR_AREAS database table.
 *
 */
@Entity
@Table(name = "HOR_AGRUPACIONES_ESTUDIOS")
public class AgrupacionEstudioDTO implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "AGRUPACION_ID")
    private AgrupacionDTO agrupacion;

    @ManyToOne
    @JoinColumn(name = "ESTUDIO_ID")
    private EstudioDTO estudio;

    public AgrupacionEstudioDTO()
    {
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public AgrupacionDTO getAgrupacion()
    {
        return agrupacion;
    }

    public void setAgrupacion(AgrupacionDTO agrupacion)
    {
        this.agrupacion = agrupacion;
    }

    public EstudioDTO getEstudio()
    {
        return estudio;
    }

    public void setEstudio(EstudioDTO estudio)
    {
        this.estudio = estudio;
    }
}