package es.uji.apps.hor.db;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import org.hibernate.LazyInitializationException;
import org.hibernate.annotations.BatchSize;

/**
 * The persistent class for the HOR_ITEMS_PROFESORES database table.
 * 
 */
@SuppressWarnings("serial")
@Entity
@BatchSize(size = 100)
@Table(name = "HOR_ITEMS_PROF_IDIOMA")
public class ItemProfesorIdiomaDTO implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "ITEM_PROFESOR_ID")
    private ItemProfesorDTO itemProfesor;

    @ManyToOne
    @JoinColumn(name = "IDIOMA_ID")
    private IdiomaDTO idioma;

    public ItemProfesorIdiomaDTO()
    {
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public ItemProfesorDTO getItemProfesor()
    {
        return itemProfesor;
    }

    public void setItemProfesor(ItemProfesorDTO itemProfesor)
    {
        this.itemProfesor = itemProfesor;
    }

    public IdiomaDTO getIdioma()
    {
        return idioma;
    }

    public void setIdioma(IdiomaDTO idioma)
    {
        this.idioma = idioma;
    }
}
