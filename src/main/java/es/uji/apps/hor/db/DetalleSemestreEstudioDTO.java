package es.uji.apps.hor.db;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

/**
 * The persistent class for the HOR_SEMESTRES_DETALLE_ESTUDIO database table.
 * 
 */
@Entity
@Table(name = "HOR_SEMESTRES_DETALLE_ESTUDIO")
public class DetalleSemestreEstudioDTO implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "CURSO_ID")
    private Long cursoAcademicoId;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_INICIO")
    private Date fechaInicio;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_FIN")
    private Date fechaFin;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_EXAMENES_INICIO")
    private Date fechaExamenesInicio;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_EXAMENES_FIN")
    private Date fechaExamenesFin;

    @Column(name = "NUMERO_SEMANAS")
    private Long numeroSemanas;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_EXAMENES_INICIO_C2")
    private Date fechaExamenesInicioC2;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_EXAMENES_FIN_C2")
    private Date fechaExamenesFinC2;

    // bi-directional many-to-one association to TipoEstudioDTO
    @ManyToOne
    @JoinColumn(name = "DETALLE_ID")
    private DetalleSemestreDTO detalleSemestre;

    // bi-directional many-to-one association to TipoEstudioDTO
    @ManyToOne
    @JoinColumn(name = "ESTUDIO_ID")
    private EstudioDTO estudio;

    public DetalleSemestreEstudioDTO()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getCursoAcademicoId()
    {
        return cursoAcademicoId;
    }

    public void setCursoAcademicoId(Long cursoAcademicoId)
    {
        this.cursoAcademicoId = cursoAcademicoId;
    }

    public Date getFechaInicio()
    {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio)
    {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin()
    {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin)
    {
        this.fechaFin = fechaFin;
    }

    public Date getFechaExamenesInicio()
    {
        return fechaExamenesInicio;
    }

    public void setFechaExamenesInicio(Date fechaExamenesInicio)
    {
        this.fechaExamenesInicio = fechaExamenesInicio;
    }

    public Date getFechaExamenesFin()
    {
        return fechaExamenesFin;
    }

    public void setFechaExamenesFin(Date fechaExamenesFin)
    {
        this.fechaExamenesFin = fechaExamenesFin;
    }

    public Long getNumeroSemanas()
    {
        return numeroSemanas;
    }

    public void setNumeroSemanas(Long numeroSemanas)
    {
        this.numeroSemanas = numeroSemanas;
    }

    public Date getFechaExamenesInicioC2()
    {
        return fechaExamenesInicioC2;
    }

    public void setFechaExamenesInicioC2(Date fechaExamenesInicioC2)
    {
        this.fechaExamenesInicioC2 = fechaExamenesInicioC2;
    }

    public Date getFechaExamenesFinC2()
    {
        return fechaExamenesFinC2;
    }

    public void setFechaExamenesFinC2(Date fechaExamenesFinC2)
    {
        this.fechaExamenesFinC2 = fechaExamenesFinC2;
    }

    public DetalleSemestreDTO getDetalleSemestre()
    {
        return detalleSemestre;
    }

    public void setDetalleSemestre(DetalleSemestreDTO detalleSemestre)
    {
        this.detalleSemestre = detalleSemestre;
    }

    public EstudioDTO getEstudio()
    {
        return estudio;
    }

    public void setEstudio(EstudioDTO estudio)
    {
        this.estudio = estudio;
    }
}