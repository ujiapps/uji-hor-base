package es.uji.apps.hor.db;

import java.io.Serializable;

import javax.persistence.*;

/**
 * The persistent class for the HOR_EXAMENES_AULAS database table.
 * 
 */
@Entity
@Table(name = "HOR_EXAMENES_AULAS")
@SuppressWarnings("serial")
public class ExamenAulaDTO implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "EXAMEN_ID")
    private ExamenDTO examen;

    @ManyToOne
    @JoinColumn(name = "AULA_ID")
    private AulaDTO aula;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public ExamenDTO getExamen()
    {
        return examen;
    }

    public void setExamen(ExamenDTO examen)
    {
        this.examen = examen;
    }

    public AulaDTO getAula()
    {
        return aula;
    }

    public void setAula(AulaDTO aula)
    {
        this.aula = aula;
    }
}