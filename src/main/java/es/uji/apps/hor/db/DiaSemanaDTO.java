package es.uji.apps.hor.db;

import org.hibernate.annotations.BatchSize;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the HOR_DIAS_SEMANA database table.
 */
@Entity
@SuppressWarnings("serial")
@BatchSize(size = 100)
@Table(name = "HOR_DIAS_SEMANA")
public class DiaSemanaDTO implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String nombre;

    //bi-directional many-to-one association to ItemDTO
    @OneToMany(mappedBy = "diaSemana")
    private Set<ItemDTO> items;

    public DiaSemanaDTO()
    {
    }

    public DiaSemanaDTO(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Set<ItemDTO> getItems()
    {
        return this.items;
    }

    public void setItems(Set<ItemDTO> items)
    {
        this.items = items;
    }

}