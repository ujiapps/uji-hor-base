package es.uji.apps.hor.db;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The persistent class for the HOR_EXT_ASIGNATURAS_DETALLE database table.
 * 
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "HOR_EXT_ASIGNATURAS_DETALLE")
public class AsignaturaDetalleDTO implements Serializable
{
    @Id
    @Column(name = "ASIGNATURA_ID")
    private String asignaturaId;

    @Column(name = "NOMBRE_ASIGNATURA")
    private String nombre;

    @Column(name = "CREDITOS")
    private BigDecimal creditosAsignatura;

    @Column(name = "CRD_CTOTAL")
    private BigDecimal creditosPod;

    @Column(name = "CRD_CTOTAL_COMPUTABLES")
    private BigDecimal creditosPodComputables;

    @Column(name = "CARACTER")
    private String caracter;

    @Column(name = "CICLO")
    private String ciclo;

    @Column(name = "TIPO")
    private String tipo;

    public AsignaturaDetalleDTO()
    {
    }

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId)
    {
        this.asignaturaId = asignaturaId;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public BigDecimal getCreditosAsignatura()
    {
        return creditosAsignatura;
    }

    public void setCreditosAsignatura(BigDecimal creditosAsignatura)
    {
        this.creditosAsignatura = creditosAsignatura;
    }

    public BigDecimal getCreditosPod()
    {
        return creditosPod;
    }

    public void setCreditosPod(BigDecimal creditosPod)
    {
        this.creditosPod = creditosPod;
    }

    public String getCaracter()
    {
        return caracter;
    }

    public void setCaracter(String caracter)
    {
        this.caracter = caracter;
    }

    public String getCiclo()
    {
        return ciclo;
    }

    public void setCiclo(String ciclo)
    {
        this.ciclo = ciclo;
    }

    public String getTipo()
    {
        return tipo;
    }

    public void setTipo(String tipo)
    {
        this.tipo = tipo;
    }

    public BigDecimal getCreditosPodComputables() {
        return creditosPodComputables;
    }

    public void setCreditosPodComputables(BigDecimal creditosPodComputables) {
        this.creditosPodComputables = creditosPodComputables;
    }
}