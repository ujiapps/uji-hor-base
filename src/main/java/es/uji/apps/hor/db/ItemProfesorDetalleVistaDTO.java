package es.uji.apps.hor.db;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
@Entity
@Table(name = "HOR_V_ITEMS_PROF_DETALLE")
public class ItemProfesorDetalleVistaDTO implements Serializable
{
    @Id
    @Column(name = "ID")
    private String id;

    @Column(name = "DETALLE_ID")
    private Long detalleId;

    @Column(name = "DET_PROFESOR_ID")
    private Long detalleProfesorId;

    @Column(name = "ITEM_ID")
    private Long itemId;

    @Temporal( TemporalType.DATE)
    private Date inicio;

    @Temporal( TemporalType.DATE)
    private Date fin;

    private String descripcion;

    @Column(name = "PROFESOR_ID")
    private Long profesorId;

    @Column(name = "NOMBRE")
    private String profesor;

    @Column(name = "PROFESOR_COMPLETO")
    private String profesorCompleto;

    @Column(name = "DETALLE_MANUAL")
    private Boolean detalleManual;

    private Float creditos;

    @Column(name = "CREDITOS_DETALLE")
    private Float creditosDetalle;

    private String seleccionado;

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public Long getDetalleId()
    {
        return detalleId;
    }

    public void setDetalleId(Long detalleId)
    {
        this.detalleId = detalleId;
    }

    public Long getDetalleProfesorId()
    {
        return detalleProfesorId;
    }

    public void setDetalleProfesorId(Long detalleProfesorId)
    {
        this.detalleProfesorId = detalleProfesorId;
    }

    public Long getItemId()
    {
        return itemId;
    }

    public void setItemId(Long itemId)
    {
        this.itemId = itemId;
    }

    public Date getInicio()
    {
        return inicio;
    }

    public void setInicio(Date inicio)
    {
        this.inicio = inicio;
    }

    public Date getFin()
    {
        return fin;
    }

    public void setFin(Date fin)
    {
        this.fin = fin;
    }

    public String getDescripcion()
    {
        return descripcion;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public Long getProfesorId()
    {
        return profesorId;
    }

    public void setProfesorId(Long profesorId)
    {
        this.profesorId = profesorId;
    }

    public Boolean getDetalleManual()
    {
        return detalleManual;
    }

    public void setDetalleManual(Boolean detalleManual)
    {
        this.detalleManual = detalleManual;
    }

    public Float getCreditos()
    {
        return creditos;
    }

    public void setCreditos(Float creditos)
    {
        this.creditos = creditos;
    }

    public Float getCreditosDetalle()
    {
        return creditosDetalle;
    }

    public void setCreditosDetalle(Float creditosDetalle)
    {
        this.creditosDetalle = creditosDetalle;
    }

    public String getSeleccionado()
    {
        return seleccionado;
    }

    public void setSeleccionado(String seleccionado)
    {
        this.seleccionado = seleccionado;
    }

    public String getProfesor()
    {
        return profesor;
    }

    public void setProfesor(String profesor)
    {
        this.profesor = profesor;
    }

    public String getProfesorCompleto()
    {
        return profesorCompleto;
    }

    public void setProfesorCompleto(String profesorCompleto)
    {
        this.profesorCompleto = profesorCompleto;
    }
}
