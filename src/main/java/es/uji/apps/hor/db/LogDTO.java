package es.uji.apps.hor.db;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the HOR_LOGS database table.
 */
@Entity
@Table(name = "HOR_LOGS")
@SuppressWarnings("serial")
public class LogDTO implements Serializable
{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String descripcion;

    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;

    @Column(name = "ITEM_ID")
    private Long itemId;

    private String usuario;

    @Column(name = "TIPO_ACCION")
    private Long tipoAccion;

    @Column(name = "DIA_SEMANA_ID")
    private Long diaSemana;

    @Column(name = "HORA_FIN")
    private Date horaFin;

    @Column(name = "HORA_INICIO")
    private Date horaInicio;

    @Column(name = "AULA_PLANIFICACION_ID")
    private Long aulaId;

    @Column(name = "DESHECHO")
    private Boolean deshecho;

    public LogDTO()
    {
    }

    public long getId()
    {
        return this.id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public String getDescripcion()
    {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public Date getFecha()
    {
        return this.fecha;
    }

    public void setFecha(Date fecha)
    {
        this.fecha = fecha;
    }

    public Long getItemId()
    {
        return this.itemId;
    }

    public void setItemId(Long itemId)
    {
        this.itemId = itemId;
    }

    public String getUsuario()
    {
        return this.usuario;
    }

    public void setUsuario(String usuario)
    {
        this.usuario = usuario;
    }

    public Long getTipoAccion()
    {
        return tipoAccion;
    }

    public void setTipoAccion(Long tipoAccion)
    {
        this.tipoAccion = tipoAccion;
    }

    public Long getDiaSemana()
    {
        return diaSemana;
    }

    public void setDiaSemana(Long diaSemana)
    {
        this.diaSemana = diaSemana;
    }

    public Date getHoraFin()
    {
        return horaFin;
    }

    public void setHoraFin(Date horaFin)
    {
        this.horaFin = horaFin;
    }

    public Date getHoraInicio()
    {
        return horaInicio;
    }

    public void setHoraInicio(Date horaInicio)
    {
        this.horaInicio = horaInicio;
    }

    public Long getAulaId()
    {
        return aulaId;
    }

    public void setAulaId(Long aulaId)
    {
        this.aulaId = aulaId;
    }

    public Boolean isDeshecho()
    {
        return deshecho;
    }

    public void setDeshecho(Boolean deshecho)
    {
        this.deshecho = deshecho;
    }
}