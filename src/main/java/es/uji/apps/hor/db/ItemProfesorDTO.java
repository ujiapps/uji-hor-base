package es.uji.apps.hor.db;

import org.hibernate.LazyInitializationException;
import org.hibernate.annotations.BatchSize;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the HOR_ITEMS_PROFESORES database table.
 * 
 */
@SuppressWarnings("serial")
@Entity
@BatchSize(size = 100)
@Table(name = "HOR_ITEMS_PROFESORES")
public class ItemProfesorDTO implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "ITEM_ID")
    private ItemDTO item;

    @ManyToOne
    @JoinColumn(name = "PROFESOR_ID")
    private ProfesorDTO profesor;

    @Column(name = "DETALLE_MANUAL")
    private Boolean detalleManual;

    @Column(name = "CREDITOS")
    private BigDecimal creditos;

    @Column(name = "CREDITOS_COMPUTABLES")
    private BigDecimal creditosComputables;

    @OneToMany(mappedBy = "itemProfesor", cascade = CascadeType.ALL)
    private Set<ItemDetalleProfesorDTO> itemsProfesor;

    @OneToMany(mappedBy = "itemProfesor", cascade = CascadeType.ALL)
    private Set<ItemProfesorIdiomaDTO> itemsProfesorIdioma;

    public ItemProfesorDTO()
    {
    }

    public ItemProfesorDTO(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public ItemDTO getItem()
    {
        return item;
    }

    public void setItem(ItemDTO item)
    {
        this.item = item;
    }

    public ProfesorDTO getProfesor()
    {
        return profesor;
    }

    public void setProfesor(ProfesorDTO profesor)
    {
        this.profesor = profesor;
    }

    public Boolean getDetalleManual()
    {
        return detalleManual;
    }

    public void setDetalleManual(Boolean detalleManual)
    {
        this.detalleManual = detalleManual;
    }

    public BigDecimal getCreditos()
    {
        return creditos;
    }

    public void setCreditos(BigDecimal creditos)
    {
        this.creditos = creditos;
    }

    public BigDecimal getCreditosComputables()
    {
        return creditosComputables;
    }

    public void setCreditosComputables(BigDecimal creditosComputables)
    {
        this.creditosComputables = creditosComputables;
    }

    public Set<ItemDetalleProfesorDTO> getItemsProfesor()
    {
        try
        {
            this.itemsProfesor.iterator();
            return this.itemsProfesor;
        }
        catch (LazyInitializationException e)
        {
            return new HashSet<>();
        }
        catch (NullPointerException e)
        {
            return null;
        }
    }

    public void setItemsProfesor(Set<ItemDetalleProfesorDTO> itemsProfesor)
    {
        this.itemsProfesor = itemsProfesor;
    }

    public Set<ItemProfesorIdiomaDTO> getItemsProfesorIdioma()
    {
        return itemsProfesorIdioma;
    }

    public void setItemsProfesorIdioma(Set<ItemProfesorIdiomaDTO> itemsProfesorIdioma)
    {
        this.itemsProfesorIdioma = itemsProfesorIdioma;
    }
}
