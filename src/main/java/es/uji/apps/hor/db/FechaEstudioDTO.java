package es.uji.apps.hor.db;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "HOR_V_FECHAS_ESTUDIOS")
public class FechaEstudioDTO implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    private Long id;

    @Column(name = "ESTUDIO_ID")
    private Long estudioId;

    @Column(name = "ESTUDIO_NOMBRE")
    private String estudioNombre;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_EXAMENES_FIN")
    private Date fechaExamenesFin;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_EXAMENES_INICIO")
    private Date fechaExamenesInicio;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_FIN")
    private Date fechaFin;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_INICIO")
    private Date fechaInicio;

    @Column(name = "CURSO_ID")
    private Long cursoAcademicoId;

    @Column(name = "NUMERO_SEMANAS")
    private Long numeroSemanas;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHAS_EXAMENES_INICIO_C2")
    private Date fechaExamenesInicioC2;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHAS_EXAMENES_FIN_C2")
    private Date fechaExamenesFinC2;

    @ManyToOne
    @JoinColumn(name = "SEMESTRE_ID")
    private SemestreDTO semestre;

    @ManyToOne
    @JoinColumn(name = "TIPO_ESTUDIO_ID")
    private TipoEstudioDTO tiposEstudio;

    public FechaEstudioDTO()
    {
    }

    public Date getFechaExamenesFin()
    {
        return fechaExamenesFin;
    }

    public void setFechaExamenesFin(Date fechaExamenesFin)
    {
        this.fechaExamenesFin = fechaExamenesFin;
    }

    public Date getFechaExamenesInicio()
    {
        return fechaExamenesInicio;
    }

    public void setFechaExamenesInicio(Date fechaExamenesInicio)
    {
        this.fechaExamenesInicio = fechaExamenesInicio;
    }

    public Date getFechaFin()
    {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin)
    {
        this.fechaFin = fechaFin;
    }

    public Date getFechaInicio()
    {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio)
    {
        this.fechaInicio = fechaInicio;
    }

    public Long getCursoAcademicoId()
    {
        return cursoAcademicoId;
    }

    public void setCursoAcademicoId(Long cursoAcademicoId)
    {
        this.cursoAcademicoId = cursoAcademicoId;
    }

    public Date getFechaExamenesInicioC2()
    {
        return fechaExamenesInicioC2;
    }

    public void setFechaExamenesInicioC2(Date fechaExamenesInicioC2)
    {
        this.fechaExamenesInicioC2 = fechaExamenesInicioC2;
    }

    public Date getFechaExamenesFinC2()
    {
        return fechaExamenesFinC2;
    }

    public void setFechaExamenesFinC2(Date fechaExamenesFinC2)
    {
        this.fechaExamenesFinC2 = fechaExamenesFinC2;
    }

    public TipoEstudioDTO getTiposEstudio()
    {
        return tiposEstudio;
    }

    public void setTiposEstudio(TipoEstudioDTO tiposEstudio)
    {
        this.tiposEstudio = tiposEstudio;
    }

    public Long getEstudioId()
    {
        return estudioId;
    }

    public void setEstudioId(Long estudioId)
    {
        this.estudioId = estudioId;
    }

    public String getEstudioNombre()
    {
        return estudioNombre;
    }

    public void setEstudioNombre(String estudioNombre)
    {
        this.estudioNombre = estudioNombre;
    }

    public Long getNumeroSemanas()
    {
        return numeroSemanas;
    }

    public void setNumeroSemanas(Long numeroSemanas)
    {
        this.numeroSemanas = numeroSemanas;
    }

    public SemestreDTO getSemestre()
    {
        return semestre;
    }

    public void setSemestre(SemestreDTO semestre)
    {
        this.semestre = semestre;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }
}