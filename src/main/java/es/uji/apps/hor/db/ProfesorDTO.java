package es.uji.apps.hor.db;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.*;

import org.hibernate.annotations.BatchSize;


/**
 * The persistent class for the HOR_PROFESORES database table.
 * 
 */
@Entity
@BatchSize(size=100)
@Table(name="HOR_PROFESORES")
public class ProfesorDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	@Column(name="DEPARTAMENTO_ID")
	private Long departamentoId;

	private String email;

	private String nombre;

	@Column(name="PENDIENTE_CONTRATACION")
	private Long pendienteContratacion;

    private BigDecimal creditos;

    @Column(name="CREDITOS_MAXIMOS")
    private BigDecimal creditosMaximos;

    @Column(name="CREDITOS_REDUCCION")
    private BigDecimal creditosReduccion;

    @Column(name="CATEGORIA_ID")
    private String categoriaId;

    @Column(name="CATEGORIA_NOMBRE")
    private String nombreCategoria;

    //bi-directional many-to-one association to AreaDTO
    @ManyToOne
	@JoinColumn(name="AREA_ID")
	private AreaDTO area;

    // bi-directional many-to-one association to ItemProfesorDTO
    @OneToMany(mappedBy = "profesor")
    private Set<ItemProfesorDTO> itemsProfesores;

    public ProfesorDTO() {
    }

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getDepartamentoId() {
		return this.departamentoId;
	}

	public void setDepartamentoId(Long departamentoId) {
		this.departamentoId = departamentoId;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Long getPendienteContratacion() {
		return this.pendienteContratacion;
	}

	public void setPendienteContratacion(Long pendienteContratacion) {
		this.pendienteContratacion = pendienteContratacion;
	}

	public AreaDTO getArea() {
		return this.area;
	}

	public void setArea(AreaDTO area) {
		this.area = area;
	}

    public Set<ItemProfesorDTO> getItemsProfesores() {
        return itemsProfesores;
    }

    public void setItemsProfesores(Set<ItemProfesorDTO> itemsProfesores) {
        this.itemsProfesores = itemsProfesores;
    }

    public BigDecimal getCreditos() {
        return creditos;
    }

    public void setCreditos(BigDecimal creditos) {
        this.creditos = creditos;
    }

    public BigDecimal getCreditosMaximos() {
        return creditosMaximos;
    }

    public void setCreditosMaximos(BigDecimal creditosMaximos) {
        this.creditosMaximos = creditosMaximos;
    }

    public BigDecimal getCreditosReduccion() {
        return creditosReduccion;
    }

    public void setCreditosReduccion(BigDecimal creditosReduccion) {
        this.creditosReduccion = creditosReduccion;
    }

    public String getCategoriaId() {
        return categoriaId;
    }

    public void setCategoriaId(String categoriaId) {
        this.categoriaId = categoriaId;
    }

    public String getNombreCategoria() {
        return nombreCategoria;
    }

    public void setNombreCategoria(String nombreCategoria) {
        this.nombreCategoria = nombreCategoria;
    }
}
