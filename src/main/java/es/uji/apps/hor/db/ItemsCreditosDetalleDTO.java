package es.uji.apps.hor.db;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.*;


/**
 * The persistent class for the HOR_V_GRUPOS database table.
 * 
 */
@Entity
@Table(name="HOR_V_ITEMS_CREDITOS_DETALLE")
public class ItemsCreditosDetalleDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	private String nombre;

    @Column(name = "CREDITOS_PROFESOR")
    private BigDecimal creditosProfesor;

    @Id
    @ManyToOne
    @JoinColumn(name = "ITEM_ID")
    private ItemDTO item;

    @Column(name = "CRD_PROF")
    private BigDecimal crdProf;

    @Column(name = "CRD_ITEM")
    private BigDecimal crdItem;

    private BigDecimal cuantos;

    @Column(name = "CRD_FINAL")
    private BigDecimal crdFinal;

    @Column(name = "CRD_PACTADOS_FINAL")
    private BigDecimal crdComputablesFinal;

    @Column(name = "GRUPO_ID")
    private String grupoId;

    private String tipo;

    @Column(name = "ASIGNATURA_ID")
    private String asignaturaId;

    @Column(name = "ASIGNATURA_TXT")
    private String asignaturaTxt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public BigDecimal getCreditosProfesor() {
        return creditosProfesor;
    }

    public void setCreditosProfesor(BigDecimal creditosProfesor) {
        this.creditosProfesor = creditosProfesor;
    }

    public ItemDTO getItem() {
        return item;
    }

    public void setItem(ItemDTO item) {
        this.item = item;
    }

    public BigDecimal getCrdProf() {
        return crdProf;
    }

    public void setCrdProf(BigDecimal crdProf) {
        this.crdProf = crdProf;
    }

    public BigDecimal getCrdItem() {
        return crdItem;
    }

    public void setCrdItem(BigDecimal crdItem) {
        this.crdItem = crdItem;
    }

    public BigDecimal getCuantos() {
        return cuantos;
    }

    public void setCuantos(BigDecimal cuantos) {
        this.cuantos = cuantos;
    }

    public BigDecimal getCrdFinal() {
        return crdFinal;
    }

    public void setCrdFinal(BigDecimal crdFinal) {
        this.crdFinal = crdFinal;
    }

    public String getGrupoId() {
        return grupoId;
    }

    public void setGrupoId(String grupoId) {
        this.grupoId = grupoId;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getAsignaturaId() {
        return asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId) {
        this.asignaturaId = asignaturaId;
    }

    public String getAsignaturaTxt() {
        return asignaturaTxt;
    }

    public void setAsignaturaTxt(String asignaturaTxt) {
        this.asignaturaTxt = asignaturaTxt;
    }

    public BigDecimal getCrdComputablesFinal() {
        return crdComputablesFinal;
    }

    public void setCrdComputablesFinal(BigDecimal crdComputablesFinal) {
        this.crdComputablesFinal = crdComputablesFinal;
    }
}
