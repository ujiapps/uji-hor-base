package es.uji.apps.hor.db;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * The persistent class for the HOR_EXAMENES_ASIGNATURAS database table.
 * 
 */
@Entity
@Table(name = "HOR_EXAMENES_ASIGNATURAS")
@SuppressWarnings("serial")
public class ExamenAsignaturaDTO implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "ASIGNATURA_ID")
    private String asignaturaId;

    @Column(name = "ASIGNATURA_NOMBRE")
    private String asignaturaNombre;

    @Column
    private Long semestre;

    @Column(name = "TIPO_ASIGNATURA")
    private String tipoAsignatura;

    @Column(name = "ESTUDIO_NOMBRE")
    private String estudioNombre;

    @ManyToOne
    @JoinColumn(name = "EXAMEN_ID")
    private ExamenDTO examen;

    @ManyToOne
    @JoinColumn(name = "ESTUDIO_ID")
    private EstudioDTO estudio;

    @Column(name = "CENTRO_ID")
    private Long centroId;

    @Column(name = "CURSO_ID")
    private Long cursoId;

    public ExamenAsignaturaDTO()
    {
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId)
    {
        this.asignaturaId = asignaturaId;
    }

    public String getAsignaturaNombre()
    {
        return asignaturaNombre;
    }

    public void setAsignaturaNombre(String asignaturaNombre)
    {
        this.asignaturaNombre = asignaturaNombre;
    }

    public Long getSemestre()
    {
        return semestre;
    }

    public void setSemestre(Long semestre)
    {
        this.semestre = semestre;
    }

    public String getTipoAsignatura()
    {
        return tipoAsignatura;
    }

    public void setTipoAsignatura(String tipoAsignatura)
    {
        this.tipoAsignatura = tipoAsignatura;
    }

    public String getEstudioNombre()
    {
        return estudioNombre;
    }

    public void setEstudioNombre(String estudioNombre)
    {
        this.estudioNombre = estudioNombre;
    }

    public ExamenDTO getExamen()
    {
        return examen;
    }

    public void setExamen(ExamenDTO examen)
    {
        this.examen = examen;
    }

    public EstudioDTO getEstudio()
    {
        return estudio;
    }

    public void setEstudio(EstudioDTO estudio)
    {
        this.estudio = estudio;
    }

    public Long getCentroId() {
        return centroId;
    }

    public void setCentroId(Long centroId) {
        this.centroId = centroId;
    }

    public Long getCursoId() {
        return cursoId;
    }

    public void setCursoId(Long cursoId) {
        this.cursoId = cursoId;
    }
}