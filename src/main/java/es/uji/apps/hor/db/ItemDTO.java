package es.uji.apps.hor.db;

import org.hibernate.LazyInitializationException;
import org.hibernate.annotations.BatchSize;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * The persistent class for the HOR_ITEMS database table.
 * 
 */
@Entity
@BatchSize(size = 100)
@Table(name = "HOR_ITEMS")
@SuppressWarnings("serial")
public class ItemDTO implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Long comun;

    @Temporal(TemporalType.DATE)
    @Column(name = "DESDE_EL_DIA")
    private Date desdeElDia;

    @Column(name = "GRUPO_ID")
    private String grupoId;

    @Temporal(TemporalType.DATE)
    @Column(name = "HASTA_EL_DIA")
    private Date hastaElDia;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "HORA_FIN")
    private Date horaFin;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "HORA_INICIO")
    private Date horaInicio;

    private Long plazas;

    @Column(name = "PORCENTAJE_COMUN")
    private Long porcentajeComun;

    @Column(name = "SUBGRUPO_ID")
    private Long subgrupoId;

    @Column(name = "TIPO_ASIGNATURA")
    private String tipoAsignatura;

    @Column(name = "TIPO_ASIGNATURA_ID")
    private String tipoAsignaturaId;

    @Column(name = "TIPO_ESTUDIO")
    private String tipoEstudio;

    @Column(name = "TIPO_ESTUDIO_ID")
    private String tipoEstudioId;

    @Column(name = "TIPO_SUBGRUPO")
    private String tipoSubgrupo;

    @Column(name = "TIPO_SUBGRUPO_ID")
    private String tipoSubgrupoId;

    // bi-directional many-to-one association to AulaPlanificacionDTO
    @ManyToOne
    @JoinColumn(name = "AULA_PLANIFICACION_ID")
    private AulaDTO aula;

    @Column(name = "AULA_PLANIFICACION_NOMBRE")
    private String aulaNombre;

    // bi-directional many-to-one association to DiaSemanaDTO
    @ManyToOne
    @JoinColumn(name = "DIA_SEMANA_ID")
    private DiaSemanaDTO diaSemana;

    // bi-directional many-to-one association to SemestreDTO
    @ManyToOne
    @JoinColumn(name = "SEMESTRE_ID")
    private SemestreDTO semestre;

    // bi-directional many-to-one association to ItemCircuitoDTO
    @OneToMany(mappedBy = "item", cascade = CascadeType.ALL)
    private Set<ItemCircuitoDTO> itemsCircuitos;

    // bi-directional many-to-one association to ProfesorDTO
    @OneToMany(mappedBy = "item", cascade = CascadeType.REMOVE)
    private Set<ItemProfesorDTO> itemsProfesores;

    // bi-directional many-to-one association to ItemsAsignaturaDTO
    @OneToMany(mappedBy = "item", cascade = CascadeType.REMOVE)
    private Set<ItemsAsignaturaDTO> itemsAsignaturas = new HashSet<ItemsAsignaturaDTO>();

    // bi-directional many-to-one association to ItemDetalleDTO
    @OneToMany(mappedBy = "item")
    private Set<ItemDetalleDTO> itemsDetalles;

    // bi-directional many-to-one association to ItemAgrupacionDTO
    @OneToMany(mappedBy = "item")
    private Set<ItemAgrupacionDTO> itemsAgrupacion;

    // bi-directional many-to-one association to ItemsCreditosDetalleDTO
    @OneToMany(mappedBy = "item")
    private Set<ItemsCreditosDetalleDTO> itemsCreditosDetalle;

    @Column(name = "DETALLE_MANUAL")
    private Boolean detalleManual;

    @Column(name = "NUMERO_ITERACIONES")
    private Integer numeroIteraciones;

    @Column(name = "REPETIR_CADA_SEMANAS")
    private Integer repetirCadaSemanas;

    @Column(name = "COMUN_TEXTO")
    private String comunes;

    @Column(name = "CREDITOS")
    private BigDecimal creditos;

    @Column(name = "CREDITOS_COMPUTABLES")
    private BigDecimal creditosComputables;

    @Column(name = "COMENTARIOS")
    private String comentarios;

    @Column(name = "NOMBRE_ITEM")
    private String nombreItem;

    public ItemDTO()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getComun()
    {
        return this.comun;
    }

    public void setComun(Long comun)
    {
        this.comun = comun;
    }

    public Date getDesdeElDia()
    {
        return this.desdeElDia;
    }

    public void setDesdeElDia(Date desdeElDia)
    {
        this.desdeElDia = desdeElDia;
    }

    public String getGrupoId()
    {
        return this.grupoId;
    }

    public void setGrupoId(String grupoId)
    {
        this.grupoId = grupoId;
    }

    public Date getHastaElDia()
    {
        return this.hastaElDia;
    }

    public void setHastaElDia(Date hastaElDia)
    {
        this.hastaElDia = hastaElDia;
    }

    public Date getHoraFin()
    {
        return this.horaFin;
    }

    public void setHoraFin(Date horaFin)
    {
        this.horaFin = horaFin;
    }

    public Date getHoraInicio()
    {
        return this.horaInicio;
    }

    public void setHoraInicio(Date horaInicio)
    {
        this.horaInicio = horaInicio;
    }

    public Long getPlazas()
    {
        return this.plazas;
    }

    public void setPlazas(Long plazas)
    {
        this.plazas = plazas;
    }

    public Long getPorcentajeComun()
    {
        return this.porcentajeComun;
    }

    public void setPorcentajeComun(Long porcentajeComun)
    {
        this.porcentajeComun = porcentajeComun;
    }

    public Long getSubgrupoId()
    {
        return this.subgrupoId;
    }

    public void setSubgrupoId(Long subgrupoId)
    {
        this.subgrupoId = subgrupoId;
    }

    public String getTipoAsignatura()
    {
        return this.tipoAsignatura;
    }

    public void setTipoAsignatura(String tipoAsignatura)
    {
        this.tipoAsignatura = tipoAsignatura;
    }

    public String getTipoAsignaturaId()
    {
        return this.tipoAsignaturaId;
    }

    public void setTipoAsignaturaId(String tipoAsignaturaId)
    {
        this.tipoAsignaturaId = tipoAsignaturaId;
    }

    public String getTipoEstudio()
    {
        return this.tipoEstudio;
    }

    public void setTipoEstudio(String tipoEstudio)
    {
        this.tipoEstudio = tipoEstudio;
    }

    public String getTipoEstudioId()
    {
        return this.tipoEstudioId;
    }

    public void setTipoEstudioId(String tipoEstudioId)
    {
        this.tipoEstudioId = tipoEstudioId;
    }

    public String getTipoSubgrupo()
    {
        return this.tipoSubgrupo;
    }

    public void setTipoSubgrupo(String tipoSubgrupo)
    {
        this.tipoSubgrupo = tipoSubgrupo;
    }

    public String getTipoSubgrupoId()
    {
        return this.tipoSubgrupoId;
    }

    public void setTipoSubgrupoId(String tipoSubgrupoId)
    {
        this.tipoSubgrupoId = tipoSubgrupoId;
    }

    public String getAulaNombre()
    {
        return aulaNombre;
    }

    public void setAulaNombre(String aulaNombre)
    {
        this.aulaNombre = aulaNombre;
    }

    public DiaSemanaDTO getDiaSemana()
    {
        return this.diaSemana;
    }

    public void setDiaSemana(DiaSemanaDTO diaSemana)
    {
        this.diaSemana = diaSemana;
    }

    public SemestreDTO getSemestre()
    {
        return this.semestre;
    }

    public void setSemestre(SemestreDTO semestre)
    {
        this.semestre = semestre;
    }

    public Set<ItemCircuitoDTO> getItemsCircuitos()
    {
        return this.itemsCircuitos;
    }

    public void setItemsCircuitos(Set<ItemCircuitoDTO> itemsCircuitos)
    {
        this.itemsCircuitos = itemsCircuitos;
    }

    public Set<ItemDetalleDTO> getItemsDetalles()
    {
        try
        {
            this.itemsDetalles.iterator();
            return this.itemsDetalles;
        }
        catch (LazyInitializationException e)
        {
            return new HashSet<>();
        }
        catch (NullPointerException e)
        {
            return new HashSet<>();
        }
    }

    public void setItemsDetalles(Set<ItemDetalleDTO> itemsDetalles)
    {
        this.itemsDetalles = itemsDetalles;
    }

    public Boolean getDetalleManual()
    {
        return detalleManual;
    }

    public void setDetalleManual(Boolean detalleManual)
    {
        this.detalleManual = detalleManual;
    }

    public Integer getNumeroIteraciones()
    {
        return numeroIteraciones;
    }

    public void setNumeroIteraciones(Integer numeroIteraciones)
    {
        this.numeroIteraciones = numeroIteraciones;
    }

    public Integer getRepetirCadaSemanas()
    {
        return repetirCadaSemanas;
    }

    public void setRepetirCadaSemanas(Integer repetirCadaSemanas)
    {
        this.repetirCadaSemanas = repetirCadaSemanas;
    }

    public String getComunes()
    {
        return comunes;
    }

    public void setComunes(String comunes)
    {
        this.comunes = comunes;
    }

    public AulaDTO getAula()
    {
        return aula;
    }

    public void setAula(AulaDTO aula)
    {
        this.aula = aula;
    }

    public Set<ItemProfesorDTO> getItemsProfesores()
    {
        try
        {
            this.itemsProfesores.iterator();
            return this.itemsProfesores;
        }
        catch (LazyInitializationException e)
        {
            return new HashSet<>();
        }
        catch (NullPointerException e)
        {
            return null;
        }
    }

    public void setItemsProfesores(Set<ItemProfesorDTO> itemsProfesores)
    {
        this.itemsProfesores = itemsProfesores;
    }

    public Set<ItemsAsignaturaDTO> getItemsAsignaturas()
    {
        try
        {
            this.itemsAsignaturas.iterator();
            return this.itemsAsignaturas;
        }
        catch (LazyInitializationException e)
        {
            return new HashSet<>();
        }
    }

    public void setItemsAsignaturas(Set<ItemsAsignaturaDTO> itemsAsignaturas)
    {
        this.itemsAsignaturas = itemsAsignaturas;
    }

    public Set<ItemsCreditosDetalleDTO> getItemsCreditosDetalle()
    {
        try
        {
            this.itemsCreditosDetalle.iterator();
            return this.itemsCreditosDetalle;
        }
        catch (LazyInitializationException e)
        {
            return new HashSet<>();
        }
    }

    public void setItemsCreditosDetalle(Set<ItemsCreditosDetalleDTO> itemsCreditosDetalle)
    {
        this.itemsCreditosDetalle = itemsCreditosDetalle;
    }

    public BigDecimal getCreditos()
    {
        return creditos;
    }

    public void setCreditos(BigDecimal creditos)
    {
        this.creditos = creditos;
    }

    public BigDecimal getCreditosComputables()
    {
        return creditosComputables;
    }

    public void setCreditosComputables(BigDecimal creditosComputables)
    {
        this.creditosComputables = creditosComputables;
    }

    public String getComentarios()
    {
        return comentarios;
    }

    public void setComentarios(String comentarios)
    {
        this.comentarios = comentarios;
    }

    public Set<ItemAgrupacionDTO> getItemsAgrupacion()
    {
        return itemsAgrupacion;
    }

    public void setItemsAgrupacion(Set<ItemAgrupacionDTO> itemsAgrupacion)
    {
        this.itemsAgrupacion = itemsAgrupacion;
    }

    public String getNombreItem() {
        return nombreItem;
    }

    public void setNombreItem(String nombreItem) {
        this.nombreItem = nombreItem;
    }
}
