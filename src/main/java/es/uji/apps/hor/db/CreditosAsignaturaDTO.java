package es.uji.apps.hor.db;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * The persistent class for the HOR_V_CREDITOS_ASIGNATURAS database table.
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "HOR_V_CREDITOS_ASIGNATURAS")
public class CreditosAsignaturaDTO implements Serializable {
    @Id
    @Column(name = "ASIGNATURA_ID")
    private String asignaturaId;

    @Column(name = "COMUN_TEXTO")
    private String comunTexto;
    @Column(name = "CREDITOS_TOTALES")
    private BigDecimal creditosPod;

    @Column(name = "CREDITOS_COMPUTABLES_TOTALES")
    private BigDecimal creditosPodComputables;

    @Column(name = "PREFIJO")
    private String prefijo;

    @Column(name = "PORCENTAJE")
    private BigDecimal porcentaje;

    public CreditosAsignaturaDTO() {
    }

    public String getAsignaturaId() {
        return asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId) {
        this.asignaturaId = asignaturaId;
    }

    public String getComunTexto() {
        return comunTexto;
    }

    public void setComunTexto(String comunTexto) {
        this.comunTexto = comunTexto;
    }

    public BigDecimal getCreditosPod() {
        return creditosPod;
    }

    public void setCreditosPod(BigDecimal creditosPod) {
        this.creditosPod = creditosPod;
    }

    public BigDecimal getCreditosPodComputables() {
        return creditosPodComputables;
    }

    public void setCreditosPodComputables(BigDecimal creditosPodComputables) {
        this.creditosPodComputables = creditosPodComputables;
    }

    public String getPrefijo() {
        return prefijo;
    }

    public void setPrefijo(String prefijo) {
        this.prefijo = prefijo;
    }

    public BigDecimal getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(BigDecimal porcentaje) {
        this.porcentaje = porcentaje;
    }
}