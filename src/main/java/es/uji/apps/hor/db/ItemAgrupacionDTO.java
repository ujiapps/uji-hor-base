package es.uji.apps.hor.db;

import java.io.Serializable;

import javax.persistence.*;

/**
 * The persistent class for the HOR_V_AGRUPACIONES_ITEMS database table.
 * 
 */
@Entity
@Table(name = "HOR_V_AGRUPACIONES_ITEMS")
@SuppressWarnings("serial")
public class ItemAgrupacionDTO implements Serializable
{
    @Id
    private Long id;

    @Column(name = "NOMBRE_AGRUPACION")
    private String nombreAgrupacion;

    @Column(name = "ASIGNATURA_ID")
    private String asignaturaId;

    @Column(name = "CURSO_ID")
    private Long cursoId;

    @Column(name = "CARACTER_ID")
    private String caracterId;

    // bi-directional many-to-one association to EstudioDTO
    @ManyToOne
    @JoinColumn(name = "AGRUPACION_ID")
    private AgrupacionDTO agrupacion;

    // bi-directional many-to-one association to EstudioDTO
    @ManyToOne
    @JoinColumn(name = "ESTUDIO_ID")
    private EstudioDTO estudio;

    // bi-directional many-to-one association to ItemDTO
    @ManyToOne
    @JoinColumn(name = "ITEM_ID")
    private ItemDTO item;

    public ItemAgrupacionDTO()
    {
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombreAgrupacion()
    {
        return nombreAgrupacion;
    }

    public void setNombreAgrupacion(String nombreAgrupacion)
    {
        this.nombreAgrupacion = nombreAgrupacion;
    }

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId)
    {
        this.asignaturaId = asignaturaId;
    }

    public Long getCursoId()
    {
        return cursoId;
    }

    public void setCursoId(Long cursoId)
    {
        this.cursoId = cursoId;
    }

    public String getCaracterId()
    {
        return caracterId;
    }

    public void setCaracterId(String caracterId)
    {
        this.caracterId = caracterId;
    }

    public AgrupacionDTO getAgrupacion()
    {
        return agrupacion;
    }

    public void setAgrupacion(AgrupacionDTO agrupacion)
    {
        this.agrupacion = agrupacion;
    }

    public EstudioDTO getEstudio()
    {
        return estudio;
    }

    public void setEstudio(EstudioDTO estudio)
    {
        this.estudio = estudio;
    }

    public ItemDTO getItem()
    {
        return item;
    }

    public void setItem(ItemDTO item)
    {
        this.item = item;
    }
}
