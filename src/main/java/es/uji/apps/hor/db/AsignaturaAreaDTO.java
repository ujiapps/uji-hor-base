package es.uji.apps.hor.db;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * The persistent class for the HOR_V_ASIGNATURAS_AREA database table.
 * 
 */
@Entity
@Table(name="HOR_V_ASIGNATURAS_AREA")
public class AsignaturaAreaDTO implements Serializable {
	private static final long serialVersionUID = 1L;

    @Id
	@Column(name="AREA_ID")
	private Long areaId;

    @Id
    @Column(name="ASIGNATURA_ID")
	private String asignaturaId;

	private Long porcentaje;

	@Column(name="RECIBE_ACTA")
	private String recibeActa;

	public AsignaturaAreaDTO() {
	}

	public Long getAreaId() {
		return this.areaId;
	}

	public void setAreaId(Long areaId) {
		this.areaId = areaId;
	}

	public String getAsignaturaId() {
		return this.asignaturaId;
	}

	public void setAsignaturaId(String asignaturaId) {
		this.asignaturaId = asignaturaId;
	}

	public Long getPorcentaje() {
		return this.porcentaje;
	}

	public void setPorcentaje(Long porcentaje) {
		this.porcentaje = porcentaje;
	}

	public String getRecibeActa() {
		return this.recibeActa;
	}

	public void setRecibeActa(String recibeActa) {
		this.recibeActa = recibeActa;
	}

}
