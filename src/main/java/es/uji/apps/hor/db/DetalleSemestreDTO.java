package es.uji.apps.hor.db;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the HOR_SEMESTRES_DETALLE database table.
 * 
 */
@Entity
@Table(name="HOR_SEMESTRES_DETALLE")
public class DetalleSemestreDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

    @Temporal( TemporalType.DATE)
	@Column(name="FECHA_EXAMENES_FIN")
	private Date fechaExamenesFin;

    @Temporal( TemporalType.DATE)
	@Column(name="FECHA_EXAMENES_INICIO")
	private Date fechaExamenesInicio;

    @Temporal( TemporalType.DATE)
	@Column(name="FECHA_FIN")
	private Date fechaFin;

    @Temporal( TemporalType.DATE)
	@Column(name="FECHA_INICIO")
	private Date fechaInicio;

	@Column(name="NUMERO_SEMANAS")
	private Long numeroSemanas;

    @Column(name="CURSO_ACADEMICO_ID")
    private Long cursoAcademicoId;

    @Temporal( TemporalType.DATE)
    @Column(name="FECHAS_EXAMENES_INICIO_C2")
    private Date fechaExamenesInicioC2;

    @Temporal( TemporalType.DATE)
    @Column(name="FECHAS_EXAMENES_FIN_C2")
    private Date fechaExamenesFinC2;

    //bi-directional many-to-one association to SemestreDTO
    @ManyToOne
	@JoinColumn(name="SEMESTRE_ID")
	private SemestreDTO semestre;

	//bi-directional many-to-one association to TipoEstudioDTO
    @ManyToOne
	@JoinColumn(name="TIPO_ESTUDIO_ID")
	private TipoEstudioDTO tiposEstudio;

    public DetalleSemestreDTO() {
    }

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getFechaExamenesFin() {
		return this.fechaExamenesFin;
	}

	public void setFechaExamenesFin(Date fechaExamenesFin) {
		this.fechaExamenesFin = fechaExamenesFin;
	}

	public Date getFechaExamenesInicio() {
		return this.fechaExamenesInicio;
	}

	public void setFechaExamenesInicio(Date fechaExamenesInicio) {
		this.fechaExamenesInicio = fechaExamenesInicio;
	}

	public Date getFechaFin() {
		return this.fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public Date getFechaInicio() {
		return this.fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Long getNumeroSemanas() {
		return this.numeroSemanas;
	}

	public void setNumeroSemanas(Long numeroSemanas) {
		this.numeroSemanas = numeroSemanas;
	}

	public SemestreDTO getSemestre() {
		return this.semestre;
	}

	public void setSemestre(SemestreDTO semestre) {
		this.semestre = semestre;
	}
	
	public TipoEstudioDTO getTiposEstudio() {
		return this.tiposEstudio;
	}

	public void setTiposEstudio(TipoEstudioDTO tiposEstudio) {
		this.tiposEstudio = tiposEstudio;
	}

    public Long getCursoAcademicoId() {
        return cursoAcademicoId;
    }

    public void setCursoAcademicoId(Long cursoAcademicoId) {
        this.cursoAcademicoId = cursoAcademicoId;
    }

    public Date getFechaExamenesInicioC2() {
        return fechaExamenesInicioC2;
    }

    public void setFechaExamenesInicioC2(Date fechaExamenesInicioC2) {
        this.fechaExamenesInicioC2 = fechaExamenesInicioC2;
    }

    public Date getFechaExamenesFinC2() {
        return fechaExamenesFinC2;
    }

    public void setFechaExamenesFinC2(Date fechaExamenesFinC2) {
        this.fechaExamenesFinC2 = fechaExamenesFinC2;
    }
}