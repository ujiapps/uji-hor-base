package es.uji.apps.hor;

@SuppressWarnings("serial")
public class DiaNoValidoExamenException extends GeneralHORException {
    public DiaNoValidoExamenException() {
        super("No es pot posar un examen en festiu o fora del període d'exàmens");
    }
    public DiaNoValidoExamenException(String msg)
    {
        super(msg);
    }
}
