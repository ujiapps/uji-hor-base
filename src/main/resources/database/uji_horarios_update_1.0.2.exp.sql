CREATE OR REPLACE VIEW uji_horarios.hor_v_cursos  AS
SELECT DISTINCT hor_items_asignaturas.estudio_id,
  hor_items_asignaturas.estudio,
  hor_items.curso_id
FROM hor_items_asignaturas,
  hor_items
WHERE hor_items.id      = hor_items_asignaturas.item_id
AND (hor_items.curso_id < 7) ;



CREATE OR REPLACE FORCE VIEW UJI_HORARIOS.HOR_EXT_CARGOS_PER (ID,
                                                              PERSONA_ID,
                                                              NOMBRE,
                                                              CENTRO_ID,
                                                              CENTRO,
                                                              ESTUDIO_ID,
                                                              ESTUDIO,
                                                              CARGO_ID,
                                                              CARGO
                                                             ) AS
   select rownum id, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, TITULACION_ID estudio_id, TITULACION estudio, CARGO_ID,
          CARGO
   from   (select   cp.per_id persona_id, p.nombre || ' ' || apellido1 || ' ' || apellido2 nombre, ulogica_id centro_id,
                    ubic.nombre centro, tit.id titulacion_id, tit.nombre titulacion, tc.id cargo_id, tc.nombre cargo
           from     grh_grh.grh_cargos_per cp,
                    gri_est.est_ubic_estructurales ubic,
                    gra_Exp.exp_v_titu_todas tit,
                    uji_horarios.hor_tipos_Cargos tc,
                    gri_per.per_personas p
           where    (    f_fin is null
                     and (   f_fin is null
                          or f_fin >= sysdate)
                     and crg_id in (189, 195, 188, 30))
           and      ulogica_id = ubic.id
           and      ulogica_id = tit.uest_id
           and      tit.activa = 'S'
           and      (tit.tipo = 'G' or tit.id in (9,26,31))
           and      tc.id = 3
           and      cp.per_id = p.id
           union all
/* PAS de centro */
           select   cp.per_id persona_id, p.nombre || ' ' || apellido1 || ' ' || apellido2 nombre,
                    ubicacion_id centro_id, ubic.nombre centro, tit.id titulacion_id, tit.nombre titulacion,
                    tc.id cargo_id, tc.nombre cargo
           from     grh_grh.grh_vw_contrataciones_ult cp,
                    gri_est.est_ubic_estructurales ubic,
                    gra_Exp.exp_v_titu_todas tit,
                    uji_horarios.hor_tipos_Cargos tc,
                    gri_per.per_personas p
           where    ubicacion_id = ubic.id
           and      ubicacion_id = tit.uest_id
           and      tit.activa = 'S'
           and      (tit.tipo = 'G' or tit.id in (9,26,31))
           and      tc.id = 4
           and      cp.per_id = p.id
           union all
/* directores de titulacion */
           select   cp.per_id persona_id, p.nombre || ' ' || apellido1 || ' ' || apellido2 nombre, ulogica_id centro_id,
                    ubic.nombre centro, tit.id titulacion_id, tit.nombre titulacion, tc.id cargo_id, tc.nombre cargo
           from     grh_grh.grh_cargos_per cp,
                    gri_est.est_ubic_estructurales ubic,
                    gra_Exp.exp_v_titu_todas tit,
                    uji_horarios.hor_tipos_Cargos tc,
                    gri_per.per_personas p
           where    (    f_fin is null
                     and (   f_fin is null
                          or f_fin >= sysdate)
                     and crg_id in (192, 193))
           and      ulogica_id = ubic.id
           and      ulogica_id = tit.uest_id
           and      tit.activa = 'S'
           and      (tit.tipo = 'G' or tit.id in (9,26,31))
           and      tc.id = 1
           and      cp.per_id = p.id
           and      cp.tit_id = tit.id
           union all
/* permisos extra */
           select   per.persona_id, p.nombre || ' ' || apellido1 || ' ' || apellido2 nombre, ubic.id centro_id,
                    ubic.nombre centro, tit.id titulacion_id, tit.nombre titulacion, tc.id cargo_id, tc.nombre cargo
           from     uji_horarios.hor_permisos_extra per,
                    gri_per.per_personas p,
                    gra_Exp.exp_v_titu_todas tit,
                    uji_horarios.hor_tipos_cargos tc,
                    gri_est.est_ubic_estructurales ubic
           where    persona_id = p.id
           and      estudio_id = tit.id
           and      tipo_Cargo_id = tc.id
           and      tit.activa = 'S'
           and      (tit.tipo = 'G' or tit.id in (9,26,31))
           and      tit.uest_id = ubic.id
/* administradores */
           union all
           select distinct per.persona_id, p.nombre || ' ' || apellido1 || ' ' || apellido2 nombre, u.id centro_id,
                           u.nombre centro, tit.id titulacion_id, tit.nombre titulacion, tc.id cargo_id,
                           tc.nombre cargo
           from            uji_apa.apa_vw_personas_items per,
                           gri_per.per_personas p,
                           gra_Exp.exp_v_titu_todas tit,
                           uji_horarios.hor_tipos_cargos tc,
                           gri_Est.est_ubic_estructurales u
           where           persona_id = p.id
           and             tc.id = 1
           and             tit.activa = 'S'
           and      (tit.tipo = 'G' or tit.id in (9,26,31))
           and             aplicacion_id = 46
           and             role = 'ADMIN'
           and             tit.uest_id = u.id
           order by        titulacion_id);


CREATE OR REPLACE VIEW uji_horarios.hor_v_aulas_personas  AS
SELECT DISTINCT c.persona_id,
  ap.aula_id,
  a.nombre,
  a.centro_id,
  cen.nombre centro,
  a.codigo,
  a.tipo
FROM hor_ext_cargos_per c,
  hor_aulas_planificacion ap,
  hor_aulas a,
  hor_centros cen
WHERE c.estudio_id = ap.estudio_id
AND ap.aula_id     = a.id
AND a.centro_id    = cen.id ;



CREATE OR REPLACE PROCEDURE UJI_HORARIOS.HORARIOS_SEMANA_PDF (
   name_array    IN   euji_util.ident_arr2,
   value_array   IN   euji_util.ident_arr2
) IS
   -- Parametros
   pEstudio    CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pEstudio');
   pSemestre   CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pSemestre');
   pSesion     CONSTANT VARCHAR2 (4000) := euji_util.euji_getparam (name_array, value_array, 'pSesion');
   pGrupo      CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pGrupo');
   pCurso      CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pCurso');
   pSemana     CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pSemana');
   vCursoAcaNum         number;
   vSesion              number;
   vFechaIniSemestre    date;
   vFechaFinSemestre    date;
   vEsGenerica          boolean         := pSemana = 'G';

     -- Variables
   --  vPerId              per_personas.id%TYPE := euji_get_perid ();
   CURSOR c_items_gen IS
      SELECT asignatura_id, asignatura, estudio, caracter, semestre_id, comun, grupo_id, tipo_subgrupo,
             tipo_subgrupo_id, subgrupo_id, dia_semana_id, hora_inicio, hora_fin, tipo_asignatura_id, tipo_asignatura,
             to_date (dia_semana_id + 1 || '/01/2006 ' || to_char (hora_fin, 'hh24:mi'), 'dd/mm/yyyy hh24:mi') fin,
             to_date (dia_semana_id + 1 || '/01/2006 ' || to_char (hora_inicio, 'hh24:mi'),
                      'dd/mm/yyyy hh24:mi') inicio
      FROM   UJI_HORARIOS.HOR_ITEMS items,
             uji_horarios.hor_items_asignaturas asi
      WHERE  items.id = asi.item_id
      and    asi.estudio_id = pEstudio
      AND    items.curso_id = pCurso
      AND    pGrupo like '%' || items.grupo_id || '%'
      AND    items.semestre_id = pSemestre
      AND    items.dia_semana_id IS NOT NULL;

   CURSOR c_items_det (vIniDate date, vFinDate date) IS
      SELECT   asignatura_id, asignatura, estudio, grupo_id, tipo_subgrupo, tipo_subgrupo_id, subgrupo_id,
               dia_semana_id, hora_inicio, hora_fin, inicio, fin
      FROM     UJI_HORARIOS.HOR_ITEMS items,
               UJI_HORARIOS.HOR_ITEMS_DETALLE det,
               uji_horarios.hor_items_asignaturas asi
      WHERE    items.id = asi.item_id
      and      asi.estudio_id = pEstudio
      AND      items.curso_id = pCurso
      AND      items.grupo_id = pGrupo
      AND      items.semestre_id = pSemestre
      AND      items.dia_semana_id IS NOT NULL
      and      det.item_id = items.id
      and      det.inicio > vIniDate
      and      det.fin < vFinDate
      order by det.inicio;

   CURSOR c_asigs is
      SELECT distinct (asignatura_id), asignatura
      FROM            UJI_HORARIOS.HOR_ITEMS items,
                      uji_horarios.hor_items_asignaturas asi
      WHERE           items.id = asi.item_id
      and             asi.estudio_id = pEstudio
      AND             items.curso_id = pCurso
      AND             items.grupo_id = pGrupo
      AND             items.semestre_id = pSemestre
      AND             items.dia_semana_id IS NOT NULL
      order by        asignatura_id;

   CURSOR c_clases_asig (vAsignaturaId varchar2) is
      SELECT   tipo_subgrupo_id || subgrupo_id subgrupo,
               decode (dia_semana_id, 1, 'L', 2, 'M', 3, 'X', 4, 'J', 5, 'V', '?') dia_sem,
               to_char (hora_fin, 'hh24:mi') hora_fin_str, to_char (hora_inicio, 'hh24:mi') hora_inicio_str
      FROM     UJI_HORARIOS.HOR_ITEMS items,
               uji_horarios.hor_items_asignaturas asi
      WHERE    items.id = asi.item_id
      and      asi.estudio_id = pEstudio
      AND      items.curso_id = pCurso
      AND      items.grupo_id = pGrupo
      AND      items.semestre_id = pSemestre
      and      asi.asignatura_id = vAsignaturaId
      AND      items.dia_semana_id IS NOT NULL
      order by dia_semana_id,
               hora_inicio,
               subgrupo;

   vFechas              t_horario;
   vRdo                 clob;

   procedure cabecera is
   begin
      owa_util.mime_header (ccontent_type => 'text/xml', bclose_header => true);
      fop.documentOpen (page_width => 29.7, page_height => 21, margin_left => 0.5, margin_right => 0.5,
                        margin_top => 0.5, margin_bottom => 0, extent_before => 0, extent_after => 0.5,
                        extent_margin_top => 0, extent_margin_bottom => 0.5);
      fop.pageBodyOpen;
      fop.documentfooteropen;
      fop.block ('Fecha documento: ' || to_char (sysdate, 'yyyy/mm/dd HH24:MI'), font_size => 6,
                 font_style => 'italic');
      fop.documentfooterclose;
      fop.pageFlowOpen;
   end;

   procedure calcula_curso_aca is
   begin
      SELECT   curso_academico_id
      into     vCursoAcaNum
      FROM     hor_semestres_detalle d,
               hor_estudios e
      WHERE    e.id = pEstudio
      AND      e.tipo_id = d.tipo_estudio_id
      AND      ROWNUM = 1
      ORDER BY fecha_inicio;
   end;

   procedure info_calendario_gen is
      vCursoAca           varchar2 (4000);
      vTitulacionNombre   varchar2 (4000);
      vCursoNombre        varchar2 (4000) := pCurso || '�';
      vTextoCabecera      varchar2 (4000);
      vTextoGrupo         varchar2 (4000);
   begin
      select nombre
      into   vTitulacionNombre
      from   hor_estudios
      where  id = pEstudio;

      vCursoAca := to_char (vCursoAcaNum) || '/' || to_char (vCursoAcaNum + 1);

      if pCurso in ('1', '3') then
         vCursoNombre := pCurso || 'er';
      elsif pCurso = '2' then
         vCursoNombre := '2on';
      elsif pCurso = '4' then
         vCursoNombre := '4t';
      else
         vCursoNombre := pCurso || '�';
      end if;

      if pGrupo like '%,%' then
         vTextoGrupo := 'Grups';
      else
         vTextoGrupo := 'Grup';
      end if;

      vTextoCabecera :=
         vTitulacionNombre || ' - ' || vCursoNombre || ' curs - ' || vTextoGrupo || ' ' || pGrupo || ' (semestre '
         || pSemestre || ') - ' || vCursoAca;
      fop.block (text => vTextoCabecera, border_bottom => 'solid', border_width => 0.05);
   end;

   procedure muestra_leyenda_asig (item c_asigs%ROWTYPE) is
      vClasesText   varchar2 (4000);
   begin
      fop.block (item.asignatura_id || ', ' || item.asignatura || ':', font_size => 7, padding_top => 0.1);
      vClasesText := '<fo:inline color="white">___</fo:inline>';

      for c in c_clases_asig (item.asignatura_id) loop
         vClasesText :=
            vClasesText || c.dia_sem || ', ' || c.hora_inicio_str || '-' || c.hora_fin_str || ' (' || c.subgrupo
            || '); ';
      end loop;

      vClasesText := substr (vClasesText, 1, length (vClasesText) - 2);
      fop.block (vClasesText, font_size => 7, padding_top => 0.06);
   end;

   procedure leyenda_asigs is
   begin
      fop.block (' ', padding_top => 1);

      for item in c_asigs loop
         muestra_leyenda_asig (item);
      end loop;
   end;

   procedure pie is
   begin
      fop.pageFlowClose;
      fop.pageBodyClose;
      fop.documentClose;
   end;

   procedure calendario_gen is
   begin
      vFechas := t_horario ();

      for item in c_items_gen loop
         vFechas.extend;
         vFechas (vFechas.count) :=
            ItemHorarios (hor_contenido_item (item.hora_inicio, item.hora_fin, item.asignatura_id,
                                              '(' || item.tipo_subgrupo_id || item.subgrupo_id || ')'),
                          item.inicio, item.fin, null);
      end loop;

      if vFechas.count > 0 then
         vRdo := xpfpkg_horarios_academicos.drawTimetable (vFechas, 5, null, 'CA', 'PDF_APP_HORARIOS');
         xup_utils.dump (vRdo);
         dbms_lob.freeTemporary (vRdo);
      end if;
   end;

   procedure calendario_det (vFechaIni date, vFechaFin date) is
   begin
      vFechas := t_horario ();

      for item in c_items_det (vFechaIni, vFechaFin) loop
         vFechas.extend;
         vFechas (vFechas.count) :=
            ItemHorarios (hor_contenido_item (item.hora_inicio, item.hora_fin, item.asignatura_id,
                                              '(' || item.tipo_subgrupo_id || item.subgrupo_id || ')'),
                          item.inicio, item.fin, null);
      end loop;

      if vFechas.count > 0 then
         vRdo := xpfpkg_horarios_academicos.drawTimetable (vFechas, 5, null, 'CA', 'PDF_APP_HORARIOS_DET');
         xup_utils.dump (vRdo);
         dbms_lob.freeTemporary (vRdo);
      end if;
   end;

   procedure calcula_fechas_semestre is
   begin
      SELECT fecha_inicio, fecha_examenes_fin
      into   vFechaIniSemestre, vFechaFinSemestre
      FROM   hor_semestres_detalle,
             hor_estudios e
      WHERE  semestre_id = pSemestre
      and    e.id = pEstudio
      and    e.tipo_id = tipo_estudio_id;
   end;

   function semana_tiene_clase (vIniDate date, vFinDate date)
      return boolean is
      num_clases   number;
   begin
      SELECT   count (*)
      into     num_clases
      FROM     UJI_HORARIOS.HOR_ITEMS items,
               UJI_HORARIOS.HOR_ITEMS_DETALLE det,
               uji_horarios.hor_items_asignaturas asi
      WHERE    items.id = asi.item_id
      and      asi.estudio_id = pEstudio
      AND      items.curso_id = pCurso
      AND      items.grupo_id = pGrupo
      AND      items.semestre_id = pSemestre
      AND      items.dia_semana_id IS NOT NULL
      and      det.item_id = items.id
      and      det.inicio > vIniDate
      and      det.fin < vFinDate
      order by det.inicio;

      return num_clases > 0;
   end;

   procedure info_calendario_det (vFechaIni date, vFechaFin date) is
      vCursoAca           varchar2 (4000);
      vTitulacionNombre   varchar2 (4000);
      vCursoNombre        varchar2 (4000) := pCurso || '�';
      vTextoCabecera      varchar2 (4000);
      vFechaIniStr        varchar2 (4000) := to_char (vFechaIni, 'dd/mm/yyyy');
      vFechaFinStr        varchar2 (4000) := to_char (vFechaFin - 1, 'dd/mm/yyyy');
   begin
      select nombre
      into   vTitulacionNombre
      from   hor_estudios
      where  id = pEstudio;

      vCursoAca := to_char (vCursoAcaNum) || '/' || to_char (vCursoAcaNum + 1);

      if pCurso in ('1', '3') then
         vCursoNombre := pCurso || 'er';
      elsif pCurso = '2' then
         vCursoNombre := '2on';
      elsif pCurso = '4' then
         vCursoNombre := '4t';
      else
         vCursoNombre := pCurso || '�';
      end if;

      vTextoCabecera :=
         vTitulacionNombre || ' - ' || vCursoNombre || ' curs - Grup ' || pGrupo || ' (semestre ' || pSemestre
         || ') - Del ' || vFechaIniStr || ' al ' || vFechaFinStr;
      fop.block (text => vTextoCabecera, border_bottom => 'solid', border_width => 0.05);
   end;

   procedure paginas_calendario_det is
      vFechaIniSemana   date;
      vFechaFinSemana   date;
   begin
      calcula_fechas_semestre;
      vFechaIniSemana := vFechaIniSemestre;
      vFechaFinSemana := vFechaIniSemana + 7;

      while vFechaFinSemana <= vFechaFinSemestre loop
         if semana_tiene_clase (vFechaIniSemana, vFechaFinSemana) then
            info_calendario_det (vFechaIniSemana, vFechaFinSemana);
            calendario_det (vFechaIniSemana, vFechaFinSemana);
            htp.p ('<fo:block break-before="page" color="white">_</fo:block>');
         end if;

         vFechaIniSemana := vFechaFinSemana;
         vFechaFinSemana := vFechaFinSemana + 7;
      end loop;
   end;
BEGIN
   --euji_control_acceso (vItem);
   select count (*)
   into   vSesion
   from   apa_sesiones
   where  sesion_id = pSesion
   and    fecha < sysdate () - 0.33;

   if vSesion > 0 then
      calcula_curso_aca;
      cabecera;

      if vEsGenerica then
         info_calendario_gen;
         calendario_gen;
         leyenda_asigs;
      else
         paginas_calendario_det;
         leyenda_asigs;
      end if;

      pie;
   else
      cabecera;
      fop.block ('No tens perm�s per accedir a aquest document.', font_weight => 'bold', text_align => 'center',
                 font_size => 14, space_after => 0.5);
      pie;
   end if;
EXCEPTION
   WHEN OTHERS THEN
      htp.p (sqlerrm);
END;


