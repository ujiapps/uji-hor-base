ALTER TABLE UJI_HORARIOS.HOR_ITEMS
 ADD (COMENTARIOS  VARCHAR2(4000));


ALTER TABLE UJI_HORARIOS.HOR_EXAMENES
 ADD (DEFINITIVO  NUMBER                            DEFAULT 0                     NOT NULL);



ALTER TABLE UJI_HORARIOS.HOR_ITEMS_PROFESORES
 ADD (CREDITOS_PACTADOS  NUMBER);

ALTER TABLE UJI_HORARIOS.HOR_ITEMS ADD (creditos_computables  NUMBER);


ALTER TABLE UJI_HORARIOS.HOR_ITEMS_PROFESORES ADD (creditos_computables  NUMBER);



update hor_items_profesores
set creditos_computables = creditos_pactados;

commit;
