
ALTER TABLE UJI_HORARIOS.HOR_EXAMENES_ASIGNATURAS
 ADD (curso_id  NUMBER);


ALTER TABLE UJI_HORARIOS.HOR_EXAMENES
 ADD (comentario  VARCHAR2(1000));


CREATE OR REPLACE FORCE VIEW UJI_HORARIOS.HOR_V_ITEMS_CREDITOS_DETALLE (ID,
                                                                        NOMBRE,
                                                                        CREDITOS_PROFESOR,
                                                                        ITEM_ID,
                                                                        CRD_PROF,
                                                                        CRD_ITEM,
                                                                        CUANTOS,
                                                                        CRD_FINAL,
                                                                        GRUPO_ID,
                                                                        TIPO,
                                                                        ASIGNATURA_ID,
                                                                        ASIGNATURA_TXT,
                                                                        DEPARTAMENTO_ID,
                                                                        AREA_ID
                                                                       ) AS
   select id, nombre, creditos_profesor, item_id, round (crd_prof, 2) crd_prof, crd_item, cuantos,
          round (nvl (crd_prof, trunc (crd_item / cuantos, 2)), 2) crd_final, GRUPO_ID,
          TIPO_SUBGRUPO_ID || SUBGRUPO_ID tipo, asignatura_id, asignatura_txt, departamento_id, area_id
   from   (select   h.id, nombre, h.creditos creditos_profesor, ip.item_id, ip.creditos crd_prof, i.creditos crd_item,
                    (select count (*)
                     from   hor_items_profesores ip2
                     where  item_id = ip.item_id) cuantos, GRUPO_ID, TIPO_SUBGRUPO_ID, SUBGRUPO_ID,
                    wm_concat (asignatura_id) asignatura_txt, min (asignatura_id) asignatura_id, departamento_id,
                    area_id
           from     hor_profesores h,
                    hor_items_profesores ip,
                    hor_items i,
                    hor_items_asignaturas ia
           where    h.id = ip.profesor_id
           and      ip.item_id = i.id
           and      i.id = ia.item_id
           group by h.id,
                    nombre,
                    h.creditos,
                    ip.item_id,
                    ip.creditos,
                    i.creditos,
                    GRUPO_ID,
                    TIPO_SUBGRUPO_ID,
                    SUBGRUPO_ID,
                    departamento_id,
                    area_id)
   union
   select id, nombre, creditos creditos_profesor, null item_id, null crd_prof, null crd_item, 0 cuantos, 0 crd_final,
          null grupo_id, null tipo, null asignatura_id, null asignatura_txt, departamento_id, area_id
   from   hor_profesores h
   where  not exists (select profesor_id
                      from   hor_items_profesores
                      where  profesor_id = h.id);


					
					
					
CREATE OR REPLACE FORCE VIEW UJI_HORARIOS.HOR_V_PROFESOR_CREDITOS (ID,
                                                                   NOMBRE,
                                                                   CREDITOS_PROFESOR,
                                                                   CREDITOS_ASIGNADOS,
                                                                   CREDITOS_PENDIENTES,
                                                                   DEPARTAMENTO_ID,
                                                                   AREA_ID
                                                                  ) AS
   select   id, nombre, creditos_profesor, sum (crd_final) creditos_asignados,
            creditos_profesor - sum (crd_final) creditos_pendientes, departamento_id, area_id
   from     (select distinct id, nombre, creditos_profesor, grupo_id, tipo, asignatura_id, asignatura_txt, crd_final,
                             departamento_id, area_id
             from            hor_v_items_creditos_detalle)
   group by id,
            nombre,
            creditos_profesor,
            departamento_id,
            area_id;

ALTER TABLE UJI_HORARIOS.HOR_TIPOS_CARGOS
 ADD (departamento  NUMBER  DEFAULT 0  NOT NULL);

ALTER TABLE UJI_HORARIOS.HOR_TIPOS_CARGOS
 ADD (estudio  NUMBER DEFAULT 0  NOT NULL);

CREATE OR REPLACE FORCE VIEW UJI_HORARIOS.HOR_EXT_CARGOS_PER (ID,
                                                              PERSONA_ID,
                                                              NOMBRE,
                                                              CENTRO_ID,
                                                              CENTRO,
                                                              ESTUDIO_ID,
                                                              ESTUDIO,
                                                              CARGO_ID,
                                                              CARGO,
                                                              DEPARTAMENTO_ID,
                                                              DEPARTAMENTO,
                                                              AREA_ID,
                                                              AREA
                                                             ) AS
   select rownum id, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, TITULACION_ID estudio_id, TITULACION estudio, CARGO_ID,
          CARGO, DEPARTAMENTO_ID, DEPARTAMENTO, AREA_ID, AREA
   from   (select cp.per_id persona_id, p.nombre || ' ' || apellido1 || ' ' || apellido2 nombre, ulogica_id centro_id,
                  ubic.nombre centro, tit.id titulacion_id, tit.nombre titulacion, tc.id cargo_id, tc.nombre cargo,
                  null departamento_id, '' departamento, null area_id, '' area
           from   grh_grh.grh_cargos_per cp,
                  gri_est.est_ubic_estructurales ubic,
                  gra_Exp.exp_v_titu_todas tit,
                  uji_horarios.hor_tipos_Cargos tc,
                  gri_per.per_personas p
           where  (    f_fin is null
                   and (   f_fin is null
                        or f_fin >= sysdate)
                   and crg_id in (189, 195, 188, 30))
           and    ulogica_id = ubic.id
           and    ulogica_id = tit.uest_id
           and    tit.activa = 'S'
           and    tit.tipo = 'G'
           and    tc.id = 3
           and    cp.per_id = p.id
           union all
/* PAS de centro */
           select cp.per_id persona_id, p.nombre || ' ' || apellido1 || ' ' || apellido2 nombre, ubicacion_id centro_id,
                  ubic.nombre centro, tit.id titulacion_id, tit.nombre titulacion, tc.id cargo_id, tc.nombre cargo,
                  null departamento_id, '' departamento, null area_id, '' area
           from   grh_grh.grh_vw_contrataciones_ult cp,
                  gri_est.est_ubic_estructurales ubic,
                  gra_Exp.exp_v_titu_todas tit,
                  uji_horarios.hor_tipos_Cargos tc,
                  gri_per.per_personas p
           where  ubicacion_id = ubic.id
           and    ubicacion_id = tit.uest_id
           and    tit.activa = 'S'
           and    tit.tipo = 'G'
           and    tc.id = 4
           and    cp.per_id = p.id
           union all
/* directores de titulacion */
           select cp.per_id persona_id, p.nombre || ' ' || apellido1 || ' ' || apellido2 nombre, ulogica_id centro_id,
                  ubic.nombre centro, tit.id titulacion_id, tit.nombre titulacion, tc.id cargo_id, tc.nombre cargo,
                  null departamento_id, '' departamento, null area_id, '' area
           from   grh_grh.grh_cargos_per cp,
                  gri_est.est_ubic_estructurales ubic,
                  gra_Exp.exp_v_titu_todas tit,
                  uji_horarios.hor_tipos_Cargos tc,
                  gri_per.per_personas p
           where  (    f_fin is null
                   and (   f_fin is null
                        or f_fin >= sysdate)
                   and crg_id in (192, 193))
           and    ulogica_id = ubic.id
           and    ulogica_id = tit.uest_id
           and    tit.activa = 'S'
           and    tit.tipo = 'G'
           and    tc.id = 1
           and    cp.per_id = p.id
           and    cp.tit_id = tit.id
           union all
/* directores de departamento */
           select cp.per_id persona_id, p.nombre || ' ' || apellido1 || ' ' || apellido2 nombre, c.id centro_id,
                  c.nombre centro, null titulacion_id, '' titulacion, tc.id cargo_id, tc.nombre cargo,
                  ulogica_id departamento_id, d.nombre departamento, null area_id, '' area
           from   grh_grh.grh_cargos_per cp,
                  uji_horarios.hor_tipos_Cargos tc,
                  gri_per.per_personas p,
                  est_ubic_estructurales d,
                  est_relaciones_ulogicas r,
                  est_ubic_estructurales c
           where  (    f_fin is null
                   and (   f_fin is null
                        or f_fin >= sysdate)
                   and crg_id in (178, 194))
           and    tc.id = 6
           and    cp.per_id = p.id
           and    ulogica_id = d.id
           and    uest_id_relacionad = d.id
           and    d.tuest_id = 'DE'
           and    uest_id = c.id
           and    trel_id = 4
           union all
/* pas de departamento */
           select cp.per_id persona_id, p.nombre || ' ' || apellido1 || ' ' || apellido2 nombre, c.id centro_id,
                  c.nombre centro, null titulacion_id, '' titulacion, tc.id cargo_id, tc.nombre cargo,
                  ubic.id departamento_id, ubic.nombre departamento, null area_id, '' area
           from   grh_grh.grh_vw_contrataciones_ult cp,
                  gri_est.est_ubic_estructurales ubic,
                  uji_horarios.hor_tipos_Cargos tc,
                  gri_per.per_personas p,
                  est_relaciones_ulogicas r,
                  est_ubic_estructurales c
           where  ubicacion_id = ubic.id
           and    tc.id = 5
           and    cp.per_id = p.id
           and    cp.act_id = 'PAS'
           and    ubicacion_id = uest_id_relacionad
           and    trel_id = 4
           and    uest_id = c.id
           union all
/* permisos extra */
           select per.persona_id, p.nombre || ' ' || apellido1 || ' ' || apellido2 nombre, ubic.id centro_id,
                  ubic.nombre centro, tit.id titulacion_id, tit.nombre titulacion, tc.id cargo_id, tc.nombre cargo,
                  departamento_id, dep.nombre departamento, area_id, (select nombre
                                                                      from   gri_est.est_ubic_estructurales
                                                                      where  id = area_id) area
           from   uji_horarios.hor_permisos_extra per,
                  gri_per.per_personas p,
                  (select *
                   from   gra_Exp.exp_v_titu_todas
                   where  activa = 'S'
                   and    tipo = 'G') tit,
                  uji_horarios.hor_tipos_cargos tc,
                  gri_est.est_ubic_estructurales ubic,
                  gri_est.est_ubic_estructurales dep
           where  persona_id = p.id
           and    estudio_id = tit.id(+)
           and    tipo_Cargo_id = tc.id
           and    tit.uest_id = ubic.id(+)
           and    per.departamento_id = dep.id(+)
/* administradores */
           union all
           select distinct per.persona_id, p.nombre || ' ' || apellido1 || ' ' || apellido2 nombre, u.id centro_id,
                           u.nombre centro, tit.id titulacion_id, tit.nombre titulacion, tc.id cargo_id,
                           tc.nombre cargo, null departamento_id, '' departamento, null area_id, '' area
           from            uji_apa.apa_vw_personas_items per,
                           gri_per.per_personas p,
                           gra_Exp.exp_v_titu_todas tit,
                           uji_horarios.hor_tipos_cargos tc,
                           gri_Est.est_ubic_estructurales u
           where           persona_id = p.id
           and             tc.id = 1
           and             tit.activa = 'S'
           and             tit.tipo = 'G'
           and             aplicacion_id = 46
           and             role = 'ADMIN'
           and             tit.uest_id = u.id
           union all
           select distinct per.persona_id, p.nombre || ' ' || apellido1 || ' ' || apellido2 nombre, c.id centro_id,
                           c.nombre centro, null titulacion_id, '' titulacion, tc.id cargo_id, tc.nombre cargo,
                           dep.id departamento_id, dep.nombre departamento, null area_id, '' area
           from            uji_apa.apa_vw_personas_items per,
                           gri_per.per_personas p,
                           est_ubic_estructurales dep,
                           uji_horarios.hor_tipos_cargos tc,
                           gri_Est.est_ubic_estructurales c,
                           est_relaciones_ulogicas r
           where           persona_id = p.id
           and             tc.id = 6
           and             dep.status = 'A'
           and             dep.tuest_id = 'DE'
           and             aplicacion_id = 46
           and             role = 'ADMIN'
           and             dep.id = r.uest_id_relacionad
           and             r.trel_id = 4
           and             r.uest_id = c.id);
		   
		   

update hor_tipos_cargos
set departamento = 1
where id in (5,6);

update hor_tipos_cargos
set estudio = 1
where id in (1,2);

commit;

CREATE OR REPLACE FORCE VIEW UJI_HORARIOS.HOR_EXT_ASIGNATURAS_AREA (ASI_ID, UEST_ID, RECIBE_ACTA, PORCENTAJE, CURSO_ACA) AS
   select "ASI_ID", "UEST_ID", "RECIBE_ACTA", "PORCENTAJE", "CURSO_ACA"
   from   pod_asignaturas_area p,
          hor_curso_academico c
   where  curso_aca = c.id
   and porcentaje > 0;

  CREATE OR REPLACE TRIGGER UJI_HORARIOS.hot_items_detallle_delete
   BEFORE DELETE
   ON UJI_HORARIOS.HOR_ITEMS_DETALLE
   REFERENCING NEW AS New OLD AS Old
   FOR EACH ROW
DECLARE
   tmpVar   NUMBER;
BEGIN
   if deleting then
      delete      hor_items_det_profesores
      where       detalle_id = :old.id;
   end if;
END hot_items_detallle_delete;

CREATE OR REPLACE FORCE VIEW UJI_HORARIOS.HOR_V_ITEMS_PROF_DETALLE (ID,
                                                                    DETALLE_ID,
                                                                    DET_PROFESOR_ID,
                                                                    ITEM_ID,
                                                                    INICIO,
                                                                    FIN,
                                                                    DESCRIPCION,
                                                                    PROFESOR_ID,
                                                                    DETALLE_MANUAL,
                                                                    CREDITOS,
                                                                    CREDITOS_DETALLE,
                                                                    SELECCIONADO,
                                                                    NOMBRE,
                                                                    PROFESOR_COMPLETO
                                                                   ) AS
   select   to_number (id || det_profesor_id || item_id || to_char (inicio, 'yyyymmddhh24miss')) id, id detalle_id,
            det_profesor_id, item_id, inicio, fin, descripcion, profesor_id, detalle_manual, creditos, creditos_detalle,
            decode (detalle_manual, 0, 'S', decode (min (orden), 1, 'S', 'N')) seleccionado, nombre, profesor_completo
   from     (select d.*, profesor_id, p.detalle_manual, i.creditos, p.creditos creditos_detalle, p.id det_profesor_id,
                    99 orden, nombre, (select wm_concat (nombre)
                                       from   hor_items_profesores pr,
                                              hor_profesores p
                                       where  item_id = i.id
                                       and    pr.profesor_id = p.id) profesor_completo
             from   hor_items i,
                    hor_items_detalle d,
                    hor_items_profesores p,
                    hor_profesores pro
             where  i.id = d.item_id
             and    i.id = p.item_id
             and    p.profesor_id = pro.id
             union all
             select i.id, i.id item_id, null inicio, null fin, null descripcion, profesor_id, p.detalle_manual,
                    i.creditos, round (p.creditos, 2) creditos_detalle, p.id det_profesor_id, 99 orden, nombre,
                    (select wm_concat (nombre)
                     from   hor_items_profesores pr,
                            hor_profesores p
                     where  item_id = i.id
                     and    pr.profesor_id = p.id) profesor_completo
             from   hor_items i,
                    hor_items_profesores p,
                    hor_profesores pro
             where  i.id = p.item_id
             and    p.profesor_id = pro.id
             and    not exists (select 1
                                from   hor_items_detalle
                                where  item_id = i.id)
             union all
             select d.*, profesor_id, p.detalle_manual, i.creditos, p.creditos creditos_detalle, p.id det_profesor_id,
                    1 orden, nombre, (select wm_concat (nombre)
                                      from   hor_items_profesores pr,
                                             hor_profesores p
                                      where  item_id = i.id
                                      and    pr.profesor_id = p.id) profesor_completo
             from   hor_items i,
                    hor_items_detalle d,
                    hor_items_profesores p,
                    hor_items_det_profesores dp,
                    hor_profesores pro
             where  i.id = d.item_id
             and    i.id = p.item_id
             and    p.detalle_manual = 1
             and    dp.detalle_id = d.id
             and    item_profesor_id = p.id
             and    p.profesor_id = pro.id)
   group by id,
            item_id,
            inicio,
            fin,
            descripcion,
            profesor_id,
            detalle_manual,
            creditos,
            creditos_detalle,
            det_profesor_id,
            nombre,
            profesor_completo;

 