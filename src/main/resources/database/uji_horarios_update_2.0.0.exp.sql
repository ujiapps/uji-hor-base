CREATE OR REPLACE procedure UJI_HORARIOS.horarios_ocupacion_pdf(name_array in euji_util.ident_arr2, value_array in euji_util.ident_arr2) is
   pAula constant              varchar2(40) := euji_util.euji_getparam(name_array, value_array, 'pAula');
   pSemestre constant          varchar2(40) := euji_util.euji_getparam(name_array, value_array, 'pSemestre');
   pSesion constant            varchar2(4000) := euji_util.euji_getparam(name_array, value_array, 'pSesion');
   pSemana constant            varchar2(40) := euji_util.euji_getparam(name_array, value_array, 'pSemana');
   vCursoAcaNum                number;
   vFechaIniSemestre           date;
   vFechaFinSemestre           date;
   vSesion                     number;
   vEsGenerica                 boolean := pSemana = 'G';
   vFechas                     t_horario;
   vRdo                        clob;

   cursor c_items_det_ant(vIniDate date, vFinDate date) is
        select items.id, items.grupo_id, items.tipo_subgrupo, items.tipo_subgrupo_id, items.subgrupo_id, items.dia_semana_id, items.hora_inicio, items.hora_fin, det.inicio, det.fin,
               rtrim(xmlagg(xmlelement(e, asi.asignatura_id || ', ')).extract('//text()'), ', ') as asignatura_id
          from UJI_HORARIOS.HOR_ITEMS items, UJI_HORARIOS.HOR_ITEMS_DETALLE det, uji_horarios.hor_items_asignaturas asi, hor_aulas aula, hor_aulas_planificacion p
         where items.id = asi.item_id
           and items.aula_planificacion_id = aula.id
           and aula.id = pAula
           and p.aula_id = aula.id
           and items.semestre_id = pSemestre
           and items.dia_semana_id is not null
           and det.item_id = items.id
           and det.inicio > vIniDate
           and det.fin < vFinDate
      group by items.id, items.grupo_id, items.tipo_subgrupo, items.tipo_subgrupo_id, items.subgrupo_id, items.dia_semana_id, items.semestre_id, items.hora_inicio, items.hora_fin, det.inicio, det.fin, asi.asignatura_id
      order by det.inicio;

   cursor c_items_det(vIniDate date, vFinDate date) is
        select *
          from ( -- Clases
                select   items.id, items.grupo_id, items.tipo_subgrupo, items.tipo_subgrupo_id, items.subgrupo_id, items.dia_semana_id, items.hora_inicio, items.hora_fin, det.inicio, det.fin, nvl(comun_texto, asi.asignatura_id) asignatura_id
                    from UJI_HORARIOS.HOR_ITEMS items, UJI_HORARIOS.HOR_ITEMS_DETALLE det, uji_horarios.hor_items_asignaturas asi, hor_aulas aula, hor_aulas_planificacion p
                   where items.id = asi.item_id
                     and items.aula_planificacion_id = aula.id
                     and aula.id = pAula
                     and p.aula_id = aula.id
                     and items.semestre_id = pSemestre
                     and items.dia_semana_id is not null
                     and det.item_id = items.id
                     and det.inicio > vIniDate
                     and det.fin < vFinDate
                group by items.id, items.grupo_id, items.tipo_subgrupo, items.tipo_subgrupo_id, items.subgrupo_id, items.dia_semana_id, items.semestre_id, items.hora_inicio, items.hora_fin, det.inicio, det.fin,
                         nvl(comun_texto, asi.asignatura_id)
                union
                -- Exámenes
                select exa.id, ' ' grupo_id, 'Exàmen' tipo_subgrupo, 'EXA' tipo_subgrupo_id, null subgrupo_id, to_number(to_char(exa.hora_inicio, 'd')) dia_semana_id, exa.hora_inicio, exa.hora_fin, exa.hora_inicio incio, exa.hora_fin fin,
                       'Exàmen ' || nombre asignatura_id
                  from hor_examenes exa join hor_examenes_aulas exa_aul on exa.id = exa_aul.examen_id
                 where exa_aul.aula_id = pAula
                   and exa.hora_inicio > vIniDate
                   and exa.hora_fin < vFinDate)
      order by inicio;

   cursor c_examenes(vIniDate date, vFinDate date) is
        select *
          from (
                -- Exámenes
                select exa.id, ' ' grupo_id, 'Exàmen' tipo_subgrupo, 'EXA' tipo_subgrupo_id, null subgrupo_id, to_number(to_char(exa.hora_inicio, 'd')) dia_semana_id, exa.hora_inicio, exa.hora_fin, exa.hora_inicio incio, exa.hora_fin fin,
                       'Exàmen ' || nombre asignatura_id
                  from hor_examenes exa join hor_examenes_aulas exa_aul on exa.id = exa_aul.examen_id
                 where exa_aul.aula_id = pAula
                   and exa.hora_inicio > vIniDate
                   and exa.hora_fin < vFinDate)
      order by hora_inicio;

   cursor c_items_gen_ant is
        select items.id, items.grupo_id, items.tipo_subgrupo, items.tipo_subgrupo_id, items.subgrupo_id, items.dia_semana_id, items.hora_inicio, items.hora_fin,
               rtrim(xmlagg(xmlelement(e, asi.asignatura_id || ', ')).extract('//text()'), ', ') as asignatura_id, to_date(dia_semana_id + 1 || '/01/2006 ' || to_char(hora_fin, 'hh24:mi'), 'dd/mm/yyyy hh24:mi') fin,
               to_date(dia_semana_id + 1 || '/01/2006 ' || to_char(hora_inicio, 'hh24:mi'), 'dd/mm/yyyy hh24:mi') inicio
          from UJI_HORARIOS.HOR_ITEMS items, uji_horarios.hor_items_asignaturas asi, hor_aulas aula, hor_aulas_planificacion p
         where items.id = asi.item_id
           and items.aula_planificacion_id = aula.id
           and aula.id = p.aula_id
           and aula.id = pAula
           and items.semestre_id = pSemestre
           and items.dia_semana_id is not null
      group by items.id, items.grupo_id, items.tipo_subgrupo, items.tipo_subgrupo_id, items.subgrupo_id, items.dia_semana_id, items.semestre_id, items.hora_inicio, items.hora_fin, asi.asignatura_id
      order by items.hora_inicio;

   cursor c_items_gen is
        select items.id, items.grupo_id, items.tipo_subgrupo, items.tipo_subgrupo_id, items.subgrupo_id, items.dia_semana_id, items.hora_inicio, items.hora_fin, nvl(comun_texto, asi.asignatura_id) asignatura_id,
               to_date(dia_semana_id + 1 || '/01/2006 ' || to_char(hora_fin, 'hh24:mi'), 'dd/mm/yyyy hh24:mi') fin, to_date(dia_semana_id + 1 || '/01/2006 ' || to_char(hora_inicio, 'hh24:mi'), 'dd/mm/yyyy hh24:mi') inicio
          from UJI_HORARIOS.HOR_ITEMS items, uji_horarios.hor_items_asignaturas asi, hor_aulas aula, hor_aulas_planificacion p
         where items.id = asi.item_id
           and items.aula_planificacion_id = aula.id
           and aula.id = p.aula_id
           and aula.id = pAula
           and items.semestre_id = pSemestre
           and items.dia_semana_id is not null
      group by items.id, items.grupo_id, items.tipo_subgrupo, items.tipo_subgrupo_id, items.subgrupo_id, items.dia_semana_id, items.semestre_id, items.hora_inicio, items.hora_fin, nvl(comun_texto, asi.asignatura_id)
      order by items.hora_inicio;

   cursor c_asigs is
        select distinct (asignatura_id), asignatura
          from UJI_HORARIOS.HOR_ITEMS items, uji_horarios.hor_items_asignaturas asi, hor_aulas aula, hor_aulas_planificacion p
         where items.id = asi.item_id
           and items.aula_planificacion_id = aula.id
           and p.aula_id = aula.id
           and aula.id = pAula
           and p.semestre_id = pSemestre
           and items.dia_semana_id is not null
      order by asignatura_id;

   cursor c_clases_asig(vAsignaturaId varchar2) is
        select tipo_subgrupo_id || subgrupo_id subgrupo, decode(dia_semana_id, 1, 'L', 2, 'M', 3, 'X', 4, 'J', 5, 'V', '?') dia_sem, to_char(hora_fin, 'hh24:mi') hora_fin_str, to_char(hora_inicio, 'hh24:mi') hora_inicio_str
          from UJI_HORARIOS.HOR_ITEMS items, uji_horarios.hor_items_asignaturas asi, hor_aulas aula, hor_aulas_planificacion p
         where items.id = asi.item_id
           and items.aula_planificacion_id = aula.id
           and p.aula_id = aula.id
           and aula.id = pAula
           and items.semestre_id = pSemestre
           and asi.asignatura_id = vAsignaturaId
           and items.dia_semana_id is not null
      order by dia_semana_id, hora_inicio, subgrupo;

   function redondea_fecha_a_cuartos(vIniDate date)
      return date is
      fecha_redondeada          date;
   begin
      select trunc(vIniDate, 'HH') + (15 * round(to_char(trunc(vIniDate, 'MI'), 'MI') / 15)) / 1440
        into fecha_redondeada
        from dual;

      return fecha_redondeada;
   end;

   function semana_tiene_clase(vIniDate date, vFinDate date)
      return boolean is
      num_clases          number;
   begin
      select count( * )
        into num_clases
        from UJI_HORARIOS.HOR_ITEMS items, UJI_HORARIOS.HOR_ITEMS_DETALLE det, hor_aulas aula, hor_aulas_planificacion p
       where items.aula_planificacion_id = aula.id
         and p.aula_id = aula.id
         and aula.id = pAula
         and items.semestre_id = pSemestre
         and items.dia_semana_id is not null
         and det.item_id = items.id
         and det.inicio > vIniDate
         and det.fin < vFinDate;

      return num_clases > 0;
   end;

   function semana_tiene_examen(vIniDate date, vFinDate date)
      return boolean is
      num_examenes          number;
   begin
      select count( * )
        into num_examenes
        from hor_examenes exa join hor_examenes_aulas exa_aul on exa.id = exa_aul.examen_id
       where exa_aul.aula_id = pAula
         and exa.hora_inicio > vIniDate
         and exa.hora_fin < vFinDate;

      return num_examenes > 0;
   end;

   procedure calcula_curso_aca is
   begin
        select distinct (curso_academico_id)
          into vCursoAcaNum
          from hor_semestres_detalle d, hor_estudios e
         where rownum = 1
      order by curso_academico_id;
   end;

   procedure calcula_fechas_semestre is
   begin
      select fecha_inicio, fecha_examenes_fin
        into vFechaIniSemestre, vFechaFinSemestre
        from hor_semestres_detalle, hor_estudios e
       where semestre_id = pSemestre
         and rownum = 1;
   end;

   procedure calendario_det(vFechaIni date, vFechaFin date) is
   begin
      vFechas := t_horario();

      for item in c_items_det(vFechaIni, vFechaFin) loop
         vFechas.extend;
         vFechas(vFechas.count) := ItemHorarios(hor_contenido_item(redondea_fecha_a_cuartos(item.hora_inicio), redondea_fecha_a_cuartos(item.hora_fin), item.asignatura_id || ' ' || item.grupo_id, '(' || item.tipo_subgrupo_id || item.subgrupo_id || ')'), redondea_fecha_a_cuartos(item.inicio), redondea_fecha_a_cuartos(item.fin), null);
      end loop;

      if vFechas.count > 0 then
         vRdo := xpfpkg_horarios_academicos.drawTimetable(vFechas, 5, null, 'CA', 'PDF_APP_HORARIOS_DET');
         xup_utils.dump(vRdo);
         dbms_lob.freeTemporary(vRdo);
      end if;
   end;

   procedure calendario_examenes(vFechaIni date, vFechaFin date) is
   begin
      vFechas := t_horario();

      for item in c_examenes(vFechaIni, vFechaFin) loop
         vFechas.extend;
         vFechas(vFechas.count) := ItemHorarios(hor_contenido_item(redondea_fecha_a_cuartos(item.hora_inicio), redondea_fecha_a_cuartos(item.hora_fin), item.asignatura_id || ' ' || item.grupo_id, '(' || item.tipo_subgrupo_id || item.subgrupo_id || ')'), redondea_fecha_a_cuartos(item.hora_inicio), redondea_fecha_a_cuartos(item.fin), null);
      end loop;

      if vFechas.count > 0 then
         vRdo := xpfpkg_horarios_academicos.drawTimetable(vFechas, 5, null, 'CA', 'PDF_APP_HORARIOS_DET');
         xup_utils.dump(vRdo);
         dbms_lob.freeTemporary(vRdo);
      end if;
   end;

   procedure calendario_gen is
   begin
      vFechas := t_horario();

      for item in c_items_gen loop
         vFechas.extend;
         vFechas(vFechas.count) := ItemHorarios(hor_contenido_item(redondea_fecha_a_cuartos(item.hora_inicio), redondea_fecha_a_cuartos(item.hora_fin), item.asignatura_id || ' ' || item.grupo_id, '(' || item.tipo_subgrupo_id || item.subgrupo_id || ')'), redondea_fecha_a_cuartos(item.inicio), redondea_fecha_a_cuartos(item.fin), null);
      end loop;

      if vFechas.count > 0 then
         vRdo := xpfpkg_horarios_academicos.drawTimetable(vFechas, 5, null, 'CA', 'PDF_APP_HORARIOS');
         xup_utils.dump(vRdo);
         dbms_lob.freeTemporary(vRdo);
      end if;
   end;

   procedure info_calendario_det(vFechaIni date, vFechaFin date) is
      vCursoAca               varchar2(4000);
      vTextoCabecera          varchar2(4000);
      vFechaIniStr            varchar2(4000) := to_char(vFechaIni, 'dd/mm/yyyy');
      vFechaFinStr            varchar2(4000) := to_char(vFechaFin - 1, 'dd/mm/yyyy');
      vAulaNombre             varchar2(4000);
      vEdificio               varchar2(4000);
      vPlanta                 varchar2(4000);
      vCentroNombre           varchar2(4000);
   begin
      vCursoAca := to_char(vCursoAcaNum) || '/' || to_char(vCursoAcaNum + 1);

      select a.nombre as aulaNombre, a.edificio, a.planta, c.nombre as centroNombre
        into vAulaNombre, vEdificio, vPlanta, vCentroNombre
        from hor_aulas a, hor_centros c
       where a.id = pAula
         and c.id = a.centro_id;

      vTextoCabecera := vAulaNombre || ' - ' || vCentroNombre || ' - Edifici ' || vEdificio || ' - Planta ' || vPlanta || '  - (semestre ' || pSemestre || ') - Del ' || vFechaIniStr || ' al ' || vFechaFinStr;
      fop.block(text => vTextoCabecera, border_bottom => 'solid', border_width => 0.05);
   end;

   procedure info_calendario_gen is
      vCursoAca               varchar2(4000);
      vTextoCabecera          varchar2(4000);
      vAulaNombre             varchar2(4000);
      vEdificio               varchar2(4000);
      vPlanta                 varchar2(4000);
      vCentroNombre           varchar2(4000);
   begin
      vCursoAca := to_char(vCursoAcaNum) || '/' || to_char(vCursoAcaNum + 1);

      select a.nombre as aulaNombre, a.edificio, a.planta, c.nombre as centroNombre
        into vAulaNombre, vEdificio, vPlanta, vCentroNombre
        from hor_aulas a, hor_centros c
       where a.id = pAula
         and c.id = a.centro_id;

      vTextoCabecera := vAulaNombre || ' - ' || vCentroNombre || ' - Edifici ' || vEdificio || ' - Planta ' || vPlanta || '  - (semestre ' || pSemestre || ') ';
      fop.block(text => vTextoCabecera, border_bottom => 'solid', border_width => 0.05);
   end;

   procedure paginas_calendario_det is
      vFechaIniSemana          date;
      vFechaFinSemana          date;
      vDiaSemana               number;
   begin
      calcula_fechas_semestre;
      vDiaSemana := to_number(to_char(vFechaIniSemestre, 'd'));

      if vDiaSemana = 1 then
         vFechaIniSemana := vFechaIniSemestre;
      else
         vFechaIniSemana := vFechaIniSemestre - vDiaSemana + 1;
      end if;

      vDiaSemana := to_number(to_char(vFechaFinSemestre, 'd'));
      if vDiaSemana > 1 then
         vFechaFinSemestre := vFechaFinSemestre - vDiaSemana + 8;
      end if;

      vFechaFinSemana := vFechaIniSemana + 7;

      --htp.p('<fo:block>Semestre del '|| vFechaIniSemestre || ' al ' || vFechaFinSemestre || '</fo:block>');

      while vFechaFinSemana <= vFechaFinSemestre loop
         if semana_tiene_clase(vFechaIniSemana, vFechaFinSemana)
         or semana_tiene_examen(vFechaIniSemana, vFechaFinSemana) then
            info_calendario_det(vFechaIniSemana, vFechaFinSemana);
            calendario_det(vFechaIniSemana, vFechaFinSemana);
            htp.p('<fo:block break-before="page" color="white">_</fo:block>');
         end if;

         vFechaIniSemana := vFechaFinSemana;
         vFechaFinSemana := vFechaFinSemana + 7;
      end loop;
   end;

   procedure paginas_calendario_examenes is
      vFechaIniSemana          date;
      vFechaFinSemana          date;
      vDiaSemana               number;
   begin
      calcula_fechas_semestre;
      vDiaSemana := to_number(to_char(vFechaIniSemestre, 'd'));

      if vDiaSemana = 1 then
         vFechaIniSemana := vFechaIniSemestre;
      else
         vFechaIniSemana := vFechaIniSemestre - vDiaSemana + 1;
      end if;

      vDiaSemana := to_number(to_char(vFechaFinSemestre, 'd'));
      if vDiaSemana > 1 then
         vFechaFinSemestre := vFechaFinSemestre - vDiaSemana + 8;
      end if;

      vFechaFinSemana := vFechaIniSemana + 7;

      while vFechaFinSemana <= vFechaFinSemestre loop
         if semana_tiene_examen(vFechaIniSemana, vFechaFinSemana) then
            htp.p('<fo:block break-before="page" color="white">_</fo:block>');
            info_calendario_det(vFechaIniSemana, vFechaFinSemana);
            calendario_examenes(vFechaIniSemana, vFechaFinSemana);
         end if;

         vFechaIniSemana := vFechaFinSemana;
         vFechaFinSemana := vFechaFinSemana + 7;
      end loop;
   end;

   procedure cabecera is
   begin
      owa_util.mime_header(ccontent_type => 'text/xml', bclose_header => true);
      fop.documentOpen(page_width => 29.7,
                       page_height => 21,
                       margin_left => 0.5,
                       margin_right => 0.5,
                       margin_top => 0.5,
                       margin_bottom => 0,
                       extent_before => 0,
                       extent_after => 0.5,
                       extent_margin_top => 0,
                       extent_margin_bottom => 0.5);
      fop.pageBodyOpen;
      fop.documentfooteropen;
      fop.block('Fecha documento: ' || to_char(sysdate, 'yyyy/mm/dd HH24:MI'), font_size => 6, font_style => 'italic');
      fop.documentfooterclose;
      fop.pageFlowOpen;
   end;

   procedure muestra_leyenda_asig(item c_asigs%rowtype) is
      vClasesText          varchar2(32000);
   begin
      fop.block(item.asignatura_id || ', ' || item.asignatura || ':', font_size => 7, padding_top => 0.1);
      vClasesText := '<fo:inline color="white">___</fo:inline>';

      for c in c_clases_asig(item.asignatura_id) loop
         vClasesText := vClasesText || c.dia_sem || ', ' || c.hora_inicio_str || '-' || c.hora_fin_str || ' (' || c.subgrupo || '); ';
      end loop;

      --vClasesText := substr (vClasesText, 1, length (vClasesText));
      fop.block(vClasesText, font_size => 7, padding_top => 0.06);
   end;

   procedure leyenda_asigs is
   begin
      fop.block(' ', padding_top => 1);

      for item in c_asigs loop
         muestra_leyenda_asig(item);
      end loop;
   end;

   procedure pie is
   begin
      fop.pageFlowClose;
      fop.pageBodyClose;
      fop.documentClose;
   end;
begin
   select count( * )
     into vSesion
     from apa_sesiones
    where sesion_id = pSesion
      and fecha < sysdate() - 0.33;

   if vSesion > 0 then
      calcula_curso_aca;
      cabecera;

      if vEsGenerica then
         info_calendario_gen;
         calendario_gen;
         leyenda_asigs;
         paginas_calendario_examenes;
      else
         paginas_calendario_det;
         leyenda_asigs;
      end if;

      pie;
   else
      cabecera;
      fop.block('No tens permís per accedir a aquest document.', font_weight => 'bold', text_align => 'center', font_size => 14, space_after => 0.5);
      pie;
   end if;
exception
   when others then
      htp.p(sqlerrm);
end;


CREATE OR REPLACE procedure UJI_HORARIOS.HORARIOS_SEMANA_PDF(name_array in euji_util.ident_arr2, value_array in euji_util.ident_arr2) is
   -- Parametros
   pEstudio constant             varchar2(40) := euji_util.euji_getparam(name_array, value_array, 'pEstudio');
   pSemestre constant            varchar2(40) := euji_util.euji_getparam(name_array, value_array, 'pSemestre');
   pSesion constant              varchar2(4000) := euji_util.euji_getparam(name_array, value_array, 'pSesion');
   pGrupo                        varchar2(40) := euji_util.euji_getparam(name_array, value_array, 'pGrupo');
   pCurso constant               varchar2(40) := euji_util.euji_getparam(name_array, value_array, 'pCurso');
   pSemana constant              varchar2(40) := euji_util.euji_getparam(name_array, value_array, 'pSemana');
   pAgrupacion constant          varchar2(40) := euji_util.euji_getparam(name_array, value_array, 'pAgrupacion');
   vCursoAcaNum                  number;
   vSesion                       number;
   vFechaIniSemestre             date;
   vFechaFinSemestre             date;
   vEsGenerica                   boolean := pSemana = 'G';
   vFirstPageBreak               boolean := true;

   cursor c_items_gen(pSemestre in number, pCurso in number) is
      select aulas.codigo as codigo_aula, asignatura_id, asignatura, estudio, asi.caracter, semestre_id, comun, grupo_id, tipo_subgrupo, tipo_subgrupo_id, subgrupo_id, dia_semana_id, hora_inicio, hora_fin, tipo_asignatura_id,
             tipo_asignatura, to_date(dia_semana_id + 1 || '/01/2006 ' || to_char(hora_fin, 'hh24:mi'), 'dd/mm/yyyy hh24:mi') fin, to_date(dia_semana_id + 1 || '/01/2006 ' || to_char(hora_inicio, 'hh24:mi'), 'dd/mm/yyyy hh24:mi') inicio
        from UJI_HORARIOS.HOR_ITEMS items left outer join hor_aulas aulas on (aulas.id = items.aula_planificacion_id) inner join uji_horarios.hor_items_asignaturas asi on (items.id = asi.item_id)
       where asi.estudio_id = pEstudio
         and asi.curso_id = pCurso
         and ',' || pGrupo || ',' like '%,' || items.grupo_id || ',%'
         and items.semestre_id = pSemestre
         and items.dia_semana_id is not null;

   cursor c_items_gen_agrupacion(pSemestre in  number,
                                 pAgrupacion in number) is
      select distinct aulas.codigo as codigo_aula, asi.asignatura_id, asignatura, estudio, asi.caracter, semestre_id, comun, grupo_id, tipo_subgrupo, tipo_subgrupo_id, subgrupo_id, dia_semana_id, hora_inicio, hora_fin, tipo_asignatura_id,
                      tipo_asignatura, to_date(dia_semana_id + 1 || '/01/2006 ' || to_char(hora_fin, 'hh24:mi'), 'dd/mm/yyyy hh24:mi') fin,
                      to_date(dia_semana_id + 1 || '/01/2006 ' || to_char(hora_inicio, 'hh24:mi'), 'dd/mm/yyyy hh24:mi') inicio
        from UJI_HORARIOS.HOR_ITEMS items left outer join hor_aulas aulas on (aulas.id = items.aula_planificacion_id) inner join uji_horarios.hor_items_asignaturas asi on (items.id = asi.item_id)
             join uji_horarios.HOR_V_AGRUPACIONES_ITEMS agr_item
                on (items.id = AGR_ITEM.ITEM_ID)
       where asi.estudio_id = pEstudio
         and agr_item.agrupacion_id = pAgrupacion
         and ',' || pGrupo || ',' like '%,' || items.grupo_id || ',%'
         and items.semestre_id = pSemestre
         and items.dia_semana_id is not null;

   cursor c_items_det(vIniDate      date,
                      vFinDate      date,
                      pCurso in     number) is
        select aulas.codigo as codigo_aula, asignatura_id, asignatura, estudio, grupo_id, tipo_subgrupo, tipo_subgrupo_id, subgrupo_id, dia_semana_id, hora_inicio, hora_fin, inicio, fin
          from UJI_HORARIOS.HOR_ITEMS items left outer join hor_aulas aulas on (aulas.id = items.aula_planificacion_id) inner join uji_horarios.hor_items_asignaturas asi on (items.id = asi.item_id)
               inner join UJI_HORARIOS.HOR_ITEMS_DETALLE det
                  on (det.item_id = items.id)
         where asi.estudio_id = pEstudio
           and asi.curso_id = pCurso
           and ',' || pGrupo || ',' like '%,' || items.grupo_id || ',%'
           and items.semestre_id = pSemestre
           and items.dia_semana_id is not null
           and det.inicio > vIniDate
           and det.fin < vFinDate
      order by det.inicio;

   cursor c_items_det_agrupacion(vIniDate      date,
                                 vFinDate      date,
                                 pAgrupacion in number) is
        select distinct aulas.codigo as codigo_aula, asi.asignatura_id, asignatura, estudio, grupo_id, tipo_subgrupo, tipo_subgrupo_id, subgrupo_id, dia_semana_id, hora_inicio, hora_fin, inicio, fin
          from UJI_HORARIOS.HOR_ITEMS items left outer join hor_aulas aulas on (aulas.id = items.aula_planificacion_id) inner join uji_horarios.hor_items_asignaturas asi on (items.id = asi.item_id)
               inner join UJI_HORARIOS.HOR_ITEMS_DETALLE det
                  on (det.item_id = items.id)
               join uji_horarios.HOR_V_AGRUPACIONES_ITEMS agr_item
                  on (items.id = AGR_ITEM.ITEM_ID)
         where asi.estudio_id = pEstudio
           and agr_item.agrupacion_id = pAgrupacion
           and ',' || pGrupo || ',' like '%,' || items.grupo_id || ',%'
           and items.semestre_id = pSemestre
           and items.dia_semana_id is not null
           and det.inicio > vIniDate
           and det.fin < vFinDate
      order by det.inicio;

   cursor c_asigs(pSemestre in number, pCurso in number) is
        select distinct (asignatura_id), asignatura
          from UJI_HORARIOS.HOR_ITEMS items, uji_horarios.hor_items_asignaturas asi
         where items.id = asi.item_id
           and asi.estudio_id = pEstudio
           and asi.curso_id = pCurso
           and ',' || pGrupo || ',' like '%,' || items.grupo_id || ',%'
           and items.semestre_id = pSemestre
           and items.dia_semana_id is not null
      order by asignatura_id;

   cursor c_asigs_agrupacion(pSemestre in number, pAgrupacion in number) is
        select distinct (asi.asignatura_id), asi.asignatura
          from UJI_HORARIOS.HOR_ITEMS items, uji_horarios.hor_items_asignaturas asi, uji_horarios.HOR_V_AGRUPACIONES_ITEMS agr_item
         where items.id = asi.item_id
           and items.id = agr_item.item_id
           and asi.estudio_id = pEstudio
           and agr_item.agrupacion_id = pAgrupacion
           and ',' || pGrupo || ',' like '%,' || items.grupo_id || ',%'
           and items.semestre_id = pSemestre
           and items.dia_semana_id is not null
      order by asignatura_id;

   cursor c_clases_asig(vAsignaturaId varchar2, vSemestre varchar2, pCurso in number) is
        select tipo_subgrupo_id || subgrupo_id subgrupo, decode(dia_semana_id, 1, 'L', 2, 'M', 3, 'X', 4, 'J', 5, 'V', '?') dia_sem, to_char(hora_fin, 'hh24:mi') hora_fin_str, to_char(hora_inicio, 'hh24:mi') hora_inicio_str
          from UJI_HORARIOS.HOR_ITEMS items, uji_horarios.hor_items_asignaturas asi
         where items.id = asi.item_id
           and asi.estudio_id = pEstudio
           and asi.curso_id = pCurso
           and ',' || pGrupo || ',' like '%,' || items.grupo_id || ',%'
           and items.semestre_id = vSemestre
           and asi.asignatura_id = vAsignaturaId
           and items.dia_semana_id is not null
      order by dia_semana_id, hora_inicio, subgrupo;

   cursor c_clases_asig_agrupacion(vAsignaturaId varchar2, vSemestre varchar2, pAgrupacion in number) is
        select tipo_subgrupo_id || subgrupo_id subgrupo, decode(dia_semana_id, 1, 'L', 2, 'M', 3, 'X', 4, 'J', 5, 'V', '?') dia_sem, to_char(hora_fin, 'hh24:mi') hora_fin_str, to_char(hora_inicio, 'hh24:mi') hora_inicio_str
          from UJI_HORARIOS.HOR_ITEMS items, uji_horarios.hor_items_asignaturas asi
         where items.id = asi.item_id
           and asi.estudio_id = pEstudio
           and ',' || pGrupo || ',' like '%,' || items.grupo_id || ',%'
           and items.semestre_id = vSemestre
           and asi.asignatura_id = vAsignaturaId
           and items.dia_semana_id is not null
           and items.id in (select item_id
                              from HOR_V_AGRUPACIONES_ITEMS agr_item
                             where agr_item.agrupacion_id = pAgrupacion)
      order by dia_semana_id, hora_inicio, subgrupo;

   vFechas                       t_horario;
   vRdo                          clob;

   cursor cursosTitulacion(vEstudio in number, vCurso in number) is
        select distinct curso_id
          from hor_items_asignaturas
         where estudio_id = vEstudio
           and (curso_id = vCurso
             or vCurso = -1)
      order by curso_id;

   cursor agrupacionesTitulacion(vEstudio in number, vAgrupacion in number) is
      select agrupacion_id
        from hor_agrupaciones_estudios
       where estudio_id = vEstudio
         and (agrupacion_id = vAgrupacion
           or vAgrupacion = -1);

   procedure cabecera is
   begin
      owa_util.mime_header(ccontent_type => 'text/xml', bclose_header => true);
      fop.documentOpen(page_width => 29.7,
                       page_height => 21,
                       margin_left => 0.5,
                       margin_right => 0.5,
                       margin_top => 0.5,
                       margin_bottom => 0,
                       extent_before => 0,
                       extent_after => 0.5,
                       extent_margin_top => 0,
                       extent_margin_bottom => 0.5);
      fop.pageBodyOpen;
      fop.documentfooteropen;
      fop.block('Fecha documento: ' || to_char(sysdate, 'yyyy/mm/dd HH24:MI'), font_size => 6, font_style => 'italic');
      fop.documentfooterclose;
      fop.pageFlowOpen;
   end;

   procedure calcula_curso_aca is
   begin
        select curso_academico_id
          into vCursoAcaNum
          from hor_semestres_detalle d, hor_estudios e
         where e.id = pEstudio
           and e.tipo_id = d.tipo_estudio_id
           and rownum = 1
      order by fecha_inicio;
   end;

   procedure info_calendario_gen(pSemestre in number, pCurso in number, pAgrupacion in number) is
      vCursoAca                  varchar2(4000);
      vTitulacionNombre          varchar2(4000);
      vCursoNombre               varchar2(4000) := '';
      vTextoCabecera             varchar2(4000);
      vTextoGrupo                varchar2(4000);
      vNombreAgrupacion          varchar2(4000) := '';
   begin
      select nombre
        into vTitulacionNombre
        from hor_estudios
       where id = pEstudio;

      if (pAgrupacion is not null) then
         select 'Agrupació ' || nombre
           into vNombreAgrupacion
           from hor_agrupaciones
          where id = pAgrupacion;
      end if;

      vCursoAca := to_char(vCursoAcaNum) || '/' || to_char(vCursoAcaNum + 1);

      if pCurso in ('1', '3') then
         vCursoNombre := pCurso || 'er curs';
      elsif pCurso = '2' then
         vCursoNombre := '2on  curs';
      elsif pCurso = '4' then
         vCursoNombre := '4t curs';
      elsif pCurso is not null then
         vCursoNombre := pCurso || '?  curs';
      end if;

      if pGrupo like '%,%' then
         vTextoGrupo := 'Grups';
      else
         vTextoGrupo := 'Grup';
      end if;

      vTextoCabecera := vTitulacionNombre || ' - ' || vCursoNombre || vNombreAgrupacion || ' - ' || vTextoGrupo || ' ' || pGrupo || ' (semestre ' || pSemestre || ') - ' || vCursoAca;
      fop.block(text => vTextoCabecera, border_bottom => 'solid', border_width => 0.05);
   end;

   procedure muestra_leyenda_asig(item c_asigs%rowtype, vSemestre varchar2, pCurso in number) is
      vClasesText          varchar2(4000);
   begin
      fop.block(item.asignatura_id || ', ' || item.asignatura || ':', font_size => 7, padding_top => 0.1);
      vClasesText := '<fo:inline color="white">___</fo:inline>';

      for c in c_clases_asig(item.asignatura_id, vSemestre, pCurso) loop
         vClasesText := vClasesText || c.dia_sem || ', ' || c.hora_inicio_str || '-' || c.hora_fin_str || ' (' || c.subgrupo || '); ';
      end loop;

      fop.block(vClasesText, font_size => 7, padding_top => 0.06);
   end;

   procedure leyenda_asigs(pSemestre in number, pCurso in number) is
   begin
      fop.block(' ', padding_top => 1);

      for item in c_asigs(pSemestre, pCurso) loop
         muestra_leyenda_asig(item, pSemestre, pCurso);
      end loop;
   end;

   procedure muestra_leyenda_asig_agr(item c_asigs%rowtype, vSemestre varchar2, vAgrupacion number) is
      vClasesText          varchar2(4000);
   begin
      fop.block(item.asignatura_id || ', ' || item.asignatura || ':', font_size => 7, padding_top => 0.1);
      vClasesText := '<fo:inline color="white">___</fo:inline>';

      for c in c_clases_asig_agrupacion(item.asignatura_id, vSemestre, vAgrupacion) loop
         vClasesText := vClasesText || c.dia_sem || ', ' || c.hora_inicio_str || '-' || c.hora_fin_str || ' (' || c.subgrupo || '); ';
      end loop;

      fop.block(vClasesText, font_size => 7, padding_top => 0.06);
   end;

   procedure leyenda_asigs_agrupacion(pSemestre in number, vAgrupacion in number) is
   begin
      fop.block(' ', padding_top => 1);

      for item in c_asigs_agrupacion(pSemestre, vAgrupacion) loop
         muestra_leyenda_asig_agr(item, pSemestre, vAgrupacion);
      end loop;
   end;

   procedure pie is
   begin
      fop.pageFlowClose;
      fop.pageBodyClose;
      fop.documentClose;
   end;

   function redondea_fecha_a_cuartos(vIniDate date)
      return date is
      fecha_redondeada          date;
   begin
      select trunc(vIniDate, 'HH') + (15 * round(to_char(trunc(vIniDate, 'MI'), 'MI') / 15)) / 1440
        into fecha_redondeada
        from dual;

      return fecha_redondeada;
   end;

   function con_varios_grupos
      return boolean is
   begin
      return instr(pGrupo, ',') <> 0;
   end;

   procedure calendario_gen(pSemestre in number, pCurso in number) is
      grupo_txt              varchar2(200);
      varios_grupos          boolean;
   begin
      varios_grupos := con_varios_grupos();
      vFechas := t_horario();

      for item in c_items_gen(pSemestre, pCurso) loop
         if (varios_grupos) then
            grupo_txt := item.grupo_id || ' ' || '(' || item.tipo_subgrupo_id || item.subgrupo_id || ') ' || item.codigo_aula;
         else
            grupo_txt := '(' || item.tipo_subgrupo_id || item.subgrupo_id || ') ' || item.codigo_aula;
         end if;

         vFechas.extend;
         vFechas(vFechas.count) := ItemHorarios(hor_contenido_item(redondea_fecha_a_cuartos(item.hora_inicio), redondea_fecha_a_cuartos(item.hora_fin), item.asignatura_id, grupo_txt), redondea_fecha_a_cuartos(item.inicio), redondea_fecha_a_cuartos(item.fin), null);
      end loop;

      if vFechas.count > 0 then
         vRdo := xpfpkg_horarios_academicos.drawTimetable(vFechas, 5, null, 'CA', 'PDF_APP_HORARIOS');
         xup_utils.dump(vRdo);
         dbms_lob.freeTemporary(vRdo);
      end if;
   end;

   procedure calendario_gen_agrupacion(pSemestre in number, pAgrupacion in number) is
      grupo_txt              varchar2(200);
      varios_grupos          boolean;
   begin
      varios_grupos := con_varios_grupos();
      vFechas := t_horario();

      for item in c_items_gen_agrupacion(pSemestre, pAgrupacion) loop
         if (varios_grupos) then
            grupo_txt := item.grupo_id || ' ' || '(' || item.tipo_subgrupo_id || item.subgrupo_id || ') ' || item.codigo_aula;
         else
            grupo_txt := '(' || item.tipo_subgrupo_id || item.subgrupo_id || ') ' || item.codigo_aula;
         end if;

         vFechas.extend;
         vFechas(vFechas.count) := ItemHorarios(hor_contenido_item(redondea_fecha_a_cuartos(item.hora_inicio), redondea_fecha_a_cuartos(item.hora_fin), item.asignatura_id, grupo_txt), redondea_fecha_a_cuartos(item.inicio), redondea_fecha_a_cuartos(item.fin), null);
      end loop;

      if vFechas.count > 0 then
         vRdo := xpfpkg_horarios_academicos.drawTimetable(vFechas, 5, null, 'CA', 'PDF_APP_HORARIOS');
         xup_utils.dump(vRdo);
         dbms_lob.freeTemporary(vRdo);
      end if;
   end;

   procedure calendario_det(vFechaIni date, vFechaFin date) is
      grupo_txt              varchar2(200);
      varios_grupos          boolean;
   begin
      varios_grupos := con_varios_grupos();
      vFechas := t_horario();

      for item in c_items_det(vFechaIni, vFechaFin, pCurso) loop
         if (varios_grupos) then
            grupo_txt := item.grupo_id || ' ' || '(' || item.tipo_subgrupo_id || item.subgrupo_id || ') ' || item.codigo_aula;
         else
            grupo_txt := '(' || item.tipo_subgrupo_id || item.subgrupo_id || ') ' || item.codigo_aula;
         end if;

         vFechas.extend;
         vFechas(vFechas.count) := ItemHorarios(hor_contenido_item(redondea_fecha_a_cuartos(item.hora_inicio), redondea_fecha_a_cuartos(item.hora_fin), item.asignatura_id, grupo_txt), redondea_fecha_a_cuartos(item.inicio), redondea_fecha_a_cuartos(item.fin), null);
      end loop;

      if vFechas.count > 0 then
         vRdo := xpfpkg_horarios_academicos.drawTimetable(vFechas, 5, null, 'CA', 'PDF_APP_HORARIOS_DET');
         xup_utils.dump(vRdo);
         dbms_lob.freeTemporary(vRdo);
      end if;
   end;

   procedure calendario_det_agrupacion(vFechaIni date, vFechaFin date, pAgrupacion in number) is
      grupo_txt              varchar2(200);
      varios_grupos          boolean;
   begin
      varios_grupos := con_varios_grupos();
      vFechas := t_horario();

      for item in c_items_det_agrupacion(vFechaIni, vFechaFin, pAgrupacion) loop
         if (varios_grupos) then
            grupo_txt := item.grupo_id || ' ' || '(' || item.tipo_subgrupo_id || item.subgrupo_id || ') ' || item.codigo_aula;
         else
            grupo_txt := '(' || item.tipo_subgrupo_id || item.subgrupo_id || ') ' || item.codigo_aula;
         end if;

         vFechas.extend;
         vFechas(vFechas.count) := ItemHorarios(hor_contenido_item(redondea_fecha_a_cuartos(item.hora_inicio), redondea_fecha_a_cuartos(item.hora_fin), item.asignatura_id, grupo_txt), redondea_fecha_a_cuartos(item.inicio), redondea_fecha_a_cuartos(item.fin), null);
      end loop;

      if vFechas.count > 0 then
         vRdo := xpfpkg_horarios_academicos.drawTimetable(vFechas, 5, null, 'CA', 'PDF_APP_HORARIOS_DET');
         xup_utils.dump(vRdo);
         dbms_lob.freeTemporary(vRdo);
      end if;
   end;

   procedure calcula_fechas_semestre(pSemestre in number) is
   begin
      select fecha_inicio, fecha_examenes_fin
        into vFechaIniSemestre, vFechaFinSemestre
        from hor_semestres_detalle, hor_estudios e
       where semestre_id = pSemestre
         and e.id = pEstudio
         and e.tipo_id = tipo_estudio_id;

      select next_day(trunc(vFechaIniSemestre) - 7, 'LUN')
        into vFechaIniSemestre
        from dual;
   end;

   function semana_tiene_clase(vIniDate date, vFinDate date, pCurso in number)
      return boolean is
      num_clases          number;
   begin
        select count( * )
          into num_clases
          from UJI_HORARIOS.HOR_ITEMS items, UJI_HORARIOS.HOR_ITEMS_DETALLE det, uji_horarios.hor_items_asignaturas asi
         where items.id = asi.item_id
           and asi.estudio_id = pEstudio
           and asi.curso_id = pCurso
           and ',' || pGrupo || ',' like '%,' || items.grupo_id || ',%'
           and items.semestre_id = pSemestre
           and items.dia_semana_id is not null
           and det.item_id = items.id
           and det.inicio > vIniDate
           and det.fin < vFinDate
      order by det.inicio;

      return num_clases > 0;
   end;

   function semana_tiene_clase_agr(vIniDate date, vFinDate date, pAgrupacion in number)
      return boolean is
      num_clases          number;
   begin
        select count( * )
          into num_clases
          from UJI_HORARIOS.HOR_ITEMS items, UJI_HORARIOS.HOR_ITEMS_DETALLE det, uji_horarios.hor_items_asignaturas asi, HOR_V_AGRUPACIONES_ITEMS agr_items
         where items.id = asi.item_id
           and asi.estudio_id = pEstudio
           and ',' || pGrupo || ',' like '%,' || items.grupo_id || ',%'
           and items.semestre_id = pSemestre
           and items.dia_semana_id is not null
           and det.item_id = items.id
           and items.id = agr_items.item_id
           and agr_items.agrupacion_id = pAgrupacion
           and det.inicio > vIniDate
           and det.fin < vFinDate
      order by det.inicio;

      return num_clases > 0;
   end;

   procedure info_calendario_det(vFechaIni date, vFechaFin date, pCurso in number) is
      vCursoAca                  varchar2(4000);
      vTitulacionNombre          varchar2(4000);
      vCursoNombre               varchar2(4000) := '';
      vTextoCabecera             varchar2(4000);
      vFechaIniStr               varchar2(4000) := to_char(vFechaIni, 'dd/mm/yyyy');
      vFechaFinStr               varchar2(4000) := to_char(vFechaFin - 1, 'dd/mm/yyyy');
      vNombreAgrupacion          varchar2(4000) := '';
   begin
      select nombre
        into vTitulacionNombre
        from hor_estudios
       where id = pEstudio;

      if (pAgrupacion is not null) then
         select 'Agrupació ' || nombre
           into vNombreAgrupacion
           from hor_agrupaciones
          where id = pAgrupacion;
      end if;

      vCursoAca := to_char(vCursoAcaNum) || '/' || to_char(vCursoAcaNum + 1);

      if pCurso in ('1', '3') then
         vCursoNombre := pCurso || 'er curs';
      elsif pCurso = '2' then
         vCursoNombre := '2on  curs';
      elsif pCurso = '4' then
         vCursoNombre := '4t curs';
      elsif pCurso is not null then
         vCursoNombre := pCurso || '?  curs';
      end if;

      vTextoCabecera := vTitulacionNombre || ' - ' || vCursoNombre || vNombreAgrupacion || ' - Grup ' || pGrupo || ' (semestre ' || pSemestre || ') - Del ' || vFechaIniStr || ' al ' || vFechaFinStr;
      fop.block(text => vTextoCabecera, border_bottom => 'solid', border_width => 0.05);
   end;

   procedure paginas_calendario_det(pSemestre in number, pCurso in number) is
      vFechaIniSemana          date;
      vFechaFinSemana          date;
   begin
      calcula_fechas_semestre(pSemestre);
      vFechaIniSemana := vFechaIniSemestre;
      vFechaFinSemana := vFechaIniSemana + 7;

      while vFechaFinSemana <= vFechaFinSemestre loop
         if semana_tiene_clase(vFechaIniSemana, vFechaFinSemana, pCurso) then
            info_calendario_det(vFechaIniSemana, vFechaFinSemana, pCurso);
            calendario_det(vFechaIniSemana, vFechaFinSemana);
            htp.p('<fo:block break-before="page" color="white">_</fo:block>');
         end if;

         vFechaIniSemana := vFechaFinSemana;
         vFechaFinSemana := vFechaFinSemana + 7;
      end loop;
   end;

   procedure paginas_calendario_det_agr(pSemestre in number, pCurso in number, pAgrupacion in number) is
      vFechaIniSemana          date;
      vFechaFinSemana          date;
   begin
      calcula_fechas_semestre(pSemestre);
      vFechaIniSemana := vFechaIniSemestre;
      vFechaFinSemana := vFechaIniSemana + 7;

      while vFechaFinSemana <= vFechaFinSemestre loop
         if semana_tiene_clase_agr(vFechaIniSemana, vFechaFinSemana, pAgrupacion) then
            info_calendario_det(vFechaIniSemana, vFechaFinSemana, pCurso);
            calendario_det_agrupacion(vFechaIniSemana, vFechaFinSemana, pAgrupacion);
            htp.p('<fo:block break-before="page" color="white">_</fo:block>');
         end if;
         vFechaIniSemana := vFechaFinSemana;
         vFechaFinSemana := vFechaFinSemana + 7;
      end loop;
   end;

   procedure pageBreak is
   begin
      if (not vFirstPageBreak) then
         htp.p('<fo:block break-before="page" color="white">_</fo:block>');
      end if;
      vFirstPageBreak := false;
   end;

   procedure impresionSemanaGenerica is
   begin
      if (pCurso is not null) then
         for c in cursosTitulacion(pEstudio, pCurso) loop
            if (pSemestre = 1
             or pSemestre = -1) then
               pageBreak();
               info_calendario_gen(1, c.curso_id, pAgrupacion);
               calendario_gen(1, c.curso_id);
               leyenda_asigs(1, c.curso_id);
            end if;

            if (pSemestre = 2
             or pSemestre = -1) then
               pageBreak();
               info_calendario_gen(2, c.curso_id, pAgrupacion);
               calendario_gen(2, c.curso_id);
               leyenda_asigs(2, c.curso_id);
            end if;
         end loop;
      else
         --fop.block('HI', font_weight => 'bold', text_align => 'center', font_size => 14, space_after => 0.5);
         for a in agrupacionesTitulacion(pEstudio, pAgrupacion) loop
            if (pSemestre = 1
             or pSemestre = -1) then
               pageBreak();
               info_calendario_gen(1, pCurso, a.agrupacion_id);
               calendario_gen_agrupacion(1, a.agrupacion_id);
               leyenda_asigs_agrupacion(1, a.agrupacion_id);
            end if;

            if (pSemestre = 2
             or pSemestre = -1) then
               pageBreak();
               info_calendario_gen(2, pCurso, a.agrupacion_id);
               calendario_gen_agrupacion(2, a.agrupacion_id);
               leyenda_asigs_agrupacion(2, a.agrupacion_id);
            end if;
         end loop;
      end if;
   end;

   procedure impresionSemanaDetalle is
   begin
      if (pCurso is not null) then
         for c in cursosTitulacion(pEstudio, pCurso) loop
            paginas_calendario_det(pSemestre, c.curso_id);
         end loop;
      else
         paginas_calendario_det_agr(pSemestre, pCurso, pAgrupacion);
      end if;

      leyenda_asigs(pSemestre, pCurso);
   end;
begin
   select count( * )
     into vSesion
     from apa_sesiones
    where sesion_id = pSesion
      and fecha < sysdate() - 0.33;

   if vSesion > 0 then
      calcula_curso_aca;
      cabecera;

      if (pGrupo = '-1') then
         select wm_concat(grupo_id)
           into pGrupo
           from (  select distinct i.grupo_id
                     from hor_items i join hor_items_asignaturas ia on i.id = ia.item_id
                    where ia.estudio_id = pEstudio
                 order by i.grupo_id);
      end if;

      if vEsGenerica then
         impresionSemanaGenerica;
      else
         impresionSemanaDetalle;
      end if;

      pie;
   else
      cabecera;
      fop.block('No tens permís per accedir a aquest document.', font_weight => 'bold', text_align => 'center', font_size => 14, space_after => 0.5);
      pie;
   end if;
exception
   when others then
      htp.p(sqlerrm);
end;


CREATE OR REPLACE procedure UJI_HORARIOS.HORARIOS_VALIDACION_PDF(name_array in euji_util.ident_arr2, value_array in euji_util.ident_arr2) is
   -- Parametros
   pEstudio constant           varchar2(40) := euji_util.euji_getparam(name_array, value_array, 'pEstudio');
   pSemestre constant          varchar2(40) := euji_util.euji_getparam(name_array, value_array, 'pSemestre');
   pGrupo constant             varchar2(40) := euji_util.euji_getparam(name_array, value_array, 'pGrupo');
   pCurso constant             varchar2(40) := euji_util.euji_getparam(name_array, value_array, 'pCurso');
   pSesion constant            varchar2(400) := euji_util.euji_getparam(name_array, value_array, 'pSesion');
   v_nombre                    varchar2(2000);
   vSesion                     number;

   procedure cabecera is
   begin
      owa_util.mime_header(ccontent_type => 'text/xml', bclose_header => true);
      fop.documentOpen(margin_left => 1.5,
                       margin_right => 1.5,
                       margin_top => 0.5,
                       margin_bottom => 0.5,
                       extent_before => 3,
                       extent_after => 1.5,
                       extent_margin_top => 3,
                       extent_margin_bottom => 1.5);
      fop.pageBodyOpen;
      htp.p(fof.documentheaderopen || fof.blockopen(space_after => 0.1) || fof.tableOpen || fof.tablecolumndefinition(column_width => 3) || fof.tablecolumndefinition(column_width => 15) || fof.tablebodyopen || fof.tableRowOpen || fof.tablecellopen || fof.block(fof.logo_uji, text_align => 'center') || fof.tablecellclose || fof.tablecellopen(display_align => 'center', padding => 0.4) || fof.block(text => 'Controls horaris', font_size => 12, font_weight => 'bold', font_family => 'Arial', text_align => 'center') || fof.block(text => v_nombre, font_size => 12, font_weight => 'bold', font_family => 'Arial', text_align => 'center') || fof.block(text => 'Curs: ' || pCurso || ' - Semestre: ' || pSemestre || ' - Grup: ' || pGrupo, font_size => 12, font_weight => 'bold', font_family => 'Arial', text_align => 'center') || fof.tablecellclose || fof.tableRowClose || fof.tablebodyclose || fof.tableClose || fof.blockclose || fof.documentheaderclose);
      -- El numero paginas se escribe a mano para poner el texto m?s peque?o
      htp.p(fof.documentfooteropen || fof.tableOpen || fof.tablecolumndefinition(column_width => 18) || fof.tablebodyopen || fof.tableRowOpen || fof.tablecellopen || fof.block(text => 'Pag. <fo:page-number /> de <fo:page-number-citation ref-id="lastBlock" />', text_align => 'right', font_weight => 'bold', font_size => 8) || fof.tablecellclose || fof.tableRowClose || fof.tablebodyclose || fof.tableClose || fof.documentfooterclose);
      fop.pageFlowOpen;
      fop.block(fof.white_space);
   end;

   procedure pie is
   begin
      htp.p('<fo:block  id="lastBlock" space-before="1cm" font-size="8pt">');
      htp.p('Data de generació: ' || to_char(sysdate, 'dd/mm/yyy hh24:mi'));
      fop.blockclose();
      fop.pageFlowClose;
      fop.pageBodyClose;
      fop.documentClose;
   end;

   function espacios(p_espacios in number)
      return varchar2 is
      v_rdo          varchar2(2000);
   begin
      v_rdo := null;

      for x in 1 .. p_espacios loop
         v_rdo := v_rdo || fof.white_space;
      end loop;

      return v_rdo;
   end;

   procedure p(p_texto in varchar2) is
   begin
      fop.block(espacios(3) || p_texto);
   end;

   procedure control_1 is
      v_aux          number := 0;

      cursor lista_errores_1 is
           select distinct asignatura_id, grupo_id, tipo_subgrupo_id || subgrupo_id subgrupo, decode(tipo_subgrupo_id, 'TE', 1, 'PR', 2, 'LA', 3, 'SE', 4, 0) tipo
             from uji_horarios.hor_items i, hor_items_asignaturas asi
            where i.id = asi.item_id
              and asi.curso_id = pCurso
              and semestre_id = pSemestre
              --and             grupo_id = pGrupo
              and pGrupo like '%' || grupo_id || '%'
              and tipo_subgrupo_id not in ('AV', 'TU')
              and dia_semana_id is null
              and estudio_id = pEstudio
         order by 1, 2, decode(tipo_subgrupo_id, 'TE', 1, 'PR', 2, 'LA', 3, 'SE', 4, 0);
   begin
      fop.block('Control 1 - Subgrups sense planificar', font_size => 12, font_weight => 'bold', border_bottom => 'solid');

      for x in lista_errores_1 loop
         v_aux := v_aux + 1;
         p(x.asignatura_id || ' ' || x.grupo_id || ' ' || x.subgrupo);
      end loop;

      if v_aux = 0 then
         fop.block('No hi ha cap error.', space_before => 0.5);
      elsif v_aux = 1 then
         fop.block('Trovat ' || v_aux || ' error.', space_before => 0.5);
      else
         fop.block('Trovats ' || v_aux || ' errors.', space_before => 0.5);
      end if;
   exception
      when others then
         p(sqlerrm);
   end;

   procedure control_2 is
      v_aux                 number := 0;
      v_asignatura          varchar2(20);

      cursor lista_errores_2_v1 is
           select asignatura_id, grupo_id, tipo_subgrupo_id || subgrupo_id subgrupo, decode(dia_semana_id, 1, 'Dilluns', 2, 'Dimarts', 3, 'Dimecres', 4, 'Dijous', 5, 'Divendres', 6, 'Dissabte', 7, 'Diumenge', 'Error') dia_semana, hora_inicio,
                  hora_fin, (select asi2.asignatura_id || ' ' || i2.tipo_subgrupo_id || i2.subgrupo_id grupo
                               from uji_horarios.hor_items i2, uji_horarios.hor_items_asignaturas asi2
                              where i.id <> i2.id
                                and i2.id = asi2.item_id
                                --and    i.curso_id = i2.curso_id
                                and i.semestre_id = i2.semestre_id
                                and i.grupo_id = i2.grupo_id
                                and i.tipo_subgrupo_id <> i2.tipo_subgrupo_id
                                and i.dia_semana_id = i2.dia_semana_id
                                and asi.estudio_id = asi2.estudio_id
                                and asi.asignatura_id = asi2.asignatura_id
                                and (to_number(to_char(i.hora_inicio, 'hh24mi')) between to_number(to_char(i2.hora_inicio, 'hh24mi')) and to_number(to_char(i2.hora_fin, 'hh24mi'))
                                  or to_number(to_char(i.hora_fin, 'hh24mi')) between to_number(to_char(i2.hora_inicio, 'hh24mi')) and to_number(to_char(i2.hora_fin, 'hh24mi')))
                                and rownum <= 1)
                               solape
             from uji_horarios.hor_items i, uji_horarios.hor_items_asignaturas asi
            where i.id = asi.item_id
              and asi.curso_id = pCurso
              and semestre_id = pSemestre
              --and      grupo_id = pGrupo
              and pGrupo like '%' || grupo_id || '%'
              and tipo_subgrupo_id in ('TE')
              and estudio_id = pEstudio
              and exists (select 1
                            from uji_horarios.hor_items i2, uji_horarios.hor_items_asignaturas asi2
                           where i.id <> i2.id
                             and i2.id = asi2.item_id
                             --and    i.curso_id = i2.curso_id
                             and i.semestre_id = i2.semestre_id
                             and i.grupo_id = i2.grupo_id
                             and i.tipo_subgrupo_id <> i2.tipo_subgrupo_id
                             and i.dia_semana_id = i2.dia_semana_id
                             and asi.estudio_id = asi2.estudio_id
                             and asi.asignatura_id = asi2.asignatura_id
                             and (to_number(to_char(i.hora_inicio, 'hh24mi')) between to_number(to_char(i2.hora_inicio, 'hh24mi')) and to_number(to_char(i2.hora_fin, 'hh24mi'))
                               or to_number(to_char(i.hora_fin, 'hh24mi')) between to_number(to_char(i2.hora_inicio, 'hh24mi')) and to_number(to_char(i2.hora_fin, 'hh24mi'))))
         order by 1, 2, 3;

      cursor lista_errores_2 is
           select distinct asignatura_id, grupo_id, tipo_subgrupo_id || subgrupo_id subgrupo, decode(dia_semana_id, 1, 'Dilluns', 2, 'Dimarts', 3, 'Dimecres', 4, 'Dijous', 5, 'Divendres', 6, 'Dissabte', 7, 'Diumenge', 'Error') dia_semana,
                           to_char(inicio, 'dd/mm/yyyy') fecha, to_char(inicio, 'hh24:mi') hora_inicio, to_char(fin, 'hh24:mi') hora_fin,
                           (select asi2.asignatura_id || ' ' || i2.tipo_subgrupo_id || i2.subgrupo_id || ' - ' || to_char(inicio, 'hh24:mi') || '-' || to_char(fin, 'hh24:mi') grupo
                              from uji_horarios.hor_items i2, uji_horarios.hor_items_asignaturas asi2, uji_horarios.hor_items_detalle det2
                             where i.id <> i2.id
                               and i2.id = det2.item_id
                               and i2.id = asi2.item_id
                               --and    i.curso_id = i2.curso_id
                               and i.semestre_id = i2.semestre_id
                               and i.grupo_id = i2.grupo_id
                               and i.tipo_subgrupo_id <> i2.tipo_subgrupo_id
                               and i.dia_semana_id = i2.dia_semana_id
                               and asi.estudio_id = asi2.estudio_id
                               and asi.asignatura_id = asi2.asignatura_id
                               and trunc(det.inicio) = trunc(det2.inicio)
                               and (to_number(to_char(det.inicio + 1 / 1440, 'hh24mi')) between to_number(to_char(det2.inicio, 'hh24mi')) and to_number(to_char(det2.fin, 'hh24mi'))
                                 or to_number(to_char(det.fin - 1 / 1440, 'hh24mi')) between to_number(to_char(det2.inicio, 'hh24mi')) and to_number(to_char(det2.fin, 'hh24mi')))
                               and rownum <= 1)
                              solape
             from uji_horarios.hor_items i, uji_horarios.hor_items_asignaturas asi, uji_horarios.hor_items_detalle det
            where i.id = asi.item_id
              and i.id = det.item_id
              and asi.curso_id = pCurso
              and semestre_id = pSemestre
              --and             grupo_id = pGrupo
              and pGrupo like '%' || grupo_id || '%'
              and tipo_subgrupo_id in ('TE')
              and estudio_id = pEstudio
              and exists (select 1
                            from uji_horarios.hor_items i2, uji_horarios.hor_items_asignaturas asi2, uji_horarios.hor_items_detalle det2
                           where i.id <> i2.id
                             and i2.id = det2.item_id
                             and i2.id = asi2.item_id
                             --and    i.curso_id = i2.curso_id
                             and i.semestre_id = i2.semestre_id
                             and i.grupo_id = i2.grupo_id
                             and i.tipo_subgrupo_id <> i2.tipo_subgrupo_id
                             and i.dia_semana_id = i2.dia_semana_id
                             and asi.estudio_id = asi2.estudio_id
                             and asi.asignatura_id = asi2.asignatura_id
                             and trunc(det.inicio) = trunc(det2.inicio)
                             and (to_number(to_char(det.inicio + 1 / 1440, 'hh24mi')) between to_number(to_char(det2.inicio, 'hh24mi')) and to_number(to_char(det2.fin, 'hh24mi'))
                               or to_number(to_char(det.fin - 1 / 1440, 'hh24mi')) between to_number(to_char(det2.inicio, 'hh24mi')) and to_number(to_char((det2.fin), 'hh24mi'))))
         order by 1, 2, 3, 5;

      /* 01/03/2016 se cambia la tabla hor_items_detalle por hor_v_items detalle ya que el trigger no mantiene sincronizada la tabla hor_items_detalle */
      cursor lista_errores_3 is
           select distinct asi.asignatura_id, det.grupo_id, det.tipo_subgrupo_id || det.subgrupo_id subgrupo,
                           decode(det.dia_semana_id, 1, 'Dilluns', 2, 'Dimarts', 3, 'Dimecres', 4, 'Dijous', 5, 'Divendres', 6, 'Dissabte', 7, 'Diumenge', 'Error') dia_semana, to_char(det.fecha, 'dd/mm/yyyy') fecha,
                           to_char(hora_inicio, 'hh24:mi') hora_inicio, to_char(hora_fin, 'hh24:mi') hora_fin,
                           (select asi2.asignatura_id || ' ' || it2.tipo_subgrupo_id || it2.subgrupo_id || ' - ' || to_char(it2.hora_inicio, 'hh24:mi') || '-' || to_char(it2.hora_fin, 'hh24:mi') grupo
                              from hor_v_items_detalle det2 join hor_items_asignaturas asi2 on det2.id = asi2.item_id join hor_items it2 on det2.id = it2.id
                             where det2.semestre_id = det.semestre_id
                               and det2.grupo_id = det.grupo_id
                               and det2.tipo_subgrupo_id <> det.tipo_subgrupo_id
                               and det2.dia_semana_id = det.dia_semana_id
                               and det2.docencia = 'S'
                               and trunc(det2.fecha) = trunc(det.fecha)
                               and asi2.asignatura_id = asi.asignatura_id
                               and asi2.estudio_id = asi.estudio_id
                               and (to_number(to_char(it.hora_inicio + 1 / 1440, 'hh24mi')) between to_number(to_char(it2.hora_inicio, 'hh24mi')) and to_number(to_char(it2.hora_fin, 'hh24mi'))
                                 or to_number(to_char(it.hora_fin - 1 / 1440, 'hh24mi')) between to_number(to_char(it2.hora_inicio, 'hh24mi')) and to_number(to_char((it2.hora_fin), 'hh24mi')))
                               and rownum <= 1)
                              solape
             from uji_horarios.hor_v_items_detalle det, uji_horarios.hor_items_asignaturas asi, uji_horarios.hor_items it
            where det.id = asi.item_id
              and det.id = it.id
              and asi.curso_id = pCurso
              and det.semestre_id = pSemestre
              and pGrupo like '%' || det.grupo_id || '%'
              and det.tipo_subgrupo_id in ('TE')
              and asi.estudio_id = pEstudio
              and det.docencia = 'S'
              --and asi.asignatura_id = 'EI1057'
              and exists (select det2.id
                            from hor_v_items_detalle det2 join hor_items_asignaturas asi2 on det2.id = asi2.item_id join hor_items it2 on det2.id = it2.id
                           where det2.semestre_id = det.semestre_id
                             and det2.grupo_id = det.grupo_id
                             and det2.tipo_subgrupo_id <> det.tipo_subgrupo_id
                             and det2.dia_semana_id = det.dia_semana_id
                             and det2.docencia = 'S'
                             and trunc(det2.fecha) = trunc(det.fecha)
                             and asi2.asignatura_id = asi.asignatura_id
                             and asi2.estudio_id = asi.estudio_id
                             and (to_number(to_char(it.hora_inicio + 1 / 1440, 'hh24mi')) between to_number(to_char(it2.hora_inicio, 'hh24mi')) and to_number(to_char(it2.hora_fin, 'hh24mi'))
                               or to_number(to_char(it.hora_fin - 1 / 1440, 'hh24mi')) between to_number(to_char(it2.hora_inicio, 'hh24mi')) and to_number(to_char((it2.hora_fin), 'hh24mi'))))
         order by 1, 2, 3, 5;
   begin
      fop.block('Control 2 - Solapament subgrups TE amb la resta', font_size => 12, font_weight => 'bold', border_bottom => 'solid', space_before => 0.5);
      v_asignatura := 'PRIMERA';

      for x in lista_errores_3 loop
         v_aux := v_aux + 1;

         if v_aux <> 1
        and v_asignatura <> x.asignatura_id then
            p(' ');
         end if;

         p(x.asignatura_id || ' ' || x.grupo_id || ' ' || x.subgrupo || ' ' || x.dia_semana || ' - ' || x.fecha || ' ' || x.hora_inicio || '  ->  solapa amb: ' || x.solape);
         v_asignatura := x.asignatura_id;
      end loop;

      if v_aux = 0 then
         fop.block('No hi ha cap error.', space_before => 0.5);
      elsif v_aux = 1 then
         fop.block('Trovat ' || v_aux || ' error.', space_before => 0.5);
      else
         fop.block('Trovats ' || v_aux || ' errors.', space_before => 0.5);
      end if;
   exception
      when others then
         p(sqlerrm);
   end;

   procedure control_3 is
      v_aux                 number := 0;
      v_asignatura          varchar2(20);
      v_control             number;

      cursor lista_asignaturas is
           select distinct asi.asignatura_id
             from uji_horarios.hor_items i, uji_horarios.hor_items_asignaturas asi, uji_horarios.hor_items_detalle det
            where i.id = asi.item_id
              and i.id = det.item_id
              and asi.curso_id = pCurso
              and semestre_id = pSemestre
              --and             grupo_id = pGrupo
              and pGrupo like '%' || grupo_id || '%'
              and estudio_id = pEstudio
         order by 1;

      cursor lista_errores_3(p_asignatura in varchar2) is
           select *
             from (  select asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo, creditos, horas_totales, sum(horas_detalle) horas_detalle, decode(tipo, 'S', 'Semestral', 'A', 'Anual') tipo
                       from (select asi.asignatura_id, grupo_id, tipo_subgrupo_id, tipo_subgrupo_id || subgrupo_id subgrupo, decode(tipo_subgrupo_id, 'TE', crd_te, 'PR', crd_pr, 'LA', crd_la, 'SE', crd_se, 'TU', crd_tu, crd_ev) creditos,
                                    decode(tipo_subgrupo_id, 'TE', crd_te, 'PR', crd_pr, 'LA', crd_la, 'SE', crd_se, 'TU', crd_tu, crd_ev) / decode(ad.tipo, 'A', 2, 1) * 10 horas_totales, (fin - inicio) * 24 horas_detalle, ad.tipo tipo
                               from uji_horarios.hor_items i, uji_horarios.hor_items_asignaturas asi, uji_horarios.hor_items_detalle det, uji_horarios.hor_ext_asignaturas_detalle ad
                              where i.id = asi.item_id
                                and i.id = det.item_id
                                and asi.curso_id = pCurso
                                and semestre_id = pSemestre
                                --and    grupo_id = pGrupo
                                and pGrupo like '%' || grupo_id || '%'
                                and estudio_id = pEstudio
                                and asi.asignatura_id = ad.asignatura_id
                                and asi.asignatura_id = p_asignatura
                                and ad.tipo = 'S'
                                and dia_semana_id is not null)
                   group by asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo, creditos, tipo, horas_totales
                     having horas_totales not between sum(horas_detalle) * 0.95 and sum(horas_detalle) * 1.05
                   union all
                     select asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo, max(creditos) creditos, max(horas_totales) horas_totales, sum(horas_detalle) horas_detalle, decode(tipo, 'S', 'Semestral', 'A', 'Anual') tipo
                       from (  select asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo, creditos, horas_totales, sum(horas_detalle) horas_detalle, tipo, null semestre_id
                                 from (select asi.asignatura_id, grupo_id, tipo_subgrupo_id, tipo_subgrupo_id || subgrupo_id subgrupo,
                                              decode(tipo_subgrupo_id, 'TE', crd_te, 'PR', crd_pr, 'LA', crd_la, 'SE', crd_se, 'TU', crd_tu, crd_ev) creditos,
                                              decode(tipo_subgrupo_id, 'TE', crd_te, 'PR', crd_pr, 'LA', crd_la, 'SE', crd_se, 'TU', crd_tu, crd_ev) * 10 horas_totales, (fin - inicio) * 24 horas_detalle, ad.tipo tipo, semestre_id
                                         from uji_horarios.hor_items i, uji_horarios.hor_items_asignaturas asi, uji_horarios.hor_items_detalle det, uji_horarios.hor_ext_asignaturas_detalle ad
                                        where i.id = asi.item_id
                                          and i.id = det.item_id
                                          and asi.curso_id = pCurso
                                          --and    semestre_id = pSemestre
                                          --and    grupo_id = pGrupo
                                          and pGrupo like '%' || grupo_id || '%'
                                          and estudio_id = pEstudio
                                          and asi.asignatura_id = ad.asignatura_id
                                          and asi.asignatura_id = p_asignatura
                                          and ad.tipo = 'A'
                                          and dia_semana_id is not null)
                             group by asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo, creditos, tipo, horas_totales --,
                               --semestre_id
                               having horas_totales not between sum(horas_detalle) * 0.95 and sum(horas_detalle) * 1.05)
                   group by asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo, tipo)
         order by 1, decode(tipo_subgrupo_id, 'TE', 1, 'PR', 2, 'LA', 3, 'SE', 4, 'TU', 5, 6), 3;
   begin
      v_control := 0;
      fop.block('Control 3 - Control d''hores planificades per subgrup', font_size => 12, font_weight => 'bold', border_bottom => 'solid', space_before => 0.5);
      v_asignatura := 'PRIMERA';
      v_control := 1;

      for asi in lista_asignaturas loop
         v_control := 2;

         for x in lista_errores_3(asi.asignatura_id) loop
            v_control := 3;
            v_aux := v_aux + 1;

            if v_aux <> 1
           and v_asignatura <> x.asignatura_id then
               p(' ');
            end if;

            if x.horas_totales > x.horas_detalle then
               p(x.asignatura_id || ' ' || x.grupo_id || ' ' || x.subgrupo || ' -' || x.tipo || '- ' || 'Planificades ' || round(to_char(x.horas_totales - x.horas_detalle), 3) || ' hores de menys, cal planificar-ne ' || x.horas_totales);
            else
               p(x.asignatura_id || ' ' || x.grupo_id || ' ' || x.subgrupo || ' -' || x.tipo || '- ' || 'Planificades ' || round(to_char(x.horas_detalle - x.horas_totales), 3) || ' hores de més, cal planificar-ne ' || x.horas_totales);
            end if;

            v_asignatura := x.asignatura_id;
         end loop;
      end loop;

      v_control := 6;

      if v_aux = 0 then
         fop.block('No hi ha cap error.', space_before => 0.5);
      elsif v_aux = 1 then
         fop.block('Trovat ' || v_aux || ' error.', space_before => 0.5);
      else
         fop.block('Trovats ' || v_aux || ' errors.', space_before => 0.5);
      end if;
   exception
      when others then
         p(v_control || '-' || sqlerrm || dbms_utility.format_error_backtrace);
   end;

   procedure control_4 is
      v_aux                 number := 0;
      v_asignatura          varchar2(20);

      cursor lista_errores_4 is
           select asi.asignatura_id, grupo_id, i.tipo_subgrupo_id, subgrupo_id, to_char(hora_inicio, 'hh24:mi') hora_inicio,
                  decode(dia_semana_id, 1, 'Dilluns', 2, 'Dimarts', 3, 'Dimecres', 4, 'Dijous', 5, 'Divendres', 6, 'Dissabte', 7, 'Diumenge', 'Error') dia_semana, dia_semana_id
             from uji_horarios.hor_items i, uji_horarios.hor_items_asignaturas asi
            where i.id = asi.item_id
              and asi.curso_id = pCurso
              and semestre_id = pSemestre
              --and      grupo_id = pGrupo
              and pGrupo like '%' || grupo_id || '%'
              and estudio_id = pEstudio
              and dia_semana_id is not null
              and aula_planificacion_id is null
         order by 1, decode(tipo_subgrupo_id, 'TE', 1, 'PR', 2, 'LA', 3, 'SE', 4, 'TU', 5, 6), 4;
   begin
      fop.block('Control 4 - Control d''assignació d''aules per subgrup', font_size => 12, font_weight => 'bold', border_bottom => 'solid', space_before => 0.5);
      v_asignatura := 'PRIMERA';

      for x in lista_errores_4 loop
         v_aux := v_aux + 1;

         if v_aux <> 1
        and v_asignatura <> x.asignatura_id then
            p(' ');
         end if;

         p(x.asignatura_id || ' ' || x.grupo_id || ' ' || x.tipo_subgrupo_id || x.subgrupo_id || ' ' || x.dia_semana || ' ' || x.hora_inicio || ' - no te aula assignada');
         v_asignatura := x.asignatura_id;
      end loop;
   exception
      when others then
         p(sqlerrm);
   end;

   procedure control_5 is
      v_aux                 number := 0;
      v_aula                number;
      v_dia_semana          number;
      v_hora                varchar2(20);
      v_txt                 varchar2(2000);

      cursor lista_errores_5 is
           select dia_semana_id, trunc(det.inicio) fecha, to_char(hora_inicio, 'hh24:mi') hora_inicio, aula_planificacion_id,
                  decode(dia_semana_id, 1, 'Dilluns', 2, 'Dimarts', 3, 'Dimecres', 4, 'Dijous', 5, 'Divendres', 6, 'Dissabte', 7, 'Diumenge', 'Error') dia_semana,
                  nvl(a.nombre, 'Aula mal assignada a l''estudi (' || aula_planificacion_id || ')') nombre_aula
             from uji_horarios.hor_items i, uji_horarios.hor_items_asignaturas asi, uji_horarios.hor_items_detalle det, uji_horarios.hor_aulas a
            where i.id = asi.item_id
              and i.id = det.item_id
              and asi.curso_id = pCurso
              and semestre_id = pSemestre
              --and      grupo_id = pGrupo
              and pGrupo like '%' || grupo_id || '%'
              and estudio_id = pEstudio
              and dia_semana_id is not null
              and aula_planificacion_id is not null
              and aula_planificacion_id = a.id(+)
         group by --asi.asignatura_id,
                 grupo_id, dia_semana_id, trunc(det.inicio), to_char(hora_inicio, 'hh24:mi'), aula_planificacion_id, a.nombre
           having count( * ) > 1
         order by 5, 1, 3, 2;

      cursor lista_subgrupos(p_dia_Semana in number, p_fecha in date, p_hora in varchar2, p_aula in number) is
           select nvl(comun_texto, asignatura_id) asignatura_id, grupo_id, tipo_subgrupo_id || subgrupo_id subgrupo
             from uji_horarios.hor_items i, uji_horarios.hor_items_asignaturas asi, uji_horarios.hor_items_detalle det
            where i.id = asi.item_id
              and i.id = det.item_id
              and dia_semana_id = p_dia_semana
              and trunc(inicio) = p_fecha
              and to_char(hora_inicio, 'hh24:mi') = p_hora
              and aula_planificacion_id = p_aula
              and estudio_id = pEstudio
         order by 1, 2;

      function numero_eventos_no_compartidos(p_dia_Semana in number, p_fecha in date, p_hora in varchar2, p_aula in number) return number is
         numero number;
      begin
          select count(distinct com.grupo_comun_id)
            into numero
             from uji_horarios.hor_items i, uji_horarios.hor_items_asignaturas asi, uji_horarios.hor_items_detalle det, hor_ext_asignaturas_comunes com
            where i.id = asi.item_id
              and i.id = det.item_id
              and asi.asignatura_id = com.asignatura_id
              and dia_semana_id = p_dia_semana
              and trunc(inicio) = p_fecha
              and to_char(hora_inicio, 'hh24:mi') = p_hora
              and aula_planificacion_id = p_aula
              and estudio_id = pEstudio;

         return numero;
      end;
   begin
      fop.block('Control 5 - Control de solapament en aules', font_size => 12, font_weight => 'bold', border_bottom => 'solid', space_before => 0.5);
      v_aula := 0;
      v_dia_semana := 0;
      v_hora := '';

      for x in lista_errores_5 loop
         v_aux := v_aux + 1;

         if v_aux <> 1
        and not (v_aula = x.aula_planificacion_id
             and v_dia_semana = x.dia_semana_id
             and v_hora = x.hora_inicio) then
            p(' ');
         end if;

         v_txt := null;

         if 1 = 2 then
            p(x.nombre_aula || ' - ' || to_char(x.fecha, 'dd/mm/yyyy') || ' ' || x.dia_semana || ' ' || x.hora_inicio);

            for det in lista_subgrupos(x.dia_semana_id, x.fecha, x.hora_inicio, x.aula_planificacion_id) loop
               p(espacios(30) || det.grupo_id || ' ' || det.asignatura_id || '-' || det.subgrupo);
            end loop;
         else
            if (numero_eventos_no_compartidos(x.dia_semana_id, x.fecha, x.hora_inicio, x.aula_planificacion_id) > 1) then
               for det in lista_subgrupos(x.dia_semana_id, x.fecha, x.hora_inicio, x.aula_planificacion_id) loop
                  v_txt := v_txt || det.asignatura_id || '--- ' || det.grupo_id || ' ' || det.subgrupo || ' | ';
               end loop;

               v_txt := trim(v_txt);
               v_txt := substr(v_txt, 1, length(v_txt) - 2);
               p(x.nombre_aula || ' - ' || to_char(x.fecha, 'dd/mm/yyyy') || ' ' || x.dia_semana || ' ' || x.hora_inicio || ' ==> ' || v_txt);
            end if;
         end if;

         v_aula := x.aula_planificacion_id;
         v_dia_semana := x.dia_semana_id;
         v_hora := x.hora_inicio;
      end loop;
   exception
      when others then
         p(sqlerrm);
   end;

   procedure control_6 is
      v_aux          number;

      cursor control_6 is
           select distinct asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo_id, trunc(inicio) fecha,
                           decode(dia_semana_id, 1, 'Dilluns', 2, 'Dimarts', 3, 'Dimecres', 4, 'Dijous', 5, 'Divendres', 6, 'Dissabte', 7, 'Diumenge', 'Error') dia_semana
             from hor_items i, hor_items_asignaturas a, hor_items_detalle d
            where i.id = a.item_id
              and detalle_manual = 0
              and estudio_id = pEstudio
              and curso_id = pCurso
              and semestre_id = pSemestre
              and pGrupo like '%' || grupo_id || '%'
              and i.id = d.item_id
              and trunc(d.inicio) in (select fecha
                                        from hor_v_items_DEtalle
                                       where id = i.id
                                         and docencia = 'N')
         order by asignatura_id, grupo_id, decode(tipo_subgrupo_id, 'TE', 1, 'PR', 2, 'LA', 3, 'SE', 4, 'TU', 5, 6), subgrupo_id;
   begin
      fop.block('Control 6 - Control de items amb data incorrecta', font_size => 12, font_weight => 'bold', border_bottom => 'solid', space_before => 0.5);
      v_aux := 0;

      for x in control_6 loop
         p(x.asignatura_id || ' ' || x.grupo_id || ' ' || x.tipo_subgrupo_id || x.subgrupo_id || ' del ' || x.dia_semana || ' te problemes amb la data ' || to_char(x.fecha, 'dd/mm/yyyy'));
         v_aux := v_aux + 1;
      end loop;

      if v_aux > 0 then
         p(' ');
         p('Trovats ' || v_aux || ' errors.');
         p(' ');
      end if;
   exception
      when others then
         p(sqlerrm);
   end;


   procedure control_7 is
      v_aux          number;

      cursor control_7(p_estudio number) is
           select ia.asignatura_id, I.GRUPO_ID, i.tipo_subgrupo_id, i.subgrupo_id, decode(dia_semana_id, 1, 'Dilluns', 2, 'Dimarts', 3, 'Dimecres', 4, 'Dijous', 5, 'Divendres', 6, 'Dissabte', 7, 'Diumenge', 'Error') dia_semana, i.semestre_id,
                  to_char(i.hora_inicio, 'HH24:MI') hora, count( * )
             from hor_items i join hor_items_asignaturas ia on i.id = ia.item_id
            where ia.estudio_id = p_estudio
         group by ia.asignatura_id, I.GRUPO_ID, i.tipo_subgrupo_id, i.subgrupo_id, i.semestre_id, decode(dia_semana_id, 1, 'Dilluns', 2, 'Dimarts', 3, 'Dimecres', 4, 'Dijous', 5, 'Divendres', 6, 'Dissabte', 7, 'Diumenge', 'Error'), to_char(i.hora_inicio, 'HH24:MI')
           having count( * ) > 1
         order by i.semestre_id, ia.asignatura_id, I.GRUPO_ID, i.tipo_subgrupo_id, i.subgrupo_id, i.semestre_id;
   begin
      fop.block('Control 7 - Horaris del mateix subgrup de la mateixa assignatura amb la mateixa hora', font_size => 12, font_weight => 'bold', border_bottom => 'solid', space_before => 0.5);
      v_aux := 0;
      p(' ');
      for x in control_7(pEstudio) loop
         p(x.asignatura_id || ' ' || x.grupo_id || ' ' || x.tipo_subgrupo_id || x.subgrupo_id || ' ' || x.dia_semana || ' hora inici  ' || x.hora || ' semestre ' || x.semestre_id);
         v_aux := v_aux + 1;
      end loop;

      if v_aux > 0 then
         p(' ');
         p('Trovats ' || v_aux || ' errors.');
      else
         p('No s''ha trobat cap error');
      end if;
   exception
      when others then
         p(dbms_utility.format_error_backtrace);
         p(sqlerrm);
   end;

   procedure advertencia_1 is
      v_aux                    number := 0;
      v_asignatura             varchar2(20);
      v_tipo_subgrupo          varchar2(20);
      v_subgrupo               number;

      cursor lista_advertencia_1 is
           select asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo_id
             from uji_horarios.hor_items i, uji_horarios.hor_items_asignaturas asi
            where i.id = asi.item_id
              and asi.curso_id = pCurso
              and semestre_id = pSemestre
              --and      grupo_id = pGrupo
              and pGrupo like '%' || grupo_id || '%'
              and estudio_id = pEstudio
              and dia_semana_id is not null
              and aula_planificacion_id is not null
         group by asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo_id
           having count(distinct aula_planificacion_id) > 1;

      cursor lista_aulas(p_asignatura in varchar2, p_tipo_subgrupo in varchar2, p_subgrupo in number) is
           select distinct nvl(a.nombre, 'Aula mal assignada a l''estudi (' || aula_planificacion_id || ')') nombre_aula
             from uji_horarios.hor_items i, uji_horarios.hor_items_asignaturas asi, uji_horarios.hor_aulas a
            where i.id = asi.item_id
              and i.aula_planificacion_id = a.id(+)
              and asi.curso_id = pCurso
              and semestre_id = pSemestre
              --and             grupo_id = pGrupo
              and pGrupo like '%' || grupo_id || '%'
              and estudio_id = pEstudio
              and dia_semana_id is not null
              and aula_planificacion_id is not null
              and asignatura_id = p_asignatura
              and tipo_subgrupo_id = p_tipo_subgrupo
              and subgrupo_id = p_subgrupo
         order by 1;
   begin
      fop.block('Advertencia 1 - Classes d''un subgrup en aules diferents', font_size => 12, font_weight => 'bold', border_bottom => 'solid', space_before => 0.5);
      v_aux := v_aux + 1;
      v_asignatura := 'PRIMERA';
      v_tipo_subgrupo := 'XX';
      v_subgrupo := 0;

      for x in lista_advertencia_1 loop
         v_aux := v_aux + 1;

         if v_aux <> 1
        and not (v_asignatura = x.asignatura_id
             and v_tipo_subgrupo = x.tipo_subgrupo_id
             and v_subgrupo = x.subgrupo_id) then
            p(' ');
         end if;

         for det in lista_aulas(x.asignatura_id, x.tipo_subgrupo_id, x.subgrupo_id) loop
            p(x.asignatura_id || ' - ' || x.grupo_id || ' ' || x.tipo_subgrupo_id || x.subgrupo_id || ' ' || det.nombre_aula);
         end loop;

         v_asignatura := x.asignatura_id;
         v_tipo_subgrupo := x.tipo_subgrupo_id;
         v_subgrupo := x.subgrupo_id;
      end loop;
   exception
      when others then
         p(sqlerrm);
   end;
begin
   select count( * )
     into vSesion
     from apa_sesiones
    where sesion_id = pSesion
      and fecha < sysdate() - 0.33;

   if vSesion > 0 then
      select nombre
        into v_nombre
        from hor_estudios
       where id = pEstudio;

      cabecera;
      control_1;
      control_2;
      control_3;
      control_4;
      control_5;
      control_6;
      control_7;
      advertencia_1;
      pie;
   else
      cabecera;
      fop.block('No tens permís per accedir a aquest document.', font_weight => 'bold', text_align => 'center', font_size => 14, space_after => 0.5);
      pie;
   end if;
exception
   when others then
      htp.p(sqlerrm || dbms_utility.format_error_backtrace);
end;



CREATE OR REPLACE PROCEDURE UJI_HORARIOS.pod_consulta_asignaturas_pdf (
   name_array    IN   euji_util.ident_arr2,
   value_array   IN   euji_util.ident_arr2
) IS
   v_aux           NUMBER;
   -- pProfesor       number          := euji_util.euji_getparam (name_array, value_array, 'pProfesor');
   pDepartamento   number          := euji_util.euji_getparam (name_array, value_array, 'pDepartamento');
   pArea           number          := euji_util.euji_getparam (name_array, value_array, 'pArea');
   pSesion         VARCHAR2 (400)  := euji_util.euji_getparam (name_array, value_array, 'pSesion');
   v_nombre        varchar2 (2000);
   vSesion         number;
   vCurso          number;
   vCreditos       number;
   vAcum           number;
   vAcum2          number;
   vAcum3          number;
   vAcum4          number;
   vAcum5          number;
   vDepAcum        number;
   vDepAcum2       number;
   vDepAcum3       number;
   vDepAcum4       number;
   vDepAcum5       number;
   vFechas         t_horario;
   vRdo            clob;

   cursor lista_areas (p_Area in number) is
      select   *
      from     hor_areas
      where    departamento_id = pDepartamento
      and      id = p_Area
      order by nombre;

   cursor lista_subgrupos (p_Area in number) is
      select   nvl (comun_texto, ia.asignatura_id) asignatura_txt, min (ia.asignatura_id) asignatura_id,
               hor_buscar_nombre (nvl (comun_texto, ia.asignatura_id)) asignatura, area_id, grupo_id, tipo_subgrupo_id,
               subgrupo_id, creditos
      from     hor_v_asignaturas_area aa,
               hor_items i,
               hor_items_asignaturas ia
      where    i.id = ia.item_id
      and      area_id = p_area
      and      ia.asignatura_id = aa.asignatura_id
      -- TODO Eliminar condicion
      --and      ia.asignatura_id in ('PS1048')
      group by nvl (comun_texto, ia.asignatura_id),
               --asignatura,
               area_id,
               grupo_id,
               tipo_subgrupo_id,
               subgrupo_id,
               creditos
      order by 1,
               2,
               3,
               4,
               5,
               decode (tipo_subgrupo_id, 'TE', 1, 'PR', 2, 'LA', 3, 'SE', 4, 'TU', 5, 6),
               7;

   function formatear (p_entrada in number)
      return varchar2 is
      v_rdo   varchar2 (100);
   begin
      v_rdo := replace (to_char (p_entrada, '9990.00'), '.', ',');
      return (v_rdo);
   end;

   procedure cabecera is
   begin
      owa_util.mime_header (ccontent_type => 'text/xml', bclose_header => true);
      fop.documentOpen (page_width => 29.7, page_height => 21, margin_left => 1.5, margin_right => 1.5,
                        margin_top => 0.5, margin_bottom => 0.5, extent_before => 3, extent_after => 1.5,
                        extent_margin_top => 3, extent_margin_bottom => 1.5);
      fop.pageBodyOpen;
      htp.p (fof.documentheaderopen || fof.blockopen (space_after => 0.1) || fof.tableOpen
             || fof.tablecolumndefinition (column_width => 3) || fof.tablecolumndefinition (column_width => 24)
             || fof.tablebodyopen || fof.tableRowOpen || fof.tablecellopen
             || fof.block (fof.logo_uji, text_align => 'center') || fof.tablecellclose
             || fof.tablecellopen (display_align => 'center', padding => 0.4)
             || fof.block (text => v_nombre, font_size => 16, font_weight => 'bold', font_family => 'Arial',
                           text_align => 'center')
             || fof.block (text => 'Informe POD per assignatura - Curs: ' || vCurso || '/'
                            || substr (to_char (vCurso + 1), 3),
                           font_size => 16, font_weight => 'bold', font_family => 'Arial', text_align => 'center')
             || fof.tablecellclose || fof.tableRowClose || fof.tablebodyclose || fof.tableClose || fof.blockclose
             || fof.documentheaderclose);
      -- El numero paginas se escribe a mano para poner el texto m?s peque?o
      htp.p (fof.documentfooteropen || fof.tableOpen || fof.tablecolumndefinition (column_width => 18)
             || fof.tablebodyopen || fof.tableRowOpen || fof.tablecellopen
             || fof.block (text => 'Pag. <fo:page-number /> de <fo:page-number-citation ref-id="lastBlock" />',
                           text_align => 'right', font_weight => 'bold', font_size => 8)
             || fof.tablecellclose || fof.tableRowClose || fof.tablebodyclose || fof.tableClose
             || fof.documentfooterclose);
      fop.pageFlowOpen;
      fop.block (fof.white_space);
   end;

   procedure pie is
   begin
      htp.p ('<fo:block  id="lastBlock" space-before="1cm" font-size="8pt">');
      htp.p ('Data de generació: ' || to_char (sysdate, 'dd/mm/yyy hh24:mi'));
      fop.blockclose ();
      fop.pageFlowClose;
      fop.pageBodyClose;
      fop.documentClose;
   end;

   function espacios (p_espacios in number)
      return varchar2 is
      v_rdo   varchar2 (2000);
   begin
      v_rdo := null;

      for x in 1 .. p_espacios loop
         v_rdo := v_rdo || fof.white_space;
      end loop;

      return v_rdo;
   end;

   procedure mostrar_subgrupos is
      v_asi_ant          varchar2 (100);
      v_border           varchar2 (100);
      v_creditos         number;
      v_creditos_Area    number;
      v_porcentaje       number;
      v_acum_1           number;
      v_acum_2           number;
      v_acum_3           number;
      v_area_1           number;
      v_area_2           number;
      v_area_3           number;
      v_area_1_porcent   number;
   begin
      for a in lista_areas (pArea) loop
         fop.block ('Area de coneixement: ' || fof.bold (a.nombre), font_size => 16, space_after => 0.2);
         fop.tableOpen;
         fop.tablecolumndefinition (column_width => 4);
         fop.tablecolumndefinition (column_width => 8);
         fop.tablecolumndefinition (column_width => 1);
         fop.tablecolumndefinition (column_width => 1);
         fop.tablecolumndefinition (column_width => 2);
         fop.tablecolumndefinition (column_width => 2);
         fop.tablecolumndefinition (column_width => 2);
         fop.tablecolumndefinition (column_width => 2);
         fop.tablecolumndefinition (column_width => 3);
         fop.tablecolumndefinition (column_width => 2);
         fop.tablebodyopen;
         fop.tableRowOpen;
         fop.tablecellopen;
         fop.block (fof.bold ('Codi'), font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold ('Nom assignatura'), font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold ('Grup'), font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold ('Tipus'), font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold ('Crèdits a impartir (A)'), font_size => 10, text_align => 'center');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold ('Crèdits assignats (B)'), font_size => 10, text_align => 'center');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold ('(B) - (A)'), font_size => 10, text_align => 'center');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold ('Crèdits assignats àrea (C)'), font_size => 10, text_align => 'center');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold ('(C) / (A)'), font_size => 10, text_align => 'center');
         fop.tablecellclose;
         fop.tableRowClose;
         v_asi_ant := 'XX';
         v_acum_1 := 0;
         v_acum_2 := 0;
         v_acum_3 := 0;
         v_area_1 := 0;
         v_area_1_porcent := 0;
         v_area_2 := 0;
         v_area_3 := 0;

         for det in lista_subgrupos (a.id) loop
            /*select nvl (sum (creditos), 0), nvl (sum (creditos / cuantos), 0)
              into   v_creditos, v_creditos_Area
              from   (select distinct asignatura_id, grupo_id, tipo_subgrupo_id || subgrupo_id subgrp, d.profesor_id,
                                      nvl (creditos_detalle, d.creditos) creditos, area_id,
                                      count (distinct profesor_id) over (partition by asignatura_id, grupo_id, tipo_subgrupo_id
                                                                                                               || subgrupo_id)
                                                                                                                  cuantos*/
            select nvl (sum (nvl (creditos_detalle, creditos / cuantos)), 0)
                                                                            --nvl (sum (nvl (creditos_detalle, creditos / cuantos)), 0)
            ,
                   nvl (sum (decode (area_id, a.id, nvl (creditos_detalle, creditos / cuantos), 0)), 0)
            into   v_creditos,
                   v_creditos_area
            --v_creditos_Area
            from   (select distinct nvl (comun_texto, asignatura_id) asignatura_id, grupo_id,
                                    tipo_subgrupo_id || subgrupo_id subgrp, d.profesor_id, creditos_detalle, d.creditos,
                                    area_id,
                                    count (distinct profesor_id) over (partition by asignatura_id, grupo_id, tipo_subgrupo_id
                                                                                                             || subgrupo_id)
                                                                                                                cuantos
                    from            hor_v_items_prof_detalle d,
                                    hor_profesores p,
                                    hor_items_asignaturas asi,
                                    hor_items i
                    where           d.item_id in (
                                       select i.id
                                       from   hor_items i,
                                              hor_items_asignaturas asi2
                                       where  i.id = asi2.item_id
                                       and    asignatura_id = det.asignatura_id
                                       and    grupo_id = det.grupo_id
                                       and    tipo_subgrupo_id = det.tipo_subgrupo_id
                                       and    subgrupo_id = det.subgrupo_id)
                    and             profesor_id = p.id
                    and             d.item_id = asi.item_id
                    and             d.item_id = i.id
                    and             semestre_id =
                                       (select min (semestre_id)
                                        from   hor_items i,
                                               hor_items_asignaturas asi2
                                        where  i.id = asi2.item_id
                                        and    asignatura_id = det.asignatura_id
                                        and    grupo_id = det.grupo_id
                                        and    tipo_subgrupo_id = det.tipo_subgrupo_id
                                        and    subgrupo_id = det.subgrupo_id))
                                                                              --where  area_id = a.id
            ;

            /*
            select                                                    --nvl (sum (nvl (creditos_detalle, creditos)), 0),
                   nvl (sum (nvl (creditos_detalle, creditos / cuantos)), 0)
            into                                                                                           --v_creditos,
                   v_creditos_Area
            from   (select distinct nvl (comun_texto, asignatura_id) asignatura_id, grupo_id,
                                    tipo_subgrupo_id || subgrupo_id subgrp, d.profesor_id, creditos_detalle, d.creditos,
                                    area_id,
                                    count (distinct profesor_id) over (partition by asignatura_id, grupo_id, tipo_subgrupo_id
                                                                                                             || subgrupo_id)
                                                                                                                cuantos
                    from            hor_v_items_prof_detalle d,
                                    hor_profesores p,
                                    hor_items_asignaturas asi,
                                    hor_items i
                    where           d.item_id in (
                                       select i.id
                                       from   hor_items i,
                                              hor_items_asignaturas asi2
                                       where  i.id = asi2.item_id
                                       and    asignatura_id = det.asignatura_id
                                       and    grupo_id = det.grupo_id
                                       and    tipo_subgrupo_id = det.tipo_subgrupo_id
                                       and    subgrupo_id = det.subgrupo_id)
                    and             profesor_id = p.id
                    and             d.item_id = asi.item_id
                    and             d.item_id = i.id
                    and             semestre_id =
                                       (select min (semestre_id)
                                        from   hor_items i,
                                               hor_items_asignaturas asi2
                                        where  i.id = asi2.item_id
                                        and    asignatura_id = det.asignatura_id
                                        and    grupo_id = det.grupo_id
                                        and    tipo_subgrupo_id = det.tipo_subgrupo_id
                                        and    subgrupo_id = det.subgrupo_id))
            where  area_id = a.id;
            */
            fop.tableRowOpen;

            if v_asi_ant = det.asignatura_txt /*det.asignatura_id (HOR-1024)*/ then
               fop.tablecellopen;
               fop.block (fof.white_space, font_size => 10);
               fop.tablecellclose;
               fop.tablecellopen;
               fop.block (fof.white_space, font_size => 10);
               fop.tablecellclose;
               v_border := '';
            else
               if v_asi_ant <> 'XX' then
                  -- linea 1
                  fop.tablecellopen (number_columns_spanned => 2);
                  fop.block (fof.bold ('Total per assignatura'), font_size => 10, space_after => 0.2,
                             text_align => 'right');
                  fop.tablecellclose;
                  fop.tablecellopen (number_columns_spanned => 2);
                  fop.block (fof.white_space, font_size => 10);
                  fop.tablecellclose;
                  fop.tablecellopen;
                  fop.block (fof.bold (formatear (v_acum_1)), font_size => 10, text_align => 'center');
                  fop.tablecellclose;
                  fop.tablecellopen;
                  fop.block (fof.bold (formatear (v_acum_2)), font_size => 10, text_align => 'center');
                  fop.tablecellclose;
                  fop.tablecellopen;
                  fop.block (fof.bold (formatear (v_acum_2 - v_acum_1)), font_size => 10, text_align => 'center');
                  fop.tablecellclose;
                  fop.tablecellopen;
                  fop.block (fof.bold (formatear (v_acum_3)), font_size => 10, text_align => 'center');
                  fop.tablecellclose;
                  fop.tablecellopen;

                  if v_acum_1 <> 0 then
                     fop.block (fof.bold (formatear (v_acum_3 / v_acum_1 * 100) || '%'), font_size => 10,
                                text_align => 'center');
                  else
                     fop.block (fof.bold (formatear (0) || '%'), font_size => 10, text_align => 'center');
                  end if;

                  fop.tablecellclose;
                  fop.tableRowClose;
                  -- linea 2
                  fop.tableRowOpen;
                  fop.tablecellopen (number_columns_spanned => 2);
                  fop.block (fof.bold ('Crèdits a impartir per l''àrea de coneixement'), font_size => 10,
                             space_after => 0.2, text_align => 'right');
                  fop.tablecellclose;
                  fop.tablecellopen (number_columns_spanned => 5);
                  fop.block (fof.white_space, font_size => 10);
                  fop.tablecellclose;
                  fop.tablecellopen;

                  if v_porcentaje <> 0 then
                     fop.block (fof.bold (formatear (v_acum_1 * v_porcentaje / 100)), font_size => 10,
                                text_align => 'center');
                  else
                     fop.block (fof.bold (formatear (0)), font_size => 10, text_align => 'center');
                  end if;

                  fop.tablecellclose;
                  fop.tablecellOpen;
                  fop.block (fof.bold (formatear (nvl (v_porcentaje, 0)) || '%'), font_size => 10,
                             text_align => 'center');
                  fop.tablecellclose;
                  fop.tableRowClose;
                  -- linea 3
                  fop.tableRowOpen;
                  fop.tablecellopen (number_columns_spanned => 2);
                  fop.block (fof.bold ('Estat de l''assignació docent'), font_size => 10,
                             space_after => 0.2, text_align => 'right');
                  fop.tablecellclose;
                  fop.tablecellopen (number_columns_spanned => 5);
                  fop.block (fof.white_space, font_size => 10);
                  fop.tablecellclose;

                  if v_porcentaje <> 0 then
                     fop.tablecellopen;
                     fop.block (fof.bold (formatear ((v_acum_1 * v_porcentaje / 100) - v_acum_3)), font_size => 10,
                                text_align => 'center');
                     fop.tablecellclose;

                     if round (((v_acum_1 * v_porcentaje / 100) - v_acum_3), 2) < 0 then
                        fop.tablecellopen;
                        fop.block (fof.bold ('Excés de crèdits'), font_size => 10, text_align => 'center');
                        fop.tablecellclose;
                     elsif round (((v_acum_1 * v_porcentaje / 100) - v_acum_3), 2) > 0 then
                        fop.tablecellopen;
                        fop.block (fof.bold ('Defecte de crèdits'), font_size => 10, text_align => 'center');
                        fop.tablecellclose;
                     end if;
                  else
                     fop.tablecellopen;
                     fop.block (fof.bold (formatear (0)), font_size => 10, text_align => 'center');
                     fop.tablecellclose;
                  end if;

                  fop.tableRowClose;
                  -- continuacion
                  fop.tableRowOpen;
               end if;

               v_acum_1 := 0;
               v_acum_2 := 0;
               v_acum_3 := 0;

               if v_asi_ant = 'XX' then
                  v_border := '';
               else
                  v_border := 'solid';
               end if;

               fop.tablecellopen (border_top => v_border);
               fop.block (det.asignatura_txt, font_size => 10);
               fop.tablecellclose;
               fop.tablecellopen (border_top => v_border);
               fop.block (det.asignatura, font_size => 10);
               fop.tablecellclose;
            end if;

            fop.tablecellopen (border_top => v_border);
            fop.block (det.grupo_id, font_size => 10, text_align => 'center');
            fop.tablecellclose;
            fop.tablecellopen (border_top => v_border);
            fop.block (det.tipo_subgrupo_id || det.subgrupo_id, font_size => 10, text_align => 'center');
            fop.tablecellclose;
            fop.tablecellopen (border_top => v_border);
            fop.block (formatear (det.creditos), font_size => 10, text_align => 'center');
            fop.tablecellclose;
            fop.tablecellopen (border_top => v_border);
            fop.block (formatear (v_creditos), font_size => 10, text_align => 'center');
            fop.tablecellclose;
            fop.tablecellopen (border_top => v_border);
            fop.block (formatear (v_creditos - det.creditos), font_size => 10, text_align => 'center');
            fop.tablecellclose;
            fop.tablecellopen (border_top => v_border);
            fop.block (formatear (v_creditos_Area), font_size => 10, text_align => 'center');
            fop.tablecellclose;
            fop.tablecellopen (border_top => v_border);

            if v_creditos > 0 then
               fop.block (formatear (v_creditos_area / det.creditos * 100) || '%', font_size => 10,
                          text_align => 'center');
            /*
            fop.block (formatear (v_creditos_area / v_creditos * 100) || '%', font_size => 10,
                       text_align => 'center');
            */
            else
               fop.block (formatear (0) || '%', font_size => 10, text_align => 'center');
            end if;

            fop.tablecellclose;
            fop.tableRowClose;
            v_asi_ant := det.asignatura_txt;                                             --det.asignatura_id (HOR-1024);
            v_acum_1 := v_acum_1 + det.creditos;
            v_acum_2 := v_acum_2 + v_creditos;
            v_acum_3 := v_acum_3 + v_creditos_Area;
            v_area_1 := v_area_1 + det.creditos;
            v_area_2 := v_area_2 + v_creditos;
            v_area_3 := v_area_3 + v_creditos_Area;

            select porcentaje
            into   v_porcentaje
            from   hor_ext_asignaturas_area
            where  asi_id = det.asignatura_id
            and    uest_id = a.id;

            v_area_1_porcent := v_area_1_porcent + det.creditos * v_porcentaje / 100;
         end loop;

         -- linea 1
         fop.tableRowOpen;
         fop.tablecellopen (number_columns_spanned => 2);
         fop.block (fof.bold ('Total per assignatura'), font_size => 10, space_after => 0.2, text_align => 'right');
         fop.tablecellclose;
         fop.tablecellopen (number_columns_spanned => 2);
         fop.block (fof.white_space, font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold (formatear (v_acum_1)), font_size => 10, text_align => 'center');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold (formatear (v_acum_2)), font_size => 10, text_align => 'center');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold (formatear (v_acum_2 - v_acum_1)), font_size => 10, text_align => 'center');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold (formatear (v_acum_3)), font_size => 10, text_align => 'center');
         fop.tablecellclose;
         fop.tablecellopen;

         if v_acum_1 <> 0 then
            fop.block (fof.bold (formatear (v_acum_3 / v_acum_1 * 100) || '%'), font_size => 10,
                       text_align => 'center');
         else
            fop.block (fof.bold (formatear (0) || '%'), font_size => 10, text_align => 'center');
         end if;

         fop.tablecellclose;
         fop.tableRowClose;
         -- linea 2
         fop.tableRowOpen;
         fop.tablecellopen (number_columns_spanned => 2);
         fop.block (fof.bold ('Crèdits a impartir per l''àrea de coneixement'), font_size => 10, space_after => 0.2,
                    text_align => 'right');
         fop.tablecellclose;
         fop.tablecellopen (number_columns_spanned => 5);
         fop.block (fof.white_space, font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;

         if nvl (v_porcentaje, 0) <> 0 then
            fop.block (fof.bold (formatear (v_acum_1 * v_porcentaje / 100)), font_size => 10, text_align => 'center');
         else
            fop.block (fof.bold (formatear (0)), font_size => 10, text_align => 'center');
         end if;

         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold (formatear (nvl (v_porcentaje, 0)) || '%'), font_size => 10, text_align => 'center');
         fop.tablecellclose;
         fop.tableRowClose;
         -- linea 3
         fop.tableRowOpen;
         fop.tablecellopen (number_columns_spanned => 2);
         fop.block (fof.bold ('Estat de l''assignació docent'), font_size => 10, space_after => 0.2,
                    text_align => 'right');
         fop.tablecellclose;
         fop.tablecellopen (number_columns_spanned => 5);
         fop.block (fof.white_space, font_size => 10);
         fop.tablecellclose;

         if nvl (v_porcentaje, 0) <> 0 then
            fop.tablecellopen;
            fop.block (fof.bold (formatear ((v_acum_1 * v_porcentaje / 100) - v_acum_3)), font_size => 10,
                       text_align => 'center');
            fop.tablecellclose;

            if round (((v_acum_1 * v_porcentaje / 100) - v_acum_3), 2) < 0 then
               fop.tablecellopen;
               fop.block (fof.bold ('Excés de crèdits'), font_size => 10, text_align => 'center');
               fop.tablecellclose;
            elsif round (((v_acum_1 * v_porcentaje / 100) - v_acum_3), 0) > 0 then
               fop.tablecellopen;
               fop.block (fof.bold ('Defecte de crèdits'), font_size => 10, text_align => 'center');
               fop.tablecellclose;
            end if;
         else
            fop.tablecellopen;
            fop.block (fof.bold (formatear (0)), font_size => 10, text_align => 'center');
            fop.tablecellclose;
         end if;

         fop.tableRowClose;
         fop.tableRowOpen;
         fop.tablecellopen (number_columns_spanned => 2);
         fop.block (fof.bold ('Total per àrea'), font_size => 13, space_before => 0.2, space_after => 0.2,
                    text_align => 'right');
         fop.tablecellclose;
         fop.tablecellopen (number_columns_spanned => 2);
         fop.block (fof.white_space, font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold (formatear (v_area_1)), font_size => 13, text_align => 'center');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold (formatear (v_area_2)), font_size => 13, text_align => 'center');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold (formatear (v_area_2 - v_area_1)), font_size => 13, text_align => 'center');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold (formatear (v_area_3)), font_size => 13, text_align => 'center');
         fop.tablecellclose;
         fop.tablecellopen;

         if nvl (v_area_1, 0) <> 0 then
            fop.block (fof.bold (formatear (v_area_3 / v_area_1 * 100) || '%'), font_size => 13,
                       text_align => 'center');
         else
            fop.block (fof.bold (formatear (0) || '%'), font_size => 10, text_align => 'center');
         end if;

         fop.tablecellclose;
         fop.tableRowClose;
         -- Total linea 2
         fop.tableRowOpen;
         fop.tablecellopen (number_columns_spanned => 2);
         fop.block (fof.bold ('Total crèdits a impartir per l''àrea de coneixement'), font_size => 13,
                    space_after => 0.2, text_align => 'right');
         fop.tablecellclose;
         fop.tablecellopen (number_columns_spanned => 5);
         fop.block (fof.white_space, font_size => 13);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold (formatear (v_area_3)), font_size => 13, text_align => 'center');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold (formatear (nvl (v_area_3 / v_area_1 * 100, 0)) || '%'), font_size => 13,
                    text_align => 'center');
         fop.tablecellclose;
         fop.tableRowClose;
         -- Fin total linea 2

         -- Total linea 3
         fop.tableRowOpen;
         fop.tablecellopen (number_columns_spanned => 2);
         fop.block (fof.bold ('Estat global de l''assignació docent'), font_size => 13, space_after => 0.2,
                    text_align => 'right');
         fop.tablecellclose;
         fop.tablecellopen (number_columns_spanned => 5);
         fop.block (fof.white_space, font_size => 13);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold (formatear (v_area_1_porcent - v_area_3)), font_size => 13, text_align => 'center');
         fop.tablecellclose;

         if round ((v_area_1_porcent - v_area_3), 2) < 0 then
            fop.tablecellopen;
            fop.block (fof.bold ('Excés de crèdits'), font_size => 13, text_align => 'center');
            fop.tablecellclose;
         elsif round ((v_area_1_porcent - v_area_3), 0) > 0 then
            fop.tablecellopen;
            fop.block (fof.bold ('Defecte de crèdits'), font_size => 10, text_align => 'center');
            fop.tablecellclose;
         end if;

         fop.tableRowClose;
         -- Fin total linea 3
         fop.tablebodyClose;
         fop.tableClose;
         fop.block (fof.white_space, space_before => 0.5);
      end loop;
   end;
BEGIN
   select count (*) + 1
   into   vSesion
   from   apa_sesiones
   where  sesion_id = pSesion
   and    fecha < sysdate () - 0.33;

   select id
   into   vCurso
   from   hor_curso_academico;

   if vSesion > 0 then
      select nombre
      into   v_nombre
      from   hor_Departamentos
      where  id = pDepartamento;

      cabecera;
      fop.block (fof.bold ('----  Informe per assignatura  -----'), font_size => 16, space_after => 0.5,
                 text_align => 'center');
      mostrar_subgrupos;
      --htp.p ('<fo:block break-before="page" color="white">_</fo:block>');
      pie;
   else
      cabecera;
      fop.block ('No tens permís per accedir a aquest document.', font_weight => 'bold', font_size => 14,
                 space_after => 0.5);
      pie;
   end if;
END;



CREATE OR REPLACE PROCEDURE UJI_HORARIOS.pod_consulta_detalle_pdf (
   name_array    IN   euji_util.ident_arr2,
   value_array   IN   euji_util.ident_arr2
) IS
   v_aux           NUMBER;
   pProfesor       number          := euji_util.euji_getparam (name_array, value_array, 'pProfesor');
   pArea           number          := euji_util.euji_getparam (name_array, value_array, 'pArea');
   pDepartamento   number          := euji_util.euji_getparam (name_array, value_array, 'pDepartamento');
   pSesion         VARCHAR2 (400)  := euji_util.euji_getparam (name_array, value_array, 'pSesion');
   v_nombre        varchar2 (2000);
   vSesion         number;
   vCurso          number;
   vCreditos       number;
   vFechas         t_horario;
   vRdo            clob;
   vProfesorIndex  number;

   cursor lista_areas (p_area in number, p_prof in number) is
      select   *
          from hor_areas
         where departamento_id = pDepartamento
           and (   id in (select area_id
                            from hor_profesores
                           where id = p_prof)
                or id = p_area
                or p_area = -1)
      order by nombre;

   cursor lista_profesores (p_area in number, p_prof in number) is
      select   id, nombre, creditos, pendiente_contratacion, creditos_maximos, creditos_reduccion, categoria_nombre
          from hor_profesores
         where departamento_id = pDepartamento
           and area_id = p_area
           and (   id = p_prof
                or p_prof = -1)
      order by nombre;

   cursor lista_items (p_prof in number, p_semestre in number, p_horario in varchar2 default 'S') is
      select distinct id, grupo_id, subgrupo_id, tipo_subgrupo, tipo_subgrupo_id, dia_semana_id, hora_inicio, hora_fin,
                      wm_concat (asignatura_id) asignatura_id, fin, inicio, creditos
                 from (select   items.id, items.grupo_id, items.tipo_subgrupo, items.tipo_subgrupo_id,
                                items.subgrupo_id, items.dia_semana_id, items.hora_inicio, items.hora_fin,
                                items.creditos,
                                rtrim (xmlagg (xmlelement (e, asi.asignatura_id || ', ')).extract
                                                                                           ('//text()'),
                                       ', ') as asignatura_id,
                                decode (p_horario,
                                        'S', to_date (dia_semana_id + 1 || '/01/2006 ' || to_char (hora_fin, 'hh24:mi'),
                                                      'dd/mm/yyyy hh24:mi')
                                       ) fin,
                                decode (p_horario,
                                        'S', to_date (dia_semana_id + 1 || '/01/2006 '
                                                      || to_char (hora_inicio, 'hh24:mi'),
                                                      'dd/mm/yyyy hh24:mi')
                                       ) inicio
                           from uji_horarios.hor_items items,
                                uji_horarios.hor_items_asignaturas asi
                          where items.id = asi.item_id
                            and items.semestre_id = p_semestre
                            and (   (    p_horario = 'S'
                                     and items.dia_semana_id is not null)
                                 or (    p_horario = 'N'
                                     and items.dia_semana_id is null)
                                )
                            and items.id in (select item_id
                                               from hor_items_profesores
                                              where profesor_id = p_prof)
                       group by items.id,
                                items.grupo_id,
                                items.tipo_subgrupo,
                                items.tipo_subgrupo_id,
                                items.subgrupo_id,
                                items.dia_semana_id,
                                items.semestre_id,
                                items.hora_inicio,
                                items.hora_fin,
                                asi.asignatura_id,
                                items.creditos)
             group by id,
                      grupo_id,
                      subgrupo_id,
                      tipo_subgrupo,
                      tipo_subgrupo_id,
                      dia_semana_id,
                      hora_inicio,
                      hora_fin,
                      fin,
                      inicio,
                      creditos;

   cursor lista_items_planificados (p_prof in number, p_semestre in number, p_horario in varchar2) is
      select   *
          from (select   id, asignatura_id, grupo_id, tipo_subgrupo, tipo_subgrupo_id, subgrupo_id,
                         min (nvl (dia_Semana_id, 9999)) dia_semana_id, nvl (crd_profesor, creditos / cuantos) creditos,
                         cuantos
                    from (select   items.id, items.grupo_id, items.tipo_subgrupo, items.tipo_subgrupo_id,
                                   items.subgrupo_id, items.dia_semana_id, items.hora_inicio, items.hora_fin,
                                   prof.creditos crd_profesor, items.creditos,
                                   nvl (comun_Texto, asignatura_id) asignatura_id,
                                   (select count (*)
                                      from hor_items_profesores prof2
                                     where items.id = prof2.item_id) cuantos
                              from uji_horarios.hor_items items,
                                   uji_horarios.hor_items_asignaturas asi,
                                   uji_horarios.hor_items_profesores prof
                             where items.id = asi.item_id
                               and items.id = prof.item_id
                               and prof.profesor_id = p_prof
                               and items.semestre_id = p_semestre
                          group by items.id,
                                   items.grupo_id,
                                   items.tipo_subgrupo,
                                   items.tipo_subgrupo_id,
                                   items.subgrupo_id,
                                   items.dia_semana_id,
                                   items.semestre_id,
                                   items.hora_inicio,
                                   items.hora_fin,
                                   nvl (comun_Texto, asignatura_id),
                                   prof.creditos,
                                   items.creditos)
                group by id,
                         asignatura_id,
                         grupo_id,
                         tipo_subgrupo,
                         tipo_subgrupo_id,
                         subgrupo_id,
                         nvl (crd_profesor, creditos / cuantos),
                         cuantos)
         where (    p_horario = 'S'
                and dia_Semana_id < 9999)
            or (    p_horario = 'N'
                and dia_Semana_id = 9999)
      order by asignatura_id,
               grupo_id,
               decode (tipo_subgrupo_id, 'TE', 1, 'PR', 2, 'LA', 3, 'SE', 4, 'TU', 5, 6),
               subgrupo_id;

   cursor lista_items_crd (p_prof in number, p_sem in number) is
      /*select   *
      from     (select   grupo_id, tipo_subgrupo, tipo_subgrupo_id, subgrupo_id, asignatura_id, creditos
                from     (select distinct items.grupo_id, items.tipo_subgrupo, items.tipo_subgrupo_id,
                                          items.subgrupo_id, nvl (items.comun_texto, asi.asignatura_id) asignatura_id,
                                          creditos
                          from            uji_horarios.hor_items items,
                                          uji_horarios.hor_items_asignaturas asi
                          where           items.id = asi.item_id
                          and             items.dia_semana_id is not null
                          and             items.semestre_id = p_sem
                          and             items.id in (select item_id
                                                       from   hor_items_profesores
                                                       where  profesor_id = p_prof)
                          group by        items.grupo_id,
                                          items.tipo_subgrupo,
                                          items.tipo_subgrupo_id,
                                          items.subgrupo_id,
                                          nvl (items.comun_texto, asi.asignatura_id),
                                          creditos)
                group by grupo_id,
                         tipo_subgrupo,
                         tipo_subgrupo_id,
                         subgrupo_id,
                         creditos,
                         asignatura_id)
      order by 5,
               1,
               2,
               3,
               4;*/
      select distinct grupo_id, substr (tipo, 1, 2) tipo_subgrupo_id, substr (tipo, 3) subgrupo_id,
                      asignatura_txt asignatura_id, crd_final creditos, departamento_id, area_id
                 from hor_v_items_creditos_detalle
                where id = p_prof
                  and item_id in (select id
                                    from hor_items
                                   where semestre_id = p_sem)
             order by 5,
                      1,
                      2,
                      3,
                      4;

   cursor lista_solapamientos (p_prof in number, p_semestre in number) is
      select   id, min (asignatura_id) asignatura_id, grupo_id, tipo_subgrupo_id || subgrupo_id subgrupo_id,
               semestre_id, dia_semana_id,
               decode (dia_semana_id,
                       1, 'Dilluns',
                       2, 'Dimarts',
                       3, 'Dimecres',
                       4, 'Dijous',
                       5, 'Divendres',
                       6, 'Dissabte',
                       7, 'Diumenge',
                       'Error'
                      ) dia_semana,
               min (hora_inicio) inicio, max (hora_fin) fin
          from (select   id, grupo_id, subgrupo_id, tipo_subgrupo, tipo_subgrupo_id, dia_semana_id, hora_inicio,
                         hora_fin, wm_concat (asignatura_id) asignatura_id, semestre_id,
                         count (*) over (partition by semestre_id, dia_semana_id) cuantos
                    from (select id, asignatura_id, semestre_id, grupo_id, tipo_subgrupo, tipo_subgrupo_id, subgrupo_id,
                                 dia_semana_id, hora_inicio, hora_fin
                            from (select   items.id, items.grupo_id, items.tipo_subgrupo, items.tipo_subgrupo_id,
                                           items.subgrupo_id, items.dia_semana_id, items.hora_inicio, items.hora_fin,
                                           items.semestre_id, wm_concat (asi.asignatura_id) asignatura_id
                                      from uji_horarios.hor_items items,
                                           uji_horarios.hor_items_asignaturas asi
                                     where items.id = asi.item_id
                                       and items.semestre_id = p_semestre
                                       and items.dia_semana_id is not null
                                       and items.id in (select item_id
                                                          from hor_items_profesores
                                                         where profesor_id = p_prof)
                                  group by asi.asignatura_id,
                                           items.id,
                                           items.grupo_id,
                                           items.tipo_subgrupo,
                                           items.tipo_subgrupo_id,
                                           items.subgrupo_id,
                                           items.dia_semana_id,
                                           items.semestre_id,
                                           items.hora_inicio,
                                           items.hora_fin) x
                           where exists (
                                    select 1
                                      from hor_items i,
                                           hor_items_profesores p
                                     where i.id = p.item_id
                                       and profesor_id = p_prof
                                       and i.id <> x.id
                                       and i.dia_semana_id = x.dia_Semana_id
                                       and i.semestre_id = x.semestre_id
                                       and (   to_number (to_char ((i.hora_inicio + 1 / 1440), 'hh24mi'))
                                                  between to_number (to_char (x.hora_inicio, 'hh24mi'))
                                                      and to_number (to_char (x.hora_fin, 'hh24mi'))
                                            or to_number (to_char ((i.hora_fin - 1 / 1440), 'hh24mi'))
                                                  between to_number (to_char (x.hora_inicio, 'hh24mi'))
                                                      and to_number (to_char (x.hora_fin, 'hh24mi'))
                                           )))
                group by id,
                         grupo_id,
                         subgrupo_id,
                         tipo_subgrupo,
                         tipo_subgrupo_id,
                         dia_semana_id,
                         semestre_id,
                         hora_inicio,
                         hora_fin)
         where cuantos > 1
      group by id,
               semestre_id,
               dia_Semana_id,
               grupo_id,
               tipo_subgrupo_id || subgrupo_id
      order by 5,
               6,
               2,
               3,
               4,
               6;

   function redondea_fecha_a_cuartos (vIniDate date)
      return date is
      fecha_redondeada   date;
   begin
      select trunc (vIniDate, 'HH') + (15 * round (to_char (trunc (vIniDate, 'MI'), 'MI') / 15)) / 1440
        into fecha_redondeada
        from dual;

      return fecha_redondeada;
   end;

   function get_lista_fechas_prof_detalle (p_prof in number, p_item in number) return varchar2 is
      fechas varchar2(4000);
   begin
      select wm_concat(to_char(inicio, ' dd/mm/yy'))
      into fechas
        from hor_v_items_prof_detalle pd
       where PD.PROFESOR_ID = p_prof
         and pd.item_id = p_item
         and pd.seleccionado = 'S';

      return fechas;
   end;

   procedure cabecera is
   begin
      owa_util.mime_header (ccontent_type => 'text/xml', bclose_header => true);
      fop.documentOpen (page_width => 29.7, page_height => 21, margin_left => 1.5, margin_right => 1.5,
                        margin_top => 0.5, margin_bottom => 0.5, extent_before => 3, extent_after => 1.5,
                        extent_margin_top => 3, extent_margin_bottom => 1.5);
      fop.pageBodyOpen;
      htp.p (fof.documentheaderopen || fof.blockopen (space_after => 0.1) || fof.tableOpen
             || fof.tablecolumndefinition (column_width => 3) || fof.tablecolumndefinition (column_width => 24)
             || fof.tablebodyopen || fof.tableRowOpen || fof.tablecellopen
             || fof.block (fof.logo_uji, text_align => 'center') || fof.tablecellclose
             || fof.tablecellopen (display_align => 'center', padding => 0.4)
             || fof.block (text => v_nombre, font_size => 16, font_weight => 'bold', font_family => 'Arial',
                           text_align => 'center')
             || fof.block (text => 'POD provisional - Curs: ' || vCurso || '/' || substr (to_char (vCurso + 1), 3),
                           font_size => 16, font_weight => 'bold', font_family => 'Arial', text_align => 'center')
             || fof.tablecellclose || fof.tableRowClose || fof.tablebodyclose || fof.tableClose || fof.blockclose
             || fof.documentheaderclose);
      -- El numero paginas se escribe a mano para poner el texto m?s peque?o
      htp.p (fof.documentfooteropen || fof.tableOpen || fof.tablecolumndefinition (column_width => 18)
             || fof.tablebodyopen || fof.tableRowOpen || fof.tablecellopen
             || fof.block (text => 'Pag. <fo:page-number /> de <fo:page-number-citation ref-id="lastBlock" />',
                           text_align => 'right', font_weight => 'bold', font_size => 8)
             || fof.tablecellclose || fof.tableRowClose || fof.tablebodyclose || fof.tableClose
             || fof.documentfooterclose);
      fop.pageFlowOpen;
      fop.block (fof.white_space);
   end;

   procedure pie is
   begin
      htp.p ('<fo:block  id="lastBlock" space-before="1cm" font-size="8pt">');
      htp.p ('Data de generació: ' || to_char (sysdate, 'dd/mm/yyy hh24:mi'));
      fop.blockclose ();
      fop.pageFlowClose;
      fop.pageBodyClose;
      fop.documentClose;
   end;

   function formatear (p_entrada in number)
      return varchar2 is
      v_rdo   varchar2 (100);
   begin
      v_rdo := replace (to_char (p_entrada, '9990.00'), '.', ',');
      return (v_rdo);
   end;

   function espacios (p_espacios in number)
      return varchar2 is
      v_rdo   varchar2 (2000);
   begin
      v_rdo := null;

      for x in 1 .. p_espacios loop
         v_rdo := v_rdo || fof.white_space;
      end loop;

      return v_rdo;
   end;

   procedure mostrar_calendario (p_prof in number, p_semestre in number) is
      v_aux   number;
   begin
      vFechas := t_horario ();

      for item in lista_items (p_prof, p_semestre, 'S') loop
         vFechas.extend;
         vFechas (vFechas.count) :=
            ItemHorarios (hor_contenido_item (item.hora_inicio, item.hora_fin,
                                              item.asignatura_id || ' ' || item.grupo_id,
                                              '(' || item.tipo_subgrupo_id || item.subgrupo_id || ')'),
                          redondea_fecha_a_cuartos (item.inicio), redondea_fecha_a_cuartos (item.fin), null);
      end loop;

      if vFechas.count > 0 then
         fop.block (fof.white_space);
         fop.block (fof.white_space);
         fop.block (fof.bold ('Semestre ' || p_semestre) || '  _____________________________', font_size => 12);
         vRdo := xpfpkg_horarios_academicos.drawTimetable (vFechas, 5, null, 'CA', 'PDF_APP_HORARIOS');
         xup_utils.dump (vRdo);
         dbms_lob.freeTemporary (vRdo);
         --fop.block (fof.white_space);
         fop.block (fof.white_space);
      end if;
   --fop.block (fof.white_space);
   end;

   procedure mostrar_pod (p_prof in number, p_semestre in number) is
      v_aux   number;
      fechas varchar2(4000);
   begin
      begin
         v_aux := 0;

         --for item in lista_items_crd (p_prof, p_semestre) loop
         for item in lista_items_planificados (p_prof, p_semestre, 'S') loop
            v_aux := v_aux + 1;

            if v_aux = 1 then
               fop.block (fof.white_space);
               fop.block (espacios (8) || fof.bold ('Subgrups amb horari assignat al semestre ' || p_semestre),
                          font_size => 12);
            end if;


            fop.block (espacios (8) || espacios (5) || item.asignatura_id || ' ' || item.grupo_id || ' '
                       || item.tipo_subgrupo_id || item.subgrupo_id || '  ' || formatear (item.creditos) || ' crd.',
                       font_size => 10);
            fechas := get_lista_fechas_prof_detalle(p_prof, item.id);
            if (fechas is not null) then
               fop.block (espacios (18) || '(Dates: ' || fechas || ')',
                       font_size => 8);
            else
               fop.block (espacios (18) || '(No hi ha dates assignades)',
                       font_size => 8);
            end if;
         end loop;
      exception
         when others then
            fop.block (sqlerrm);
      end;

      begin
         v_aux := 0;

         for item in lista_items_planificados (p_prof, p_semestre, 'N') loop
            v_aux := v_aux + 1;

            if v_aux = 1 then
               fop.block (fof.white_space);
               fop.block (espacios (8) || fof.bold ('Subgrups sense horari assignat al semestre ' || p_semestre),
                          font_size => 12);
            end if;

            fop.block (espacios (8) || espacios (5) || item.asignatura_id || ' ' || item.grupo_id || ' '
                       || item.tipo_subgrupo_id || item.subgrupo_id || '  ' || formatear (item.creditos) || ' crd.',
                       font_size => 10);
         end loop;
      exception
         when others then
            fop.block (sqlerrm);
      end;
   end;

   procedure mostrar_solapaments (p_prof in number, p_semestre in number) is
   begin
      begin
         v_aux := 0;

         for item in lista_solapamientos (p_prof, p_semestre) loop
            v_aux := v_aux + 1;

            if v_aux = 1 then
               fop.block (fof.white_space);
               fop.block (espacios (8) || fof.bold ('Solapaments al semestre ' || p_semestre), font_size => 12);
            end if;

            fop.block (espacios (8) || espacios (5) || item.dia_Semana || ' - ' || to_char (item.inicio, 'hh24:mi')
                       || '-' || to_char (item.fin, 'hh24:mi') || ' - ' || item.asignatura_id || ' ' || item.grupo_id
                       || ' ' || item.subgrupo_id,
                       font_size => 10);
         end loop;
      exception
         when others then
            fop.block (sqlerrm);
      end;
   end;
BEGIN
   select count (*) + 1
     into vSesion
     from apa_sesiones
    where sesion_id = pSesion
      and fecha < sysdate () - 0.33;

   select id
     into vCurso
     from hor_curso_academico;

   if vSesion > 0 then
      begin

         select nombre
           into v_nombre
           from hor_Departamentos
          where id = pDepartamento;

         cabecera;

         if pArea = -1 then
            pProfesor := -1;
         end if;

         for a in lista_areas (pArea, pProfesor) loop

            if (vProfesorIndex > 0) then
               htp.p('<fo:block break-before="page"></fo:block>');
            end if;
            vProfesorIndex := 0;

            fop.block ('Area de coneixement: ' || fof.bold (a.nombre), font_size => 16);
            fop.block (fof.white_space);

            for x in lista_profesores (a.id, pProfesor) loop
               begin
                  select nvl (sum (creditos), 0)
                    into vCreditos
                    from (select   grupo_id, tipo_subgrupo, tipo_subgrupo_id, subgrupo_id,
                                   wm_concat (asignatura_id) asignatura_id, creditos
                              from (select distinct items.grupo_id, items.tipo_subgrupo, items.tipo_subgrupo_id,
                                                    items.subgrupo_id, asi.asignatura_id, creditos
                                               from uji_horarios.hor_items items,
                                                    uji_horarios.hor_items_asignaturas asi
                                              where items.id = asi.item_id
                                                --and             items.dia_semana_id is not null
                                                and items.id in (select item_id
                                                                   from hor_items_profesores
                                                                  where profesor_id = x.id)
                                           group by items.grupo_id,
                                                    items.tipo_subgrupo,
                                                    items.tipo_subgrupo_id,
                                                    items.subgrupo_id,
                                                    asi.asignatura_id,
                                                    creditos)
                          group by grupo_id,
                                   tipo_subgrupo,
                                   tipo_subgrupo_id,
                                   subgrupo_id,
                                   creditos);

                  select nvl (sum (creditos), 0)
                    into vCreditos
                    from (select distinct d.id, nvl (comun_Texto, d.asignatura_id) asignatura_id, d.grupo_id,
                                          tipo_subgrupo_id, subgrupo_id, crd_final creditos
                                     from hor_v_items_creditos_detalle d,
                                          hor_items i,
                                          hor_items_asignaturas a
                                    where d.id = x.id
                                      and d.item_id = i.id
                                      and i.id = a.item_id);
               exception
                  when others then
                     vCreditos := 0;
               end;

               if (vProfesorIndex > 0) then
                  htp.p('<fo:block break-after="page"></fo:block>');
               end if;
               vProfesorIndex := vProfesorIndex + 1;

               fop.block (espacios (3) || fof.bold (x.nombre) || ' - ' || x.categoria_nombre, space_before => 1,
                          font_size => 14);
               fop.tableOpen;
               fop.tablecolumndefinition (column_width => 2);
               fop.tablecolumndefinition (column_width => 3);
               fop.tablecolumndefinition (column_width => 3);
               fop.tablecolumndefinition (column_width => 3);
               fop.tablecolumndefinition (column_width => 3);
               fop.tablecolumndefinition (column_width => 5);
               fop.tablecolumndefinition (column_width => 5);
               fop.tablebodyopen;
               fop.tableRowOpen;
               fop.tablecellopen;
               fop.block (fof.white_space, text_align => 'center');
               fop.tablecellclose;
               fop.tablecellopen;
               fop.block ('Dedicació inicial', text_align => 'center');
               fop.tablecellclose;
               fop.tablecellopen;
               fop.block ('Reducció', text_align => 'center');
               fop.tablecellclose;
               fop.tablecellopen;
               fop.block ('Dedicació neta', text_align => 'center');
               fop.tablecellclose;
               fop.tablecellopen;
               fop.block ('Crèdits a impartir', text_align => 'center');
               fop.tablecellclose;
               fop.tablecellopen;
               fop.block ('% d''assignació docent', text_align => 'center');
               fop.tablecellclose;
               fop.tablecellopen;
               fop.block ('Crèdits pendents d''assignar', text_align => 'center');
               fop.tablecellclose;
               fop.tableRowClose;
               fop.tableRowOpen;
               fop.tablecellopen;
               fop.block (fof.white_space, text_align => 'center');
               fop.tablecellclose;
               fop.tablecellopen;
               fop.block (formatear (x.creditos_maximos) || ' crd', text_align => 'center');
               fop.tablecellclose;
               fop.tablecellopen;
               fop.block (formatear (x.creditos_Reduccion) || ' crd', text_align => 'center');
               fop.tablecellclose;
               fop.tablecellopen;
               fop.block (fof.bold (formatear (x.creditos)) || ' crd', text_align => 'center');
               fop.tablecellclose;
               fop.tablecellopen;
               fop.block (formatear (vCreditos) || ' crd', text_align => 'center');
               fop.tablecellclose;
               fop.tablecellopen;

               if x.creditos <> 0 then
                  fop.block (formatear (round (vCreditos / x.creditos * 100, 2)) || '%', text_align => 'center');
               else
                  fop.block (formatear (0) || '%', text_align => 'center');
               end if;

               fop.tablecellclose;
               fop.tablecellopen;
               fop.block (fof.bold (formatear (x.creditos - vCreditos)) || ' crd.', text_align => 'center');
               fop.tablecellclose;
               fop.tableRowClose;
               fop.tablebodyclose;
               fop.tableClose;

               /*
               fop.block (espacios (6) || ' ' || x.creditos_maximos || ' crd' || espacios (4) || 'Reducció '
                          || x.creditos_Reduccion || ' crd' || espacios (4) || 'Dedicació neta '
                          || fof.bold (x.creditos) || ' crd' || espacios (4) || 'Crèdits a impartir ' || vCreditos
                          || ' crd' || espacios (4) || 'Crèdits pendents d''assignar '
                          || fof.bold (x.creditos - vCreditos) || ' crd.',
                          font_size => 10);
                          */
               for semestre in 1 .. 2 loop
                  begin
                     mostrar_calendario (x.id, semestre);
                     mostrar_pod (x.id, semestre);
                     mostrar_solapaments (x.id, semestre);
                     null;
                  exception
                     when others then
                        fop.block (sqlerrm);
                  end;
               end loop;



            end loop;
         end loop;

         pie;
      exception
         when others then
            --cabecera;
            fop.block ('Error en el departament. ' || sqlerrm, font_weight => 'bold', text_align => 'center',
                       font_size => 14, space_after => 0.5);
            pie;
      end;
   else
      cabecera;
      fop.block ('No tens permís per accedir a aquest document.', font_weight => 'bold', text_align => 'center',
                 font_size => 14, space_after => 0.5);
      pie;
   end if;
END;



CREATE OR REPLACE procedure UJI_HORARIOS.pod_detalle_asignatura_pdf(name_array in euji_util.ident_arr2, value_array in euji_util.ident_arr2) is
   v_aux              number;
   --pEstudio           number := euji_util.euji_getparam(name_array, value_array, 'pEstudio');
   --pCurso             number := euji_util.euji_getparam(name_array, value_array, 'pCurso');
   pDepartamento      number := euji_util.euji_getparam(name_array, value_array, 'pDepartamento');
   pArea              number := euji_util.euji_getparam(name_array, value_array, 'pArea');
   pSemestre          number := euji_util.euji_getparam(name_array, value_array, 'pSemestre');
   pSesion            varchar2(400) := euji_util.euji_getparam(name_array, value_array, 'pSesion');

   vEstudio           varchar2(4000);
   v_nombre           varchar2(2000);
   vSesion            number;
   vCurso             number;
   vCreditos          number;

   vCrdAsigSubgrupo    number;
   vCrdAsigAsignatura  number;
   vCrdTotalAsignatura number;

   vFechas            t_horario;
   vRdo               clob;

   cursor lista_asignaturas is
   select distinct semestre_id, curso_id, estudio_id, asignatura, asig.asignatura_id, grupo_id, subgrupo_id, tipo_subgrupo_id, tipo_subgrupo, creditos, nvl(comun_texto, asig.asignatura_id) asignatura_txt, tipo_asignatura, caracter_id
          from hor_items items
          join hor_items_asignaturas asig on items.id = asig.item_id
          join hor_v_asignaturas_area asi_area on asi_area.asignatura_id = asig.asignatura_id
          join hor_areas areas on asi_area.area_id = areas.id
         where (departamento_id = pDepartamento) and
               (area_id = pArea or pArea = -1)
      order by curso_id, semestre_id, asignatura_id, grupo_id, tipo_subgrupo_id desc, subgrupo_id;


   cursor lista_profesores(p_asignatura_id in varchar2, p_grupo_id in varchar2, p_subgrupo_id in number, p_tipo_subgrupo_id in varchar2) is
      select p.id, p.nombre, nvl(creditos, 0) creditos, pendiente_contratacion, nvl(creditos_maximos, 0) creditos_maximos, nvl(creditos_reduccion, 0) creditos_Reduccion, categoria_nombre, a.nombre nombre_area, d.nombre nombre_departamento
        from hor_profesores p join hor_areas a on p.area_id = a.id join hor_departamentos d on d.id = a.departamento_id
       where p.id in (select profesor_id
                        from hor_items_profesores ip join hor_items item on ip.item_id = item.id join hor_items_asignaturas ia on ia.item_id = item.id
                       where ia.asignatura_id = p_asignatura_id
                         and item.grupo_id = p_grupo_id
                         and item.subgrupo_id = p_subgrupo_id
                         and item.tipo_subgrupo_id = p_tipo_subgrupo_id);

   cursor lista_profesores_2(p_asignatura_id in varchar2, p_grupo_id in varchar2, p_subgrupo_id in number, p_tipo_subgrupo_id in varchar2) is
      select cd.nombre, nvl(cd.crd_prof, cd.crd_item / cd.cuantos) creditos, a.nombre nombre_area, d.nombre nombre_departamento
        from hor_v_items_creditos_detalle cd join hor_areas a on cd.area_id = a.id join hor_departamentos d on d.id = a.departamento_id
       where cd.asignatura_txt = p_asignatura_id
         and cd.grupo_id = p_grupo_id
         and cd.tipo = p_tipo_subgrupo_id || p_subgrupo_id;


   function tiene_profesores(p_asignatura_id in varchar2, p_grupo_id in varchar2, p_subgrupo_id in number, p_tipo_subgrupo_id in varchar2)
      return boolean is
      numero          number;
   begin
      select count( * )
        into numero
        from hor_items_profesores ip join hor_items item on ip.item_id = item.id join hor_items_asignaturas ia on ia.item_id = item.id
       where ia.asignatura_id = p_asignatura_id
         and item.grupo_id = p_grupo_id
         and item.subgrupo_id = p_subgrupo_id
         and item.tipo_subgrupo_id = p_tipo_subgrupo_id;

      return numero > 0;
   end;

   function get_creditos_asi_profesor(p_profesor_id in number, p_asignatura_id in varchar2, p_grupo_id in varchar2, p_subgrupo_id in number, p_tipo_subgrupo_id in varchar2)
      return number is
   begin
      select nvl(sum(creditos), 0)
        into vCreditos
        from (select distinct d.id, nvl(comun_Texto, d.asignatura_id) asignatura_id, d.grupo_id, tipo_subgrupo_id, subgrupo_id, crd_final creditos
                from hor_v_items_creditos_detalle d, hor_items i, hor_items_asignaturas a
               where d.id = p_profesor_id
                 and a.asignatura_id = p_asignatura_id
                 and i.grupo_id = p_grupo_id
                 and i.subgrupo_id = p_subgrupo_id
                 and i.tipo_subgrupo_id = p_tipo_subgrupo_id
                 and d.item_id = i.id
                 and i.id = a.item_id);
      return vCreditos;
   end;

   function formatear(p_entrada in number)
      return varchar2 is
      v_rdo          varchar2(100);
   begin
      v_rdo := replace(to_char(p_entrada, '9990.00'), '.', ',');
      return (v_rdo);
   end;

   procedure cabecera is
   begin
      owa_util.mime_header(ccontent_type => 'text/xml', bclose_header => true);
      fop.documentOpen(page_width => 29.7,
                       page_height => 21,
                       margin_left => 1.5,
                       margin_right => 1.5,
                       margin_top => 0.5,
                       margin_bottom => 0.5,
                       extent_before => 3,
                       extent_after => 1.5,
                       extent_margin_top => 3,
                       extent_margin_bottom => 1.5);
      fop.pageBodyOpen;
      htp.p(fof.documentheaderopen || fof.blockopen(space_after => 0.1) || fof.tableOpen || fof.tablecolumndefinition(column_width => 3) || fof.tablecolumndefinition(column_width => 24) || fof.tablebodyopen || fof.tableRowOpen || fof.tablecellopen || fof.block(fof.logo_uji, text_align => 'center') || fof.tablecellclose || fof.tablecellopen(display_align => 'center', padding => 0.4) || fof.block(text => v_nombre, font_size => 16, font_weight => 'bold', font_family => 'Arial', text_align => 'center') || fof.block(text => 'Informe provisional POD - Curs: ' || vCurso || '/' || substr(to_char(vCurso + 1), 3), font_size => 16, font_weight => 'bold', font_family => 'Arial', text_align => 'center') || fof.tablecellclose || fof.tableRowClose || fof.tablebodyclose || fof.tableClose || fof.blockclose || fof.documentheaderclose);
      -- El numero paginas se escribe a mano para poner el texto m?s peque?o
      htp.p(fof.documentfooteropen || fof.tableOpen || fof.tablecolumndefinition(column_width => 18) || fof.tablebodyopen || fof.tableRowOpen || fof.tablecellopen || fof.block(text => 'Pag. <fo:page-number /> de <fo:page-number-citation ref-id="lastBlock" />', text_align => 'right', font_weight => 'bold', font_size => 8) || fof.tablecellclose || fof.tableRowClose || fof.tablebodyclose || fof.tableClose || fof.documentfooterclose);
      fop.pageFlowOpen;
      fop.block(fof.white_space);
   end;

   procedure pie is
   begin
      htp.p('<fo:block  id="lastBlock" space-before="1cm" font-size="8pt">');
      htp.p('Data de generació: ' || to_char(sysdate, 'dd/mm/yyy hh24:mi'));
      fop.blockclose();
      fop.pageFlowClose;
      fop.pageBodyClose;
      fop.documentClose;
   end;

   function espacios(p_espacios in number)
      return varchar2 is
      v_rdo          varchar2(2000);
   begin
      v_rdo := null;

      for x in 1 .. p_espacios loop
         v_rdo := v_rdo || fof.white_space;
      end loop;

      return v_rdo;
   end;

   procedure mostrar_resumen_asignatura(creditosTotales number, creditosAsignados number) is
      porcentaje number;
   begin
      if (creditosTotales > 0) then
         porcentaje := 100 * creditosAsignados / creditosTotales;
      end if;

      fop.block('Total crèdits assignatura ' || creditosTotales || ' assignats ' || creditosAsignados || ' (' || porcentaje || '%)', font_size => 14);
      fop.block(fof.white_space);
      fop.block(fof.white_space);
   end;

   procedure mostrar_por_profesor is
      lista_asign           lista_asignaturas%rowtype;
      asi_anterior          varchar2(4000);
      asi                   varchar2(4000);
   begin
      vCrdAsigSubgrupo := 0;
      vCrdAsigAsignatura := 0;
      vCrdTotalAsignatura := 0;

      asi_anterior := '-';
      for a in lista_asignaturas loop
         asi := a.asignatura_txt || ' ' || a.asignatura || ' ' || a.caracter_id || ' ' || a.tipo_asignatura;
         if (asi <> asi_anterior) then
            asi_anterior := asi;

            if (vCrdTotalAsignatura > 0) then
               mostrar_resumen_asignatura(vCrdTotalAsignatura, vCrdAsigAsignatura);
               vCrdTotalAsignatura := 0;
               vCrdAsigAsignatura := 0;
            end if;

            fop.block(fof.bold(asi), font_size => 16);
            fop.block(fof.white_space);
         end if;

         vCrdTotalAsignatura := vCrdTotalAsignatura + a.creditos;

         fop.block(a.grupo_id || ' ' || a.tipo_subgrupo_id || a.subgrupo_id || ' (' || to_char(a.creditos, '999990D99') || ' crèdits )', font_size => 13);
         fop.block(fof.white_space);
         if (tiene_profesores(a.asignatura_id, a.grupo_id, a.subgrupo_id, a.tipo_subgrupo_id)) then
            fop.tableOpen;
            fop.tablecolumndefinition(column_width => 5);
            fop.tablecolumndefinition(column_width => 4);
            fop.tablecolumndefinition(column_width => 6);
            fop.tablecolumndefinition(column_width => 7);
            fop.tablecolumndefinition(column_width => 2);

            fop.tablebodyopen;
            fop.tableRowOpen;
            fop.tablecellopen;
            fop.block(fof.bold('Professor/a'), font_size => 10);
            fop.tablecellclose;
            fop.tablecellopen;
            fop.block(fof.bold('Categoria'), font_size => 10);
            fop.tablecellclose;
            fop.tablecellopen;
            fop.block(fof.bold('Area'), font_size => 10);
            fop.tablecellclose;
            fop.tablecellopen;
            fop.block(fof.bold('Departament'), font_size => 10);
            fop.tablecellclose;
            fop.tablecellopen;
            fop.block(fof.bold('Crèdits'), font_size => 10, text_align => 'right');
            fop.tablecellclose;
            fop.tableRowClose;
            vCrdAsigSubgrupo := 0;

            for x in lista_profesores(a.asignatura_id, a.grupo_id, a.subgrupo_id, a.tipo_subgrupo_id) loop
               vCreditos := get_creditos_asi_profesor(x.id, a.asignatura_id, a.grupo_id, a.subgrupo_id, a.tipo_subgrupo_id);
               fop.tableRowOpen;
               fop.tablecellopen;
               fop.block(x.nombre, font_size => 10);
               fop.tablecellclose;
               fop.tablecellopen;
               fop.block(x.categoria_nombre, font_size => 10);
               fop.tablecellclose;

               fop.tablecellopen;
               fop.block(x.nombre_area, font_size => 10);
               fop.tablecellclose;

               fop.tablecellopen;
               fop.block(x.nombre_departamento, font_size => 10);
               fop.tablecellclose;

               fop.tablecellopen;
               fop.block(formatear(vCreditos), font_size => 10, text_align => 'right');
               fop.tablecellclose;
               fop.tableRowClose;
               vCrdAsigSubgrupo := vCrdAsigSubgrupo + vCreditos;
               vCrdAsigAsignatura := vCrdAsigAsignatura + vCreditos;

            end loop;

            fop.tableRowOpen;
            fop.tablecellopen;
            fop.block(fof.white_space, font_size => 10);
            fop.tablecellclose;
            fop.tablecellopen;
            fop.block(fof.white_space, font_size => 10);
            fop.tablecellclose;
            fop.tablecellopen;
            fop.block(fof.white_space, font_size => 10);
            fop.tablecellclose;
            fop.tablecellopen;
            fop.block(fof.white_space, font_size => 10);
            fop.tablecellclose;
            fop.tablecellopen;
            fop.block(fof.bold(formatear(vCrdAsigSubgrupo)), font_size => 10, text_align => 'right');
            fop.tablecellclose;

            fop.tableRowClose;
            fop.tablebodyclose;
            fop.tableClose;
            fop.block(fof.white_space);
            fop.block(fof.white_space);
         else
            fop.block('Sense professors assignats', font_size => 11);
            fop.block(fof.white_space);
         end if;
      end loop;

      mostrar_resumen_asignatura(vCrdTotalAsignatura, vCrdAsigAsignatura);


   end;
begin
   select count( * ) + 1
     into vSesion
     from apa_sesiones
    where sesion_id = pSesion
      and fecha < sysdate() - 0.33;

   select id
     into vCurso
     from hor_curso_academico;

   if vSesion > 0 then

      select nombre
        into v_nombre
        from hor_departamentos
       where id = pDepartamento;

      cabecera;
      fop.block(fof.bold('----  Informe POD per assignatura  -----'), font_size => 16, space_after => 0.5, text_align => 'center');
      mostrar_por_profesor;
      pie;
   else
      cabecera;
      fop.block('No tens permís per accedir a aquest document.', font_weight => 'bold', font_size => 14, space_after => 0.5);
      pie;
   end if;
exception
   when others then
      htp.print(dbms_utility.format_error_backtrace);
      htp.print(sqlerrm);
end;



CREATE OR REPLACE procedure UJI_HORARIOS.pod_validacion_pdf(name_array in euji_util.ident_arr2, value_array in euji_util.ident_arr2) is
   v_aux                  number;
   pDepartamento          number := euji_util.euji_getparam(name_array, value_array, 'pDepartamento');
   pSesion                varchar2(400) := euji_util.euji_getparam(name_array, value_array, 'pSesion');
   v_nombre               varchar2(2000);
   vSesion                number;
   vCurso                 number;
   pProfesor              number;

   cursor lista_areas(p_departamento in number) is
        select *
          from hor_areas
         where departamento_id = p_Departamento
      order by nombre;

   cursor lista_profesores(p_departamento in number, p_area in number) is
        select p.id, p.nombre, creditos, pendiente_contratacion, creditos_maximos, creditos_reduccion, categoria_nombre, a.nombre nombre_area
          from hor_profesores p, hor_areas a
         where p.departamento_id = p_Departamento
           and area_id = p_area
           and area_id = a.id
      order by nombre;

   procedure cabecera is
   begin
      begin
         select nombre
           into v_nombre
           from hor_Departamentos
          where id = pDepartamento;
      exception
         when no_data_found then
            v_nombre := 'Desconegut';
         when others then
            v_nombre := sqlerrm;
      end;

      owa_util.mime_header(ccontent_type => 'text/xml', bclose_header => true);
      fop.documentOpen(margin_left => 1.5,
                       margin_right => 1.5,
                       margin_top => 0.5,
                       margin_bottom => 0.5,
                       extent_before => 3,
                       extent_after => 1.5,
                       extent_margin_top => 3,
                       extent_margin_bottom => 1.5);
      fop.pageBodyOpen;
      htp.p(fof.documentheaderopen || fof.blockopen(space_after => 0.1) || fof.tableOpen || fof.tablecolumndefinition(column_width => 3) || fof.tablecolumndefinition(column_width => 15) || fof.tablebodyopen || fof.tableRowOpen || fof.tablecellopen || fof.block(fof.logo_uji, text_align => 'center') || fof.tablecellclose || fof.tablecellopen(display_align => 'center', padding => 0.4) || fof.block(text => v_nombre, font_size => 16, font_weight => 'bold', font_family => 'Arial', text_align => 'center') || fof.block(text => 'Validació POD - Curs: ' || vCurso || '/' || substr(to_char(vCurso + 1), 3), font_size => 16, font_weight => 'bold', font_family => 'Arial', text_align => 'center') || fof.tablecellclose || fof.tableRowClose || fof.tablebodyclose || fof.tableClose || fof.blockclose || fof.documentheaderclose);
      -- El numero paginas se escribe a mano para poner el texto m?s peque?o
      htp.p(fof.documentfooteropen || fof.tableOpen || fof.tablecolumndefinition(column_width => 18) || fof.tablebodyopen || fof.tableRowOpen || fof.tablecellopen || fof.block(text => 'Pag. <fo:page-number /> de <fo:page-number-citation ref-id="lastBlock" />', text_align => 'right', font_weight => 'bold', font_size => 8) || fof.tablecellclose || fof.tableRowClose || fof.tablebodyclose || fof.tableClose || fof.documentfooterclose);
      fop.pageFlowOpen;
      fop.block(fof.white_space);
   end;

   procedure pie is
   begin
      htp.p('<fo:block  id="lastBlock" space-before="1cm" font-size="8pt">');
      htp.p('Data de generació: ' || to_char(sysdate, 'dd/mm/yyy hh24:mi'));
      fop.blockclose();
      fop.pageFlowClose;
      fop.pageBodyClose;
      fop.documentClose;
   end;

   function italic(p_entrada in varchar2)
      return varchar2 is
      v_rdo          varchar2(2000);
   begin
      v_rdo := '<fo:inline font-style="italic">' || p_entrada || '</fo:inline>';
      return (v_rdo);
   end;

   function espacios(p_espacios in number)
      return varchar2 is
      v_rdo          varchar2(2000);
   begin
      v_rdo := null;

      for x in 1 .. p_espacios loop
         v_rdo := v_rdo || fof.white_space;
      end loop;

      return v_rdo;
   end;

   function formatear(p_entrada in number)
      return varchar2 is
      v_rdo          varchar2(100);
   begin
      v_rdo := replace(to_char(p_entrada, '9990.00'), '.', ',');
      return (v_rdo);
   end;

   procedure control_1(p_dep in number, p_area in number) is
      vCreditos           number;
      v_aux               number;
      v_mostrada          number;
      v_nombre            varchar2(2000);

      cursor lista_control_1(p_dep in number, p_area in number) is
           select id, nombre, creditos_maximos, creditos_Reduccion, creditos, creditos_asignados, creditos - creditos_asignados creditos_pendientes, categoria_nombre
             from (select p.id, p.nombre, nvl(creditos_maximos, 0) creditos_maximos, nvl(creditos_reduccion, 0) creditos_reduccion, nvl(creditos, 0) creditos, (select nvl(sum(nvl(ip.creditos, i.creditos)), 0) creditos
                                                                                                                                                                  from hor_items_profesores ip, hor_items i
                                                                                                                                                                 where profesor_id = p.id
                                                                                                                                                                   and ip.item_id = i.id)
                                                                                                                                                                  creditos_asignados, categoria_nombre
                     from hor_profesores p
                    where p.departamento_id = p_dep
                      and area_id = p_area)
         order by nombre;
   begin
      select nombre
        into v_nombre
        from hor_areas
       where id = p_area;

      v_aux := 0;
      v_mostrada := 0;

      for x in lista_control_1(p_dep, p_area) loop
         begin
            select nvl(sum(creditos), 0)
              into vCreditos
              from (  select grupo_id, tipo_subgrupo, tipo_subgrupo_id, wm_concat(asignatura_id) asignatura_id, creditos
                        from (  select items.id, items.grupo_id, items.tipo_subgrupo, items.tipo_subgrupo_id, items.subgrupo_id, items.dia_semana_id, items.hora_inicio, items.hora_fin,
                                       rtrim(xmlagg(xmlelement(e, asi.asignatura_id || ', ')).extract('//text()'), ', ') as asignatura_id, to_date(dia_semana_id + 1 || '/01/2006 ' || to_char(hora_fin, 'hh24:mi'), 'dd/mm/yyyy hh24:mi') fin,
                                       to_date(dia_semana_id + 1 || '/01/2006 ' || to_char(hora_inicio, 'hh24:mi'), 'dd/mm/yyyy hh24:mi') inicio, creditos
                                  from uji_horarios.hor_items items, uji_horarios.hor_items_asignaturas asi
                                 where items.id = asi.item_id
                                   and items.id in (select item_id
                                                      from hor_items_profesores
                                                     where profesor_id = x.id)
                              group by items.id, items.grupo_id, items.tipo_subgrupo, items.tipo_subgrupo_id, items.subgrupo_id, items.dia_semana_id, items.semestre_id, items.hora_inicio, items.hora_fin, asi.asignatura_id, creditos)
                    group by grupo_id, tipo_subgrupo, tipo_subgrupo_id, creditos);

            select creditos_asignados
              into vCreditos
              from hor_v_profesor_creditos
             where id = x.id;

            if (x.creditos - vCreditos) < 0 then
               v_aux := v_aux + 1;
            end if;
         exception
            when others then
               vCreditos := 0;
         end;

         if v_aux = 1
        and v_mostrada = 0 then
            fop.block(espacios(2) || fof.bold(v_nombre), font_size => 12, space_before => 0.1);
            v_mostrada := 1;
         end if;

         if (x.creditos - vCreditos) < 0 then
            fop.block(espacios(5) || x.categoria_nombre || ' - ' || x.nombre || ': ' || fof.bold(formatear(x.creditos - vCreditos)), font_size => 10);
         end if;
      end loop;
   end;

   procedure control_2(p_dep in number, p_area in number) is
      v_aux               number;
      v_nombre            varchar2(1000);
      v_sesiones          number;

      cursor lista_solapamientos(p_prof in number, p_semestre in number) is
           select id, min(asignatura_id) asignatura_id, grupo_id, tipo_subgrupo_id || subgrupo_id subgrupo_id, semestre_id, dia_semana_id,
                  decode(dia_semana_id, 1, 'Dilluns', 2, 'Dimarts', 3, 'Dimecres', 4, 'Dijous', 5, 'Divendres', 6, 'Dissabte', 7, 'Diumenge', 'Error') dia_semana, min(hora_inicio) inicio, max(hora_fin) fin
             from (  select id, grupo_id, subgrupo_id, tipo_subgrupo, tipo_subgrupo_id, dia_semana_id, hora_inicio, hora_fin, wm_concat(asignatura_id) asignatura_id, semestre_id,
                            count( * ) over (partition by semestre_id, dia_semana_id) cuantos
                       from (select id, asignatura_id, semestre_id, grupo_id, tipo_subgrupo, tipo_subgrupo_id, subgrupo_id, dia_semana_id, hora_inicio, hora_fin
                               from (  select items.id, items.grupo_id, items.tipo_subgrupo, items.tipo_subgrupo_id, items.subgrupo_id, items.dia_semana_id, items.hora_inicio, items.hora_fin, items.semestre_id,
                                              wm_concat(asi.asignatura_id) asignatura_id
                                         from uji_horarios.hor_items items, uji_horarios.hor_items_asignaturas asi
                                        where items.id = asi.item_id
                                          and items.semestre_id = p_semestre
                                          and items.dia_semana_id is not null
                                          and items.id in (select item_id
                                                             from hor_items_profesores
                                                            where profesor_id = p_prof)
                                     group by asi.asignatura_id, items.id, items.grupo_id, items.tipo_subgrupo, items.tipo_subgrupo_id, items.subgrupo_id, items.dia_semana_id, items.semestre_id, items.hora_inicio, items.hora_fin) x
                              where exists (select 1
                                              from hor_items i, hor_items_profesores p
                                             where i.id = p.item_id
                                               and profesor_id = p_prof
                                               and i.id <> x.id
                                               and i.dia_semana_id = x.dia_Semana_id
                                               and i.semestre_id = x.semestre_id
                                               and (to_number(to_char((i.hora_inicio + 1 / 1440), 'hh24mi')) between to_number(to_char(x.hora_inicio, 'hh24mi')) and to_number(to_char(x.hora_fin, 'hh24mi'))
                                                 or to_number(to_char((i.hora_fin - 1 / 1440), 'hh24mi')) between to_number(to_char(x.hora_inicio, 'hh24mi')) and to_number(to_char(x.hora_fin, 'hh24mi')))))
                   group by id, grupo_id, subgrupo_id, tipo_subgrupo, tipo_subgrupo_id, dia_semana_id, semestre_id, hora_inicio, hora_fin)
            where cuantos > 1
         group by id, semestre_id, dia_Semana_id, grupo_id, tipo_subgrupo_id || subgrupo_id
         order by 5, 6, 2, 3, 4, 6;


      cursor lista_solapes_Semana_Detalle(pProfesor in number, pSemestre in number, pItem in number) is
         select distinct i.grupo_id || ' ' || i.tipo_subgrupo_id || subgrupo_id as evento, inicio, fin
           from hor_v_items_prof_detalle pd join hor_items i on i.id = pd.item_id
          where pd.profesor_id = pProfesor
            and seleccionado = 'S'
            and (pd.inicio, pd.fin) in (select pd2.inicio, pd2.fin
                                          from hor_v_items_prof_detalle pd2
                                         where seleccionado = 'S'
                                           and pd2.profesor_id = pProfesor
                                           and pd2.id <> pd.id
                                           and pd2.item_id = pItem
                                           and trunc(pd2.inicio) = trunc(pd.inicio)
                                           and pd2.inicio <= pd.fin
                                           and pd2.fin >= pd.inicio);


      cursor lista_solapes_Semana_Detalle_2(p_prof in     number,
                                            p_semestre in number,
                                            p_item in     number) is
           select distinct inicio, fin
             from hor_items i, hor_items_detalle d, hor_items_profesores p
            where i.id = d.item_id
              and i.id = p.item_id
              and profesor_id = p_prof
              and (d.inicio, d.fin) in
                        (  select inicio, fin
                             from (select d.inicio, fin
                                     from hor_items i, hor_items_profesores p, hor_items_detalle d, hor_items_det_profesores dp
                                    where i.id = p.item_id
                                      and profesor_id = p_prof
                                      and i.id in (  select id
                                                       from (  select id, grupo_id, subgrupo_id, tipo_subgrupo, tipo_subgrupo_id, dia_semana_id, hora_inicio, hora_fin, wm_concat(asignatura_id) asignatura_id, semestre_id,
                                                                      count( * ) over (partition by semestre_id, dia_semana_id) cuantos, detalle_manual
                                                                 from (select id, asignatura_id, semestre_id, grupo_id, tipo_subgrupo, tipo_subgrupo_id, subgrupo_id, dia_semana_id, hora_inicio, hora_fin, detalle_manual
                                                                         from (  select items.id, items.grupo_id, items.tipo_subgrupo, items.tipo_subgrupo_id, items.subgrupo_id, items.dia_semana_id, items.hora_inicio, items.hora_fin,
                                                                                        items.semestre_id, wm_concat(asi.asignatura_id) asignatura_id, detalle_manual
                                                                                   from uji_horarios.hor_items items, uji_horarios.hor_items_asignaturas asi
                                                                                  where items.id = asi.item_id
                                                                                    and items.semestre_id = p_semestre
                                                                                    and items.dia_semana_id is not null
                                                                                    and items.id in (select item_id
                                                                                                       from hor_items_profesores
                                                                                                      where profesor_id = p_prof)
                                                                               group by asi.asignatura_id, items.id, items.grupo_id, items.tipo_subgrupo, items.tipo_subgrupo_id, items.subgrupo_id, items.dia_semana_id,
                                                                                        items.semestre_id, items.hora_inicio, items.hora_fin, items.detalle_manual) x
                                                                        where exists (select 1
                                                                                        from hor_items i, hor_items_profesores p
                                                                                       where i.id = p.item_id
                                                                                         and profesor_id = p_prof
                                                                                         and i.id <> x.id
                                                                                         and i.dia_semana_id = x.dia_Semana_id
                                                                                         and i.semestre_id = x.semestre_id
                                                                                         and i.detalle_manual = x.detalle_manual
                                                                                         --and i.detalle_manual = p.detalle_manual
                                                                                         and (to_number(to_char((i.hora_inicio + 1 / 1440), 'hh24mi')) between to_number(to_char(x.hora_inicio, 'hh24mi')) and to_number(to_char(x.hora_fin, 'hh24mi'))
                                                                                           or to_number(to_char((i.hora_fin - 1 / 1440), 'hh24mi')) between to_number(to_char(x.hora_inicio, 'hh24mi')) and to_number(to_char(x.hora_fin, 'hh24mi')))))
                                                             group by id, grupo_id, subgrupo_id, tipo_subgrupo, tipo_subgrupo_id, dia_semana_id, semestre_id, hora_inicio, hora_fin, detalle_manual)
                                                      where cuantos > 1
                                                   group by id, semestre_id, dia_Semana_id, grupo_id, tipo_subgrupo_id || subgrupo_id, detalle_manual)
                                      and i.id = d.item_id
                                      and d.id = dp.detalle_id
                                      and p.id = dp.item_profesor_id
                                      and p.detalle_manual = 1
                                   union all
                                   select d.inicio, fin
                                     from hor_items i, hor_items_profesores p, hor_items_detalle d
                                    where i.id = p.item_id
                                      and profesor_id = p_prof
                                      and i.id in (  select id
                                                       from (  select id, grupo_id, subgrupo_id, tipo_subgrupo, tipo_subgrupo_id, dia_semana_id, hora_inicio, hora_fin, wm_concat(asignatura_id) asignatura_id, semestre_id,
                                                                      count( * ) over (partition by semestre_id, dia_semana_id) cuantos, detalle_manual
                                                                 from (select id, asignatura_id, semestre_id, grupo_id, tipo_subgrupo, tipo_subgrupo_id, subgrupo_id, dia_semana_id, hora_inicio, hora_fin, detalle_manual
                                                                         from (  select items.id, items.grupo_id, items.tipo_subgrupo, items.tipo_subgrupo_id, items.subgrupo_id, items.dia_semana_id, items.hora_inicio, items.hora_fin,
                                                                                        items.semestre_id, wm_concat(asi.asignatura_id) asignatura_id, detalle_manual
                                                                                   from uji_horarios.hor_items items, uji_horarios.hor_items_asignaturas asi
                                                                                  where items.id = asi.item_id
                                                                                    and items.semestre_id = p_semestre
                                                                                    and items.dia_semana_id is not null
                                                                                    and items.id in (select item_id
                                                                                                       from hor_items_profesores
                                                                                                      where profesor_id = p_prof)
                                                                               group by asi.asignatura_id, items.id, items.grupo_id, items.tipo_subgrupo, items.tipo_subgrupo_id, items.subgrupo_id, items.dia_semana_id, items.semestre_id,
                                                                                        items.hora_inicio, items.hora_fin, items.detalle_manual) x
                                                                        where exists (select 1
                                                                                        from hor_items i, hor_items_profesores p
                                                                                       where i.id = p.item_id
                                                                                         and profesor_id = p_prof
                                                                                         and i.id <> x.id
                                                                                         and i.dia_semana_id = x.dia_Semana_id
                                                                                         and i.semestre_id = x.semestre_id
                                                                                         and i.detalle_manual = x.detalle_manual
                                                                                         --and i.detalle_manual = p.detalle_manual
                                                                                         and (to_number(to_char((i.hora_inicio + 1 / 1440), 'hh24mi')) between to_number(to_char(x.hora_inicio, 'hh24mi')) and to_number(to_char(x.hora_fin, 'hh24mi'))
                                                                                           or to_number(to_char((i.hora_fin - 1 / 1440), 'hh24mi')) between to_number(to_char(x.hora_inicio, 'hh24mi')) and to_number(to_char(x.hora_fin, 'hh24mi')))))
                                                             group by id, grupo_id, subgrupo_id, tipo_subgrupo, tipo_subgrupo_id, dia_semana_id, semestre_id, hora_inicio, hora_fin, detalle_manual)
                                                      where cuantos > 1
                                                   group by id, semestre_id, dia_Semana_id, grupo_id, tipo_subgrupo_id || subgrupo_id, detalle_manual)
                                      and i.id = d.item_id
                                      and p.detalle_manual = 0)
                         group by inicio, fin
                           having count( * ) > 1)
         order by 1;
   begin
      v_nombre := 'cap';

      for p in lista_profesores(p_dep, p_Area) loop
         for p_Sem in 1 .. 2 loop
            begin
               v_aux := 0;

               for item in lista_solapamientos(p.id, p_sem) loop
                  v_aux := v_aux + 1;

                  if v_aux = 1 then
                     if p.nombre_area <> v_nombre then
                        v_nombre := p.nombre_area;
                        fop.block(espacios(3) || fof.bold(p.nombre_area), font_size => 12, space_after => 0.2, space_before => 0.2);
                     end if;

                     fop.block(espacios(6) || fof.bold('Semestre ' || p_sem || ' de ' || p.nombre), font_size => 10);
                  end if;

                  fop.block(espacios(9) || item.dia_semana || ' - ' || to_char(item.inicio, 'hh24:mi') || '-' || to_char(item.fin, 'hh24:mi') || ' - ' || item.asignatura_id || ' - ' || item.grupo_id || ' ' || item.subgrupo_id, font_size => 10);
                  v_sesiones := 0;

                  --fop.block(p.id || ' ' || p_sem || ' ' || item.id, font_size => 10);

                  for sesiones in lista_solapes_Semana_Detalle(p.id, p_sem, item.id) loop
                     fop.block(espacios(12) || sesiones.evento || '  ' || to_char(sesiones.inicio, 'dd/mm/yyyy') || '    ' || to_char(sesiones.inicio, 'hh24:mi') || ' - ' || to_char(sesiones.fin, 'hh24:mi'), font_size => 10);
                     v_sesiones := v_sesiones + 1;
                  end loop;

                  if v_sesiones = 0 then
                     fop.block(espacios(12) || 'Sols solapa en setmana generica, en semana detall no.', font_size => 10);
                  end if;
               end loop;

               if v_aux > 0 then
                  fop.block(fof.white_space);
               end if;
            exception
               when others then
                  fop.block(sqlerrm);
            end;
         end loop;
      end loop;
   end;

   procedure control_3(p_departamento in number, p_area in number) is
      v_control                 varchar2(200);
      v_crd_final               number;
      v_total                   number;
      v_crd_final_area          number;
      v_total_area              number;
      v_txt                     varchar2(2000);
      v_debug                   varchar2(4000) := '';

      cursor lista_Descuadres_ant(p_dep in number) is
           select id, nombre, nvl(asignatura_txt, asignatura_id) asignatura_id, grupo_id, tipo, crd_final, crd_item, total, nombre_area, asignatura_txt
             from (select id, nombre, asignatura_id, grupo_id, tipo, crd_final, crd_item, nombre_area, sum(crd_final) over (partition by asignatura_id, grupo_id, tipo) total, asignatura_txt
                     from (select distinct d.id, d.nombre, asignatura_id, grupo_id, tipo, crd_final, crd_item, a.nombre nombre_area, asignatura_txt
                             from hor_v_items_creditos_detalle d, hor_areas a
                            where d.departamento_id = p_dep
                              and area_id = a.id))
            where crd_item <> total
              and asignatura_id in (select asignatura_id
                                      from hor_v_asignaturas_area aa, hor_Areas a
                                     where area_id = a.id
                                       and departamento_id = p_dep
                                       and porcentaje > 0)
         order by asignatura_id, grupo_id, tipo, nombre;

      cursor lista_Descuadres(p_dep in number) is
           select id, nombre, nvl(asignatura_txt, asignatura_id) asignatura_id, grupo_id, tipo, crd_final, crd_item, total, nombre_area, asignatura_Txt
             from (  select id, nombre, asignatura_id, grupo_id, tipo, crd_final, crd_item, total, nombre_area, min(asignatura_txt) asignatura_txt
                       from (select id, nombre, asignatura_id, grupo_id, tipo, crd_final, crd_item, nombre_area, sum(crd_final) over (partition by asignatura_id, grupo_id, tipo) total, asignatura_txt
                               from (select distinct d.id, d.nombre, asignatura_id, grupo_id, tipo, crd_final, crd_item, a.nombre nombre_area, asignatura_txt
                                       from hor_v_items_creditos_detalle d, hor_areas a
                                      where d.departamento_id = p_dep
                                        and area_id = a.id))
                      where crd_item <> total
                        and asignatura_id in (select asignatura_id
                                                from hor_v_asignaturas_area aa, hor_Areas a
                                               where area_id = a.id
                                                 and departamento_id = p_dep
                                                 and porcentaje > 0)
                   group by id, nombre, asignatura_id, grupo_id, tipo, crd_final, crd_item, total, nombre_area)
         order by asignatura_id, grupo_id, tipo, nombre;

      /* 25/02/2016 - Se elimina asignatura_txt del distinct ya que el campo de las asignaturas separadas por comas no siempre es coherente en el orden de las mismas, por lo que falsea */
      /* los calculos del total de créditos */
      cursor lista_Descuadres_new(p_dep in number) is
           select id, nombre, nvl(asignatura_txt, asignatura_id) asignatura_id, grupo_id, tipo, crd_final, crd_item, total, nombre_area, asignatura_Txt
             from (  select id, nombre, asignatura_id, grupo_id, tipo, crd_final, crd_item, total, nombre_area, min(asignatura_txt) asignatura_txt
                       from (select id, nombre, asignatura_id, grupo_id, tipo, crd_final, crd_item, nombre_area, sum(crd_final) over (partition by asignatura_id, grupo_id, tipo) total, asignatura_txt
                               from (select distinct d.id, d.nombre, asignatura_id, grupo_id, tipo, crd_final, crd_item, a.nombre nombre_area, asignatura_id asignatura_txt
                                       from hor_v_items_creditos_detalle d, hor_areas a
                                      where d.departamento_id = p_dep
                                        and area_id = a.id))
                      where crd_item <> total
                        and asignatura_id in (select asignatura_id
                                                from hor_v_asignaturas_area aa, hor_Areas a
                                               where area_id = a.id
                                                 and departamento_id = p_dep
                                                 and porcentaje > 0)
                   group by id, nombre, asignatura_id, grupo_id, tipo, crd_final, crd_item, total, nombre_area)
         order by asignatura_id, grupo_id, tipo, nombre;

      cursor lista_Descuadres_area(p_dep in number, p_area in number) is
           select id, nombre, nvl(asignatura_txt, asignatura_id) asignatura_id, grupo_id, tipo, crd_final, crd_item, total, nombre_area, asignatura_Txt
             from (  select id, nombre, asignatura_id, grupo_id, tipo, crd_final, crd_item, total, nombre_area, min(asignatura_txt) asignatura_txt
                       from (select id, nombre, asignatura_id, grupo_id, tipo, crd_final, crd_item, nombre_area, sum(crd_final) over (partition by asignatura_id, grupo_id, tipo) total, asignatura_txt
                               from (select distinct d.id, d.nombre, asignatura_id, grupo_id, tipo, crd_final, crd_item, a.nombre nombre_area, asignatura_id asignatura_txt
                                       from hor_v_items_creditos_detalle d, hor_areas a
                                      where d.departamento_id = p_dep
                                        and area_id = a.id))
                      where crd_item <> total
                        and asignatura_id in (select asignatura_id
                                                from hor_v_asignaturas_area aa
                                               where area_id = p_area
                                                 and porcentaje > 0)
                   group by id, nombre, asignatura_id, grupo_id, tipo, crd_final, crd_item, total, nombre_area)
         order by asignatura_id, grupo_id, tipo, nombre;
   begin
      v_aux := 0;
      v_control := 'x';

      v_crd_final_area := 0;
      v_total_area := 0;

      for p in lista_descuadres_area(p_departamento, p_area) loop
         v_aux := v_aux + 1;

         if v_aux = 1 then
            fop.tableOpen;
            fop.tablecolumndefinition(column_width => 2);
            fop.tablecolumndefinition(column_width => 2);
            fop.tablecolumndefinition(column_width => 7);
            fop.tablecolumndefinition(column_width => 2);
            fop.tablecolumndefinition(column_width => 2);
            fop.tablecolumndefinition(column_width => 2);
            fop.tablebodyopen;
            fop.tableRowOpen;
            fop.tablecellopen;
            fop.block(fof.white_space, font_size => 10);
            fop.tablecellclose;
            fop.tablecellopen;
            fop.block(fof.bold('Crd Grup'), font_size => 10, text_align => 'center');
            fop.tablecellclose;
            fop.tablecellopen;
            fop.block(fof.white_space, font_size => 10);
            fop.tablecellclose;
            fop.tablecellopen;
            fop.block(fof.bold('Grup'), font_size => 10, text_align => 'center');
            fop.tablecellclose;
            fop.tablecellopen;
            fop.block(fof.bold('Crd Prof'), font_size => 10, text_align => 'right');
            fop.tablecellclose;
            fop.tablecellopen;
            fop.block(fof.bold('Diferència'), font_size => 10, text_align => 'right');
            fop.tablecellclose;
            fop.tableRowClose;
         else
            if v_control <> p.asignatura_id || ' ' || p.grupo_id || ' ' || p.tipo then
               fop.tableRowOpen;
               fop.tablecellopen;
               fop.block(fof.white_space, font_size => 10);
               fop.tablecellclose;
               fop.tablecellopen;
               fop.block(fof.white_space, font_size => 10);
               fop.tablecellclose;
               fop.tablecellopen;
               fop.block(fof.white_space, font_size => 10);
               fop.tablecellclose;
               fop.tablecellopen;
               fop.block(fof.white_space, font_size => 10);
               fop.tablecellclose;
               fop.tablecellopen;
               fop.block(formatear(v_total), font_size => 10, text_align => 'right');
               fop.tablecellclose;
               fop.tablecellopen;
               fop.block(fof.bold(formatear(v_total - v_crd_final)), font_size => 14, text_align => 'right');
               fop.tablecellclose;
               fop.tableRowClose;
               fop.tableRowOpen;
               fop.tablecellopen;
               fop.block(fof.white_space, font_size => 10);
               fop.tablecellclose;
               fop.tableRowClose;
            end if;
         end if;

         v_txt := p.asignatura_id;

         if instr(p.asignatura_id, ',') > 0 then
            v_txt := replace(p.asignatura_id, ',', ', ');
         end if;

         fop.tableRowOpen;
         fop.tablecellopen;
         fop.block(v_txt, font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block(formatear(p.crd_item), font_size => 10, text_align => 'center');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block(p.nombre || ' (' || italic(p.nombre_area) || ')', font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block(p.grupo_id || ' - ' || p.tipo, font_size => 10, text_align => 'center');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block(formatear(p.crd_final), font_size => 10, text_align => 'right');
         fop.tablecellclose;
         fop.tableRowClose;

         if (v_control <> p.asignatura_id || ' ' || p.grupo_id || ' ' || p.tipo) then
            v_crd_final_area := v_crd_final_area + p.crd_item;
         end if;
         v_total_area := v_total_area + p.crd_final;
         v_control := p.asignatura_id || ' ' || p.grupo_id || ' ' || p.tipo;
         v_crd_final := p.crd_item;
         v_total := p.total;
      end loop;

      if v_aux >= 1 then
         fop.tableRowOpen;
         fop.tablecellopen;
         fop.block(fof.white_space, font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block(fof.white_space, font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block(fof.white_space, font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block(fof.white_space, font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block(formatear(v_total), font_size => 10, text_align => 'right');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block(fof.bold(formatear(nvl(v_total, 0) - nvl(v_crd_final, 0))), font_size => 14, text_align => 'right');
         fop.tablecellclose;
         fop.tableRowClose;
      end if;

      -- Total Area
      fop.tableRowOpen;
      fop.tablecellopen;
      fop.block('Total', font_size => 16);
      fop.tablecellclose;
      fop.tablecellopen;
      fop.block(formatear(nvl(v_crd_final_area, 0)), font_size => 13, text_align => 'center');
      fop.tablecellclose;
      fop.tablecellopen;
      fop.block(fof.white_space, font_size => 10);
      fop.tablecellclose;
      fop.tablecellopen;
      fop.block(fof.white_space, font_size => 10);
      fop.tablecellclose;
      fop.tablecellopen;
      fop.block(formatear(v_total_area), font_size => 13, text_align => 'right');
      fop.tablecellclose;
      fop.tablecellopen;
      fop.block(fof.bold(formatear(nvl(v_total_area, 0) - nvl(v_crd_final_area, 0))), font_size => 16, text_align => 'right');
      fop.tablecellclose;
      fop.tableRowClose;
      if v_aux > 0 then
         fop.tablebodyclose;
         fop.tableClose;
      end if;
      fop.block(fof.white_space);
   end;

   procedure control_4(p_dep in number) is
      v_total_area          number;
      v_area                number;
      v_acum1               number;
      v_acum2               number;
   begin
      fop.tableOpen;
      fop.tablecolumndefinition(column_width => 9);
      fop.tablecolumndefinition(column_width => 2);
      fop.tablecolumndefinition(column_width => 2);
      fop.tablecolumndefinition(column_width => 2);
      fop.tablecolumndefinition(column_width => 3);
      fop.tablebodyopen;
      fop.tableRowOpen;
      fop.tablecellopen;
      fop.block(fof.bold('Àrees de coneixement'), font_size => 10);
      fop.tablecellclose;
      fop.tablecellopen;
      fop.block(fof.bold('Crèdits a impartir'), font_size => 10, text_align => 'right');
      fop.tablecellclose;
      fop.tablecellopen;
      fop.block(fof.bold('Crèdits assignats'), font_size => 10, text_align => 'right');
      fop.tablecellclose;
      fop.tablecellopen;
      fop.block(fof.bold('% assig- nació'), font_size => 10, text_align => 'right');
      fop.tablecellclose;
      fop.tableRowClose;
      v_acum1 := 0;
      v_acum2 := 0;

      for x in lista_areas(p_dep) loop
         /*
         select nvl(sum(creditos * porcentaje / 100), 0) crd_total_area
           into v_total_area
           from (select distinct nvl(comun_texto, asignatura_id) asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo_id, creditos, porcentaje
                   from hor_items i, hor_items_asignaturas a, hor_ext_asignaturas_Area aa
                  where i.id = a.item_id
                    and asignatura_id = asi_id
                    and uest_id = x.id);
                    */

         /* comentado 22/02/2016
         select round (nvl (sum (creditos * porcentaje / 100 * porcentaje_comun / 100), 0), 2) crd_total_area
         into   v_total_area
         from   (select distinct asignatura_id, asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo_id, creditos,
                                 porcentaje, 100 porcentaje_comun
                 from            hor_items i,
                                 hor_items_asignaturas a,
                                 hor_ext_asignaturas_Area aa
                 where           i.id = a.item_id
                 and             asignatura_id = asi_id
                 and             uest_id = x.id
                 and             comun = 0
                 union all
                 select distinct comun_texto, a.asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo_id, creditos,
                                 aa.porcentaje, c.porcentaje porcentaje_comun
                 from            hor_items i,
                                 hor_items_asignaturas a,
                                 hor_ext_asignaturas_Area aa,
                                 hor_ext_asignaturas_comunes c
                 where           i.id = a.item_id
                 and             a.asignatura_id = asi_id
                 and             uest_id = x.id
                 and             comun = 1
                 and             a.asignatura_id = c.asignatura_id);
                 */

         /* version febrero 2016 */
         select round(nvl(sum(creditos * porcentaje / 100), 0), 2) crd_total_area
           into v_total_area
           from (select distinct asignatura_id comun_texto, asignatura_id, porcentaje, 100 porcentaje_comun, uji_horarios.hor_Calcular_creditos_asi(asignatura_id, curso.id) creditos
                   from hor_items i, hor_items_asignaturas a, hor_ext_asignaturas_Area aa, hor_curso_academico curso
                  where i.id = a.item_id
                    and asignatura_id = asi_id
                    and uest_id = x.id);

         /*
         select nvl (sum (creditos), 0)
         into   v_Area
         from   (select distinct d.profesor_id, nvl (comun_Texto, asignatura_id) asignatura_id, grupo_id,
                                 tipo_subgrupo_id, subgrupo_id, uest_id area_id,
                                 round (nvl (creditos_detalle, d.creditos) * porcentaje / 100, 2) creditos
                 from            hor_v_items_prof_detalle d,
                                 hor_items i,
                                 hor_items_asignaturas a,
                                 hor_ext_asignaturas_area aa
                 where           d.item_id = i.id
                 and             i.id = a.item_id
                 and             asignatura_id = asi_id
                 and             uest_id = x.id);
                 */
         select nvl(sum(creditos), 0)
           into v_area
           from (  select distinct d.profesor_id, nvl(comun_Texto, asignatura_id) asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo_id, uest_id area_id, --nvl (creditos_detalle, d.creditos) * porcentaje / 100 creditos
                                                                                                                                                          nvl(creditos_detalle, d.creditos) creditos
                     from hor_v_items_prof_detalle d, hor_items i, hor_items_asignaturas a, hor_ext_asignaturas_area aa, hor_profesores h
                    where d.item_id = i.id
                      and i.id = a.item_id
                      and asignatura_id = asi_id
                      and uest_id = x.id
                      and d.profesor_id = h.id
                      and h.area_id = uest_id
                 group by d.profesor_id, nvl(comun_Texto, asignatura_id), grupo_id, tipo_subgrupo_id, subgrupo_id, uest_id, creditos_detalle, d.creditos, porcentaje);

         fop.tableRowOpen;
         fop.tablecellopen;
         fop.block(x.nombre, font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block(formatear(v_total_area), font_size => 10, text_align => 'right');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block(formatear(v_area), font_size => 10, text_align => 'right');
         fop.tablecellclose;
         fop.tablecellopen;

         if v_total_area <> 0 then
            fop.block(formatear(round(v_area / v_total_area * 100, 2)) || '%', font_size => 10, text_align => 'right');
         else
            fop.block(formatear(0) || '%', font_size => 10, text_align => 'right');
         end if;

         fop.tablecellclose;

         if v_area > v_total_area then
            fop.tablecellopen;
            fop.block(fof.bold('Excés de crèdits'), font_size => 10, text_align => 'right');
            fop.tablecellclose;
         end if;

         fop.tableRowClose;
         v_acum1 := v_acum1 + v_total_Area;
         v_acum2 := v_acum2 + v_area;
      end loop;

      fop.tableRowOpen;
      fop.tablecellopen;
      fop.block(fof.white_space, font_size => 10);
      fop.tablecellclose;
      fop.tablecellopen;
      fop.block(fof.bold(formatear(v_acum1)), font_size => 10, text_align => 'right');
      fop.tablecellclose;
      fop.tablecellopen;
      fop.block(fof.bold(formatear(v_acum2)), font_size => 10, text_align => 'right');
      fop.tablecellclose;
      fop.tableRowClose;
      fop.tableBodyClose;
      fop.tableClose;
   end;

   procedure control_5(p_dep in number) is
      numero_solapes          number;

      cursor lista_solapamientos_pdi(p_dep in number) is
           select id, wm_concat(asignatura_id) asignatura, grupo_id, tipo_subgrupo_id, subgrupo_id, dia_semana_id, dia_semana, hora_inicio, hora_fin
             from (  select distinct i.id, a.asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo_id, dia_semana_id,
                                     decode(dia_semana_id, 1, 'Dilluns', 2, 'Dimarts', 3, 'Dimecres', 4, 'Dijous', 5, 'Divendres', 6, 'Dissabte', 7, 'Diumenge', 'No planificat') dia_semana, hora_inicio, hora_fin
                       from hor_items i join hor_items_asignaturas a on i.id = a.item_id join hor_items_detalle idet on idet.item_id = i.id
                      where idet.id in (  select idp.detalle_id
                                            from hor_items_det_profesores idp join hor_items_profesores itemprof on itemprof.id = idp.item_profesor_id join hor_profesores prof on prof.id = itemprof.profesor_id
                                           where prof.departamento_id = p_dep
                                        group by idp.detalle_id
                                          having count( * ) > 1)
                   order by dia_semana_id, tipo_subgrupo_id)
         group by id, asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo_id, dia_semana_id, dia_semana, hora_inicio, hora_fin;
   /* select i.id, WM_CONCAT(a.asignatura_id) asignatura, grupo_id, tipo_subgrupo_id, subgrupo_id, dia_semana_id,
           decode(dia_semana_id, 1, 'Dilluns', 2, 'Dimarts', 3, 'Dimecres', 4, 'Dijous', 5, 'Divendres', 6, 'Dissabte', 7, 'Diumenge', 'No planificat') dia_semana, hora_inicio, hora_fin
      from hor_items i join hor_items_asignaturas a on i.id = a.item_id join hor_items_detalle idet on idet.item_id = i.id
     where idet.id in (  select idp.detalle_id
                           from hor_items_det_profesores idp join hor_items_profesores itemprof on itemprof.id = idp.item_profesor_id join hor_profesores prof on prof.id = itemprof.profesor_id
                          where prof.departamento_id = p_dep
                       group by idp.detalle_id
                         having count( * ) > 1)
  group by i.id, grupo_id, tipo_subgrupo_id, subgrupo_id, dia_semana_id, decode(dia_semana_id, 1, 'Dilluns', 2, 'Dimarts', 3, 'Dimecres', 4, 'Dijous', 5, 'Divendres', 6, 'Dissabte', 7, 'Diumenge', 'No planificat'), hora_inicio,
           hora_fin
  order by dia_semana_id, WM_CONCAT(a.asignatura_id), tipo_subgrupo_id;*/
   begin
      numero_solapes := 0;

      for s in lista_solapamientos_pdi(p_dep) loop
         numero_solapes := numero_solapes + 1;
      end loop;

      if (numero_solapes = 0) then
         fop.block('No s''ha detectat cap solapament', font_size => 10);
         return;
      end if;

      fop.tableOpen;
      fop.tablecolumndefinition(column_width => 6);
      fop.tablecolumndefinition(column_width => 4);
      fop.tablecolumndefinition(column_width => 4);
      fop.tablebodyopen;
      fop.tableRowOpen;
      fop.tablecellopen;
      fop.block(fof.bold('Assignatura i subgrup'), font_size => 10);
      fop.tablecellclose;
      fop.tablecellopen;
      fop.block(fof.bold('Dia setmana'), font_size => 10);
      fop.tablecellclose;
      fop.tablecellopen;
      fop.block(fof.bold('Horari'), font_size => 10);
      fop.tablecellclose;
      fop.tableRowClose;

      for s in lista_solapamientos_pdi(p_dep) loop
         fop.tableRowOpen;
         fop.tablecellopen;
         fop.block(s.asignatura || ' ' || s.grupo_id || ' ' || s.tipo_subgrupo_id || s.subgrupo_id, font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block(s.dia_semana, font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block(to_char(s.hora_inicio, 'HH24:MI') || ' - ' || to_char(s.hora_fin, 'HH24:MI'), font_size => 10);
         fop.tablecellclose;
         fop.tableRowClose;
      end loop;

      fop.tableBodyClose;
      fop.tableClose;
   end;


   procedure control_6(p_dep in number) is
      numero_solapes          number;
      UMBRAL_AVISO            number := 5;

      cursor lista_descuadres_creditos(p_dep in number) is
         select *
           from (  select p.id, p.nombre, p.categoria_nombre, p.creditos, (select sum(nvl((fin - inicio) * 24 / 10, nvl(creditos_detalle, creditos)))
                                                                             from hor_v_items_prof_detalle d
                                                                            where profesor_id = p.id
                                                                              and seleccionado = 'S')
                                                                             as creditos_sesiones
                     from hor_profesores p
                    where departamento_id = p_dep
                 order by nombre)
          where abs(creditos - creditos_sesiones) > UMBRAL_AVISO;
   begin
      numero_solapes := 0;

      for s in lista_descuadres_creditos(p_dep) loop
         numero_solapes := numero_solapes + 1;
      end loop;

      if (numero_solapes = 0) then
         fop.block('No s''ha detectat desquadre', font_size => 10);
         return;
      end if;

      fop.tableOpen;
      fop.tablecolumndefinition(column_width => 8);
      fop.tablecolumndefinition(column_width => 4);
      fop.tablecolumndefinition(column_width => 4);
      fop.tablecolumndefinition(column_width => 2);
      fop.tablebodyopen;
      fop.tableRowOpen;
      fop.tablecellopen;
      fop.block(fof.bold('Professor'), font_size => 10);
      fop.tablecellclose;
      fop.tablecellopen;
      fop.block(fof.bold('Crèdits professor'), font_size => 10, text_align => 'right');
      fop.tablecellclose;
      fop.tablecellopen;
      fop.block(fof.bold('Crèdits sessions'), font_size => 10, text_align => 'right');
      fop.tablecellclose;
      fop.tablecellopen;
      fop.block(fof.bold('Diff'), font_size => 10, text_align => 'right');
      fop.tablecellclose;
      fop.tableRowClose;

      for s in lista_descuadres_creditos(p_dep) loop
         fop.tableRowOpen;
         fop.tablecellopen;
         fop.block(s.nombre, font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block(formatear(s.creditos), font_size => 10, text_align => 'right');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block(formatear(s.creditos_sesiones), font_size => 10, text_align => 'right');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block(formatear(s.creditos_sesiones - s.creditos), font_size => 10, text_align => 'right');
         fop.tablecellclose;
         fop.tableRowClose;
      end loop;

      fop.tableBodyClose;
      fop.tableClose;
   end;
begin
   select count( * ) + 1
     into vSesion
     from apa_sesiones
    where sesion_id = pSesion
      and fecha < sysdate() - 0.33;

   select id
     into vCurso
     from hor_curso_academico;

   if vSesion > 0 then
      cabecera;

      begin
         fop.block(fof.bold('Control 1 - Professors amb excés de crèdits assignats'), font_size => 16);
         fop.block(fof.white_space);
         for a in lista_areas(pDepartamento) loop
            control_1(pDepartamento, a.id);
         end loop;
         fop.block(fof.white_space);
         fop.block(fof.white_space);
      exception
         when others then
            fop.block('Error control 1: ' || sqlerrm, font_size => 12);
      end;

      begin
         fop.block(fof.bold('Control 2 - Solapaments (a setmana generica)'), font_size => 16);
         fop.block(fof.white_space);
         for a in lista_areas(pDepartamento) loop
            control_2(pDepartamento, a.id);
         end loop;
         fop.block(fof.white_space);
         fop.block(fof.white_space);
      exception
         when others then
            fop.block('Error control 2: ' || sqlerrm, font_size => 12);
      end;

      begin
         fop.block(fof.bold('Control 3 - Desquadres de crèdits per assignatura'), font_size => 16);
         fop.block(fof.white_space);
         for a in lista_areas(pDepartamento) loop
            fop.block(fof.bold('Àrea ' || a.nombre), font_size => 13);
            fop.block(fof.white_space);
            control_3(pDepartamento, a.id);
         end loop;
         fop.block(fof.white_space);
         fop.block(fof.white_space);
      exception
         when others then
            fop.block('Error control 3: ' || sqlerrm, font_size => 12);
      end;

      begin
         fop.block(fof.bold('Control 4 - Estat d''assignació de crèdits per àrea de coneixement'), font_size => 16);
         fop.block(fof.white_space);
         control_4(pDepartamento);
         fop.block(fof.white_space);
         fop.block(fof.white_space);
      exception
         when others then
            fop.block('Error control 4: ' || sqlerrm, font_size => 12);
      end;

      begin
         fop.block(fof.bold('Control 5 - Subgrups amd més de un PDI assignat'), font_size => 16);
         fop.block(fof.white_space);
         control_5(pDepartamento);
         fop.block(fof.white_space);
         fop.block(fof.white_space);
      exception
         when others then
            fop.block('Error control 5: ' || sqlerrm, font_size => 12);
      end;

      begin
         fop.block(fof.bold('Control 6 - Desquadraments de crèdits per sessions'), font_size => 16);
         fop.block(fof.white_space);
         control_6(pDepartamento);
         fop.block(fof.white_space);
         fop.block(fof.white_space);
      exception
         when others then
            fop.block('Error control 6: ' || sqlerrm, font_size => 12);
      end;

      pie;
   else
      cabecera;
      fop.block('No tens permís per accedir a aquest document.', font_weight => 'bold', text_align => 'center', font_size => 14, space_after => 0.5);
      pie;
   end if;
end;


CREATE OR REPLACE function UJI_HORARIOS.hor_asignaturas_item(p_item in varchar2)
   return varchar2 is
   v_rdo          varchar2(1000);
begin
     select wm_concat(asignatura_id)
       into v_rdo
       from hor_items_asignaturas ia
      where ia.item_id = p_item
   order by asignatura_id;

   return (v_rdo);
exception
   when others then
      return (sqlerrm);
end hor_asignaturas_item;


CREATE OR REPLACE FUNCTION UJI_HORARIOS.hor_calcular_creditos_asi (p_asignatura in varchar2, p_curso in number)
   RETURN NUMBER IS
   v_aux   NUMBER;
BEGIN
   v_aux := gri_www.calcular_creditos_pod_Test (p_asignatura, p_curso);
   RETURN v_aux;
END hor_calcular_creditos_asi;
