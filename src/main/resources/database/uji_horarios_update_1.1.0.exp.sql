ALTER TABLE UJI_HORARIOS.HOR_ITEMS
  DROP CONSTRAINT HOR_ITEMS_HOR_PROFESORES_FK;


  
CREATE TABLE uji_horarios.hor_items_det_profesores 
    ( 
     id NUMBER  NOT NULL , 
     detalle_id NUMBER  NOT NULL , 
     item_profesor_id NUMBER  NOT NULL 
    ) 
;


CREATE INDEX uji_horarios.hor_items_det_prof_det_IDX ON uji_horarios.hor_items_det_profesores 
    ( 
     detalle_id ASC 
    ) 
;
CREATE INDEX uji_horarios.hor_items_det_prof_prof_IDX ON uji_horarios.hor_items_det_profesores 
    ( 
     item_profesor_id ASC 
    ) 
;

ALTER TABLE uji_horarios.hor_items_det_profesores 
    ADD CONSTRAINT hor_items_det_profesores_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_horarios.hor_items_profesores 
    ( 
     id NUMBER  NOT NULL , 
     item_id NUMBER  NOT NULL , 
     profesor_id NUMBER  NOT NULL 
    ) 
;


CREATE INDEX uji_horarios.hor_items_prof_item_IDX ON uji_horarios.hor_items_profesores 
    ( 
     item_id ASC 
    ) 
;
CREATE INDEX uji_horarios.hor_items_prof_prof_IDX ON uji_horarios.hor_items_profesores 
    ( 
     profesor_id ASC 
    ) 
;

ALTER TABLE uji_horarios.hor_items_profesores 
    ADD CONSTRAINT hor_items_profesores_PK PRIMARY KEY ( id ) ;




ALTER TABLE uji_horarios.hor_items_det_profesores 
    ADD CONSTRAINT hor_items_det_prof_det_FK FOREIGN KEY 
    ( 
     detalle_id
    ) 
    REFERENCES uji_horarios.hor_items_detalle 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_items_det_profesores 
    ADD CONSTRAINT hor_items_det_prof_it_prof_FK FOREIGN KEY 
    ( 
     item_profesor_id
    ) 
    REFERENCES uji_horarios.hor_items_profesores 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_items_profesores 
    ADD CONSTRAINT hor_items_prof_items_FK FOREIGN KEY 
    ( 
     item_id
    ) 
    REFERENCES uji_horarios.hor_items 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_items_profesores 
    ADD CONSTRAINT hor_items_prof_prof_FK FOREIGN KEY 
    ( 
     profesor_id
    ) 
    REFERENCES uji_horarios.hor_profesores 
    ( 
     id
    ) 
;

CREATE OR REPLACE VIEW uji_horarios.hor_Ext_asignaturas_Area as
select *
from pod_asignaturas_area
where curso_aca = 2013;


CREATE OR REPLACE VIEW uji_horarios.hor_v_asignaturas_area  AS
SELECT DISTINCT asignatura_id,
  uest_id area_id,
  porcentaje,
  recibe_acta
FROM hor_ext_asignaturas_area aa,
  hor_items_asignaturas a
WHERE asignatura_id = asi_id ;


ALTER TABLE UJI_HORARIOS.HOR_ITEMS DROP COLUMN PROFESOR_ID;

ALTER TABLE UJI_HORARIOS.HOR_ITEMS
 ADD (creditos  NUMBER);



ALTER TABLE UJI_HORARIOS.HOR_DEPARTAMENTOS
 ADD (consulta_pod  NUMBER                          DEFAULT 0                     NOT NULL);

ALTER TABLE UJI_HORARIOS.HOR_PERMISOS_EXTRA
 ADD (area_id  NUMBER);


ALTER TABLE uji_horarios.hor_permisos_extra 
    ADD CONSTRAINT hor_perm_ext_areas_FK FOREIGN KEY 
    ( 
     area_id
    ) 
    REFERENCES uji_horarios.hor_areas 
    ( 
     id
    ) 
;


ALTER TABLE UJI_HORARIOS.HOR_PROFESORES
 ADD (creditos  NUMBER);

ALTER TABLE UJI_HORARIOS.HOR_ITEMS_PROFESORES
 ADD (detalle_manual  NUMBER                        DEFAULT 0                     NOT NULL);


ALTER TABLE uji_horarios.hor_items_profesores 
    ADD CONSTRAINT hor_items_profesores_UN UNIQUE ( item_id , profesor_id ) ;

 