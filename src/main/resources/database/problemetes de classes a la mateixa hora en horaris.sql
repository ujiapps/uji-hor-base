/* Formatted on 11/07/2013 12:54 (Formatter Plus v4.8.8) */
select   asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo_id, semestre_id, dia_Semana_id,
         to_char (hora_inicio, 'hh24:mi') hora_inicio, count (*)
from     uji_horarios.hor_items i,
         uji_horarios.hor_items_asignaturas a
where    i.id = a.item_id
--and      asignatura_id = 'AE1003'
--and      tipo_subgrupo_id = 'LA'
--and      subgrupo_id = 37
group by asignatura_id,
         grupo_id,
         tipo_subgrupo_id,
         subgrupo_id,
         semestre_id,
         dia_Semana_id,
         to_char (hora_inicio, 'hh24:mi')
having   count (*) > 1
order by 1




select   asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo_id, semestre_id, dia_Semana_id,
         hora_inicio, hora_fin
from     uji_horarios.hor_items i,
         uji_horarios.hor_items_asignaturas a
where    i.id = a.item_id
and      asignatura_id = 'AE1003'
and      tipo_subgrupo_id = 'LA'
and      subgrupo_id = 37

select   asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo_id, semestre_id, dia_Semana_id,
         hora_inicio, hora_fin
from     uji_horarios.hor_items i,
         uji_horarios.hor_items_asignaturas a
where    i.id = a.item_id
and      asignatura_id = 'FC1016'
and      tipo_subgrupo_id = 'PR'
and      subgrupo_id = 8


hor_items_detalle

select *
from pod_horarios
where sgr_grp_curso_aca = 2013
and sgr_grp_asi_id = 'AE1003'
and sgr_tipo = 'LA'
and sgr_id= 37

select *
from pod_horarios_fechas




/* Formatted on 12/07/2013 08:45 (Formatter Plus v4.8.8) */
declare
   v_curso_aca      number         := 2013;
   v_existe         number;
   v_acciones       varchar2 (1)   := 'S';
   v_acciones_det   varchar2 (1)   := 'S';
   v_inicio         varchar2 (100) := null;

   cursor c1 is

      select   asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo_id, semestre_id, dia_Semana_id,
               to_char (hora_inicio, 'hh24:mi') hora_inicio, estudio_id, curso_id, decode (comun, 0, 'N', 'S') comun,
               count (*) cuantos
      from     uji_horarios.hor_items i,
               uji_horarios.hor_items_asignaturas a
      where    i.id = a.item_id
      and      asignatura_id = 'MD1109'
--      and      tipo_subgrupo_id = 'LA'
--      and      subgrupo_id = 12
      group by asignatura_id,
               grupo_id,
               tipo_subgrupo_id,
               subgrupo_id,
               semestre_id,
               dia_Semana_id,
               to_char (hora_inicio, 'hh24:mi'),
               estudio_id,
               curso_id,
               comun
      having   count (*) > 1
      order by 1;

   cursor c2 (
      p_asi    in   varchar2,
      p_grp    in   varchar2,
      p_tipo   in   varchar2,
      p_id     in   number,
      p_sem    in   number,
      p_dia    in   number,
      p_ini    in   varchar2
   ) is
      select distinct asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo_id, semestre_id, dia_Semana_id,
                      to_char (hora_inicio, 'hh24:mi') hora_inicio, to_char (hora_fin, 'hh24:mi') hora_fin,
                      hora_fin fin_completo, aula_planificacion_id
      from            uji_horarios.hor_items i,
                      uji_horarios.hor_items_asignaturas a
      where           i.id = a.item_id
      and             asignatura_id = p_asi
      and             grupo_id = p_grp
      and             tipo_subgrupo_id = p_tipo
      and             subgrupo_id = p_id
      and             semestre_id = p_sem
      and             dia_semana_id = p_dia
      and             to_char (hora_inicio, 'hh24:mi') = p_ini
      and             dia_semana_id is not null
      group by        asignatura_id,
                      grupo_id,
                      tipo_subgrupo_id,
                      subgrupo_id,
                      semestre_id,
                      dia_Semana_id,
                      to_char (hora_inicio, 'hh24:mi'),
                      to_char (hora_fin, 'hh24:mi'),
                      hora_fin,
                      aula_planificacion_id
      order by        8;

   cursor c3 (
      p_asi    in   varchar2,
      p_grp    in   varchar2,
      p_tipo   in   varchar2,
      p_id     in   number,
      p_sem    in   number,
      p_dia    in   number,
      p_ini    in   varchar2,
      p_fin    in   varchar2
   ) is
      select distinct asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo_id, semestre_id, dia_Semana_id, hora_inicio,
                      hora_fin, inicio, fin, aula_planificacion_id
      from            uji_horarios.hor_items i,
                      uji_horarios.hor_items_asignaturas a,
                      uji_horarios.hor_items_detalle d
      where           i.id = a.item_id
      and             i.id = d.item_id
      and             asignatura_id = p_asi
      and             grupo_id = p_grp
      and             tipo_subgrupo_id = p_tipo
      and             subgrupo_id = p_id
      and             semestre_id = p_sem
      and             dia_semana_id = p_dia
      and             dia_semana_id is not null
      and             to_char (inicio, 'hh24:mi') = p_ini
      and             to_number (to_char (fin, 'hh24mi')) >=
                                       to_number (to_char (to_date ('1-1-1 ' || p_fin, 'dd-mm-yyyy hh24:mi'), 'hh24mi'));

   cursor lista_inicial is
      select 'ALTER TRIGGER gra_pod.pod_horarios_blk DISABLE' accion
      from   dual
      union all
      select 'ALTER TRIGGER gra_pod.pod_gen_fecha_hor_at DISABLE' accion
      from   dual
      union all
      select 'ALTER TRIGGER gra_pod.pod_gen_fecha_hor DISABLE' accion
      from   dual
      union all
      select 'ALTER TRIGGER gra_pod.pod_horarios_fecha_t DISABLE' accion
      from   dual;

   cursor lista_final is
      select 'ALTER TRIGGER gra_pod.pod_horarios_blk enable' accion
      from   dual
      union all
      select 'ALTER TRIGGER gra_pod.pod_gen_fecha_hor enable' accion
      from   dual
      union all
      select 'ALTER TRIGGER gra_pod.pod_gen_fecha_hor_at enable' accion
      from   dual
      union all
      select 'ALTER TRIGGER gra_pod.pod_horarios_fecha_t enable' accion
      from   dual;

   procedure ejecutar (p_accion in varchar2) is
   begin
      execute immediate p_accion;
   end;

   procedure desactivar_triggers is
   begin
      for x in lista_inicial loop
         ejecutar (x.accion);
      end loop;
   end;

   procedure activar_triggers is
   begin
      for x in lista_final loop
         ejecutar (x.accion);
      end loop;
   end;
begin
   desactivar_triggers;

   for a in c1 loop
      dbms_output.put_line (a.asignatura_id || ' ' || a.grupo_id || ' ' || a.tipo_subgrupo_id || ' ' || a.subgrupo_id
                            || ' ' || a.semestre_id || ' ' || a.dia_Semana_id || ' ' || a.hora_inicio);
      v_inicio := null;

      for s in c2 (a.asignatura_id, a.grupo_id, a.tipo_subgrupo_id, a.subgrupo_id, a.semestre_id, a.dia_Semana_id,
                   a.hora_inicio) loop
         if v_acciones = 'S' then
            dbms_output.put_line ('----- borrado 1');

            delete      pod_horarios_fechas
            where       hor_sgr_grp_asi_id = a.asignatura_id
            and         hor_sgr_grp_id = a.grupo_id
            and         hor_sgr_grp_curso_aca = v_curso_aca
            and         hor_sgr_id = a.subgrupo_id
            and         hor_sgr_tipo = a.tipo_subgrupo_id
            and         hor_semestre = a.semestre_id
            and         hor_dia_sem = a.dia_semana_id
            and         hor_ini = to_date ('01012000' || nvl (v_inicio, s.hora_inicio), 'ddmmyyyyhh24:mi');

            dbms_output.put_line ('----- borrado 2');

            delete      pod_horarios
            where       sgr_grp_asi_id = a.asignatura_id
            and         sgr_grp_id = a.grupo_id
            and         sgr_grp_curso_aca = v_curso_aca
            and         sgr_id = a.subgrupo_id
            and         sgr_tipo = a.tipo_subgrupo_id
            and         semestre = a.semestre_id
            and         dia_sem = a.dia_semana_id
            and         ini = to_date ('01012000' || nvl (v_inicio, s.hora_inicio), 'ddmmyyyyhh24:mi');
         end if;

         dbms_output.put_line ('----- ' || nvl (v_inicio, s.hora_inicio) || ' ' || s.hora_fin);

         if v_acciones = 'S' then
            insert into pod_horarios
                        (sgr_grp_asi_id, sgr_grp_id, semestre, sgr_grp_curso_aca, sgr_id, sgr_tipo,
                         dia_sem,
                         ini, fin,
                         ubi_id, grc_curso_aca, grc_cur_tit_id, grc_cur_id, grc_grupo, compartido
                        )
            values      (a.asignatura_id, a.grupo_id, a.semestre_id, v_curso_aca, a.subgrupo_id, a.tipo_subgrupo_id,
                         a.dia_Semana_id,
                         to_date ('1-1-2000 ' || nvl (v_inicio, s.hora_inicio), 'dd-mm-yyyy hh24:mi'), s.fin_completo,
                         s.aula_planificacion_id, v_curso_aca, a.estudio_id, a.curso_id, a.grupo_id, a.comun
                        );
         end if;

         for f in c3 (a.asignatura_id, a.grupo_id, a.tipo_subgrupo_id, a.subgrupo_id, a.semestre_id, a.dia_Semana_id,
                      a.hora_inicio, s.hora_fin) loop
            dbms_output.put_line ('----- ----- ' || nvl (v_inicio, s.hora_inicio) || ' ' || s.hora_fin || ' - '
                                  || to_char (f.inicio, 'dd/mm/yyyy'));

            if v_acciones_det = 'S' then
               select count (*)
               into   v_existe
               from   pod_horarios_fechas
               where  hor_sgr_grp_asi_id = a.asignatura_id
               and    hor_sgr_grp_id = a.grupo_id
               and    hor_sgr_grp_curso_aca = v_curso_aca
               and    hor_sgr_id = a.subgrupo_id
               and    hor_sgr_tipo = a.tipo_subgrupo_id
               and    hor_semestre = a.semestre_id
               and    hor_dia_sem = a.dia_semana_id
               and    hor_ini = to_date ('1-1-2000 ' || nvl (v_inicio, s.hora_inicio), 'dd-mm-yyyy hh24:mi')
               and    fecha = trunc (f.inicio)
               and    ubi_id = f.aula_planificacion_id;

               if v_existe = 0 then
                  insert into pod_horarios_fechas
                              (hor_sgr_grp_asi_id, hor_sgr_grp_id, hor_sgr_grp_curso_aca, hor_sgr_id, hor_sgr_tipo,
                               hor_semestre, hor_dia_sem,
                               hor_ini,
                               fecha, ubi_id
                              )
                  values      (a.asignatura_id, a.grupo_id, v_curso_aca, a.subgrupo_id, a.tipo_subgrupo_id,
                               a.semestre_id, a.dia_Semana_id,
                               to_date ('1-1-2000 ' || nvl (v_inicio, s.hora_inicio), 'dd-mm-yyyy hh24:mi'),
                               trunc (f.inicio), f.aula_planificacion_id
                              );
               else
                  dbms_output.put_line ('----- ----- ----- fecha duplicada');
               end if;
            end if;
         end loop;

         v_inicio := s.hora_fin;
      end loop;
   end loop;

   activar_triggers;
end;










select *
from pod_horarios_fechas
      where       hor_sgr_grp_asi_id = :asignatura_id
      and         hor_sgr_grp_id = :grupo_id
      and         hor_sgr_grp_curso_aca = :v_curso_aca
      and         hor_sgr_id = :subgrupo_id
      and         hor_sgr_tipo = :tipo_subgrupo_id
      and         hor_semestre = :semestre_id
      and         hor_dia_sem = :dia_semana_id
      --and         hor_ini = to_date ('01012000' || :hora_inicio, 'ddmmyyyyhh24mi');

      
      select *
      from pod_horarios
      where       sgr_grp_asi_id = :asignatura_id
      and         sgr_grp_id = :grupo_id
      and         sgr_grp_curso_aca = :v_curso_aca
      and         sgr_id = :subgrupo_id
      and         sgr_tipo = :tipo_subgrupo_id
      and         semestre = :semestre_id
      and         dia_sem = :dia_semana_id
      
      --and         ini = to_date ('01012000' || :hora_inicio, 'ddmmyyyyhh24mi');
      
      
      
       select   asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo_id, semestre_id, dia_Semana_id,
               to_char (hora_inicio, 'hh24:mi') hora_inicio, to_char (hora_fin, 'hh24:mi') hora_fin,
               hora_fin fin_completo
      from     uji_horarios.hor_items i,
               uji_horarios.hor_items_asignaturas a
      where    i.id = a.item_id
      and      asignatura_id = :p_asi
      and      grupo_id = :p_grp
      and      tipo_subgrupo_id = :p_tipo
      and      subgrupo_id = :p_id
      and      semestre_id = :p_sem
      and      dia_semana_id = :p_dia
      and      to_char (hora_inicio, 'hh24:mi') = :p_ini
      and      dia_semana_id is not null
      group by asignatura_id,
               grupo_id,
               tipo_subgrupo_id,
               subgrupo_id,
               semestre_id,
               dia_Semana_id,
               to_char (hora_inicio, 'hh24:mi'),
               to_char (hora_fin, 'hh24:mi'),
               hora_fin
      order by 8;
      
      
delete      pod_horarios
         where       sgr_grp_asi_id = :asignatura_id
         and         sgr_grp_id = :grupo_id
         and         sgr_grp_curso_aca = :v_curso_aca
         and         sgr_id = :subgrupo_id
         and         sgr_tipo = :tipo_subgrupo_id
         and         semestre = :semestre_id
         and         dia_sem = :dia_semana_id
         and         ini = to_date ('01012000' || :hora_inicio, 'ddmmyyyyhh24:mi');      
		 
		 
		 
		 
		 