ALTER TABLE UJI_HORARIOS.HOR_AGRUPACIONES_ASIGNATURAS
MODIFY(ASIGNATURA_ID  NULL);

ALTER TABLE UJI_HORARIOS.HOR_AGRUPACIONES_ASIGNATURAS
 ADD (tipo_subgrupo_id  VARCHAR2(10));

ALTER TABLE UJI_HORARIOS.HOR_AGRUPACIONES_ASIGNATURAS
 ADD (subgrupo_id  NUMBER);

ALTER TABLE UJI_HORARIOS.HOR_AGRUPACIONES_ASIGNATURAS
  DROP CONSTRAINT HOR_AGR_ASI_UN;


