CREATE OR REPLACE TRIGGER UJI_HORARIOS.mutante_3_final_2014
   after insert or update of dia_semana_id,
                             desde_el_dia,
                             hasta_el_dia,
                             repetir_cada_semanas,
                             numero_iteraciones,
                             hora_inicio,
                             hora_fin
   ON UJI_HORARIOS.HOR_ITEMS
begin
   declare
      v_aux           number;
      v_num_items     number;
      v_id            number;
      v_hora_ini      date;
      v_hora_fin      date;
      v_ult_orden     number;
      v_dia_item      number;
      v_dia_Detalle   number;
      v_semestre      number;
      v_dia_primero   date;

      cursor reg (v_rowid rowid) is
         select *
           from uji_horarios.hor_items
          where rowid = v_rowid;

      cursor lista_detalle (
         p_id                     in   number,
         p_detalle_manual         in   number,
         p_dia_semana_id          in   number,
         p_semestre_id            in   number,
         p_numero_iteraciones     in   number,
         p_repetir_cada_Semanas   in   number,
         p_hasta_el_dia           in   date,
         p_desde_el_dia           in   date,
         p_subgrupo_id            in   number,
         p_tipo_subgrupo_id       in   varchar2,
         p_grupo_id               in   varchar2
      ) is
         select   todo.*, row_number () over (partition by id order by fecha) orden_final
             from (select distinct id, fecha, docencia_paso_1, docencia_paso_2, docencia, orden_id, numero_iteraciones,
                                   repetir_cada_semanas, fecha_inicio, fecha_fin, semestre_id, grupo_id,
                                   tipo_subgrupo_id, subgrupo_id, dia_semana_id, tipo_dia, festivos
                              from (select i.id, fecha, docencia docencia_paso_1,
                                           decode (nvl (d.repetir_cada_semanas, 1),
                                                   1, docencia,
                                                   decode (mod (orden_id, d.repetir_cada_semanas), 1, docencia, 'N')
                                                  ) docencia_paso_2,
                                           decode
                                                 (tipo_dia,
                                                  'F', 'N',
                                                  decode (d.numero_iteraciones,
                                                          null, decode (nvl (d.repetir_cada_semanas, 1),
                                                                        1, docencia,
                                                                        decode (mod (orden_id, d.repetir_cada_semanas),
                                                                                1, docencia,
                                                                                'N'
                                                                               )
                                                                       ),
                                                          decode (sign (((orden_id - festivos) / d.repetir_cada_semanas
                                                                        )
                                                                        - (d.numero_iteraciones)),
                                                                  1, 'N',
                                                                  decode (nvl (d.repetir_cada_semanas, 1),
                                                                          1, docencia,
                                                                          decode (mod (orden_id, d.repetir_cada_semanas),
                                                                                  1, docencia,
                                                                                  'N'
                                                                                 )
                                                                         )
                                                                 )
                                                         )
                                                 ) docencia,
                                           d.orden_id, d.numero_iteraciones, d.repetir_cada_semanas, d.fecha_inicio,
                                           d.fecha_fin, d.estudio_id, d.semestre_id, d.asignatura_id, d.grupo_id,
                                           d.tipo_subgrupo_id, d.subgrupo_id, d.dia_semana_id, tipo_dia, festivos
                                      from (select id, fecha,
                                                   hor_contar_festivos (decode (x.desde_el_dia,
                                                                                null, fecha_inicio,
                                                                                x.desde_el_dia
                                                                               ),
                                                                        x.fecha, x.dia_semana_id,
                                                                        x.repetir_cada_semanas) festivos,
                                                   row_number () over (partition by decode
                                                                                      (decode
                                                                                             (sign (x.fecha
                                                                                                    - fecha_inicio),
                                                                                              -1, 'N',
                                                                                              decode (sign (fecha_fin
                                                                                                            - x.fecha),
                                                                                                      -1, 'N',
                                                                                                      'S'
                                                                                                     )
                                                                                             ),
                                                                                       'S', decode
                                                                                          (decode
                                                                                              (sign
                                                                                                  (x.fecha
                                                                                                   - decode
                                                                                                        (x.desde_el_dia,
                                                                                                         null, fecha_inicio,
                                                                                                         x.desde_el_dia
                                                                                                        )),
                                                                                               -1, 'N',
                                                                                               decode
                                                                                                  (sign
                                                                                                      (decode
                                                                                                          (x.hasta_el_dia,
                                                                                                           null, fecha_fin,
                                                                                                           x.hasta_el_dia
                                                                                                          )
                                                                                                       - x.fecha),
                                                                                                   -1, 'N',
                                                                                                   'S'
                                                                                                  )
                                                                                              ),
                                                                                           'S', 'S',
                                                                                           'N'
                                                                                          ),
                                                                                       'N'
                                                                                      ), id, estudio_id, semestre_id, asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo_id, dia_semana_id order by fecha)
                                                                                                               orden_id,
                                                   decode (hor_f_fecha_entre (x.fecha, fecha_inicio, fecha_fin),
                                                           'S', decode (hor_f_fecha_entre (x.fecha,
                                                                                           decode (desde_el_dia,
                                                                                                   null, fecha_inicio,
                                                                                                   desde_el_dia
                                                                                                  ),
                                                                                           decode (hasta_el_dia,
                                                                                                   null, fecha_fin,
                                                                                                   hasta_el_dia
                                                                                                  )),
                                                                        'S', 'S',
                                                                        'N'
                                                                       ),
                                                           'N'
                                                          ) docencia,
                                                   estudio_id, semestre_id, grupo_id, tipo_subgrupo_id, subgrupo_id,
                                                   dia_semana_id, asignatura_id, fecha_inicio, fecha_fin,
                                                   fecha_examenes_inicio, fecha_examenes_fin, desde_el_dia,
                                                   hasta_el_dia, repetir_cada_semanas, numero_iteraciones,
                                                   detalle_manual, tipo_dia, dia_semana
                                              from (select distinct i.id, null estudio_id, i.semestre_id, i.grupo_id,
                                                                    i.tipo_subgrupo_id, i.subgrupo_id, i.dia_semana_id,
                                                                    null asignatura_id, fecha_inicio, fecha_fin,
                                                                    fecha_examenes_inicio, fecha_examenes_fin,
                                                                    decode (sign (i.desde_el_dia - fecha_inicio),
                                                                            -1, fecha_inicio,
                                                                            i.desde_el_dia
                                                                           ) desde_el_dia,
                                                                    hasta_el_dia, repetir_cada_semanas,
                                                                    numero_iteraciones, detalle_manual, c.fecha,
                                                                    tipo_dia, dia_semana
                                                               from hor_v_fechas_estudios s,
                                                                    (select p_id id, p_detalle_manual detalle_manual,
                                                                            p_dia_semana_id dia_semana_id,
                                                                            p_semestre_id semestre_id,
                                                                            p_numero_iteraciones numero_iteraciones,
                                                                            p_repetir_cada_semanas repetir_cada_semanas,
                                                                            p_hasta_el_dia hasta_el_dia,
                                                                            p_desde_el_dia desde_el_dia,
                                                                            p_subgrupo_id subgrupo_id,
                                                                            p_tipo_subgrupo_id tipo_subgrupo_id,
                                                                            p_grupo_id grupo_id
                                                                       from dual) i,
                                                                    hor_ext_calendario c
                                                              where i.semestre_id = s.semestre_id
                                                                and trunc (c.fecha) between fecha_inicio
                                                                                        and decode (fecha_examenes_fin,
                                                                                                    null, fecha_fin,
                                                                                                    fecha_examenes_fin
                                                                                                   )
                                                                and c.dia_semana_id = i.dia_semana_id
                                                                and tipo_dia in ('L', 'E', 'F')
                                                                and vacaciones = 0
                                                                and s.tipo_estudio_id = 'G'
                                                                and detalle_manual = 0
                                                                and s.estudio_id in (
                                                                          select estudio_id
                                                                            from hor_items_asignaturas
                                                                           where item_id = i.id
                                                                             and curso_id = s.curso_id)
                                                                                                       --and i.id = 559274
                                                   ) x) d,
                                           (select p_id id, p_detalle_manual detalle_manual,
                                                   p_dia_semana_id dia_semana_id, p_semestre_id semestre_id,
                                                   p_numero_iteraciones numero_iteraciones,
                                                   p_repetir_cada_semanas repetir_cada_semanas,
                                                   p_hasta_el_dia hasta_el_dia, p_desde_el_dia desde_el_dia,
                                                   p_subgrupo_id subgrupo_id, p_tipo_subgrupo_id tipo_subgrupo_id,
                                                   p_grupo_id grupo_id
                                              from dual) i
                                     where i.semestre_id = d.semestre_id
                                       and i.grupo_id = d.grupo_id
                                       and i.tipo_subgrupo_id = d.tipo_subgrupo_id
                                       and i.subgrupo_id = d.subgrupo_id
                                       and i.dia_semana_id = d.dia_semana_id
                                       and i.detalle_manual = 0
                                       and i.id = d.id
                                    union all
                                    select distinct c.id, c.fecha, 'N' docencia_paso_1, 'N' docencia_paso_2,
                                                    decode (d.id, null, 'N', 'S') docencia,
                                                    row_number () over (partition by d.item_id order by d.inicio)
                                                                                                               orden_id,
                                                    numero_iteraciones, repetir_cada_semanas, fecha_inicio, fecha_fin,
                                                    estudio_id, semestre_id, asignatura_id, grupo_id, tipo_subgrupo_id,
                                                    subgrupo_id, dia_semana_id, tipo_dia,
                                                    decode (tipo_dia, 'F', 1, 0) festivos
                                               from (select distinct i.id, c.fecha, numero_iteraciones,
                                                                     repetir_cada_semanas, s.fecha_inicio, s.fecha_fin,
                                                                     null estudio_id, i.semestre_id, null asignatura_id,
                                                                     i.grupo_id, i.tipo_subgrupo_id, i.subgrupo_id,
                                                                     i.dia_semana_id, tipo_dia
                                                                from hor_v_fechas_estudios s,
                                                                     (select p_id id, p_detalle_manual detalle_manual,
                                                                             p_dia_semana_id dia_semana_id,
                                                                             p_semestre_id semestre_id,
                                                                             p_numero_iteraciones numero_iteraciones,
                                                                             p_repetir_cada_semanas
                                                                                                   repetir_cada_semanas,
                                                                             p_hasta_el_dia hasta_el_dia,
                                                                             p_desde_el_dia desde_el_dia,
                                                                             p_subgrupo_id subgrupo_id,
                                                                             p_tipo_subgrupo_id tipo_subgrupo_id,
                                                                             p_grupo_id grupo_id
                                                                        from dual) i,
                                                                     hor_ext_calendario c
                                                               where i.semestre_id = s.semestre_id
                                                                 and trunc (c.fecha) between fecha_inicio
                                                                                         and decode (fecha_examenes_fin,
                                                                                                     null, fecha_fin,
                                                                                                     fecha_examenes_fin
                                                                                                    )
                                                                 and c.dia_semana_id = i.dia_semana_id
                                                                 and tipo_dia in ('L', 'E', 'F')
                                                                 and s.tipo_estudio_id = 'G'
                                                                 and vacaciones = 0
                                                                 and detalle_manual = 1
                                                                 and s.estudio_id in (
                                                                          select estudio_id
                                                                            from hor_items_asignaturas
                                                                           where item_id = i.id
                                                                             and curso_id = s.curso_id)) c,
                                                    hor_items_detalle d
                                              where c.id = d.item_id(+)
                                                and trunc (c.fecha) = trunc (d.inicio(+)))
                             where id = p_id
                               and docencia = 'S') todo
         order by 18;

      function buscar_item (p_item_id in number, p_orden in number)
         return number is
         v_aux   number;

         cursor lista_detalle_ordenado is
            select id, row_number () over (partition by item_id order by trunc (inicio)) orden
              from hor_items_detalle
             where item_id = p_item_id;
      begin
         v_aux := -1;

         for det in lista_detalle_ordenado loop
            if det.orden = p_orden then
               v_aux := det.id;
            end if;
         end loop;

         return v_aux;
      exception
         when no_data_found then
            return (-1);
         when others then
            return (0);
      end;
   begin
      for i in 1 .. mutante_items.v_num loop
         for v_reg in reg (mutante_items.v_var_tabla (i)) loop
            if v_reg.detalle_manual = 0 then
               v_hora_ini := v_Reg.hora_inicio;
               v_hora_fin := v_Reg.hora_fin;

               for x in lista_detalle (v_reg.id, v_reg.detalle_manual, v_reg.dia_semana_id, v_reg.semestre_id,
                                       v_reg.numero_iteraciones, v_reg.repetir_cada_Semanas, v_reg.hasta_el_dia,
                                       v_Reg.desde_el_dia, v_reg.subgrupo_id, v_reg.tipo_subgrupo_id, v_reg.grupo_id) loop
                  v_id := buscar_item (v_Reg.id, x.orden_final);
                  dbms_output.put_line (v_reg.id || ', ' || v_reg.detalle_manual || ', ' || v_reg.dia_semana_id || ', '
                                        || v_reg.semestre_id || ', ' || v_reg.numero_iteraciones || ', '
                                        || v_reg.repetir_cada_Semanas || ', ' || v_reg.hasta_el_dia || ', '
                                        || v_Reg.desde_el_dia || ', ' || v_reg.subgrupo_id || ', '
                                        || v_reg.tipo_subgrupo_id || ', ' || v_reg.grupo_id);
                  dbms_output.put_line (v_Reg.id || ' ' || to_char (x.fecha, 'dd/mm/yyyyy') || ' ' || x.orden_final);

                  if v_id > 0 then
                     /* por defecto actualiza fechas */
                     dbms_output.put_line ('update ' || x.orden_final || ' - ' || to_char (x.fecha, 'dd/mm/yyyy')
                                           || ' - ' || to_char (v_Reg.desde_el_dia, 'dd/mm/yyyy'));

                     update hor_items_detalle
                     set inicio =
                            to_date (to_char (x.fecha, 'dd/mm/yyyy') || ' ' || to_char (v_hora_ini, 'hh24:mi:ss'),
                                     'dd/mm/yyyy hh24:mi:ss'),
                         fin =
                            to_date (to_char (x.fecha, 'dd/mm/yyyy') || ' ' || to_char (v_hora_fin, 'hh24:mi:ss'),
                                     'dd/mm/yyyy hh24:mi:ss')
                     where  id = v_id;
                  elsif v_id = -1 then
                     dbms_output.put_line ('insertar ' || x.orden_final);

                     begin
                        /* solo inserta si no le quedan fechas por actualizar */
                        v_aux := uji_horarios.hibernate_sequence.nextval;

                        insert into hor_items_detalle
                                    (id, item_id,
                                     inicio,
                                     fin
                                    )
                        values      (v_aux, v_Reg.id,
                                     to_date (to_char (x.fecha, 'dd/mm/yyyy') || ' '
                                              || to_char (v_hora_ini, 'hh24:mi:ss'),
                                              'dd/mm/yyyy hh24:mi:ss'),
                                     to_date (to_char (x.fecha, 'dd/mm/yyyy') || ' '
                                              || to_char (v_hora_fin, 'hh24:mi:ss'),
                                              'dd/mm/yyyy hh24:mi:ss')
                                    );
                     exception
                        when others then
                           null;
                     end;
                  end if;

                  if x.orden_id > nvl (v_ult_orden, 0) then
                     v_ult_orden := x.orden_final;
                  end if;
               end loop;

               /* y solo borra si sobran al final */
               delete      hor_items_detalle
               where       id in (
                              select id
                                from (select id, inicio, fin,
                                             row_number () over (partition by item_id order by trunc (inicio)) orden
                                        from hor_items_detalle
                                       where item_id = v_reg.id)
                               where orden > v_ult_orden);
            else
               /* detalle manual, si ha cambiado de dia de semana u hora, aplicar este cambio a las sesiones detalladas */
               dbms_output.put_line (1);

               begin
                  select distinct to_number (to_char (inicio, 'd'))
                             into v_dia_detalle
                             from hor_items_detalle
                            where item_id = v_reg.id;

                  v_dia_item := to_number (to_char (v_Reg.hora_inicio, 'd'));

                  if v_dia_item <> v_dia_detalle then
                     update hor_items_detalle
                     set inicio = inicio + (v_dia_item - v_dia_detalle),
                         fin = fin + (v_dia_item - v_dia_detalle)
                     where  item_id = v_reg.id;

                     delete      uji_horarios.hor_items_detalle
                     where       id in (select d.id
                                          from uji_horarios.hor_items_detalle d,
                                               uji_horarios.hor_ext_calendario c
                                         where item_id = v_Reg.id
                                           and trunc (inicio) = fecha
                                           and tipo_dia in ('F', 'N'));
                  end if;
               exception
                  when no_data_found then
                     null;
               end;

               dbms_output.put_line (2);

               /* detalle manual, tambien hay que actualizar la hora inicio y hora final */
               begin
                  select inicio, fin
                    into v_hora_ini, v_hora_fin
                    from hor_items_detalle
                   where item_id = v_Reg.id
                     and rownum <= 1;

                  if    to_char (v_hora_ini, 'hh24:mi:ss') <> to_char (v_reg.hora_inicio, 'hh24:mi:ss')
                     or to_char (v_hora_fin, 'hh24:mi:ss') <> to_char (v_reg.hora_fin, 'hh24:mi:ss') then
                     update hor_items_detalle
                     set inicio =
                            to_date (to_char (inicio, 'dd/mm/yyyy') || ' ' || to_char (v_reg.hora_inicio, 'hh24:mi:ss'),
                                     'dd/mm/yyyy hh24:mi:ss'),
                         fin =
                            to_date (to_char (fin, 'dd/mm/yyyy') || ' ' || to_char (v_reg.hora_fin, 'hh24:mi:ss'),
                                     'dd/mm/yyyy hh24:mi:ss')
                     where  item_id = v_reg.id;
                  end if;
               exception
                  when no_data_found then
                     null;
               end;
            end if;
         end loop;
      end loop;
   end;
end mutante_3_final_2014;


/* Formatted on 27/04/2015 15:28 (Formatter Plus v4.8.8) */
CREATE OR REPLACE PROCEDURE UJI_HORARIOS.pod_validacion_pdf (
   name_array    IN   euji_util.ident_arr2,
   value_array   IN   euji_util.ident_arr2
) IS
   v_aux           NUMBER;
   pDepartamento   number          := euji_util.euji_getparam (name_array, value_array, 'pDepartamento');
   pSesion         VARCHAR2 (400)  := euji_util.euji_getparam (name_array, value_array, 'pSesion');
   v_nombre        varchar2 (2000);
   vSesion         number;
   vCurso          number;
   pProfesor       number;

   cursor lista_areas (p_departamento in number) is
      select   *
          from hor_areas
         where departamento_id = p_Departamento
      order by nombre;

   cursor lista_profesores (p_departamento in number, p_area in number) is
      select   p.id, p.nombre, creditos, pendiente_contratacion, creditos_maximos, creditos_reduccion, categoria_nombre,
               a.nombre nombre_area
          from hor_profesores p,
               hor_areas a
         where p.departamento_id = p_Departamento
           and area_id = p_area
           and area_id = a.id
      order by nombre;

   procedure cabecera is
   begin
      begin
         select nombre
           into v_nombre
           from hor_Departamentos
          where id = pDepartamento;
      exception
         when no_data_found then
            v_nombre := 'Desconegut';
         when others then
            v_nombre := sqlerrm;
      end;

      owa_util.mime_header (ccontent_type => 'text/xml', bclose_header => true);
      fop.documentOpen (margin_left => 1.5, margin_right => 1.5, margin_top => 0.5, margin_bottom => 0.5,
                        extent_before => 3, extent_after => 1.5, extent_margin_top => 3, extent_margin_bottom => 1.5);
      fop.pageBodyOpen;
      htp.p (fof.documentheaderopen || fof.blockopen (space_after => 0.1) || fof.tableOpen
             || fof.tablecolumndefinition (column_width => 3) || fof.tablecolumndefinition (column_width => 15)
             || fof.tablebodyopen || fof.tableRowOpen || fof.tablecellopen
             || fof.block (fof.logo_uji, text_align => 'center') || fof.tablecellclose
             || fof.tablecellopen (display_align => 'center', padding => 0.4)
             || fof.block (text => v_nombre, font_size => 16, font_weight => 'bold', font_family => 'Arial',
                           text_align => 'center')
             || fof.block (text => 'Validació POD - Curs: ' || vCurso || '/' || substr (to_char (vCurso + 1), 3),
                           font_size => 16, font_weight => 'bold', font_family => 'Arial', text_align => 'center')
             || fof.tablecellclose || fof.tableRowClose || fof.tablebodyclose || fof.tableClose || fof.blockclose
             || fof.documentheaderclose);
      -- El numero paginas se escribe a mano para poner el texto m?s peque?o
      htp.p (fof.documentfooteropen || fof.tableOpen || fof.tablecolumndefinition (column_width => 18)
             || fof.tablebodyopen || fof.tableRowOpen || fof.tablecellopen
             || fof.block (text => 'Pag. <fo:page-number /> de <fo:page-number-citation ref-id="lastBlock" />',
                           text_align => 'right', font_weight => 'bold', font_size => 8)
             || fof.tablecellclose || fof.tableRowClose || fof.tablebodyclose || fof.tableClose
             || fof.documentfooterclose);
      fop.pageFlowOpen;
      fop.block (fof.white_space);
   end;

   procedure pie is
   begin
      htp.p ('<fo:block  id="lastBlock" space-before="1cm" font-size="8pt">');
      htp.p ('Data de generació: ' || to_char (sysdate, 'dd/mm/yyy hh24:mi'));
      fop.blockclose ();
      fop.pageFlowClose;
      fop.pageBodyClose;
      fop.documentClose;
   end;

   function italic (p_entrada in varchar2)
      return varchar2 is
      v_rdo   varchar2 (2000);
   begin
      v_rdo := '<fo:inline font-style="italic">' || p_entrada || '</fo:inline>';
      return (v_rdo);
   end;

   function espacios (p_espacios in number)
      return varchar2 is
      v_rdo   varchar2 (2000);
   begin
      v_rdo := null;

      for x in 1 .. p_espacios loop
         v_rdo := v_rdo || fof.white_space;
      end loop;

      return v_rdo;
   end;

   function formatear (p_entrada in number)
      return varchar2 is
      v_rdo   varchar2 (100);
   begin
      v_rdo := replace (to_char (p_entrada, '9990.00'), '.', ',');
      return (v_rdo);
   end;

   procedure control_1 (p_dep in number, p_area in number) is
      vCreditos    number;
      v_aux        number;
      v_mostrada   number;
      v_nombre     varchar2 (2000);

      cursor lista_control_1 (p_dep in number, p_area in number) is
         select   id, nombre, creditos_maximos, creditos_Reduccion, creditos, creditos_asignados,
                  creditos - creditos_asignados creditos_pendientes, categoria_nombre
             from (select p.id, p.nombre, nvl (creditos_maximos, 0) creditos_maximos,
                          nvl (creditos_reduccion, 0) creditos_reduccion, nvl (creditos, 0) creditos,
                          (select nvl (sum (nvl (ip.creditos, i.creditos)), 0) creditos
                             from hor_items_profesores ip,
                                  hor_items i
                            where profesor_id = p.id
                              and ip.item_id = i.id) creditos_asignados, categoria_nombre
                     from hor_profesores p
                    where p.departamento_id = p_dep
                      and area_id = p_area)
         order by nombre;
   begin
      select nombre
        into v_nombre
        from hor_areas
       where id = p_area;

      v_aux := 0;
      v_mostrada := 0;

      for x in lista_control_1 (p_dep, p_area) loop
         begin
            select nvl (sum (creditos), 0)
              into vCreditos
              from (select   grupo_id, tipo_subgrupo, tipo_subgrupo_id, wm_concat (asignatura_id) asignatura_id,
                             creditos
                        from (select   items.id, items.grupo_id, items.tipo_subgrupo, items.tipo_subgrupo_id,
                                       items.subgrupo_id, items.dia_semana_id, items.hora_inicio, items.hora_fin,
                                       rtrim
                                          (xmlagg (xmlelement (e, asi.asignatura_id || ', ')).extract
                                                                                           ('//text()'),
                                           ', ') as asignatura_id,
                                       to_date (dia_semana_id + 1 || '/01/2006 ' || to_char (hora_fin, 'hh24:mi'),
                                                'dd/mm/yyyy hh24:mi') fin,
                                       to_date (dia_semana_id + 1 || '/01/2006 '
                                                || to_char (hora_inicio, 'hh24:mi'),
                                                'dd/mm/yyyy hh24:mi') inicio,
                                       creditos
                                  from uji_horarios.hor_items items,
                                       uji_horarios.hor_items_asignaturas asi
                                 where items.id = asi.item_id
                                   and items.id in (select item_id
                                                      from hor_items_profesores
                                                     where profesor_id = x.id)
                              group by items.id,
                                       items.grupo_id,
                                       items.tipo_subgrupo,
                                       items.tipo_subgrupo_id,
                                       items.subgrupo_id,
                                       items.dia_semana_id,
                                       items.semestre_id,
                                       items.hora_inicio,
                                       items.hora_fin,
                                       asi.asignatura_id,
                                       creditos)
                    group by grupo_id,
                             tipo_subgrupo,
                             tipo_subgrupo_id,
                             creditos);

            select creditos_asignados
              into vCreditos
              from hor_v_profesor_creditos
             where id = x.id;

            if (x.creditos - vCreditos) < 0 then
               v_aux := v_aux + 1;
            end if;
         exception
            when others then
               vCreditos := 0;
         end;

         if     v_aux = 1
            and v_mostrada = 0 then
            fop.block (espacios (2) || fof.bold (v_nombre), font_size => 12, space_before => 0.1);
            v_mostrada := 1;
         end if;

         if (x.creditos - vCreditos) < 0 then
            fop.block (espacios (5) || x.categoria_nombre || ' - ' || x.nombre || ': '
                       || fof.bold (formatear (x.creditos - vCreditos)),
                       font_size => 10);
         end if;
      end loop;
   end;

   procedure control_2 (p_dep in number, p_area in number) is
      v_aux        number;
      v_nombre     varchar2 (1000);
      v_sesiones   number;

      cursor lista_solapamientos (p_prof in number, p_semestre in number) is
         select   id, min (asignatura_id) asignatura_id, grupo_id, tipo_subgrupo_id || subgrupo_id subgrupo_id,
                  semestre_id, dia_semana_id,
                  decode (dia_semana_id,
                          1, 'Dilluns',
                          2, 'Dimarts',
                          3, 'Dimecres',
                          4, 'Dijous',
                          5, 'Divendres',
                          6, 'Dissabte',
                          7, 'Diumenge',
                          'Error'
                         ) dia_semana,
                  min (hora_inicio) inicio, max (hora_fin) fin
             from (select   id, grupo_id, subgrupo_id, tipo_subgrupo, tipo_subgrupo_id, dia_semana_id, hora_inicio,
                            hora_fin, wm_concat (asignatura_id) asignatura_id, semestre_id,
                            count (*) over (partition by semestre_id, dia_semana_id) cuantos
                       from (select id, asignatura_id, semestre_id, grupo_id, tipo_subgrupo, tipo_subgrupo_id,
                                    subgrupo_id, dia_semana_id, hora_inicio, hora_fin
                               from (select   items.id, items.grupo_id, items.tipo_subgrupo, items.tipo_subgrupo_id,
                                              items.subgrupo_id, items.dia_semana_id, items.hora_inicio, items.hora_fin,
                                              items.semestre_id, wm_concat (asi.asignatura_id) asignatura_id
                                         from uji_horarios.hor_items items,
                                              uji_horarios.hor_items_asignaturas asi
                                        where items.id = asi.item_id
                                          and items.semestre_id = p_semestre
                                          and items.dia_semana_id is not null
                                          and items.id in (select item_id
                                                             from hor_items_profesores
                                                            where profesor_id = p_prof)
                                     group by asi.asignatura_id,
                                              items.id,
                                              items.grupo_id,
                                              items.tipo_subgrupo,
                                              items.tipo_subgrupo_id,
                                              items.subgrupo_id,
                                              items.dia_semana_id,
                                              items.semestre_id,
                                              items.hora_inicio,
                                              items.hora_fin) x
                              where exists (
                                       select 1
                                         from hor_items i,
                                              hor_items_profesores p
                                        where i.id = p.item_id
                                          and profesor_id = p_prof
                                          and i.id <> x.id
                                          and i.dia_semana_id = x.dia_Semana_id
                                          and i.semestre_id = x.semestre_id
                                          and (   to_number (to_char ((i.hora_inicio + 1 / 1440), 'hh24mi'))
                                                     between to_number (to_char (x.hora_inicio, 'hh24mi'))
                                                         and to_number (to_char (x.hora_fin, 'hh24mi'))
                                               or to_number (to_char ((i.hora_fin - 1 / 1440), 'hh24mi'))
                                                     between to_number (to_char (x.hora_inicio, 'hh24mi'))
                                                         and to_number (to_char (x.hora_fin, 'hh24mi'))
                                              )))
                   group by id,
                            grupo_id,
                            subgrupo_id,
                            tipo_subgrupo,
                            tipo_subgrupo_id,
                            dia_semana_id,
                            semestre_id,
                            hora_inicio,
                            hora_fin)
            where cuantos > 1
         group by id,
                  semestre_id,
                  dia_Semana_id,
                  grupo_id,
                  tipo_subgrupo_id || subgrupo_id
         order by 5,
                  6,
                  2,
                  3,
                  4,
                  6;

      cursor lista_solapes_Semana_Detalle (p_prof in number, p_semestre in number, p_item in number) is
         select distinct inicio, fin
                    from hor_items i,
                         hor_items_detalle d,
                         hor_items_profesores p
                   where i.id = d.item_id
                     and i.id = p.item_id
                     and profesor_id = p_prof
                     and (d.inicio, d.fin) in (
                            select   inicio, fin
                                from (select d.inicio, fin
                                        from hor_items i,
                                             hor_items_profesores p,
                                             hor_items_detalle d,
                                             hor_items_det_profesores dp
                                       where i.id = p.item_id
                                         and profesor_id = p_prof
                                         and i.id in (
                                                select   id
                                                    from (select   id, grupo_id, subgrupo_id, tipo_subgrupo,
                                                                   tipo_subgrupo_id, dia_semana_id, hora_inicio,
                                                                   hora_fin, wm_concat (asignatura_id) asignatura_id,
                                                                   semestre_id,
                                                                   count (*) over (partition by semestre_id, dia_semana_id)
                                                                                                                cuantos,
                                                                   detalle_manual
                                                              from (select id, asignatura_id, semestre_id, grupo_id,
                                                                           tipo_subgrupo, tipo_subgrupo_id, subgrupo_id,
                                                                           dia_semana_id, hora_inicio, hora_fin,
                                                                           detalle_manual
                                                                      from (select   items.id, items.grupo_id,
                                                                                     items.tipo_subgrupo,
                                                                                     items.tipo_subgrupo_id,
                                                                                     items.subgrupo_id,
                                                                                     items.dia_semana_id,
                                                                                     items.hora_inicio, items.hora_fin,
                                                                                     items.semestre_id,
                                                                                     wm_concat
                                                                                        (asi.asignatura_id)
                                                                                                          asignatura_id,
                                                                                     detalle_manual
                                                                                from uji_horarios.hor_items items,
                                                                                     uji_horarios.hor_items_asignaturas asi
                                                                               where items.id = asi.item_id
                                                                                 and items.semestre_id = p_semestre
                                                                                 and items.dia_semana_id is not null
                                                                                 and items.id in (
                                                                                              select item_id
                                                                                                from hor_items_profesores
                                                                                               where profesor_id =
                                                                                                                  p_prof)
                                                                            group by asi.asignatura_id,
                                                                                     items.id,
                                                                                     items.grupo_id,
                                                                                     items.tipo_subgrupo,
                                                                                     items.tipo_subgrupo_id,
                                                                                     items.subgrupo_id,
                                                                                     items.dia_semana_id,
                                                                                     items.semestre_id,
                                                                                     items.hora_inicio,
                                                                                     items.hora_fin,
                                                                                     items.detalle_manual) x
                                                                     where exists (
                                                                              select 1
                                                                                from hor_items i,
                                                                                     hor_items_profesores p
                                                                               where i.id = p.item_id
                                                                                 and profesor_id = p_prof
                                                                                 and i.id <> x.id
                                                                                 and i.dia_semana_id = x.dia_Semana_id
                                                                                 and i.semestre_id = x.semestre_id
                                                                                 and i.detalle_manual = x.detalle_manual
                                                                                 --and i.detalle_manual = p.detalle_manual
                                                                                 and (   to_number
                                                                                             (to_char ((i.hora_inicio
                                                                                                        + 1 / 1440
                                                                                                       ),
                                                                                                       'hh24mi'))
                                                                                            between to_number
                                                                                                      (to_char
                                                                                                          (x.hora_inicio,
                                                                                                           'hh24mi'))
                                                                                                and to_number
                                                                                                      (to_char
                                                                                                            (x.hora_fin,
                                                                                                             'hh24mi'))
                                                                                      or to_number
                                                                                                (to_char ((i.hora_fin
                                                                                                           - 1 / 1440
                                                                                                          ),
                                                                                                          'hh24mi'))
                                                                                            between to_number
                                                                                                      (to_char
                                                                                                          (x.hora_inicio,
                                                                                                           'hh24mi'))
                                                                                                and to_number
                                                                                                      (to_char
                                                                                                            (x.hora_fin,
                                                                                                             'hh24mi'))
                                                                                     )))
                                                          group by id,
                                                                   grupo_id,
                                                                   subgrupo_id,
                                                                   tipo_subgrupo,
                                                                   tipo_subgrupo_id,
                                                                   dia_semana_id,
                                                                   semestre_id,
                                                                   hora_inicio,
                                                                   hora_fin,
                                                                   detalle_manual)
                                                   where cuantos > 1
                                                group by id,
                                                         semestre_id,
                                                         dia_Semana_id,
                                                         grupo_id,
                                                         tipo_subgrupo_id || subgrupo_id,
                                                         detalle_manual)
                                         and i.id = d.item_id
                                         and d.id = dp.detalle_id
                                         and p.id = dp.item_profesor_id
                                         and p.detalle_manual = 1
                                      union all
                                      select d.inicio, fin
                                        from hor_items i,
                                             hor_items_profesores p,
                                             hor_items_detalle d
                                       where i.id = p.item_id
                                         and profesor_id = p_prof
                                         and i.id in (
                                                select   id
                                                    from (select   id, grupo_id, subgrupo_id, tipo_subgrupo,
                                                                   tipo_subgrupo_id, dia_semana_id, hora_inicio,
                                                                   hora_fin, wm_concat (asignatura_id) asignatura_id,
                                                                   semestre_id,
                                                                   count (*) over (partition by semestre_id, dia_semana_id)
                                                                                                                cuantos,
                                                                   detalle_manual
                                                              from (select id, asignatura_id, semestre_id, grupo_id,
                                                                           tipo_subgrupo, tipo_subgrupo_id, subgrupo_id,
                                                                           dia_semana_id, hora_inicio, hora_fin,
                                                                           detalle_manual
                                                                      from (select   items.id, items.grupo_id,
                                                                                     items.tipo_subgrupo,
                                                                                     items.tipo_subgrupo_id,
                                                                                     items.subgrupo_id,
                                                                                     items.dia_semana_id,
                                                                                     items.hora_inicio, items.hora_fin,
                                                                                     items.semestre_id,
                                                                                     wm_concat
                                                                                        (asi.asignatura_id)
                                                                                                          asignatura_id,
                                                                                     detalle_manual
                                                                                from uji_horarios.hor_items items,
                                                                                     uji_horarios.hor_items_asignaturas asi
                                                                               where items.id = asi.item_id
                                                                                 and items.semestre_id = p_semestre
                                                                                 and items.dia_semana_id is not null
                                                                                 and items.id in (
                                                                                              select item_id
                                                                                                from hor_items_profesores
                                                                                               where profesor_id =
                                                                                                                  p_prof)
                                                                            group by asi.asignatura_id,
                                                                                     items.id,
                                                                                     items.grupo_id,
                                                                                     items.tipo_subgrupo,
                                                                                     items.tipo_subgrupo_id,
                                                                                     items.subgrupo_id,
                                                                                     items.dia_semana_id,
                                                                                     items.semestre_id,
                                                                                     items.hora_inicio,
                                                                                     items.hora_fin,
                                                                                     items.detalle_manual) x
                                                                     where exists (
                                                                              select 1
                                                                                from hor_items i,
                                                                                     hor_items_profesores p
                                                                               where i.id = p.item_id
                                                                                 and profesor_id = p_prof
                                                                                 and i.id <> x.id
                                                                                 and i.dia_semana_id = x.dia_Semana_id
                                                                                 and i.semestre_id = x.semestre_id
                                                                                 and i.detalle_manual = x.detalle_manual
                                                                                 --and i.detalle_manual = p.detalle_manual
                                                                                 and (   to_number
                                                                                             (to_char ((i.hora_inicio
                                                                                                        + 1 / 1440
                                                                                                       ),
                                                                                                       'hh24mi'))
                                                                                            between to_number
                                                                                                      (to_char
                                                                                                          (x.hora_inicio,
                                                                                                           'hh24mi'))
                                                                                                and to_number
                                                                                                      (to_char
                                                                                                            (x.hora_fin,
                                                                                                             'hh24mi'))
                                                                                      or to_number
                                                                                                (to_char ((i.hora_fin
                                                                                                           - 1 / 1440
                                                                                                          ),
                                                                                                          'hh24mi'))
                                                                                            between to_number
                                                                                                      (to_char
                                                                                                          (x.hora_inicio,
                                                                                                           'hh24mi'))
                                                                                                and to_number
                                                                                                      (to_char
                                                                                                            (x.hora_fin,
                                                                                                             'hh24mi'))
                                                                                     )))
                                                          group by id,
                                                                   grupo_id,
                                                                   subgrupo_id,
                                                                   tipo_subgrupo,
                                                                   tipo_subgrupo_id,
                                                                   dia_semana_id,
                                                                   semestre_id,
                                                                   hora_inicio,
                                                                   hora_fin,
                                                                   detalle_manual)
                                                   where cuantos > 1
                                                group by id,
                                                         semestre_id,
                                                         dia_Semana_id,
                                                         grupo_id,
                                                         tipo_subgrupo_id || subgrupo_id,
                                                         detalle_manual)
                                         and i.id = d.item_id
                                         and p.detalle_manual = 0)
                            group by inicio,
                                     fin
                              having count (*) > 1)
                order by 1;
   begin
      v_nombre := 'cap';

      for p in lista_profesores (p_dep, p_Area) loop
         for p_Sem in 1 .. 2 loop
            begin
               v_aux := 0;

               for item in lista_solapamientos (p.id, p_sem) loop
                  v_aux := v_aux + 1;

                  if v_aux = 1 then
                     if p.nombre_area <> v_nombre then
                        v_nombre := p.nombre_area;
                        fop.block (espacios (3) || fof.bold (p.nombre_area), font_size => 12, space_after => 0.2,
                                   space_before => 0.2);
                     end if;

                     fop.block (espacios (6) || fof.bold ('Semestre ' || p_sem || ' de ' || p.nombre), font_size => 10);
                  end if;

                  fop.block (espacios (9) || item.dia_semana || ' - ' || to_char (item.inicio, 'hh24:mi') || '-'
                             || to_char (item.fin, 'hh24:mi') || ' - ' || item.asignatura_id || ' - ' || item.grupo_id
                             || ' ' || item.subgrupo_id,
                             font_size => 10);
                  v_sesiones := 0;

                  for sesiones in lista_solapes_Semana_Detalle (p.id, p_sem, item.id) loop
                     fop.block (espacios (12) || to_char (sesiones.inicio, 'dd/mm/yyyy') || '    '
                                || to_char (sesiones.inicio, 'hh24:mi') || ' - ' || to_char (sesiones.fin, 'hh24:mi'),
                                font_size => 10);
                     v_sesiones := v_sesiones + 1;
                  end loop;

                  if v_sesiones = 0 then
                     fop.block (espacios (12) || 'Sols solapa en setmana generica, en semana detall no.',
                                font_size => 10);
                  end if;
               end loop;

               if v_aux > 0 then
                  fop.block (fof.white_space);
               end if;
            exception
               when others then
                  fop.block (sqlerrm);
            end;
         end loop;
      end loop;
   end;

   procedure control_3 (p_dep in number) is
      v_control     varchar2 (200);
      v_crd_final   number;
      v_total       number;
      v_txt         varchar2 (2000);

      cursor lista_Descuadres_ant (p_dep in number) is
         select   id, nombre, nvl (asignatura_txt, asignatura_id) asignatura_id, grupo_id, tipo, crd_final, crd_item,
                  total, nombre_area, asignatura_txt
             from (select id, nombre, asignatura_id, grupo_id, tipo, crd_final, crd_item, nombre_area,
                          sum (crd_final) over (partition by asignatura_id, grupo_id, tipo) total, asignatura_txt
                     from (select distinct d.id, d.nombre, asignatura_id, grupo_id, tipo, crd_final, crd_item,
                                           a.nombre nombre_area, asignatura_txt
                                      from hor_v_items_creditos_detalle d,
                                           hor_areas a
                                     where d.departamento_id = p_dep
                                       and area_id = a.id))
            where crd_item <> total
              and asignatura_id in (select asignatura_id
                                      from hor_v_asignaturas_area aa,
                                           hor_Areas a
                                     where area_id = a.id
                                       and departamento_id = p_dep
                                       and porcentaje > 0)
         order by asignatura_id,
                  grupo_id,
                  tipo,
                  nombre;

      cursor lista_Descuadres (p_dep in number) is
         select   id, nombre, nvl (asignatura_txt, asignatura_id) asignatura_id, grupo_id, tipo, crd_final, crd_item,
                  total, nombre_area, asignatura_Txt
             from (select   id, nombre, asignatura_id, grupo_id, tipo, crd_final, crd_item, total, nombre_area,
                            min (asignatura_txt) asignatura_txt
                       from (select id, nombre, asignatura_id, grupo_id, tipo, crd_final, crd_item, nombre_area,
                                    sum (crd_final) over (partition by asignatura_id, grupo_id, tipo) total,
                                    asignatura_txt
                               from (select distinct d.id, d.nombre, asignatura_id, grupo_id, tipo, crd_final, crd_item,
                                                     a.nombre nombre_area, asignatura_txt
                                                from hor_v_items_creditos_detalle d,
                                                     hor_areas a
                                               where d.departamento_id = p_dep
                                                 and area_id = a.id))
                      where crd_item <> total
                        and asignatura_id in (select asignatura_id
                                                from hor_v_asignaturas_area aa,
                                                     hor_Areas a
                                               where area_id = a.id
                                                 and departamento_id = p_dep
                                                 and porcentaje > 0)
                   group by id,
                            nombre,
                            asignatura_id,
                            grupo_id,
                            tipo,
                            crd_final,
                            crd_item,
                            total,
                            nombre_area)
         order by asignatura_id,
                  grupo_id,
                  tipo,
                  nombre;
   begin
      v_aux := 0;
      v_control := 'x';

      for p in lista_descuadres (p_dep) loop
         v_aux := v_aux + 1;

         if v_aux = 1 then
            fop.tableOpen;
            fop.tablecolumndefinition (column_width => 2);
            fop.tablecolumndefinition (column_width => 2);
            fop.tablecolumndefinition (column_width => 7);
            fop.tablecolumndefinition (column_width => 2);
            fop.tablecolumndefinition (column_width => 2);
            fop.tablecolumndefinition (column_width => 2);
            fop.tablebodyopen;
            fop.tableRowOpen;
            fop.tablecellopen;
            fop.block (fof.white_space, font_size => 10);
            fop.tablecellclose;
            fop.tablecellopen;
            fop.block (fof.bold ('Crd Grup'), font_size => 10, text_align => 'center');
            fop.tablecellclose;
            fop.tablecellopen;
            fop.block (fof.white_space, font_size => 10);
            fop.tablecellclose;
            fop.tablecellopen;
            fop.block (fof.bold ('Grup'), font_size => 10, text_align => 'center');
            fop.tablecellclose;
            fop.tablecellopen;
            fop.block (fof.bold ('Crd Prof'), font_size => 10, text_align => 'right');
            fop.tablecellclose;
            fop.tablecellopen;
            fop.block (fof.bold ('Diferència'), font_size => 10, text_align => 'right');
            fop.tablecellclose;
            fop.tableRowClose;
         else
            if v_control <> p.asignatura_id || ' ' || p.grupo_id || ' ' || p.tipo then
               fop.tableRowOpen;
               fop.tablecellopen;
               fop.block (fof.white_space, font_size => 10);
               fop.tablecellclose;
               fop.tablecellopen;
               fop.block (fof.white_space, font_size => 10);
               fop.tablecellclose;
               fop.tablecellopen;
               fop.block (fof.white_space, font_size => 10);
               fop.tablecellclose;
               fop.tablecellopen;
               fop.block (fof.white_space, font_size => 10);
               fop.tablecellclose;
               fop.tablecellopen;
               fop.block (formatear (v_total), font_size => 10, text_align => 'right');
               fop.tablecellclose;
               fop.tablecellopen;
               fop.block (fof.bold (formatear (v_total - v_crd_final)), font_size => 14, text_align => 'right');
               fop.tablecellclose;
               fop.tableRowClose;
               fop.tableRowOpen;
               fop.tablecellopen;
               fop.block (fof.white_space, font_size => 10);
               fop.tablecellclose;
               fop.tableRowClose;
            end if;
         end if;

         v_txt := p.asignatura_id;

         if instr (p.asignatura_id, ',') > 0 then
            v_txt := replace (p.asignatura_id, ',', ', ');
         end if;

         fop.tableRowOpen;
         fop.tablecellopen;
         fop.block (v_txt, font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (formatear (p.crd_item), font_size => 10, text_align => 'center');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (p.nombre || ' (' || italic (p.nombre_area) || ')', font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (p.grupo_id || ' - ' || p.tipo, font_size => 10, text_align => 'center');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (formatear (p.crd_final), font_size => 10, text_align => 'right');
         fop.tablecellclose;
         fop.tableRowClose;
         v_control := p.asignatura_id || ' ' || p.grupo_id || ' ' || p.tipo;
         v_crd_final := p.crd_item;
         v_total := p.total;
      end loop;

      if v_aux >= 1 then
         fop.tableRowOpen;
         fop.tablecellopen;
         fop.block (fof.white_space, font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.white_space, font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.white_space, font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.white_space, font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (formatear (v_total), font_size => 10, text_align => 'right');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold (formatear (nvl (v_total, 0) - nvl (v_crd_final, 0))), font_size => 14,
                    text_align => 'right');
         fop.tablecellclose;
         fop.tableRowClose;
      end if;

      if v_aux > 0 then
         fop.tablebodyclose;
         fop.tableClose;
      end if;
   end;

   procedure control_4 (p_dep in number) is
      v_total_area   number;
      v_area         number;
      v_acum1        number;
      v_acum2        number;
   begin
      fop.tableOpen;
      fop.tablecolumndefinition (column_width => 9);
      fop.tablecolumndefinition (column_width => 2);
      fop.tablecolumndefinition (column_width => 2);
      fop.tablecolumndefinition (column_width => 2);
      fop.tablecolumndefinition (column_width => 3);
      fop.tablebodyopen;
      fop.tableRowOpen;
      fop.tablecellopen;
      fop.block (fof.bold ('Àrees de coneixement'), font_size => 10);
      fop.tablecellclose;
      fop.tablecellopen;
      fop.block (fof.bold ('Crèdits a impartir'), font_size => 10, text_align => 'right');
      fop.tablecellclose;
      fop.tablecellopen;
      fop.block (fof.bold ('Crèdits assignats'), font_size => 10, text_align => 'right');
      fop.tablecellclose;
      fop.tablecellopen;
      fop.block (fof.bold ('% assig- nació'), font_size => 10, text_align => 'right');
      fop.tablecellclose;
      fop.tableRowClose;
      v_acum1 := 0;
      v_acum2 := 0;

      for x in lista_areas (p_dep) loop
         select nvl (sum (creditos * porcentaje / 100), 0) crd_total_area
           into v_total_area
           from (select distinct nvl (comun_texto, asignatura_id) asignatura_id, grupo_id, tipo_subgrupo_id,
                                 subgrupo_id, creditos, porcentaje
                            from hor_items i,
                                 hor_items_asignaturas a,
                                 hor_ext_asignaturas_Area aa
                           where i.id = a.item_id
                             and asignatura_id = asi_id
                             and uest_id = x.id);

         /*
         select nvl (sum (creditos), 0)
         into   v_Area
         from   (select distinct d.profesor_id, nvl (comun_Texto, asignatura_id) asignatura_id, grupo_id,
                                 tipo_subgrupo_id, subgrupo_id, uest_id area_id,
                                 round (nvl (creditos_detalle, d.creditos) * porcentaje / 100, 2) creditos
                 from            hor_v_items_prof_detalle d,
                                 hor_items i,
                                 hor_items_asignaturas a,
                                 hor_ext_asignaturas_area aa
                 where           d.item_id = i.id
                 and             i.id = a.item_id
                 and             asignatura_id = asi_id
                 and             uest_id = x.id);
                 */
         select nvl (sum (creditos), 0)
           into v_area
           from (select distinct d.profesor_id, min (nvl (comun_Texto, asignatura_id)) asignatura_id, grupo_id,
                                 tipo_subgrupo_id, subgrupo_id, uest_id area_id,

                                 --nvl (creditos_detalle, d.creditos) * porcentaje / 100 creditos
                                 nvl (creditos_detalle, d.creditos) creditos
                            from hor_v_items_prof_detalle d,
                                 hor_items i,
                                 hor_items_asignaturas a,
                                 hor_ext_asignaturas_area aa,
                                 hor_profesores h
                           where d.item_id = i.id
                             and i.id = a.item_id
                             and asignatura_id = asi_id
                             and uest_id = x.id
                             and d.profesor_id = h.id
                             and h.area_id = uest_id
                        group by d.profesor_id,
                                 grupo_id,
                                 tipo_subgrupo_id,
                                 subgrupo_id,
                                 uest_id,
                                 creditos_detalle,
                                 d.creditos,
                                 porcentaje);

         fop.tableRowOpen;
         fop.tablecellopen;
         fop.block (x.nombre, font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (formatear (v_total_area), font_size => 10, text_align => 'right');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (formatear (v_area), font_size => 10, text_align => 'right');
         fop.tablecellclose;
         fop.tablecellopen;

         if v_total_area <> 0 then
            fop.block (formatear (round (v_area / v_total_area * 100, 2)) || '%', font_size => 10,
                       text_align => 'right');
         else
            fop.block (formatear (0) || '%', font_size => 10, text_align => 'right');
         end if;

         fop.tablecellclose;

         if v_area > v_total_area then
            fop.tablecellopen;
            fop.block (fof.bold ('Excés de crèdits'), font_size => 10, text_align => 'right');
            fop.tablecellclose;
         end if;

         fop.tableRowClose;
         v_acum1 := v_acum1 + v_total_Area;
         v_acum2 := v_acum2 + v_area;
      end loop;

      fop.tableRowOpen;
      fop.tablecellopen;
      fop.block (fof.white_space, font_size => 10);
      fop.tablecellclose;
      fop.tablecellopen;
      fop.block (fof.bold (formatear (v_acum1)), font_size => 10, text_align => 'right');
      fop.tablecellclose;
      fop.tablecellopen;
      fop.block (fof.bold (formatear (v_acum2)), font_size => 10, text_align => 'right');
      fop.tablecellclose;
      fop.tableRowClose;
      fop.tableBodyClose;
      fop.tableClose;
   end;
BEGIN
   select count (*) + 1
     into vSesion
     from apa_sesiones
    where sesion_id = pSesion
      and fecha < sysdate () - 0.33;

   select id
     into vCurso
     from hor_curso_academico;

   if vSesion > 0 then
      cabecera;

      begin
         fop.block (fof.bold ('Control 1 - Professors amb excés de crèdits assignats'), font_size => 16);
         fop.block (fof.white_space);

         for a in lista_areas (pDepartamento) loop
            control_1 (pDepartamento, a.id);
         end loop;

         fop.block (fof.white_space);
         fop.block (fof.white_space);
      exception
         when others then
            fop.block ('Error control 1: ' || sqlerrm, font_size => 12);
      end;

      begin
         fop.block (fof.bold ('Control 2 - Solapaments (a setmana generica)'), font_size => 16);
         fop.block (fof.white_space);

         for a in lista_areas (pDepartamento) loop
            control_2 (pDepartamento, a.id);
         end loop;

         fop.block (fof.white_space);
         fop.block (fof.white_space);
      exception
         when others then
            fop.block ('Error control 2: ' || sqlerrm, font_size => 12);
      end;

      begin
         fop.block (fof.bold ('Control 3 - Desquadres de crèdits per assignatura'), font_size => 16);
         fop.block (fof.white_space);
         control_3 (pDepartamento);
         fop.block (fof.white_space);
         fop.block (fof.white_space);
      exception
         when others then
            fop.block ('Error control 3: ' || sqlerrm, font_size => 12);
      end;

      begin
         fop.block (fof.bold ('Control 4 - Estat d''assignació de crèdits per àrea de coneixement'), font_size => 16);
         fop.block (fof.white_space);
         control_4 (pDepartamento);
         fop.block (fof.white_space);
         fop.block (fof.white_space);
      exception
         when others then
            fop.block ('Error control 3: ' || sqlerrm, font_size => 12);
      end;

      pie;
   else
      cabecera;
      fop.block ('No tens permís per accedir a aquest document.', font_weight => 'bold', text_align => 'center',
                 font_size => 14, space_after => 0.5);
      pie;
   end if;
END;

