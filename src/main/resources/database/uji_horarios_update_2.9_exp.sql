create or replace force view UJI_HORARIOS.HOR_EXT_CONVOCATORIAS
(
   ID,
   NOMBRE,
   NOMCAS,
   ORDEN,
   NOMANG,
   TIPO,
   EXTRA,
   NOMBRE_CERT_CA,
   NOMBRE_CERT_ES,
   NOMBRE_CERT_UK,
   TIPO_ESTUDIO_ID
)
   bequeath definer as
   select ID, NOMBRE || ' [Grau]', NOMCAS || ' [Grado]', ORDEN, NOMANG || ' [Grade]', TIPO, EXTRA, NOMBRE_CERT_CA, NOMBRE_CERT_ES, NOMBRE_CERT_UK, 'G'
     from pod_convocatorias
    where id in (9, 10, 11)
   union
   select decode(id,  9, 3,  10, 6,  11, 4,  id) ID, replace(nombre, 'Grau', 'Master') || ' [Master]' NOMBRE, replace(nomcas, 'Grado', 'Master') || ' [Master]' NOMCAS, ORDEN, replace(nomang, 'Grade', 'Master') || ' [Master]' NOMANG, 'M' TIPO, EXTRA, NOMBRE_CERT_CA, NOMBRE_CERT_ES, NOMBRE_CERT_UK, 'M'
     from pod_convocatorias
    where id in (9, 10, 11);

    create or replace force view UJI_HORARIOS.HOR_V_FECHAS_CONVOCATORIAS
    (
       ID,
       NOMBRE,
       FECHA_EXAMENES_INICIO,
       FECHA_EXAMENES_FIN,
       TIPO_ESTUDIO_ID
    )
       bequeath definer as
       select distinct convocatoria_id id, c.nombre nombre, s.fecha_examenes_inicio, s.fecha_examenes_fin, c.TIPO_ESTUDIO_ID
         from hor_examenes e, hor_semestres_detalle s, hor_ext_convocatorias c
        where ((e.convocatoria_id = 9 and semestre_id = 1) or (e.convocatoria_id = 10 and semestre_id = 2)) and e.convocatoria_id = c.id
       union all
       select distinct convocatoria_id, c.nombre, s.fechas_examenes_inicio_c2, s.fechas_examenes_fin_c2, c.TIPO_ESTUDIO_ID
         from hor_examenes e, hor_semestres_detalle s, hor_ext_convocatorias c
        where (e.convocatoria_id = 11) and e.convocatoria_id = c.id
       union all -- Nuevo convocatorias de master
       select distinct convocatoria_id id, c.nombre nombre, s.fecha_examenes_inicio, s.fecha_examenes_fin, c.TIPO_ESTUDIO_ID
         from hor_examenes e, hor_semestres_detalle s, hor_ext_convocatorias c
        where ((e.convocatoria_id = 3 and semestre_id = 1) or (e.convocatoria_id = 6 and semestre_id = 2)) and e.convocatoria_id = c.id
       union all
       select distinct convocatoria_id, c.nombre, s.fechas_examenes_inicio_c2, s.fechas_examenes_fin_c2, c.TIPO_ESTUDIO_ID
         from hor_examenes e, hor_semestres_detalle s, hor_ext_convocatorias c
        where (e.convocatoria_id = 4) and e.convocatoria_id = c.id;

create or replace force view UJI_HORARIOS.HOR_V_FECHAS_CONVOCATORIAS_EST
(
   ID,
   CONVOCATORIA_ID,
   NOMBRE,
   FECHA_EXAMENES_INICIO,
   FECHA_EXAMENES_FIN,
   ESTUDIO_ID,
   CURSO_ID,
   TIPO_ESTUDIO_ID
)
   bequeath definer as
   select e.id * 100000 + c.id * 100 + x.curso_id id, c.id convocatoria_id, c.nombre, c.fecha_examenes_inicio, c.fecha_examenes_fin, e.id estudio_id, x.curso_id, e.tipo_id
     from hor_v_Fechas_convocatorias c,
          hor_estudios               e,
          (    select level curso_id
                 from dual
           connect by level <= 6) x
    where e.tipo_id = c.tipo_estudio_id and x.curso_id <= e.numero_cursos and (e.id, x.curso_id) not in (select de.estudio_id, de.curso_id
                                                                                                           from hor_semestres_detalle_estudio de)
   union all
   select distinct de.estudio_id * 100000 + c.id * 100 + de.curso_id id, c.id convocatoria_id, c.nombre, de.fecha_examenes_inicio, de.fecha_examenes_fin, de.estudio_id, de.curso_id, e.tipo_id
     from hor_v_Fechas_convocatorias c, hor_estudios e, hor_semestres_detalle d, hor_semestres_detalle_estudio de
    where e.id = de.estudio_id and d.id = de.detalle_id and c.id in (9, 3) and semestre_id = 1 and e.tipo_id = c.tipo_estudio_id
   union all
   select distinct de.estudio_id * 100000 + c.id * 100 + de.curso_id id, c.id convocatoria_id, c.nombre, de.fecha_examenes_inicio, de.fecha_examenes_fin, de.estudio_id, de.curso_id, e.tipo_id
     from hor_v_Fechas_convocatorias c, hor_estudios e, hor_semestres_detalle d, hor_semestres_detalle_estudio de
    where e.id = de.estudio_id and d.id = de.detalle_id and c.id in (10, 6) and semestre_id = 2 and e.tipo_id = c.tipo_estudio_id
   union all
   select distinct de.estudio_id * 100000 + c.id * 100 + de.curso_id id, c.id convocatoria_id, c.nombre, de.fecha_examenes_inicio_c2, de.fecha_examenes_fin_c2, de.estudio_id, de.curso_id, e.tipo_id
     from hor_v_Fechas_convocatorias c, hor_estudios e, hor_semestres_detalle d, hor_semestres_detalle_estudio de
    where e.id = de.estudio_id and d.id = de.detalle_id and c.id in (11, 4) and e.tipo_id = c.tipo_estudio_id;


create or replace force view UJI_HORARIOS.HOR_V_EXT_CARGOS_PER
(
   ID,
   PERSONA_ID,
   NOMBRE,
   CENTRO_ID,
   CENTRO,
   ESTUDIO_ID,
   ESTUDIO,
   CARGO_ID,
   CARGO,
   DEPARTAMENTO_ID,
   DEPARTAMENTO,
   AREA_ID,
   AREA,
   CURSO_ID,
   ORIGEN
)
   bequeath definer as
   select "ID", "PERSONA_ID", "NOMBRE", "CENTRO_ID", "CENTRO", "ESTUDIO_ID", "ESTUDIO", "CARGO_ID", "CARGO", "DEPARTAMENTO_ID", "DEPARTAMENTO", "AREA_ID", "AREA", "CURSO_ID", "ORIGEN"
     from (select rownum id, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, TITULACION_ID estudio_id, TITULACION estudio, CARGO_ID, CARGO, DEPARTAMENTO_ID, DEPARTAMENTO, AREA_ID, AREA, CURSO_ID, origen
             from ( /* PDI de centro + director (33) y decanos (34,35) */
                   select cp.per_id persona_id, p.nombre || ' ' || apellido1 || ' ' || apellido2 nombre, ulogica_id centro_id, ubic.nombre centro, tit.id titulacion_id, tit.nombre titulacion, tc.id cargo_id, tc.nombre cargo, d.id departamento_id, d.nombre departamento, a.id area_id, a.nombre area, null curso_id, 1 origen
                     from grh_grh.grh_cargos_per         cp,
                          gri_est.est_ubic_estructurales ubic,
                          gra_Exp.exp_v_titu_todas       tit,
                          uji_horarios.hor_tipos_Cargos  tc,
                          gri_per.per_personas           p,
                          uji_horarios.hor_departamentos d,
                          uji_horarios.hor_areas         a
                    where (f_fin is null and (f_fin is null or f_fin >= sysdate) and crg_id in (189, 195, 188, 30, 33, 34, 35)) and ulogica_id = ubic.id and ulogica_id = tit.uest_id and tit.activa =
                          'S' and tit.tipo = 'G' and tc.id = 3 and cp.per_id = p.id and ulogica_id = d.centro_id and d.id = a.departamento_id
                   union all
                   /* PAS de centro */
                   select cp.per_id persona_id, p.nombre || ' ' || apellido1 || ' ' || apellido2 nombre, ubicacion_id centro_id, ubic.nombre centro, tit.id titulacion_id, tit.nombre titulacion, tc.id cargo_id, tc.nombre cargo, d.id departamento_id, d.nombre departamento, a.id area_id, a.nombre area, null curso_id, 2 origen
                     from grh_grh.grh_vw_contrataciones_ult cp,
                          gri_est.est_ubic_estructurales    ubic,
                          gra_Exp.exp_v_titu_todas          tit,
                          uji_horarios.hor_tipos_Cargos     tc,
                          gri_per.per_personas              p,
                          uji_horarios.hor_departamentos    d,
                          uji_horarios.hor_areas            a
                    where ubicacion_id = ubic.id and ubicacion_id = tit.uest_id and tit.activa = 'S' and tit.tipo = 'G' and tc.id = 4 and cp.per_id = p.id --and ubicacion_id = d.centro_id
                                                                                                                                                          and decode(ubicacion_id,
                          97, 2922,
                          96, 2922,
                          ubicacion_id) = d.centro_id and d.id = a.departamento_id
                   union all
                   /* directores de titulacion */
                   select cp.per_id persona_id, p.nombre || ' ' || apellido1 || ' ' || apellido2 nombre, ulogica_id centro_id, ubic.nombre centro, tit.id titulacion_id, tit.nombre titulacion, tc.id cargo_id, tc.nombre cargo, null departamento_id, '' departamento, null area_id, '' area, null curso_id, 3 origen
                     from grh_grh.grh_cargos_per cp, gri_est.est_ubic_estructurales ubic, gra_Exp.exp_v_titu_todas tit, uji_horarios.hor_tipos_Cargos tc, gri_per.per_personas p
                    where (f_fin is null and (f_fin is null or f_fin >= sysdate) and crg_id in (192, 193)) and ulogica_id = ubic.id and ulogica_id = tit.uest_id and tit.activa = 'S' and tit.tipo =
                          'G' and tc.id = 1 and cp.per_id = p.id and cp.tit_id = tit.id
                   union all
                   /* coordinadores de curso, académico y calidad*/
                   select cp.per_id persona_id, p.nombre || ' ' || apellido1 || ' ' || apellido2 nombre, ulogica_id centro_id, ubic.nombre centro, tit.id titulacion_id, tit.nombre titulacion, tc.id cargo_id, tc.nombre cargo, null departamento_id, '' departamento, null area_id, '' area, ciclo_cargo curso_id, 4 origen
                     from grh_grh.grh_cargos_per cp, gri_est.est_ubic_estructurales ubic, gra_Exp.exp_v_titu_todas tit, uji_horarios.hor_tipos_Cargos tc, gri_per.per_personas p
                    where (f_fin is null and (f_fin is null or f_fin >= sysdate) and crg_id in (305, 421, 422)) and ulogica_id = ubic.id and ulogica_id = tit.uest_id and tit.activa = 'S' and tit.tipo
                          = 'G' and tc.id = 2 and cp.per_id = p.id and cp.tit_id = tit.id
                   /* coordinadores de master*/
                   union all
                   select cp.per_id persona_id, p.nombre || ' ' || apellido1 || ' ' || apellido2 nombre, ubic.id centro_id, ubic.nombre centro, tit.id titulacion_id, tit.nombre titulacion, tc.id cargo_id, tc.nombre cargo, null departamento_id, '' departamento, null area_id, '' area, null curso_id, 3 origen
                     from grh_grh.grh_cargos_per cp, gri_est.est_ubic_estructurales ubic, gra_Exp.exp_v_titu_todas tit, uji_horarios.hor_tipos_Cargos tc, gri_per.per_personas p
                    where (f_fin is null and (f_fin is null or f_fin >= sysdate) and crg_id in (292)) and cp.TIT_ID = tit.id and tit.UEST_ID = ubic.id and tit.activa = 'S' and tit.tipo = 'M' and tc.
                          id = 2 and cp.per_id = p.id and cp.tit_id = tit.id
                   union all
                   /* directores de departamento */
                   select cp.per_id persona_id, p.nombre || ' ' || apellido1 || ' ' || apellido2 nombre, c.id centro_id, c.nombre centro, null titulacion_id, '' titulacion, tc.id cargo_id, tc.nombre cargo, ulogica_id departamento_id, d.nombre departamento, a.id area_id, a.nombre area, null curso_id, 5 origen
                     from grh_grh.grh_cargos_per        cp,
                          uji_horarios.hor_tipos_Cargos tc,
                          gri_per.per_personas          p,
                          est_ubic_estructurales        d,
                          est_relaciones_ulogicas       r,
                          est_ubic_estructurales        c,
                          uji_horarios.hor_Areas        a
                    where (f_fin is null and (f_fin is null or f_fin >= sysdate) and crg_id in (178, 194)) and tc.id = 6 and cp.per_id = p.id and ulogica_id = d.id and uest_id_relacionad = d.id and
                          d.tuest_id = 'DE' --and uest_id = c.id
                                           and decode(ulogica_id,  97, 2922,  96, 2922,  uest_id) = c.id and trel_id = 4 and ulogica_id = a.departamento_id
                   union all
                   /* pas de departamento */
                   select distinct cp.per_id persona_id, p.nombre || ' ' || apellido1 || ' ' || apellido2 nombre, c.id centro_id, c.nombre centro, null titulacion_id, '' titulacion, tc.id cargo_id, tc.nombre cargo, ubic.id departamento_id, ubic.nombre departamento, a.id area_id, a.nombre area, null curso_id, 6 origen
                     from grh_grh.grh_vw_contrataciones_ult cp,
                          gri_est.est_ubic_estructurales    ubic,
                          uji_horarios.hor_tipos_Cargos     tc,
                          gri_per.per_personas              p,
                          est_estructuras                   r,
                          est_ubic_estructurales            c,
                          uji_horarios.hor_areas            a
                    where ubic.id in (select ubicacion_id from dual
                          union
                                      select uest_id
                                        from gra_pod.pod_permisos_extra_pod
                                       where per_id = p.id) and tc.id = 5 and r.estado = 1 and cp.per_id = p.id and cp.act_id = 'PAS' and (ubicacion_id = uest_id_relacionad or (uest_id_relacionad in
                          (select uest_id
                             from gra_pod.pod_permisos_extra_pod
                            where per_id = p.id))) and trel_id = 4 --and uest_id = c.id
                                                                  and decode(ubic.id,  97, 2922,  96, 2922,  uest_id) = c.id and ubic.id = departamento_id
                   union all
                   /* pas de centre */
                   select distinct cp.per_id persona_id, p.nombre || ' ' || apellido1 || ' ' || apellido2 nombre, c.id centro_id, c.nombre centro, null titulacion_id, '' titulacion, tc.id cargo_id, tc.nombre cargo, ubic.id departamento_id, ubic.nombre departamento, a.id area_id, a.nombre area, null curso_id, 6 origen
                     from hor_areas a, hor_departamentos ubic, grh_vw_contrataciones_ult cp, gri_per.per_personas p, uji_horarios.hor_tipos_Cargos tc, est_ubic_estructurales c
                    where departamento_id = ubic.id and centro_id = cp.ubicacion_id and cp.ubicacion_id in (2, 3, 4, 2922) and tc.id = 4 and cp.per_id = p.id and centro_id = c.id
                   union all
                   /* permisos extra */
                   select distinct x.persona_id,
                                   x.nombre,
                                   nvl(x.centro_id,
                                       (select uest_id
                                          from est_relaciones_ulogicas
                                         where trel_id = 4 and uest_id in (2, 3, 4, 2922) and uest_id_relacionad = x.departamento_id))
                                      centro_id,
                                   nvl(x.centro,
                                       (select u.nombre
                                          from est_relaciones_ulogicas r, est_ubic_estructurales u
                                         where trel_id = 4 and uest_id in (2, 3, 4, 2922) and uest_id_relacionad = x.departamento_id and uest_id = u.id))
                                      centro,
                                   x.titulacion_id,
                                   titulacion,
                                   x.cargo_id,
                                   x.cargo,
                                   x.departamento_id,
                                   x.departamento,
                                   decode(cargo_id, 6, decode(area_id, null, a.id, area_id), area_id) area_id,
                                   decode(cargo_id, 6, decode(area_id, null, a.nombre, area), area)   area,
                                   curso_id,
                                   7                                                                  origen
                     from (select per.persona_id,
                                  p.nombre || ' ' || apellido1 || ' ' || apellido2 nombre,
                                  ubic.id                                          centro_id,
                                  ubic.nombre                                      centro,
                                  tit.id                                           titulacion_id,
                                  tit.nombre                                       titulacion,
                                  tc.id                                            cargo_id,
                                  tc.nombre                                        cargo,
                                  departamento_id,
                                  dep.nombre                                       departamento,
                                  area_id,
                                  (select nombre
                                     from gri_est.est_ubic_estructurales
                                    where id = area_id)
                                     area,
                                  per.curso_id
                             from uji_horarios.hor_permisos_extra per,
                                  gri_per.per_personas            p,
                                  (select *
                                     from gra_Exp.exp_v_titu_todas
                                    where activa = 'S' and tipo = 'G') tit,
                                  uji_horarios.hor_tipos_cargos   tc,
                                  gri_est.est_ubic_estructurales  ubic,
                                  gri_est.est_ubic_estructurales  dep
                            where persona_id = p.id and estudio_id = tit.id(+) and tipo_Cargo_id = tc.id and tit.uest_id = ubic.id(+) and per.departamento_id = dep.id(+)) x,
                          uji_horarios.hor_areas a
                    where ((cargo_id <> 6 and a.id in (select id
                                                         from uji_horarios.hor_areas)) or (cargo_id = 6 and area_id is not null and a.id = area_id) or (cargo_id = 6 and area_id is null and x.
                           departamento_id = a.departamento_id))
                   union all
                   /* administradores */
                   select distinct per.persona_id, p.nombre || ' ' || apellido1 || ' ' || apellido2 nombre, u.id centro_id, u.nombre centro, tit.id titulacion_id, tit.nombre titulacion, tc.id cargo_id, tc.nombre cargo, null departamento_id, '' departamento, null area_id, '' area, null curso_id, 8 origen
                     from uji_apa.apa_vw_personas_items per, gri_per.per_personas p, gra_Exp.exp_v_titu_todas tit, uji_horarios.hor_tipos_cargos tc, gri_Est.est_ubic_estructurales u
                    where persona_id = p.id and tc.id = 1 and tit.activa = 'S' and tit.tipo = 'G' and aplicacion_id = 46 and role = 'ADMIN' and tit.uest_id = u.id
                   union all
                   select distinct per.persona_id, p.nombre || ' ' || apellido1 || ' ' || apellido2 nombre, c.id centro_id, c.nombre centro, null titulacion_id, '' titulacion, tc.id cargo_id, tc.nombre cargo, dep.id departamento_id, dep.nombre departamento, null area_id, '' area, null curso_id, 9 origen
                     from uji_apa.apa_vw_personas_items  per,
                          gri_per.per_personas           p,
                          est_ubic_estructurales         dep,
                          uji_horarios.hor_tipos_cargos  tc,
                          gri_Est.est_ubic_estructurales c,
                          est_relaciones_ulogicas        r
                    where persona_id = p.id and tc.id = 6 and dep.status = 'A' and dep.tuest_id = 'DE' and aplicacion_id = 46 and role = 'ADMIN' --and decode(dep.id,97,2943,dep.id)
                                                                                                                                                and dep.id = r.uest_id_relacionad and r.trel_id = 4
                          and decode(dep.id,  97, 2922,  96, 2922,  r.uest_id) = c.id));