CREATE OR REPLACE FORCE VIEW UJI_HORARIOS.HOR_V_FECHAS_CONVOCATORIAS_EST (ID,
                                                                          NOMBRE,
                                                                          FECHA_EXAMENES_INICIO,
                                                                          FECHA_EXAMENES_FIN,
                                                                          ESTUDIO_ID
                                                                         ) AS
   select c.id, c.nombre, c.fecha_examenes_inicio, c.fecha_examenes_fin, e.id estudio_id
     from hor_v_Fechas_convocatorias c,
          hor_estudios e
    where e.id not in (select estudio_id
                         from hor_semestres_detalle_estudio de)
   union all
   select c.id, c.nombre, c.fecha_examenes_inicio, de.fecha_examenes_fin, de.estudio_id
     from hor_v_Fechas_convocatorias c,
          hor_estudios e,
          hor_semestres_detalle d,
          hor_semestres_detalle_estudio de
    where e.id = de.estudio_id
      and d.id = de.detalle_id
      and c.id = 9
      and semestre_id = 1
   union all
   select c.id, c.nombre, c.fecha_examenes_inicio, de.fecha_examenes_fin, de.estudio_id
     from hor_v_Fechas_convocatorias c,
          hor_estudios e,
          hor_semestres_detalle d,
          hor_semestres_detalle_estudio de
    where e.id = de.estudio_id
      and d.id = de.detalle_id
      and c.id = 10
      and semestre_id = 2
   union all
   select distinct c.id, c.nombre, c.fecha_examenes_inicio, de.fecha_examenes_fin_c2, de.estudio_id
              from hor_v_Fechas_convocatorias c,
                   hor_estudios e,
                   hor_semestres_detalle d,
                   hor_semestres_detalle_estudio de
             where e.id = de.estudio_id
               and d.id = de.detalle_id
               and c.id = 11;

			   

CREATE TABLE uji_horarios.hor_agrupaciones 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2 (1000)  NOT NULL 
    ) 
;



ALTER TABLE uji_horarios.hor_agrupaciones 
    ADD CONSTRAINT hor_agrupaciones_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_horarios.hor_agrupaciones_asignaturas 
    ( 
     id NUMBER  NOT NULL , 
     agrupacion_id NUMBER  NOT NULL , 
     asignatura_id VARCHAR2 (100)  NOT NULL 
    ) 
;


CREATE INDEX uji_horarios.hor_agr_asi_agr_IDX ON uji_horarios.hor_agrupaciones_asignaturas 
    ( 
     agrupacion_id ASC 
    ) 
;

ALTER TABLE uji_horarios.hor_agrupaciones_asignaturas 
    ADD CONSTRAINT hor_agr_asi_PK PRIMARY KEY ( id ) ;


ALTER TABLE uji_horarios.hor_agrupaciones_asignaturas 
    ADD CONSTRAINT hor_agr_asi_UN UNIQUE ( agrupacion_id , asignatura_id ) ;



CREATE TABLE uji_horarios.hor_agrupaciones_estudios 
    ( 
     id NUMBER  NOT NULL , 
     agrupacion_id NUMBER  NOT NULL , 
     estudio_id NUMBER  NOT NULL 
    ) 
;


CREATE INDEX uji_horarios.hor_agr_est_agr_IDX ON uji_horarios.hor_agrupaciones_estudios 
    ( 
     agrupacion_id ASC 
    ) 
;
CREATE INDEX uji_horarios.hor_agr_est_est_IDX ON uji_horarios.hor_agrupaciones_estudios 
    ( 
     estudio_id ASC 
    ) 
;

ALTER TABLE uji_horarios.hor_agrupaciones_estudios 
    ADD CONSTRAINT hor_agrupaciones_estudios_PK PRIMARY KEY ( id ) ;




ALTER TABLE uji_horarios.hor_agrupaciones_asignaturas 
    ADD CONSTRAINT hor_agr_asi_agr_FK FOREIGN KEY 
    ( 
     agrupacion_id
    ) 
    REFERENCES uji_horarios.hor_agrupaciones 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_agrupaciones_estudios 
    ADD CONSTRAINT hor_agr_est_agr_FK FOREIGN KEY 
    ( 
     agrupacion_id
    ) 
    REFERENCES uji_horarios.hor_agrupaciones 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_agrupaciones_estudios 
    ADD CONSTRAINT hor_agr_est_est_FK FOREIGN KEY 
    ( 
     estudio_id
    ) 
    REFERENCES uji_horarios.hor_estudios 
    ( 
     id
    ) 
;

CREATE OR REPLACE FORCE VIEW UJI_HORARIOS.HOR_V_ASIGNATURAS (ID,
                                                             ASIGNATURA,
                                                             ESTUDIO_ID,
                                                             ESTUDIO,
                                                             CURSO_ID,
                                                             CARACTER_ID,
                                                             CARACTER
                                                            ) AS
   select distinct asignatura_id id, asignatura, estudio_id, estudio, curso_id, caracter_id, caracter
              from hor_items_asignaturas;

ALTER TABLE UJI_HORARIOS.HOR_EXT_CARGOS_PER
 ADD (origen  NUMBER);


CREATE OR REPLACE FORCE VIEW UJI_HORARIOS.HOR_V_AGRUPACIONES_ITEMS (AGRUPACION_ID,
                                                                    NOMBRE_AGRUPACION,
                                                                    ASIGNATURA_ID,
                                                                    ESTUDIO_ID,
                                                                    CURSO_ID,
                                                                    CARACTER_ID,
                                                                    ITEM_ID,
                                                                    SEMESTRE_ID,
                                                                    AULA_PLANIFICACION_ID,
                                                                    COMUN,
                                                                    PORCENTAJE_COMUN,
                                                                    GRUPO_ID,
                                                                    TIPO_SUBGRUPO_ID,
                                                                    TIPO_SUBGRUPO,
                                                                    SUBGRUPO_ID,
                                                                    DIA_SEMANA_ID,
                                                                    HORA_INICIO,
                                                                    HORA_FIN,
                                                                    DESDE_EL_DIA,
                                                                    HASTA_EL_DIA,
                                                                    TIPO_ASIGNATURA_ID,
                                                                    TIPO_ASIGNATURA,
                                                                    TIPO_ESTUDIO_ID,
                                                                    TIPO_ESTUDIO,
                                                                    PLAZAS,
                                                                    REPETIR_CADA_SEMANAS,
                                                                    NUMERO_ITERACIONES,
                                                                    DETALLE_MANUAL,
                                                                    COMUN_TEXTO,
                                                                    AULA_PLANIFICACION_NOMBRE,
                                                                    CREDITOS
                                                                   ) AS
   select distinct a.id agrupacion_id, nombre nombre_agrupacion, aa.asignatura_id, estudio_id, ia.curso_id,
                   ia.caracter_id, item_id, i.semestre_id, aula_planificacion_id, comun, porcentaje_comun, grupo_id,
                   tipo_subgrupo_id, tipo_subgrupo, subgrupo_id, dia_semana_id, hora_inicio, hora_fin, desde_el_dia,
                   hasta_el_dia, tipo_asignatura_id, tipo_asignatura, tipo_estudio_id, tipo_estudio, plazas,
                   repetir_cada_semanas, numero_iteraciones, detalle_manual, comun_texto, aula_planificacion_nombre,
                   creditos
              from hor_agrupaciones a,
                   hor_agrupaciones_asignaturas aa,
                   hor_items_asignaturas ia,
                   hor_items i
             where a.id = aa.agrupacion_id
               and aa.asignatura_id = ia.asignatura_id
               and ia.item_id = i.id;

CREATE OR REPLACE FUNCTION UJI_HORARIOS.hor_f_fecha_orden (
   p_estudio         in   number,
   p_semestre        in   number,
   p_curso           in   number,
   p_asignatura      in   varchar2,
   p_grupo           in   varchar2,
   p_subgrupo_tipo   in   varchar2,
   p_subgrupo        in   number,
   p_dia_semana      in   number,
   p_fecha           in   date
)
   RETURN NUMBER IS
   v_aux   NUMBER;
BEGIN
   select count (*)
   into   v_aux
   from   (select ia.estudio_id, ia.curso_id, i.semestre_id, i.grupo_id, i.tipo_subgrupo_id, i.subgrupo_id,
                  i.dia_Semana_id, ia.asignatura_id, fecha_inicio, fecha_fin, fecha_examenes_inicio, fecha_examenes_fin,
                  i.desde_el_dia, hasta_el_dia, repetir_cada_semanas, numero_iteraciones, detalle_manual, c.fecha,
                  tipo_dia, dia_semana
           from   hor_estudios e,
                  hor_semestres_detalle s,
                  hor_items i,
                  hor_items_asignaturas ia,
                  hor_ext_calendario c
           where  e.tipo_id = s.tipo_estudio_id
           and    i.semestre_id = s.semestre_id
           and    c.fecha between fecha_inicio and nvl (fecha_examenes_fin, fecha_fin)
           and    c.dia_semana_id = i.dia_semana_id
           and    tipo_dia = 'L'
           and    detalle_manual = 0
           and    i.semestre_id = p_semestre
           and    ia.curso_id = p_curso
           and    i.id = ia.item_id
           and    ia.asignatura_id = p_asignatura
           and    ia.estudio_id = p_estudio
           and    ia.estudio_id = e.id
           and    i.grupo_id = p_grupo
           and    i.tipo_subgrupo_id = p_subgrupo_tipo
           and    i.subgrupo_id = p_subgrupo
           and    i.dia_semana_id = p_dia_semana
           and    fecha <= p_fecha);

   RETURN v_aux;
END hor_f_fecha_orden;


CREATE OR REPLACE TCREATE OR REPLACE TRIGGER UJI_HORARIOS.mutante_3_final_2014
   after insert or update of dia_semana_id,
                             desde_el_dia,
                             hasta_el_dia,
                             repetir_cada_semanas,
                             numero_iteraciones,
                             hora_inicio,
                             hora_fin
   ON UJI_HORARIOS.HOR_ITEMS
begin
   declare
      v_aux         number;
      v_num_items   number;
      v_id          number;
      v_hora_ini    date;
      v_hora_fin    date;
      v_ult_orden   number;

      cursor reg (v_rowid rowid) is
         select *
           from uji_horarios.hor_items
          where rowid = v_rowid;

      cursor lista_detalle (
         p_id                     in   number,
         p_detalle_manual         in   number,
         p_dia_semana_id          in   number,
         p_semestre_id            in   number,
         p_numero_iteraciones     in   number,
         p_repetir_cada_Semanas   in   number,
         p_hasta_el_dia           in   date,
         p_desde_el_dia           in   date,
         p_subgrupo_id            in   number,
         p_tipo_subgrupo_id       in   varchar2,
         p_grupo_id               in   varchar2
      ) is
         select   todo.*, row_number () over (partition by id order by fecha) orden_final
             from (select distinct id, fecha, docencia_paso_1, docencia_paso_2, docencia, orden_id, numero_iteraciones,
                                   repetir_cada_semanas, fecha_inicio, fecha_fin, semestre_id, grupo_id,
                                   tipo_subgrupo_id, subgrupo_id, dia_semana_id, tipo_dia, festivos
                              from (select i.id, fecha, docencia docencia_paso_1,
                                           decode (nvl (d.repetir_cada_semanas, 1),
                                                   1, docencia,
                                                   decode (mod (orden_id, d.repetir_cada_semanas), 1, docencia, 'N')
                                                  ) docencia_paso_2,
                                           decode
                                                 (tipo_dia,
                                                  'F', 'N',
                                                  decode (d.numero_iteraciones,
                                                          null, decode (nvl (d.repetir_cada_semanas, 1),
                                                                        1, docencia,
                                                                        decode (mod (orden_id, d.repetir_cada_semanas),
                                                                                1, docencia,
                                                                                'N'
                                                                               )
                                                                       ),
                                                          decode (sign (((orden_id - festivos) / d.repetir_cada_semanas
                                                                        )
                                                                        - (d.numero_iteraciones)),
                                                                  1, 'N',
                                                                  decode (nvl (d.repetir_cada_semanas, 1),
                                                                          1, docencia,
                                                                          decode (mod (orden_id, d.repetir_cada_semanas),
                                                                                  1, docencia,
                                                                                  'N'
                                                                                 )
                                                                         )
                                                                 )
                                                         )
                                                 ) docencia,
                                           d.orden_id, d.numero_iteraciones, d.repetir_cada_semanas, d.fecha_inicio,
                                           d.fecha_fin, d.estudio_id, d.semestre_id, d.asignatura_id, d.grupo_id,
                                           d.tipo_subgrupo_id, d.subgrupo_id, d.dia_semana_id, tipo_dia, festivos
                                      from (select id, fecha,
                                                   hor_contar_festivos (decode (x.desde_el_dia,
                                                                                null, fecha_inicio,
                                                                                x.desde_el_dia
                                                                               ),
                                                                        x.fecha, x.dia_semana_id,
                                                                        x.repetir_cada_semanas) festivos,
                                                   row_number () over (partition by decode
                                                                                      (decode
                                                                                             (sign (x.fecha
                                                                                                    - fecha_inicio),
                                                                                              -1, 'N',
                                                                                              decode (sign (fecha_fin
                                                                                                            - x.fecha),
                                                                                                      -1, 'N',
                                                                                                      'S'
                                                                                                     )
                                                                                             ),
                                                                                       'S', decode
                                                                                          (decode
                                                                                              (sign
                                                                                                  (x.fecha
                                                                                                   - decode
                                                                                                        (x.desde_el_dia,
                                                                                                         null, fecha_inicio,
                                                                                                         x.desde_el_dia
                                                                                                        )),
                                                                                               -1, 'N',
                                                                                               decode
                                                                                                  (sign
                                                                                                      (decode
                                                                                                          (x.hasta_el_dia,
                                                                                                           null, fecha_fin,
                                                                                                           x.hasta_el_dia
                                                                                                          )
                                                                                                       - x.fecha),
                                                                                                   -1, 'N',
                                                                                                   'S'
                                                                                                  )
                                                                                              ),
                                                                                           'S', 'S',
                                                                                           'N'
                                                                                          ),
                                                                                       'N'
                                                                                      ), id, estudio_id, semestre_id, asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo_id, dia_semana_id order by fecha)
                                                                                                               orden_id,
                                                   decode (hor_f_fecha_entre (x.fecha, fecha_inicio, fecha_fin),
                                                           'S', decode (hor_f_fecha_entre (x.fecha,
                                                                                           decode (desde_el_dia,
                                                                                                   null, fecha_inicio,
                                                                                                   desde_el_dia
                                                                                                  ),
                                                                                           decode (hasta_el_dia,
                                                                                                   null, fecha_fin,
                                                                                                   hasta_el_dia
                                                                                                  )),
                                                                        'S', 'S',
                                                                        'N'
                                                                       ),
                                                           'N'
                                                          ) docencia,
                                                   estudio_id, semestre_id, grupo_id, tipo_subgrupo_id, subgrupo_id,
                                                   dia_semana_id, asignatura_id, fecha_inicio, fecha_fin,
                                                   fecha_examenes_inicio, fecha_examenes_fin, desde_el_dia,
                                                   hasta_el_dia, repetir_cada_semanas, numero_iteraciones,
                                                   detalle_manual, tipo_dia, dia_semana
                                              from (select distinct i.id, null estudio_id, i.semestre_id, i.grupo_id,
                                                                    i.tipo_subgrupo_id, i.subgrupo_id, i.dia_semana_id,
                                                                    null asignatura_id, fecha_inicio, fecha_fin,
                                                                    fecha_examenes_inicio, fecha_examenes_fin,
                                                                    i.desde_el_dia, hasta_el_dia, repetir_cada_semanas,
                                                                    numero_iteraciones, detalle_manual, c.fecha,
                                                                    tipo_dia, dia_semana
                                                               from hor_v_fechas_estudios s,
                                                                    (select p_id id, p_detalle_manual detalle_manual,
                                                                            p_dia_semana_id dia_semana_id,
                                                                            p_semestre_id semestre_id,
                                                                            p_numero_iteraciones numero_iteraciones,
                                                                            p_repetir_cada_semanas repetir_cada_semanas,
                                                                            p_hasta_el_dia hasta_el_dia,
                                                                            p_desde_el_dia desde_el_dia,
                                                                            p_subgrupo_id subgrupo_id,
                                                                            p_tipo_subgrupo_id tipo_subgrupo_id,
                                                                            p_grupo_id grupo_id
                                                                       from dual) i,
                                                                    hor_ext_calendario c
                                                              where i.semestre_id = s.semestre_id
                                                                and trunc (c.fecha) between fecha_inicio
                                                                                        and decode (fecha_examenes_fin,
                                                                                                    null, fecha_fin,
                                                                                                    fecha_examenes_fin
                                                                                                   )
                                                                and c.dia_semana_id = i.dia_semana_id
                                                                and tipo_dia in ('L', 'E', 'F')
                                                                and vacaciones = 0
                                                                and s.tipo_estudio_id = 'G'
                                                                and detalle_manual = 0
                                                                and s.estudio_id in (
                                                                          select estudio_id
                                                                            from hor_items_asignaturas
                                                                           where item_id = i.id
                                                                             and curso_id = s.curso_id)
                                                                                                       --and i.id = 559274
                                                   ) x) d,
                                           (select p_id id, p_detalle_manual detalle_manual,
                                                   p_dia_semana_id dia_semana_id, p_semestre_id semestre_id,
                                                   p_numero_iteraciones numero_iteraciones,
                                                   p_repetir_cada_semanas repetir_cada_semanas,
                                                   p_hasta_el_dia hasta_el_dia, p_desde_el_dia desde_el_dia,
                                                   p_subgrupo_id subgrupo_id, p_tipo_subgrupo_id tipo_subgrupo_id,
                                                   p_grupo_id grupo_id
                                              from dual) i
                                     where i.semestre_id = d.semestre_id
                                       and i.grupo_id = d.grupo_id
                                       and i.tipo_subgrupo_id = d.tipo_subgrupo_id
                                       and i.subgrupo_id = d.subgrupo_id
                                       and i.dia_semana_id = d.dia_semana_id
                                       and i.detalle_manual = 0
                                       and i.id = d.id
                                    union all
                                    select distinct c.id, c.fecha, 'N' docencia_paso_1, 'N' docencia_paso_2,
                                                    decode (d.id, null, 'N', 'S') docencia,
                                                    row_number () over (partition by d.item_id order by d.inicio)
                                                                                                               orden_id,
                                                    numero_iteraciones, repetir_cada_semanas, fecha_inicio, fecha_fin,
                                                    estudio_id, semestre_id, asignatura_id, grupo_id, tipo_subgrupo_id,
                                                    subgrupo_id, dia_semana_id, tipo_dia,
                                                    decode (tipo_dia, 'F', 1, 0) festivos
                                               from (select distinct i.id, c.fecha, numero_iteraciones,
                                                                     repetir_cada_semanas, s.fecha_inicio, s.fecha_fin,
                                                                     null estudio_id, i.semestre_id, null asignatura_id,
                                                                     i.grupo_id, i.tipo_subgrupo_id, i.subgrupo_id,
                                                                     i.dia_semana_id, tipo_dia
                                                                from hor_v_fechas_estudios s,
                                                                     (select p_id id, p_detalle_manual detalle_manual,
                                                                             p_dia_semana_id dia_semana_id,
                                                                             p_semestre_id semestre_id,
                                                                             p_numero_iteraciones numero_iteraciones,
                                                                             p_repetir_cada_semanas
                                                                                                   repetir_cada_semanas,
                                                                             p_hasta_el_dia hasta_el_dia,
                                                                             p_desde_el_dia desde_el_dia,
                                                                             p_subgrupo_id subgrupo_id,
                                                                             p_tipo_subgrupo_id tipo_subgrupo_id,
                                                                             p_grupo_id grupo_id
                                                                        from dual) i,
                                                                     hor_ext_calendario c
                                                               where i.semestre_id = s.semestre_id
                                                                 and trunc (c.fecha) between fecha_inicio
                                                                                         and decode (fecha_examenes_fin,
                                                                                                     null, fecha_fin,
                                                                                                     fecha_examenes_fin
                                                                                                    )
                                                                 and c.dia_semana_id = i.dia_semana_id
                                                                 and tipo_dia in ('L', 'E', 'F')
                                                                 and s.tipo_estudio_id = 'G'
                                                                 and vacaciones = 0
                                                                 and detalle_manual = 1
                                                                 and s.estudio_id in (
                                                                          select estudio_id
                                                                            from hor_items_asignaturas
                                                                           where item_id = i.id
                                                                             and curso_id = s.curso_id)) c,
                                                    hor_items_detalle d
                                              where c.id = d.item_id(+)
                                                and trunc (c.fecha) = trunc (d.inicio(+)))
                             where id = p_id
                               and docencia = 'S') todo
         order by 18;

      function buscar_item (p_item_id in number, p_orden in number)
         return number is
         v_aux   number;

         cursor lista_detalle_ordenado is
            select id, row_number () over (partition by item_id order by trunc (inicio)) orden
              from hor_items_detalle
             where item_id = p_item_id;
      begin
         v_aux := -1;

         for det in lista_detalle_ordenado loop
            if det.orden = p_orden then
               v_aux := det.id;
            end if;
         end loop;

         return v_aux;
      exception
         when no_data_found then
            return (-1);
         when others then
            return (0);
      end;
   begin
      for i in 1 .. mutante_items.v_num loop
         for v_reg in reg (mutante_items.v_var_tabla (i)) loop
            if v_reg.detalle_manual = 0 then
               v_hora_ini := v_Reg.hora_inicio;
               v_hora_fin := v_Reg.hora_fin;

               for x in lista_detalle (v_reg.id, v_reg.detalle_manual, v_reg.dia_semana_id, v_reg.semestre_id,
                                       v_reg.numero_iteraciones, v_reg.repetir_cada_Semanas, v_reg.hasta_el_dia,
                                       v_reg.desde_el_dia, v_reg.subgrupo_id, v_reg.tipo_subgrupo_id, v_reg.grupo_id) loop
                  v_id := buscar_item (v_Reg.id, x.orden_final);

                  --dbms_output.put_line (v_Reg.id || ' ' || to_char (x.fecha, 'dd/mm/yyyyy') || ' ' || x.orden_final);
                  if v_id > 0 then
                     /* por defecto actualiza fechas */
                     --dbms_output.put_line ('update ' || x.orden_final);
                     update hor_items_detalle
                     set inicio =
                            to_Date (to_char (x.fecha, 'dd/mm/yyyy') || ' ' || to_char (v_hora_ini, 'hh24:mi:ss'),
                                     'dd/mm/yyyy hh24:mi:ss'),
                         fin =
                            to_Date (to_char (x.fecha, 'dd/mm/yyyy') || ' ' || to_char (v_hora_fin, 'hh24:mi:ss'),
                                     'dd/mm/yyyy hh24:mi:ss')
                     where  id = v_id;
                  elsif v_id = -1 then
                     --dbms_output.put_line ('insertar ' || x.orden_final);
                     begin
                        /* solo inserta si no le quedan fechas por actualizar */
                        v_aux := uji_horarios.hibernate_sequence.nextval;

                        insert into hor_items_detalle
                                    (id, item_id,
                                     inicio,
                                     fin
                                    )
                        values      (v_aux, v_Reg.id,
                                     to_date (to_char (x.fecha, 'dd/mm/yyyy') || ' '
                                              || to_char (v_hora_ini, 'hh24:mi:ss'),
                                              'dd/mm/yyyy hh24:mi:ss'),
                                     to_date (to_char (x.fecha, 'dd/mm/yyyy') || ' '
                                              || to_char (v_hora_fin, 'hh24:mi:ss'),
                                              'dd/mm/yyyy hh24:mi:ss')
                                    );
                     exception
                        when others then
                           null;
                     end;
                  end if;

                  if x.orden_id > nvl (v_ult_orden, 0) then
                     v_ult_orden := x.orden_final;
                  end if;
               end loop;

               /* y solo borra si sobran al final */
               delete      hor_items_detalle
               where       id in (
                              select id
                                from (select id, inicio, fin,
                                             row_number () over (partition by item_id order by trunc (inicio)) orden
                                        from hor_items_detalle
                                       where item_id = v_reg.id)
                               where orden > v_ult_orden);
            end if;
         end loop;
      end loop;
   end;
end mutante_3_final_2014;


CREATE OR REPLACE FORCE VIEW UJI_HORARIOS.HOR_V_ITEMS_DETALLE (ID,
                                                               FECHA,
                                                               DOCENCIA_PASO_1,
                                                               DOCENCIA_PASO_2,
                                                               DOCENCIA,
                                                               ORDEN_ID,
                                                               NUMERO_ITERACIONES,
                                                               REPETIR_CADA_SEMANAS,
                                                               FECHA_INICIO,
                                                               FECHA_FIN,
                                                               ESTUDIO_ID,
                                                               SEMESTRE_ID,
                                                               ASIGNATURA_ID,
                                                               GRUPO_ID,
                                                               TIPO_SUBGRUPO_ID,
                                                               SUBGRUPO_ID,
                                                               DIA_SEMANA_ID,
                                                               TIPO_DIA,
                                                               FESTIVOS
                                                              ) AS
   SELECT i.id, fecha, docencia docencia_paso_1,
          DECODE (NVL (d.repetir_cada_semanas, 1),
                  1, docencia,
                  DECODE (MOD (orden_id, d.repetir_cada_semanas), 1, docencia, 'N')
                 ) docencia_paso_2,
          decode (tipo_dia,
                  'F', 'N',
                  DECODE (d.numero_iteraciones,
                          NULL, DECODE (NVL (d.repetir_cada_semanas, 1),
                                        1, docencia,
                                        DECODE (MOD (orden_id, d.repetir_cada_semanas), 1, docencia, 'N')
                                       ),
                          DECODE (SIGN (((orden_id - festivos) / d.repetir_cada_Semanas) - (d.numero_iteraciones)),
                                  1, 'N',
                                  DECODE (NVL (d.repetir_cada_semanas, 1),
                                          1, docencia,
                                          DECODE (MOD (orden_id, d.repetir_cada_semanas), 1, docencia, 'N')
                                         )
                                 )
                         )
                 ) docencia,
          d.orden_id, d.numero_iteraciones, d.repetir_cada_semanas, d.fecha_inicio, d.fecha_fin, d.estudio_id,
          d.semestre_id, d.asignatura_id, d.grupo_id, d.tipo_subgrupo_id, d.subgrupo_id, d.dia_semana_id, tipo_dia,
          festivos
     FROM (SELECT id, fecha,
                  hor_contar_festivos (NVL (x.desde_el_dia, fecha_inicio), x.fecha, x.dia_semana_id,
                                       x.repetir_cada_semanas) festivos,
                  ROW_NUMBER () OVER (PARTITION BY DECODE
                                                       (decode (sign (x.fecha - fecha_inicio),
                                                                -1, 'N',
                                                                decode (sign (fecha_fin - x.fecha), -1, 'N', 'S')
                                                               ),
                                                        'S', DECODE (decode (sign (x.fecha
                                                                                   - NVL (x.desde_el_dia, fecha_inicio)),
                                                                             -1, 'N',
                                                                             decode (sign (NVL (x.hasta_el_dia,
                                                                                                fecha_fin)
                                                                                           - x.fecha),
                                                                                     -1, 'N',
                                                                                     'S'
                                                                                    )
                                                                            ),
                                                                     'S', 'S',
                                                                     'N'
                                                                    ),
                                                        'N'
                                                       ), id, estudio_id, semestre_id, asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo_id, dia_semana_id ORDER BY fecha)
                                                                                                               orden_id,
                  DECODE (hor_f_fecha_entre (x.fecha, fecha_inicio, fecha_fin),
                          'S', DECODE (hor_f_fecha_entre (x.fecha, NVL (desde_el_dia, fecha_inicio),
                                                          NVL (hasta_el_dia, fecha_fin)),
                                       'S', 'S',
                                       'N'
                                      ),
                          'N'
                         ) docencia,
                  estudio_id, semestre_id, grupo_id, tipo_subgrupo_id, subgrupo_id, dia_Semana_id, asignatura_id,
                  fecha_inicio, fecha_fin, fecha_examenes_inicio, fecha_examenes_fin, desde_el_dia, hasta_el_dia,
                  repetir_cada_semanas, numero_iteraciones, detalle_manual, tipo_dia, dia_semana
             FROM (SELECT distinct i.id, null estudio_id, i.semestre_id, i.grupo_id, i.tipo_subgrupo_id, i.subgrupo_id,
                          i.dia_Semana_id, null asignatura_id, fecha_inicio, fecha_fin, fecha_examenes_inicio,
                          fecha_examenes_fin, i.desde_el_dia, hasta_el_dia, repetir_cada_semanas, numero_iteraciones,
                          detalle_manual, c.fecha, tipo_dia, dia_semana
                     FROM hor_v_fechas_estudios s,
                          hor_items i,
                          hor_ext_calendario c
                    WHERE i.semestre_id = s.semestre_id
                      AND trunc (c.fecha) BETWEEN fecha_inicio AND NVL (fecha_examenes_fin, fecha_fin)
                      AND c.dia_semana_id = i.dia_semana_id
                      AND tipo_dia IN ('L', 'E', 'F')
                      and vacaciones = 0
                      and s.tipo_estudio_id = 'G'
                      AND detalle_manual = 0
                      and s.estudio_id in (select estudio_id
                                             from hor_items_asignaturas
                                            where item_id = i.id
                                              and curso_id = s.curso_id)
                                                                        --and i.id = 559274
                  ) x) d,
          hor_items i
    WHERE i.semestre_id = d.semestre_id
      AND i.grupo_id = d.grupo_id
      AND i.tipo_subgrupo_id = d.tipo_subgrupo_id
      AND i.subgrupo_id = d.subgrupo_id
      AND i.dia_semana_id = d.dia_semana_id
      AND i.detalle_manual = 0
      AND i.id = d.id
   UNION ALL
   SELECT c.id, c.fecha, 'N' docencia_paso_1, 'N' docencia_paso_2, DECODE (d.id, NULL, 'N', 'S') docencia, 1 orden_id,
          numero_iteraciones, repetir_cada_semanas, fecha_inicio, fecha_fin, estudio_id, semestre_id, asignatura_id,
          grupo_id, tipo_subgrupo_id, subgrupo_id, dia_semana_id, tipo_dia, decode (tipo_dia, 'F', 1, 0) festivos
     FROM (SELECT distinct i.id, c.fecha, numero_iteraciones, repetir_cada_semanas, s.fecha_inicio, s.fecha_fin,
                           null estudio_id, i.semestre_id, null asignatura_id, i.grupo_id, i.tipo_subgrupo_id,
                           i.subgrupo_id, i.dia_semana_id, tipo_dia
                      FROM hor_v_fechas_estudios s,
                           hor_items i,
                           hor_ext_calendario c
                     WHERE i.semestre_id = s.semestre_id
                       AND trunc (c.fecha) BETWEEN fecha_inicio AND NVL (fecha_examenes_fin, fecha_fin)
                       AND c.dia_semana_id = i.dia_semana_id
                       AND tipo_dia IN ('L', 'E', 'F')
                       and s.tipo_estudio_id = 'G'
                       and vacaciones = 0
                       AND detalle_manual = 1
                       and s.estudio_id in (select estudio_id
                                              from hor_items_asignaturas
                                             where item_id = i.id
                                               and curso_id = s.curso_id)) c,
          hor_items_detalle d
    WHERE c.id = d.item_id(+)
      AND trunc (c.fecha) = trunc (d.inicio(+));

CREATE OR REPLACE TRIGGER UJI_HORARIOS.mutante_2_por_fila
   after insert or update of dia_semana_id,
                             desde_el_dia,
                             hasta_el_dia,
                             repetir_cada_semanas,
                             numero_iteraciones,
                             hora_inicio,
                             hora_fin
   ON UJI_HORARIOS.HOR_ITEMS
   referencing old as new new as old
   for each row
begin
   mutante_items.v_num := mutante_items.v_num + 1;
   mutante_items.v_var_tabla (mutante_items.v_num) := :new.rowid;
end;

