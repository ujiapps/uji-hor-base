CREATE OR REPLACE FORCE VIEW UJI_HORARIOS.HOR_V_AGRUPACIONES_ITEMS (ID,
                                                                    AGRUPACION_ID,
                                                                    NOMBRE_AGRUPACION,
                                                                    ASIGNATURA_ID,
                                                                    ESTUDIO_ID,
                                                                    CURSO_ID,
                                                                    CARACTER_ID,
                                                                    ITEM_ID
                                                                   ) AS
   select distinct a.id || agrupacion_id || estudio_id || item_id id, a.id agrupacion_id, nombre nombre_agrupacion,
                   aa.asignatura_id, estudio_id, ia.curso_id, ia.caracter_id, item_id      /*, i.semestre_id,
                    aula_planificacion_id, comun, porcentaje_comun, grupo_id, i.tipo_subgrupo_id, tipo_subgrupo,
                    i.subgrupo_id, dia_semana_id, hora_inicio, hora_fin, desde_el_dia, hasta_el_dia, tipo_asignatura_id,
                    tipo_asignatura, tipo_estudio_id, tipo_estudio, plazas, repetir_cada_semanas, numero_iteraciones,
                    detalle_manual, comun_texto, aula_planificacion_nombre, creditos */
              from hor_agrupaciones a,
                   hor_agrupaciones_asignaturas aa,
                   hor_items_asignaturas ia,
                   hor_items i
             where a.id = aa.agrupacion_id
               and aa.asignatura_id = ia.asignatura_id
               and ia.item_id = i.id
               and nvl (aa.tipo_subgrupo_id, i.tipo_subgrupo_id) = i.tipo_subgrupo_id
               and nvl (aa.subgrupo_id, i.subgrupo_id) = i.subgrupo_id;
