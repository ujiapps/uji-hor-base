/* Formatted on 10/05/2016 17:27 (Formatter Plus v4.8.8) */
declare
   v_cuantos       number;
   v_id            number;
   v_id_det        number;
   v_aula_nombre   varchar2 (2000);
   v_comun_texto   varchar2 (2000);
   v_semanas_s1    number;
   v_semanas_s2    number;
   v_semana1_s1    number;
   v_semana1_s2    number;
   v_sesiones      number;
   v_control       number          := 0;

   cursor lista_items_sin_planificar is

      select   x.*, null sesiones, null fecha_inicio, null semana
      from     (select asignatura_id, asignatura, estudio_id, estudio, tipo_estudio_id, te.nombre tipo_estudio,
                       curso_id, caracter_id, cr.nombre caracter, semestre_id, null aula_planificacion_id,
                       persona_id profesor_id, grupo_id, tipo_subgrupo_id, tsg.nombre tipo_subgrupo, subgrupo_id,
                       dia_semana_id, hora_inicio, hora_fin, null desde_el_dia, null hasta_el_dia, 0 detalle_manual,
                       tipo_asignatura_id, ta.nombre tipo_asignatura, comun, porcentaje_comun, plazas, creditos
                from   (select *
                        from   (select 0 id, asit.tit_id estudio_id, t.nombre estudio, t.tipo_estudio tipo_estudio_id,
                                       cur_id curso_id, grp_asi_id asignatura_id, caracter caracter_id, grp_id grupo_id,
                                       sem.semestre semestre_id, grp_curso_aca curso_aca, s.tipo tipo_subgrupo_id,
                                       s.id subgrupo_id, null dia_semana_id, null hora_inicio, null hora_fin,
                                       null persona_id, null ubicacion_id, null compartido, a.tipo tipo_asignatura_id,
                                       a.nombre asignatura,
                                       (select decode (count (*), 0, 0, 1)
                                        from   pod_comunes com,
                                               pod_grp_comunes gc
                                        where  gc.id = com.gco_id
                                        and    curso_aca = ac.curso_aca
                                        and    asi_id = a.id) comun,
                                       (select porcentaje
                                        from   pod_comunes com,
                                               pod_grp_comunes gc
                                        where  gc.id = com.gco_id
                                        and    curso_aca = ac.curso_aca
                                        and    asi_id = a.id) porcentaje_comun,
                                       s.limite_nuevos plazas, (select decode (decode (s.tipo, 'TE', 1, 'PR', 2, 'LA', 3, 'SE', 4, 'TU', 5, 6),
                                  1, crd_Te,
                                  2, crd_pr,
                                  3, crd_la,
                                  4, crd_Se,
                                  5, crd_tu,
                                  6, crd_ev
                                 ) creditos
                   from   pod_vsp_27
                   where  curso_aca = g.curso_aca
                   and    asi_id = g.asi_id
                   and    tit_id = t.id) CREDITOS
                                from   pod_grupos g,
                                       pod_subgrupos s,
                                       pod_asignaturas_titulaciones asit,
                                       pod_asi_cursos ac,
                                       pod_asignaturas a,
                                       pod_titulaciones t,
                                       (select 'S' tipo, 1 semestre
                                        from   dual
                                        union all
                                        select 'S' tipo, 2 semestre
                                        from   dual
                                        union all
                                        select 'A' tipo, 1 semestre
                                        from   dual
                                        union all
                                        select 'A' tipo, 2 semestre
                                        from   dual) sem
                                where  g.curso_aca = s.grp_curso_aca
                                and    g.id = s.grp_id
                                and    g.asi_id = s.grp_asi_id
                                and    s.grp_asi_id = asit.asi_id
                                and    grp_curso_aca = (select id
                                                        from   uji_horarios.hor_curso_academico)
                                and    grp_curso_aca = ac.curso_aca
                                and    grp_asi_id = ac.asi_id
                                and    asit.tit_id = ac.cur_tit_id
                                and    asit.tit_id between 201 and 9999
                                and    asit.tit_id = t.id
                                and    g.asi_id = a.id
                                and    a.tipo = sem.tipo
                                and    g.semestre = sem.semestre)
                        where  (estudio_id,
                                estudio,
                                tipo_estudio_id,
                                curso_id,
                                asignatura_id,
                                caracter_id,
                                grupo_id,
                                semestre_id,
                                tipo_subgrupo_id,
                                subgrupo_id
                               ) not in (
                                  select estudio_id, estudio, tipo_estudio_id, curso_id, asignatura_id, caracter_id,
                                         grupo_id, semestre_id, tipo_subgrupo_id, subgrupo_id
                                  from   uji_horarios.hor_items i,
                                         uji_horarios.hor_items_asignaturas a
                                  where  i.id = a.item_id)) i,
                       (select 'AV' id, 'Avaluaci�' nombre, 7 orden
                        from   dual
                        union all
                        select 'LA' id, 'Laboratori' nombre, 4 orden
                        from   dual
                        union all
                        select 'PR' id, 'Problemes' nombre, 3 orden
                        from   dual
                        union all
                        select 'SE' id, 'Seminari' nombre, 5 orden
                        from   dual
                        union all
                        select 'TE' id, 'Teoria' nombre, 1 orden
                        from   dual
                        union all
                        select 'TP' id, 'Teoria i problemes' nombre, 2 orden
                        from   dual
                        union all
                        select 'TU' id, 'Tutories' nombre, 6 orden
                        from   dual) tsg,
                       (select 'S' id, 'Semestral' nombre, 1 orden
                        from   dual
                        union all
                        select 'A' id, 'Anual' nombre, 2 orden
                        from   dual) ta,
                       (select 'TR' id, 'Troncal' nombre, 1 orden
                        from   dual
                        union all
                        select 'FB' id, 'Formaci� b�sica' nombre, 2 orden
                        from   dual
                        union all
                        select 'OB' id, 'Obligatoria' nombre, 3 orden
                        from   dual
                        union all
                        select 'OP' id, 'Optativa' nombre, 4 orden
                        from   dual
                        union all
                        select 'LC' id, 'Lliure configuraci�' nombre, 5 orden
                        from   dual
                        union all
                        select 'PR' id, 'Pr�ctiques externes' nombre, 6 orden
                        from   dual
                        union all
                        select 'PF' id, 'Treball fi de grau' nombre, 7 orden
                        from   dual) cr,
                       (select '12C' id, 'Primer i segon cicle' nombre, 2 orden
                        from   dual
                        union all
                        select 'G' id, 'Grau' nombre, 1 orden
                        from   dual
                        union all
                        select 'M' id, 'M�ster' nombre, 3 orden
                        from   dual) te
                where  tipo_subgrupo_id = tsg.id
                and    tipo_asignatura_id = ta.id
                and    caracter_id = cr.id
                and    tipo_estudio_id = te.id) x
      where    1 = 1
      --and    asignatura_id like ('EI1002%')
      and      asignatura_id in
                  ('CA0918', 'EA0906', 'EA0906', 'EI1002', 'EI1002', 'EI1003', 'EI1003', 'EI1003', 'EI1004', 'EI1004',
                   'EI1004', 'EI1008', 'EI1008', 'EI1008', 'EI1010', 'EI1010', 'EI1010', 'EI1013', 'EI1014', 'EI1015',
                   'EI1015', 'EI1017', 'EI1017', 'EI1017', 'EI1018', 'EI1018', 'EI1018', 'EI1019', 'EI1019', 'EI1019',
                   'EI1019', 'EI1020', 'EI1022', 'EI1022', 'EI1022', 'EI1022', 'EI1023', 'EI1027', 'EI1027', 'EI1028',
                   'EI1031', 'EI1039', 'EI1048', 'EI1049', 'EI1050', 'EI1055', 'ET1032', 'MI1013', 'MI1014', 'MI1016',
                   'MI1016', 'MT1002', 'MT1002', 'MT1003', 'MT1003', 'MT1003', 'MT1004', 'MT1004', 'MT1004', 'MT1008',
                   'MT1008', 'MT1008', 'MT1010', 'MT1010', 'MT1010', 'MT1015', 'MT1015', 'MT1019', 'MT1019', 'MT1019',
                   'MT1019', 'MT1020', 'MT1022', 'MT1022', 'MT1022', 'MT1022', 'MT1028', 'PE0918', 'PU0918', 'QU0908',
                   'QU0908', 'QU0912', 'QU0913', 'QU0914', 'QU0915', 'QU0917', 'QU0918', 'QU0919', 'QU0920', 'QU0920',
                   'QU0921', 'QU0921', 'QU0922', 'QU0923', 'QU0923', 'QU0924', 'QU0925', 'QU0926', 'QU0926', 'QU0927',
                   'QU0928', 'QU0929', 'QU0929', 'QU0930', 'QU0930', 'QU0931', 'QU0932', 'TI0906', 'TI0906', 'TI0922',
                   'TI0922', 'TI0925', 'TI0925', 'TI0925', 'TI0925', 'TI0925', 'TU0910', 'TU0910', 'TU0910', 'VJ1202',
                   'VJ1203', 'VJ1203', 'VJ1203', 'VJ1208', 'VJ1208', 'VJ1208', 'VJ1215', 'VJ1216', 'VJ1217', 'MI1016',
                   'HU1041', 'HU1041', 'HU1041', 'HU1041', 'HU1041', 'VJ1240', 'VJ1241', 'AG1003', 'EE1003', 'EM1003',
                   'EQ1003', 'ET1003', 'EI1014', 'MI1014', 'EI1054', 'MI1016')
      /*
          and      asignatura_id not like ('EI%')
          and      2 = 2
          and      curso_id < 7
          and      caracter_id not in ('PF', 'LC')
          and      tipo_subgrupo_id not in ('AV', 'TU')
            */
      and      2 = 2
      and      curso_id < 7
      order by 1;
      
begin
   v_control := 1;

   select numero_Semanas, to_number (to_char (fecha_inicio, 'ww'))
   into   v_semanas_s1, v_semana1_s1
   from   uji_horarios.hor_semestres_detalle
   where  tipo_estudio_id = 'G'
   and    semestre_id = 1;

   select numero_Semanas, to_number (to_char (fecha_inicio, 'ww'))
   into   v_semanas_s2, v_semana1_s2
   from   uji_horarios.hor_semestres_detalle
   where  tipo_estudio_id = 'G'
   and    semestre_id = 2;

   v_control := 2;

   for x in lista_items_sin_planificar loop
      v_control := 3;

      select count (*)
      into   v_cuantos
      from   uji_horarios.hor_items i,
             uji_horarios.hor_items_asignaturas a,
             uji_horarios.hor_ext_asignaturas_comunes c
      where  curso_id = x.curso_id
      and    caracter = x.caracter
      and    semestre_id = x.semestre_id
      and    grupo_id = x.grupo_id
      and    tipo_subgrupo_id = x.tipo_subgrupo_id
      and    subgrupo_id = x.subgrupo_id
      and    dia_semana_id = x.dia_semana_id
      and    hora_inicio = x.hora_inicio
      and    i.id = a.item_id
      and    a.asignatura_id = c.asignatura_id
      and    nombre like '%' || x.asignatura_id || '%';

      v_control := 4;

      if    v_cuantos = 0
         or x.comun = 0 then
         v_id := uji_horarios.hibernate_sequence.nextval;
         v_control := 5;

         if x.aula_planificacion_id is null then
            v_aula_nombre := null;
         else
            begin
               select nombre
               into   v_aula_nombre
               from   uji_horarios.hor_aulas
               where  id = x.aula_planificacion_id;
            exception
               when no_data_found then
                  v_aula_nombre := null;
            end;
         end if;

         v_control := 6;

         if x.comun = 0 then
            v_comun_texto := null;
         else
            begin
               select nombre
               into   v_comun_texto
               from   uji_horarios.hor_ext_asignaturas_comunes
               where  asignatura_id = x.asignatura_id;
            exception
               when no_data_found then
                  v_comun_texto := null;
            end;
         end if;

         v_control := 7;
               v_sesiones := null;

         v_control := 8;

         insert into uji_horarios.hor_items
                     (id, semestre_id, aula_planificacion_id,
                      comun, porcentaje_comun, grupo_id, tipo_subgrupo_id, tipo_subgrupo,
                      subgrupo_id, dia_semana_id, hora_inicio, hora_fin, desde_el_dia, hasta_el_dia,
                      tipo_asignatura_id, tipo_asignatura, tipo_estudio_id, tipo_estudio, plazas, repetir_cada_semanas,
                      numero_iteraciones, detalle_manual, comun_texto, aula_planificacion_nombre, creditos
                     )
         values      (v_id, x.semestre_id, x.aula_planificacion_id,
                      x.comun, x.porcentaje_comun, x.grupo_id, x.tipo_subgrupo_id, x.tipo_subgrupo,
                      x.subgrupo_id, x.dia_semana_id, x.hora_inicio, x.hora_fin, x.desde_el_dia, x.hasta_el_dia,
                      x.tipo_asignatura_id, x.tipo_asignatura, x.tipo_estudio_id, x.tipo_estudio, x.plazas, 1,
                      v_sesiones, x.detalle_manual, v_comun_texto, v_aula_nombre, x.creditos
                     );

         v_control := 9;
      else
         v_control := 10;

         /*dbms_output.put_line (x.curso_id || ' ' || x.caracter || ' ' || x.semestre_id || ' ' || x.grupo_id
                               || ' ' || x.tipo_subgrupo_id || ' ' || x.subgrupo_id || ' ' || x.dia_semana_id
                               || ' ' || to_char (x.hora_inicio, 'dd/mm/yyyy hh24:mi') || ' ' || x.comun || ' '
                               || x.asignatura_id || ' ' || x.estudio_id);*/

         /*select id
         into   v_id
         from   uji_horarios.hor_items
         where  curso_id = x.curso_id
         and    caracter = x.caracter
         and    semestre_id = x.semestre_id
         and    grupo_id = x.grupo_id
         and    tipo_subgrupo_id = x.tipo_subgrupo_id
         and    subgrupo_id = x.subgrupo_id
         and    dia_semana_id = x.dia_semana_id
         and    hora_inicio = x.hora_inicio;
         */
         select distinct i.id
         into            v_id
         from            uji_horarios.hor_items i,
                         uji_horarios.hor_items_asignaturas a,
                         uji_horarios.hor_ext_asignaturas_comunes c
         where           curso_id = x.curso_id
         and             caracter = x.caracter
         and             semestre_id = x.semestre_id
         and             grupo_id = x.grupo_id
         and             tipo_subgrupo_id = x.tipo_subgrupo_id
         and             subgrupo_id = x.subgrupo_id
         and             dia_semana_id = x.dia_semana_id
         and             hora_inicio = x.hora_inicio
         and             i.id = a.item_id
         and             a.asignatura_id = c.asignatura_id
         and             nombre like '%' || x.asignatura_id || '%';

         v_control := 11;
      end if;

      v_control := 12;
      v_id_det := uji_horarios.hibernate_sequence.nextval;

      insert into uji_horarios.hor_items_asignaturas
                  (id, item_id, asignatura_id, asignatura, estudio_id, estudio, curso_id, caracter_id, caracter
                  )
      values      (v_id_det, v_id, x.asignatura_id, x.asignatura, x.estudio_id, x.estudio, x.curso_id, x.caracter_id, x.caracter
                  );

      commit;
      v_control := 13;

   end loop;
exception
   when others then
      dbms_output.put_line (v_control || ' ' || sqlerrm);
end;