/* Formatted on 28/02/2014 14:30 (Formatter Plus v4.8.8) */
CREATE OR REPLACE PROCEDURE JIDBA.generar_uji_horarios (p_parte in number) IS
   p_curso_aca   number;
BEGIN
   select id
   into   p_curso_aca
   from   uji_horarios.hor_curso_academico;

   if p_parte = 0 then
	    delete      uji_horarios.HOR_ITEMS_CIRCUITOS;

	   delete      uji_horarios.HOR_ITEMS_COMUNES;

	   delete      uji_horarios.HOR_ITEMS_ASIGNATURAS;

	   delete      uji_horarios.HOR_ITEMS_DET_PROFESORES;

	   delete      uji_horarios.HOR_ITEMS_PROFESORES;

	   delete      uji_horarios.HOR_ITEMS_DETALLE;

	   delete      uji_horarios.HOR_ITEMS;

	   delete      uji_horarios.HOR_EXAMENES_ASIGNATURAS;

	   delete      uji_horarios.HOR_EXAMENES_AULAS;

	   delete      uji_horarios.HOR_EXAMENES;


      --delete      uji_horarios.HOR_PERMISOS_EXTRA;
      delete      uji_horarios.HOR_CIRCUITOS_ESTUDIOS;

      delete      uji_horarios.HOR_CIRCUITOS;

      delete      uji_horarios.HOR_HORARIOS_HORAS;

      delete      uji_horarios.HOR_AULAS_PLANIFICACION;
	  
	        delete      uji_horarios.HOR_PROFESORES;
      delete      uji_horarios.HOR_AREAS;
      delete      uji_horarios.HOR_DEPARTAMENTOS;


      delete      uji_horarios.hor_permisos_extra;

      /*
      delete      uji_horarios.HOR_EXT_CALENDARIO;

      delete      uji_horarios.HOR_DIAS_SEMANA;

      delete      uji_horarios.HOR_PROFESORES;

      delete      uji_horarios.HOR_SEMESTRES_DETALLE;

      delete      uji_horarios.HOR_SEMESTRES;

      delete      uji_horarios.HOR_AREAS;

      delete      uji_horarios.HOR_DEPARTAMENTOS;

      delete      uji_horarios.HOR_ESTUDIOS;

      delete      uji_horarios.HOR_CURSO_ACADEMICO;

      delete      uji_horarios.HOR_AULAS;

      delete      uji_horarios.HOR_CENTROS;

      delete      uji_horarios.HOR_TIPOS_CARGOS;
	  
	 delete uji_horarios.hor_tipos_examenes;

      delete      uji_horarios.HOR_TIPOS_ESTUDIOS;
      */
      commit;
      dbms_output.put_line ('Borrados los datos');
   end if;

   if p_parte = 1 then
      -- ==> hor_dias_semana
      insert into uji_horarios.hor_dias_semana
         select   dia id, decode (dia, 1, 'Dilluns', 2, 'Dimarts', 3, 'Dimecres', 4, 'Dijous', 5, 'Divendres') nombre
         from     (select to_char (sysdate, 'd') dia, to_char (sysdate, 'Day') dia_txt_es
                   from   dual
                   union all
                   select to_char (sysdate + 1, 'd'), to_char (sysdate + 1, 'Day')
                   from   dual
                   union all
                   select to_char (sysdate + 2, 'd'), to_char (sysdate + 2, 'Day')
                   from   dual
                   union all
                   select to_char (sysdate + 3, 'd'), to_char (sysdate + 3, 'Day')
                   from   dual
                   union all
                   select to_char (sysdate + 4, 'd'), to_char (sysdate + 4, 'Day')
                   from   dual
                   union all
                   select to_char (sysdate + 5, 'd'), to_char (sysdate + 5, 'Day')
                   from   dual
                   union all
                   select to_char (sysdate + 6, 'd'), to_char (sysdate + 6, 'Day')
                   from   dual)
         where    dia between 1 and 5
         order by 1;

      commit;
      dbms_output.put_line ('hor_dias_semana');

-- ==> hor_semestres
      insert into uji_horarios.hor_semestres
         select   1 id, 'Primer semestre' nombre
         from     dual
         union all
         select   2 id, 'Segon semestre' nombre
         from     dual
         order by 2;

      commit;
      dbms_output.put_line ('hor_semestres');

--==> hor_tipos_estudios
      insert into uji_horarios.hor_tipos_estudios
         select 'G' id, 'Graus' nombre, 1 orden
         from   dual;

      commit;
      dbms_output.put_line ('hor_tipos_estudios');
	  
--==> hor_tipos_examenes
Insert into UJI_HORARIOS.HOR_TIPOS_EXAMENES
   (ID, NOMBRE, POR_DEFECTO, CODIGO)
 Values
   (1, 'Teoria', 1, 'TEO');
Insert into UJI_HORARIOS.HOR_TIPOS_EXAMENES
   (ID, NOMBRE, POR_DEFECTO, CODIGO)
 Values
   (2, 'Problemes', 0, 'PRO');
Insert into UJI_HORARIOS.HOR_TIPOS_EXAMENES
   (ID, NOMBRE, POR_DEFECTO, CODIGO)
 Values
   (3, 'Laboratori', 0, 'LAB');
Insert into UJI_HORARIOS.HOR_TIPOS_EXAMENES
   (ID, NOMBRE, POR_DEFECTO, CODIGO)
 Values
   (4, 'Oral', 0, 'ORA');
Insert into UJI_HORARIOS.HOR_TIPOS_EXAMENES
   (ID, NOMBRE, POR_DEFECTO, CODIGO)
 Values
   (5, 'Pr�ctiques', 0, 'PRA');
COMMIT;

      dbms_output.put_line ('hor_tipos_examenes');


--==> hor_curso_academico
      insert into uji_horarios.hor_curso_academico
      values      (2013);

      commit;
      dbms_output.put_line ('hor_curso_academico');

--==> hor_semestres_detalle (falta fecha_examenes_inicio, fecha_examenes_fin)
      insert into uji_horarios.hor_semestres_detalle
         select   1 id, 1 semestre_id, 'G' tipo_estudio_id, ini_sem1_g fecha_inicio, fin_sem1_g fecha_fin, fin_sem1_g,
                  fin_sem1_g, round (((fin_sem1_g - ini_sem1_g) / 7 + 0.49999), 0) numero_semanas, cursos_aca
         from     pod_cursos_aca
         where    cursos_aca = (select id
                                from   uji_horarios.hor_curso_academico)
         union all
         select   2 id, 2 semestre_id, 'G' tipo_estudio_id, ini_sem2_g fecha_inicio, fin_sem2_g fecha_fin, fin_sem2_g,
                  fin_sem2_g, round (((fin_sem2_g - ini_sem2_g) / 7 + 0.49999), 0) numero_semanas, cursos_aca
         from     pod_cursos_aca
         where    cursos_aca = (select id
                                from   uji_horarios.hor_curso_academico)
         order by 1;

      commit;
      dbms_output.put_line ('hor_semestres_detalle');

--==> hor_centros
      insert into uji_horarios.hor_centros
         select id, nombre
         from   est_ubic_estructurales
         where  tuest_id = 'CE'
         or     id = 318
                        /*union
                        select 318 id, 'Incorrecte' nombre
                        from   dual */
      ;

      commit;
      dbms_output.put_line ('hor_centros');

--==> hor_estudios
      insert into uji_horarios.hor_estudios
         select   id, nombre, tipo_estudio tipo_id, uest_id centro_id,
                  decode (id, 51001, 0, 51002, 0, 51003, 0, 1) oficial, decode (id, 229, 6, 4) numero_cursos
         from     pod_titulaciones
         where    tipo_estudio in ('G')
         and      activa = 'S'
         -- and      id in (210, 211, 212)
         order by 1;

      commit;
      dbms_output.put_line ('hor_estudios');

   insert into uji_horarios.hor_departamentos
         select   id, nombre, centro_id, activo, 0 consulta_pod
         from     (select   uest.id, uest.nombre, uest_id centro_id, estado activo, trel_id,
                            row_number () over (partition by id order by trel_id desc) orden
                   from     est_estructuras e,
                            est_ubic_estructurales uest
                   where    uest_id_relacionad = uest.id
                   and      uest.id in (select ubicacion_id
                                        from   grh_vw_contrataciones_ult
                                        where  act_id = 'PDI')
                   and      id not in (720, 264, 2022, 3045)
                   order by 1)
         where    orden = 1
         order by 1;

      commit;
      dbms_output.put_line ('hor_departamentos');

--==> hor_areas
      insert into uji_horarios.hor_areas
         select uest.id, uest.nombre, uest_id departamento_id, estado activa
         from   est_estructuras e,
                est_ubic_estructurales uest
         where  estado = 1
         and    uest_id_relacionad = uest.id
         and    tuest_id = 'AC'
         and    status = 'A'
         and    uest_id in (
                   select uest.id
                   from   est_estructuras e,
                          est_ubic_estructurales uest
                   where  estado = 1
                   and    uest_id_relacionad = uest.id
                   and    tuest_id = 'DE'
                   and    status = 'A'
                   and    trel_id = 4)
         and    uest_id in (select id
                            from   uji_horarios.hor_departamentos)
  and    uest.id not in (select id
                          from   uji_horarios.hor_areas)
						  /*union all
         select 0 id, 'Desconeguda' nombre, 318 departamento_id, 0 activa
         from dual*/
        ;

      commit;
      dbms_output.put_line ('hor_areas');

--==> hor_profesores
       insert into       uji_horarios.hor_profesores   
  select   x.per_id, x.nombre, cuenta, area_id, ubicacion_id, por_contratar, creditos, cdo_max, reduccion, cod_grh, cod_grh_nombre
         from     (select c.per_id, p.apellido1 || ' ' || apellido2 || ', ' || p.nombre nombre,
                          busca_cuenta (c.per_id) cuenta,nvl (ubicacion_id,
                               (select uest_id
                                from   est_estructuras e
                                where  uest_id_relacionad = c.uest_id
                                and    trel_id = 2
                                and    estado = 1)) ubicacion_id,
                          decode (area_id, null, uest_id, area_id) area_id, cdo_max,
                          reduccion, cdo_max - reduccion creditos, decode (ubicacion_id, null, 1, 0) por_contratar,
                          cod_grh, cat.nombre cod_grh_nombre
                   from   gra_pod.pod_carga_docente c,
                          pod_categorias cat,
                          per_personas p,
                          (select *
                           from   grh_vw_contrataciones_ult
                           where  act_id = 'PDI') cont
                   where  curso_aca = 2014
                   and    c.per_id = p.id
                   and    c.per_id = cont.per_id(+)
                   and    cdo_id = cat.id
                   and    cdo_max > 0
                   and    nvl (ubicacion_id,
                               (select uest_id
                                from   est_estructuras e
                                where  uest_id_relacionad = c.uest_id
                                and    trel_id = 2
                                and    estado = 1)) in (select id
                                           from   uji_horarios.hor_departamentos)
                     ) x,
                  est_ubic_estructurales u
         where    ubicacion_id = u.id
         and x.per_id not in (select id from uji_horarios.hor_profesores)
         order by u.nombre,
                  ubicacion_id,
                  area_id,
                  2;
                  

      commit;
      dbms_output.put_line ('hor_profesores');

--==> hor_aulas
      /*insert into uji_horarios.hor_aulas
                  (id, nombre, centro_id, tipo, plazas, codigo, area, edificio, planta)
         select distinct u.id, u.nombre || ' - ' || codigo, nvl (u.centro_id, 0) centro_id, u.tipo_nombre tipo,
                         u.plazas, u.codigo, substr (codigo, 1, 1), substr (codigo, 2, 2), substr (codigo, 4, 1)
         from            (select 0 id, grc_cur_tit_id titulacion_id, grc_cur_id curso_id, sgr_grp_asi_id asignatura_id,
                                 caracter, sgr_grp_id grupo_id, to_number (semestre) semestre_id,
                                 sgr_grp_curso_aca curso_aca, sgr_tipo tipo_id, sgr_id subgrupo_id, ubi_id ubicacion_id
                          from   pod_horarios h,
                                 pod_circuitos_det cir,
                                 pod_asi_cursos ac
                          where  sgr_grp_curso_aca = p_curso_aca - 1
                          --and    sgr_grp_asi_id = 'AE1008'
                          --and    sgr_grp_id = 'A'
                          and    sgr_grp_curso_aca = ac.curso_aca
                          and    grc_cur_tit_id = ac.cur_tit_id
                          and    grc_cur_id = ac.cur_id
                          and    sgr_grp_asi_id = ac.asi_id
                          and    cur_tit_id between 201 and 9999
                          and    sgr_grp_curso_aca = sgd_sgr_grp_curso_aca(+)
                          and    sgr_grp_asi_id = sgd_sgr_grp_asi_id(+)
                          and    sgr_grp_id = SGD_SGR_GRP_ID(+)
                          and    sgr_tipo = SGD_SGR_TIPO(+)
                          and    sgr_id = SGD_SGR_ID(+)) x,
                         (select u.id id, u.descripcion nombre, u.id aula_id, uest_id centro_id, tubic_id tipo,
                                 num_alumnos plazas,
                                 edi_are_area || edi_edificio || planta || dependencia || tubic_id codigo,
                                 t.nombre tipo_nombre
                          from   est_ubicaciones u,
                                 est_areas_ubicacion a,
                                 est_tipos_ubicacion t
                          where  edi_are_area = a.area
                          and    u.tubic_id = t.id) u
         where           ubicacion_id is not null
         and             ubicacion_id = u.id(+)
         and             u.centro_id in (select id
                                         from   uji_horarios.hor_centros);
*/
      /* todas las aulas */
      insert into uji_horarios.hor_aulas
                  (id, nombre, centro_id, tipo, plazas, codigo, area, edificio, planta)
         select u.id id,
                u.descripcion || ' ' || edi_are_area || edi_edificio || planta || dependencia || tubic_id nombre,
                decode (edi_Are_area, 'M', 2922, 'D', 2, uest_id) centro_id, t.nombre tipo, num_alumnos plazas,
                edi_are_area || edi_edificio || planta || dependencia || tubic_id codigo, edi_are_area,
                edi_are_area || edi_edificio, planta
         from   est_ubicaciones u,
                est_areas_ubicacion a,
                est_tipos_ubicacion t
         where  edi_are_area = a.area
         and    u.tubic_id = t.id
         and    (   (    area in ('J', 'T', 'H', 'M', 'D')
                     and tubic_id in ('AA', 'AI', 'AL', 'DS', 'TA', 'TL', 'TS', 'CC', 'DL')
                    )
                 or (    area in ('D')
                     and tubic_id in ('AA', 'AI', 'AL', 'PA', 'PC', 'DS'))
                )
         and    u.descripcion <> 'BAJA'
         minus
         select id, nombre, centro_id, tipo, plazas, codigo, area, edificio, planta
         from   uji_horarios.hor_aulas;

      /* limpiar los codigos duplicados */
      declare
         cursor lista_repetidos is
            select   id, x.nombre, trim (substr (nombre, 1, repe - 1)) nombre_limpio
            from     (select a.*, instr (nombre, codigo, 1, 2) repe
                      from   uji_horarios.hor_aulas a) x
            where    repe > 0
            order by 2;
      begin
         for x in lista_repetidos loop
            update uji_horarios.hor_aulas
            set nombre = x.nombre_limpio
            where  id = x.id;

            commit;
         end loop;
      end;

      commit;
      dbms_output.put_line ('hor_aulas');

--==> hor_ext_calendario
      insert into uji_horarios.hor_ext_calendario
                  (id, dia, mes, a�o, tipo_dia, dia_semana, dia_semana_id, fecha)
         select   rownum id, dia, mes, a�o, tipo_aca tipo_dia, dia_semana,
                  decode (dia_semana,
                          'LUNES', 1,
                          'MARTES', 2,
                          'MI�RCOLES', 3,
                          'JUEVES', 4,
                          'VIERNES', 5,
                          'S�BADO', 6,
                          'DOMINGO', 7,
                          8
                         ) dia_Semana_id,
                  fecha_completa fecha
         from     grh_calendario c,
                  uji_horarios.hor_curso_academico ca
         where    fecha_completa between (select min (fecha_completa)
                                          from   grh_calendario
                                          where  a�o = ca.id
                                          and    semana = to_char (to_Date ('1/9/' || ca.id, 'dd/mm/yyyy'), 'ww'))
                                     and (select max (fecha_completa)
                                          from   grh_calendario
                                          where  a�o = ca.id + 1
                                          and    semana =
                                                    to_char (to_Date ('30/9/' || to_char (ca.id + 1), 'dd/mm/yyyy'),
                                                             'ww'))
         order by a�o,
                  mes,
                  dia;

      commit;
      dbms_output.put_line ('hor_ext_calendario');

      /*
            - CAL MARCAR LES VACANCES A 1
            CAL MARCAR ELS EXAMENS A E
          */

      --==> hor_tipos_cargos
      insert into uji_horarios.hor_tipos_cargos
         select 1 id, 'Director d''estudi' nombre
         from   dual
         union all
         select 2 id, 'Coordinador de curs' nombre
         from   dual
         union all
         select 3 id, 'Director, Dega o Secretari de Centre' nombre
         from   dual
         union all
         select 4 id, 'PAS de centre' nombre
         from   dual
         union all
         select 5 id, 'PAS de departament' nombre
         from   dual
         union all
         select 6 id, 'Director de departament' nombre
         from   dual
         union all
         select 0 id, 'Error' nombre
         from   dual;

      commit;
      dbms_output.put_line ('hor_tipos_cargos');
   end if;

   if p_parte = 2 then
--==> hor_items  KKKKKKKKKKKKKKK
      declare
         v_cuantos       number;
         v_id            number;
         v_id_det        number;
         v_aula_nombre   varchar2 (2000);
         v_comun_texto   varchar2 (2000);
         v_semanas_s1    number;
         v_semanas_s2    number;
         v_semana1_s1    number;
         v_semana1_s2    number;
         v_sesiones      number;
         v_control       number          := 0;

         cursor lista_items is
            select x.*, sesiones, fecha_inicio, to_number (to_char (fecha_inicio, 'ww')) semana
            from   (select asignatura_id, asignatura, estudio_id, estudio, tipo_estudio_id, te.nombre tipo_estudio,
                           curso_id, caracter_id, cr.nombre caracter, semestre_id, null aula_planificacion_id,
                           persona_id profesor_id, grupo_id, tipo_subgrupo_id, tsg.nombre tipo_subgrupo, subgrupo_id,
                           dia_semana_id, hora_inicio, hora_fin, null desde_el_dia, null hasta_el_dia, 0 detalle_manual,
                           tipo_asignatura_id, ta.nombre tipo_asignatura, comun, porcentaje_comun, plazas
                    from   (select 0 id, asit.tit_id estudio_id, t.nombre estudio, t.tipo_estudio tipo_estudio_id,
                                   cur_id curso_id, grp_asi_id asignatura_id, caracter caracter_id, grp_id grupo_id,
                                   sem.semestre semestre_id, grp_curso_aca curso_aca, s.tipo tipo_subgrupo_id,
                                   s.id subgrupo_id, null dia_semana_id, null hora_inicio, null hora_fin,
                                   null persona_id, null ubicacion_id, null compartido, a.tipo tipo_asignatura_id,
                                   a.nombre asignatura,
                                   (select decode (count (*), 0, 0, 1)
                                    from   pod_comunes com,
                                           pod_grp_comunes gc
                                    where  gc.id = com.gco_id
                                    and    curso_aca = ac.curso_aca
                                    and    asi_id = a.id) comun,
                                   (select porcentaje
                                    from   pod_comunes com,
                                           pod_grp_comunes gc
                                    where  gc.id = com.gco_id
                                    and    curso_aca = ac.curso_aca
                                    and    asi_id = a.id) porcentaje_comun,
                                   s.limite_nuevos plazas
                            from   pod_grupos g,
                                   pod_subgrupos s,
                                   pod_asignaturas_titulaciones asit,
                                   pod_asi_cursos ac,
                                   pod_asignaturas a,
                                   pod_titulaciones t,
                                   (select '1' tipo, 1 semestre
                                    from   dual
                                    union all
                                    select '2' tipo, 2 semestre
                                    from   dual
                                    union all
                                    select 'A' tipo, 1 semestre
                                    from   dual
                                    union all
                                    select 'A' tipo, 2 semestre
                                    from   dual) sem
                            where  g.curso_aca = s.grp_curso_aca
                            and    g.id = s.grp_id
                            and    g.asi_id = s.grp_asi_id
                            and    s.grp_asi_id = asit.asi_id
                            and    grp_curso_aca = (select id - 1
                                                    from   uji_horarios.hor_curso_academico)
                            and    grp_curso_aca = ac.curso_aca
                            and    grp_asi_id = ac.asi_id
                            and    asit.tit_id = ac.cur_tit_id
                            and    asit.tit_id between 201 and 9999
                            --and    tit_id not in (220, 221, 222, 227, 228, 225, 231)
                            --and    a.id in ('AG1002', 'EM1002', 'EI1002', 'EQ1002', 'ET1002')
                            and    asit.tit_id = t.id
                            and    g.asi_id = a.id
                            and    a.tipo = sem.tipo
                            and    not exists (
                                      select 1
                                      from   pod_horarios h
                                      where  s.grp_asi_id = sgr_grp_asi_id
                                      and    s.grp_id = sgr_grp_id
                                      and    s.grp_curso_aca = sgr_grp_curso_aca
                                      and    s.id = sgr_id
                                      and    s.tipo = sgr_tipo
                                      and    sem.semestre = semestre)
                            union
                            select 0 id, grc_cur_tit_id estudio_id, t.nombre estudio, t.tipo_estudio tipo_estudio_id,
                                   grc_cur_id curso_id, sgr_grp_asi_id asignatura_id, caracter caracter_id,
                                   sgr_grp_id grupo_id, to_number (semestre) semestre_id, sgr_grp_curso_aca curso_aca,
                                   sgr_tipo tipo_subgrupo_id, sgr_id subgrupo_id, dia_sem dia_semana_id,
                                   to_date ('1-1-1 ' || to_char (ini, 'hh24:mi'), 'dd-mm-yyyy hh24:mi') hora_inicio,
                                   to_date ('1-1-1 ' || to_char (fin, 'hh24:mi'), 'dd-mm-yyyy hh24:mi') hora_fin,
                                   per_id persona_id, ubi_id ubicacion_id, compartido, a.tipo tipo_asignatura_id,
                                   a.nombre asignatura,
                                   (select decode (count (*), 0, 0, 1)
                                    from   pod_comunes com,
                                           pod_grp_comunes gc
                                    where  gc.id = com.gco_id
                                    and    curso_aca = ac.curso_aca
                                    and    asi_id = a.id) comun,
                                   (select porcentaje
                                    from   pod_comunes com,
                                           pod_grp_comunes gc
                                    where  gc.id = com.gco_id
                                    and    curso_aca = ac.curso_aca
                                    and    asi_id = a.id) porcentaje_comun,
                                   s.limite_nuevos plazas
                            from   pod_horarios h,
                                   pod_asi_cursos ac,
                                   pod_asignaturas a,
                                   pod_titulaciones t,
                                   pod_subgrupos s
                            where  sgr_grp_curso_aca = (select id - 1
                                                        from   uji_horarios.hor_curso_academico)
                            and    sgr_grp_curso_aca = ac.curso_aca
                            and    grc_cur_tit_id = ac.cur_tit_id
                            and    grc_cur_id = ac.cur_id
                            and    sgr_grp_asi_id = ac.asi_id
                            and    cur_tit_id between 201 and 9999
                            --and    cur_tit_id in (220, 221, 222, 227, 228, 225, 231)
                            --and    a.id in ('AG1002', 'EM1002', 'EI1002', 'EQ1002', 'ET1002')
                            and    cur_tit_id = t.id
                            and    ac.asi_id = a.id
                            and    s.GRP_ASI_ID = h.sgr_grp_asi_id
                            and    s.GRP_ID = h.sgr_grp_id
                            and    s.GRP_CURSO_ACA = h.sgr_grp_curso_aca
                            and    s.ID = h.sgr_id
                            and    s.TIPO = h.sgr_tipo) i,
                           (select 'AV' id, 'Avaluaci�' nombre, 7 orden
                            from   dual
                            union all
                            select 'LA' id, 'Laboratori' nombre, 4 orden
                            from   dual
                            union all
                            select 'PR' id, 'Problemes' nombre, 3 orden
                            from   dual
                            union all
                            select 'SE' id, 'Seminari' nombre, 5 orden
                            from   dual
                            union all
                            select 'TE' id, 'Teoria' nombre, 1 orden
                            from   dual
                            union all
                            select 'TP' id, 'Teoria i problemes' nombre, 2 orden
                            from   dual
                            union all
                            select 'TU' id, 'Tutories' nombre, 6 orden
                            from   dual) tsg,
                           (select 'S' id, 'Semestral' nombre, 1 orden
                            from   dual
                            union all
                            select 'A' id, 'Anual' nombre, 2 orden
                            from   dual) ta,
                           (select 'TR' id, 'Troncal' nombre, 1 orden
                            from   dual
                            union all
                            select 'FB' id, 'Formaci� b�sica' nombre, 2 orden
                            from   dual
                            union all
                            select 'OB' id, 'Obligatoria' nombre, 3 orden
                            from   dual
                            union all
                            select 'OP' id, 'Optativa' nombre, 4 orden
                            from   dual
                            union all
                            select 'LC' id, 'Lliure configuraci�' nombre, 5 orden
                            from   dual
                            union all
                            select 'PR' id, 'Pr�ctiques externes' nombre, 6 orden
                            from   dual
                            union all
                            select 'PF' id, 'Treball fi de grau' nombre, 7 orden
                            from   dual) cr,
                           (select '12C' id, 'Primer i segon cicle' nombre, 2 orden
                            from   dual
                            union all
                            select 'G' id, 'Grau' nombre, 1 orden
                            from   dual
                            union all
                            select 'M' id, 'M�ster' nombre, 3 orden
                            from   dual) te
                    where  tipo_subgrupo_id = tsg.id
                    and    tipo_asignatura_id = ta.id
                    and    caracter_id = cr.id
                    and    tipo_estudio_id = te.id) x,
                   (select   hor_sgr_grp_asi_id, hor_sgr_grp_id, hor_sgr_grp_curso_aca, hor_sgr_id, hor_sgr_tipo,
                             hor_semestre, hor_dia_sem, hor_ini, count (distinct fecha) sesiones,
                             min (fecha) fecha_inicio
                    from     pod_horarios_fechas hf
                    group by hor_sgr_grp_asi_id,
                             hor_sgr_grp_id,
                             hor_sgr_grp_curso_aca,
                             hor_sgr_id,
                             hor_sgr_tipo,
                             hor_semestre,
                             hor_dia_sem,
                             hor_ini) det
            where  det.hor_sgr_grp_curso_aca = (select id - 1
                                                from   uji_horarios.hor_curso_academico)
            and    det.hor_sgr_grp_asi_id = x.asignatura_id
            and    det.hor_semestre = x.semestre_id
            and    det.hor_sgr_grp_id = x.grupo_id
            and    det.hor_sgr_tipo = x.tipo_subgrupo_id
            and    det.hor_sgr_id = x.subgrupo_id
            and    det.hor_dia_sem = x.dia_semana_id
            and    to_char (det.hor_ini, 'hh24:mi') = to_char (x.hora_inicio, 'hh24:mi')
                                                                                        --and    substr (asignatura_id, 1, 2) in ('AE', 'EC', 'FC')
                                                                                        --and    asignatura_id in ('AE1013', 'EC1013', 'FC1013')
                                                                                        --and    asignatura_id like ('VJ%')
                                                                                                                         --where  asignatura_id in ('AE1001', 'EC1001', 'FC1001')
                                                                                                                         --and    comun = 1;
                                                                                                                         /*and    exists (
                                                                                                                                   select 1
                                                                                                                                   from   pod_subgrupos
                                                                                                                                   where  grp_curso_aca = (select id
                                                                                                                                                           from   uji_horarios.hor_curso_academico)
                                                                                                                                   and    grp_asi_id = asignatura_id
                                                                                                                                   and    grp_id = grupo_id
                                                                                                                                   and    tipo = tipo_subgrupo_id
                                                                                                                                   and    id = subgrupo_id)*/
         ;

         cursor lista_items_sin_planificar is
            select x.*, null sesiones, null fecha_inicio, null semana
            from   (select asignatura_id, asignatura, estudio_id, estudio, tipo_estudio_id, te.nombre tipo_estudio,
                           curso_id, caracter_id, cr.nombre caracter, semestre_id, null aula_planificacion_id,
                           persona_id profesor_id, grupo_id, tipo_subgrupo_id, tsg.nombre tipo_subgrupo, subgrupo_id,
                           dia_semana_id, hora_inicio, hora_fin, null desde_el_dia, null hasta_el_dia, 0 detalle_manual,
                           tipo_asignatura_id, ta.nombre tipo_asignatura, comun, porcentaje_comun, plazas
                    from   (select *
                            from   (select 0 id, asit.tit_id estudio_id, t.nombre estudio,
                                           t.tipo_estudio tipo_estudio_id, cur_id curso_id, grp_asi_id asignatura_id,
                                           caracter caracter_id, grp_id grupo_id, sem.semestre semestre_id,
                                           grp_curso_aca curso_aca, s.tipo tipo_subgrupo_id, s.id subgrupo_id,
                                           null dia_semana_id, null hora_inicio, null hora_fin, null persona_id,
                                           null ubicacion_id, null compartido, a.tipo tipo_asignatura_id,
                                           a.nombre asignatura,
                                           (select decode (count (*), 0, 0, 1)
                                            from   pod_comunes com,
                                                   pod_grp_comunes gc
                                            where  gc.id = com.gco_id
                                            and    curso_aca = ac.curso_aca
                                            and    asi_id = a.id) comun,
                                           (select porcentaje
                                            from   pod_comunes com,
                                                   pod_grp_comunes gc
                                            where  gc.id = com.gco_id
                                            and    curso_aca = ac.curso_aca
                                            and    asi_id = a.id) porcentaje_comun,
                                           s.limite_nuevos plazas
                                    from   pod_grupos g,
                                           pod_subgrupos s,
                                           pod_asignaturas_titulaciones asit,
                                           pod_asi_cursos ac,
                                           pod_asignaturas a,
                                           pod_titulaciones t,
                                           (select 'S' tipo, 1 semestre
                                            from   dual
                                            union all
                                            select 'S' tipo, 2 semestre
                                            from   dual
                                            union all
                                            select 'A' tipo, 1 semestre
                                            from   dual
                                            union all
                                            select 'A' tipo, 2 semestre
                                            from   dual) sem
                                    where  g.curso_aca = s.grp_curso_aca
                                    and    g.id = s.grp_id
                                    and    g.asi_id = s.grp_asi_id
                                    and    s.grp_asi_id = asit.asi_id
                                    and    grp_curso_aca = (select id
                                                            from   uji_horarios.hor_curso_academico)
                                    and    grp_curso_aca = ac.curso_aca
                                    and    grp_asi_id = ac.asi_id
                                    and    asit.tit_id = ac.cur_tit_id
                                    and    asit.tit_id between 201 and 9999
                                    and    asit.tit_id in (220, 221, 222, 227, 228, 225, 231)
                                    --and    a.id in ('AG1002', 'EM1002', 'EI1002', 'EQ1002', 'ET1002')
                                    and    asit.tit_id = t.id
                                    and    g.asi_id = a.id
                                    and    a.tipo = sem.tipo
                                    and    g.semestre = sem.semestre)
                            where  (estudio_id,
                                    estudio,
                                    tipo_estudio_id,
                                    curso_id,
                                    asignatura_id,
                                    caracter_id,
                                    grupo_id,
                                    semestre_id,
                                    tipo_subgrupo_id,
                                    subgrupo_id
                                   ) not in (
                                      select estudio_id, estudio, tipo_estudio_id, curso_id, asignatura_id, caracter_id,
                                             grupo_id, semestre_id, tipo_subgrupo_id, subgrupo_id
                                      from   uji_horarios.hor_items i,
                                             uji_horarios.hor_items_asignaturas a
                                      where  i.id = a.item_id)) i,
                           (select 'AV' id, 'Avaluaci�' nombre, 7 orden
                            from   dual
                            union all
                            select 'LA' id, 'Laboratori' nombre, 4 orden
                            from   dual
                            union all
                            select 'PR' id, 'Problemes' nombre, 3 orden
                            from   dual
                            union all
                            select 'SE' id, 'Seminari' nombre, 5 orden
                            from   dual
                            union all
                            select 'TE' id, 'Teoria' nombre, 1 orden
                            from   dual
                            union all
                            select 'TP' id, 'Teoria i problemes' nombre, 2 orden
                            from   dual
                            union all
                            select 'TU' id, 'Tutories' nombre, 6 orden
                            from   dual) tsg,
                           (select 'S' id, 'Semestral' nombre, 1 orden
                            from   dual
                            union all
                            select 'A' id, 'Anual' nombre, 2 orden
                            from   dual) ta,
                           (select 'TR' id, 'Troncal' nombre, 1 orden
                            from   dual
                            union all
                            select 'FB' id, 'Formaci� b�sica' nombre, 2 orden
                            from   dual
                            union all
                            select 'OB' id, 'Obligatoria' nombre, 3 orden
                            from   dual
                            union all
                            select 'OP' id, 'Optativa' nombre, 4 orden
                            from   dual
                            union all
                            select 'LC' id, 'Lliure configuraci�' nombre, 5 orden
                            from   dual
                            union all
                            select 'PR' id, 'Pr�ctiques externes' nombre, 6 orden
                            from   dual
                            union all
                            select 'PF' id, 'Treball fi de grau' nombre, 7 orden
                            from   dual) cr,
                           (select '12C' id, 'Primer i segon cicle' nombre, 2 orden
                            from   dual
                            union all
                            select 'G' id, 'Grau' nombre, 1 orden
                            from   dual
                            union all
                            select 'M' id, 'M�ster' nombre, 3 orden
                            from   dual) te
                    where  tipo_subgrupo_id = tsg.id
                    and    tipo_asignatura_id = ta.id
                    and    caracter_id = cr.id
                    and    tipo_estudio_id = te.id) x
            where  1 = 1
            --and    asignatura_id like ('VJ%')
            and    2 = 2;
      begin
         delete      uji_horarios.hor_items_detalle;

         delete      uji_horarios.hor_items_asignaturas;

         delete      uji_horarios.hor_items_det_profesores;

         delete      uji_horarios.hor_items_profesores;

         delete      uji_horarios.hor_items_comunes;

         delete      uji_horarios.hor_items;

         commit;
         v_control := 1;

         select numero_Semanas, to_number (to_char (fecha_inicio, 'ww'))
         into   v_semanas_s1, v_semana1_s1
         from   uji_horarios.hor_semestres_detalle
         where  tipo_estudio_id = 'G'
         and    semestre_id = 1;

         select numero_Semanas, to_number (to_char (fecha_inicio, 'ww'))
         into   v_semanas_s2, v_semana1_s2
         from   uji_horarios.hor_semestres_detalle
         where  tipo_estudio_id = 'G'
         and    semestre_id = 2;

         v_control := 2;

         for x in lista_items loop
            v_control := 3;

   
            select count (*)
            into   v_cuantos
            from   uji_horarios.hor_items i,
                   uji_horarios.hor_items_asignaturas a,
                   uji_horarios.hor_ext_asignaturas_comunes c
            where  curso_id = x.curso_id
            and    caracter = x.caracter
            and    semestre_id = x.semestre_id
            and    grupo_id = x.grupo_id
            and    tipo_subgrupo_id = x.tipo_subgrupo_id
            and    subgrupo_id = x.subgrupo_id
            and    dia_semana_id = x.dia_semana_id
            and    hora_inicio = x.hora_inicio
            and    i.id = a.item_id
            and    a.asignatura_id = c.asignatura_id
            and    nombre like '%' || x.asignatura_id || '%';

            v_control := 4;

            if    v_cuantos = 0
               or x.comun = 0 then
               v_id := uji_horarios.hibernate_sequence.nextval;
               v_control := 5;

               if x.aula_planificacion_id is null then
                  v_aula_nombre := null;
               else
                  begin
                     select nombre
                     into   v_aula_nombre
                     from   uji_horarios.hor_aulas
                     where  id = x.aula_planificacion_id;
                  exception
                     when no_data_found then
                        v_aula_nombre := null;
                  end;
               end if;

               v_control := 6;

               if x.comun = 0 then
                  v_comun_texto := null;
               else
                  begin
                     select nombre
                     into   v_comun_texto
                     from   uji_horarios.hor_ext_asignaturas_comunes
                     where  asignatura_id = x.asignatura_id;
                  exception
                     when no_data_found then
                        v_comun_texto := null;
                  end;
               end if;

               v_control := 7;

               if x.semestre_id = 1 then
                  if x.sesiones >= v_semanas_s1 - 2 then
                     v_sesiones := null;
                  else
                     v_sesiones := x.sesiones;
                  end if;
               else
                  if x.sesiones >= v_semanas_s2 - 2 then
                     v_sesiones := null;
                  else
                     v_sesiones := x.sesiones;
                  end if;
               end if;

               v_control := 8;

               insert into uji_horarios.hor_items
                           (id, curso_id, caracter_id, caracter, semestre_id, aula_planificacion_id,
                            comun, porcentaje_comun, grupo_id, tipo_subgrupo_id, tipo_subgrupo,
                            subgrupo_id, dia_semana_id, hora_inicio, hora_fin, desde_el_dia, hasta_el_dia,
                            tipo_asignatura_id, tipo_asignatura, tipo_estudio_id, tipo_estudio, plazas,
                            repetir_cada_semanas, numero_iteraciones, detalle_manual, comun_texto,
                            aula_planificacion_nombre
                           )
               values      (v_id, x.curso_id, x.caracter_id, x.caracter, x.semestre_id, x.aula_planificacion_id,
                            x.comun, x.porcentaje_comun, x.grupo_id, x.tipo_subgrupo_id, x.tipo_subgrupo,
                            x.subgrupo_id, x.dia_semana_id, x.hora_inicio, x.hora_fin, x.desde_el_dia, x.hasta_el_dia,
                            x.tipo_asignatura_id, x.tipo_asignatura, x.tipo_estudio_id, x.tipo_estudio, x.plazas,
                            1, v_sesiones, x.detalle_manual, v_comun_texto,
                            v_aula_nombre
                           );

               v_control := 9;
            else
               v_control := 10;

               /*dbms_output.put_line (x.curso_id || ' ' || x.caracter || ' ' || x.semestre_id || ' ' || x.grupo_id
                                     || ' ' || x.tipo_subgrupo_id || ' ' || x.subgrupo_id || ' ' || x.dia_semana_id
                                     || ' ' || to_char (x.hora_inicio, 'dd/mm/yyyy hh24:mi') || ' ' || x.comun || ' '
                                     || x.asignatura_id || ' ' || x.estudio_id);*/

               /*select id
               into   v_id
               from   uji_horarios.hor_items
               where  curso_id = x.curso_id
               and    caracter = x.caracter
               and    semestre_id = x.semestre_id
               and    grupo_id = x.grupo_id
               and    tipo_subgrupo_id = x.tipo_subgrupo_id
               and    subgrupo_id = x.subgrupo_id
               and    dia_semana_id = x.dia_semana_id
               and    hora_inicio = x.hora_inicio;
               */
               select distinct i.id
               into            v_id
               from            uji_horarios.hor_items i,
                               uji_horarios.hor_items_asignaturas a,
                               uji_horarios.hor_ext_asignaturas_comunes c
               where           curso_id = x.curso_id
               and             caracter = x.caracter
               and             semestre_id = x.semestre_id
               and             grupo_id = x.grupo_id
               and             tipo_subgrupo_id = x.tipo_subgrupo_id
               and             subgrupo_id = x.subgrupo_id
               and             dia_semana_id = x.dia_semana_id
               and             hora_inicio = x.hora_inicio
               and             i.id = a.item_id
               and             a.asignatura_id = c.asignatura_id
               and             nombre like '%' || x.asignatura_id || '%';

               v_control := 11;
            end if;

            v_control := 12;
            v_id_det := uji_horarios.hibernate_sequence.nextval;

            insert into uji_horarios.hor_items_asignaturas
                        (id, item_id, asignatura_id, asignatura, estudio_id, estudio
                        )
            values      (v_id_det, v_id, x.asignatura_id, x.asignatura, x.estudio_id, x.estudio
                        );

            commit;
            v_control := 13;

            update uji_horarios.hor_items
            set numero_iteraciones = numero_iteraciones
            where  id = v_id;

            commit;
            v_control := 14;
         end loop;

         for x in lista_items_sin_planificar loop
            v_control := 3;

            select count (*)
            into   v_cuantos
            from   uji_horarios.hor_items i,
                   uji_horarios.hor_items_asignaturas a,
                   uji_horarios.hor_ext_asignaturas_comunes c
            where  curso_id = x.curso_id
            and    caracter = x.caracter
            and    semestre_id = x.semestre_id
            and    grupo_id = x.grupo_id
            and    tipo_subgrupo_id = x.tipo_subgrupo_id
            and    subgrupo_id = x.subgrupo_id
            --and    dia_semana_id = x.dia_semana_id
            --and    hora_inicio = x.hora_inicio
            and    i.id = a.item_id
            and    a.asignatura_id = c.asignatura_id
            and    nombre like '%' || x.asignatura_id || '%';

            /*
            select count (*)
            into   v_cuantos
            from   uji_horarios.hor_ext_asignaturas_comunes c
            where  nombre like '%' || x.asignatura_id || '%';
            */
            v_control := 4;

            if    v_cuantos = 0
               or x.comun = 0 then
               v_id := uji_horarios.hibernate_sequence.nextval;
               v_control := 5;

               if x.aula_planificacion_id is null then
                  v_aula_nombre := null;
               else
                  begin
                     select nombre
                     into   v_aula_nombre
                     from   uji_horarios.hor_aulas
                     where  id = x.aula_planificacion_id;
                  exception
                     when no_data_found then
                        v_aula_nombre := null;
                  end;
               end if;

               v_control := 6;

               if x.comun = 0 then
                  v_comun_texto := null;
               else
                  begin
                     select nombre
                     into   v_comun_texto
                     from   uji_horarios.hor_ext_asignaturas_comunes
                     where  asignatura_id = x.asignatura_id;
                  exception
                     when no_data_found then
                        v_comun_texto := null;
                  end;
               end if;

               v_control := 7;

               if x.semestre_id = 1 then
                  if x.sesiones >= v_semanas_s1 - 2 then
                     v_sesiones := null;
                  else
                     v_sesiones := x.sesiones;
                  end if;
               else
                  if x.sesiones >= v_semanas_s2 - 2 then
                     v_sesiones := null;
                  else
                     v_sesiones := x.sesiones;
                  end if;
               end if;

               v_control := 8;

               insert into uji_horarios.hor_items
                           (id, curso_id, caracter_id, caracter, semestre_id, aula_planificacion_id,
                            comun, porcentaje_comun, grupo_id, tipo_subgrupo_id, tipo_subgrupo,
                            subgrupo_id, dia_semana_id, hora_inicio, hora_fin, desde_el_dia, hasta_el_dia,
                            tipo_asignatura_id, tipo_asignatura, tipo_estudio_id, tipo_estudio, plazas,
                            repetir_cada_semanas, numero_iteraciones, detalle_manual, comun_texto,
                            aula_planificacion_nombre
                           )
               values      (v_id, x.curso_id, x.caracter_id, x.caracter, x.semestre_id, x.aula_planificacion_id,
                            x.comun, x.porcentaje_comun, x.grupo_id, x.tipo_subgrupo_id, x.tipo_subgrupo,
                            x.subgrupo_id, x.dia_semana_id, x.hora_inicio, x.hora_fin, x.desde_el_dia, x.hasta_el_dia,
                            x.tipo_asignatura_id, x.tipo_asignatura, x.tipo_estudio_id, x.tipo_estudio, x.plazas,
                            1, v_sesiones, x.detalle_manual, v_comun_texto,
                            v_aula_nombre
                           );

               v_control := 9;
            else
               v_control := 10;

               /*dbms_output.put_line (x.curso_id || ' ' || x.caracter || ' ' || x.semestre_id || ' ' || x.grupo_id
                                     || ' ' || x.tipo_subgrupo_id || ' ' || x.subgrupo_id || ' ' || x.dia_semana_id
                                     || ' ' || to_char (x.hora_inicio, 'dd/mm/yyyy hh24:mi') || ' ' || x.comun || ' '
                                     || x.asignatura_id || ' ' || x.estudio_id);*/

               /*select id
               into   v_id
               from   uji_horarios.hor_items
               where  curso_id = x.curso_id
               and    caracter = x.caracter
               and    semestre_id = x.semestre_id
               and    grupo_id = x.grupo_id
               and    tipo_subgrupo_id = x.tipo_subgrupo_id
               and    subgrupo_id = x.subgrupo_id
               and    dia_semana_id = x.dia_semana_id
               and    hora_inicio = x.hora_inicio;
               */
               select distinct i.id
               into            v_id
               from            uji_horarios.hor_items i,
                               uji_horarios.hor_items_asignaturas a,
                               uji_horarios.hor_ext_asignaturas_comunes c
               where           curso_id = x.curso_id
               and             caracter = x.caracter
               and             semestre_id = x.semestre_id
               and             grupo_id = x.grupo_id
               and             tipo_subgrupo_id = x.tipo_subgrupo_id
               and             subgrupo_id = x.subgrupo_id
               --and             dia_semana_id = x.dia_semana_id
               --and             hora_inicio = x.hora_inicio
               and             i.id = a.item_id
               and             a.asignatura_id = c.asignatura_id
               and             nombre like '%' || x.asignatura_id || '%';

               v_control := 11;
            end if;

            v_control := 12;
            v_id_det := uji_horarios.hibernate_sequence.nextval;

            insert into uji_horarios.hor_items_asignaturas
                        (id, item_id, asignatura_id, asignatura, estudio_id, estudio
                        )
            values      (v_id_det, v_id, x.asignatura_id, x.asignatura, x.estudio_id, x.estudio
                        );

            commit;
            v_control := 13;

            update uji_horarios.hor_items
            set numero_iteraciones = numero_iteraciones
            where  id = v_id;

            commit;
            v_control := 14;
         end loop;
      exception
         when others then
            dbms_output.put_line (v_control || ' ' || sqlerrm);
      end;

      dbms_output.put_line ('hor_items_detalles');

      /* control de semestres para asignaturas semestrales */
      declare
         cursor lista is
            select   asignatura_id, semestre_id, decode (semestre_id, 1, 2, 2, 1) semestre_bueno, i.id item_id
            from     uji_horarios.hor_items i,
                     uji_horarios.hor_items_asignaturas a
            where    i.id = a.item_id
--and             asignatura_id like 'EA%'
            and      semestre_id not in (
                        select 1 semestre
                        from   pod_grupos g,
                               pod_asignaturas a
                        where  curso_Aca >= 2013
                        and    asi_id = asignatura_id
                        and    (    a.tipo = 'S'
                                and semestre = 1)
                        and    asi_id = a.id
                        union all
                        select 2 semestre
                        from   pod_grupos g,
                               pod_asignaturas a
                        where  curso_Aca >= 2013
                        and    asi_id = asignatura_id
                        and    (    a.tipo = 'S'
                                and semestre = 2)
                        and    asi_id = a.id)
            --and      asignatura_id = 'AE1003'
            order by asignatura_id;
      begin
         for x in lista loop
            update uji_horarios.hor_items i
            set semestre_id = x.semestre_bueno
            where  i.id = x.item_id
            and    i.semestre_id = x.semestre_id;

            commit;
         end loop;
      end;

/*

--==> hor_aulas_planificacion
      --insert into uji_horarios.hor_aulas_planificacion
      --            (id, nombre, aula_id, estudio_id, semestre_id)
      declare
         cursor lista is
            select rownum id, nombre, aula_id, estudio_id, semestre_id
            from   (select distinct 0 id, replace (u.nombre, chr (10), ' ') nombre, u.id aula_id,
                                    titulacion_id estudio_id, semestre_id
                    from            (select 0 id, grc_cur_tit_id titulacion_id, grc_cur_id curso_id,
                                            sgr_grp_asi_id asignatura_id, caracter, sgr_grp_id grupo_id,
                                            to_number (semestre) semestre_id, sgr_grp_curso_aca curso_aca,
                                            sgr_tipo tipo_id, sgr_id subgrupo_id, ubi_id ubicacion_id
                                     from   pod_horarios h,
                                            pod_circuitos_det cir,
                                            pod_asi_cursos ac
                                     where  sgr_grp_curso_aca = p_curso_aca - 1
                                     --and    sgr_grp_asi_id = 'AE1008'
                                     --and    sgr_grp_id = 'A'
                                     and    sgr_grp_curso_aca = ac.curso_aca
                                     and    grc_cur_tit_id = ac.cur_tit_id
                                     and    grc_cur_id = ac.cur_id
                                     and    sgr_grp_asi_id = ac.asi_id
                                     and    cur_tit_id between 201 and 9999
                                     and    sgr_grp_curso_aca = sgd_sgr_grp_curso_aca(+)
                                     and    sgr_grp_asi_id = sgd_sgr_grp_asi_id(+)
                                     and    sgr_grp_id = SGD_SGR_GRP_ID(+)
                                     and    sgr_tipo = SGD_SGR_TIPO(+)
                                     and    sgr_id = SGD_SGR_ID(+)) x,
                                    (select u.id id, u.descripcion nombre, u.id aula_id, uest_id centro_id,
                                            tubic_id tipo, num_alumnos plazas,
                                            edi_are_area || edi_edificio || planta || dependencia || tubic_id codigo
                                     from   est_ubicaciones u,
                                            est_areas_ubicacion a,
                                            est_tipos_ubicacion t
                                     where  edi_are_area = a.area
                                     and    u.tubic_id = t.id) u
                    where           ubicacion_id is not null
                    and             ubicacion_id = u.id(+)
                    and             u.centro_id in (select id
                                                    from   uji_horarios.hor_centros));
      begin
         for x in lista loop
            begin
               insert into uji_horarios.hor_aulas_planificacion
                           (id, nombre, aula_id, estudio_id, semestre_id
                           )
               values      (x.id, x.nombre, x.aula_id, x.estudio_id, x.semestre_id
                           );
            exception
               when others then
                  null;
            end;
         end loop;

         insert into uji_horarios.hor_aulas_planificacion
            select uji_horarios.hibernate_sequence.nextval, nombre, aula_id, estudio_id, curso_id, semestre_id
            from   (select nombre, aula_id, estudio_id, null curso_id, semestre semestre_id
                    from   (select distinct tit_id estudio_id, ubi_id,
                                            tu.nombre || ' ' || edi_are_area || edi_edificio || planta
                                            || dependencia || tubic_id nombre,
                                            edi_are_area || edi_edificio || planta || dependencia || tubic_id codigo,
                                            to_number (semestre) semestre,
                                            (select distinct id
                                             from            uji_horarios.hor_aulas a
                                             where           a.codigo =
                                                                u.edi_are_area || u.edi_edificio || u.planta
                                                                || u.dependencia || u.tubic_id) aula_id
                            from            pod_horarios h,
                                            pod_asignaturas_titulaciones atit,
                                            est_ubicaciones u,
                                            est_tipos_ubicacion tu
                            where           sgr_grp_curso_aca = 2012
                            and             sgr_grp_asi_id like 'EI%'
                            and             sgr_grp_asi_id = atit.asi_id
                            and             ubi_id = u.id
                            and             tubic_id = tu.id)
                    minus
                    select nombre, aula_id, estudio_id, curso_id, semestre_id
                    from   uji_horarios.hor_aulas_planificacion);
      end;

      commit;
      dbms_output.put_line ('hor_aulas_planificacion');
*/

      /*  SE GENERAN DESDE LA APLICAC�ION


            --==> hor_circuitos
            insert into uji_horarios.hor_circuitos
                        (id, id_circuito, nombre, estudio_id, grupo_id, especial)
               select rownum id, id id_circuito, nombre, tit_id estudio_id, grp_id grupo_id,
                      decode (grp_id, 'Y', 1, 0) especial
               from   pod_circuitos_cab
               where  curso_aca = p_curso_aca - 1
               --and    tit_id in (210, 211, 212)
               ;

            commit;
            dbms_output.put_line ('hor_circuitos');

      --==> hor_items_circuitos
            insert into uji_horarios.hor_items_circuitos
                        (id, item_id, circuito_id, plazas)
               select rownum id, i.id item_id, circuito_id, e.plazas
               from   uji_horarios.hor_ext_circuitos e,
                      uji_horarios.hor_circuitos c,
                      uji_horarios.hor_items i,
                      uji_horarios.hor_items_asignaturas asi
               where  e.estudio_id = c.estudio_id
               and    e.circuito_id = c.id_circuito
               and    e.asignatura_id = asi.asignatura_id
               and    e.estudio_id = asi.estudio_id
               and    e.grupo_id = i.grupo_id
               and    e.tipo = i.tipo_subgrupo_id
               and    e.subgrupo_id = i.subgrupo_id
               and    i.id = asi.item_id;

            commit;
            dbms_output.put_line ('hor_items_circuitos');

      --==> hor_items_comunes
            insert into uji_horarios.hor_items_comunes
               select rownum id, id item_id, asignatura_id, asignatura_comun_id, item_comun_id
               from   (select i.id, asi.asignatura_id, c.asignatura_id asignatura_comun_id, x.id item_comun_id
                       from   uji_horarios.hor_items i,
                              uji_horarios.hor_ext_asignaturas_comunes c,
                              uji_horarios.hor_items x,
                              uji_horarios.hor_items_asignaturas asi
                       where  c.nombre like '%' || asi.asignatura_id || '%'
                       and    c.asignatura_id <> asi.asignatura_id
                       and    c.asignatura_id = asi.asignatura_id
                       and    i.curso_id = x.curso_id
                       and    i.semestre_id = x.semestre_id
                       and    i.grupo_id = x.grupo_id
                       and    i.tipo_subgrupo_id = x.tipo_subgrupo_id
                       and    i.subgrupo_id = x.subgrupo_id
                       and    i.dia_semana_id = x.dia_Semana_id
                       and    x.id = asi.item_id
                       and    to_char (i.hora_inicio, 'hh24:mi') = to_char (x.hora_inicio, 'hh24:mi'));

            commit;
            dbms_output.put_line ('hor_items_comunes');
            *//* profesorado y creditos */
       update uji_horarios.hor_profesores prof
      set creditos = (select sum( cdo_max - reduccion) crd_impartir
                      from   pod_carga_docente
                      where  curso_Aca = :p_curso_aca+1
                      and    per_id = prof.id),
       creditos_maximos = (select cdo_max
                      from   pod_carga_docente
                      where  curso_Aca = :p_curso_aca+1
                      and    per_id = prof.id),
      creditos_reduccion = (select reduccion
                      from   pod_carga_docente
                      where  curso_Aca = :p_curso_aca+1
                      and    per_id = prof.id);

      

      commit;

      /* items y creditos */
      declare
         cursor lista is
            select i.id, asignatura_id, tipo_subgrupo_id, subgrupo_id,
                   (select decode (tipo_subgrupo_id,
                                   'TE', crd_te,
                                   'PR', crd_pr,
                                   'LA', crd_la,
                                   'SE', crd_se,
                                   'TU', crd_tu,
                                   'AV', crd_ev,
                                   -1
                                  )
                    from   pod_vsp_27
                    where  curso_aca = p_curso_aca
                    and    asi_id = asignatura_id
                    and    tit_id = estudio_id) creditos
            from   uji_horarios.hor_items i,
                   uji_horarios.hor_items_asignaturas ia
            where  i.id = ia.item_id;
      begin
         for x in lista loop
            update uji_horarios.hor_items
            set creditos = x.creditos
            where  id = x.id
            and    (   creditos is null
                    or creditos <> x.creditos);

            commit;
         end loop;
      end;
      
  /* Formatted on 23/03/2014 10:27 (Formatter Plus v4.8.8) */
    /* generar examenes */

	declare
	   v_aux   number;
	   v_id    number;
	   v_id2   number;

	   cursor lista is
	      select distinct c.curso_aca, cur_tit_id estudio_id, t.nombre estudio, c.cur_id curso_id, c.asi_id asi_id,
	                      a.nombre asi_nombre, decode (caracter, 'LC', 'EP', caracter) caracter,
	                      decode (decode (a.eep, 'S', 'S', a.tipo),
	                              'S', decode (a.tipo, 'A', 'A', decode (g.semestre, 1, '1', '2')),
	                              'A'
	                             ) semestre,
	                      g.semestre sem_grupo, decode (com.asi_id, null, null, 'S') comun, com.nombre comun_nombre,
	                      nvl (gco_id, 0) comun_id
	      from            pod_asi_cursos c,
	                      pod_grupos g,
	                      pod_asignaturas a,
	                      pod_titulaciones t,
	                      (select curso_aca, c2.asi_id, nombre, c2.gco_id
	                       from   pod_grp_comunes g,
	                              pod_comunes c,
	                              pod_comunes c2
	                       where  g.id = c.gco_id
	                       and    c.gco_id = c2.gco_id) com
	      where           c.asi_id = g.asi_id
	      and             g.asi_id = a.id
	      and             g.curso_aca = :vCurso
	      and             g.curso_aca = c.curso_aca
	      and             cur_tit_id = t.id
	      and             c.cur_id < 6
	      and             c.visible = 'S'
	      and             cur_tit_id not in (100, 104)
	      and             length (g.asi_id) >= 6
	      and             g.curso_aca = com.curso_aca(+)
	      and             g.asi_id = com.asi_id(+)
	      --and             cur_tit_id = 228
	      and             caracter not in ('LC', 'PF')
	      --and a.tipo ='A'
	      order by        5,
	                      11,
	                      1,
	                      2,
	                      3,
	                      4,
	                      5;

	   cursor convocatorias (p_sem in varchar2, p_sem_grupo in number) is
	      select   id, nombre, '' parcial
	      from     pod_convocatorias
	      where    tipo = 'G'
	      and      extra = 'N'
	      and      (   (    p_Sem_grupo = 1
	                    and id in (9, 11))
	                or (    p_Sem_grupo = 2
	                    and id in (10, 11)))
	      union all
	      select   id, nombre, ' - Parcial' parcial
	      from     pod_convocatorias
	      where    p_sem = 'A'
	      and      id = 9
	      order by 1;
	begin
	   delete      uji_horarios.hor_Examenes_asignaturas;

	   delete      uji_horarios.hor_Examenes_aulas;

	   delete      uji_horarios.hor_Examenes;

	   commit;
	   v_aux := 0;

	   for x in lista loop
	      v_aux := v_aux + 1;

	      for c in convocatorias (x.semestre, x.sem_grupo) loop
	         if x.comun is null then
	            v_id := uji_horarios.hibernate_sequence.nextval;

	            insert into uji_horarios.hor_examenes
	                        (id, nombre, convocatoria_id, convocatoria_nombre, comun, tipo_examen_id
	                        )
	            values      (v_id, x.asi_id || c.parcial, c.id, c.nombre, 0, 1
	                        );

	            insert into uji_horarios.hor_examenes_asignaturas
	                        (ID, EXAMEN_ID, ASIGNATURA_ID, ASIGNATURA_NOMBRE, ESTUDIO_ID,
	                         ESTUDIO_NOMBRE, SEMESTRE, TIPO_ASIGNATURA, CENTRO_ID
	                        )
	            values      (uji_horarios.hibernate_sequence.nextval, v_id, x.asi_id, x.asi_nombre, x.estudio_id,
	                         x.estudio, x.sem_grupo, decode (x.semestre, 'A', 'Anual', 'Semestral'), null
	                        );

	            dbms_output.put_line (v_aux || ' ' || x.asi_id || ' ' || x.comun_nombre || ' ' || x.semestre || ' '
	                                  || c.nombre);
	         else
	            begin
	               select e.id
	               into   v_id
	               from   uji_horarios.hor_examenes e,
	                      uji_horarios.hor_examenes_asignaturas ea
	               where  e.id = ea.examen_id
	               and    x.comun_nombre like '%' || asignatura_id || '%'
	               and    convocatoria_id = c.id;
	            exception
	               when too_many_rows then
	                  dbms_output.put_line ('too many ' || v_aux || ' ' || x.asi_id || ' ' || x.comun_nombre || ' '
	                                        || x.semestre || ' ' || c.nombre);
	               when no_data_found then
	                  v_id := uji_horarios.hibernate_sequence.nextval;

	                  insert into uji_horarios.hor_examenes
	                              (id, nombre, convocatoria_id, convocatoria_nombre, comun, comun_Texto, tipo_examen_id
	                              )
	                  values      (v_id, x.asi_id || ' - ' || c.nombre || c.parcial, c.id, c.nombre, 1, x.comun_nombre, 1
	                              );
	            end;

	            insert into uji_horarios.hor_examenes_asignaturas
	                        (ID, EXAMEN_ID, ASIGNATURA_ID, ASIGNATURA_NOMBRE, ESTUDIO_ID,
	                         ESTUDIO_NOMBRE, SEMESTRE, TIPO_ASIGNATURA, CENTRO_ID
	                        )
	            values      (uji_horarios.hibernate_sequence.nextval, v_id, x.asi_id, x.asi_nombre, x.estudio_id,
	                         x.estudio, x.sem_grupo, decode (x.semestre, 'A', 'Anual', 'Semestral'), null
	                        );

	            dbms_output.put_line ('buscar si existe');
	            dbms_output.put_line (v_aux || ' ' || x.asi_id || ' ' || x.comun_nombre || ' ' || x.semestre || ' '
	                                  || c.nombre);
	         end if;

	         commit;
	      end loop;
	   end loop;

	   /*
	   update uji_horarios.hor_examenes
	   set convocatoria_id = 111
	   where  id in (select e.id tipo_asignatura
	                 from   uji_horarios.hor_examenes e,
	                        uji_horarios.hor_examenes_asignaturas ea
	                 where  e.id = examen_id
	                 and    convocatoria_id = 11
	                 and    semestre = 2);
					 */

	   /*
	   update uji_horarios.hor_examenes e
	   set nombre = nombre || ' - Juny'
	   where  convocatoria_id = 111;

	   update uji_horarios.hor_examenes e
	   set nombre = nombre || ' - Juliol'
	   where  convocatoria_id = 111;
	   */
	   
	   update uji_horarios.hor_examenes_asignaturas
set curso_id =
       (select distinct cur_id
        from            pod_asi_cursos
        where           asi_id = asignatura_id
        and             curso_aca = 2014
        and             cur_tit_id <> 100
        and             cur_tit_id between 201 and 9999
        and             cur_id < 7
        and             asi_id not like 'OT%'
        and             cur_id < 7);
                
                

	   commit;
	end;
	
   end if;
END generar_uji_horarios;