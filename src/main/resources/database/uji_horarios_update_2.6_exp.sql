CREATE OR REPLACE procedure UJI_HORARIOS.HORARIOS_SEMANA_PDF(name_array in euji_util.ident_arr2, value_array in euji_util.ident_arr2) is
   -- Parametros
   pEstudio constant             varchar2(40) := euji_util.euji_getparam(name_array, value_array, 'pEstudio');
   pSemestre constant            varchar2(40) := euji_util.euji_getparam(name_array, value_array, 'pSemestre');
   pSesion constant              varchar2(4000) := euji_util.euji_getparam(name_array, value_array, 'pSesion');
   pGrupo                        varchar2(40) := euji_util.euji_getparam(name_array, value_array, 'pGrupo');
   pCurso constant               varchar2(40) := euji_util.euji_getparam(name_array, value_array, 'pCurso');
   pSemana constant              varchar2(40) := euji_util.euji_getparam(name_array, value_array, 'pSemana');
   pAgrupacion constant          varchar2(40) := euji_util.euji_getparam(name_array, value_array, 'pAgrupacion');
   vCursoAcaNum                  number;
   vSesion                       number;
   vFechaIniSemestre             date;
   vFechaFinSemestre             date;
   vEsGenerica                   boolean := pSemana = 'G';
   vFirstPageBreak               boolean := true;
   pGrupoParam                   varchar2(40) := euji_util.euji_getparam(name_array, value_array, 'pGrupo');

   cursor c_items_gen(pSemestre in number, pCurso in number) is
      select distinct aulas.codigo as codigo_aula, asignatura_id, asignatura, estudio, asi.caracter, semestre_id, comun, grupo_id, tipo_subgrupo, tipo_subgrupo_id, subgrupo_id, dia_semana_id, hora_inicio, hora_fin, tipo_asignatura_id,
             tipo_asignatura, to_date(dia_semana_id + 1 || '/01/2006 ' || to_char(hora_fin, 'hh24:mi'), 'dd/mm/yyyy hh24:mi') fin, to_date(dia_semana_id + 1 || '/01/2006 ' || to_char(hora_inicio, 'hh24:mi'), 'dd/mm/yyyy hh24:mi') inicio
        from UJI_HORARIOS.HOR_ITEMS items left outer join hor_aulas aulas on (aulas.id = items.aula_planificacion_id) inner join uji_horarios.hor_items_asignaturas asi on (items.id = asi.item_id)
       where asi.estudio_id = pEstudio
         and asi.curso_id = pCurso
         and ',' || pGrupo || ',' like '%,' || items.grupo_id || ',%'
         and items.semestre_id = pSemestre
         and items.dia_semana_id is not null;

   cursor c_items_gen_agrupacion(pSemestre in  number,
                                 pAgrupacion in number) is
      select distinct aulas.codigo as codigo_aula, asi.asignatura_id, asignatura, estudio, asi.caracter, semestre_id, comun, grupo_id, tipo_subgrupo, tipo_subgrupo_id, subgrupo_id, dia_semana_id, hora_inicio, hora_fin, tipo_asignatura_id,
                      tipo_asignatura, to_date(dia_semana_id + 1 || '/01/2006 ' || to_char(hora_fin, 'hh24:mi'), 'dd/mm/yyyy hh24:mi') fin,
                      to_date(dia_semana_id + 1 || '/01/2006 ' || to_char(hora_inicio, 'hh24:mi'), 'dd/mm/yyyy hh24:mi') inicio
        from UJI_HORARIOS.HOR_ITEMS items left outer join hor_aulas aulas on (aulas.id = items.aula_planificacion_id) inner join uji_horarios.hor_items_asignaturas asi on (items.id = asi.item_id)
             join uji_horarios.HOR_V_AGRUPACIONES_ITEMS agr_item
                on (items.id = AGR_ITEM.ITEM_ID)
       where asi.estudio_id = pEstudio
         and agr_item.agrupacion_id = pAgrupacion
         and ',' || pGrupo || ',' like '%,' || items.grupo_id || ',%'
         and items.semestre_id = pSemestre
         and items.dia_semana_id is not null;

   cursor c_items_det(vIniDate      date,
                      vFinDate      date,
                      pCurso in     number) is
        select distinct aulas.codigo as codigo_aula, asignatura_id, asignatura, estudio, grupo_id, tipo_subgrupo, tipo_subgrupo_id, subgrupo_id, dia_semana_id, hora_inicio, hora_fin, inicio, fin
          from UJI_HORARIOS.HOR_ITEMS items left outer join hor_aulas aulas on (aulas.id = items.aula_planificacion_id) inner join uji_horarios.hor_items_asignaturas asi on (items.id = asi.item_id)
               inner join UJI_HORARIOS.HOR_ITEMS_DETALLE det
                  on (det.item_id = items.id)
         where asi.estudio_id = pEstudio
           and asi.curso_id = pCurso
           and ',' || pGrupo || ',' like '%,' || items.grupo_id || ',%'
           and items.semestre_id = pSemestre
           and items.dia_semana_id is not null
           and det.inicio > vIniDate
           and det.fin < vFinDate
      order by det.inicio;

   cursor c_items_det_agrupacion(vIniDate      date,
                                 vFinDate      date,
                                 pAgrupacion in number) is
        select distinct aulas.codigo as codigo_aula, asi.asignatura_id, asignatura, estudio, grupo_id, tipo_subgrupo, tipo_subgrupo_id, subgrupo_id, dia_semana_id, hora_inicio, hora_fin, inicio, fin
          from UJI_HORARIOS.HOR_ITEMS items left outer join hor_aulas aulas on (aulas.id = items.aula_planificacion_id) inner join uji_horarios.hor_items_asignaturas asi on (items.id = asi.item_id)
               inner join UJI_HORARIOS.HOR_ITEMS_DETALLE det
                  on (det.item_id = items.id)
               join uji_horarios.HOR_V_AGRUPACIONES_ITEMS agr_item
                  on (items.id = AGR_ITEM.ITEM_ID)
         where asi.estudio_id = pEstudio
           and agr_item.agrupacion_id = pAgrupacion
           and ',' || pGrupo || ',' like '%,' || items.grupo_id || ',%'
           and items.semestre_id = pSemestre
           and items.dia_semana_id is not null
           and det.inicio > vIniDate
           and det.fin < vFinDate
      order by det.inicio;

   cursor c_asigs(pSemestre in number, pCurso in number) is
        select distinct (asignatura_id), asignatura
          from UJI_HORARIOS.HOR_ITEMS items, uji_horarios.hor_items_asignaturas asi
         where items.id = asi.item_id
           and asi.estudio_id = pEstudio
           and asi.curso_id = pCurso
           and ',' || pGrupo || ',' like '%,' || items.grupo_id || ',%'
           and items.semestre_id = pSemestre
           and items.dia_semana_id is not null
      order by asignatura_id;

   cursor c_asigs_agrupacion(pSemestre in number, pAgrupacion in number) is
        select distinct (asi.asignatura_id), asi.asignatura
          from UJI_HORARIOS.HOR_ITEMS items, uji_horarios.hor_items_asignaturas asi, uji_horarios.HOR_V_AGRUPACIONES_ITEMS agr_item
         where items.id = asi.item_id
           and items.id = agr_item.item_id
           and asi.estudio_id = pEstudio
           and agr_item.agrupacion_id = pAgrupacion
           and ',' || pGrupo || ',' like '%,' || items.grupo_id || ',%'
           and items.semestre_id = pSemestre
           and items.dia_semana_id is not null
      order by asignatura_id;

   cursor c_clases_asig(vAsignaturaId varchar2, vSemestre varchar2, pCurso in number) is
        select tipo_subgrupo_id || subgrupo_id subgrupo, decode(dia_semana_id, 1, 'L', 2, 'M', 3, 'X', 4, 'J', 5, 'V', '?') dia_sem, to_char(hora_fin, 'hh24:mi') hora_fin_str, to_char(hora_inicio, 'hh24:mi') hora_inicio_str
          from UJI_HORARIOS.HOR_ITEMS items, uji_horarios.hor_items_asignaturas asi
         where items.id = asi.item_id
           and asi.estudio_id = pEstudio
           and asi.curso_id = pCurso
           and ',' || pGrupo || ',' like '%,' || items.grupo_id || ',%'
           and items.semestre_id = vSemestre
           and asi.asignatura_id = vAsignaturaId
           and items.dia_semana_id is not null
      order by dia_semana_id, hora_inicio, subgrupo;

   cursor c_clases_asig_agrupacion(vAsignaturaId varchar2, vSemestre varchar2, pAgrupacion in number) is
        select tipo_subgrupo_id || subgrupo_id subgrupo, decode(dia_semana_id, 1, 'L', 2, 'M', 3, 'X', 4, 'J', 5, 'V', '?') dia_sem, to_char(hora_fin, 'hh24:mi') hora_fin_str, to_char(hora_inicio, 'hh24:mi') hora_inicio_str
          from UJI_HORARIOS.HOR_ITEMS items, uji_horarios.hor_items_asignaturas asi
         where items.id = asi.item_id
           and asi.estudio_id = pEstudio
           and ',' || pGrupo || ',' like '%,' || items.grupo_id || ',%'
           and items.semestre_id = vSemestre
           and asi.asignatura_id = vAsignaturaId
           and items.dia_semana_id is not null
           and items.id in (select item_id
                              from HOR_V_AGRUPACIONES_ITEMS agr_item
                             where agr_item.agrupacion_id = pAgrupacion)
      order by dia_semana_id, hora_inicio, subgrupo;

   vFechas                       t_horario;
   vRdo                          clob;

   cursor cursosTitulacion(vEstudio in number, vCurso in number) is
        select distinct curso_id
          from hor_items_asignaturas
         where estudio_id = vEstudio
           and (curso_id = vCurso
             or vCurso = -1)
      order by curso_id;

   cursor agrupacionesTitulacion(vEstudio in number, vAgrupacion in number) is
      select agrupacion_id
        from hor_agrupaciones_estudios
       where estudio_id = vEstudio
         and (agrupacion_id = vAgrupacion
           or vAgrupacion = -1);

   procedure cabecera is
   begin
      owa_util.mime_header(ccontent_type => 'text/xml', bclose_header => true);
      fop.documentOpen(page_width => 29.7,
                       page_height => 21,
                       margin_left => 0.5,
                       margin_right => 0.5,
                       margin_top => 0.5,
                       margin_bottom => 0,
                       extent_before => 0,
                       extent_after => 0.5,
                       extent_margin_top => 0,
                       extent_margin_bottom => 0.5);
      fop.pageBodyOpen;
      fop.documentfooteropen;
      fop.block('Fecha documento: ' || to_char(sysdate, 'yyyy/mm/dd HH24:MI'), font_size => 6, font_style => 'italic');
      fop.documentfooterclose;
      fop.pageFlowOpen;
   end;

   procedure calcula_curso_aca is
   begin
        select curso_academico_id
          into vCursoAcaNum
          from hor_semestres_detalle d, hor_estudios e
         where e.id = pEstudio
           and e.tipo_id = d.tipo_estudio_id
           and rownum = 1
      order by fecha_inicio;
   end;
   
   function get_grupos(curso number, semestre number)
      return varchar2 is
      grupos          varchar2(200);
   begin
      begin
         select listagg(grupo_id, ',') within group (order by grupo_id)
           into grupos
           from (  select distinct i.grupo_id
                     from hor_items i join hor_items_asignaturas ia on i.id = ia.item_id
                    where ia.estudio_id = pEstudio
                      and i.semestre_id = semestre
                      and ia.curso_id = curso
                 order by i.grupo_id);
         return grupos;
      exception
         when others then
            return pGrupo; 
      end;
   end;

   procedure info_calendario_gen(pSemestre in number, pCurso in number, pAgrupacion in number) is
      vCursoAca                  varchar2(4000);
      vTitulacionNombre          varchar2(4000);
      vCursoNombre               varchar2(4000) := '';
      vTextoCabecera             varchar2(4000);
      vTextoGrupo                varchar2(4000);
      vNombreAgrupacion          varchar2(4000) := '';
      vGruposTxt                 varchar2(4000) := '';
   begin
      select nombre
        into vTitulacionNombre
        from hor_estudios
       where id = pEstudio;

      if (pAgrupacion is not null) then
         select 'Agrupació ' || nombre
           into vNombreAgrupacion
           from hor_agrupaciones
          where id = pAgrupacion;
      end if;

      vCursoAca := to_char(vCursoAcaNum) || '/' || to_char(vCursoAcaNum + 1);

      if pCurso in ('1', '3') then
         vCursoNombre := pCurso || 'er curs';
      elsif pCurso = '2' then
         vCursoNombre := '2on  curs';
      elsif pCurso = '4' then
         vCursoNombre := '4t curs';
      elsif pCurso is not null then
         vCursoNombre := pCurso || '?  curs';
      end if;


      if pGrupoParam = '-1' then
         vGruposTxt := get_grupos(pCurso, pSemestre);
      else
         vGruposTxt := pGrupo;
      end if;

      if vGruposTxt like '%,%' then
         vTextoGrupo := 'Grups ' || vGruposTxt;
      else
         vTextoGrupo := 'Grup ' || vGruposTxt;
      end if;
      
      
      vTextoCabecera := vTitulacionNombre || ' - ' || vCursoNombre || vNombreAgrupacion || ' - ' || vTextoGrupo || ' (semestre ' || pSemestre || ') - ' || vCursoAca;
      fop.block(text => vTextoCabecera, border_bottom => 'solid', border_width => 0.05);
   end;

   procedure muestra_leyenda_asig(item c_asigs%rowtype, vSemestre varchar2, pCurso in number) is
      vClasesText          varchar2(4000);
   begin
      fop.block(item.asignatura_id || ', ' || item.asignatura || ':', font_size => 7, padding_top => 0.1);
      vClasesText := '<fo:inline color="white">___</fo:inline>';

      for c in c_clases_asig(item.asignatura_id, vSemestre, pCurso) loop
         vClasesText := vClasesText || c.dia_sem || ', ' || c.hora_inicio_str || '-' || c.hora_fin_str || ' (' || c.subgrupo || '); ';
      end loop;

      fop.block(vClasesText, font_size => 7, padding_top => 0.06);
   end;

   procedure leyenda_asigs(pSemestre in number, pCurso in number) is
   begin
      fop.block(' ', padding_top => 1);

      for item in c_asigs(pSemestre, pCurso) loop
         muestra_leyenda_asig(item, pSemestre, pCurso);
      end loop;
   end;

   procedure muestra_leyenda_asig_agr(item c_asigs%rowtype, vSemestre varchar2, vAgrupacion number) is
      vClasesText          varchar2(4000);
   begin
      fop.block(item.asignatura_id || ', ' || item.asignatura || ':', font_size => 7, padding_top => 0.1);
      vClasesText := '<fo:inline color="white">___</fo:inline>';

      for c in c_clases_asig_agrupacion(item.asignatura_id, vSemestre, vAgrupacion) loop
         vClasesText := vClasesText || c.dia_sem || ', ' || c.hora_inicio_str || '-' || c.hora_fin_str || ' (' || c.subgrupo || '); ';
      end loop;

      fop.block(vClasesText, font_size => 7, padding_top => 0.06);
   end;

   procedure leyenda_asigs_agrupacion(pSemestre in number, vAgrupacion in number) is
   begin
      fop.block(' ', padding_top => 1);

      for item in c_asigs_agrupacion(pSemestre, vAgrupacion) loop
         muestra_leyenda_asig_agr(item, pSemestre, vAgrupacion);
      end loop;
   end;

   procedure pie is
   begin
      fop.pageFlowClose;
      fop.pageBodyClose;
      fop.documentClose;
   end;

   function redondea_fecha_a_cuartos(vIniDate date)
      return date is
      fecha_redondeada          date;
   begin
      select trunc(vIniDate, 'HH') + (15 * round(to_char(trunc(vIniDate, 'MI'), 'MI') / 15)) / 1440
        into fecha_redondeada
        from dual;

      return fecha_redondeada;
   end;

   function con_varios_grupos
      return boolean is
   begin
      return instr(pGrupo, ',') <> 0;
   end;

   procedure calendario_gen(pSemestre in number, pCurso in number) is
      grupo_txt              varchar2(200);
      varios_grupos          boolean;
   begin
      varios_grupos := con_varios_grupos();
      vFechas := t_horario();

      for item in c_items_gen(pSemestre, pCurso) loop
         if (varios_grupos) then
            grupo_txt := item.grupo_id || ' ' || '(' || item.tipo_subgrupo_id || item.subgrupo_id || ') ' || item.codigo_aula;
         else
            grupo_txt := '(' || item.tipo_subgrupo_id || item.subgrupo_id || ') ' || item.codigo_aula;
         end if;

         vFechas.extend;
         vFechas(vFechas.count) := ItemHorarios(hor_contenido_item(redondea_fecha_a_cuartos(item.hora_inicio), redondea_fecha_a_cuartos(item.hora_fin), item.asignatura_id, grupo_txt), redondea_fecha_a_cuartos(item.inicio), redondea_fecha_a_cuartos(item.fin), null);
      end loop;

      if vFechas.count > 0 then
         vRdo := xpfpkg_horarios_academicos.drawTimetable(vFechas, 5, null, 'CA', 'PDF_APP_HORARIOS');
         xup_utils.dump(vRdo);
         dbms_lob.freeTemporary(vRdo);
      end if;
   end;

   procedure calendario_gen_agrupacion(pSemestre in number, pAgrupacion in number) is
      grupo_txt              varchar2(200);
      varios_grupos          boolean;
   begin
      varios_grupos := con_varios_grupos();
      vFechas := t_horario();

      for item in c_items_gen_agrupacion(pSemestre, pAgrupacion) loop
         if (varios_grupos) then
            grupo_txt := item.grupo_id || ' ' || '(' || item.tipo_subgrupo_id || item.subgrupo_id || ') ' || item.codigo_aula;
         else
            grupo_txt := '(' || item.tipo_subgrupo_id || item.subgrupo_id || ') ' || item.codigo_aula;
         end if;

         vFechas.extend;
         vFechas(vFechas.count) := ItemHorarios(hor_contenido_item(redondea_fecha_a_cuartos(item.hora_inicio), redondea_fecha_a_cuartos(item.hora_fin), item.asignatura_id, grupo_txt), redondea_fecha_a_cuartos(item.inicio), redondea_fecha_a_cuartos(item.fin), null);
      end loop;

      if vFechas.count > 0 then
         vRdo := xpfpkg_horarios_academicos.drawTimetable(vFechas, 5, null, 'CA', 'PDF_APP_HORARIOS');
         xup_utils.dump(vRdo);
         dbms_lob.freeTemporary(vRdo);
      end if;
   end;

   procedure calendario_det(vFechaIni date, vFechaFin date) is
      grupo_txt              varchar2(200);
      varios_grupos          boolean;
   begin
      varios_grupos := con_varios_grupos();
      vFechas := t_horario();

      for item in c_items_det(vFechaIni, vFechaFin, pCurso) loop
         if (varios_grupos) then
            grupo_txt := item.grupo_id || ' ' || '(' || item.tipo_subgrupo_id || item.subgrupo_id || ') ' || item.codigo_aula;
         else
            grupo_txt := '(' || item.tipo_subgrupo_id || item.subgrupo_id || ') ' || item.codigo_aula;
         end if;

         vFechas.extend;
         vFechas(vFechas.count) := ItemHorarios(hor_contenido_item(redondea_fecha_a_cuartos(item.hora_inicio), redondea_fecha_a_cuartos(item.hora_fin), item.asignatura_id, grupo_txt), redondea_fecha_a_cuartos(item.inicio), redondea_fecha_a_cuartos(item.fin), null);
      end loop;

      if vFechas.count > 0 then
         vRdo := xpfpkg_horarios_academicos.drawTimetable(vFechas, 5, null, 'CA', 'PDF_APP_HORARIOS_DET');
         xup_utils.dump(vRdo);
         dbms_lob.freeTemporary(vRdo);
      end if;
   end;

   procedure calendario_det_agrupacion(vFechaIni date, vFechaFin date, pAgrupacion in number) is
      grupo_txt              varchar2(200);
      varios_grupos          boolean;
   begin
      varios_grupos := con_varios_grupos();
      vFechas := t_horario();

      for item in c_items_det_agrupacion(vFechaIni, vFechaFin, pAgrupacion) loop
         if (varios_grupos) then
            grupo_txt := item.grupo_id || ' ' || '(' || item.tipo_subgrupo_id || item.subgrupo_id || ') ' || item.codigo_aula;
         else
            grupo_txt := '(' || item.tipo_subgrupo_id || item.subgrupo_id || ') ' || item.codigo_aula;
         end if;

         vFechas.extend;
         vFechas(vFechas.count) := ItemHorarios(hor_contenido_item(redondea_fecha_a_cuartos(item.hora_inicio), redondea_fecha_a_cuartos(item.hora_fin), item.asignatura_id, grupo_txt), redondea_fecha_a_cuartos(item.inicio), redondea_fecha_a_cuartos(item.fin), null);
      end loop;

      if vFechas.count > 0 then
         vRdo := xpfpkg_horarios_academicos.drawTimetable(vFechas, 5, null, 'CA', 'PDF_APP_HORARIOS_DET');
         xup_utils.dump(vRdo);
         dbms_lob.freeTemporary(vRdo);
      end if;
   end;

   procedure calcula_fechas_semestre(pSemestre in number) is
   begin
      select fecha_inicio, fecha_examenes_fin
        into vFechaIniSemestre, vFechaFinSemestre
        from hor_semestres_detalle, hor_estudios e
       where semestre_id = pSemestre
         and e.id = pEstudio
         and e.tipo_id = tipo_estudio_id;

      select next_day(trunc(vFechaIniSemestre) - 7, 'LUN')
        into vFechaIniSemestre
        from dual;
   end;

   function semana_tiene_clase(vIniDate date, vFinDate date, pCurso in number)
      return boolean is
      num_clases          number;
   begin
        select count( * )
          into num_clases
          from UJI_HORARIOS.HOR_ITEMS items, UJI_HORARIOS.HOR_ITEMS_DETALLE det, uji_horarios.hor_items_asignaturas asi
         where items.id = asi.item_id
           and asi.estudio_id = pEstudio
           and asi.curso_id = pCurso
           and ',' || pGrupo || ',' like '%,' || items.grupo_id || ',%'
           and items.semestre_id = pSemestre
           and items.dia_semana_id is not null
           and det.item_id = items.id
           and det.inicio > vIniDate
           and det.fin < vFinDate
      order by det.inicio;

      return num_clases > 0;
   end;

   function semana_tiene_clase_agr(vIniDate date, vFinDate date, pAgrupacion in number)
      return boolean is
      num_clases          number;
   begin
        select count( * )
          into num_clases
          from UJI_HORARIOS.HOR_ITEMS items, UJI_HORARIOS.HOR_ITEMS_DETALLE det, uji_horarios.hor_items_asignaturas asi, HOR_V_AGRUPACIONES_ITEMS agr_items
         where items.id = asi.item_id
           and asi.estudio_id = pEstudio
           and ',' || pGrupo || ',' like '%,' || items.grupo_id || ',%'
           and items.semestre_id = pSemestre
           and items.dia_semana_id is not null
           and det.item_id = items.id
           and items.id = agr_items.item_id
           and agr_items.agrupacion_id = pAgrupacion
           and det.inicio > vIniDate
           and det.fin < vFinDate
      order by det.inicio;

      return num_clases > 0;
   end;

   procedure info_calendario_det(vFechaIni date, vFechaFin date, pCurso in number) is
      vCursoAca                  varchar2(4000);
      vTitulacionNombre          varchar2(4000);
      vCursoNombre               varchar2(4000) := '';
      vTextoCabecera             varchar2(4000);
      vFechaIniStr               varchar2(4000) := to_char(vFechaIni, 'dd/mm/yyyy');
      vFechaFinStr               varchar2(4000) := to_char(vFechaFin - 1, 'dd/mm/yyyy');
      vNombreAgrupacion          varchar2(4000) := '';
   begin
      select nombre
        into vTitulacionNombre
        from hor_estudios
       where id = pEstudio;

      if (pAgrupacion is not null) then
         select 'Agrupació ' || nombre
           into vNombreAgrupacion
           from hor_agrupaciones
          where id = pAgrupacion;
      end if;

      vCursoAca := to_char(vCursoAcaNum) || '/' || to_char(vCursoAcaNum + 1);

      if pCurso in ('1', '3') then
         vCursoNombre := pCurso || 'er curs';
      elsif pCurso = '2' then
         vCursoNombre := '2on  curs';
      elsif pCurso = '4' then
         vCursoNombre := '4t curs';
      elsif pCurso is not null then
         vCursoNombre := pCurso || '?  curs';
      end if;

      vTextoCabecera := vTitulacionNombre || ' - ' || vCursoNombre || vNombreAgrupacion || ' - Grup ' || pGrupo || ' (semestre ' || pSemestre || ') - Del ' || vFechaIniStr || ' al ' || vFechaFinStr;
      fop.block(text => vTextoCabecera, border_bottom => 'solid', border_width => 0.05);
   end;

   procedure paginas_calendario_det(pSemestre in number, pCurso in number) is
      vFechaIniSemana          date;
      vFechaFinSemana          date;
   begin
      calcula_fechas_semestre(pSemestre);
      vFechaIniSemana := vFechaIniSemestre;
      vFechaFinSemana := vFechaIniSemana + 7;

      while vFechaFinSemana <= vFechaFinSemestre loop
         if semana_tiene_clase(vFechaIniSemana, vFechaFinSemana, pCurso) then
            info_calendario_det(vFechaIniSemana, vFechaFinSemana, pCurso);
            calendario_det(vFechaIniSemana, vFechaFinSemana);
            htp.p('<fo:block break-before="page" color="white">_</fo:block>');
         end if;

         vFechaIniSemana := vFechaFinSemana;
         vFechaFinSemana := vFechaFinSemana + 7;
      end loop;
   end;

   procedure paginas_calendario_det_agr(pSemestre in number, pCurso in number, pAgrupacion in number) is
      vFechaIniSemana          date;
      vFechaFinSemana          date;
   begin
      calcula_fechas_semestre(pSemestre);
      vFechaIniSemana := vFechaIniSemestre;
      vFechaFinSemana := vFechaIniSemana + 7;

      while vFechaFinSemana <= vFechaFinSemestre loop
         if semana_tiene_clase_agr(vFechaIniSemana, vFechaFinSemana, pAgrupacion) then
            info_calendario_det(vFechaIniSemana, vFechaFinSemana, pCurso);
            calendario_det_agrupacion(vFechaIniSemana, vFechaFinSemana, pAgrupacion);
            htp.p('<fo:block break-before="page" color="white">_</fo:block>');
         end if;
         vFechaIniSemana := vFechaFinSemana;
         vFechaFinSemana := vFechaFinSemana + 7;
      end loop;
   end;

   procedure pageBreak is
   begin
      if (not vFirstPageBreak) then
         htp.p('<fo:block break-before="page" color="white">_</fo:block>');
      end if;
      vFirstPageBreak := false;
   end;

   procedure impresionSemanaGenerica is
   begin
      if (pCurso is not null) then
         for c in cursosTitulacion(pEstudio, pCurso) loop
            if (pSemestre = 1
             or pSemestre = -1) then
               pageBreak();
               info_calendario_gen(1, c.curso_id, pAgrupacion);
               calendario_gen(1, c.curso_id);
               leyenda_asigs(1, c.curso_id);
            end if;

            if (pSemestre = 2
             or pSemestre = -1) then
               pageBreak();
               info_calendario_gen(2, c.curso_id, pAgrupacion);
               calendario_gen(2, c.curso_id);
               leyenda_asigs(2, c.curso_id);
            end if;
         end loop;
      else
         --fop.block('HI', font_weight => 'bold', text_align => 'center', font_size => 14, space_after => 0.5);
         for a in agrupacionesTitulacion(pEstudio, pAgrupacion) loop
            if (pSemestre = 1
             or pSemestre = -1) then
               pageBreak();
               info_calendario_gen(1, pCurso, a.agrupacion_id);
               calendario_gen_agrupacion(1, a.agrupacion_id);
               leyenda_asigs_agrupacion(1, a.agrupacion_id);
            end if;

            if (pSemestre = 2
             or pSemestre = -1) then
               pageBreak();
               info_calendario_gen(2, pCurso, a.agrupacion_id);
               calendario_gen_agrupacion(2, a.agrupacion_id);
               leyenda_asigs_agrupacion(2, a.agrupacion_id);
            end if;
         end loop;
      end if;
   end;

   procedure impresionSemanaDetalle is
   begin
      if (pCurso is not null) then
         for c in cursosTitulacion(pEstudio, pCurso) loop
            paginas_calendario_det(pSemestre, c.curso_id);
         end loop;
      else
         paginas_calendario_det_agr(pSemestre, pCurso, pAgrupacion);
      end if;

      leyenda_asigs(pSemestre, pCurso);
   end;
begin
   select count( * )
     into vSesion
     from apa_sesiones
    where sesion_id = pSesion
      and fecha < sysdate() - 0.33;

   if vSesion > 0 then
      calcula_curso_aca;
      cabecera;

      if (pGrupo = '-1') then
         select listagg(grupo_id, ',') within group (order by grupo_id)
           into pGrupo
           from (  select distinct i.grupo_id
                     from hor_items i join hor_items_asignaturas ia on i.id = ia.item_id
                    where ia.estudio_id = pEstudio
                 order by i.grupo_id);
      end if;

      if vEsGenerica then
         impresionSemanaGenerica;
      else
         impresionSemanaDetalle;
      end if;

      pie;
   else
      cabecera;
      fop.block('No tens permís per accedir a aquest document.', font_weight => 'bold', text_align => 'center', font_size => 14, space_after => 0.5);
      pie;
   end if;
exception
   when others then
      htp.p(sqlerrm);
end;



ALTER TABLE UJI_HORARIOS.HOR_PERMISOS_EXTRA
 ADD (persona_id_otorga  NUMBER);
 

create or replace force view UJI_HORARIOS.HOR_V_ITEMS_CREDITOS_DETALLE
(
   ID
 , NOMBRE
 , CREDITOS_PROFESOR
 , ITEM_ID
 , CRD_PROF
 , CRD_PACTADOS
 , CRD_ITEM
 , CRD_ITEM_PACTADOS
 , CUANTOS
 , CRD_FINAL
 , CRD_PACTADOS_FINAL
 , GRUPO_ID
 , TIPO
 , ASIGNATURA_ID
 , ASIGNATURA_TXT
 , DEPARTAMENTO_ID
 , AREA_ID
)
   bequeath definer as
   select id, nombre, creditos_profesor, item_id, round(crd_prof, 2) crd_prof, round(crd_pactados, 2) crd_pactados, crd_item, crd_item_pactados, cuantos, round(nvl(crd_prof, trunc(crd_item / cuantos, 2)), 2) crd_final, round(nvl(crd_pactados, nvl(crd_prof, trunc(crd_item_pactados / cuantos, 2))), 2) crd_pactados_final, GRUPO_ID, TIPO_SUBGRUPO_ID || SUBGRUPO_ID tipo, asignatura_id, asignatura_txt, departamento_id, area_id
     from (  select h.id
                  , nombre
                  , h.creditos                                                 creditos_profesor
                  , ip.item_id
                  , ip.creditos                                                crd_prof
                  , nvl(ip.creditos_computables, i.creditos)                   crd_pactados
                  , i.creditos                                                 crd_item
                  , nvl(i.creditos_computables, i.creditos)                    crd_item_pactados
                  , (select count(*)
                       from hor_items_profesores ip2
                      where item_id = ip.item_id)
                       cuantos
                  , GRUPO_ID
                  , TIPO_SUBGRUPO_ID
                  , SUBGRUPO_ID
                  , listagg(asignatura_id) within group (order by asignatura_id) asignatura_txt
                  , min(asignatura_id)                                         asignatura_id
                  , departamento_id
                  , area_id
               from hor_profesores      h
                  , hor_items_profesores ip
                  , hor_items           i
                  , hor_items_asignaturas ia
              where h.id = ip.profesor_id
                and ip.item_id = i.id
                and i.id = ia.item_id
           group by h.id
                  , nombre
                  , h.creditos
                  , ip.item_id
                  , ip.creditos
                  , ip.creditos_computables
                  , i.creditos
                  , i.creditos_computables
                  , GRUPO_ID
                  , TIPO_SUBGRUPO_ID
                  , SUBGRUPO_ID
                  , departamento_id
                  , area_id) x
   union all
   select id, nombre, creditos creditos_profesor, null item_id, null crd_prof, null crd_pactados, null crd_item, null crd_item_pactados, 0 cuantos, 0 crd_final, 0 crd_pactados_final, null grupo_id, null tipo, null asignatura_id, null asignatura_txt, departamento_id, area_id
     from hor_profesores h
    where not exists
             (select profesor_id
                from hor_items_profesores
               where profesor_id = h.id);