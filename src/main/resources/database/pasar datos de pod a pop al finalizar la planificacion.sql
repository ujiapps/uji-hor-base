/* Formatted on 08/07/2014 08:13 (Formatter Plus v4.8.8) */

declare
   v_txt         varchar2 (2000);
   v_curso_aca   number          := 2014;
   v_acciones    varchar2 (1)    := 'S';

   cursor lista is
      select   asignatura_id, substr (grupo_id, 1, 1) grupo_id, subgrupo_id, tipo_subgrupo_id,
               max (nvl (ip.creditos, i.creditos)) creditos, 'N' idioma, profesor_id, area_id, ip.detalle_manual,
               estudio_id
          from uji_horarios.hor_items i,
               uji_horarios.hor_items_asignaturas a,
               uji_horarios.hor_items_profesores ip,
               uji_horarios.hor_profesores p
         where i.id = a.item_id
           and i.id = ip.item_id
           --and comun = 0
           and ip.profesor_id = p.id
           --and asignatura_id = 'EM1045'
           and estudio_id not between 201 and 9999
      --and tipo_subgrupo_id = 'AV'
      --and 1 = 2
      group by asignatura_id,
               grupo_id,
               subgrupo_id,
               tipo_subgrupo_id,
               profesor_id,
               area_id,
               estudio_id,
               ip.detalle_manual;

   cursor lista_inicial is
      select 'ALTER TRIGGER gra_pop.TRG_GRUPOS_PDI_BLOQUEADO DISABLE' accion
        from dual;

   cursor lista_final is
      select 'ALTER TRIGGER gra_pop.TRG_GRUPOS_PDI_BLOQUEADO enable' accion
        from dual;

   procedure ejecutar (p_accion in varchar2) is
   begin
      execute immediate p_accion;
   end;

   procedure desactivar_triggers is
   begin
      for x in lista_inicial loop
         ejecutar (x.accion);
      end loop;
   end;

   procedure activar_triggers is
   begin
      for x in lista_final loop
         ejecutar (x.accion);
      end loop;
   end;
begin
   dbms_output.put_line (' ');
   dbms_output.put_line ('---------------------------------------------------------------');
   dbms_output.put_line (v_curso_aca);
   dbms_output.put_line (' ');
   desactivar_triggers;

   for x in lista loop
      begin
         dbms_output.put_line ('insertar ' || x.asignatura_id || ' - ' || x.grupo_id || ' - ' || v_curso_aca || ' - '
                               || x.subgrupo_id || ' - ' || x.tipo_subgrupo_id || ' - ' || x.creditos || ' - '
                               || x.idioma || ' - ' || x.profesor_id || ' - ' || v_curso_aca || ' - ' || x.area_id
                               || ' - detalla ' || x.detalle_manual);

         if v_acciones = 'S' then
            insert into pop_grupos_pdi
                        (PGR_POF_CURSO_ACA, PGR_POF_PMAS_ID, PGR_POF_PASI_ID, PGR_ID, PER_ID, CRD_IMPARTIDOS, IDIOMA,
                         CRD_COMPUTABLES
                        )
            values      (v_curso_aca, x.estudio_id, x.asignatura_id, x.grupo_id, x.profesor_id, x.creditos, x.idioma,
                         x.creditos
                        );

            commit;
         end if;
      exception
         when others then
            dbms_output.put_line ('ERROR  ----- ' || v_curso_aca || ' - ' || x.estudio_id || ' - ' || x.asignatura_id
                                  || ' - ' || x.grupo_id || ' - ' || x.profesor_id || ' - ' || x.creditos || ' - '
                                  || x.idioma || ' - ' || x.creditos || ' ' || sqlerrm);
      end;
   end loop;

   activar_triggers;
end;


---------------------------------------






/* Formatted on 08/07/2014 16:33 (Formatter Plus v4.8.8) */

declare
   v_txt         varchar2 (2000);
   v_curso_aca   number          := 2014;
   v_acciones    varchar2 (1)    := 'S';

   cursor lista is
      select   :v_curso_aca curso_aca, estudio_id, asignatura_id, substr (grupo_id, 1, 1) grupo_id, profesor_id,
               max (nvl (creditos_prof, round (creditos_item / cuantos_prof, 2))) creditos_impartidos, 'N' idioma,
               max (nvl (creditos_prof, round (creditos_item / cuantos_prof, 2))) creditos_computables
          from (select asignatura_id, substr (grupo_id, 1, 1) grupo_id, subgrupo_id, tipo_subgrupo_id,
                       ip.creditos creditos_prof, i.creditos creditos_item, profesor_id, area_id, ip.detalle_manual,
                       estudio_id,
                       count (*) over (partition by asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo_id)
                                                                                                           cuantos_prof
                  from uji_horarios.hor_items i,
                       uji_horarios.hor_items_asignaturas a,
                       uji_horarios.hor_items_profesores ip,
                       uji_horarios.hor_profesores p
                 where i.id = a.item_id
                   and i.id = ip.item_id
                   --and comun = 0
                   and ip.profesor_id = p.id
                   --and asignatura_id = 'SIU029'
                   and estudio_id not between 201 and 9999
                                                          --and tipo_subgrupo_id = 'AV'
                                                          --and 1 = 2
               )
      group by asignatura_id,
               grupo_id,
               subgrupo_id,
               tipo_subgrupo_id,
               profesor_id,
               area_id,
               estudio_id,
               detalle_manual
      minus
      select pgr_pof_curso_aca, pgr_pof_pmas_id, pgr_pof_pasi_id, to_char (pgr_id), per_id, crd_impartidos, idioma,
             crd_computables
        from pop_grupos_pdi
       where pgr_pof_curso_aca = :v_curso_aca;

   cursor lista_inicial is
      select 'ALTER TRIGGER gra_pop.TRG_GRUPOS_PDI_BLOQUEADO DISABLE' accion
        from dual;

   cursor lista_final is
      select 'ALTER TRIGGER gra_pop.TRG_GRUPOS_PDI_BLOQUEADO enable' accion
        from dual;

   procedure ejecutar (p_accion in varchar2) is
   begin
      execute immediate p_accion;
   end;

   procedure desactivar_triggers is
   begin
      for x in lista_inicial loop
         ejecutar (x.accion);
      end loop;
   end;

   procedure activar_triggers is
   begin
      for x in lista_final loop
         ejecutar (x.accion);
      end loop;
   end;
begin
   dbms_output.put_line (' ');
   dbms_output.put_line ('---------------------------------------------------------------');
   dbms_output.put_line (v_curso_aca);
   dbms_output.put_line (' ');
   desactivar_triggers;

   for x in lista loop
      begin
         if v_acciones = 'S' then
            begin
               insert into pop_grupos_pdi
                           (PGR_POF_CURSO_ACA, PGR_POF_PMAS_ID, PGR_POF_PASI_ID, PGR_ID, PER_ID,
                            CRD_IMPARTIDOS, IDIOMA, CRD_COMPUTABLES
                           )
               values      (x.curso_aca, x.estudio_id, x.asignatura_id, x.grupo_id, x.profesor_id,
                            x.creditos_impartidos, x.idioma, x.creditos_computables
                           );

               dbms_output.put_line ('insert crd ' || x.asignatura_id || ' - ' || x.grupo_id || ' - ' || v_curso_aca
                                     || ' - ' || x.creditos_impartidos || ' - ' || x.idioma || ' - ' || x.profesor_id
                                     || ' - ' || v_curso_aca);
            exception
               when others then
                  update pop_grupos_pdi
                  set crd_impartidos = x.creditos_impartidos,
                      crd_computables = x.creditos_computables
                  where  PGR_POF_CURSO_ACA = x.curso_aca
                     and PGR_POF_PMAS_ID = x.estudio_id
                     and PGR_POF_PASI_ID = x.asignatura_id
                     and PGR_ID = x.grupo_id
                     and PER_ID = x.profesor_id;

                  dbms_output.put_line ('update crd ' || x.asignatura_id || ' - ' || x.grupo_id || ' - ' || v_curso_aca
                                        || ' - ' || x.creditos_impartidos || ' - ' || x.idioma || ' - ' || x.profesor_id
                                        || ' - ' || v_curso_aca);
            end;

            commit;
         end if;
      exception
         when others then
            dbms_output.put_line ('ERROR  ----- ' || v_curso_aca || ' - ' || x.estudio_id || ' - ' || x.asignatura_id
                                  || ' - ' || x.grupo_id || ' - ' || x.profesor_id || ' - ' || x.creditos_impartidos
                                  || ' - ' || x.idioma || ' ' || sqlerrm);
      end;
   end loop;

   activar_triggers;
end;




