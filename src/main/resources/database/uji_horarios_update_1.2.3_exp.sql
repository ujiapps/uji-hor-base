CREATE OR REPLACE FORCE VIEW UJI_HORARIOS.HOR_V_FECHAS_CONVOCATORIAS_EST (ID,
                                                                          NOMBRE,
                                                                          FECHA_EXAMENES_INICIO,
                                                                          FECHA_EXAMENES_FIN,
                                                                          ESTUDIO_ID
                                                                         ) AS
   select c.id, c.nombre, c.fecha_examenes_inicio, c.fecha_examenes_fin, e.id estudio_id
     from hor_v_Fechas_convocatorias c,
          hor_estudios e
    where e.id not in (select estudio_id
                         from hor_semestres_detalle_estudio de)
   union all
   select c.id, c.nombre, c.fecha_examenes_inicio, de.fecha_examenes_fin, de.estudio_id
     from hor_v_Fechas_convocatorias c,
          hor_estudios e,
          hor_semestres_detalle d,
          hor_semestres_detalle_estudio de
    where e.id = de.estudio_id
      and d.id = de.detalle_id
      and c.id = 9
      and semestre_id = 1
   union all
   select c.id, c.nombre, c.fecha_examenes_inicio, de.fecha_examenes_fin, de.estudio_id
     from hor_v_Fechas_convocatorias c,
          hor_estudios e,
          hor_semestres_detalle d,
          hor_semestres_detalle_estudio de
    where e.id = de.estudio_id
      and d.id = de.detalle_id
      and c.id = 10
      and semestre_id = 2
   union all
   select distinct c.id, c.nombre, c.fecha_examenes_inicio, de.fecha_examenes_fin_c2, de.estudio_id
              from hor_v_Fechas_convocatorias c,
                   hor_estudios e,
                   hor_semestres_detalle d,
                   hor_semestres_detalle_estudio de
             where e.id = de.estudio_id
               and d.id = de.detalle_id
               and c.id = 11;

			   
CREATE TABLE uji_horarios.hor_agrupaciones 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2 (1000)  NOT NULL 
    ) 
;



ALTER TABLE uji_horarios.hor_agrupaciones 
    ADD CONSTRAINT hor_agrupaciones_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_horarios.hor_agrupaciones_asignaturas 
    ( 
     id NUMBER  NOT NULL , 
     agrupacion_id NUMBER  NOT NULL , 
     asignatura_id VARCHAR2 (100)  NOT NULL 
    ) 
;


CREATE INDEX uji_horarios.hor_agr_asi_agr_IDX ON uji_horarios.hor_agrupaciones_asignaturas 
    ( 
     agrupacion_id ASC 
    ) 
;

ALTER TABLE uji_horarios.hor_agrupaciones_asignaturas 
    ADD CONSTRAINT hor_agr_asi_PK PRIMARY KEY ( id ) ;


ALTER TABLE uji_horarios.hor_agrupaciones_asignaturas 
    ADD CONSTRAINT hor_agr_asi_UN UNIQUE ( agrupacion_id , asignatura_id ) ;



CREATE TABLE uji_horarios.hor_agrupaciones_estudios 
    ( 
     id NUMBER  NOT NULL , 
     agrupacion_id NUMBER  NOT NULL , 
     estudio_id NUMBER  NOT NULL 
    ) 
;


CREATE INDEX uji_horarios.hor_agr_est_agr_IDX ON uji_horarios.hor_agrupaciones_estudios 
    ( 
     agrupacion_id ASC 
    ) 
;
CREATE INDEX uji_horarios.hor_agr_est_est_IDX ON uji_horarios.hor_agrupaciones_estudios 
    ( 
     estudio_id ASC 
    ) 
;

ALTER TABLE uji_horarios.hor_agrupaciones_estudios 
    ADD CONSTRAINT hor_agrupaciones_estudios_PK PRIMARY KEY ( id ) ;




ALTER TABLE uji_horarios.hor_agrupaciones_asignaturas 
    ADD CONSTRAINT hor_agr_asi_agr_FK FOREIGN KEY 
    ( 
     agrupacion_id
    ) 
    REFERENCES uji_horarios.hor_agrupaciones 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_agrupaciones_estudios 
    ADD CONSTRAINT hor_agr_est_agr_FK FOREIGN KEY 
    ( 
     agrupacion_id
    ) 
    REFERENCES uji_horarios.hor_agrupaciones 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_agrupaciones_estudios 
    ADD CONSTRAINT hor_agr_est_est_FK FOREIGN KEY 
    ( 
     estudio_id
    ) 
    REFERENCES uji_horarios.hor_estudios 
    ( 
     id
    ) 
;

CREATE OR REPLACE FORCE VIEW UJI_HORARIOS.HOR_V_ASIGNATURAS (ID,
                                                             ASIGNATURA,
                                                             ESTUDIO_ID,
                                                             ESTUDIO,
                                                             CURSO_ID,
                                                             CARACTER_ID,
                                                             CARACTER
                                                            ) AS
   select distinct asignatura_id id, asignatura, estudio_id, estudio, curso_id, caracter_id, caracter
              from hor_items_asignaturas;

			   
CREATE OR REPLACE PROCEDURE UJI_HORARIOS.CIRCUITOS_VALIDACION_PDF (
   name_array    IN   euji_util.ident_arr2,
   value_array   IN   euji_util.ident_arr2
) IS
   -- Parametros
   pEstudio    CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pEstudio');
   pCircuito   CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pCircuito');
   pGrupo      CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pGrupo');
   --pCurso      CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pCurso');
   pSesion     CONSTANT VARCHAR2 (4000) := euji_util.euji_getparam (name_array, value_array, 'pSesion');
   v_nombre             varchar2 (2000);
   vSesion              number;

   cursor lista_circuito is
      select grupo_id, nombre, plazas
      from   hor_circuitos
      where  id = pCircuito
      and    grupo_id = pGrupo;

   cursor lista_semestres is
      select   1 id
      from     dual
      union all
      select   2 id
      from     dual
      order by 1;

   procedure cabecera is
   begin
      owa_util.mime_header (ccontent_type => 'text/xml', bclose_header => true);
      fop.documentOpen (margin_left => 1.5, margin_right => 1.5, margin_top => 0.5, margin_bottom => 0.5,
                        extent_before => 3, extent_after => 1.5, extent_margin_top => 3, extent_margin_bottom => 1.5);
      fop.pageBodyOpen;

      for x in lista_circuito loop
         htp.p (fof.documentheaderopen || fof.blockopen (space_after => 0.1) || fof.tableOpen
                || fof.tablecolumndefinition (column_width => 3) || fof.tablecolumndefinition (column_width => 15)
                || fof.tablebodyopen || fof.tableRowOpen || fof.tablecellopen
                || fof.block (fof.logo_uji, text_align => 'center') || fof.tablecellclose
                || fof.tablecellopen (display_align => 'center', padding => 0.4)
                || fof.block (text => 'Controls Circuits', font_size => 12, font_weight => 'bold',
                              font_family => 'Arial', text_align => 'center')
                || fof.block (text => v_nombre, font_size => 12, font_weight => 'bold', font_family => 'Arial',
                              text_align => 'center')
                || fof.block (text => 'Curs: 1 - Grup: ' || pGrupo || ' - Circuit: ' || x.nombre || ' (' || x.plazas
                               || ' pla�es)',
                              font_size => 12, font_weight => 'bold', font_family => 'Arial', text_align => 'center')
                || fof.tablecellclose || fof.tableRowClose || fof.tablebodyclose || fof.tableClose || fof.blockclose
                || fof.documentheaderclose);
         -- El numero paginas se escribe a mano para poner el texto m?s peque?o
         htp.p (fof.documentfooteropen || fof.tableOpen || fof.tablecolumndefinition (column_width => 18)
                || fof.tablebodyopen || fof.tableRowOpen || fof.tablecellopen
                || fof.block (text => 'Pag. <fo:page-number /> de <fo:page-number-citation ref-id="lastBlock" />',
                              text_align => 'right', font_weight => 'bold', font_size => 8)
                || fof.tablecellclose || fof.tableRowClose || fof.tablebodyclose || fof.tableClose
                || fof.documentfooterclose);
      end loop;

      fop.pageFlowOpen;
      fop.block (fof.white_space);
   end;

   procedure pie is
   begin
      htp.p ('<fo:block  id="lastBlock" space-before="1cm" font-size="8pt">');
      htp.p ('Data de generaci�: ' || to_char (sysdate, 'dd/mm/yyy hh24:mi'));
      fop.blockclose ();
      fop.pageFlowClose;
      fop.pageBodyClose;
      fop.documentClose;
   end;

   function espacios (p_espacios in number)
      return varchar2 is
      v_rdo   varchar2 (2000);
   begin
      v_rdo := null;

      for x in 1 .. p_espacios loop
         v_rdo := v_rdo || fof.white_space;
      end loop;

      return v_rdo;
   end;

   procedure p (p_texto in varchar2) is
   begin
      fop.block (espacios (3) || p_texto);
   end;

   procedure control_1 (pSemestre in number) is
      v_aux   number := 0;

      cursor lista_errores_1 is
         select   *
         from     (select distinct ia.asignatura_id, tipo_subgrupo_id, grupo_id
                   from            hor_items i,
                                   hor_items_asignaturas ia
                   where           ia.item_id = i.id
                   and             ia.estudio_id = pEstudio
                   and             semestre_id = pSemestre
                   and             grupo_id = pGrupo
                   and             tipo_subgrupo_id not in ('AV', 'TU')
                   and             ia.curso_id = 1
                   minus
                   select distinct ia.asignatura_id, tipo_subgrupo_id, grupo_id
                   from            hor_items_circuitos ic,
                                   hor_items i,
                                   hor_items_asignaturas ia
                   where           circuito_id = pCircuito
                   and             i.id = ic.item_id
                   and             ia.item_id = i.id
                   and             ia.estudio_id = pEstudio
                   and             semestre_id = pSemestre
                   and             grupo_id = pGrupo)
         order by 1,
                  decode (tipo_subgrupo_id, 'TE', 1, 'PR', 2, 'LA', 3, 'SE', 4, 5);
   begin
      fop.block ('Control 1 - Subgrups que no estan en el circuit', font_size => 12, border_bottom => 'solid',
                 space_after => 0.1);

      for x in lista_errores_1 loop
         v_aux := v_aux + 1;
         p (x.asignatura_id || ' ' || x.tipo_subgrupo_id);
      end loop;

      if v_aux = 0 then
         fop.block ('No hi ha cap error.', space_before => 0.5);
      elsif v_aux = 1 then
         fop.block ('Trovat ' || v_aux || ' error.', space_before => 0.5, space_after => 0.5);
      else
         fop.block ('Trovats ' || v_aux || ' errors.', space_before => 0.5, space_after => 0.5);
      end if;
   exception
      when others then
         p (sqlerrm);
   end;

   procedure control_2 (pSemestre in number) is
      v_aux   number := 0;

      cursor lista_errores_2 is
         select distinct ia.asignatura_id, tipo_subgrupo_id, subgrupo_id, grupo_id
         from            hor_items_circuitos ic,
                         hor_items i,
                         hor_items_asignaturas ia
         where           circuito_id = pCircuito
         and             i.id = ic.item_id
         and             ia.item_id = i.id
         and             ia.estudio_id = pEstudio
         and             semestre_id = pSemestre
         and             grupo_id = pGrupo
         and             (asignatura_id, grupo_id, tipo_subgrupo_id) in (
                            select   asignatura_id, grupo_id, tipo_subgrupo_id
                            from     (select distinct ia.asignatura_id, tipo_subgrupo_id, subgrupo_id, grupo_id
                                      from            hor_items_circuitos ic,
                                                      hor_items i,
                                                      hor_items_asignaturas ia
                                      where           circuito_id = pCircuito
                                      and             i.id = ic.item_id
                                      and             ia.item_id = i.id
                                      and             ia.estudio_id = pEstudio
                                      and             semestre_id = pSemestre
                                      and             grupo_id = pGrupo)
                            group by asignatura_id,
                                     grupo_id,
                                     tipo_subgrupo_id
                            having   count (*) > 1)
         order by        1,
                         decode (tipo_subgrupo_id, 'TE', 1, 'PR', 2, 'LA', 3, 'SE', 4, 5),
                         3;
   begin
      fop.block ('Control 2 - Assignatures amb m�s d''un subgrup per tipus al circuit', font_size => 12,
                 border_bottom => 'solid', space_after => 0.1);

      for x in lista_errores_2 loop
         v_aux := v_aux + 1;
         p (x.asignatura_id || ' ' || x.tipo_subgrupo_id || x.subgrupo_id);
      end loop;

      if v_aux = 0 then
         fop.block ('No hi ha cap error.', space_before => 0.5);
      elsif v_aux = 1 then
         fop.block ('Trovat ' || v_aux || ' error.', space_before => 0.5, space_after => 0.5);
      else
         fop.block ('Trovats ' || v_aux || ' errors.', space_before => 0.5, space_after => 0.5);
      end if;
   exception
      when others then
         p (sqlerrm);
   end;

   procedure control_3 is
      v_aux   number := 0;

      cursor lista_errores_3 is
         select a.estudio_id, a.asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo_id, i.id
         from   hor_items_circuitos c,
                hor_items i,
                hor_circuitos_estudios e,
                hor_items_asignaturas a
         where  c.circuito_id = e.circuito_id
         and    c.item_id = i.id
         and    dia_semana_id is null
         and    c.circuito_id = pCircuito
         and    e.estudio_id = pEstudio
         and    i.id = a.item_id
         and    e.estudio_id = a.estudio_id;
   begin
      fop.block ('Control 3 - Assignatures o clases sense planificar en el circuit', font_size => 12,
                 border_bottom => 'solid', space_after => 0.1, space_before => 0.5);

      for x in lista_errores_3 loop
         v_aux := v_aux + 1;
         p (x.asignatura_id || ' ' || x.grupo_id || ' ' || x.tipo_subgrupo_id || x.subgrupo_id || ' (' || x.id || ')');
      end loop;

      if v_aux = 0 then
         fop.block ('No hi ha cap error.', space_before => 0.5);
      elsif v_aux = 1 then
         fop.block ('Trovat ' || v_aux || ' error.', space_before => 0.5, space_after => 0.5);
      else
         fop.block ('Trovats ' || v_aux || ' errors.', space_before => 0.5, space_after => 0.5);
      end if;
   exception
      when others then
         p (sqlerrm);
   end;
BEGIN
   select count (*)
   into   vSesion
   from   apa_sesiones
   where  sesion_id = pSesion
   and    fecha < sysdate () - 0.33;

   if vSesion > 0 then
      select nombre
      into   v_nombre
      from   hor_estudios
      where  id = pEstudio;

      cabecera;

      for x in lista_semestres loop
         fop.block ('Semestre ' || x.id, font_weight => 'bold', text_align => 'center', font_size => 14,
                    space_after => 0.5);
         control_1 (x.id);
         control_2 (x.id);
      end loop;

      control_3;
      pie;
   else
      cabecera;
      fop.block ('No tens perm�s per accedir a aquest document.', font_weight => 'bold', text_align => 'center',
                 font_size => 14, space_after => 0.5);
      pie;
   end if;
EXCEPTION
   WHEN OTHERS THEN
      htp.p (sqlerrm);
END;



CREATE OR REPLACE PROCEDURE UJI_HORARIOS.HORARIOS_CIRCUITO_PDF (
   name_array    IN   euji_util.ident_arr2,
   value_array   IN   euji_util.ident_arr2
) IS
   -- Parametros
   pEstudio    CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pEstudio');
   pCircuito   CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pCircuito');
   pSesion     CONSTANT VARCHAR2 (4000) := euji_util.euji_getparam (name_array, value_array, 'pSesion');
   pSemestre   CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pSemestre');
   pGrupo      CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pGrupo');
   pSemana     CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pSemana');
   pCurso               number          := 1;
   vCursoAcaNum         number;
   vSesion              number;
   vFechaIniSemestre    date;
   vFechaFinSemestre    date;
   vEsGenerica          boolean         := pSemana = 'G';

     -- Variables
   --  vPerId              per_personas.id%TYPE := euji_get_perid ();
   CURSOR c_items_gen IS
      SELECT asignatura_id, asignatura, estudio, asi.caracter, semestre_id, comun, grupo_id, tipo_subgrupo,
             tipo_subgrupo_id, subgrupo_id, dia_semana_id, hora_inicio, hora_fin, tipo_asignatura_id, tipo_asignatura,
             to_date (dia_semana_id + 1 || '/01/2006 ' || to_char (hora_fin, 'hh24:mi'), 'dd/mm/yyyy hh24:mi') fin,
             to_date (dia_semana_id + 1 || '/01/2006 ' || to_char (hora_inicio, 'hh24:mi'),
                      'dd/mm/yyyy hh24:mi') inicio
      FROM   UJI_HORARIOS.HOR_ITEMS items,
             uji_horarios.hor_items_asignaturas asi,
             uji_horarios.hor_items_circuitos cir
      WHERE  items.id = asi.item_id
      and    cir.item_id = items.id
      and    cir.circuito_id = pCircuito
      and    asi.estudio_id = pEstudio
      AND    items.curso_id = pCurso
      AND    items.grupo_id = pGrupo
      AND    items.semestre_id = pSemestre
      AND    items.dia_semana_id IS NOT NULL
      GROUP BY   asignatura_id,
           asignatura,
           estudio,
           asi.caracter,
           semestre_id,
           comun,
           grupo_id,
           tipo_subgrupo,
           tipo_subgrupo_id,
           subgrupo_id,
           dia_semana_id,
           hora_inicio,
           hora_fin,
           tipo_asignatura_id,
           tipo_asignatura;

   CURSOR c_items_det (vIniDate date, vFinDate date) IS
      SELECT   asignatura_id, asignatura, estudio, grupo_id, tipo_subgrupo, tipo_subgrupo_id, subgrupo_id,
               dia_semana_id, hora_inicio, hora_fin, inicio, fin
      FROM     UJI_HORARIOS.HOR_ITEMS items,
               UJI_HORARIOS.HOR_ITEMS_DETALLE det,
               uji_horarios.hor_items_asignaturas asi,
               uji_horarios.hor_items_circuitos cir
      WHERE    items.id = asi.item_id
      and      cir.item_id = items.id
      and      cir.circuito_id = pCircuito
      and      asi.estudio_id = pEstudio
      AND      items.curso_id = pCurso
      AND      items.grupo_id = pGrupo
      AND      items.semestre_id = pSemestre
      AND      items.dia_semana_id IS NOT NULL
      and      det.item_id = items.id
      and      det.inicio > vIniDate
      and      det.fin < vFinDate
      GROUP BY asignatura_id,
               asignatura,
               estudio,
               grupo_id,
               tipo_subgrupo,
               tipo_subgrupo_id,
               subgrupo_id,
               dia_semana_id,
               hora_inicio,
               hora_fin,
               inicio,
               fin
      order by det.inicio;

   CURSOR c_asigs is
      SELECT distinct (asignatura_id), asignatura
      FROM            UJI_HORARIOS.HOR_ITEMS items,
                      uji_horarios.hor_items_asignaturas asi,
                      uji_horarios.hor_items_circuitos itemcir
      WHERE           items.id = asi.item_id
      and             items.id = itemcir.item_id
      and             itemcir.circuito_id = pCircuito
      and             asi.estudio_id = pEstudio
      AND             items.curso_id = pCurso
      AND             items.grupo_id = pGrupo
      AND             items.semestre_id = pSemestre
      AND             items.dia_semana_id IS NOT NULL
      order by        asignatura_id;

   CURSOR c_clases_asig (vAsignaturaId varchar2) is
      SELECT   tipo_subgrupo_id || subgrupo_id subgrupo,
               decode (dia_semana_id, 1, 'L', 2, 'M', 3, 'X', 4, 'J', 5, 'V', '?') dia_sem,
               to_char (hora_fin, 'hh24:mi') hora_fin_str, to_char (hora_inicio, 'hh24:mi') hora_inicio_str
      FROM     UJI_HORARIOS.HOR_ITEMS items,
               uji_horarios.hor_items_asignaturas asi,
               uji_horarios.hor_items_circuitos itemcir
      WHERE    items.id = asi.item_id
      and      items.id = itemcir.item_id
      and      itemcir.circuito_id = pCircuito
      and      asi.estudio_id = pEstudio
      AND      items.curso_id = pCurso
      AND      items.grupo_id = pGrupo
      AND      items.semestre_id = pSemestre
      and      asi.asignatura_id = vAsignaturaId
      AND      items.dia_semana_id IS NOT NULL
      order by dia_semana_id,
               hora_inicio,
               subgrupo;

   vFechas              t_horario;
   vRdo                 clob;

   procedure cabecera is
   begin
      owa_util.mime_header (ccontent_type => 'text/xml', bclose_header => true);
      fop.documentOpen (page_width => 29.7, page_height => 21, margin_left => 0.5, margin_right => 0.5,
                        margin_top => 0.5, margin_bottom => 0, extent_before => 0, extent_after => 0.5,
                        extent_margin_top => 0, extent_margin_bottom => 0.5);
      fop.pageBodyOpen;
      fop.documentfooteropen;
      fop.block ('Fecha documento: ' || to_char (sysdate, 'yyyy/mm/dd HH24:MI'), font_size => 6,
                 font_style => 'italic');
      fop.documentfooterclose;
      fop.pageFlowOpen;
   end;

   procedure calcula_curso_aca is
   begin
      SELECT   curso_academico_id
      into     vCursoAcaNum
      FROM     hor_semestres_detalle d,
               hor_estudios e
      WHERE    e.id = pEstudio
      AND      e.tipo_id = d.tipo_estudio_id
      AND      ROWNUM = 1
      ORDER BY fecha_inicio;
   end;

   procedure info_calendario_gen is
      vCursoAca           varchar2 (4000);
      vTitulacionNombre   varchar2 (4000);
      vCircuitoNombre     varchar2 (4000);
      vCursoNombre        varchar2 (4000) := pCurso || '�';
      vTextoCabecera      varchar2 (4000);
   begin
      select nombre
      into   vTitulacionNombre
      from   hor_estudios
      where  id = pEstudio;

      select nombre
      into   vCircuitoNombre
      from   hor_circuitos
      where  id = pCircuito;

      vCursoAca := to_char (vCursoAcaNum) || '/' || to_char (vCursoAcaNum + 1);

      if pCurso in ('1', '3') then
         vCursoNombre := pCurso || 'er';
      elsif pCurso = '2' then
         vCursoNombre := '2on';
      elsif pCurso = '4' then
         vCursoNombre := '4t';
      else
         vCursoNombre := pCurso || '�';
      end if;

      vTextoCabecera :=
         vTitulacionNombre || ' - Circuit ' || vCircuitoNombre || ' - ' || vCursoNombre || ' curs - Grup ' || pGrupo
         || ' (semestre ' || pSemestre || ') - ' || vCursoAca;
      fop.block (text => vTextoCabecera, border_bottom => 'solid', border_width => 0.05);
   end;

   procedure muestra_leyenda_asig (item c_asigs%ROWTYPE) is
      vClasesText   varchar2 (4000);
   begin
      fop.block (item.asignatura_id || ', ' || item.asignatura || ':', font_size => 7, padding_top => 0.1);
      vClasesText := '<fo:inline color="white">___</fo:inline>';

      for c in c_clases_asig (item.asignatura_id) loop
         vClasesText :=
            vClasesText || c.dia_sem || ', ' || c.hora_inicio_str || '-' || c.hora_fin_str || ' (' || c.subgrupo
            || '); ';
      end loop;

      --vClasesText := substr (vClasesText, 1, length (vClasesText) - 2);
      fop.block (vClasesText, font_size => 7, padding_top => 0.06);
   end;

   procedure leyenda_asigs is
   begin
      fop.block (' ', padding_top => 1);

      for item in c_asigs loop
         muestra_leyenda_asig (item);
      end loop;
   end;

   procedure pie is
   begin
      fop.pageFlowClose;
      fop.pageBodyClose;
      fop.documentClose;
   end;

   procedure calendario_gen is
   begin
      vFechas := t_horario ();

      for item in c_items_gen loop
         vFechas.extend;
         vFechas (vFechas.count) :=
            ItemHorarios (hor_contenido_item (item.hora_inicio, item.hora_fin, item.asignatura_id,
                                              '(' || item.tipo_subgrupo_id || item.subgrupo_id || ')'),
                          redondea_fecha_a_cuartos(item.inicio), redondea_fecha_a_cuartos(item.fin), null);
      end loop;

      if vFechas.count > 0 then
         vRdo := xpfpkg_horarios_academicos.drawTimetable (vFechas, 5, null, 'CA', 'PDF_APP_HORARIOS');
         xup_utils.dump (vRdo);
         dbms_lob.freeTemporary (vRdo);
      end if;
   end;

   procedure calendario_det (vFechaIni date, vFechaFin date) is
   begin
      vFechas := t_horario ();

      for item in c_items_det (vFechaIni, vFechaFin) loop
         vFechas.extend;
         vFechas (vFechas.count) :=
            ItemHorarios (hor_contenido_item (item.hora_inicio, item.hora_fin, item.asignatura_id,
                                              '(' || item.tipo_subgrupo_id || item.subgrupo_id || ')'),
                          redondea_fecha_a_cuartos(item.inicio), redondea_fecha_a_cuartos(item.fin), null);
      end loop;

      if vFechas.count > 0 then
         vRdo := xpfpkg_horarios_academicos.drawTimetable (vFechas, 5, null, 'CA', 'PDF_APP_HORARIOS_DET');
         xup_utils.dump (vRdo);
         dbms_lob.freeTemporary (vRdo);
      end if;
   end;

   procedure calcula_fechas_semestre is
   begin
      SELECT fecha_inicio, fecha_examenes_fin
      into   vFechaIniSemestre, vFechaFinSemestre
      FROM   hor_semestres_detalle,
             hor_estudios e
      WHERE  semestre_id = pSemestre
      and    e.id = pEstudio
      and    e.tipo_id = tipo_estudio_id;
   end;

   function semana_tiene_clase (vIniDate date, vFinDate date)
      return boolean is
      num_clases   number;
   begin
      SELECT   count (*)
      into     num_clases
      FROM     UJI_HORARIOS.HOR_ITEMS items,
               UJI_HORARIOS.HOR_ITEMS_DETALLE det,
               uji_horarios.hor_items_asignaturas asi
      WHERE    items.id = asi.item_id
      and      asi.estudio_id = pEstudio
      AND      items.curso_id = pCurso
      AND      items.grupo_id = pGrupo
      AND      items.semestre_id = pSemestre
      AND      items.dia_semana_id IS NOT NULL
      and      det.item_id = items.id
      and      det.inicio > vIniDate
      and      det.fin < vFinDate
      order by det.inicio;

      return num_clases > 0;
   end;

   procedure info_calendario_det (vFechaIni date, vFechaFin date) is
      vCursoAca           varchar2 (4000);
      vTitulacionNombre   varchar2 (4000);
      vCursoNombre        varchar2 (4000) := pCurso || '�';
      vCircuitoNombre     varchar2 (4000);
      vTextoCabecera      varchar2 (4000);
      vFechaIniStr        varchar2 (4000) := to_char (vFechaIni, 'dd/mm/yyyy');
      vFechaFinStr        varchar2 (4000) := to_char (vFechaFin - 1, 'dd/mm/yyyy');
   begin
      select nombre
      into   vTitulacionNombre
      from   hor_estudios
      where  id = pEstudio;

      select nombre
      into   vCircuitoNombre
      from   hor_circuitos
      where  id = pCircuito;

      vCursoAca := to_char (vCursoAcaNum) || '/' || to_char (vCursoAcaNum + 1);

      if pCurso in ('1', '3') then
         vCursoNombre := pCurso || 'er';
      elsif pCurso = '2' then
         vCursoNombre := '2on';
      elsif pCurso = '4' then
         vCursoNombre := '4t';
      else
         vCursoNombre := pCurso || '�';
      end if;

      vTextoCabecera :=
         vTitulacionNombre || ' - Circuit ' || vCircuitoNombre || ' - ' || vCursoNombre || ' curs - Grup ' || pGrupo
         || ' (semestre ' || pSemestre || ') - Del ' || vFechaIniStr || ' al ' || vFechaFinStr;
      fop.block (text => vTextoCabecera, border_bottom => 'solid', border_width => 0.05);
   end;

   procedure paginas_calendario_det is
      vFechaIniSemana   date;
      vFechaFinSemana   date;
   begin
      calcula_fechas_semestre;
      vFechaIniSemana := vFechaIniSemestre;
      vFechaFinSemana := vFechaIniSemana + 7;

      while vFechaFinSemana <= vFechaFinSemestre loop
         if semana_tiene_clase (vFechaIniSemana, vFechaFinSemana) then
            info_calendario_det (vFechaIniSemana, vFechaFinSemana);
            calendario_det (vFechaIniSemana, vFechaFinSemana);
            htp.p ('<fo:block break-before="page" color="white">_</fo:block>');
         end if;

         vFechaIniSemana := vFechaFinSemana;
         vFechaFinSemana := vFechaFinSemana + 7;
      end loop;
   end;
BEGIN
   select count (*)
   into   vSesion
   from   apa_sesiones
   where  sesion_id = pSesion
   and    fecha < sysdate () - 0.33;

   if vSesion > 0 then
      calcula_curso_aca;
      cabecera;

      if vEsGenerica then
         info_calendario_gen;
         calendario_gen;
         leyenda_asigs;
      else
         paginas_calendario_det;
         leyenda_asigs;
      end if;

      pie;
   else
      cabecera;
      fop.block ('No tens perm�s per accedir a aquest document.', font_weight => 'bold', text_align => 'center',
                 font_size => 14, space_after => 0.5);
      pie;
   end if;
EXCEPTION
   WHEN OTHERS THEN
      htp.p (sqlerrm);
END;



CREATE OR REPLACE PROCEDURE UJI_HORARIOS.HORARIOS_SEMANA_PDF (

   name_array    IN   euji_util.ident_arr2,
   value_array   IN   euji_util.ident_arr2
) IS
   -- Parametros
   pEstudio    CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pEstudio');
   pSemestre   CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pSemestre');
   pSesion     CONSTANT VARCHAR2 (4000) := euji_util.euji_getparam (name_array, value_array, 'pSesion');
   pGrupo      CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pGrupo');
   pCurso      CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pCurso');
   pSemana     CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pSemana');
   vCursoAcaNum         number;
   vSesion              number;
   vFechaIniSemestre    date;
   vFechaFinSemestre    date;
   vEsGenerica          boolean         := pSemana = 'G';

     -- Variables
   --  vPerId              per_personas.id%TYPE := euji_get_perid ();
   CURSOR c_items_gen (pSemestre in number) IS
      SELECT aulas.codigo as codigo_aula, asignatura_id, asignatura, estudio, asi.caracter, semestre_id, comun, grupo_id,
             tipo_subgrupo, tipo_subgrupo_id, subgrupo_id, dia_semana_id, hora_inicio, hora_fin, tipo_asignatura_id,
             tipo_asignatura,
             to_date (dia_semana_id + 1 || '/01/2006 ' || to_char (hora_fin, 'hh24:mi'), 'dd/mm/yyyy hh24:mi') fin,
             to_date (dia_semana_id + 1 || '/01/2006 ' || to_char (hora_inicio, 'hh24:mi'),
                      'dd/mm/yyyy hh24:mi') inicio
      FROM   UJI_HORARIOS.HOR_ITEMS items LEFT OUTER JOIN hor_aulas aulas ON (aulas.id = items.aula_planificacion_id)
             INNER JOIN uji_horarios.hor_items_asignaturas asi ON (items.id = asi.item_id)
      WHERE  asi.estudio_id = pEstudio
      AND    items.curso_id = pCurso
      AND    ',' || pGrupo || ',' like '%,' || items.grupo_id || ',%'
      AND    items.semestre_id = pSemestre
      AND    items.dia_semana_id IS NOT NULL;

   CURSOR c_items_det (vIniDate date, vFinDate date) IS
      SELECT   aulas.codigo as codigo_aula, asignatura_id, asignatura, estudio, grupo_id, tipo_subgrupo,
               tipo_subgrupo_id, subgrupo_id, dia_semana_id, hora_inicio, hora_fin, inicio, fin
      FROM     UJI_HORARIOS.HOR_ITEMS items LEFT OUTER JOIN hor_aulas aulas ON (aulas.id = items.aula_planificacion_id)
               INNER JOIN uji_horarios.hor_items_asignaturas asi ON (items.id = asi.item_id)
               INNER JOIN UJI_HORARIOS.HOR_ITEMS_DETALLE det ON (det.item_id = items.id)
      WHERE    asi.estudio_id = pEstudio
      AND      items.curso_id = pCurso
      AND      items.grupo_id = pGrupo
      AND      items.semestre_id = pSemestre
      AND      items.dia_semana_id IS NOT NULL
      and      det.inicio > vIniDate
      and      det.fin < vFinDate
      order by det.inicio;

   CURSOR c_asigs (pSemestre in number) is
      SELECT distinct (asignatura_id), asignatura
      FROM            UJI_HORARIOS.HOR_ITEMS items,
                      uji_horarios.hor_items_asignaturas asi
      WHERE           items.id = asi.item_id
      and             asi.estudio_id = pEstudio
      AND             items.curso_id = pCurso
      AND             items.grupo_id = pGrupo
      AND             items.semestre_id = pSemestre
      AND             items.dia_semana_id IS NOT NULL
      order by        asignatura_id;

   CURSOR c_clases_asig (vAsignaturaId varchar2) is
      SELECT   tipo_subgrupo_id || subgrupo_id subgrupo,
               decode (dia_semana_id, 1, 'L', 2, 'M', 3, 'X', 4, 'J', 5, 'V', '?') dia_sem,
               to_char (hora_fin, 'hh24:mi') hora_fin_str, to_char (hora_inicio, 'hh24:mi') hora_inicio_str
      FROM     UJI_HORARIOS.HOR_ITEMS items,
               uji_horarios.hor_items_asignaturas asi
      WHERE    items.id = asi.item_id
      and      asi.estudio_id = pEstudio
      AND      items.curso_id = pCurso
      AND      items.grupo_id = pGrupo
      AND      items.semestre_id = pSemestre
      and      asi.asignatura_id = vAsignaturaId
      AND      items.dia_semana_id IS NOT NULL
      order by dia_semana_id,
               hora_inicio,
               subgrupo;

   vFechas              t_horario;
   vRdo                 clob;

   procedure cabecera is
   begin
      owa_util.mime_header (ccontent_type => 'text/xml', bclose_header => true);
      fop.documentOpen (page_width => 29.7, page_height => 21, margin_left => 0.5, margin_right => 0.5,
                        margin_top => 0.5, margin_bottom => 0, extent_before => 0, extent_after => 0.5,
                        extent_margin_top => 0, extent_margin_bottom => 0.5);
      fop.pageBodyOpen;
      fop.documentfooteropen;
      fop.block ('Fecha documento: ' || to_char (sysdate, 'yyyy/mm/dd HH24:MI'), font_size => 6,
                 font_style => 'italic');
      fop.documentfooterclose;
      fop.pageFlowOpen;
   end;

   procedure calcula_curso_aca is
   begin
      SELECT   curso_academico_id
      into     vCursoAcaNum
      FROM     hor_semestres_detalle d,
               hor_estudios e
      WHERE    e.id = pEstudio
      AND      e.tipo_id = d.tipo_estudio_id
      AND      ROWNUM = 1
      ORDER BY fecha_inicio;
   end;

   procedure info_calendario_gen (pSemestre in number) is
      vCursoAca           varchar2 (4000);
      vTitulacionNombre   varchar2 (4000);
      vCursoNombre        varchar2 (4000) := pCurso || '�';
      vTextoCabecera      varchar2 (4000);
      vTextoGrupo         varchar2 (4000);
   begin
      select nombre
      into   vTitulacionNombre
      from   hor_estudios
      where  id = pEstudio;

      vCursoAca := to_char (vCursoAcaNum) || '/' || to_char (vCursoAcaNum + 1);

      if pCurso in ('1', '3') then
         vCursoNombre := pCurso || 'er';
      elsif pCurso = '2' then
         vCursoNombre := '2on';
      elsif pCurso = '4' then
         vCursoNombre := '4t';
      else
         vCursoNombre := pCurso || '�';
      end if;

      if pGrupo like '%,%' then
         vTextoGrupo := 'Grups';
      else
         vTextoGrupo := 'Grup';
      end if;

      vTextoCabecera :=
         vTitulacionNombre || ' - ' || vCursoNombre || ' curs - ' || vTextoGrupo || ' ' || pGrupo || ' (semestre '
         || pSemestre || ') - ' || vCursoAca;
      fop.block (text => vTextoCabecera, border_bottom => 'solid', border_width => 0.05);
   end;

   procedure muestra_leyenda_asig (item c_asigs%ROWTYPE) is
      vClasesText   varchar2 (4000);
   begin
      fop.block (item.asignatura_id || ', ' || item.asignatura || ':', font_size => 7, padding_top => 0.1);
      vClasesText := '<fo:inline color="white">___</fo:inline>';

      for c in c_clases_asig (item.asignatura_id) loop
         vClasesText :=
            vClasesText || c.dia_sem || ', ' || c.hora_inicio_str || '-' || c.hora_fin_str || ' (' || c.subgrupo
            || '); ';
      end loop;

      --vClasesText := substr (vClasesText, 1, length (vClasesText) - 2);
      fop.block (vClasesText, font_size => 7, padding_top => 0.06);
   end;

   procedure leyenda_asigs (pSemestre in number) is
   begin
      fop.block (' ', padding_top => 1);

      for item in c_asigs (pSemestre) loop
         muestra_leyenda_asig (item);
      end loop;
   end;

   procedure pie is
   begin
      fop.pageFlowClose;
      fop.pageBodyClose;
      fop.documentClose;
   end;

   function redondea_fecha_a_cuartos (vIniDate date)
      return date is
      fecha_redondeada   date;
   begin
      select trunc(vIniDate,'HH')+(15*round(to_char( trunc(vIniDate,'MI'),'MI')/15))/1440
        into fecha_redondeada
        from dual;
      
      return fecha_redondeada;
   end;
   
   procedure calendario_gen (pSemestre in number) is
   begin
      vFechas := t_horario ();

      for item in c_items_gen (pSemestre) loop
         vFechas.extend;
         vFechas (vFechas.count) :=
            ItemHorarios (hor_contenido_item (redondea_fecha_a_cuartos(item.hora_inicio), redondea_fecha_a_cuartos(item.hora_fin), item.asignatura_id,
                                              '(' || item.tipo_subgrupo_id || item.subgrupo_id || ') '
                                              || item.codigo_aula),
                          redondea_fecha_a_cuartos(item.inicio), redondea_fecha_a_cuartos(item.fin), null);
      end loop;

      if vFechas.count > 0 then
         vRdo := xpfpkg_horarios_academicos.drawTimetable (vFechas, 5, null, 'CA', 'PDF_APP_HORARIOS');
         xup_utils.dump (vRdo);
         dbms_lob.freeTemporary (vRdo);
      end if;
   end;

   procedure calendario_det (vFechaIni date, vFechaFin date) is
   begin
      vFechas := t_horario ();

      for item in c_items_det (vFechaIni, vFechaFin) loop
         vFechas.extend;
         vFechas (vFechas.count) :=
            ItemHorarios (hor_contenido_item (redondea_fecha_a_cuartos(item.hora_inicio), redondea_fecha_a_cuartos(item.hora_fin), item.asignatura_id,
                                              '(' || item.tipo_subgrupo_id || item.subgrupo_id || ') '
                                              || item.codigo_aula),
                          redondea_fecha_a_cuartos(item.inicio), redondea_fecha_a_cuartos(item.fin), null);
      end loop;

      if vFechas.count > 0 then
         vRdo := xpfpkg_horarios_academicos.drawTimetable (vFechas, 5, null, 'CA', 'PDF_APP_HORARIOS_DET');
         xup_utils.dump (vRdo);
         dbms_lob.freeTemporary (vRdo);
      end if;
   end;

   procedure calcula_fechas_semestre (pSemestre in number) is
   begin
      SELECT fecha_inicio, fecha_examenes_fin
      into   vFechaIniSemestre, vFechaFinSemestre
      FROM   hor_semestres_detalle,
             hor_estudios e
      WHERE  semestre_id = pSemestre
      and    e.id = pEstudio
      and    e.tipo_id = tipo_estudio_id;
      
      SELECT Next_Day(Trunc(vFechaIniSemestre) - 7,'LUN') into vFechaIniSemestre FROM dual;
      
   end;

   function semana_tiene_clase (vIniDate date, vFinDate date)
      return boolean is
      num_clases   number;
   begin
      SELECT   count (*)
      into     num_clases
      FROM     UJI_HORARIOS.HOR_ITEMS items,
               UJI_HORARIOS.HOR_ITEMS_DETALLE det,
               uji_horarios.hor_items_asignaturas asi
      WHERE    items.id = asi.item_id
      and      asi.estudio_id = pEstudio
      AND      items.curso_id = pCurso
      AND      items.grupo_id = pGrupo
      AND      items.semestre_id = pSemestre
      AND      items.dia_semana_id IS NOT NULL
      and      det.item_id = items.id
      and      det.inicio > vIniDate
      and      det.fin < vFinDate
      order by det.inicio;

      return num_clases > 0;
   end;

   procedure info_calendario_det (vFechaIni date, vFechaFin date) is
      vCursoAca           varchar2 (4000);
      vTitulacionNombre   varchar2 (4000);
      vCursoNombre        varchar2 (4000) := pCurso || '�';
      vTextoCabecera      varchar2 (4000);
      vFechaIniStr        varchar2 (4000) := to_char (vFechaIni, 'dd/mm/yyyy');
      vFechaFinStr        varchar2 (4000) := to_char (vFechaFin - 1, 'dd/mm/yyyy');
   begin
      select nombre
      into   vTitulacionNombre
      from   hor_estudios
      where  id = pEstudio;

      vCursoAca := to_char (vCursoAcaNum) || '/' || to_char (vCursoAcaNum + 1);

      if pCurso in ('1', '3') then
         vCursoNombre := pCurso || 'er';
      elsif pCurso = '2' then
         vCursoNombre := '2on';
      elsif pCurso = '4' then
         vCursoNombre := '4t';
      else
         vCursoNombre := pCurso || '�';
      end if;

      vTextoCabecera :=
         vTitulacionNombre || ' - ' || vCursoNombre || ' curs - Grup ' || pGrupo || ' (semestre ' || pSemestre
         || ') - Del ' || vFechaIniStr || ' al ' || vFechaFinStr;
      fop.block (text => vTextoCabecera, border_bottom => 'solid', border_width => 0.05);
   end;

   procedure paginas_calendario_det (pSemestre in number) is
      vFechaIniSemana   date;
      vFechaFinSemana   date;
   begin
      calcula_fechas_semestre (pSemestre);
      vFechaIniSemana := vFechaIniSemestre;
      vFechaFinSemana := vFechaIniSemana + 7;

      while vFechaFinSemana <= vFechaFinSemestre loop
         if semana_tiene_clase (vFechaIniSemana, vFechaFinSemana) then
            info_calendario_det (vFechaIniSemana, vFechaFinSemana);
            calendario_det (vFechaIniSemana, vFechaFinSemana);
            htp.p ('<fo:block break-before="page" color="white">_</fo:block>');
         end if;

         vFechaIniSemana := vFechaFinSemana;
         vFechaFinSemana := vFechaFinSemana + 7;
      end loop;
   end;
BEGIN
   --euji_control_acceso (vItem);
   select count (*)
   into   vSesion
   from   apa_sesiones
   where  sesion_id = pSesion
   and    fecha < sysdate () - 0.33;

   if vSesion > 0 then
      calcula_curso_aca;
      cabecera;

      if vEsGenerica then
         --info_calendario_gen (pSemestre);
         --calendario_gen (pSemestre);
         --leyenda_asigs (pSemestre);
         info_calendario_gen (1);
         calendario_gen (1);
         leyenda_asigs (1);
         htp.p ('<fo:block break-before="page" color="white">_</fo:block>');
         info_calendario_gen (2);
         calendario_gen (2);
         leyenda_asigs (2);
      else
         paginas_calendario_det (pSemestre);
         leyenda_asigs (pSemestre);
      end if;

      pie;
   else
      cabecera;
      fop.block ('No tens perm�s per accedir a aquest document.', font_weight => 'bold', text_align => 'center',
                 font_size => 14, space_after => 0.5);
      pie;
   end if;
EXCEPTION
   WHEN OTHERS THEN
      htp.p (sqlerrm);
END;



CREATE OR REPLACE PROCEDURE UJI_HORARIOS.HORARIOS_SEMANA4_PDF (
   name_array    IN   euji_util.ident_arr2,
   value_array   IN   euji_util.ident_arr2
) IS
   -- Parametros
   pEstudio    CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pEstudio');
   pSemestre   CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pSemestre');
   pSesion     CONSTANT VARCHAR2 (4000) := euji_util.euji_getparam (name_array, value_array, 'pSesion');
   pGrupo      CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pGrupo');
   pCurso      CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pCurso');
   pSemana     CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pSemana');
   vCursoAcaNum         number;
   vSesion              number;
   vFechaIniSemestre    date;
   vFechaFinSemestre    date;
   vEsGenerica          boolean         := pSemana = 'G';

     -- Variables
   --  vPerId              per_personas.id%TYPE := euji_get_perid ();
   CURSOR c_items_gen IS
      SELECT aulas.codigo as codigo_aula, asignatura_id, asignatura, estudio, asi.caracter, semestre_id, comun, grupo_id, tipo_subgrupo,
             tipo_subgrupo_id, subgrupo_id, dia_semana_id, hora_inicio, hora_fin, tipo_asignatura_id, tipo_asignatura,
             to_date (dia_semana_id + 1 || '/01/2006 ' || to_char (hora_fin, 'hh24:mi'), 'dd/mm/yyyy hh24:mi') fin,
             to_date (dia_semana_id + 1 || '/01/2006 ' || to_char (hora_inicio, 'hh24:mi'),
                      'dd/mm/yyyy hh24:mi') inicio
      FROM   UJI_HORARIOS.HOR_ITEMS items
      LEFT OUTER JOIN hor_aulas aulas  ON (aulas.id = items.aula_planificacion_id)
      INNER JOIN   uji_horarios.hor_items_asignaturas asi ON (items.id = asi.item_id)      
      WHERE  asi.estudio_id = pEstudio
      AND    items.curso_id = pCurso
      AND    pGrupo = items.grupo_id
      AND    items.semestre_id = pSemestre
      AND    items.dia_semana_id IS NOT NULL
      AND ROWNUM < 10;

   CURSOR c_items_det (vIniDate date, vFinDate date) IS
      SELECT   aulas.codigo as codigo_aula, asignatura_id, asignatura, estudio, grupo_id, tipo_subgrupo, tipo_subgrupo_id, subgrupo_id,
               dia_semana_id, hora_inicio, hora_fin, inicio, fin
      FROM     UJI_HORARIOS.HOR_ITEMS items
      LEFT OUTER JOIN hor_aulas aulas  ON (aulas.id = items.aula_planificacion_id)
      INNER JOIN uji_horarios.hor_items_asignaturas asi ON (items.id = asi.item_id)
      INNER JOIN UJI_HORARIOS.HOR_ITEMS_DETALLE det ON (det.item_id = items.id) 
      WHERE    asi.estudio_id = pEstudio
      AND      items.curso_id = pCurso
      AND      items.grupo_id = pGrupo
      AND      items.semestre_id = pSemestre
      AND      items.dia_semana_id IS NOT NULL
      and      det.inicio > vIniDate
      and      det.fin < vFinDate
      order by det.inicio;

   CURSOR c_asigs is
      SELECT distinct (asignatura_id), asignatura
      FROM            UJI_HORARIOS.HOR_ITEMS items,
                      uji_horarios.hor_items_asignaturas asi
      WHERE           items.id = asi.item_id
      and             asi.estudio_id = pEstudio
      AND             items.curso_id = pCurso
      AND             items.grupo_id = pGrupo
      AND             items.semestre_id = pSemestre
      AND             items.dia_semana_id IS NOT NULL
      order by        asignatura_id;

   CURSOR c_clases_asig (vAsignaturaId varchar2) is
      SELECT   tipo_subgrupo_id || subgrupo_id subgrupo,
               decode (dia_semana_id, 1, 'L', 2, 'M', 3, 'X', 4, 'J', 5, 'V', '?') dia_sem,
               to_char (hora_fin, 'hh24:mi') hora_fin_str, to_char (hora_inicio, 'hh24:mi') hora_inicio_str
      FROM     UJI_HORARIOS.HOR_ITEMS items,
               uji_horarios.hor_items_asignaturas asi
      WHERE    items.id = asi.item_id
      and      asi.estudio_id = pEstudio
      AND      items.curso_id = pCurso
      AND      items.grupo_id = pGrupo
      AND      items.semestre_id = pSemestre
      and      asi.asignatura_id = vAsignaturaId
      AND      items.dia_semana_id IS NOT NULL
      order by dia_semana_id,
               hora_inicio,
               subgrupo;

   vFechas              t_horario;
   vRdo                 clob;

   procedure cabecera is
   begin
      owa_util.mime_header (ccontent_type => 'text/xml', bclose_header => true);
      fop.documentOpen (page_width => 29.7, page_height => 21, margin_left => 0.5, margin_right => 0.5,
                        margin_top => 0.5, margin_bottom => 0, extent_before => 0, extent_after => 0.5,
                        extent_margin_top => 0, extent_margin_bottom => 0.5);
      fop.pageBodyOpen;
      fop.documentfooteropen;
      fop.block ('Fecha documento: ' || to_char (sysdate, 'yyyy/mm/dd HH24:MI'), font_size => 6,
                 font_style => 'italic');
      fop.documentfooterclose;
      fop.pageFlowOpen;
   end;

   procedure calcula_curso_aca is
   begin
      SELECT   curso_academico_id
      into     vCursoAcaNum
      FROM     hor_semestres_detalle d,
               hor_estudios e
      WHERE    e.id = pEstudio
      AND      e.tipo_id = d.tipo_estudio_id
      AND      ROWNUM = 1
      ORDER BY fecha_inicio;
   end;

   procedure info_calendario_gen is
      vCursoAca           varchar2 (4000);
      vTitulacionNombre   varchar2 (4000);
      vCursoNombre        varchar2 (4000) := pCurso || '�';
      vTextoCabecera      varchar2 (4000);
      vTextoGrupo         varchar2 (4000);
   begin
      select nombre
      into   vTitulacionNombre
      from   hor_estudios
      where  id = pEstudio;

      vCursoAca := to_char (vCursoAcaNum) || '/' || to_char (vCursoAcaNum + 1);

      if pCurso in ('1', '3') then
         vCursoNombre := pCurso || 'er';
      elsif pCurso = '2' then
         vCursoNombre := '2on';
      elsif pCurso = '4' then
         vCursoNombre := '4t';
      else
         vCursoNombre := pCurso || '�';
      end if;

      if pGrupo like '%,%' then
         vTextoGrupo := 'Grups';
      else
         vTextoGrupo := 'Grup';
      end if;

      vTextoCabecera :=
         vTitulacionNombre || ' - ' || vCursoNombre || ' curs - ' || vTextoGrupo || ' ' || pGrupo || ' (semestre '
         || pSemestre || ') - ' || vCursoAca;
      fop.block (text => vTextoCabecera, border_bottom => 'solid', border_width => 0.05);
   end;

   procedure muestra_leyenda_asig (item c_asigs%ROWTYPE) is
      vClasesText   varchar2 (4000);
   begin
      fop.block (item.asignatura_id || ', ' || item.asignatura || ':', font_size => 7, padding_top => 0.1);
      vClasesText := '<fo:inline color="white">___</fo:inline>';

      for c in c_clases_asig (item.asignatura_id) loop
         vClasesText :=
            vClasesText || c.dia_sem || ', ' || c.hora_inicio_str || '-' || c.hora_fin_str || ' (' || c.subgrupo
            || '); ';
      end loop;

      --vClasesText := substr (vClasesText, 1, length (vClasesText) - 2);
      fop.block (vClasesText, font_size => 7, padding_top => 0.06);
   end;

   procedure leyenda_asigs is
   begin
      fop.block (' ', padding_top => 1);

      for item in c_asigs loop
         muestra_leyenda_asig (item);
      end loop;
   end;

   procedure pie is
   begin
      fop.pageFlowClose;
      fop.pageBodyClose;
      fop.documentClose;
   end;

   procedure calendario_gen is
   begin
      vFechas := t_horario ();

      for item in c_items_gen loop
         vFechas.extend;
         vFechas (vFechas.count) :=
            ItemHorarios (hor_contenido_item (item.hora_inicio, item.hora_fin, item.asignatura_id,
                                              '(' || item.tipo_subgrupo_id || item.subgrupo_id || ') '||item.codigo_aula),
                          redondea_fecha_a_cuartos(item.inicio), redondea_fecha_a_cuartos(item.fin), null);
      end loop;

      if vFechas.count > 0 then
         vRdo := xpfpkg_horarios_academicos4.drawTimetable (vFechas, 5, null, 'CA', 'PDF_APP_HORARIOS');
         xup_utils.dump (vRdo);
         dbms_lob.freeTemporary (vRdo);
      end if;
   end;

   procedure calendario_det (vFechaIni date, vFechaFin date) is
   begin
      vFechas := t_horario ();

      for item in c_items_det (vFechaIni, vFechaFin) loop
         vFechas.extend;
         vFechas (vFechas.count) :=
            ItemHorarios (hor_contenido_item (item.hora_inicio, item.hora_fin, item.asignatura_id,
                                              '(' || item.tipo_subgrupo_id || item.subgrupo_id || ') '||item.codigo_aula),
                          redondea_fecha_a_cuartos(item.inicio), redondea_fecha_a_cuartos(item.fin), null);
      end loop;

      if vFechas.count > 0 then
         vRdo := xpfpkg_horarios_academicos.drawTimetable (vFechas, 5, null, 'CA', 'PDF_APP_HORARIOS_DET');
         xup_utils.dump (vRdo);
         dbms_lob.freeTemporary (vRdo);
      end if;
   end;

   procedure calcula_fechas_semestre is
   begin
      SELECT fecha_inicio, fecha_examenes_fin
      into   vFechaIniSemestre, vFechaFinSemestre
      FROM   hor_semestres_detalle,
             hor_estudios e
      WHERE  semestre_id = pSemestre
      and    e.id = pEstudio
      and    e.tipo_id = tipo_estudio_id;
   end;

   function semana_tiene_clase (vIniDate date, vFinDate date)
      return boolean is
      num_clases   number;
   begin
      SELECT   count (*)
      into     num_clases
      FROM     UJI_HORARIOS.HOR_ITEMS items,
               UJI_HORARIOS.HOR_ITEMS_DETALLE det,
               uji_horarios.hor_items_asignaturas asi
      WHERE    items.id = asi.item_id
      and      asi.estudio_id = pEstudio
      AND      items.curso_id = pCurso
      AND      items.grupo_id = pGrupo
      AND      items.semestre_id = pSemestre
      AND      items.dia_semana_id IS NOT NULL
      and      det.item_id = items.id
      and      det.inicio > vIniDate
      and      det.fin < vFinDate
      order by det.inicio;

      return num_clases > 0;
   end;

   procedure info_calendario_det (vFechaIni date, vFechaFin date) is
      vCursoAca           varchar2 (4000);
      vTitulacionNombre   varchar2 (4000);
      vCursoNombre        varchar2 (4000) := pCurso || '�';
      vTextoCabecera      varchar2 (4000);
      vFechaIniStr        varchar2 (4000) := to_char (vFechaIni, 'dd/mm/yyyy');
      vFechaFinStr        varchar2 (4000) := to_char (vFechaFin - 1, 'dd/mm/yyyy');
   begin
      select nombre
      into   vTitulacionNombre
      from   hor_estudios
      where  id = pEstudio;

      vCursoAca := to_char (vCursoAcaNum) || '/' || to_char (vCursoAcaNum + 1);

      if pCurso in ('1', '3') then
         vCursoNombre := pCurso || 'er';
      elsif pCurso = '2' then
         vCursoNombre := '2on';
      elsif pCurso = '4' then
         vCursoNombre := '4t';
      else
         vCursoNombre := pCurso || '�';
      end if;

      vTextoCabecera :=
         vTitulacionNombre || ' - ' || vCursoNombre || ' curs - Grup ' || pGrupo || ' (semestre ' || pSemestre
         || ') - Del ' || vFechaIniStr || ' al ' || vFechaFinStr;
      fop.block (text => vTextoCabecera, border_bottom => 'solid', border_width => 0.05);
   end;

   procedure paginas_calendario_det is
      vFechaIniSemana   date;
      vFechaFinSemana   date;
   begin
      calcula_fechas_semestre;
      vFechaIniSemana := vFechaIniSemestre;
      vFechaFinSemana := vFechaIniSemana + 7;

      while vFechaFinSemana <= vFechaFinSemestre loop
         if semana_tiene_clase (vFechaIniSemana, vFechaFinSemana) then
            info_calendario_det (vFechaIniSemana, vFechaFinSemana);
            calendario_det (vFechaIniSemana, vFechaFinSemana);
            htp.p ('<fo:block break-before="page" color="white">_</fo:block>');
         end if;

         vFechaIniSemana := vFechaFinSemana;
         vFechaFinSemana := vFechaFinSemana + 7;
      end loop;
   end;
BEGIN
   --euji_control_acceso (vItem);
   select count (*)
   into   vSesion
   from   apa_sesiones
   where  sesion_id = pSesion
   and    fecha < sysdate () - 0.33;

   if vSesion > 0 then
      calcula_curso_aca;
      cabecera;

      if vEsGenerica then
         info_calendario_gen;
         calendario_gen;
         leyenda_asigs;
      else
         paginas_calendario_det;
         --leyenda_asigs;
      end if;

      pie;
   else
      cabecera;
      fop.block ('No tens perm�s per accedir a aquest document.', font_weight => 'bold', text_align => 'center',
                 font_size => 14, space_after => 0.5);
      pie;
   end if;
EXCEPTION
   WHEN OTHERS THEN
      htp.p (sqlerrm);
END;



CREATE OR REPLACE PROCEDURE UJI_HORARIOS.HORARIOS_SEMANA5_PDF (
   name_array    IN   euji_util.ident_arr2,
   value_array   IN   euji_util.ident_arr2
) IS
   -- Parametros
   pEstudio    CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pEstudio');
   pSemestre   CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pSemestre');
   pSesion     CONSTANT VARCHAR2 (4000) := euji_util.euji_getparam (name_array, value_array, 'pSesion');
   pGrupo      CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pGrupo');
   pCurso      CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pCurso');
   pSemana     CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pSemana');
   vCursoAcaNum         NUMBER;
   vSesion              NUMBER;
   vFechaIniSemestre    DATE;
   vFechaFinSemestre    DATE;
   vEsGenerica          BOOLEAN         := pSemana = 'G';

   -- Variables
   --  vPerId              per_personas.id%TYPE := euji_get_perid ();
   CURSOR c_items_gen IS
      SELECT aulas.codigo AS codigo_aula, asignatura_id, asignatura, estudio, asi.caracter, semestre_id, comun, grupo_id,
             tipo_subgrupo, tipo_subgrupo_id, subgrupo_id, dia_semana_id, hora_inicio, hora_fin, tipo_asignatura_id,
             tipo_asignatura,
             TO_DATE (dia_semana_id + 1 || '/01/2006 ' || TO_CHAR (hora_fin, 'hh24:mi'), 'dd/mm/yyyy hh24:mi') fin,
             TO_DATE (dia_semana_id + 1 || '/01/2006 ' || TO_CHAR (hora_inicio, 'hh24:mi'),
                      'dd/mm/yyyy hh24:mi') inicio
        FROM UJI_HORARIOS.HOR_ITEMS items LEFT OUTER JOIN hor_aulas aulas ON (aulas.id = items.aula_planificacion_id)
             INNER JOIN uji_horarios.hor_items_asignaturas asi ON (items.id = asi.item_id)
       WHERE asi.estudio_id = pEstudio
         AND items.curso_id = pCurso
         AND pGrupo LIKE '%' || items.grupo_id || '%'
         AND items.semestre_id = pSemestre
         AND items.dia_semana_id IS NOT NULL;

   CURSOR c_items_det (vIniDate DATE, vFinDate DATE) IS
      SELECT   aulas.codigo AS codigo_aula, asignatura_id, asignatura, estudio, grupo_id, tipo_subgrupo,
               tipo_subgrupo_id, subgrupo_id, dia_semana_id, hora_inicio, hora_fin, inicio, fin
          FROM UJI_HORARIOS.HOR_ITEMS items LEFT OUTER JOIN hor_aulas aulas ON (aulas.id = items.aula_planificacion_id)
               INNER JOIN uji_horarios.hor_items_asignaturas asi ON (items.id = asi.item_id)
               INNER JOIN UJI_HORARIOS.HOR_ITEMS_DETALLE det ON (det.item_id = items.id)
         WHERE asi.estudio_id = pEstudio
           AND items.curso_id = pCurso
           AND items.grupo_id = pGrupo
           AND items.semestre_id = pSemestre
           AND items.dia_semana_id IS NOT NULL
           AND det.inicio > vIniDate
           AND det.fin < vFinDate
      ORDER BY det.inicio;

   CURSOR c_asigs IS
      SELECT DISTINCT (asignatura_id), asignatura
                 FROM UJI_HORARIOS.HOR_ITEMS items,
                      uji_horarios.hor_items_asignaturas asi
                WHERE items.id = asi.item_id
                  AND asi.estudio_id = pEstudio
                  AND items.curso_id = pCurso
                  AND items.grupo_id = pGrupo
                  AND items.semestre_id = pSemestre
                  AND items.dia_semana_id IS NOT NULL
             ORDER BY asignatura_id;

   CURSOR c_clases_asig (vAsignaturaId VARCHAR2) IS
      SELECT   tipo_subgrupo_id || subgrupo_id subgrupo,
               DECODE (dia_semana_id, 1, 'L', 2, 'M', 3, 'X', 4, 'J', 5, 'V', '?') dia_sem, desde_el_dia, hasta_el_dia,
               repetir_cada_semanas, numero_iteraciones, detalle_manual, items.id, asignatura_id,
               TO_CHAR (hora_fin, 'hh24:mi') hora_fin_str, TO_CHAR (hora_inicio, 'hh24:mi') hora_inicio_str
          FROM UJI_HORARIOS.HOR_ITEMS items,
               uji_horarios.hor_items_asignaturas asi
         WHERE items.id = asi.item_id
           AND asi.estudio_id = pEstudio
           AND items.curso_id = pCurso
           AND items.grupo_id = pGrupo
           AND items.semestre_id = pSemestre
           AND asi.asignatura_id = vAsignaturaId
           AND items.dia_semana_id IS NOT NULL
      ORDER BY dia_semana_id,
               hora_inicio,
               subgrupo;

   vFechas              t_horario;
   vRdo                 CLOB;

   PROCEDURE cabecera IS
   BEGIN
      OWA_UTIL.mime_header (ccontent_type => 'text/xml', bclose_header => TRUE);
      fop.documentOpen (page_width => 29.7, page_height => 21, margin_left => 0.5, margin_right => 0.5,
                        margin_top => 0.5, margin_bottom => 0, extent_before => 0, extent_after => 0.5,
                        extent_margin_top => 0, extent_margin_bottom => 0.5);
      fop.pageBodyOpen;
      fop.documentfooteropen;
      fop.block ('Fecha documento: ' || TO_CHAR (SYSDATE, 'yyyy/mm/dd HH24:MI'), font_size => 6,
                 font_style => 'italic');
      fop.documentfooterclose;
      fop.pageFlowOpen;
   END;

   PROCEDURE calcula_curso_aca IS
   BEGIN
      SELECT   curso_academico_id
          INTO vCursoAcaNum
          FROM hor_semestres_detalle d,
               hor_estudios e
         WHERE e.id = pEstudio
           AND e.tipo_id = d.tipo_estudio_id
           AND ROWNUM = 1
      ORDER BY fecha_inicio;
   END;

   PROCEDURE info_calendario_gen IS
      vCursoAca           VARCHAR2 (4000);
      vTitulacionNombre   VARCHAR2 (4000);
      vCursoNombre        VARCHAR2 (4000) := pCurso || '�';
      vTextoCabecera      VARCHAR2 (4000);
      vTextoGrupo         VARCHAR2 (4000);
   BEGIN
      SELECT nombre
        INTO vTitulacionNombre
        FROM hor_estudios
       WHERE id = pEstudio;

      vCursoAca := TO_CHAR (vCursoAcaNum) || '/' || TO_CHAR (vCursoAcaNum + 1);

      IF pCurso IN ('1', '3') THEN
         vCursoNombre := pCurso || 'er';
      ELSIF pCurso = '2' THEN
         vCursoNombre := '2on';
      ELSIF pCurso = '4' THEN
         vCursoNombre := '4t';
      ELSE
         vCursoNombre := pCurso || '�';
      END IF;

      IF pGrupo LIKE '%,%' THEN
         vTextoGrupo := 'Grups';
      ELSE
         vTextoGrupo := 'Grup';
      END IF;

      vTextoCabecera :=
         vTitulacionNombre || ' - ' || vCursoNombre || ' curs - ' || vTextoGrupo || ' ' || pGrupo || ' (semestre '
         || pSemestre || ') - ' || vCursoAca;
      fop.block (text => vTextoCabecera, border_bottom => 'solid', border_width => 0.05);
   END;

   FUNCTION fechas_detalle (Vitem_id IN NUMBER)
      return VARCHAR2 is
      vFechas   VARCHAR2 (4000);

      CURSOR items_detalle IS
         SELECT   ITEM_ID, INICIO, FIN
             FROM UJI_HORARIOS.HOR_ITEMS_DETALLE d
            WHERE d.item_id = Vitem_id
         ORDER BY inicio;
   BEGIN
      vFechas := '';

      for c IN items_detalle LOOP
         vFechas := vFechas || c.inicio || ', ';
      END LOOP;

      return substr (vFechas, 1, length (vFechas) - 2);
   END;

   PROCEDURE muestra_leyenda_asig (item c_asigs%ROWTYPE) IS
      vClasesText      VARCHAR2 (4000);
      vDetalleFechas   VARCHAR2 (4000);
   BEGIN
      fop.block (item.asignatura_id || ', ' || item.asignatura || ':', font_size => 7, padding_top => 0.1);

      FOR c IN c_clases_asig (item.asignatura_id) LOOP
         vClasesText := '<fo:inline color="white">___</fo:inline>';
         vClasesText :=
            vClasesText || c.dia_sem || ', ' || c.hora_inicio_str || '-' || c.hora_fin_str || ' (' || c.subgrupo
            || '). Dies: ';

         IF    c.detalle_manual = 1
            OR c.desde_el_dia is not null
            OR c.hasta_el_dia is not null
            OR nvl (c.repetir_cada_semanas, 0) <> 1
            OR c.numero_iteraciones is not null THEN
            vDetalleFechas := fechas_detalle (c.id);
         ELSE
            vDetalleFechas := 'Tot el semestre';
         END IF;

         fop.block (vClasesText || vDetalleFechas, font_size => 7, padding_top => 0.06);
      END LOOP;
--vClasesText := substr (vClasesText, 1, length (vClasesText) - 2);
--fop.block (vClasesText, font_size => 7, padding_top => 0.06);
   END;

   PROCEDURE leyenda_asigs IS
   BEGIN
      fop.block (' ', padding_top => 1);

      FOR item IN c_asigs LOOP
         muestra_leyenda_asig (item);
      END LOOP;
   END;

   PROCEDURE pie IS
   BEGIN
      fop.pageFlowClose;
      fop.pageBodyClose;
      fop.documentClose;
   END;

   PROCEDURE calendario_gen IS
   BEGIN
      vFechas := t_horario ();

      FOR item IN c_items_gen LOOP
         vFechas.EXTEND;
         vFechas (vFechas.COUNT) :=
            ItemHorarios (hor_contenido_item (item.hora_inicio, item.hora_fin, item.asignatura_id,
                                              '(' || item.tipo_subgrupo_id || item.subgrupo_id || ') '
                                              || item.codigo_aula),
                          redondea_fecha_a_cuartos (item.inicio), redondea_fecha_a_cuartos (item.fin), NULL);
      END LOOP;

      IF vFechas.COUNT > 0 THEN
         vRdo := xpfpkg_horarios_academicos.drawTimetable (vFechas, 5, NULL, 'CA', 'PDF_APP_HORARIOS');
         xup_utils.DUMP (vRdo);
         DBMS_LOB.freeTemporary (vRdo);
      END IF;
   END;

   PROCEDURE calendario_det (vFechaIni DATE, vFechaFin DATE) IS
   BEGIN
      vFechas := t_horario ();

      FOR item IN c_items_det (vFechaIni, vFechaFin) LOOP
         vFechas.EXTEND;
         vFechas (vFechas.COUNT) :=
            ItemHorarios (hor_contenido_item (item.hora_inicio, item.hora_fin, item.asignatura_id,
                                              '(' || item.tipo_subgrupo_id || item.subgrupo_id || ') '
                                              || item.codigo_aula),
                          redondea_fecha_a_cuartos (item.inicio), redondea_fecha_a_cuartos (item.fin), NULL);
      END LOOP;

      IF vFechas.COUNT > 0 THEN
         vRdo := xpfpkg_horarios_academicos.drawTimetable (vFechas, 5, NULL, 'CA', 'PDF_APP_HORARIOS_DET');
         xup_utils.DUMP (vRdo);
         DBMS_LOB.freeTemporary (vRdo);
      END IF;
   END;

   PROCEDURE calcula_fechas_semestre IS
   BEGIN
      SELECT fecha_inicio, fecha_examenes_fin
        INTO vFechaIniSemestre, vFechaFinSemestre
        FROM hor_semestres_detalle,
             hor_estudios e
       WHERE semestre_id = pSemestre
         AND e.id = pEstudio
         AND e.tipo_id = tipo_estudio_id;
   END;

   FUNCTION semana_tiene_clase (vIniDate DATE, vFinDate DATE)
      RETURN BOOLEAN IS
      num_clases   NUMBER;
   BEGIN
      SELECT   COUNT (*)
          INTO num_clases
          FROM UJI_HORARIOS.HOR_ITEMS items,
               UJI_HORARIOS.HOR_ITEMS_DETALLE det,
               uji_horarios.hor_items_asignaturas asi
         WHERE items.id = asi.item_id
           AND asi.estudio_id = pEstudio
           AND items.curso_id = pCurso
           AND items.grupo_id = pGrupo
           AND items.semestre_id = pSemestre
           AND items.dia_semana_id IS NOT NULL
           AND det.item_id = items.id
           AND det.inicio > vIniDate
           AND det.fin < vFinDate
      ORDER BY det.inicio;

      RETURN num_clases > 0;
   END;

   PROCEDURE info_calendario_det (vFechaIni DATE, vFechaFin DATE) IS
      vCursoAca           VARCHAR2 (4000);
      vTitulacionNombre   VARCHAR2 (4000);
      vCursoNombre        VARCHAR2 (4000) := pCurso || '�';
      vTextoCabecera      VARCHAR2 (4000);
      vFechaIniStr        VARCHAR2 (4000) := TO_CHAR (vFechaIni, 'dd/mm/yyyy');
      vFechaFinStr        VARCHAR2 (4000) := TO_CHAR (vFechaFin - 1, 'dd/mm/yyyy');
   BEGIN
      SELECT nombre
        INTO vTitulacionNombre
        FROM hor_estudios
       WHERE id = pEstudio;

      vCursoAca := TO_CHAR (vCursoAcaNum) || '/' || TO_CHAR (vCursoAcaNum + 1);

      IF pCurso IN ('1', '3') THEN
         vCursoNombre := pCurso || 'er';
      ELSIF pCurso = '2' THEN
         vCursoNombre := '2on';
      ELSIF pCurso = '4' THEN
         vCursoNombre := '4t';
      ELSE
         vCursoNombre := pCurso || '�';
      END IF;

      vTextoCabecera :=
         vTitulacionNombre || ' - ' || vCursoNombre || ' curs - Grup ' || pGrupo || ' (semestre ' || pSemestre
         || ') - Del ' || vFechaIniStr || ' al ' || vFechaFinStr;
      fop.block (text => vTextoCabecera, border_bottom => 'solid', border_width => 0.05);
   END;

   PROCEDURE paginas_calendario_det IS
      vFechaIniSemana   DATE;
      vFechaFinSemana   DATE;
   BEGIN
      calcula_fechas_semestre;
      vFechaIniSemana := vFechaIniSemestre;
      vFechaFinSemana := vFechaIniSemana + 7;

      WHILE vFechaFinSemana <= vFechaFinSemestre LOOP
         IF semana_tiene_clase (vFechaIniSemana, vFechaFinSemana) THEN
            info_calendario_det (vFechaIniSemana, vFechaFinSemana);
            calendario_det (vFechaIniSemana, vFechaFinSemana);
            HTP.p ('<fo:block break-before="page" color="white">_</fo:block>');
         END IF;

         vFechaIniSemana := vFechaFinSemana;
         vFechaFinSemana := vFechaFinSemana + 7;
      END LOOP;
   END;
BEGIN
   --euji_control_acceso (vItem);
   SELECT COUNT (*)
     INTO vSesion
     FROM apa_sesiones
    WHERE sesion_id = pSesion
      AND fecha < SYSDATE () - 0.33;

   IF vSesion > 0 THEN
      calcula_curso_aca;
      cabecera;

      IF vEsGenerica THEN
         info_calendario_gen;
         calendario_gen;
         leyenda_asigs;
      ELSE
         paginas_calendario_det;
         leyenda_asigs;
      END IF;

      pie;
   ELSE
      cabecera;
      fop.block ('No tens perm�s per accedir a aquest document.', font_weight => 'bold', text_align => 'center',
                 font_size => 14, space_after => 0.5);
      pie;
   END IF;
EXCEPTION
   WHEN OTHERS THEN
      HTP.p (SQLERRM);
END;



CREATE OR REPLACE PROCEDURE UJI_HORARIOS.HORARIOS_VALIDACION_PDF (
   name_array    IN   euji_util.ident_arr2,
   value_array   IN   euji_util.ident_arr2
) IS
   -- Parametros
   pEstudio    CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pEstudio');
   pSemestre   CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pSemestre');
   pGrupo      CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pGrupo');
   pCurso      CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pCurso');
   pSesion     CONSTANT VARCHAR2 (400)  := euji_util.euji_getparam (name_array, value_array, 'pSesion');
   v_nombre             varchar2 (2000);
   vSesion              number;

   procedure cabecera is
   begin
      owa_util.mime_header (ccontent_type => 'text/xml', bclose_header => true);
      fop.documentOpen (margin_left => 1.5, margin_right => 1.5, margin_top => 0.5, margin_bottom => 0.5,
                        extent_before => 3, extent_after => 1.5, extent_margin_top => 3, extent_margin_bottom => 1.5);
      fop.pageBodyOpen;
      htp.p (fof.documentheaderopen || fof.blockopen (space_after => 0.1) || fof.tableOpen
             || fof.tablecolumndefinition (column_width => 3) || fof.tablecolumndefinition (column_width => 15)
             || fof.tablebodyopen || fof.tableRowOpen || fof.tablecellopen
             || fof.block (fof.logo_uji, text_align => 'center') || fof.tablecellclose
             || fof.tablecellopen (display_align => 'center', padding => 0.4)
             || fof.block (text => 'Controls horaris', font_size => 12, font_weight => 'bold', font_family => 'Arial',
                           text_align => 'center')
             || fof.block (text => v_nombre, font_size => 12, font_weight => 'bold', font_family => 'Arial',
                           text_align => 'center')
             || fof.block (text => 'Curs: ' || pCurso || ' - Semestre: ' || pSemestre || ' - Grup: ' || pGrupo,
                           font_size => 12, font_weight => 'bold', font_family => 'Arial', text_align => 'center')
             || fof.tablecellclose || fof.tableRowClose || fof.tablebodyclose || fof.tableClose || fof.blockclose
             || fof.documentheaderclose);
      -- El numero paginas se escribe a mano para poner el texto m?s peque?o
      htp.p (fof.documentfooteropen || fof.tableOpen || fof.tablecolumndefinition (column_width => 18)
             || fof.tablebodyopen || fof.tableRowOpen || fof.tablecellopen
             || fof.block (text => 'Pag. <fo:page-number /> de <fo:page-number-citation ref-id="lastBlock" />',
                           text_align => 'right', font_weight => 'bold', font_size => 8)
             || fof.tablecellclose || fof.tableRowClose || fof.tablebodyclose || fof.tableClose
             || fof.documentfooterclose);
      fop.pageFlowOpen;
      fop.block (fof.white_space);
   end;

   procedure pie is
   begin
      htp.p ('<fo:block  id="lastBlock" space-before="1cm" font-size="8pt">');
      htp.p ('Data de generaci�: ' || to_char (sysdate, 'dd/mm/yyy hh24:mi'));
      fop.blockclose ();
      fop.pageFlowClose;
      fop.pageBodyClose;
      fop.documentClose;
   end;

   function espacios (p_espacios in number)
      return varchar2 is
      v_rdo   varchar2 (2000);
   begin
      v_rdo := null;

      for x in 1 .. p_espacios loop
         v_rdo := v_rdo || fof.white_space;
      end loop;

      return v_rdo;
   end;

   procedure p (p_texto in varchar2) is
   begin
      fop.block (espacios (3) || p_texto);
   end;

   procedure control_1 is
      v_aux   number := 0;

      cursor lista_errores_1 is
         select distinct asignatura_id, grupo_id, tipo_subgrupo_id || subgrupo_id subgrupo,
                         decode (tipo_subgrupo_id, 'TE', 1, 'PR', 2, 'LA', 3, 'SE', 4, 0) tipo
         from            uji_horarios.hor_items i,
                         hor_items_asignaturas asi
         where           i.id = asi.item_id
         and             asi.curso_id = pCurso
         and             semestre_id = pSemestre
         --and             grupo_id = pGrupo
         and             pGrupo like '%' || grupo_id || '%'
         and             tipo_subgrupo_id not in ('AV', 'TU')
         and             dia_semana_id is null
         and             estudio_id = pEstudio
         order by        1,
                         2,
                         decode (tipo_subgrupo_id, 'TE', 1, 'PR', 2, 'LA', 3, 'SE', 4, 0);
   begin
      fop.block ('Control 1 - Subgrups sense planificar', font_size => 12, font_weight => 'bold',
                 border_bottom => 'solid');

      for x in lista_errores_1 loop
         v_aux := v_aux + 1;
         p (x.asignatura_id || ' ' || x.grupo_id || ' ' || x.subgrupo);
      end loop;

      if v_aux = 0 then
         fop.block ('No hi ha cap error.', space_before => 0.5);
      elsif v_aux = 1 then
         fop.block ('Trovat ' || v_aux || ' error.', space_before => 0.5);
      else
         fop.block ('Trovats ' || v_aux || ' errors.', space_before => 0.5);
      end if;
   exception
      when others then
         p (sqlerrm);
   end;

   procedure control_2 is
      v_aux          number        := 0;
      v_asignatura   varchar2 (20);

      cursor lista_errores_2_v1 is
         select   asignatura_id, grupo_id, tipo_subgrupo_id || subgrupo_id subgrupo,
                  decode (dia_semana_id,
                          1, 'Dilluns',
                          2, 'Dimarts',
                          3, 'Dimecres',
                          4, 'Dijous',
                          5, 'Divendres',
                          6, 'Dissabte',
                          7, 'Diumenge',
                          'Error'
                         ) dia_semana,
                  hora_inicio, hora_fin,
                  (select asi2.asignatura_id || ' ' || i2.tipo_subgrupo_id || i2.subgrupo_id grupo
                   from   uji_horarios.hor_items i2,
                          uji_horarios.hor_items_asignaturas asi2
                   where  i.id <> i2.id
                   and    i2.id = asi2.item_id
                   and    i.curso_id = i2.curso_id
                   and    i.semestre_id = i2.semestre_id
                   and    i.grupo_id = i2.grupo_id
                   and    i.tipo_subgrupo_id <> i2.tipo_subgrupo_id
                   and    i.dia_semana_id = i2.dia_semana_id
                   and    asi.estudio_id = asi2.estudio_id
                   and    asi.asignatura_id = asi2.asignatura_id
                   and    (   to_number (to_char (i.hora_inicio, 'hh24mi')) between to_number (to_char (i2.hora_inicio,
                                                                                                        'hh24mi'))
                                                                                and to_number (to_char (i2.hora_fin,
                                                                                                        'hh24mi'))
                           or to_number (to_char (i.hora_fin, 'hh24mi')) between to_number (to_char (i2.hora_inicio,
                                                                                                     'hh24mi'))
                                                                             and to_number (to_char (i2.hora_fin,
                                                                                                     'hh24mi'))
                          )
                   and    rownum <= 1) solape
         from     uji_horarios.hor_items i,
                  uji_horarios.hor_items_asignaturas asi
         where    i.id = asi.item_id
         and      asi.curso_id = pCurso
         and      semestre_id = pSemestre
         --and      grupo_id = pGrupo
         and      pGrupo like '%' || grupo_id || '%'
         and      tipo_subgrupo_id in ('TE')
         and      estudio_id = pEstudio
         and      exists (
                     select 1
                     from   uji_horarios.hor_items i2,
                            uji_horarios.hor_items_asignaturas asi2
                     where  i.id <> i2.id
                     and    i2.id = asi2.item_id
                     and    i.curso_id = i2.curso_id
                     and    i.semestre_id = i2.semestre_id
                     and    i.grupo_id = i2.grupo_id
                     and    i.tipo_subgrupo_id <> i2.tipo_subgrupo_id
                     and    i.dia_semana_id = i2.dia_semana_id
                     and    asi.estudio_id = asi2.estudio_id
                     and    asi.asignatura_id = asi2.asignatura_id
                     and    (   to_number (to_char (i.hora_inicio, 'hh24mi')) between to_number
                                                                                               (to_char (i2.hora_inicio,
                                                                                                         'hh24mi'))
                                                                                  and to_number (to_char (i2.hora_fin,
                                                                                                          'hh24mi'))
                             or to_number (to_char (i.hora_fin, 'hh24mi')) between to_number (to_char (i2.hora_inicio,
                                                                                                       'hh24mi'))
                                                                               and to_number (to_char (i2.hora_fin,
                                                                                                       'hh24mi'))
                            ))
         order by 1,
                  2,
                  3;

      cursor lista_errores_2 is
         select distinct asignatura_id, grupo_id, tipo_subgrupo_id || subgrupo_id subgrupo,
                         decode (dia_semana_id,
                                 1, 'Dilluns',
                                 2, 'Dimarts',
                                 3, 'Dimecres',
                                 4, 'Dijous',
                                 5, 'Divendres',
                                 6, 'Dissabte',
                                 7, 'Diumenge',
                                 'Error'
                                ) dia_semana,
                         to_char (inicio, 'dd/mm/yyyy') fecha, to_char (inicio, 'hh24:mi') hora_inicio,
                         to_char (fin, 'hh24:mi') hora_fin,
                         (select asi2.asignatura_id || ' ' || i2.tipo_subgrupo_id || i2.subgrupo_id || ' - '
                                 || to_char (inicio, 'hh24:mi') || '-' || to_char (fin, 'hh24:mi') grupo
                          from   uji_horarios.hor_items i2,
                                 uji_horarios.hor_items_asignaturas asi2,
                                 uji_horarios.hor_items_detalle det2
                          where  i.id <> i2.id
                          and    i2.id = det2.item_id
                          and    i2.id = asi2.item_id
                          and    i.curso_id = i2.curso_id
                          and    i.semestre_id = i2.semestre_id
                          and    i.grupo_id = i2.grupo_id
                          and    i.tipo_subgrupo_id <> i2.tipo_subgrupo_id
                          and    i.dia_semana_id = i2.dia_semana_id
                          and    asi.estudio_id = asi2.estudio_id
                          and    asi.asignatura_id = asi2.asignatura_id
                          and    trunc (det.inicio) = trunc (det2.inicio)
                          and    (   to_number (to_char (det.inicio + 1 / 1440, 'hh24mi'))
                                        between to_number (to_char (det2.inicio, 'hh24mi'))
                                            and to_number (to_char (det2.fin, 'hh24mi'))
                                  or to_number (to_char (det.fin - 1 / 1440, 'hh24mi'))
                                        between to_number (to_char (det2.inicio, 'hh24mi'))
                                            and to_number (to_char (det2.fin, 'hh24mi'))
                                 )
                          and    rownum <= 1) solape
         from            uji_horarios.hor_items i,
                         uji_horarios.hor_items_asignaturas asi,
                         uji_horarios.hor_items_detalle det
         where           i.id = asi.item_id
         and             i.id = det.item_id
         and             asi.curso_id = pCurso
         and             semestre_id = pSemestre
         --and             grupo_id = pGrupo
         and             pGrupo like '%' || grupo_id || '%'
         and             tipo_subgrupo_id in ('TE')
         and             estudio_id = pEstudio
         and             exists (
                            select 1
                            from   uji_horarios.hor_items i2,
                                   uji_horarios.hor_items_asignaturas asi2,
                                   uji_horarios.hor_items_detalle det2
                            where  i.id <> i2.id
                            and    i2.id = det2.item_id
                            and    i2.id = asi2.item_id
                            and    i.curso_id = i2.curso_id
                            and    i.semestre_id = i2.semestre_id
                            and    i.grupo_id = i2.grupo_id
                            and    i.tipo_subgrupo_id <> i2.tipo_subgrupo_id
                            and    i.dia_semana_id = i2.dia_semana_id
                            and    asi.estudio_id = asi2.estudio_id
                            and    asi.asignatura_id = asi2.asignatura_id
                            and    trunc (det.inicio) = trunc (det2.inicio)
                            and    (   to_number (to_char (det.inicio + 1 / 1440, 'hh24mi'))
                                          between to_number (to_char (det2.inicio, 'hh24mi'))
                                              and to_number (to_char (det2.fin, 'hh24mi'))
                                    or to_number (to_char (det.fin - 1 / 1440, 'hh24mi'))
                                          between to_number (to_char (det2.inicio, 'hh24mi'))
                                              and to_number (to_char ((det2.fin), 'hh24mi'))
                                   ))
         order by        1,
                         2,
                         3,
                         5;
   begin
      fop.block ('Control 2 - Solpament subgrups TE amb la resta', font_size => 12, font_weight => 'bold',
                 border_bottom => 'solid', space_before => 0.5);
      v_asignatura := 'PRIMERA';

      for x in lista_errores_2 loop
         v_aux := v_aux + 1;

         if     v_aux <> 1
            and v_asignatura <> x.asignatura_id then
            p (' ');
         end if;

         p (x.asignatura_id || ' ' || x.grupo_id || ' ' || x.subgrupo || ' ' || x.dia_semana || ' - ' || x.fecha || ' '
            || x.hora_inicio || '  ->  solapa amb: ' || x.solape);
         v_asignatura := x.asignatura_id;
      end loop;

      if v_aux = 0 then
         fop.block ('No hi ha cap error.', space_before => 0.5);
      elsif v_aux = 1 then
         fop.block ('Trovat ' || v_aux || ' error.', space_before => 0.5);
      else
         fop.block ('Trovats ' || v_aux || ' errors.', space_before => 0.5);
      end if;
   exception
      when others then
         p (sqlerrm);
   end;

   procedure control_3 is
      v_aux          number        := 0;
      v_asignatura   varchar2 (20);
      v_control      number;

      cursor lista_asignaturas is
         select distinct asi.asignatura_id
         from            uji_horarios.hor_items i,
                         uji_horarios.hor_items_asignaturas asi,
                         uji_horarios.hor_items_detalle det
         where           i.id = asi.item_id
         and             i.id = det.item_id
         and             asi.curso_id = pCurso
         and             semestre_id = pSemestre
         --and             grupo_id = pGrupo
         and             pGrupo like '%' || grupo_id || '%'
         and             estudio_id = pEstudio
         order by        1;

      cursor lista_errores_3 (p_asignatura in varchar2) is
         select   *
         from     (select   asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo, creditos, horas_totales,
                            sum (horas_detalle) horas_detalle, decode (tipo, 'S', 'Semestral', 'A', 'Anual') tipo
                   from     (select asi.asignatura_id, grupo_id, tipo_subgrupo_id,
                                    tipo_subgrupo_id || subgrupo_id subgrupo,
                                    decode (tipo_subgrupo_id,
                                            'TE', crd_te,
                                            'PR', crd_pr,
                                            'LA', crd_la,
                                            'SE', crd_se,
                                            'TU', crd_tu,
                                            crd_ev
                                           ) creditos,
                                    decode (tipo_subgrupo_id,
                                            'TE', crd_te,
                                            'PR', crd_pr,
                                            'LA', crd_la,
                                            'SE', crd_se,
                                            'TU', crd_tu,
                                            crd_ev
                                           )
                                    / decode (ad.tipo, 'A', 2, 1) * 10 horas_totales,
                                    (fin - inicio) * 24 horas_detalle, ad.tipo tipo
                             from   uji_horarios.hor_items i,
                                    uji_horarios.hor_items_asignaturas asi,
                                    uji_horarios.hor_items_detalle det,
                                    uji_horarios.hor_ext_asignaturas_detalle ad
                             where  i.id = asi.item_id
                             and    i.id = det.item_id
                             and    asi.curso_id = pCurso
                             and    semestre_id = pSemestre
                             --and    grupo_id = pGrupo
                             and    pGrupo like '%' || grupo_id || '%'
                             and    estudio_id = pEstudio
                             and    asi.asignatura_id = ad.asignatura_id
                             and    asi.asignatura_id = p_asignatura
                             and    ad.tipo = 'S'
                             and    dia_semana_id is not null)
                   group by asignatura_id,
                            grupo_id,
                            tipo_subgrupo_id,
                            subgrupo,
                            creditos,
                            tipo
                   having   horas_totales not between sum (horas_detalle) * 0.95 and sum (horas_detalle) * 1.05
                   union all
                   select   asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo, max (creditos) creditos,
                            max (horas_totales) horas_totales, sum (horas_detalle) horas_detalle,
                            decode (tipo, 'S', 'Semestral', 'A', 'Anual') tipo
                   from     (select   asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo, creditos, horas_totales,
                                      sum (horas_detalle) horas_detalle, tipo, null semestre_id
                             from     (select asi.asignatura_id, grupo_id, tipo_subgrupo_id,
                                              tipo_subgrupo_id || subgrupo_id subgrupo,
                                              decode (tipo_subgrupo_id,
                                                      'TE', crd_te,
                                                      'PR', crd_pr,
                                                      'LA', crd_la,
                                                      'SE', crd_se,
                                                      'TU', crd_tu,
                                                      crd_ev
                                                     ) creditos,
                                              decode (tipo_subgrupo_id,
                                                      'TE', crd_te,
                                                      'PR', crd_pr,
                                                      'LA', crd_la,
                                                      'SE', crd_se,
                                                      'TU', crd_tu,
                                                      crd_ev
                                                     )
                                              * 10 horas_totales,
                                              (fin - inicio) * 24 horas_detalle, ad.tipo tipo, semestre_id
                                       from   uji_horarios.hor_items i,
                                              uji_horarios.hor_items_asignaturas asi,
                                              uji_horarios.hor_items_detalle det,
                                              uji_horarios.hor_ext_asignaturas_detalle ad
                                       where  i.id = asi.item_id
                                       and    i.id = det.item_id
                                       and    asi.curso_id = pCurso
                                       --and    semestre_id = :pSemestre
                                       --and    grupo_id = pGrupo
                                       and    pGrupo like '%' || grupo_id || '%'
                                       and    estudio_id = pEstudio
                                       and    asi.asignatura_id = ad.asignatura_id
                                       and    asi.asignatura_id = p_asignatura
                                       and    ad.tipo = 'A'
                                       and    dia_semana_id is not null)
                             group by asignatura_id,
                                      grupo_id,
                                      tipo_subgrupo_id,
                                      subgrupo,
                                      creditos,
                                      tipo                                                                           --,
                             --semestre_id
                             having   horas_totales not between sum (horas_detalle) * 0.95 and sum (horas_detalle)
                                                                                               * 1.05)
                   group by asignatura_id,
                            grupo_id,
                            tipo_subgrupo_id,
                            subgrupo,
                            tipo)
         order by 1,
                  decode (tipo_subgrupo_id, 'TE', 1, 'PR', 2, 'LA', 3, 'SE', 4, 'TU', 5, 6),
                  3;
   begin
      v_control := 0;
      fop.block ('Control 3 - Control d''hores planificades per subgrup', font_size => 12, font_weight => 'bold',
                 border_bottom => 'solid', space_before => 0.5);
      v_asignatura := 'PRIMERA';
      v_control := 1;

      for asi in lista_asignaturas loop
         v_control := 2;

         for x in lista_errores_3 (asi.asignatura_id) loop
            v_control := 3;
            v_aux := v_aux + 1;

            if     v_aux <> 1
               and v_asignatura <> x.asignatura_id then
               p (' ');
            end if;

            if x.horas_totales > x.horas_detalle then
               p (x.asignatura_id || ' ' || x.grupo_id || ' ' || x.subgrupo || ' -' || x.tipo || '- '
                  || 'Planificades ' || to_char (x.horas_totales - x.horas_detalle)
                  || ' hores de menys, cal planificar-ne ' || x.horas_totales);
            else
               p (x.asignatura_id || ' ' || x.grupo_id || ' ' || x.subgrupo || ' -' || x.tipo || '- '
                  || 'Planificades ' || to_char (x.horas_detalle - x.horas_totales)
                  || ' hores de m�s, cal planificar-ne ' || x.horas_totales);
            end if;

            v_asignatura := x.asignatura_id;
         end loop;
      end loop;

      v_control := 6;

      if v_aux = 0 then
         fop.block ('No hi ha cap error.', space_before => 0.5);
      elsif v_aux = 1 then
         fop.block ('Trovat ' || v_aux || ' error.', space_before => 0.5);
      else
         fop.block ('Trovats ' || v_aux || ' errors.', space_before => 0.5);
      end if;
   exception
      when others then
         p (v_control || '-' || sqlerrm);
   end;

   procedure control_4 is
      v_aux          number        := 0;
      v_asignatura   varchar2 (20);

      cursor lista_errores_4 is
         select   asi.asignatura_id, grupo_id, i.tipo_subgrupo_id, subgrupo_id,
                  to_char (hora_inicio, 'hh24:mi') hora_inicio,
                  decode (dia_semana_id,
                          1, 'Dilluns',
                          2, 'Dimarts',
                          3, 'Dimecres',
                          4, 'Dijous',
                          5, 'Divendres',
                          6, 'Dissabte',
                          7, 'Diumenge',
                          'Error'
                         ) dia_semana,
                  dia_semana_id
         from     uji_horarios.hor_items i,
                  uji_horarios.hor_items_asignaturas asi
         where    i.id = asi.item_id
         and      asi.curso_id = pCurso
         and      semestre_id = pSemestre
         --and      grupo_id = pGrupo
         and      pGrupo like '%' || grupo_id || '%'
         and      estudio_id = pEstudio
         and      dia_semana_id is not null
         and      aula_planificacion_id is null
         order by 1,
                  decode (tipo_subgrupo_id, 'TE', 1, 'PR', 2, 'LA', 3, 'SE', 4, 'TU', 5, 6),
                  4;
   begin
      fop.block ('Control 4 - Control d''assignaci� d''aules per subgrup', font_size => 12, font_weight => 'bold',
                 border_bottom => 'solid', space_before => 0.5);
      v_asignatura := 'PRIMERA';

      for x in lista_errores_4 loop
         v_aux := v_aux + 1;

         if     v_aux <> 1
            and v_asignatura <> x.asignatura_id then
            p (' ');
         end if;

         p (x.asignatura_id || ' ' || x.grupo_id || ' ' || x.tipo_subgrupo_id || x.subgrupo_id || ' ' || x.dia_semana
            || ' ' || x.hora_inicio || ' - no te aula assignada');
         v_asignatura := x.asignatura_id;
      end loop;
   exception
      when others then
         p (sqlerrm);
   end;

   procedure control_5 is
      v_aux          number          := 0;
      v_aula         number;
      v_dia_semana   number;
      v_hora         varchar2 (20);
      v_txt          varchar2 (2000);

      cursor lista_errores_5 is
         select   dia_semana_id, trunc (det.inicio) fecha, to_char (hora_inicio, 'hh24:mi') hora_inicio,
                  aula_planificacion_id,
                  decode (dia_semana_id,
                          1, 'Dilluns',
                          2, 'Dimarts',
                          3, 'Dimecres',
                          4, 'Dijous',
                          5, 'Divendres',
                          6, 'Dissabte',
                          7, 'Diumenge',
                          'Error'
                         ) dia_semana,
                  nvl (a.nombre, 'Aula mal assignada a l''estudi (' || aula_planificacion_id || ')') nombre_aula
         from     uji_horarios.hor_items i,
                  uji_horarios.hor_items_asignaturas asi,
                  uji_horarios.hor_items_detalle det,
                  uji_horarios.hor_aulas a
         where    i.id = asi.item_id
         and      i.id = det.item_id
         and      asi.curso_id = pCurso
         and      semestre_id = pSemestre
         --and      grupo_id = pGrupo
         and      pGrupo like '%' || grupo_id || '%'
         and      estudio_id = pEstudio
         and      dia_semana_id is not null
         and      aula_planificacion_id is not null
         and      aula_planificacion_id = a.id(+)
         group by                                                                                   --asi.asignatura_id,
                  grupo_id,
                  dia_semana_id,
                  trunc (det.inicio),
                  to_char (hora_inicio, 'hh24:mi'),
                  aula_planificacion_id,
                  a.nombre
         having   count (*) > 1
         order by 5,
                  1,
                  3,
                  2;

      cursor lista_subgrupos (p_dia_Semana in number, p_fecha in date, p_hora in varchar2, p_aula in number) is
         select   nvl (comun_texto, asignatura_id) asignatura_id, grupo_id, tipo_subgrupo_id || subgrupo_id subgrupo
         from     uji_horarios.hor_items i,
                  uji_horarios.hor_items_asignaturas asi,
                  uji_horarios.hor_items_detalle det
         where    i.id = asi.item_id
         and      i.id = det.item_id
         and      dia_semana_id = p_dia_semana
         and      trunc (inicio) = p_fecha
         and      to_char (hora_inicio, 'hh24:mi') = p_hora
         and      aula_planificacion_id = p_aula
         and      estudio_id = pEstudio
         order by 1,
                  2;
   begin
      fop.block ('Control 5 - Control de solapament en aules', font_size => 12, font_weight => 'bold',
                 border_bottom => 'solid', space_before => 0.5);
      v_aula := 0;
      v_dia_semana := 0;
      v_hora := '';

      for x in lista_errores_5 loop
         v_aux := v_aux + 1;

         if     v_aux <> 1
            and not (    v_aula = x.aula_planificacion_id
                     and v_dia_semana = x.dia_semana_id
                     and v_hora = x.hora_inicio) then
            p (' ');
         end if;

         v_txt := null;

         if 1 = 2 then
            p (x.nombre_aula || ' - ' || to_char (x.fecha, 'dd/mm/yyyy') || ' ' || x.dia_semana || ' ' || x.hora_inicio);

            for det in lista_subgrupos (x.dia_semana_id, x.fecha, x.hora_inicio, x.aula_planificacion_id) loop
               p (espacios (30) || det.grupo_id || ' ' || det.asignatura_id || '-' || det.subgrupo);
            end loop;
         else
            for det in lista_subgrupos (x.dia_semana_id, x.fecha, x.hora_inicio, x.aula_planificacion_id) loop
               v_txt := v_txt || det.asignatura_id || '-' || det.grupo_id || ' ' || det.subgrupo || ' | ';
            end loop;

            v_txt := trim (v_txt);
            v_txt := substr (v_txt, 1, length (v_txt) - 2);
            p (x.nombre_aula || ' - ' || to_char (x.fecha, 'dd/mm/yyyy') || ' ' || x.dia_semana || ' ' || x.hora_inicio
               || ' ==> ' || v_txt);
         end if;

         v_aula := x.aula_planificacion_id;
         v_dia_semana := x.dia_semana_id;
         v_hora := x.hora_inicio;
      end loop;
   exception
      when others then
         p (sqlerrm);
   end;

   procedure advertencia_1 is
      v_aux             number        := 0;
      v_asignatura      varchar2 (20);
      v_tipo_subgrupo   varchar2 (20);
      v_subgrupo        number;

      cursor lista_advertencia_1 is
         select   asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo_id
         from     uji_horarios.hor_items i,
                  uji_horarios.hor_items_asignaturas asi
         where    i.id = asi.item_id
         and      asi.curso_id = pCurso
         and      semestre_id = pSemestre
         --and      grupo_id = pGrupo
         and      pGrupo like '%' || grupo_id || '%'
         and      estudio_id = pEstudio
         and      dia_semana_id is not null
         and      aula_planificacion_id is not null
         group by asignatura_id,
                  grupo_id,
                  tipo_subgrupo_id,
                  subgrupo_id
         having   count (distinct aula_planificacion_id) > 1;

      cursor lista_aulas (p_asignatura in varchar2, p_tipo_subgrupo in varchar2, p_subgrupo in number) is
         select distinct nvl (a.nombre, 'Aula mal assignada a l''estudi (' || aula_planificacion_id || ')') nombre_aula
         from            uji_horarios.hor_items i,
                         uji_horarios.hor_items_asignaturas asi,
                         uji_horarios.hor_aulas a
         where           i.id = asi.item_id
         and             i.aula_planificacion_id = a.id(+)
         and             asi.curso_id = pCurso
         and             semestre_id = pSemestre
         --and             grupo_id = pGrupo
         and             pGrupo like '%' || grupo_id || '%'
         and             estudio_id = pEstudio
         and             dia_semana_id is not null
         and             aula_planificacion_id is not null
         and             asignatura_id = p_asignatura
         and             tipo_subgrupo_id = p_tipo_subgrupo
         and             subgrupo_id = p_subgrupo
         order by        1;
   begin
      fop.block ('Advertencia 1 - Classes d''un subgrup en aules diferents', font_size => 12, font_weight => 'bold',
                 border_bottom => 'solid', space_before => 0.5);
      v_aux := v_aux + 1;
      v_asignatura := 'PRIMERA';
      v_tipo_subgrupo := 'XX';
      v_subgrupo := 0;

      for x in lista_advertencia_1 loop
         v_aux := v_aux + 1;

         if     v_aux <> 1
            and not (    v_asignatura = x.asignatura_id
                     and v_tipo_subgrupo = x.tipo_subgrupo_id
                     and v_subgrupo = x.subgrupo_id
                    ) then
            p (' ');
         end if;

         for det in lista_aulas (x.asignatura_id, x.tipo_subgrupo_id, x.subgrupo_id) loop
            p (x.asignatura_id || ' - ' || x.grupo_id || ' ' || x.tipo_subgrupo_id || x.subgrupo_id || ' '
               || det.nombre_aula);
         end loop;

         v_asignatura := x.asignatura_id;
         v_tipo_subgrupo := x.tipo_subgrupo_id;
         v_subgrupo := x.subgrupo_id;
      end loop;
   exception
      when others then
         p (sqlerrm);
   end;
BEGIN
   select count (*)
   into   vSesion
   from   apa_sesiones
   where  sesion_id = pSesion
   and    fecha < sysdate () - 0.33;

   if vSesion > 0 then
      select nombre
      into   v_nombre
      from   hor_estudios
      where  id = pEstudio;

      cabecera;
      control_1;
      control_2;
      control_3;
      control_4;
      control_5;
      advertencia_1;
      pie;
   else
      cabecera;
      fop.block ('No tens perm�s per accedir a aquest document.', font_weight => 'bold', text_align => 'center',
                 font_size => 14, space_after => 0.5);
      pie;
   end if;
EXCEPTION
   WHEN OTHERS THEN
      htp.p (sqlerrm);
END;




CREATE OR REPLACE PROCEDURE UJI_HORARIOS.X_HORARIOS_PDF_SEMGEN (
   name_array    IN   euji_util.ident_arr2,
   value_array   IN   euji_util.ident_arr2
) IS
   -- Parametros
   pEstudio    CONSTANT VARCHAR2 (40) := euji_util.euji_getparam (name_array, value_array, 'pEstudio');
   pSemestre   CONSTANT VARCHAR2 (40) := euji_util.euji_getparam (name_array, value_array, 'pSemestre');
   pGrupo      CONSTANT VARCHAR2 (40) := euji_util.euji_getparam (name_array, value_array, 'pGrupo');
   pCurso      CONSTANT VARCHAR2 (40) := euji_util.euji_getparam (name_array, value_array, 'pCurso');
   pSemana     CONSTANT VARCHAR2 (40) := euji_util.euji_getparam (name_array, value_array, 'pSemana');
   vCursoAcaNum         number;
   vFechaIniSemestre    date;
   vFechaFinSemestre    date;
   vEsGenerica          boolean       := pSemana = 'G';

     -- Variables
   --  vPerId              per_personas.id%TYPE := euji_get_perid ();
   CURSOR c_items_gen IS
      SELECT asignatura_id, asignatura, estudio, asi.caracter, semestre_id, comun, grupo_id, tipo_subgrupo,
             tipo_subgrupo_id, subgrupo_id, dia_semana_id, hora_inicio, hora_fin, tipo_asignatura_id, tipo_asignatura,
             to_date (dia_semana_id + 1 || '/01/2006 ' || to_char (hora_fin, 'hh24:mi'), 'dd/mm/yyyy hh24:mi') fin,
             to_date (dia_semana_id + 1 || '/01/2006 ' || to_char (hora_inicio, 'hh24:mi'),
                      'dd/mm/yyyy hh24:mi') inicio
      FROM   UJI_HORARIOS.HOR_ITEMS items,
             uji_horarios.hor_items_asignaturas asi
      WHERE  items.id = asi.item_id
      and    asi.estudio_id = pEstudio
      AND    items.curso_id = pCurso
      AND    items.grupo_id = pGrupo
      AND    items.semestre_id = pSemestre
      AND    items.dia_semana_id IS NOT NULL;

   CURSOR c_items_det (vIniDate date, vFinDate date) IS
      SELECT   asignatura_id, asignatura, estudio, grupo_id, tipo_subgrupo, tipo_subgrupo_id, subgrupo_id,
               dia_semana_id, hora_inicio, hora_fin, inicio, fin
      FROM     UJI_HORARIOS.HOR_ITEMS items,
               UJI_HORARIOS.HOR_ITEMS_DETALLE det,
               uji_horarios.hor_items_asignaturas asi
      WHERE    items.id = asi.item_id
      and      asi.estudio_id = pEstudio
      AND      items.curso_id = pCurso
      AND      items.grupo_id = pGrupo
      AND      items.semestre_id = pSemestre
      AND      items.dia_semana_id IS NOT NULL
      and      det.item_id = items.id
      and      det.inicio > vIniDate
      and      det.fin < vFinDate
      order by det.inicio;

   CURSOR c_asigs is
      SELECT distinct (asignatura_id), asignatura
      FROM            UJI_HORARIOS.HOR_ITEMS items,
                      uji_horarios.hor_items_asignaturas asi
      WHERE           items.id = asi.item_id
      and             asi.estudio_id = pEstudio
      AND             items.curso_id = pCurso
      AND             items.grupo_id = pGrupo
      AND             items.semestre_id = pSemestre
      AND             items.dia_semana_id IS NOT NULL
      order by        asignatura_id;

   CURSOR c_clases_asig (vAsignaturaId varchar2) is
      SELECT   tipo_subgrupo_id || subgrupo_id subgrupo,
               decode (dia_semana_id, 1, 'L', 2, 'M', 3, 'X', 4, 'J', 5, 'V', '?') dia_sem,
               to_char (hora_fin, 'hh24:mi') hora_fin_str, to_char (hora_inicio, 'hh24:mi') hora_inicio_str
      FROM     UJI_HORARIOS.HOR_ITEMS items,
               uji_horarios.hor_items_asignaturas asi
      WHERE    items.id = asi.item_id
      and      asi.estudio_id = pEstudio
      AND      items.curso_id = pCurso
      AND      items.grupo_id = pGrupo
      AND      items.semestre_id = pSemestre
      and      asi.asignatura_id = vAsignaturaId
      AND      items.dia_semana_id IS NOT NULL
      order by dia_semana_id,
               hora_inicio,
               subgrupo;

   vFechas              t_horario;
   vRdo                 clob;

   function contenido_item (hora_ini date, hora_fin date, texto varchar2, subtexto varchar2)
      return varchar2 is
      contenido   varchar2 (4000);
   begin
      --contenido := '<fo:block font-size="6pt" text-align="center">' || to_char (hora_ini, 'hh24:mi') ||' - '||to_char (hora_fin, 'hh24:mi')||' '||texto||' ('||subtexto||')</fo:block>';
      contenido := '<fo:table border="none" width="100%" height="100%" text-align="start">';
      contenido := contenido || '<fo:table-column column-width="25%"/>';
      contenido := contenido || '<fo:table-column column-width="50%"/>';
      contenido := contenido || '<fo:table-column column-width="25%"/>';
      contenido := contenido || fof.tableBodyOpen;
      contenido := contenido || fof.tableRowOpen;
      contenido := contenido || fof.tableCellOpen;
      contenido :=
         contenido
         || fof.block (to_char (hora_ini, 'hh24:mi'), font_size => 6, text_align => 'start', padding_bottom => 0.3);
      contenido := contenido || fof.tableCellClose;
      contenido := contenido || fof.tableRowClose;
      contenido := contenido || fof.tableRowOpen;
      contenido := contenido || fof.tableCellOpen;
      contenido := contenido || fof.tableCellClose;
      contenido := contenido || fof.tableCellOpen;
      contenido := contenido || fof.block (fof.bold (texto), font_size => 6, text_align => 'center');
      contenido := contenido || fof.block (subtexto, font_size => 5, text_align => 'center');
      contenido := contenido || fof.tableCellClose;
      contenido := contenido || fof.tableRowClose;
      contenido := contenido || fof.tableRowOpen;
      contenido := contenido || fof.tableCellOpen;
      contenido :=
         contenido
         || fof.block (to_char (hora_fin, 'hh24:mi'), font_size => 6, text_align => 'start', padding_top => 0.3);
      contenido := contenido || fof.tableCellClose;
      contenido := contenido || fof.tableRowClose;
      contenido := contenido || fof.tableBodyClose;
      contenido := contenido || fof.tableClose;
      return contenido;
   end;

   procedure cabecera is
   begin
      owa_util.mime_header (ccontent_type => 'text/xml', bclose_header => true);
      fop.documentOpen (page_width => 29.7, page_height => 21, margin_left => 0.5, margin_right => 0.5,
                        margin_top => 0.5, margin_bottom => 0, extent_before => 0, extent_after => 0.5,
                        extent_margin_top => 0, extent_margin_bottom => 0.5);
      fop.pageBodyOpen;
      fop.documentfooteropen;
      fop.block ('Fecha documento: ' || to_char (sysdate, 'yyyy/mm/dd HH24:MI'), font_size => 6,
                 font_style => 'italic');
      fop.documentfooterclose;
      fop.pageFlowOpen;
   end;

   procedure calcula_curso_aca is
   begin
      SELECT   curso_academico_id
      into     vCursoAcaNum
      FROM     hor_semestres_detalle d,
               hor_estudios e
      WHERE    e.id = pEstudio
      AND      e.tipo_id = d.tipo_estudio_id
      AND      ROWNUM = 1
      ORDER BY fecha_inicio;
   end;

   procedure info_calendario_gen is
      vCursoAca           varchar2 (4000);
      vTitulacionNombre   varchar2 (4000);
      vCursoNombre        varchar2 (4000) := pCurso || '�';
      vTextoCabecera      varchar2 (4000);
   begin
      select nombre
      into   vTitulacionNombre
      from   hor_estudios
      where  id = pEstudio;

      vCursoAca := to_char (vCursoAcaNum) || '/' || to_char (vCursoAcaNum + 1);

      if pCurso in ('1', '3') then
         vCursoNombre := pCurso || 'er';
      elsif pCurso = '2' then
         vCursoNombre := '2on';
      elsif pCurso = '4' then
         vCursoNombre := '4t';
      else
         vCursoNombre := pCurso || '�';
      end if;

      vTextoCabecera :=
         vTitulacionNombre || ' - ' || vCursoNombre || ' curs - Grup ' || pGrupo || ' (semestre ' || pSemestre || ') - '
         || vCursoAca;
      fop.block (text => vTextoCabecera, border_bottom => 'solid', border_width => 0.05);
   end;

   procedure muestra_leyenda_asig (item c_asigs%ROWTYPE) is
      vClasesText   varchar2 (4000);
   begin
      fop.block (item.asignatura_id || ', ' || item.asignatura || ':', font_size => 7, padding_top => 0.1);
      vClasesText := '<fo:inline color="white">___</fo:inline>';

      for c in c_clases_asig (item.asignatura_id) loop
         vClasesText :=
            vClasesText || c.dia_sem || ', ' || c.hora_inicio_str || '-' || c.hora_fin_str || ' (' || c.subgrupo
            || '); ';
      end loop;

      vClasesText := substr (vClasesText, 1, length (vClasesText) - 2);
      fop.block (vClasesText, font_size => 7, padding_top => 0.06);
   end;

   procedure leyenda_asigs is
   begin
      fop.block (' ', padding_top => 1);

      for item in c_asigs loop
         muestra_leyenda_asig (item);
      end loop;
   end;

   procedure pie is
   begin
      fop.pageFlowClose;
      fop.pageBodyClose;
      fop.documentClose;
   end;

   procedure calendario_gen is
   begin
      vFechas := t_horario ();

      for item in c_items_gen loop
         vFechas.extend;
         vFechas (vFechas.count) :=
            ItemHorarios (contenido_item (item.hora_inicio, item.hora_fin, item.asignatura_id,
                                          '(' || item.tipo_subgrupo_id || item.subgrupo_id || ')'),
                          item.inicio, item.fin, null);
      end loop;

      if vFechas.count > 0 then
         vRdo := xpfpkg_horarios2.drawTimetable (vFechas, 5, null, 'CA', 'PDF_APP_HORARIOS');
         xup_utils.dump (vRdo);
         dbms_lob.freeTemporary (vRdo);
      end if;
   end;

   procedure calendario_det (vFechaIni date, vFechaFin date) is
   begin
      vFechas := t_horario ();

      for item in c_items_det (vFechaIni, vFechaFin) loop
         vFechas.extend;
         vFechas (vFechas.count) :=
            ItemHorarios (contenido_item (item.hora_inicio, item.hora_fin, item.asignatura_id,
                                          '(' || item.tipo_subgrupo_id || item.subgrupo_id || ')'),
                          item.inicio, item.fin, null);
      end loop;

      if vFechas.count > 0 then
         vRdo := xpfpkg_horarios2.drawTimetable (vFechas, 5, null, 'CA', 'PDF_APP_HORARIOS_DET');
         xup_utils.dump (vRdo);
         dbms_lob.freeTemporary (vRdo);
      end if;
   end;

   procedure calcula_fechas_semestre is
   begin
      SELECT fecha_inicio, fecha_examenes_fin
      into   vFechaIniSemestre, vFechaFinSemestre
      FROM   hor_semestres_detalle,
             hor_estudios e
      WHERE  semestre_id = pSemestre
      and    e.id = pEstudio
      and    e.tipo_id = tipo_estudio_id;
   end;

   function semana_tiene_clase (vIniDate date, vFinDate date)
      return boolean is
      num_clases   number;
   begin
      SELECT   count (*)
      into     num_clases
      FROM     UJI_HORARIOS.HOR_ITEMS items,
               UJI_HORARIOS.HOR_ITEMS_DETALLE det,
               uji_horarios.hor_items_asignaturas asi
      WHERE    items.id = asi.item_id
      and      asi.estudio_id = pEstudio
      AND      items.curso_id = pCurso
      AND      items.grupo_id = pGrupo
      AND      items.semestre_id = pSemestre
      AND      items.dia_semana_id IS NOT NULL
      and      det.item_id = items.id
      and      det.inicio > vIniDate
      and      det.fin < vFinDate
      order by det.inicio;

      return num_clases > 0;
   end;

   procedure info_calendario_det (vFechaIni date, vFechaFin date) is
      vCursoAca           varchar2 (4000);
      vTitulacionNombre   varchar2 (4000);
      vCursoNombre        varchar2 (4000) := pCurso || '�';
      vTextoCabecera      varchar2 (4000);
      vFechaIniStr        varchar2 (4000) := to_char (vFechaIni, 'dd/mm/yyyy');
      vFechaFinStr        varchar2 (4000) := to_char (vFechaFin - 1, 'dd/mm/yyyy');
   begin
      select nombre
      into   vTitulacionNombre
      from   hor_estudios
      where  id = pEstudio;

      vCursoAca := to_char (vCursoAcaNum) || '/' || to_char (vCursoAcaNum + 1);

      if pCurso in ('1', '3') then
         vCursoNombre := pCurso || 'er';
      elsif pCurso = '2' then
         vCursoNombre := '2on';
      elsif pCurso = '4' then
         vCursoNombre := '4t';
      else
         vCursoNombre := pCurso || '�';
      end if;

      vTextoCabecera :=
         vTitulacionNombre || ' - ' || vCursoNombre || ' curs - Grup ' || pGrupo || ' (semestre ' || pSemestre
         || ') - Del ' || vFechaIniStr || ' al ' || vFechaFinStr;
      fop.block (text => vTextoCabecera, border_bottom => 'solid', border_width => 0.05);
   end;

   procedure paginas_calendario_det is
      vFechaIniSemana   date;
      vFechaFinSemana   date;
   begin
      calcula_fechas_semestre;
      vFechaIniSemana := vFechaIniSemestre;
      vFechaFinSemana := vFechaIniSemana + 7;

      while vFechaFinSemana <= vFechaFinSemestre loop
         if semana_tiene_clase (vFechaIniSemana, vFechaFinSemana) then
            info_calendario_det (vFechaIniSemana, vFechaFinSemana);
            calendario_det (vFechaIniSemana, vFechaFinSemana);
            htp.p ('<fo:block break-before="page" color="white">_</fo:block>');
         end if;

         vFechaIniSemana := vFechaFinSemana;
         vFechaFinSemana := vFechaFinSemana + 7;
      end loop;
   end;
BEGIN
   --euji_control_acceso (vItem);
   calcula_curso_aca;
   cabecera;

   if vEsGenerica then
      info_calendario_gen;
      calendario_gen;
      leyenda_asigs;
   else
      paginas_calendario_det;
      leyenda_asigs;
   end if;

   pie;
EXCEPTION
   WHEN OTHERS THEN
      htp.p (sqlerrm);
END;


CREATE OR REPLACE PROCEDURE UJI_HORARIOS.X_VALIDACION_HORARIOS_PDF (
   name_array    IN   euji_util.ident_arr2,
   value_array   IN   euji_util.ident_arr2
) IS
   -- Parametros
   pEstudio    CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pEstudio');
   pSemestre   CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pSemestre');
   pGrupo      CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pGrupo');
   pCurso      CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pCurso');
   v_nombre             varchar2 (2000);

   procedure cabecera is
   begin
      owa_util.mime_header (ccontent_type => 'text/xml', bclose_header => true);
      fop.documentOpen (margin_left => 1.5, margin_right => 1.5, margin_top => 0.5, margin_bottom => 0.5,
                        extent_before => 3, extent_after => 1.5, extent_margin_top => 3, extent_margin_bottom => 1.5);
      fop.pageBodyOpen;
      htp.p (fof.documentheaderopen || fof.blockopen (space_after => 0.1) || fof.tableOpen
             || fof.tablecolumndefinition (column_width => 3) || fof.tablecolumndefinition (column_width => 15)
             || fof.tablebodyopen || fof.tableRowOpen || fof.tablecellopen
             || fof.block (fof.logo_uji, text_align => 'center') || fof.tablecellclose
             || fof.tablecellopen (display_align => 'center', padding => 0.4)
             || fof.block (text => 'Controls horaris', font_size => 12, font_weight => 'bold', font_family => 'Arial',
                           text_align => 'center')
             || fof.block (text => v_nombre, font_size => 12, font_weight => 'bold', font_family => 'Arial',
                           text_align => 'center')
             || fof.block (text => 'Curs: ' || pCurso || ' - Semestre: ' || pSemestre || ' - Grup: ' || pGrupo,
                           font_size => 12, font_weight => 'bold', font_family => 'Arial', text_align => 'center')
             || fof.tablecellclose || fof.tableRowClose || fof.tablebodyclose || fof.tableClose || fof.blockclose
             || fof.documentheaderclose);
      -- El numero paginas se escribe a mano para poner el texto m?s peque?o
      htp.p (fof.documentfooteropen || fof.tableOpen || fof.tablecolumndefinition (column_width => 18)
             || fof.tablebodyopen || fof.tableRowOpen || fof.tablecellopen
             || fof.block (text => 'Pag. <fo:page-number /> de <fo:page-number-citation ref-id="lastBlock" />',
                           text_align => 'right', font_weight => 'bold', font_size => 8)
             || fof.tablecellclose || fof.tableRowClose || fof.tablebodyclose || fof.tableClose
             || fof.documentfooterclose);
      fop.pageFlowOpen;
      fop.block (fof.white_space);
   end;

   procedure pie is
   begin
      htp.p ('<fo:block  id="lastBlock" space-before="1cm" font-size="8pt">');
      htp.p ('Data de generaci?: ' || to_char (sysdate, 'dd/mm/yyy hh24:mi'));
      fop.blockclose ();
      fop.pageFlowClose;
      fop.pageBodyClose;
      fop.documentClose;
   end;

   function espacios (p_espacios in number)
      return varchar2 is
      v_rdo   varchar2 (2000);
   begin
      v_rdo := null;

      for x in 1 .. p_espacios loop
         v_rdo := v_rdo || fof.white_space;
      end loop;

      return v_rdo;
   end;

   procedure p (p_texto in varchar2) is
   begin
      fop.block (espacios (3) || p_texto);
   end;

   procedure control_1 is
      v_aux   number := 0;

      cursor lista_errores_1 is
         select distinct asignatura_id, tipo_subgrupo_id || subgrupo_id subgrupo,
                         decode (tipo_subgrupo_id, 'TE', 1, 'PR', 2, 'LA', 3, 'SE', 4, 0) tipo
         from            uji_horarios.hor_items i,
                         hor_items_asignaturas asi
         where           i.id = asi.item_id
         and             asi.curso_id = pCurso
         and             semestre_id = pSemestre
         and             grupo_id = pGrupo
         and             tipo_subgrupo_id not in ('AV', 'TU')
         and             dia_semana_id is null
         and             estudio_id = pEstudio
         order by        1,
                         decode (tipo_subgrupo_id, 'TE', 1, 'PR', 2, 'LA', 3, 'SE', 4, 0);
   begin
      fop.block ('Control 1 - Subgrups sense planificar', font_size => 12, font_weight => 'bold',
                 border_bottom => 'solid');

      for x in lista_errores_1 loop
         v_aux := v_aux + 1;
         p (x.asignatura_id || ' ' || x.subgrupo);
      end loop;

      if v_aux = 0 then
         fop.block ('No hi ha cap error.', space_before => 0.5);
      elsif v_aux = 1 then
         fop.block ('Trovat ' || v_aux || ' error.', space_before => 0.5);
      else
         fop.block ('Trovats ' || v_aux || ' errors.', space_before => 0.5);
      end if;
   exception
      when others then
         p (sqlerrm);
   end;

   procedure control_2 is
      v_aux          number        := 0;
      v_asignatura   varchar2 (20);

      cursor lista_errores_2_v1 is
         select   asignatura_id, tipo_subgrupo_id || subgrupo_id subgrupo,
                  decode (dia_semana_id,
                          1, 'Dilluns',
                          2, 'Dimarts',
                          3, 'Dimecres',
                          4, 'Dijous',
                          5, 'Divendres',
                          6, 'Dissabte',
                          7, 'Diumenge',
                          'Error'
                         ) dia_semana,
                  hora_inicio, hora_fin,
                  (select asi2.asignatura_id || ' ' || i2.tipo_subgrupo_id || i2.subgrupo_id grupo
                   from   uji_horarios.hor_items i2,
                          uji_horarios.hor_items_asignaturas asi2
                   where  i.id <> i2.id
                   and    i2.id = asi2.item_id
                   and    i.curso_id = i2.curso_id
                   and    i.semestre_id = i2.semestre_id
                   and    i.grupo_id = i2.grupo_id
                   and    i.tipo_subgrupo_id <> i2.tipo_subgrupo_id
                   and    i.dia_semana_id = i2.dia_semana_id
                   and    asi.estudio_id = asi2.estudio_id
                   and    asi.asignatura_id = asi2.asignatura_id
                   and    (   to_number (to_char (i.hora_inicio, 'hh24mi')) between to_number (to_char (i2.hora_inicio,
                                                                                                        'hh24mi'))
                                                                                and to_number (to_char (i2.hora_fin,
                                                                                                        'hh24mi'))
                           or to_number (to_char (i.hora_fin, 'hh24mi')) between to_number (to_char (i2.hora_inicio,
                                                                                                     'hh24mi'))
                                                                             and to_number (to_char (i2.hora_fin,
                                                                                                     'hh24mi'))
                          )
                   and    rownum <= 1) solape
         from     uji_horarios.hor_items i,
                  uji_horarios.hor_items_asignaturas asi
         where    i.id = asi.item_id
         and      asi.curso_id = pCurso
         and      semestre_id = pSemestre
         and      grupo_id = pGrupo
         and      tipo_subgrupo_id in ('TE')
         and      estudio_id = pEstudio
         and      exists (
                     select 1
                     from   uji_horarios.hor_items i2,
                            uji_horarios.hor_items_asignaturas asi2
                     where  i.id <> i2.id
                     and    i2.id = asi2.item_id
                     and    i.curso_id = i2.curso_id
                     and    i.semestre_id = i2.semestre_id
                     and    i.grupo_id = i2.grupo_id
                     and    i.tipo_subgrupo_id <> i2.tipo_subgrupo_id
                     and    i.dia_semana_id = i2.dia_semana_id
                     and    asi.estudio_id = asi2.estudio_id
                     and    asi.asignatura_id = asi2.asignatura_id
                     and    (   to_number (to_char (i.hora_inicio, 'hh24mi')) between to_number
                                                                                               (to_char (i2.hora_inicio,
                                                                                                         'hh24mi'))
                                                                                  and to_number (to_char (i2.hora_fin,
                                                                                                          'hh24mi'))
                             or to_number (to_char (i.hora_fin, 'hh24mi')) between to_number (to_char (i2.hora_inicio,
                                                                                                       'hh24mi'))
                                                                               and to_number (to_char (i2.hora_fin,
                                                                                                       'hh24mi'))
                            ))
         order by 1,
                  2;

      cursor lista_errores_2 is
         select distinct asignatura_id, tipo_subgrupo_id || subgrupo_id subgrupo,
                         decode (dia_semana_id,
                                 1, 'Dilluns',
                                 2, 'Dimarts',
                                 3, 'Dimecres',
                                 4, 'Dijous',
                                 5, 'Divendres',
                                 6, 'Dissabte',
                                 7, 'Diumenge',
                                 'Error'
                                ) dia_semana,
                         to_char (inicio, 'dd/mm/yyyy') fecha, to_char (inicio, 'hh24:mi') hora_inicio,
                         to_char (fin, 'hh24:mi') hora_fin,
                         (select asi2.asignatura_id || ' ' || i2.tipo_subgrupo_id || i2.subgrupo_id || ' - '
                                 || to_char (inicio, 'hh24:mi') || '-' || to_char (fin, 'hh24:mi') grupo
                          from   uji_horarios.hor_items i2,
                                 uji_horarios.hor_items_asignaturas asi2,
                                 uji_horarios.hor_items_detalle det2
                          where  i.id <> i2.id
                          and    i2.id = det2.item_id
                          and    i2.id = asi2.item_id
                          and    i.curso_id = i2.curso_id
                          and    i.semestre_id = i2.semestre_id
                          and    i.grupo_id = i2.grupo_id
                          and    i.tipo_subgrupo_id <> i2.tipo_subgrupo_id
                          and    i.dia_semana_id = i2.dia_semana_id
                          and    asi.estudio_id = asi2.estudio_id
                          and    asi.asignatura_id = asi2.asignatura_id
                          and    trunc (det.inicio) = trunc (det2.inicio)
                          and    (   to_number (to_char (det.inicio + 1 / 1440, 'hh24mi'))
                                        between to_number (to_char (det2.inicio, 'hh24mi'))
                                            and to_number (to_char (det2.fin, 'hh24mi'))
                                  or to_number (to_char (det.fin - 1 / 1440, 'hh24mi'))
                                        between to_number (to_char (det2.inicio, 'hh24mi'))
                                            and to_number (to_char (det2.fin, 'hh24mi'))
                                 )
                          and    rownum <= 1) solape
         from            uji_horarios.hor_items i,
                         uji_horarios.hor_items_asignaturas asi,
                         uji_horarios.hor_items_detalle det
         where           i.id = asi.item_id
         and             i.id = det.item_id
         and             asi.curso_id = pCurso
         and             semestre_id = pSemestre
         and             grupo_id = pGrupo
         and             tipo_subgrupo_id in ('TE')
         and             estudio_id = pEstudio
         and             exists (
                            select 1
                            from   uji_horarios.hor_items i2,
                                   uji_horarios.hor_items_asignaturas asi2,
                                   uji_horarios.hor_items_detalle det2
                            where  i.id <> i2.id
                            and    i2.id = det2.item_id
                            and    i2.id = asi2.item_id
                            and    i.curso_id = i2.curso_id
                            and    i.semestre_id = i2.semestre_id
                            and    i.grupo_id = i2.grupo_id
                            and    i.tipo_subgrupo_id <> i2.tipo_subgrupo_id
                            and    i.dia_semana_id = i2.dia_semana_id
                            and    asi.estudio_id = asi2.estudio_id
                            and    asi.asignatura_id = asi2.asignatura_id
                            and    trunc (det.inicio) = trunc (det2.inicio)
                            and    (   to_number (to_char (det.inicio + 1 / 1440, 'hh24mi'))
                                          between to_number (to_char (det2.inicio, 'hh24mi'))
                                              and to_number (to_char (det2.fin, 'hh24mi'))
                                    or to_number (to_char (det.fin - 1 / 1440, 'hh24mi'))
                                          between to_number (to_char (det2.inicio, 'hh24mi'))
                                              and to_number (to_char ((det2.fin), 'hh24mi'))
                                   ))
         order by        1,
                         2,
                         4;
   begin
      fop.block ('Control 2 - Solpament subgrups TE amb la resta', font_size => 12, font_weight => 'bold',
                 border_bottom => 'solid', space_before => 0.5);
      v_asignatura := 'PRIMERA';

      for x in lista_errores_2 loop
         v_aux := v_aux + 1;

         if     v_aux <> 1
            and v_asignatura <> x.asignatura_id then
            p (' ');
         end if;

         p (x.asignatura_id || ' ' || x.subgrupo || ' ' || x.dia_semana || ' - ' || x.fecha || ' ' || x.hora_inicio
            || '  ->  solapa amb: ' || x.solape);
         v_asignatura := x.asignatura_id;
      end loop;

      if v_aux = 0 then
         fop.block ('No hi ha cap error.', space_before => 0.5);
      elsif v_aux = 1 then
         fop.block ('Trovat ' || v_aux || ' error.', space_before => 0.5);
      else
         fop.block ('Trovats ' || v_aux || ' errors.', space_before => 0.5);
      end if;
   exception
      when others then
         p (sqlerrm);
   end;

   procedure control_3 is
      v_aux          number        := 0;
      v_asignatura   varchar2 (20);

      cursor lista_asignaturas is
         select distinct asi.asignatura_id
         from            uji_horarios.hor_items i,
                         uji_horarios.hor_items_asignaturas asi,
                         uji_horarios.hor_items_detalle det
         where           i.id = asi.item_id
         and             i.id = det.item_id
         and             asi.curso_id = pCurso
         and             semestre_id = pSemestre
         and             grupo_id = pGrupo
         and             estudio_id = pEstudio
         order by        1;

      cursor lista_errores_3 (p_asignatura in varchar2) is
         select   asignatura_id, tipo_subgrupo_id, subgrupo, creditos, horas_totales, sum (horas_detalle) horas_detalle
         from     (select asi.asignatura_id, tipo_subgrupo_id, tipo_subgrupo_id || subgrupo_id subgrupo,
                          decode (tipo_subgrupo_id,
                                  'TE', crd_te,
                                  'PR', crd_pr,
                                  'LA', crd_la,
                                  'SE', crd_se,
                                  'TU', crd_tu,
                                  crd_ev
                                 ) creditos,
                          decode (tipo_subgrupo_id,
                                  'TE', crd_te,
                                  'PR', crd_pr,
                                  'LA', crd_la,
                                  'SE', crd_se,
                                  'TU', crd_tu,
                                  crd_ev
                                 )
                          * 10 horas_totales,
                          (fin - inicio) * 24 horas_detalle
                   from   uji_horarios.hor_items i,
                          uji_horarios.hor_items_asignaturas asi,
                          uji_horarios.hor_items_detalle det,
                          uji_horarios.hor_ext_asignaturas_detalle ad
                   where  i.id = asi.item_id
                   and    i.id = det.item_id
                   and    asi.curso_id = pCurso
                   and    semestre_id = pSemestre
                   and    grupo_id = pGrupo
                   and    estudio_id = pEstudio
                   and    asi.asignatura_id = ad.asignatura_id
                   and    asi.asignatura_id = p_asignatura)
         group by asignatura_id,
                  tipo_subgrupo_id,
                  subgrupo,
                  creditos
         having   horas_totales not between sum (horas_detalle) * 0.95 and sum (horas_detalle) * 1.05
         order by 1,
                  decode (tipo_subgrupo_id, 'TE', 1, 'PR', 2, 'LA', 3, 'SE', 4, 'TU', 5, 6),
                  3;
   begin
      fop.block ('Control 3 - Control d''hores planificades per subgrup', font_size => 12, font_weight => 'bold',
                 border_bottom => 'solid', space_before => 0.5);
      v_asignatura := 'PRIMERA';

      for asi in lista_asignaturas loop
         for x in lista_errores_3 (asi.asignatura_id) loop
            v_aux := v_aux + 1;

            if     v_aux <> 1
               and v_asignatura <> x.asignatura_id then
               p (' ');
            end if;

            if x.horas_totales > x.horas_detalle then
               p (x.asignatura_id || ' ' || x.subgrupo || ' ' || 'Planificades de menys '
                  || to_char (x.horas_totales - x.horas_detalle) || ' hores, cal planificar-ne ' || x.horas_totales);
            else
               p (x.asignatura_id || ' ' || x.subgrupo || ' ' || 'Planificades de m?s '
                  || to_char (x.horas_detalle - x.horas_totales) || ' hores, cal planificar-ne ' || x.horas_totales);
            end if;

            v_asignatura := x.asignatura_id;
         end loop;
      end loop;

      if v_aux = 0 then
         fop.block ('No hi ha cap error.', space_before => 0.5);
      elsif v_aux = 1 then
         fop.block ('Trovat ' || v_aux || ' error.', space_before => 0.5);
      else
         fop.block ('Trovats ' || v_aux || ' errors.', space_before => 0.5);
      end if;
   exception
      when others then
         p (sqlerrm);
   end;

   procedure control_4 is
      v_aux          number        := 0;
      v_asignatura   varchar2 (20);

      cursor lista_errores_4 is
         select   asi.asignatura_id, grupo_id, i.tipo_subgrupo_id, subgrupo_id,
                  to_char (hora_inicio, 'hh24:mi') hora_inicio,
                  decode (dia_semana_id,
                          1, 'Dilluns',
                          2, 'Dimarts',
                          3, 'Dimecres',
                          4, 'Dijous',
                          5, 'Divendres',
                          6, 'Dissabte',
                          7, 'Diumenge',
                          'Error'
                         ) dia_semana,
                  dia_semana_id
         from     uji_horarios.hor_items i,
                  uji_horarios.hor_items_asignaturas asi
         where    i.id = asi.item_id
         and      asi.curso_id = pCurso
         and      semestre_id = pSemestre
         and      grupo_id = pGrupo
         and      estudio_id = pEstudio
         and      dia_semana_id is not null
         and      aula_planificacion_id is null
         order by 1,
                  decode (tipo_subgrupo_id, 'TE', 1, 'PR', 2, 'LA', 3, 'SE', 4, 'TU', 5, 6),
                  4;
   begin
      fop.block ('Control 4 - Control d''hores planificades per subgrup', font_size => 12, font_weight => 'bold',
                 border_bottom => 'solid', space_before => 0.5);
      v_asignatura := 'PRIMERA';

      for x in lista_errores_4 loop
         v_aux := v_aux + 1;

         if     v_aux <> 1
            and v_asignatura <> x.asignatura_id then
            p (' ');
         end if;

         p (x.asignatura_id || ' ' || x.tipo_subgrupo_id || x.subgrupo_id || ' ' || x.dia_semana || ' ' || x.hora_inicio
            || ' - no te aula assignada');
         v_asignatura := x.asignatura_id;
      end loop;
   exception
      when others then
         p (sqlerrm);
   end;

   procedure control_5 is
      v_aux          number          := 0;
      v_aula         number;
      v_dia_semana   number;
      v_hora         varchar2 (20);
      v_txt          varchar2 (2000);

      cursor lista_errores_5 is
         select   dia_semana_id, trunc (det.inicio) fecha, to_char (hora_inicio, 'hh24:mi') hora_inicio,
                  aula_planificacion_id,
                  decode (dia_semana_id,
                          1, 'Dilluns',
                          2, 'Dimarts',
                          3, 'Dimecres',
                          4, 'Dijous',
                          5, 'Divendres',
                          6, 'Dissabte',
                          7, 'Diumenge',
                          'Error'
                         ) dia_semana,
                  nvl (a.nombre, 'Aula mal assignada a l''estudi (' || aula_planificacion_id || ')') nombre_aula
         from     uji_horarios.hor_items i,
                  uji_horarios.hor_items_asignaturas asi,
                  uji_horarios.hor_items_detalle det,
                  uji_horarios.hor_aulas a
         where    i.id = asi.item_id
         and      i.id = det.item_id
         and      asi.curso_id = pCurso
         and      semestre_id = pSemestre
         and      grupo_id = pGrupo
         and      estudio_id = pEstudio
         and      dia_semana_id is not null
         and      aula_planificacion_id is not null
         and      aula_planificacion_id = a.id(+)
         group by asi.asignatura_id,
                  grupo_id,
                  dia_semana_id,
                  trunc (det.inicio),
                  to_char (hora_inicio, 'hh24:mi'),
                  aula_planificacion_id,
                  a.nombre
         having   count (*) > 1
         order by 5,
                  1,
                  3,
                  2;

      cursor lista_subgrupos (p_dia_Semana in number, p_fecha in date, p_hora in varchar2, p_aula in number) is
         select   nvl (comun_texto, asignatura_id) asignatura_id, tipo_subgrupo_id || subgrupo_id subgrupo
         from     uji_horarios.hor_items i,
                  uji_horarios.hor_items_asignaturas asi,
                  uji_horarios.hor_items_detalle det
         where    i.id = asi.item_id
         and      i.id = det.item_id
         and      dia_semana_id = p_dia_semana
         and      trunc (inicio) = p_fecha
         and      to_char (hora_inicio, 'hh24:mi') = p_hora
         and      aula_planificacion_id = p_aula
         and      estudio_id = pEstudio
         order by 1,
                  2;
   begin
      fop.block ('Control 5 - Control de solapament en aules', font_size => 12, font_weight => 'bold',
                 border_bottom => 'solid', space_before => 0.5);
      v_aula := 0;
      v_dia_semana := 0;
      v_hora := '';

      for x in lista_errores_5 loop
         v_aux := v_aux + 1;

         if     v_aux <> 1
            and not (    v_aula = x.aula_planificacion_id
                     and v_dia_semana = x.dia_semana_id
                     and v_hora = x.hora_inicio) then
            p (' ');
         end if;

         v_txt := null;

         if 1 = 2 then
            p (x.nombre_aula || ' - ' || to_char (x.fecha, 'dd/mm/yyyy') || ' ' || x.dia_semana || ' ' || x.hora_inicio);

            for det in lista_subgrupos (x.dia_semana_id, x.fecha, x.hora_inicio, x.aula_planificacion_id) loop
               p (espacios (30) || det.asignatura_id || '-' || det.subgrupo);
            end loop;
         else
            for det in lista_subgrupos (x.dia_semana_id, x.fecha, x.hora_inicio, x.aula_planificacion_id) loop
               v_txt := v_txt || det.asignatura_id || '-' || det.subgrupo || ' | ';
            end loop;

            v_txt := trim (v_txt);
            v_txt := substr (v_txt, 1, length (v_txt) - 2);
            p (x.nombre_aula || ' - ' || to_char (x.fecha, 'dd/mm/yyyy') || ' ' || x.dia_semana || ' ' || x.hora_inicio
               || ' ==> ' || v_txt);
         end if;

         v_aula := x.aula_planificacion_id;
         v_dia_semana := x.dia_semana_id;
         v_hora := x.hora_inicio;
      end loop;
   exception
      when others then
         p (sqlerrm);
   end;

   procedure advertencia_1 is
      v_aux             number        := 0;
      v_asignatura      varchar2 (20);
      v_tipo_subgrupo   varchar2 (20);
      v_subgrupo        number;

      cursor lista_advertencia_1 is
         select   asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo_id
         from     uji_horarios.hor_items i,
                  uji_horarios.hor_items_asignaturas asi
         where    i.id = asi.item_id
         and      asi.curso_id = pCurso
         and      semestre_id = pSemestre
         and      grupo_id = pGrupo
         and      estudio_id = pEstudio
         and      dia_semana_id is not null
         and      aula_planificacion_id is not null
         group by asignatura_id,
                  grupo_id,
                  tipo_subgrupo_id,
                  subgrupo_id
         having   count (distinct aula_planificacion_id) > 1;

      cursor lista_aulas (p_asignatura in varchar2, p_tipo_subgrupo in varchar2, p_subgrupo in number) is
         select distinct nvl (a.nombre, 'Aula mal assignada a l''estudi (' || aula_planificacion_id || ')') nombre_aula
         from            uji_horarios.hor_items i,
                         uji_horarios.hor_items_asignaturas asi,
                         uji_horarios.hor_aulas a
         where           i.id = asi.item_id
         and             i.aula_planificacion_id = a.id(+)
         and             asi.curso_id = pCurso
         and             semestre_id = pSemestre
         and             grupo_id = pGrupo
         and             estudio_id = pEstudio
         and             dia_semana_id is not null
         and             aula_planificacion_id is not null
         and             asignatura_id = p_asignatura
         and             tipo_subgrupo_id = p_tipo_subgrupo
         and             subgrupo_id = p_subgrupo
         order by        1;
   begin
      fop.block ('Advertencia 1 - Classes d''un subgrup en aules diferents', font_size => 12, font_weight => 'bold',
                 border_bottom => 'solid', space_before => 0.5);
      v_aux := v_aux + 1;
      v_asignatura := 'PRIMERA';
      v_tipo_subgrupo := 'XX';
      v_subgrupo := 0;

      for x in lista_advertencia_1 loop
         v_aux := v_aux + 1;

         if     v_aux <> 1
            and not (    v_asignatura = x.asignatura_id
                     and v_tipo_subgrupo = x.tipo_subgrupo_id
                     and v_subgrupo = x.subgrupo_id
                    ) then
            p (' ');
         end if;

         for det in lista_aulas (x.asignatura_id, x.tipo_subgrupo_id, x.subgrupo_id) loop
            p (x.asignatura_id || ' - ' || x.tipo_subgrupo_id || x.subgrupo_id || ' ' || det.nombre_aula);
         end loop;

         v_asignatura := x.asignatura_id;
         v_tipo_subgrupo := x.tipo_subgrupo_id;
         v_subgrupo := x.subgrupo_id;
      end loop;
   exception
      when others then
         p (sqlerrm);
   end;
BEGIN
   select nombre
   into   v_nombre
   from   hor_estudios
   where  id = pEstudio;

   cabecera;
   control_1;
   control_2;
   control_3;
   control_4;
   control_5;
   advertencia_1;
   pie;
EXCEPTION
   WHEN OTHERS THEN
      htp.p (sqlerrm);
END X_VALIDACION_HORARIOS_PDF;


CREATE OR REPLACE FORCE VIEW UJI_HORARIOS.HOR_EXT_CARGOS_PER (ID,
                                                              PERSONA_ID,
                                                              NOMBRE,
                                                              CENTRO_ID,
                                                              CENTRO,
                                                              ESTUDIO_ID,
                                                              ESTUDIO,
                                                              CARGO_ID,
                                                              CARGO,
                                                              DEPARTAMENTO_ID,
                                                              DEPARTAMENTO,
                                                              AREA_ID,
                                                              AREA,
                                                              CURSO_ID,
                                                              ORIGEN
                                                             ) AS
   select "ID", "PERSONA_ID", "NOMBRE", "CENTRO_ID", "CENTRO", "ESTUDIO_ID", "ESTUDIO", "CARGO_ID", "CARGO",
          "DEPARTAMENTO_ID", "DEPARTAMENTO", "AREA_ID", "AREA", "CURSO_ID", "ORIGEN"
     from (select rownum id, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, TITULACION_ID estudio_id, TITULACION estudio,
                  CARGO_ID, CARGO, DEPARTAMENTO_ID, DEPARTAMENTO, AREA_ID, AREA, CURSO_ID, origen
             from (
/* PDI de centro */
                   select cp.per_id persona_id, p.nombre || ' ' || apellido1 || ' ' || apellido2 nombre,
                          ulogica_id centro_id, ubic.nombre centro, tit.id titulacion_id, tit.nombre titulacion,
                          tc.id cargo_id, tc.nombre cargo, d.id departamento_id, d.nombre departamento, a.id area_id,
                          a.nombre area, null curso_id, 1 origen
                     from grh_grh.grh_cargos_per cp,
                          gri_est.est_ubic_estructurales ubic,
                          gra_Exp.exp_v_titu_todas tit,
                          uji_horarios.hor_tipos_Cargos tc,
                          gri_per.per_personas p,
                          uji_horarios.hor_departamentos d,
                          uji_horarios.hor_areas a
                    where (    f_fin is null
                           and (   f_fin is null
                                or f_fin >= sysdate)
                           and crg_id in (189, 195, 188, 30))
                      and ulogica_id = ubic.id
                      and ulogica_id = tit.uest_id
                      and tit.activa = 'S'
                      and tit.tipo = 'G'
                      and tc.id = 3
                      and cp.per_id = p.id
                      and ulogica_id = d.centro_id
                      and d.id = a.departamento_id
                   union all
/* PAS de centro */
                   select cp.per_id persona_id, p.nombre || ' ' || apellido1 || ' ' || apellido2 nombre,
                          ubicacion_id centro_id, ubic.nombre centro, tit.id titulacion_id, tit.nombre titulacion,
                          tc.id cargo_id, tc.nombre cargo, d.id departamento_id, d.nombre departamento, a.id area_id,
                          a.nombre area, null curso_id, 2 origen
                     from grh_grh.grh_vw_contrataciones_ult cp,
                          gri_est.est_ubic_estructurales ubic,
                          gra_Exp.exp_v_titu_todas tit,
                          uji_horarios.hor_tipos_Cargos tc,
                          gri_per.per_personas p,
                          uji_horarios.hor_departamentos d,
                          uji_horarios.hor_areas a
                    where ubicacion_id = ubic.id
                      and ubicacion_id = tit.uest_id
                      and tit.activa = 'S'
                      and tit.tipo = 'G'
                      and tc.id = 4
                      and cp.per_id = p.id
                      --and ubicacion_id = d.centro_id
                      and decode (ubicacion_id, 97, 2922, 96, 2922, ubicacion_id) = d.centro_id
                      and d.id = a.departamento_id
                   union all
/* directores de titulacion */
                   select cp.per_id persona_id, p.nombre || ' ' || apellido1 || ' ' || apellido2 nombre,
                          ulogica_id centro_id, ubic.nombre centro, tit.id titulacion_id, tit.nombre titulacion,
                          tc.id cargo_id, tc.nombre cargo, null departamento_id, '' departamento, null area_id, '' area,
                          null curso_id, 3 origen
                     from grh_grh.grh_cargos_per cp,
                          gri_est.est_ubic_estructurales ubic,
                          gra_Exp.exp_v_titu_todas tit,
                          uji_horarios.hor_tipos_Cargos tc,
                          gri_per.per_personas p
                    where (    f_fin is null
                           and (   f_fin is null
                                or f_fin >= sysdate)
                           and crg_id in (192, 193))
                      and ulogica_id = ubic.id
                      and ulogica_id = tit.uest_id
                      and tit.activa = 'S'
                      and tit.tipo = 'G'
                      and tc.id = 1
                      and cp.per_id = p.id
                      and cp.tit_id = tit.id
                   union all
/* coordinadores de curso */
                   select cp.per_id persona_id, p.nombre || ' ' || apellido1 || ' ' || apellido2 nombre,
                          ulogica_id centro_id, ubic.nombre centro, tit.id titulacion_id, tit.nombre titulacion,
                          tc.id cargo_id, tc.nombre cargo, null departamento_id, '' departamento, null area_id, '' area,
                          ciclo_cargo curso_id, 4 origen
                     from grh_grh.grh_cargos_per cp,
                          gri_est.est_ubic_estructurales ubic,
                          gra_Exp.exp_v_titu_todas tit,
                          uji_horarios.hor_tipos_Cargos tc,
                          gri_per.per_personas p
                    where (    f_fin is null
                           and (   f_fin is null
                                or f_fin >= sysdate)
                           and crg_id in (305))
                      and ulogica_id = ubic.id
                      and ulogica_id = tit.uest_id
                      and tit.activa = 'S'
                      and tit.tipo = 'G'
                      and tc.id = 2
                      and cp.per_id = p.id
                      and cp.tit_id = tit.id
                   union all
/* directores de departamento */
                   select cp.per_id persona_id, p.nombre || ' ' || apellido1 || ' ' || apellido2 nombre, c.id centro_id,
                          c.nombre centro, null titulacion_id, '' titulacion, tc.id cargo_id, tc.nombre cargo,
                          ulogica_id departamento_id, d.nombre departamento, a.id area_id, a.nombre area, null curso_id,
                          5 origen
                     from grh_grh.grh_cargos_per cp,
                          uji_horarios.hor_tipos_Cargos tc,
                          gri_per.per_personas p,
                          est_ubic_estructurales d,
                          est_relaciones_ulogicas r,
                          est_ubic_estructurales c,
                          uji_horarios.hor_Areas a
                    where (    f_fin is null
                           and (   f_fin is null
                                or f_fin >= sysdate)
                           and crg_id in (178, 194))
                      and tc.id = 6
                      and cp.per_id = p.id
                      and ulogica_id = d.id
                      and uest_id_relacionad = d.id
                      and d.tuest_id = 'DE'
                      --and uest_id = c.id
                      and decode (ulogica_id, 97, 2922, 96, 2922, uest_id) = c.id
                      and trel_id = 4
                      and ulogica_id = a.departamento_id
                   union all
                   /* pas de departamento */
                   select cp.per_id persona_id, p.nombre || ' ' || apellido1 || ' ' || apellido2 nombre, c.id centro_id,
                          c.nombre centro, null titulacion_id, '' titulacion, tc.id cargo_id, tc.nombre cargo,
                          ubic.id departamento_id, ubic.nombre departamento, a.id area_id, a.nombre area, null curso_id,
                          6 origen
                     from grh_grh.grh_vw_contrataciones_ult cp,
                          gri_est.est_ubic_estructurales ubic,
                          uji_horarios.hor_tipos_Cargos tc,
                          gri_per.per_personas p,
                          est_relaciones_ulogicas r,
                          est_ubic_estructurales c,
                          uji_horarios.hor_areas a
                    where ubicacion_id = ubic.id
                      and tc.id = 5
                      and cp.per_id = p.id
                      and cp.act_id = 'PAS'
                      and ubicacion_id = uest_id_relacionad
                      and trel_id = 4
                      --and uest_id = c.id
                      and decode (ubic.id, 97, 2922, 96, 2922, uest_id) = c.id
                      and ubic.id = departamento_id
                   union all
                   /* permisos extra */
                   select x.persona_id, x.nombre, x.centro_id, x.centro, x.titulacion_id, titulacion, x.cargo_id,
                          x.cargo, x.departamento_id, x.departamento,
                          decode (cargo_id, 6, decode (area_id, null, a.id, area_id), area_id) area_id,
                          decode (cargo_id, 6, decode (area_id, null, a.nombre, area), area) area, curso_id, 7 origen
                     from (select per.persona_id, p.nombre || ' ' || apellido1 || ' ' || apellido2 nombre,
                                  ubic.id centro_id, ubic.nombre centro, tit.id titulacion_id, tit.nombre titulacion,
                                  tc.id cargo_id, tc.nombre cargo, departamento_id, dep.nombre departamento, area_id,
                                  (select nombre
                                     from gri_est.est_ubic_estructurales
                                    where id = area_id) area, per.curso_id
                             from uji_horarios.hor_permisos_extra per,
                                  gri_per.per_personas p,
                                  (select *
                                     from gra_Exp.exp_v_titu_todas
                                    where activa = 'S'
                                      and tipo = 'G') tit,
                                  uji_horarios.hor_tipos_cargos tc,
                                  gri_est.est_ubic_estructurales ubic,
                                  gri_est.est_ubic_estructurales dep
                            where persona_id = p.id
                              and estudio_id = tit.id(+)
                              and tipo_Cargo_id = tc.id
                              and tit.uest_id = ubic.id(+)
                              and per.departamento_id = dep.id(+)) x,
                          uji_horarios.hor_areas a
                    where (   (    cargo_id <> 6
                               and a.id = (select min (id)
                                             from uji_horarios.hor_areas))
                           or (    cargo_id = 6
                               and area_id is not null
                               and a.id = area_id)
                           or (    cargo_id = 6
                               and area_id is null
                               and x.departamento_id = a.departamento_id)
                          )
                   union all
/* administradores */
                   select distinct per.persona_id, p.nombre || ' ' || apellido1 || ' ' || apellido2 nombre,
                                   u.id centro_id, u.nombre centro, tit.id titulacion_id, tit.nombre titulacion,
                                   tc.id cargo_id, tc.nombre cargo, null departamento_id, '' departamento, null area_id,
                                   '' area, null curso_id, 8 origen
                              from uji_apa.apa_vw_personas_items per,
                                   gri_per.per_personas p,
                                   gra_Exp.exp_v_titu_todas tit,
                                   uji_horarios.hor_tipos_cargos tc,
                                   gri_Est.est_ubic_estructurales u
                             where persona_id = p.id
                               and tc.id = 1
                               and tit.activa = 'S'
                               and tit.tipo = 'G'
                               and aplicacion_id = 46
                               and role = 'ADMIN'
                               and tit.uest_id = u.id
                   union all
                   select distinct per.persona_id, p.nombre || ' ' || apellido1 || ' ' || apellido2 nombre,
                                   c.id centro_id, c.nombre centro, null titulacion_id, '' titulacion, tc.id cargo_id,
                                   tc.nombre cargo, dep.id departamento_id, dep.nombre departamento, null area_id,
                                   '' area, null curso_id, 9 origen
                              from uji_apa.apa_vw_personas_items per,
                                   gri_per.per_personas p,
                                   est_ubic_estructurales dep,
                                   uji_horarios.hor_tipos_cargos tc,
                                   gri_Est.est_ubic_estructurales c,
                                   est_relaciones_ulogicas r
                             where persona_id = p.id
                               and tc.id = 6
                               and dep.status = 'A'
                               and dep.tuest_id = 'DE'
                               and aplicacion_id = 46
                               and role = 'ADMIN'
                               --and decode(dep.id,97,2943,dep.id)
                               and dep.id = r.uest_id_relacionad
                               and r.trel_id = 4
                               and decode (dep.id, 97, 2922, 96, 2922, r.uest_id) = c.id));

							   
CREATE OR REPLACE FORCE VIEW UJI_HORARIOS.HOR_V_AGRUPACIONES_ITEMS (AGRUPACION_ID,
                                                                    NOMBRE_AGRUPACION,
                                                                    ASIGNATURA_ID,
                                                                    ESTUDIO_ID,
                                                                    CURSO_ID,
                                                                    CARACTER_ID,
                                                                    ITEM_ID,
                                                                    SEMESTRE_ID,
                                                                    AULA_PLANIFICACION_ID,
                                                                    COMUN,
                                                                    PORCENTAJE_COMUN,
                                                                    GRUPO_ID,
                                                                    TIPO_SUBGRUPO_ID,
                                                                    TIPO_SUBGRUPO,
                                                                    SUBGRUPO_ID,
                                                                    DIA_SEMANA_ID,
                                                                    HORA_INICIO,
                                                                    HORA_FIN,
                                                                    DESDE_EL_DIA,
                                                                    HASTA_EL_DIA,
                                                                    TIPO_ASIGNATURA_ID,
                                                                    TIPO_ASIGNATURA,
                                                                    TIPO_ESTUDIO_ID,
                                                                    TIPO_ESTUDIO,
                                                                    PLAZAS,
                                                                    REPETIR_CADA_SEMANAS,
                                                                    NUMERO_ITERACIONES,
                                                                    DETALLE_MANUAL,
                                                                    COMUN_TEXTO,
                                                                    AULA_PLANIFICACION_NOMBRE,
                                                                    CREDITOS
                                                                   ) AS
   select distinct a.id agrupacion_id, nombre nombre_agrupacion, aa.asignatura_id, estudio_id, ia.curso_id,
                   ia.caracter_id, item_id, i.semestre_id, aula_planificacion_id, comun, porcentaje_comun, grupo_id,
                   tipo_subgrupo_id, tipo_subgrupo, subgrupo_id, dia_semana_id, hora_inicio, hora_fin, desde_el_dia,
                   hasta_el_dia, tipo_asignatura_id, tipo_asignatura, tipo_estudio_id, tipo_estudio, plazas,
                   repetir_cada_semanas, numero_iteraciones, detalle_manual, comun_texto, aula_planificacion_nombre,
                   creditos
              from hor_agrupaciones a,
                   hor_agrupaciones_asignaturas aa,
                   hor_items_asignaturas ia,
                   hor_items i
             where a.id = aa.agrupacion_id
               and aa.asignatura_id = ia.asignatura_id
               and ia.item_id = i.id;




CREATE OR REPLACE FUNCTION UJI_HORARIOS.hor_f_fecha_orden (
   p_estudio         in   number,
   p_semestre        in   number,
   p_curso           in   number,
   p_asignatura      in   varchar2,
   p_grupo           in   varchar2,
   p_subgrupo_tipo   in   varchar2,
   p_subgrupo        in   number,
   p_dia_semana      in   number,
   p_fecha           in   date
)
   RETURN NUMBER IS
   v_aux   NUMBER;
BEGIN
   select count (*)
   into   v_aux
   from   (select ia.estudio_id, ia.curso_id, i.semestre_id, i.grupo_id, i.tipo_subgrupo_id, i.subgrupo_id,
                  i.dia_Semana_id, ia.asignatura_id, fecha_inicio, fecha_fin, fecha_examenes_inicio, fecha_examenes_fin,
                  i.desde_el_dia, hasta_el_dia, repetir_cada_semanas, numero_iteraciones, detalle_manual, c.fecha,
                  tipo_dia, dia_semana
           from   hor_estudios e,
                  hor_semestres_detalle s,
                  hor_items i,
                  hor_items_asignaturas ia,
                  hor_ext_calendario c
           where  e.tipo_id = s.tipo_estudio_id
           and    i.semestre_id = s.semestre_id
           and    c.fecha between fecha_inicio and nvl (fecha_examenes_fin, fecha_fin)
           and    c.dia_semana_id = i.dia_semana_id
           and    tipo_dia = 'L'
           and    detalle_manual = 0
           and    i.semestre_id = p_semestre
           and    ia.curso_id = p_curso
           and    i.id = ia.item_id
           and    ia.asignatura_id = p_asignatura
           and    ia.estudio_id = p_estudio
           and    ia.estudio_id = e.id
           and    i.grupo_id = p_grupo
           and    i.tipo_subgrupo_id = p_subgrupo_tipo
           and    i.subgrupo_id = p_subgrupo
           and    i.dia_semana_id = p_dia_semana
           and    fecha <= p_fecha);

   RETURN v_aux;
END hor_f_fecha_orden;


CREATE OR REPLACE TRIGGER UJI_HORARIOS.mutante_3_final_2014
   after insert or update of dia_semana_id,
                             desde_el_dia,
                             hasta_el_dia,
                             repetir_cada_semanas,
                             numero_iteraciones,
                             hora_inicio,
                             hora_fin
   ON UJI_HORARIOS.HOR_ITEMS
begin
   declare
      v_aux         number;
      v_num_items   number;
      v_id          number;
      v_hora_ini    date;
      v_hora_fin    date;
      v_ult_orden   number;

      cursor reg (v_rowid rowid) is
         select *
           from uji_horarios.hor_items
          where rowid = v_rowid;

      cursor lista_detalle (
         p_id                     in   number,
         p_detalle_manual         in   number,
         p_dia_semana_id          in   number,
         p_semestre_id            in   number,
         p_numero_iteraciones     in   number,
         p_repetir_cada_Semanas   in   number,
         p_hasta_el_dia           in   date,
         p_desde_el_dia           in   date,
         p_subgrupo_id            in   number,
         p_tipo_subgrupo_id       in   varchar2,
         p_grupo_id               in   varchar2
      ) is
         select   todo.*, row_number () over (partition by id order by fecha) orden_final
             from (select distinct id, fecha, docencia_paso_1, docencia_paso_2, docencia, orden_id, numero_iteraciones,
                                   repetir_cada_semanas, fecha_inicio, fecha_fin, semestre_id, grupo_id,
                                   tipo_subgrupo_id, subgrupo_id, dia_semana_id, tipo_dia, festivos
                              from (select i.id, fecha, docencia docencia_paso_1,
                                           decode (nvl (d.repetir_cada_semanas, 1),
                                                   1, docencia,
                                                   decode (mod (orden_id, d.repetir_cada_semanas), 1, docencia, 'N')
                                                  ) docencia_paso_2,
                                           decode
                                                 (tipo_dia,
                                                  'F', 'N',
                                                  decode (d.numero_iteraciones,
                                                          null, decode (nvl (d.repetir_cada_semanas, 1),
                                                                        1, docencia,
                                                                        decode (mod (orden_id, d.repetir_cada_semanas),
                                                                                1, docencia,
                                                                                'N'
                                                                               )
                                                                       ),
                                                          decode (sign (((orden_id - festivos) / d.repetir_cada_semanas
                                                                        )
                                                                        - (d.numero_iteraciones)),
                                                                  1, 'N',
                                                                  decode (nvl (d.repetir_cada_semanas, 1),
                                                                          1, docencia,
                                                                          decode (mod (orden_id, d.repetir_cada_semanas),
                                                                                  1, docencia,
                                                                                  'N'
                                                                                 )
                                                                         )
                                                                 )
                                                         )
                                                 ) docencia,
                                           d.orden_id, d.numero_iteraciones, d.repetir_cada_semanas, d.fecha_inicio,
                                           d.fecha_fin, d.estudio_id, d.semestre_id, d.asignatura_id, d.grupo_id,
                                           d.tipo_subgrupo_id, d.subgrupo_id, d.dia_semana_id, tipo_dia, festivos
                                      from (select id, fecha,
                                                   hor_contar_festivos (decode (x.desde_el_dia,
                                                                                null, fecha_inicio,
                                                                                x.desde_el_dia
                                                                               ),
                                                                        x.fecha, x.dia_semana_id,
                                                                        x.repetir_cada_semanas) festivos,
                                                   row_number () over (partition by decode
                                                                                      (decode
                                                                                             (sign (x.fecha
                                                                                                    - fecha_inicio),
                                                                                              -1, 'N',
                                                                                              decode (sign (fecha_fin
                                                                                                            - x.fecha),
                                                                                                      -1, 'N',
                                                                                                      'S'
                                                                                                     )
                                                                                             ),
                                                                                       'S', decode
                                                                                          (decode
                                                                                              (sign
                                                                                                  (x.fecha
                                                                                                   - decode
                                                                                                        (x.desde_el_dia,
                                                                                                         null, fecha_inicio,
                                                                                                         x.desde_el_dia
                                                                                                        )),
                                                                                               -1, 'N',
                                                                                               decode
                                                                                                  (sign
                                                                                                      (decode
                                                                                                          (x.hasta_el_dia,
                                                                                                           null, fecha_fin,
                                                                                                           x.hasta_el_dia
                                                                                                          )
                                                                                                       - x.fecha),
                                                                                                   -1, 'N',
                                                                                                   'S'
                                                                                                  )
                                                                                              ),
                                                                                           'S', 'S',
                                                                                           'N'
                                                                                          ),
                                                                                       'N'
                                                                                      ), id, estudio_id, semestre_id, asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo_id, dia_semana_id order by fecha)
                                                                                                               orden_id,
                                                   decode (hor_f_fecha_entre (x.fecha, fecha_inicio, fecha_fin),
                                                           'S', decode (hor_f_fecha_entre (x.fecha,
                                                                                           decode (desde_el_dia,
                                                                                                   null, fecha_inicio,
                                                                                                   desde_el_dia
                                                                                                  ),
                                                                                           decode (hasta_el_dia,
                                                                                                   null, fecha_fin,
                                                                                                   hasta_el_dia
                                                                                                  )),
                                                                        'S', 'S',
                                                                        'N'
                                                                       ),
                                                           'N'
                                                          ) docencia,
                                                   estudio_id, semestre_id, grupo_id, tipo_subgrupo_id, subgrupo_id,
                                                   dia_semana_id, asignatura_id, fecha_inicio, fecha_fin,
                                                   fecha_examenes_inicio, fecha_examenes_fin, desde_el_dia,
                                                   hasta_el_dia, repetir_cada_semanas, numero_iteraciones,
                                                   detalle_manual, tipo_dia, dia_semana
                                              from (select distinct i.id, null estudio_id, i.semestre_id, i.grupo_id,
                                                                    i.tipo_subgrupo_id, i.subgrupo_id, i.dia_semana_id,
                                                                    null asignatura_id, fecha_inicio, fecha_fin,
                                                                    fecha_examenes_inicio, fecha_examenes_fin,
                                                                    i.desde_el_dia, hasta_el_dia, repetir_cada_semanas,
                                                                    numero_iteraciones, detalle_manual, c.fecha,
                                                                    tipo_dia, dia_semana
                                                               from hor_v_fechas_estudios s,
                                                                    (select p_id id, p_detalle_manual detalle_manual,
                                                                            p_dia_semana_id dia_semana_id,
                                                                            p_semestre_id semestre_id,
                                                                            p_numero_iteraciones numero_iteraciones,
                                                                            p_repetir_cada_semanas repetir_cada_semanas,
                                                                            p_hasta_el_dia hasta_el_dia,
                                                                            p_desde_el_dia desde_el_dia,
                                                                            p_subgrupo_id subgrupo_id,
                                                                            p_tipo_subgrupo_id tipo_subgrupo_id,
                                                                            p_grupo_id grupo_id
                                                                       from dual) i,
                                                                    hor_ext_calendario c
                                                              where i.semestre_id = s.semestre_id
                                                                and trunc (c.fecha) between fecha_inicio
                                                                                        and decode (fecha_examenes_fin,
                                                                                                    null, fecha_fin,
                                                                                                    fecha_examenes_fin
                                                                                                   )
                                                                and c.dia_semana_id = i.dia_semana_id
                                                                and tipo_dia in ('L', 'E', 'F')
                                                                and vacaciones = 0
                                                                and s.tipo_estudio_id = 'G'
                                                                and detalle_manual = 0
                                                                and s.estudio_id in (
                                                                          select estudio_id
                                                                            from hor_items_asignaturas
                                                                           where item_id = i.id
                                                                             and curso_id = s.curso_id)
                                                                                                       --and i.id = 559274
                                                   ) x) d,
                                           (select p_id id, p_detalle_manual detalle_manual,
                                                   p_dia_semana_id dia_semana_id, p_semestre_id semestre_id,
                                                   p_numero_iteraciones numero_iteraciones,
                                                   p_repetir_cada_semanas repetir_cada_semanas,
                                                   p_hasta_el_dia hasta_el_dia, p_desde_el_dia desde_el_dia,
                                                   p_subgrupo_id subgrupo_id, p_tipo_subgrupo_id tipo_subgrupo_id,
                                                   p_grupo_id grupo_id
                                              from dual) i
                                     where i.semestre_id = d.semestre_id
                                       and i.grupo_id = d.grupo_id
                                       and i.tipo_subgrupo_id = d.tipo_subgrupo_id
                                       and i.subgrupo_id = d.subgrupo_id
                                       and i.dia_semana_id = d.dia_semana_id
                                       and i.detalle_manual = 0
                                       and i.id = d.id
                                    union all
                                    select distinct c.id, c.fecha, 'N' docencia_paso_1, 'N' docencia_paso_2,
                                                    decode (d.id, null, 'N', 'S') docencia,
                                                    row_number () over (partition by d.item_id order by d.inicio)
                                                                                                               orden_id,
                                                    numero_iteraciones, repetir_cada_semanas, fecha_inicio, fecha_fin,
                                                    estudio_id, semestre_id, asignatura_id, grupo_id, tipo_subgrupo_id,
                                                    subgrupo_id, dia_semana_id, tipo_dia,
                                                    decode (tipo_dia, 'F', 1, 0) festivos
                                               from (select distinct i.id, c.fecha, numero_iteraciones,
                                                                     repetir_cada_semanas, s.fecha_inicio, s.fecha_fin,
                                                                     null estudio_id, i.semestre_id, null asignatura_id,
                                                                     i.grupo_id, i.tipo_subgrupo_id, i.subgrupo_id,
                                                                     i.dia_semana_id, tipo_dia
                                                                from hor_v_fechas_estudios s,
                                                                     (select p_id id, p_detalle_manual detalle_manual,
                                                                             p_dia_semana_id dia_semana_id,
                                                                             p_semestre_id semestre_id,
                                                                             p_numero_iteraciones numero_iteraciones,
                                                                             p_repetir_cada_semanas
                                                                                                   repetir_cada_semanas,
                                                                             p_hasta_el_dia hasta_el_dia,
                                                                             p_desde_el_dia desde_el_dia,
                                                                             p_subgrupo_id subgrupo_id,
                                                                             p_tipo_subgrupo_id tipo_subgrupo_id,
                                                                             p_grupo_id grupo_id
                                                                        from dual) i,
                                                                     hor_ext_calendario c
                                                               where i.semestre_id = s.semestre_id
                                                                 and trunc (c.fecha) between fecha_inicio
                                                                                         and decode (fecha_examenes_fin,
                                                                                                     null, fecha_fin,
                                                                                                     fecha_examenes_fin
                                                                                                    )
                                                                 and c.dia_semana_id = i.dia_semana_id
                                                                 and tipo_dia in ('L', 'E', 'F')
                                                                 and s.tipo_estudio_id = 'G'
                                                                 and vacaciones = 0
                                                                 and detalle_manual = 1
                                                                 and s.estudio_id in (
                                                                          select estudio_id
                                                                            from hor_items_asignaturas
                                                                           where item_id = i.id
                                                                             and curso_id = s.curso_id)) c,
                                                    hor_items_detalle d
                                              where c.id = d.item_id(+)
                                                and trunc (c.fecha) = trunc (d.inicio(+)))
                             where id = p_id
                               and docencia = 'S') todo
         order by 18;

      function buscar_item (p_item_id in number, p_orden in number)
         return number is
         v_aux   number;

         cursor lista_detalle_ordenado is
            select id, row_number () over (partition by item_id order by trunc (inicio)) orden
              from hor_items_detalle
             where item_id = p_item_id;
      begin
         v_aux := -1;

         for det in lista_detalle_ordenado loop
            if det.orden = p_orden then
               v_aux := det.id;
            end if;
         end loop;

         return v_aux;
      exception
         when no_data_found then
            return (-1);
         when others then
            return (0);
      end;
   begin
      for i in 1 .. mutante_items.v_num loop
         for v_reg in reg (mutante_items.v_var_tabla (i)) loop
            if v_reg.detalle_manual = 0 then
               v_hora_ini := v_Reg.hora_inicio;
               v_hora_fin := v_Reg.hora_fin;

               for x in lista_detalle (v_reg.id, v_reg.detalle_manual, v_reg.dia_semana_id, v_reg.semestre_id,
                                       v_reg.numero_iteraciones, v_reg.repetir_cada_Semanas, v_reg.hasta_el_dia,
                                       v_reg.desde_el_dia, v_reg.subgrupo_id, v_reg.tipo_subgrupo_id, v_reg.grupo_id) loop
                  v_id := buscar_item (v_Reg.id, x.orden_final);

                  --dbms_output.put_line (v_Reg.id || ' ' || to_char (x.fecha, 'dd/mm/yyyyy') || ' ' || x.orden_final);
                  if v_id > 0 then
                     /* por defecto actualiza fechas */
                     --dbms_output.put_line ('update ' || x.orden_final);
                     update hor_items_detalle
                     set inicio =
                            to_Date (to_char (x.fecha, 'dd/mm/yyyy') || ' ' || to_char (v_hora_ini, 'hh24:mi:ss'),
                                     'dd/mm/yyyy hh24:mi:ss'),
                         fin =
                            to_Date (to_char (x.fecha, 'dd/mm/yyyy') || ' ' || to_char (v_hora_fin, 'hh24:mi:ss'),
                                     'dd/mm/yyyy hh24:mi:ss')
                     where  id = v_id;
                  elsif v_id = -1 then
                     --dbms_output.put_line ('insertar ' || x.orden_final);
                     begin
                        /* solo inserta si no le quedan fechas por actualizar */
                        v_aux := uji_horarios.hibernate_sequence.nextval;

                        insert into hor_items_detalle
                                    (id, item_id,
                                     inicio,
                                     fin
                                    )
                        values      (v_aux, v_Reg.id,
                                     to_date (to_char (x.fecha, 'dd/mm/yyyy') || ' '
                                              || to_char (v_hora_ini, 'hh24:mi:ss'),
                                              'dd/mm/yyyy hh24:mi:ss'),
                                     to_date (to_char (x.fecha, 'dd/mm/yyyy') || ' '
                                              || to_char (v_hora_fin, 'hh24:mi:ss'),
                                              'dd/mm/yyyy hh24:mi:ss')
                                    );
                     exception
                        when others then
                           null;
                     end;
                  end if;

                  if x.orden_id > nvl (v_ult_orden, 0) then
                     v_ult_orden := x.orden_final;
                  end if;
               end loop;

               /* y solo borra si sobran al final */
               delete      hor_items_detalle
               where       id in (
                              select id
                                from (select id, inicio, fin,
                                             row_number () over (partition by item_id order by trunc (inicio)) orden
                                        from hor_items_detalle
                                       where item_id = v_reg.id)
                               where orden > v_ult_orden);
            end if;
         end loop;
      end loop;
   end;
end mutante_3_final_2014;



CREATE OR REPLACE FORCE VIEW UJI_HORARIOS.HOR_V_ITEMS_DETALLE (ID,
                                                               FECHA,
                                                               DOCENCIA_PASO_1,
                                                               DOCENCIA_PASO_2,
                                                               DOCENCIA,
                                                               ORDEN_ID,
                                                               NUMERO_ITERACIONES,
                                                               REPETIR_CADA_SEMANAS,
                                                               FECHA_INICIO,
                                                               FECHA_FIN,
                                                               ESTUDIO_ID,
                                                               SEMESTRE_ID,
                                                               ASIGNATURA_ID,
                                                               GRUPO_ID,
                                                               TIPO_SUBGRUPO_ID,
                                                               SUBGRUPO_ID,
                                                               DIA_SEMANA_ID,
                                                               TIPO_DIA,
                                                               FESTIVOS
                                                              ) AS
   SELECT i.id, fecha, docencia docencia_paso_1,
          DECODE (NVL (d.repetir_cada_semanas, 1),
                  1, docencia,
                  DECODE (MOD (orden_id, d.repetir_cada_semanas), 1, docencia, 'N')
                 ) docencia_paso_2,
          decode (tipo_dia,
                  'F', 'N',
                  DECODE (d.numero_iteraciones,
                          NULL, DECODE (NVL (d.repetir_cada_semanas, 1),
                                        1, docencia,
                                        DECODE (MOD (orden_id, d.repetir_cada_semanas), 1, docencia, 'N')
                                       ),
                          DECODE (SIGN (((orden_id - festivos) / d.repetir_cada_Semanas) - (d.numero_iteraciones)),
                                  1, 'N',
                                  DECODE (NVL (d.repetir_cada_semanas, 1),
                                          1, docencia,
                                          DECODE (MOD (orden_id, d.repetir_cada_semanas), 1, docencia, 'N')
                                         )
                                 )
                         )
                 ) docencia,
          d.orden_id, d.numero_iteraciones, d.repetir_cada_semanas, d.fecha_inicio, d.fecha_fin, d.estudio_id,
          d.semestre_id, d.asignatura_id, d.grupo_id, d.tipo_subgrupo_id, d.subgrupo_id, d.dia_semana_id, tipo_dia,
          festivos
     FROM (SELECT id, fecha,
                  hor_contar_festivos (NVL (x.desde_el_dia, fecha_inicio), x.fecha, x.dia_semana_id,
                                       x.repetir_cada_semanas) festivos,
                  ROW_NUMBER () OVER (PARTITION BY DECODE
                                                       (decode (sign (x.fecha - fecha_inicio),
                                                                -1, 'N',
                                                                decode (sign (fecha_fin - x.fecha), -1, 'N', 'S')
                                                               ),
                                                        'S', DECODE (decode (sign (x.fecha
                                                                                   - NVL (x.desde_el_dia, fecha_inicio)),
                                                                             -1, 'N',
                                                                             decode (sign (NVL (x.hasta_el_dia,
                                                                                                fecha_fin)
                                                                                           - x.fecha),
                                                                                     -1, 'N',
                                                                                     'S'
                                                                                    )
                                                                            ),
                                                                     'S', 'S',
                                                                     'N'
                                                                    ),
                                                        'N'
                                                       ), id, estudio_id, semestre_id, asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo_id, dia_semana_id ORDER BY fecha)
                                                                                                               orden_id,
                  DECODE (hor_f_fecha_entre (x.fecha, fecha_inicio, fecha_fin),
                          'S', DECODE (hor_f_fecha_entre (x.fecha, NVL (desde_el_dia, fecha_inicio),
                                                          NVL (hasta_el_dia, fecha_fin)),
                                       'S', 'S',
                                       'N'
                                      ),
                          'N'
                         ) docencia,
                  estudio_id, semestre_id, grupo_id, tipo_subgrupo_id, subgrupo_id, dia_Semana_id, asignatura_id,
                  fecha_inicio, fecha_fin, fecha_examenes_inicio, fecha_examenes_fin, desde_el_dia, hasta_el_dia,
                  repetir_cada_semanas, numero_iteraciones, detalle_manual, tipo_dia, dia_semana
             FROM (SELECT distinct i.id, null estudio_id, i.semestre_id, i.grupo_id, i.tipo_subgrupo_id, i.subgrupo_id,
                          i.dia_Semana_id, null asignatura_id, fecha_inicio, fecha_fin, fecha_examenes_inicio,
                          fecha_examenes_fin, i.desde_el_dia, hasta_el_dia, repetir_cada_semanas, numero_iteraciones,
                          detalle_manual, c.fecha, tipo_dia, dia_semana
                     FROM hor_v_fechas_estudios s,
                          hor_items i,
                          hor_ext_calendario c
                    WHERE i.semestre_id = s.semestre_id
                      AND trunc (c.fecha) BETWEEN fecha_inicio AND NVL (fecha_examenes_fin, fecha_fin)
                      AND c.dia_semana_id = i.dia_semana_id
                      AND tipo_dia IN ('L', 'E', 'F')
                      and vacaciones = 0
                      and s.tipo_estudio_id = 'G'
                      AND detalle_manual = 0
                      and s.estudio_id in (select estudio_id
                                             from hor_items_asignaturas
                                            where item_id = i.id
                                              and curso_id = s.curso_id)
                                                                        --and i.id = 559274
                  ) x) d,
          hor_items i
    WHERE i.semestre_id = d.semestre_id
      AND i.grupo_id = d.grupo_id
      AND i.tipo_subgrupo_id = d.tipo_subgrupo_id
      AND i.subgrupo_id = d.subgrupo_id
      AND i.dia_semana_id = d.dia_semana_id
      AND i.detalle_manual = 0
      AND i.id = d.id
   UNION ALL
   SELECT c.id, c.fecha, 'N' docencia_paso_1, 'N' docencia_paso_2, DECODE (d.id, NULL, 'N', 'S') docencia, 1 orden_id,
          numero_iteraciones, repetir_cada_semanas, fecha_inicio, fecha_fin, estudio_id, semestre_id, asignatura_id,
          grupo_id, tipo_subgrupo_id, subgrupo_id, dia_semana_id, tipo_dia, decode (tipo_dia, 'F', 1, 0) festivos
     FROM (SELECT distinct i.id, c.fecha, numero_iteraciones, repetir_cada_semanas, s.fecha_inicio, s.fecha_fin,
                           null estudio_id, i.semestre_id, null asignatura_id, i.grupo_id, i.tipo_subgrupo_id,
                           i.subgrupo_id, i.dia_semana_id, tipo_dia
                      FROM hor_v_fechas_estudios s,
                           hor_items i,
                           hor_ext_calendario c
                     WHERE i.semestre_id = s.semestre_id
                       AND trunc (c.fecha) BETWEEN fecha_inicio AND NVL (fecha_examenes_fin, fecha_fin)
                       AND c.dia_semana_id = i.dia_semana_id
                       AND tipo_dia IN ('L', 'E', 'F')
                       and s.tipo_estudio_id = 'G'
                       and vacaciones = 0
                       AND detalle_manual = 1
                       and s.estudio_id in (select estudio_id
                                              from hor_items_asignaturas
                                             where item_id = i.id
                                               and curso_id = s.curso_id)) c,
          hor_items_detalle d
    WHERE c.id = d.item_id(+)
      AND trunc (c.fecha) = trunc (d.inicio(+));

CREATE OR REPLACE TRIGGER UJI_HORARIOS.mutante_2_por_fila
   after insert or update of dia_semana_id,
                             desde_el_dia,
                             hasta_el_dia,
                             repetir_cada_semanas,
                             numero_iteraciones,
                             hora_inicio,
                             hora_fin
   ON UJI_HORARIOS.HOR_ITEMS
   referencing old as new new as old
   for each row
begin
   mutante_items.v_num := mutante_items.v_num + 1;
   mutante_items.v_var_tabla (mutante_items.v_num) := :new.rowid;
end;

