CREATE OR REPLACE package UJI_HORARIOS.horarios_util as
   type ident_arr2 is table of varchar2 (4000)
      index by binary_integer;
end;



CREATE OR REPLACE TRIGGER UJI_HORARIOS.mutante_3_final_2014
   after insert or update of dia_semana_id,
                             desde_el_dia,
                             hasta_el_dia,
                             repetir_cada_semanas,
                             numero_iteraciones,
                             hora_inicio,
                             hora_fin
   ON UJI_HORARIOS.HOR_ITEMS
begin
   declare
      v_aux           number;
      v_cuantos       number;
      v_num_items     number;
      v_id            number;
      v_id_dest       number;
      v_hora_ini      date;
      v_hora_fin      date;
      v_dia_ini       date;
      v_dia_fin       date;
      v_ult_orden     number;
      v_dia_item      number;
      v_dia_Detalle   number;
      v_semestre      number;
      v_dia_primero   date;
      v_matr_id       horarios_util.ident_arr2;

      cursor reg (v_rowid rowid) is
         select *
         from   uji_horarios.hor_items
         where  rowid = v_rowid;

      cursor lista_detalle (
         p_id                     in   number,
         p_detalle_manual         in   number,
         p_dia_semana_id          in   number,
         p_semestre_id            in   number,
         p_numero_iteraciones     in   number,
         p_repetir_cada_Semanas   in   number,
         p_hasta_el_dia           in   date,
         p_desde_el_dia           in   date,
         p_subgrupo_id            in   number,
         p_tipo_subgrupo_id       in   varchar2,
         p_grupo_id               in   varchar2
      ) is
         select   todo.*, row_number () over (partition by id order by fecha) orden_final
         from     (select distinct id, fecha, docencia_paso_1, docencia_paso_2, docencia, orden_id, numero_iteraciones,
                                   repetir_cada_semanas, fecha_inicio, fecha_fin, semestre_id, grupo_id,
                                   tipo_subgrupo_id, subgrupo_id, dia_semana_id, tipo_dia, festivos
                   from            (select i.id, fecha, docencia docencia_paso_1,
                                           decode (nvl (d.repetir_cada_semanas, 1),
                                                   1, docencia,
                                                   decode (mod (orden_id, d.repetir_cada_semanas), 1, docencia, 'N')
                                                  ) docencia_paso_2,
                                           decode
                                                 (tipo_dia,
                                                  'F', 'N',
                                                  decode (d.numero_iteraciones,
                                                          null, decode (nvl (d.repetir_cada_semanas, 1),
                                                                        1, docencia,
                                                                        decode (mod (orden_id, d.repetir_cada_semanas),
                                                                                1, docencia,
                                                                                'N'
                                                                               )
                                                                       ),
                                                          decode (sign (((orden_id - festivos) / d.repetir_cada_semanas
                                                                        )
                                                                        - (d.numero_iteraciones)),
                                                                  1, 'N',
                                                                  decode (nvl (d.repetir_cada_semanas, 1),
                                                                          1, docencia,
                                                                          decode (mod (orden_id, d.repetir_cada_semanas),
                                                                                  1, docencia,
                                                                                  'N'
                                                                                 )
                                                                         )
                                                                 )
                                                         )
                                                 ) docencia,
                                           d.orden_id, d.numero_iteraciones, d.repetir_cada_semanas, d.fecha_inicio,
                                           d.fecha_fin, d.estudio_id, d.semestre_id, d.asignatura_id, d.grupo_id,
                                           d.tipo_subgrupo_id, d.subgrupo_id, d.dia_semana_id, tipo_dia, festivos
                                    from   (select id, fecha,
                                                   hor_contar_festivos (decode (x.desde_el_dia,
                                                                                null, fecha_inicio,
                                                                                x.desde_el_dia
                                                                               ),
                                                                        x.fecha, x.dia_semana_id,
                                                                        x.repetir_cada_semanas) festivos,
                                                   row_number () over (partition by decode
                                                                                      (decode
                                                                                             (sign (x.fecha
                                                                                                    - fecha_inicio),
                                                                                              -1, 'N',
                                                                                              decode (sign (fecha_fin
                                                                                                            - x.fecha),
                                                                                                      -1, 'N',
                                                                                                      'S'
                                                                                                     )
                                                                                             ),
                                                                                       'S', decode
                                                                                          (decode
                                                                                              (sign
                                                                                                  (x.fecha
                                                                                                   - decode
                                                                                                        (x.desde_el_dia,
                                                                                                         null, fecha_inicio,
                                                                                                         x.desde_el_dia
                                                                                                        )),
                                                                                               -1, 'N',
                                                                                               decode
                                                                                                  (sign
                                                                                                      (decode
                                                                                                          (x.hasta_el_dia,
                                                                                                           null, fecha_fin,
                                                                                                           x.hasta_el_dia
                                                                                                          )
                                                                                                       - x.fecha),
                                                                                                   -1, 'N',
                                                                                                   'S'
                                                                                                  )
                                                                                              ),
                                                                                           'S', 'S',
                                                                                           'N'
                                                                                          ),
                                                                                       'N'
                                                                                      ), id, estudio_id, semestre_id, asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo_id, dia_semana_id order by fecha)
                                                                                                               orden_id,
                                                   decode (hor_f_fecha_entre (x.fecha, fecha_inicio, fecha_fin),
                                                           'S', decode (hor_f_fecha_entre (x.fecha,
                                                                                           decode (desde_el_dia,
                                                                                                   null, fecha_inicio,
                                                                                                   desde_el_dia
                                                                                                  ),
                                                                                           decode (hasta_el_dia,
                                                                                                   null, fecha_fin,
                                                                                                   hasta_el_dia
                                                                                                  )),
                                                                        'S', 'S',
                                                                        'N'
                                                                       ),
                                                           'N'
                                                          ) docencia,
                                                   estudio_id, semestre_id, grupo_id, tipo_subgrupo_id, subgrupo_id,
                                                   dia_semana_id, asignatura_id, fecha_inicio, fecha_fin,
                                                   fecha_examenes_inicio, fecha_examenes_fin, desde_el_dia,
                                                   hasta_el_dia, repetir_cada_semanas, numero_iteraciones,
                                                   detalle_manual, tipo_dia, dia_semana
                                            from   (select distinct i.id, null estudio_id, i.semestre_id, i.grupo_id,
                                                                    i.tipo_subgrupo_id, i.subgrupo_id, i.dia_semana_id,
                                                                    null asignatura_id, fecha_inicio, fecha_fin,
                                                                    fecha_examenes_inicio, fecha_examenes_fin,
                                                                    decode (sign (i.desde_el_dia - fecha_inicio),
                                                                            -1, fecha_inicio,
                                                                            i.desde_el_dia
                                                                           ) desde_el_dia,
                                                                    hasta_el_dia, repetir_cada_semanas,
                                                                    numero_iteraciones, detalle_manual, c.fecha,
                                                                    tipo_dia, dia_semana
                                                    from            hor_v_fechas_estudios s,
                                                                    (select p_id id, p_detalle_manual detalle_manual,
                                                                            p_dia_semana_id dia_semana_id,
                                                                            p_semestre_id semestre_id,
                                                                            p_numero_iteraciones numero_iteraciones,
                                                                            p_repetir_cada_semanas repetir_cada_semanas,
                                                                            p_hasta_el_dia hasta_el_dia,
                                                                            p_desde_el_dia desde_el_dia,
                                                                            p_subgrupo_id subgrupo_id,
                                                                            p_tipo_subgrupo_id tipo_subgrupo_id,
                                                                            p_grupo_id grupo_id
                                                                     from   dual) i,
                                                                    hor_ext_calendario c
                                                    where           i.semestre_id = s.semestre_id
                                                    and             trunc (c.fecha) between fecha_inicio
                                                                                        and decode (fecha_examenes_fin,
                                                                                                    null, fecha_fin,
                                                                                                    fecha_examenes_fin
                                                                                                   )
                                                    and             c.dia_semana_id = i.dia_semana_id
                                                    and             tipo_dia in ('L', 'E', 'F')
                                                    and             vacaciones = 0
                                                    and             s.tipo_estudio_id = 'G'
                                                    and             detalle_manual = 0
                                                    and             s.estudio_id in (
                                                                          select estudio_id
                                                                          from   hor_items_asignaturas
                                                                          where  item_id = i.id
                                                                          and    curso_id = s.curso_id)
                                                                                                       --and i.id = 559274
                                                   ) x) d,
                                           (select p_id id, p_detalle_manual detalle_manual,
                                                   p_dia_semana_id dia_semana_id, p_semestre_id semestre_id,
                                                   p_numero_iteraciones numero_iteraciones,
                                                   p_repetir_cada_semanas repetir_cada_semanas,
                                                   p_hasta_el_dia hasta_el_dia, p_desde_el_dia desde_el_dia,
                                                   p_subgrupo_id subgrupo_id, p_tipo_subgrupo_id tipo_subgrupo_id,
                                                   p_grupo_id grupo_id
                                            from   dual) i
                                    where  i.semestre_id = d.semestre_id
                                    and    i.grupo_id = d.grupo_id
                                    and    i.tipo_subgrupo_id = d.tipo_subgrupo_id
                                    and    i.subgrupo_id = d.subgrupo_id
                                    and    i.dia_semana_id = d.dia_semana_id
                                    and    i.detalle_manual = 0
                                    and    i.id = d.id
                                    union all
                                    select distinct c.id, c.fecha, 'N' docencia_paso_1, 'N' docencia_paso_2,
                                                    decode (d.id, null, 'N', 'S') docencia,
                                                    row_number () over (partition by d.item_id order by d.inicio)
                                                                                                               orden_id,
                                                    numero_iteraciones, repetir_cada_semanas, fecha_inicio, fecha_fin,
                                                    estudio_id, semestre_id, asignatura_id, grupo_id, tipo_subgrupo_id,
                                                    subgrupo_id, dia_semana_id, tipo_dia,
                                                    decode (tipo_dia, 'F', 1, 0) festivos
                                    from            (select distinct i.id, c.fecha, numero_iteraciones,
                                                                     repetir_cada_semanas, s.fecha_inicio, s.fecha_fin,
                                                                     null estudio_id, i.semestre_id, null asignatura_id,
                                                                     i.grupo_id, i.tipo_subgrupo_id, i.subgrupo_id,
                                                                     i.dia_semana_id, tipo_dia
                                                     from            hor_v_fechas_estudios s,
                                                                     (select p_id id, p_detalle_manual detalle_manual,
                                                                             p_dia_semana_id dia_semana_id,
                                                                             p_semestre_id semestre_id,
                                                                             p_numero_iteraciones numero_iteraciones,
                                                                             p_repetir_cada_semanas
                                                                                                   repetir_cada_semanas,
                                                                             p_hasta_el_dia hasta_el_dia,
                                                                             p_desde_el_dia desde_el_dia,
                                                                             p_subgrupo_id subgrupo_id,
                                                                             p_tipo_subgrupo_id tipo_subgrupo_id,
                                                                             p_grupo_id grupo_id
                                                                      from   dual) i,
                                                                     hor_ext_calendario c
                                                     where           i.semestre_id = s.semestre_id
                                                     and             trunc (c.fecha) between fecha_inicio
                                                                                         and decode (fecha_examenes_fin,
                                                                                                     null, fecha_fin,
                                                                                                     fecha_examenes_fin
                                                                                                    )
                                                     and             c.dia_semana_id = i.dia_semana_id
                                                     and             tipo_dia in ('L', 'E', 'F')
                                                     and             s.tipo_estudio_id = 'G'
                                                     and             vacaciones = 0
                                                     and             detalle_manual = 1
                                                     and             s.estudio_id in (
                                                                          select estudio_id
                                                                          from   hor_items_asignaturas
                                                                          where  item_id = i.id
                                                                          and    curso_id = s.curso_id)) c,
                                                    hor_items_detalle d
                                    where           c.id = d.item_id(+)
                                    and             trunc (c.fecha) = trunc (d.inicio(+)))
                   where           id = p_id
                   and             docencia = 'S') todo
         order by 18;

      cursor lista_detalla (p_item in number) is
         select   id, inicio, fin
         from     hor_items_Detalle
         where    item_id = p_item
         order by id;

      function buscar_item (p_item_id in number, p_orden in number)
         return number is
         v_aux   number;

         cursor lista_detalle_ordenado is
            select id, row_number () over (partition by item_id order by trunc (inicio)) orden
            from   hor_items_detalle
            where  item_id = p_item_id;
      begin
         v_aux := -1;

         for det in lista_detalle_ordenado loop
            if det.orden = p_orden then
               v_aux := det.id;
            end if;
         end loop;

         return v_aux;
      exception
         when no_data_found then
            return (-1);
         when others then
            return (0);
      end;

      procedure preparar_Fechas (p_item_id in number) is
         cursor lista_detalle_ordenado is
            select   id, inicio
            from     hor_items_detalle
            where    item_id = p_item_id
            order by trunc (inicio);

         v_aux   number;
      begin
         v_aux := 0;

         for det in lista_detalle_ordenado loop
            v_aux := v_aux + 1;
            v_matr_id (v_aux) := det.id;
         end loop;
      end;

      function buscar_item_v2 (p_item_id in number, p_orden in number)
         return number is
         v_aux   number;
      begin
         if p_orden <= v_matr_id.count then
            v_aux := v_matr_id (p_orden);
         else
            v_aux := 0;
         end if;

         return v_aux;
      exception
         when others then
            return (0);
      end;
   begin
      for i in 1 .. mutante_items.v_num loop
         for v_reg in reg (mutante_items.v_var_tabla (i)) loop
            if v_reg.detalle_manual = 0 then
               v_hora_ini := v_Reg.hora_inicio;
               v_hora_fin := v_Reg.hora_fin;
               preparar_fechas (v_reg.id);

               for x in lista_detalle (v_reg.id, v_reg.detalle_manual, v_reg.dia_semana_id, v_reg.semestre_id,
                                       v_reg.numero_iteraciones, v_reg.repetir_cada_Semanas, v_reg.hasta_el_dia,
                                       v_Reg.desde_el_dia, v_reg.subgrupo_id, v_reg.tipo_subgrupo_id, v_reg.grupo_id) loop
                  --v_id := buscar_item (v_Reg.id, x.orden_final);
                  v_id := buscar_item_v2 (v_Reg.id, x.orden_final);
                  dbms_output.put_line ('--------------------');
                  /*
                  dbms_output.put_line (v_reg.id || ', ' || v_reg.detalle_manual || ', ' || v_reg.dia_semana_id || ', '
                                        || v_reg.semestre_id || ', ' || v_reg.numero_iteraciones || ', '
                                        || v_reg.repetir_cada_Semanas || ', ' || v_reg.hasta_el_dia || ', '
                                        || v_Reg.desde_el_dia || ', ' || v_reg.subgrupo_id || ', '
                                        || v_reg.tipo_subgrupo_id || ', ' || v_reg.grupo_id);
                                        */
                  dbms_output.put_line (v_Reg.id || ' ' || v_id || ' ' || to_char (x.fecha, 'dd/mm/yyyy') || ' '
                                        || x.orden_final);

                  if v_id > 0 then
                     /* por defecto actualiza fechas */
                     select inicio, fin
                     into   v_dia_ini, v_dia_fin
                     from   hor_items_detalle
                     where  id = v_id;

                     dbms_output.put_line ('update ' || x.orden_final || ' - de ' || to_char (v_dia_ini, 'dd/mm/yyyy')
                                           || ' a ' || to_char (x.fecha, 'dd/mm/yyyy') || ' - '
                                           || to_char (v_Reg.desde_el_dia, 'dd/mm/yyyy'));

                     update hor_items_detalle
                     set inicio =
                            to_date (to_char (x.fecha, 'dd/mm/yyyy') || ' ' || to_char (v_hora_ini, 'hh24:mi:ss'),
                                     'dd/mm/yyyy hh24:mi:ss'),
                         fin =
                            to_date (to_char (x.fecha, 'dd/mm/yyyy') || ' ' || to_char (v_hora_fin, 'hh24:mi:ss'),
                                     'dd/mm/yyyy hh24:mi:ss')
                     where  id = v_id;
                  elsif v_id = -1 then
                     dbms_output.put_line ('insertar ' || x.orden_final);

                     begin
                        /* solo inserta si no le quedan fechas por actualizar */
                        v_aux := uji_horarios.hibernate_sequence.nextval;

                        insert into hor_items_detalle
                                    (id, item_id,
                                     inicio,
                                     fin
                                    )
                        values      (v_aux, v_Reg.id,
                                     to_date (to_char (x.fecha, 'dd/mm/yyyy') || ' '
                                              || to_char (v_hora_ini, 'hh24:mi:ss'),
                                              'dd/mm/yyyy hh24:mi:ss'),
                                     to_date (to_char (x.fecha, 'dd/mm/yyyy') || ' '
                                              || to_char (v_hora_fin, 'hh24:mi:ss'),
                                              'dd/mm/yyyy hh24:mi:ss')
                                    );
                     exception
                        when others then
                           null;
                     end;
                  end if;

                  if x.orden_id > nvl (v_ult_orden, 0) then
                     v_ult_orden := x.orden_final;
                  end if;
               end loop;

               /* y solo borra si sobran al final */
               delete      hor_items_detalle
               where       id in (
                              select id
                              from   (select id, inicio, fin,
                                             row_number () over (partition by item_id order by trunc (inicio)) orden
                                      from   hor_items_detalle
                                      where  item_id = v_reg.id)
                              where  orden > v_ult_orden);
            else
               /* detalle manual, si ha cambiado de dia de semana u hora, aplicar este cambio a las sesiones detalladas */
               dbms_output.put_line (1);

               begin
                  v_cuantos := 0;
                  --v_dia_item := to_number (to_char (v_Reg.hora_inicio, 'd'));
                  v_dia_item := v_reg.dia_Semana_id;

                  /*
                  select distinct to_number(to_char(inicio, 'd'))
                  into v_dia_detalle
                  from hor_items_detalle
                  where item_id = v_reg.id
                  and to_number(to_char(v_Reg.hora_inicio, 'd')) <> v_dia_item
                  and rownum <= 1;
                  */
                  declare
                     cursor lista_dias_semana_2 is
                        select distinct to_number (to_char (inicio, 'd')) dia_semana
                        from            hor_items_detalle
                        where           item_id = v_reg.id
                        and             to_number (to_char (inicio, 'd')) <> v_dia_item;
                  begin
                     dbms_output.put_line ('11-' || v_dia_item);

                     for det in lista_dias_Semana_2 loop
                        dbms_output.put_line (111);
                        v_cuantos := v_cuantos + 1;
                        v_dia_detalle := det.dia_semana;

                        if v_dia_item <> v_dia_detalle then
                           update hor_items_detalle
                           set inicio = inicio + (v_dia_item - v_dia_detalle),
                               fin = fin + (v_dia_item - v_dia_detalle)
                           where  item_id = v_reg.id
                           and    to_number (to_char (inicio, 'd')) = det.dia_Semana;
                        end if;
                     end loop;
                  end;

                  if v_cuantos > 0 then
                     delete      uji_horarios.hor_items_detalle
                     where       id in (select d.id
                                        from   uji_horarios.hor_items_detalle d,
                                               uji_horarios.hor_ext_calendario c
                                        where  item_id = v_Reg.id
                                        and    trunc (inicio) = fecha
                                        and    tipo_dia in ('F', 'N'));
                  end if;
               exception
                  when no_data_found then
                     null;
               end;

               dbms_output.put_line (2);

               /* detalle manual, tambien hay que actualizar la hora inicio y hora final */
               begin
                  select inicio, fin
                  into   v_hora_ini, v_hora_fin
                  from   hor_items_detalle
                  where  item_id = v_Reg.id
                  and    rownum <= 1;

                  if    to_char (v_hora_ini, 'hh24:mi:ss') <> to_char (v_reg.hora_inicio, 'hh24:mi:ss')
                     or to_char (v_hora_fin, 'hh24:mi:ss') <> to_char (v_reg.hora_fin, 'hh24:mi:ss') then
                     update hor_items_detalle
                     set inicio =
                            to_date (to_char (inicio, 'dd/mm/yyyy') || ' ' || to_char (v_reg.hora_inicio, 'hh24:mi:ss'),
                                     'dd/mm/yyyy hh24:mi:ss'),
                         fin =
                            to_date (to_char (fin, 'dd/mm/yyyy') || ' ' || to_char (v_reg.hora_fin, 'hh24:mi:ss'),
                                     'dd/mm/yyyy hh24:mi:ss')
                     where  item_id = v_reg.id;
                  end if;
               exception
                  when no_data_found then
                     null;
               end;
            end if;
         end loop;
      end loop;
   end;
end mutante_3_final_2014;


