
ALTER TABLE UJI_HORARIOS.HOR_EXAMENES_ASIGNATURAS
 ADD (curso_id  NUMBER);


ALTER TABLE UJI_HORARIOS.HOR_EXAMENES
 ADD (comentario  VARCHAR2(1000));



CREATE OR REPLACE FORCE VIEW UJI_HORARIOS.HOR_V_ITEMS_CREDITOS_DETALLE (ID,
                                                                        NOMBRE,
                                                                        CREDITOS_PROFESOR,
                                                                        ITEM_ID,
                                                                        CRD_PROF,
                                                                        CRD_ITEM,
                                                                        CUANTOS,
                                                                        CRD_FINAL,
                                                                        GRUPO_ID,
                                                                        TIPO,
                                                                        ASIGNATURA_ID,
                                                                        ASIGNATURA_TXT,
                                                                        DEPARTAMENTO_ID,
                                                                        AREA_ID
                                                                       ) AS
   select id, nombre, creditos_profesor, item_id, round (crd_prof, 2) crd_prof, crd_item, cuantos,
          round (nvl (crd_prof, trunc (crd_item / cuantos, 2)), 2) crd_final, GRUPO_ID,
          TIPO_SUBGRUPO_ID || SUBGRUPO_ID tipo, asignatura_id, asignatura_txt, departamento_id, area_id
   from   (select   h.id, nombre, h.creditos creditos_profesor, ip.item_id, ip.creditos crd_prof, i.creditos crd_item,
                    (select count (*)
                     from   hor_items_profesores ip2
                     where  item_id = ip.item_id) cuantos, GRUPO_ID, TIPO_SUBGRUPO_ID, SUBGRUPO_ID,
                    wm_concat (asignatura_id) asignatura_txt, min (asignatura_id) asignatura_id, departamento_id,
                    area_id
           from     hor_profesores h,
                    hor_items_profesores ip,
                    hor_items i,
                    hor_items_asignaturas ia
           where    h.id = ip.profesor_id
           and      ip.item_id = i.id
           and      i.id = ia.item_id
           group by h.id,
                    nombre,
                    h.creditos,
                    ip.item_id,
                    ip.creditos,
                    i.creditos,
                    GRUPO_ID,
                    TIPO_SUBGRUPO_ID,
                    SUBGRUPO_ID,
                    departamento_id,
                    area_id)
   union
   select id, nombre, creditos creditos_profesor, null item_id, null crd_prof, null crd_item, 0 cuantos, 0 crd_final,
          null grupo_id, null tipo, null asignatura_id, null asignatura_txt, departamento_id, area_id
   from   hor_profesores h
   where  not exists (select profesor_id
                      from   hor_items_profesores
                      where  profesor_id = h.id);


					
					
					
CREATE OR REPLACE FORCE VIEW UJI_HORARIOS.HOR_V_PROFESOR_CREDITOS (ID,
                                                                   NOMBRE,
                                                                   CREDITOS_PROFESOR,
                                                                   CREDITOS_ASIGNADOS,
                                                                   CREDITOS_PENDIENTES,
                                                                   DEPARTAMENTO_ID,
                                                                   AREA_ID
                                                                  ) AS
   select   id, nombre, creditos_profesor, sum (crd_final) creditos_asignados,
            creditos_profesor - sum (crd_final) creditos_pendientes, departamento_id, area_id
   from     (select distinct id, nombre, creditos_profesor, grupo_id, tipo, asignatura_id, asignatura_txt, crd_final,
                             departamento_id, area_id
             from            hor_v_items_creditos_detalle)
   group by id,
            nombre,
            creditos_profesor,
            departamento_id,
            area_id;

					
ALTER TABLE UJI_HORARIOS.HOR_TIPOS_CARGOS
 ADD (departamento  NUMBER  DEFAULT 0  NOT NULL);

ALTER TABLE UJI_HORARIOS.HOR_TIPOS_CARGOS
 ADD (estudio  NUMBER DEFAULT 0  NOT NULL);

ALTER TABLE UJI_HORARIOS.HOR_EXT_CARGOS_PER
 ADD (departamento_id  NUMBER);

ALTER TABLE UJI_HORARIOS.HOR_EXT_CARGOS_PER
 ADD (departamento  VARCHAR2(1000));

ALTER TABLE UJI_HORARIOS.HOR_EXT_CARGOS_PER
 ADD (area_id  NUMBER);

CREATE OR REPLACE TRIGGER UJI_HORARIOS.hot_items_detallle_delete
   BEFORE DELETE
   ON UJI_HORARIOS.HOR_ITEMS_DETALLE
   REFERENCING NEW AS New OLD AS Old
   FOR EACH ROW
DECLARE
   tmpVar   NUMBER;
BEGIN
   if deleting then
      delete      hor_items_det_profesores
      where       detalle_id = :old.id;
   end if;
END hot_items_detallle_delete;


 CREATE OR REPLACE FORCE VIEW UJI_HORARIOS.HOR_V_ITEMS_PROF_DETALLE (ID,
                                                                    DETALLE_ID,
                                                                    DET_PROFESOR_ID,
                                                                    ITEM_ID,
                                                                    INICIO,
                                                                    FIN,
                                                                    DESCRIPCION,
                                                                    PROFESOR_ID,
                                                                    DETALLE_MANUAL,
                                                                    CREDITOS,
                                                                    CREDITOS_DETALLE,
                                                                    SELECCIONADO,
                                                                    NOMBRE,
                                                                    PROFESOR_COMPLETO
                                                                   ) AS
   select   to_number (id || det_profesor_id || item_id || to_char (inicio, 'yyyymmddhh24miss')) id, id detalle_id,
            det_profesor_id, item_id, inicio, fin, descripcion, profesor_id, detalle_manual, creditos, creditos_detalle,
            decode (detalle_manual, 0, 'S', decode (min (orden), 1, 'S', 'N')) seleccionado, nombre, profesor_completo
   from     (select d.*, profesor_id, p.detalle_manual, i.creditos, p.creditos creditos_detalle, p.id det_profesor_id,
                    99 orden, nombre, (select wm_concat (nombre)
                                       from   hor_items_profesores pr,
                                              hor_profesores p
                                       where  item_id = i.id
                                       and    pr.profesor_id = p.id) profesor_completo
             from   hor_items i,
                    hor_items_detalle d,
                    hor_items_profesores p,
                    hor_profesores pro
             where  i.id = d.item_id
             and    i.id = p.item_id
             and    p.profesor_id = pro.id
             union all
             select i.id, i.id item_id, null inicio, null fin, null descripcion, profesor_id, p.detalle_manual,
                    i.creditos, round (p.creditos, 2) creditos_detalle, p.id det_profesor_id, 99 orden, nombre,
                    (select wm_concat (nombre)
                     from   hor_items_profesores pr,
                            hor_profesores p
                     where  item_id = i.id
                     and    pr.profesor_id = p.id) profesor_completo
             from   hor_items i,
                    hor_items_profesores p,
                    hor_profesores pro
             where  i.id = p.item_id
             and    p.profesor_id = pro.id
             and    not exists (select 1
                                from   hor_items_detalle
                                where  item_id = i.id)
             union all
             select d.*, profesor_id, p.detalle_manual, i.creditos, p.creditos creditos_detalle, p.id det_profesor_id,
                    1 orden, nombre, (select wm_concat (nombre)
                                      from   hor_items_profesores pr,
                                             hor_profesores p
                                      where  item_id = i.id
                                      and    pr.profesor_id = p.id) profesor_completo
             from   hor_items i,
                    hor_items_detalle d,
                    hor_items_profesores p,
                    hor_items_det_profesores dp,
                    hor_profesores pro
             where  i.id = d.item_id
             and    i.id = p.item_id
             and    p.detalle_manual = 1
             and    dp.detalle_id = d.id
             and    item_profesor_id = p.id
             and    p.profesor_id = pro.id)
   group by id,
            item_id,
            inicio,
            fin,
            descripcion,
            profesor_id,
            detalle_manual,
            creditos,
            creditos_detalle,
            det_profesor_id,
            nombre,
            profesor_completo;

