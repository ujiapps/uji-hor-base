CREATE OR REPLACE FORCE VIEW UJI_HORARIOS.HOR_V_FECHAS_CONVOCATORIAS_EST (ID,
                                                                          CONVOCATORIA_ID,
                                                                          NOMBRE,
                                                                          FECHA_EXAMENES_INICIO,
                                                                          FECHA_EXAMENES_FIN,
                                                                          ESTUDIO_ID
                                                                         ) AS
   select e.id * 10000 + c.id id, c.id convocatoria_id, c.nombre, c.fecha_examenes_inicio, c.fecha_examenes_fin,
          e.id estudio_id
     from hor_v_Fechas_convocatorias c,
          hor_estudios e
    where e.id not in (select estudio_id
                         from hor_semestres_detalle_estudio de)
   union all
   select de.estudio_id * 10000 + c.id id, c.id convocatoria_id, c.nombre, de.fecha_examenes_inicio,
          de.fecha_examenes_fin, de.estudio_id
     from hor_v_Fechas_convocatorias c,
          hor_estudios e,
          hor_semestres_detalle d,
          hor_semestres_detalle_estudio de
    where e.id = de.estudio_id
      and d.id = de.detalle_id
      and c.id = 9
      and semestre_id = 1
   union all
   select de.estudio_id * 10000 + c.id id, c.id convocatoria_id, c.nombre, de.fecha_examenes_inicio,
          de.fecha_examenes_fin, de.estudio_id
     from hor_v_Fechas_convocatorias c,
          hor_estudios e,
          hor_semestres_detalle d,
          hor_semestres_detalle_estudio de
    where e.id = de.estudio_id
      and d.id = de.detalle_id
      and c.id = 10
      and semestre_id = 2
   union all
   select distinct de.estudio_id * 10000 + c.id id, c.id convocatoria_id, c.nombre, de.fecha_examenes_inicio_c2,
                   de.fecha_examenes_fin_c2, de.estudio_id
              from hor_v_Fechas_convocatorias c,
                   hor_estudios e,
                   hor_semestres_detalle d,
                   hor_semestres_detalle_estudio de
             where e.id = de.estudio_id
               and d.id = de.detalle_id
               and c.id = 11;

