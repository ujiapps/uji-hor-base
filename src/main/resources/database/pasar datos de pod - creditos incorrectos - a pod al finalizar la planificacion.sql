/* Formatted on 08/07/2014 09:17 (Formatter Plus v4.8.8) */
declare
   v_txt         varchar2 (2000);
   v_curso_aca   number          := 2014;
   v_acciones    varchar2 (1)    := 'S';

   cursor lista is

      select   asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo_id, creditos, idioma, coste, comentario,
               profesor_id, area_id, detalle_manual, estudio_id, cuantos_prof
          from (select   asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo_id,
                         max (round (nvl (crd_profesor, crd_item / cuantos_prof) * porcentaje / 100, 2)) creditos,
                         idioma, coste, comentario, profesor_id, area_id, max (detalle_manual) detalle_manual,
                         estudio_id, cuantos_prof
                    from (select asignatura_id, substr (grupo_id, 1, 1) grupo_id, subgrupo_id, tipo_subgrupo_id,
                                 ip.creditos crd_profesor, i.creditos crd_item,
                                 (select count (*)
                                    from uji_horarios.hor_items_profesores itp
                                   where item_id = i.id) cuantos_prof, 100 porcentaje, 0 idioma, 'S' coste,
                                 null comentario, profesor_id, area_id, ip.detalle_manual, estudio_id
                            from uji_horarios.hor_items i,
                                 uji_horarios.hor_items_asignaturas a,
                                 uji_horarios.hor_items_profesores ip,
                                 uji_horarios.hor_profesores p
                           where i.id = a.item_id
                             and i.id = ip.item_id
                             and comun = 0
                             and ip.profesor_id = p.id
                          --and asignatura_id = 'EI1001'
                          --and estudio_id = v_estudio
                          --and tipo_subgrupo_id = 'TE'
                          --and subgrupo_id = 1
                          --and 1 = 2
                          union
                          select asignatura_id, grupo_id, subgrupo_id, tipo_subgrupo_id, crd_profesor, crd_item,
                                 cuantos_prof, porcentaje, idioma, coste, comentario, profesor_id, area_id,
                                 detalle_manual, estudio_id
                            from (select distinct asignatura_id, substr (grupo_id, 1, 1) grupo_id, subgrupo_id,
                                                  tipo_subgrupo_id, ip.creditos crd_profesor, i.creditos crd_item,
                                                  (select count (*)
                                                     from uji_horarios.hor_items_profesores itp
                                                    where item_id = i.id) cuantos_prof, 0 idioma, 'S' coste,
                                                  null comentario, profesor_id, area_id,
                                                  (select porcentaje
                                                     from pod_grp_comunes c1,
                                                          pod_comunes c2
                                                    where c1.id = c2.gco_id
                                                      and curso_aca = :v_curso_aca
                                                      and asi_id = asignatura_id) porcentaje,
                                                  ip.detalle_manual, estudio_id
                                             from uji_horarios.hor_items i,
                                                  uji_horarios.hor_items_asignaturas a,
                                                  uji_horarios.hor_items_profesores ip,
                                                  uji_horarios.hor_profesores p
                                            where i.id = a.item_id
                                              and i.id = ip.item_id
                                              and comun = 1
                                              and ip.profesor_id = p.id
                                              --and asignatura_id = 'EM1045'
                                              --and estudio_id = v_estudio
                                              and asignatura_id not in (select asi_id
                                                                          from pod_asignaturas_area_comunes
                                                                         where curso_aca = :v_curso_aca)
                                                                                                       --and asignatura_id in ('AE1002', 'EC1002', 'FC1002')
                                                                                                                                                          --and tipo_subgrupo_id= 'PR'
                                                                                                                                                          --and subgrupo_id= 1
                                 )
                          --where 1 = 2
                          union all
                          select asignatura_id, grupo_id, subgrupo_id, tipo_subgrupo_id, crd_profesor, crd_item,
                                 cuantos_prof, porcentaje, idioma, coste, comentario, profesor_id, area_id,
                                 detalle_manual, estudio_id
                            from (select distinct asignatura_id, substr (grupo_id, 1, 1) grupo_id, subgrupo_id,
                                                  tipo_subgrupo_id, ip.creditos crd_profesor, i.creditos crd_item,
                                                  (select count (*)
                                                     from uji_horarios.hor_items_profesores itp
                                                    where item_id = i.id) cuantos_prof, 0 idioma, 'S' coste,
                                                  null comentario, profesor_id, area_id,
                                                  (select porcentaje
                                                     from pod_asignaturas_area_comunes
                                                    where curso_aca = :v_curso_aca
                                                      and asi_id = asignatura_id
                                                      and grp_id = grupo_id) porcentaje,
                                                  ip.detalle_manual, estudio_id
                                             from uji_horarios.hor_items i,
                                                  uji_horarios.hor_items_asignaturas a,
                                                  uji_horarios.hor_items_profesores ip,
                                                  uji_horarios.hor_profesores p
                                            where i.id = a.item_id
                                              and i.id = ip.item_id
                                              and comun = 1
                                              and ip.profesor_id = p.id
                                              --and asignatura_id = 'EM1045'
                                              --and estudio_id = v_estudio
                                              and asignatura_id in (select asi_id
                                                                      from pod_asignaturas_area_comunes
                                                                     where curso_aca = :v_curso_aca)))
                  -- where asignatura_id in ('EI1001')
                --and grupo_id = 'A'
                --and tipo_subgrupo_id = 'TE'
                --and subgrupo_id = 1
                group by asignatura_id,
                         grupo_id,
                         subgrupo_id,
                         tipo_subgrupo_id,
                         idioma,
                         coste,
                         comentario,
                         profesor_id,
                         area_id,
                         estudio_id,
                         cuantos_prof)
         where estudio_id between 201 and 9999
           and (asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo_id, creditos, profesor_id, area_id) not in (
                                  select sgr_grp_asi_id, sgr_grp_id, sgr_tipo, sgr_id, creditos, cdo_per_id,
                                         cdo_uest_id
                                    from pod_subgrupo_pdi
                                   where sgr_grp_curso_aca = 2014)
      order by 1,
               2,
               4,
               3;

   cursor lista_inicial is
      select 'ALTER TRIGGER gra_pod.POD_AREA_CERRADA_SGP DISABLE' accion
        from dual;

   cursor lista_final is
      select 'ALTER TRIGGER gra_pod.POD_AREA_CERRADA_SGP enable' accion
        from dual;

   procedure ejecutar (p_accion in varchar2) is
   begin
      execute immediate p_accion;
   end;

   procedure desactivar_triggers is
   begin
      for x in lista_inicial loop
         ejecutar (x.accion);
      end loop;
   end;

   procedure activar_triggers is
   begin
      for x in lista_final loop
         ejecutar (x.accion);
      end loop;
   end;
begin
   desactivar_triggers;
   dbms_output.put_line (' ');
   dbms_output.put_line ('---------------------------------------------------------------');
   dbms_output.put_line (v_curso_aca);
   dbms_output.put_line (' ');

   for x in lista loop
      begin
         if v_acciones = 'S' then
            begin
               dbms_output.put_line ('insertar ' || x.asignatura_id || ' - ' || x.grupo_id || ' - ' || v_curso_aca
                                     || ' - ' || x.subgrupo_id || ' - ' || x.tipo_subgrupo_id || ' - ' || x.creditos
                                     || ' - ' || x.idioma || ' - ' || x.coste || ' - ' || x.comentario || ' - '
                                     || x.profesor_id || ' - ' || v_curso_aca || ' - ' || x.area_id || ' - detalla '
                                     || x.detalle_manual);

               insert into pod_subgrupo_pdi
                           (SGR_GRP_ASI_ID, SGR_GRP_ID, SGR_GRP_CURSO_ACA, SGR_ID, SGR_TIPO, CREDITOS,
                            IDIOMA, COSTE, COMENTARIO, CDO_PER_ID, CDO_CURSO_ACA, CDO_UEST_ID
                           )
               values      (x.asignatura_id, x.grupo_id, v_curso_aca, x.subgrupo_id, x.tipo_subgrupo_id, x.creditos,
                            x.idioma, x.coste, x.comentario, x.profesor_id, v_curso_aca, x.area_id
                           );

               commit;
            exception
               when others then
                  dbms_output.put_line ('--- update  ' || x.asignatura_id || ' - ' || x.grupo_id || ' - '
                                        || v_curso_aca || ' - ' || x.subgrupo_id || ' - ' || x.tipo_subgrupo_id
                                        || ' - ' || x.creditos || ' - ' || x.idioma || ' - ' || x.coste || ' - '
                                        || x.comentario || ' - ' || x.profesor_id || ' - ' || v_curso_aca || ' - '
                                        || x.area_id || ' - detalla ' || x.detalle_manual);

                  update pod_subgrupo_pdi
                  set creditos = x.creditos
                  where  sgr_grp_asi_id = x.asignatura_id
                     and sgr_grp_id = x.grupo_id
                     and sgr_grp_curso_aca = v_curso_aca
                     and sgr_id = x.subgrupo_id
                     and sgr_tipo = x.tipo_subgrupo_id
                     and coste = x.coste
                     and cdo_per_id = x.profesor_id
                     and cdo_curso_aca = v_curso_aca
                     and cdo_uest_id = x.area_id;

                  commit;
            end;
         end if;
      exception
         when others then
            dbms_output.put_line ('ERROR  ----- ' || x.asignatura_id || ' ' || x.grupo_id || ' ' || v_curso_aca || ' '
                                  || x.subgrupo_id || ' ' || x.tipo_subgrupo_id || ' ' || x.creditos || ' ' || x.idioma
                                  || ' ' || x.coste || ' ' || x.comentario || ' ' || x.profesor_id || ' '
                                  || v_curso_aca || ' ' || x.area_id || ' ' || sqlerrm);
      end;
   end loop;

   activar_triggers;
end;

