CREATE OR REPLACE PROCEDURE UJI_HORARIOS.HORARIOS_SEMANA_PDF (

   name_array    IN   euji_util.ident_arr2,
   value_array   IN   euji_util.ident_arr2
) IS
   -- Parametros
   pEstudio    CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pEstudio');
   pSemestre   CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pSemestre');
   pSesion     CONSTANT VARCHAR2 (4000) := euji_util.euji_getparam (name_array, value_array, 'pSesion');
   pGrupo      CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pGrupo');
   pCurso      CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pCurso');
   pSemana     CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pSemana');
   pAgrupacion CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pAgrupacion');
   vCursoAcaNum         number;
   vSesion              number;
   vFechaIniSemestre    date;
   vFechaFinSemestre    date;
   vEsGenerica          boolean         := pSemana = 'G';

     -- Variables
   --  vPerId              per_personas.id%TYPE := euji_get_perid ();
   CURSOR c_items_gen (pSemestre in number) IS
      SELECT aulas.codigo as codigo_aula, asignatura_id, asignatura, estudio, asi.caracter, semestre_id, comun, grupo_id,
             tipo_subgrupo, tipo_subgrupo_id, subgrupo_id, dia_semana_id, hora_inicio, hora_fin, tipo_asignatura_id,
             tipo_asignatura,
             to_date (dia_semana_id + 1 || '/01/2006 ' || to_char (hora_fin, 'hh24:mi'), 'dd/mm/yyyy hh24:mi') fin,
             to_date (dia_semana_id + 1 || '/01/2006 ' || to_char (hora_inicio, 'hh24:mi'),
                      'dd/mm/yyyy hh24:mi') inicio
      FROM   UJI_HORARIOS.HOR_ITEMS items LEFT OUTER JOIN hor_aulas aulas ON (aulas.id = items.aula_planificacion_id)
             INNER JOIN uji_horarios.hor_items_asignaturas asi ON (items.id = asi.item_id)
      WHERE  asi.estudio_id = pEstudio
      AND    items.curso_id = pCurso
      AND    ',' || pGrupo || ',' like '%,' || items.grupo_id || ',%'
      AND    items.semestre_id = pSemestre
      AND    items.dia_semana_id IS NOT NULL;
   
   CURSOR c_items_gen_agrupacion (pSemestre in number) IS
      SELECT DISTINCT aulas.codigo as codigo_aula, asi.asignatura_id, asignatura, estudio, asi.caracter, semestre_id, comun, grupo_id,
             tipo_subgrupo, tipo_subgrupo_id, subgrupo_id, dia_semana_id, hora_inicio, hora_fin, tipo_asignatura_id,
             tipo_asignatura,
             to_date (dia_semana_id + 1 || '/01/2006 ' || to_char (hora_fin, 'hh24:mi'), 'dd/mm/yyyy hh24:mi') fin,
             to_date (dia_semana_id + 1 || '/01/2006 ' || to_char (hora_inicio, 'hh24:mi'),
                      'dd/mm/yyyy hh24:mi') inicio
      FROM   UJI_HORARIOS.HOR_ITEMS items LEFT OUTER JOIN hor_aulas aulas ON (aulas.id = items.aula_planificacion_id)
             INNER JOIN uji_horarios.hor_items_asignaturas asi ON (items.id = asi.item_id) JOIN uji_horarios.HOR_V_AGRUPACIONES_ITEMS agr_item on (items.id = AGR_ITEM.ITEM_ID)
      WHERE  asi.estudio_id = pEstudio
      AND    agr_item.agrupacion_id = pAgrupacion
      AND    ',' || pGrupo || ',' like '%,' || items.grupo_id || ',%'
      AND    items.semestre_id = pSemestre
      AND    items.dia_semana_id IS NOT NULL;

   CURSOR c_items_det (vIniDate date, vFinDate date) IS
      SELECT   aulas.codigo as codigo_aula, asignatura_id, asignatura, estudio, grupo_id, tipo_subgrupo,
               tipo_subgrupo_id, subgrupo_id, dia_semana_id, hora_inicio, hora_fin, inicio, fin
      FROM     UJI_HORARIOS.HOR_ITEMS items LEFT OUTER JOIN hor_aulas aulas ON (aulas.id = items.aula_planificacion_id)
               INNER JOIN uji_horarios.hor_items_asignaturas asi ON (items.id = asi.item_id)
               INNER JOIN UJI_HORARIOS.HOR_ITEMS_DETALLE det ON (det.item_id = items.id)
      WHERE    asi.estudio_id = pEstudio
      AND      items.curso_id = pCurso
      AND      ',' || pGrupo || ',' like '%,' || items.grupo_id || ',%'
      AND      items.semestre_id = pSemestre
      AND      items.dia_semana_id IS NOT NULL
      and      det.inicio > vIniDate
      and      det.fin < vFinDate
      order by det.inicio;
      
   CURSOR c_items_det_agrupacion (vIniDate date, vFinDate date) IS
      SELECT   DISTINCT aulas.codigo as codigo_aula, asi.asignatura_id, asignatura, estudio, grupo_id, tipo_subgrupo,
               tipo_subgrupo_id, subgrupo_id, dia_semana_id, hora_inicio, hora_fin, inicio, fin
      FROM     UJI_HORARIOS.HOR_ITEMS items LEFT OUTER JOIN hor_aulas aulas ON (aulas.id = items.aula_planificacion_id)
               INNER JOIN uji_horarios.hor_items_asignaturas asi ON (items.id = asi.item_id)
               INNER JOIN UJI_HORARIOS.HOR_ITEMS_DETALLE det ON (det.item_id = items.id)
               JOIN uji_horarios.HOR_V_AGRUPACIONES_ITEMS agr_item on (items.id = AGR_ITEM.ITEM_ID)
      WHERE    asi.estudio_id = pEstudio
      AND      agr_item.agrupacion_id = pAgrupacion
      AND      ',' || pGrupo || ',' like '%,' || items.grupo_id || ',%'
      AND      items.semestre_id = pSemestre
      AND      items.dia_semana_id IS NOT NULL
      and      det.inicio > vIniDate
      and      det.fin < vFinDate
      order by det.inicio;   

   CURSOR c_asigs (pSemestre in number) is
      SELECT distinct (asignatura_id), asignatura
      FROM            UJI_HORARIOS.HOR_ITEMS items,
                      uji_horarios.hor_items_asignaturas asi
      WHERE           items.id = asi.item_id
      and             asi.estudio_id = pEstudio
      AND             items.curso_id = pCurso
      AND             ',' || pGrupo || ',' like '%,' || items.grupo_id || ',%'
      AND             items.semestre_id = pSemestre
      AND             items.dia_semana_id IS NOT NULL
      order by        asignatura_id;
   
    CURSOR c_asigs_agrupacion (pSemestre in number) is
      SELECT distinct (asi.asignatura_id), asi.asignatura
      FROM            UJI_HORARIOS.HOR_ITEMS items,
                      uji_horarios.hor_items_asignaturas asi,
                      uji_horarios.HOR_V_AGRUPACIONES_ITEMS agr_item 
      WHERE           items.id = asi.item_id
      and             items.id = agr_item.item_id
      and             asi.estudio_id = pEstudio
      and             agr_item.agrupacion_id = pAgrupacion
      AND             ',' || pGrupo || ',' like '%,' || items.grupo_id || ',%'
      AND             items.semestre_id = pSemestre
      AND             items.dia_semana_id IS NOT NULL
      order by        asignatura_id;   

   CURSOR c_clases_asig (vAsignaturaId varchar2, vSemestre varchar2) is
      SELECT   tipo_subgrupo_id || subgrupo_id subgrupo,
               decode (dia_semana_id, 1, 'L', 2, 'M', 3, 'X', 4, 'J', 5, 'V', '?') dia_sem,
               to_char (hora_fin, 'hh24:mi') hora_fin_str, to_char (hora_inicio, 'hh24:mi') hora_inicio_str
      FROM     UJI_HORARIOS.HOR_ITEMS items,
               uji_horarios.hor_items_asignaturas asi
      WHERE    items.id = asi.item_id
      and      asi.estudio_id = pEstudio
      AND      items.curso_id = pCurso
      AND      ',' || pGrupo || ',' like '%,' || items.grupo_id || ',%'
      AND      items.semestre_id = vSemestre
      and      asi.asignatura_id = vAsignaturaId
      AND      items.dia_semana_id IS NOT NULL
      order by dia_semana_id,
               hora_inicio,
               subgrupo;
             
   CURSOR c_clases_asig_agrupacion (vAsignaturaId varchar2, vSemestre varchar2) is
      SELECT   tipo_subgrupo_id || subgrupo_id subgrupo,
               decode (dia_semana_id, 1, 'L', 2, 'M', 3, 'X', 4, 'J', 5, 'V', '?') dia_sem,
               to_char (hora_fin, 'hh24:mi') hora_fin_str, to_char (hora_inicio, 'hh24:mi') hora_inicio_str
      FROM     UJI_HORARIOS.HOR_ITEMS items,
               uji_horarios.hor_items_asignaturas asi
      WHERE    items.id = asi.item_id
      and      asi.estudio_id = pEstudio
      AND      ',' || pGrupo || ',' like '%,' || items.grupo_id || ',%'
      AND      items.semestre_id = vSemestre
      and      asi.asignatura_id = vAsignaturaId
      AND      items.dia_semana_id IS NOT NULL
      and items.id in (SELECT item_id from HOR_V_AGRUPACIONES_ITEMS agr_item where agr_item.agrupacion_id = pAgrupacion)
      order by dia_semana_id,
               hora_inicio,
               subgrupo;

   vFechas              t_horario;
   vRdo                 clob;

   procedure cabecera is
   begin
      owa_util.mime_header (ccontent_type => 'text/xml', bclose_header => true);
      fop.documentOpen (page_width => 29.7, page_height => 21, margin_left => 0.5, margin_right => 0.5,
                        margin_top => 0.5, margin_bottom => 0, extent_before => 0, extent_after => 0.5,
                        extent_margin_top => 0, extent_margin_bottom => 0.5);
      fop.pageBodyOpen;
      fop.documentfooteropen;
      fop.block ('Fecha documento: ' || to_char (sysdate, 'yyyy/mm/dd HH24:MI'), font_size => 6,
                 font_style => 'italic');
      fop.documentfooterclose;
      fop.pageFlowOpen;
   end;

   procedure calcula_curso_aca is
   begin
      SELECT   curso_academico_id
      into     vCursoAcaNum
      FROM     hor_semestres_detalle d,
               hor_estudios e
      WHERE    e.id = pEstudio
      AND      e.tipo_id = d.tipo_estudio_id
      AND      ROWNUM = 1
      ORDER BY fecha_inicio;
   end;

   procedure info_calendario_gen (pSemestre in number) is
      vCursoAca           varchar2 (4000);
      vTitulacionNombre   varchar2 (4000);
      vCursoNombre        varchar2 (4000) := '';
      vTextoCabecera      varchar2 (4000);
      vTextoGrupo         varchar2 (4000);
      vNombreAgrupacion   varchar2 (4000) := '';
   begin
      select nombre
      into   vTitulacionNombre
      from   hor_estudios
      where  id = pEstudio;
      
      if (pAgrupacion is not null) then
         select 'Agrupació ' || nombre 
         into vNombreAgrupacion
         from hor_agrupaciones
         where id = pAgrupacion;
      end if;

      vCursoAca := to_char (vCursoAcaNum) || '/' || to_char (vCursoAcaNum + 1);

      if pCurso in ('1', '3') then
         vCursoNombre := pCurso || 'er curs';
      elsif pCurso = '2' then
         vCursoNombre := '2on  curs';
      elsif pCurso = '4' then
         vCursoNombre := '4t curs';
      elsif pCurso is not null then
         vCursoNombre := pCurso || '?  curs';
      end if;

      if pGrupo like '%,%' then
         vTextoGrupo := 'Grups';
      else
         vTextoGrupo := 'Grup';
      end if;

      vTextoCabecera :=
         vTitulacionNombre || ' - ' || vCursoNombre || vNombreAgrupacion || ' - ' || vTextoGrupo || ' ' || pGrupo || ' (semestre '
         || pSemestre || ') - ' || vCursoAca;
      fop.block (text => vTextoCabecera, border_bottom => 'solid', border_width => 0.05);
   end;

   procedure muestra_leyenda_asig (item c_asigs%ROWTYPE, vSemestre varchar2) is
      vClasesText   varchar2 (4000);
   begin
      fop.block (item.asignatura_id || ', ' || item.asignatura || ':', font_size => 7, padding_top => 0.1);
      vClasesText := '<fo:inline color="white">___</fo:inline>';

      for c in c_clases_asig (item.asignatura_id, vSemestre) loop
         vClasesText :=
            vClasesText || c.dia_sem || ', ' || c.hora_inicio_str || '-' || c.hora_fin_str || ' (' || c.subgrupo
            || '); ';
      end loop;

      fop.block (vClasesText, font_size => 7, padding_top => 0.06);
   end;

   procedure leyenda_asigs (pSemestre in number) is
   begin
      fop.block (' ', padding_top => 1);

      for item in c_asigs (pSemestre) loop
         muestra_leyenda_asig (item, pSemestre);
      end loop;
   end;
   
   procedure muestra_leyenda_asig_agr (item c_asigs%ROWTYPE, vSemestre varchar2) is
      vClasesText   varchar2 (4000);
   begin
      fop.block (item.asignatura_id || ', ' || item.asignatura || ':', font_size => 7, padding_top => 0.1);
      vClasesText := '<fo:inline color="white">___</fo:inline>';

      for c in c_clases_asig_agrupacion (item.asignatura_id, vSemestre) loop
         vClasesText :=
           vClasesText || c.dia_sem || ', ' || c.hora_inicio_str || '-' || c.hora_fin_str || ' (' || c.subgrupo
             || '); ';
      end loop;

      fop.block (vClasesText, font_size => 7, padding_top => 0.06);
   end;

   procedure leyenda_asigs_agrupacion (pSemestre in number) is
   begin
      fop.block (' ', padding_top => 1);

      for item in c_asigs_agrupacion (pSemestre) loop
         muestra_leyenda_asig_agr (item, pSemestre);
      end loop;
   end;

   procedure pie is
   begin
      fop.pageFlowClose;
      fop.pageBodyClose;
      fop.documentClose;
   end;

   function redondea_fecha_a_cuartos (vIniDate date)
      return date is
      fecha_redondeada   date;
   begin
      select trunc(vIniDate,'HH')+(15*round(to_char( trunc(vIniDate,'MI'),'MI')/15))/1440
        into fecha_redondeada
        from dual;
      
      return fecha_redondeada;
   end;
   
   procedure calendario_gen (pSemestre in number) is
   begin
      vFechas := t_horario ();

      for item in c_items_gen (pSemestre) loop
         vFechas.extend;
         vFechas (vFechas.count) :=
            ItemHorarios (hor_contenido_item (redondea_fecha_a_cuartos(item.hora_inicio), redondea_fecha_a_cuartos(item.hora_fin), item.asignatura_id,
                                              '(' || item.tipo_subgrupo_id || item.subgrupo_id || ') '
                                              || item.codigo_aula),
                          redondea_fecha_a_cuartos(item.inicio), redondea_fecha_a_cuartos(item.fin), null);
      end loop;

      if vFechas.count > 0 then
         vRdo := xpfpkg_horarios_academicos.drawTimetable (vFechas, 5, null, 'CA', 'PDF_APP_HORARIOS');
         xup_utils.dump (vRdo);
         dbms_lob.freeTemporary (vRdo);
      end if;
   end;

   procedure calendario_gen_agrupacion (pSemestre in number) is
   begin
      vFechas := t_horario ();

      for item in c_items_gen_agrupacion (pSemestre) loop
         vFechas.extend;
         vFechas (vFechas.count) :=
            ItemHorarios (hor_contenido_item (redondea_fecha_a_cuartos(item.hora_inicio), redondea_fecha_a_cuartos(item.hora_fin), item.asignatura_id,
                                              '(' || item.tipo_subgrupo_id || item.subgrupo_id || ') '
                                              || item.codigo_aula),
                          redondea_fecha_a_cuartos(item.inicio), redondea_fecha_a_cuartos(item.fin), null);
      end loop;

      if vFechas.count > 0 then
         vRdo := xpfpkg_horarios_academicos.drawTimetable (vFechas, 5, null, 'CA', 'PDF_APP_HORARIOS');
         xup_utils.dump (vRdo);
         dbms_lob.freeTemporary (vRdo);
      end if;
   end;

   procedure calendario_det (vFechaIni date, vFechaFin date) is
   begin
      vFechas := t_horario ();

      for item in c_items_det (vFechaIni, vFechaFin) loop
         vFechas.extend;
         vFechas (vFechas.count) :=
            ItemHorarios (hor_contenido_item (redondea_fecha_a_cuartos(item.hora_inicio), redondea_fecha_a_cuartos(item.hora_fin), item.asignatura_id,
                                              '(' || item.tipo_subgrupo_id || item.subgrupo_id || ') '
                                              || item.codigo_aula),
                          redondea_fecha_a_cuartos(item.inicio), redondea_fecha_a_cuartos(item.fin), null);
      end loop;

      if vFechas.count > 0 then
         vRdo := xpfpkg_horarios_academicos.drawTimetable (vFechas, 5, null, 'CA', 'PDF_APP_HORARIOS_DET');
         xup_utils.dump (vRdo);
         dbms_lob.freeTemporary (vRdo);
      end if;
   end;
   
   procedure calendario_det_agrupacion (vFechaIni date, vFechaFin date) is
   begin
      vFechas := t_horario ();

      for item in c_items_det_agrupacion (vFechaIni, vFechaFin) loop
         vFechas.extend;
         vFechas (vFechas.count) :=
            ItemHorarios (hor_contenido_item (redondea_fecha_a_cuartos(item.hora_inicio), redondea_fecha_a_cuartos(item.hora_fin), item.asignatura_id,
                                              '(' || item.tipo_subgrupo_id || item.subgrupo_id || ') '
                                              || item.codigo_aula),
                          redondea_fecha_a_cuartos(item.inicio), redondea_fecha_a_cuartos(item.fin), null);
      end loop;

      if vFechas.count > 0 then
         vRdo := xpfpkg_horarios_academicos.drawTimetable (vFechas, 5, null, 'CA', 'PDF_APP_HORARIOS_DET');
         xup_utils.dump (vRdo);
         dbms_lob.freeTemporary (vRdo);
      end if;
   end;

   procedure calcula_fechas_semestre (pSemestre in number) is
   begin
      SELECT fecha_inicio, fecha_examenes_fin
      into   vFechaIniSemestre, vFechaFinSemestre
      FROM   hor_semestres_detalle,
             hor_estudios e
      WHERE  semestre_id = pSemestre
      and    e.id = pEstudio
      and    e.tipo_id = tipo_estudio_id;
      
      SELECT Next_Day(Trunc(vFechaIniSemestre) - 7,'LUN') into vFechaIniSemestre FROM dual;
      
   end;

   function semana_tiene_clase (vIniDate date, vFinDate date)
      return boolean is
      num_clases   number;
   begin
      SELECT   count (*)
      into     num_clases
      FROM     UJI_HORARIOS.HOR_ITEMS items,
               UJI_HORARIOS.HOR_ITEMS_DETALLE det,
               uji_horarios.hor_items_asignaturas asi
      WHERE    items.id = asi.item_id
      and      asi.estudio_id = pEstudio
      AND      items.curso_id = pCurso
      AND      ',' || pGrupo || ',' like '%,' || items.grupo_id || ',%'
      AND      items.semestre_id = pSemestre
      AND      items.dia_semana_id IS NOT NULL
      and      det.item_id = items.id
      and      det.inicio > vIniDate
      and      det.fin < vFinDate
      order by det.inicio;

      return num_clases > 0;
   end;
   
      function semana_tiene_clase_agr (vIniDate date, vFinDate date)
      return boolean is
      num_clases   number;
   begin
      SELECT   count (*)
      into     num_clases
      FROM     UJI_HORARIOS.HOR_ITEMS items,
               UJI_HORARIOS.HOR_ITEMS_DETALLE det,
               uji_horarios.hor_items_asignaturas asi,
               HOR_V_AGRUPACIONES_ITEMS agr_items
      WHERE    items.id = asi.item_id
      and      asi.estudio_id = pEstudio
      AND      ',' || pGrupo || ',' like '%,' || items.grupo_id || ',%'
      AND      items.semestre_id = pSemestre
      AND      items.dia_semana_id IS NOT NULL
      and      det.item_id = items.id
      and      items.id = agr_items.item_id
      and      agr_items.agrupacion_id = pAgrupacion
      and      det.inicio > vIniDate
      and      det.fin < vFinDate
      order by det.inicio;

      return num_clases > 0;
   end;

   procedure info_calendario_det (vFechaIni date, vFechaFin date) is
      vCursoAca           varchar2 (4000);
      vTitulacionNombre   varchar2 (4000);
      vCursoNombre        varchar2 (4000) := '';
      vTextoCabecera      varchar2 (4000);
      vFechaIniStr        varchar2 (4000) := to_char (vFechaIni, 'dd/mm/yyyy');
      vFechaFinStr        varchar2 (4000) := to_char (vFechaFin - 1, 'dd/mm/yyyy');
      vNombreAgrupacion   varchar2 (4000) := '';
   begin
      select nombre
      into   vTitulacionNombre
      from   hor_estudios
      where  id = pEstudio;
      
      if (pAgrupacion is not null) then
         select 'Agrupació ' || nombre 
         into vNombreAgrupacion
         from hor_agrupaciones
         where id = pAgrupacion;
      end if;
      
      vCursoAca := to_char (vCursoAcaNum) || '/' || to_char (vCursoAcaNum + 1);

      if pCurso in ('1', '3') then
         vCursoNombre := pCurso || 'er curs';
      elsif pCurso = '2' then
         vCursoNombre := '2on  curs';
      elsif pCurso = '4' then
         vCursoNombre := '4t curs';
      elsif pCurso is not null then
         vCursoNombre := pCurso || '?  curs';
      end if;

      vTextoCabecera :=
         vTitulacionNombre || ' - ' || vCursoNombre || vNombreAgrupacion || ' - Grup ' || pGrupo || ' (semestre ' || pSemestre
         || ') - Del ' || vFechaIniStr || ' al ' || vFechaFinStr;
      fop.block (text => vTextoCabecera, border_bottom => 'solid', border_width => 0.05);
   end;

   procedure paginas_calendario_det (pSemestre in number) is
      vFechaIniSemana   date;
      vFechaFinSemana   date;
   begin
      calcula_fechas_semestre (pSemestre);
      vFechaIniSemana := vFechaIniSemestre;
      vFechaFinSemana := vFechaIniSemana + 7;

      while vFechaFinSemana <= vFechaFinSemestre loop
         if semana_tiene_clase (vFechaIniSemana, vFechaFinSemana) then
            info_calendario_det (vFechaIniSemana, vFechaFinSemana);
            calendario_det (vFechaIniSemana, vFechaFinSemana);
            htp.p ('<fo:block break-before="page" color="white">_</fo:block>');
         end if;

         vFechaIniSemana := vFechaFinSemana;
         vFechaFinSemana := vFechaFinSemana + 7;
      end loop;
   end;
   
   procedure paginas_calendario_det_agr (pSemestre in number) is
      vFechaIniSemana   date;
      vFechaFinSemana   date;
   begin
      calcula_fechas_semestre (pSemestre);
      vFechaIniSemana := vFechaIniSemestre;
      vFechaFinSemana := vFechaIniSemana + 7;

      while vFechaFinSemana <= vFechaFinSemestre loop
         if semana_tiene_clase_agr (vFechaIniSemana, vFechaFinSemana) then
            info_calendario_det (vFechaIniSemana, vFechaFinSemana);
            calendario_det_agrupacion (vFechaIniSemana, vFechaFinSemana);
            htp.p ('<fo:block break-before="page" color="white">_</fo:block>');
         end if;
         vFechaIniSemana := vFechaFinSemana;
         vFechaFinSemana := vFechaFinSemana + 7;
      end loop;
   end;
   
   
BEGIN
   --euji_control_acceso (vItem);
   select count (*)
   into   vSesion
   from   apa_sesiones
   where  sesion_id = pSesion
   and    fecha < sysdate () - 0.33;

   if vSesion > 0 then
      calcula_curso_aca;
      cabecera;

      if vEsGenerica then
         info_calendario_gen (1);
         
         if (pCurso is not null) then
            calendario_gen (1);
            leyenda_asigs (1);
         else
            calendario_gen_agrupacion(1);
            leyenda_asigs_agrupacion (1);
         end if;
         
         
         htp.p ('<fo:block break-before="page" color="white">_</fo:block>');
         info_calendario_gen (2);
         
         if (pCurso is not null) then
            calendario_gen (2);
            leyenda_asigs (2);
         else
            calendario_gen_agrupacion(2);
            leyenda_asigs_agrupacion (2);
         end if;
      else
         if (pCurso is not null) then
            paginas_calendario_det (pSemestre); 
         else
            paginas_calendario_det_agr(pSemestre); 
         end if;
          
         leyenda_asigs (pSemestre);
      end if;

      pie;
   else
      cabecera;
      fop.block ('No tens perm?s per accedir a aquest document.', font_weight => 'bold', text_align => 'center',
                 font_size => 14, space_after => 0.5);
      pie;
   end if;
EXCEPTION
   WHEN OTHERS THEN
      htp.p (sqlerrm);
END;



CREATE OR REPLACE FORCE VIEW UJI_HORARIOS.HOR_V_FECHAS_CONVOCATORIAS_EST (ID,
                                                                          CONVOCATORIA_ID,
                                                                          NOMBRE,
                                                                          FECHA_EXAMENES_INICIO,
                                                                          FECHA_EXAMENES_FIN,
                                                                          ESTUDIO_ID
                                                                         ) AS
   select e.id * 10000 + c.id id, c.id convocatoria_id, c.nombre, c.fecha_examenes_inicio, c.fecha_examenes_fin,
          e.id estudio_id
     from hor_v_Fechas_convocatorias c,
          hor_estudios e
    where e.id not in (select estudio_id
                         from hor_semestres_detalle_estudio de)
   union all
   select de.estudio_id * 10000 + c.id id, c.id convocatoria_id, c.nombre, de.fecha_examenes_inicio,
          de.fecha_examenes_fin, de.estudio_id
     from hor_v_Fechas_convocatorias c,
          hor_estudios e,
          hor_semestres_detalle d,
          hor_semestres_detalle_estudio de
    where e.id = de.estudio_id
      and d.id = de.detalle_id
      and c.id = 9
      and semestre_id = 1
   union all
   select de.estudio_id * 10000 + c.id id, c.id convocatoria_id, c.nombre, de.fecha_examenes_inicio,
          de.fecha_examenes_fin, de.estudio_id
     from hor_v_Fechas_convocatorias c,
          hor_estudios e,
          hor_semestres_detalle d,
          hor_semestres_detalle_estudio de
    where e.id = de.estudio_id
      and d.id = de.detalle_id
      and c.id = 10
      and semestre_id = 2
   union all
   select distinct de.estudio_id * 10000 + c.id id, c.id convocatoria_id, c.nombre, de.fecha_examenes_inicio_c2,
                   de.fecha_examenes_fin_c2, de.estudio_id
              from hor_v_Fechas_convocatorias c,
                   hor_estudios e,
                   hor_semestres_detalle d,
                   hor_semestres_detalle_estudio de
             where e.id = de.estudio_id
               and d.id = de.detalle_id
               and c.id = 11;

