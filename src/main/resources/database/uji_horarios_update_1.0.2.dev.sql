CREATE OR REPLACE VIEW uji_horarios.hor_v_cursos  AS
SELECT DISTINCT hor_items_asignaturas.estudio_id,
  hor_items_asignaturas.estudio,
  hor_items.curso_id
FROM hor_items_asignaturas,
  hor_items
WHERE hor_items.id      = hor_items_asignaturas.item_id
AND (hor_items.curso_id < 7) ;




CREATE OR REPLACE VIEW uji_horarios.hor_v_aulas_personas  AS
SELECT DISTINCT c.persona_id,
  ap.aula_id,
  a.nombre,
  a.centro_id,
  cen.nombre centro,
  a.codigo,
  a.tipo
FROM hor_ext_cargos_per c,
  hor_aulas_planificacion ap,
  hor_aulas a,
  hor_centros cen
WHERE c.estudio_id = ap.estudio_id
AND ap.aula_id     = a.id
AND a.centro_id    = cen.id ;

