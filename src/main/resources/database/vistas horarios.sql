/* Formatted on 31/10/2012 14:15 (Formatter Plus v4.8.8) */
with base as
     (select fecha,
             row_number () over (partition by estudio_id, semestre_id, curso_id, asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo_id, dia_semana_id, decode
                                                                                                                                                          (hor_f_fecha_entre
                                                                                                                                                              (x.fecha,
                                                                                                                                                               fecha_inicio,
                                                                                                                                                               fecha_fin),
                                                                                                                                                           'S', decode
                                                                                                                                                              (hor_f_fecha_entre
                                                                                                                                                                  (x.fecha,
                                                                                                                                                                   nvl
                                                                                                                                                                      (desde_el_dia,
                                                                                                                                                                       fecha_inicio),
                                                                                                                                                                   nvl
                                                                                                                                                                      (hasta_el_dia,
                                                                                                                                                                       fecha_fin)),
                                                                                                                                                               'S', 'S',
                                                                                                                                                               'N'
                                                                                                                                                              ),
                                                                                                                                                           'N'
                                                                                                                                                          ) order by fecha)
                                                                                                               orden_id,
             decode (hor_f_fecha_entre (x.fecha, fecha_inicio, fecha_fin),
                     'S', decode (hor_f_fecha_entre (x.fecha, nvl (desde_el_dia, fecha_inicio),
                                                     nvl (hasta_el_dia, fecha_fin)),
                                  'S', 'S',
                                  'N'
                                 ),
                     'N'
                    ) docencia,
             estudio_id, curso_id, semestre_id, grupo_id, tipo_subgrupo_id, subgrupo_id, dia_Semana_id, asignatura_id,
             fecha_inicio, fecha_fin, fecha_examenes_inicio, fecha_examenes_fin, desde_el_dia, hasta_el_dia,
             repetir_cada_semanas, numero_iteraciones, detalle_manual, tipo_dia, dia_semana, incremento
      from   (select i.estudio_id, i.curso_id, i.semestre_id, i.grupo_id, i.tipo_subgrupo_id, i.subgrupo_id,
                     i.dia_Semana_id, i.asignatura_id, fecha_inicio, fecha_fin, fecha_examenes_inicio,
                     fecha_examenes_fin, i.desde_el_dia, hasta_el_dia, repetir_cada_semanas, numero_iteraciones,
                     detalle_manual, c.fecha, tipo_dia, dia_semana,
                     decode (repetir_cada_semanas, null, 0, 1, 0, 2, 1 / 2, 3, 2 / 3, 4, 3 / 4) incremento
              from   hor_estudios e,
                     hor_semestres_detalle s,
                     hor_items i,
                     hor_ext_calendario c
              where  e.tipo_id = s.tipo_estudio_id
              and    i.estudio_id = e.id
              and    i.semestre_id = s.semestre_id
              and    c.fecha between fecha_inicio and nvl (fecha_examenes_fin, fecha_fin)
              and    c.dia_semana_id = i.dia_semana_id
              and    tipo_dia = 'L'
              and    detalle_manual = 0) x)
select   fecha,                                                                                              --orden_id,
         decode (mod (orden_id, nvl (repetir_cada_semanas, 1)),
                 1, decode (sign ((orden_id / nvl (repetir_cada_semanas, 1)
                                   - decode (repetir_cada_semanas, null, 0, 1, 0, 2, 1 / 2, 3, 2 / 3, 4, 3 / 4)
                                  )
                                  - nvl (numero_iteraciones, 1)),
                            -1, docencia,
                            'N'
                           ),
                 'N'
                ) docencia
--fecha_inicio, fecha_fin, fecha_examenes_inicio, fecha_examenes_fin, desde_el_dia, hasta_el_dia,
--repetir_cada_semanas, numero_iteraciones, detalle_manual, tipo_dia, dia_semana, estudio_id, curso_id,
--semestre_id, grupo_id, tipo_subgrupo_id, subgrupo_id, dia_Semana_id, asignatura_id
from     base
where    estudio_id = 210
and      semestre_id = 1
and      curso_id = 1
and      asignatura_id = 'AE1003'
and      grupo_id = 'A'
and      tipo_subgrupo_id = 'LA'
and      subgrupo_id = 4
and      dia_semana_id = 3
order by 1




--------------------


/* Formatted on 31/10/2012 14:23 (Formatter Plus v4.8.8) */




with base as
     (select fecha, decode (item_id, null, 'N', decode (trunc (fecha), trunc (inicio), 'S', 'N')) docencia, estudio_id,
             curso_id, semestre_id, grupo_id, tipo_subgrupo_id, subgrupo_id, dia_Semana_id, asignatura_id, fecha_inicio,
             fecha_fin, fecha_examenes_inicio, fecha_examenes_fin, desde_el_dia, hasta_el_dia, repetir_cada_semanas,
             numero_iteraciones, detalle_manual, tipo_dia, dia_semana, id
      from   (select i.estudio_id, i.curso_id, i.semestre_id, i.grupo_id, i.tipo_subgrupo_id, i.subgrupo_id,
                     i.dia_Semana_id, i.asignatura_id, fecha_inicio, fecha_fin, fecha_examenes_inicio,
                     fecha_examenes_fin, i.desde_el_dia, hasta_el_dia, repetir_cada_semanas, numero_iteraciones,
                     detalle_manual, c.fecha, tipo_dia, dia_semana, d.item_id, i.id, d.inicio
              from   hor_estudios e,
                     hor_semestres_detalle s,
                     hor_items i,
                     hor_ext_calendario c,
                     hor_items_detalle d
              where  e.tipo_id = s.tipo_estudio_id
              and    i.estudio_id = e.id
              and    i.semestre_id = s.semestre_id
              and    c.fecha between fecha_inicio and nvl (fecha_examenes_fin, fecha_fin)
              and    c.dia_semana_id(+) = i.dia_semana_id
              and    tipo_dia = 'L'
              and    detalle_manual = 1
              and    i.id = d.item_id(+)) x)
select   fecha,                                                                                              --orden_id,
               docencia, id
--fecha_inicio, fecha_fin, fecha_examenes_inicio, fecha_examenes_fin, desde_el_dia, hasta_el_dia,
--repetir_cada_semanas, numero_iteraciones, detalle_manual, tipo_dia, dia_semana, estudio_id, curso_id,
--semestre_id, grupo_id, tipo_subgrupo_id, subgrupo_id, dia_Semana_id, asignatura_id
from     base
where    estudio_id = 210
and      semestre_id = 1
and      curso_id = 1
and      asignatura_id = 'AE1003'
and      grupo_id = 'A'
and      tipo_subgrupo_id = 'LA'
and      subgrupo_id = 4
and      dia_semana_id = 3
order by 1


