CREATE TABLE uji_horarios.HOR_IDIOMAS
  (
    ID     NUMBER NOT NULL ,
    NOMBRE VARCHAR2 (100 BYTE) NOT NULL ,
    ID_POP VARCHAR2 (10 BYTE) NOT NULL
  ) ;
CREATE UNIQUE INDEX uji_horarios.HOR_IDIOMAS_PK ON uji_horarios.HOR_IDIOMAS
  (
    ID ASC
  )
  ;
  ALTER TABLE uji_horarios.HOR_IDIOMAS ADD CONSTRAINT HOR_IDIOMAS_PK PRIMARY KEY ( ID ) ;
  CREATE TABLE uji_horarios.HOR_ITEMS_PROF_IDIOMA
    (
      ID               NUMBER NOT NULL ,
      ITEM_PROFESOR_ID NUMBER NOT NULL ,
      IDIOMA_ID        NUMBER NOT NULL
    ) ;
  CREATE INDEX uji_horarios.HOR_ITEM_DET_IDI_IDX ON uji_horarios.HOR_ITEMS_PROF_IDIOMA
    (
      IDIOMA_ID ASC
    ) ;
  CREATE INDEX uji_horarios.HOR_ITEM_DET_PROF_IDX ON uji_horarios.HOR_ITEMS_PROF_IDIOMA
    (
      ITEM_PROFESOR_ID ASC
    ) ;
CREATE UNIQUE INDEX uji_horarios.HOR_ITEM_PROD_IDI_UK ON uji_horarios.HOR_ITEMS_PROF_IDIOMA
  (
    ITEM_PROFESOR_ID ASC , IDIOMA_ID ASC
  )
  ;
CREATE UNIQUE INDEX uji_horarios.HOR_ITEMS_PROF_IDIIOMA_PK ON uji_horarios.HOR_ITEMS_PROF_IDIOMA
  (
    ID ASC
  )
  ;
  ALTER TABLE uji_horarios.HOR_ITEMS_PROF_IDIOMA ADD CONSTRAINT HOR_ITEMS_PROF_IDIIOMA_PK PRIMARY KEY ( ID ) ;
  ALTER TABLE uji_horarios.HOR_ITEMS_PROF_IDIOMA ADD CONSTRAINT HOR_ITEM_PROD_IDI_UK UNIQUE ( ITEM_PROFESOR_ID , IDIOMA_ID ) ;
  ALTER TABLE uji_horarios.HOR_ITEMS_PROF_IDIOMA ADD CONSTRAINT HOR_ITEMP_PROF_IDI_FK FOREIGN KEY ( IDIOMA_ID ) REFERENCES uji_horarios.HOR_IDIOMAS ( ID ) ;
  ALTER TABLE uji_horarios.HOR_ITEMS_PROF_IDIOMA ADD CONSTRAINT HOR_ITEM_PROF_ITP_FK FOREIGN KEY ( ITEM_PROFESOR_ID ) REFERENCES uji_horarios.hor_items_profesores ( id ) ;



CREATE OR REPLACE FORCE VIEW UJI_HORARIOS.HOR_V_ITEMS_PROF_DETALLE
(
   ID,
   DETALLE_ID,
   DET_PROFESOR_ID,
   ITEM_ID,
   INICIO,
   FIN,
   DESCRIPCION,
   PROFESOR_ID,
   DETALLE_MANUAL,
   CREDITOS,
   CREDITOS_DETALLE,
   CREDITOS_PACTADOS,
   SELECCIONADO,
   NOMBRE,
   PROFESOR_COMPLETO
) AS
   select to_number(id || det_profesor_id || item_id || to_char(inicio, 'yyyymmddhh24miss')) id,
          id                                                                                 detalle_id,
          det_profesor_id,
          item_id,
          inicio,
          fin,
          descripcion,
          profesor_id,
          detalle_manual,
          creditos,
          creditos_detalle,
          creditos_pactados,
          decode(detalle_manual, 0, 'S', decode(min(orden), 1, 'S', 'N'))                    seleccionado,
          nombre,
          profesor_completo
   from (select d.*,
                profesor_id,
                p.detalle_manual,
                i.creditos,
                p.creditos   creditos_detalle,
                p.creditos_pactados,
                p.id         det_profesor_id,
                99           orden,
                nombre,
                (select wm_concat(nombre)
                 from hor_items_profesores pr, hor_profesores p
                 where     item_id = i.id
                       and pr.profesor_id = p.id)
                   profesor_completo
         from hor_items i, hor_items_detalle d, hor_items_profesores p, hor_profesores pro
         where     i.id = d.item_id
               and i.id = p.item_id
               and p.profesor_id = pro.id
               and not exists
                      (select 1
                       from hor_items_det_profesores
                       where detalle_id = d.id)
         union all
         select i.id,
                i.id                            item_id,
                null                            inicio,
                null                            fin,
                null                            descripcion,
                profesor_id,
                p.detalle_manual,
                i.creditos,
                round(p.creditos, 2)            creditos_detalle,
                round(p.creditos_pactados, 2)   creditos_pactados,
                p.id                            det_profesor_id,
                99                              orden,
                nombre,
                (select wm_concat(nombre)
                 from hor_items_profesores pr, hor_profesores p
                 where     item_id = i.id
                       and pr.profesor_id = p.id)
                   profesor_completo
         from hor_items i, hor_items_profesores p, hor_profesores pro
         where     i.id = p.item_id
               and p.profesor_id = pro.id
               and not exists
                      (select 1
                       from hor_items_detalle
                       where item_id = i.id)
         union all
         select d.*,
                profesor_id,
                p.detalle_manual,
                i.creditos,
                p.creditos   creditos_detalle,
                p.creditos   creditos_pactados,
                p.id         det_profesor_id,
                1            orden,
                nombre,
                (select wm_concat(nombre)
                 from hor_items_profesores pr, hor_profesores p
                 where     item_id = i.id
                       and pr.profesor_id = p.id)
                   profesor_completo
         from hor_items                    i,
              hor_items_detalle            d,
              hor_items_profesores         p,
              hor_items_det_profesores     dp,
              hor_profesores               pro
         where     i.id = d.item_id
               and i.id = p.item_id
               and p.detalle_manual = 1
               and dp.detalle_id = d.id
               and item_profesor_id = p.id
               and p.profesor_id = pro.id)
   group by id,
            item_id,
            inicio,
            fin,
            descripcion,
            profesor_id,
            detalle_manual,
            creditos,
            creditos_detalle,
            creditos_pactados,
            det_profesor_id,
            nombre,
            profesor_completo;


ALTER TABLE UJI_HORARIOS.HOR_ITEMS_DET_PROFESORES ADD
CONSTRAINT hor_item_det_prof_uk
 UNIQUE (DETALLE_ID)
 ENABLE
 VALIDATE;


CREATE OR REPLACE FORCE VIEW UJI_HORARIOS.HOR_V_CONTROL_EXCESO_CREDITOS
(
   ASIGNATURA_ID,
   ASIGNATURA,
   ESTUDIO_ID,
   ESTUDIO,
   AREA_ID,
   ASIGNATURA_TXT,
   GRUPO_ID,
   TIPO,
   CRD_ITEM,
   TOTAL
)
   select asignatura_id,
          asignatura,
          estudio_id,
          estudio,
          area_id,
          asignatura_txt,
          grupo_id,
          tipo,
          crd_item,
          sum(crd_final) total
   from (select distinct id,
                         nombre,
                         crd_prof,
                         crd_pactados,
                         crd_item,
                         crd_final,
                         grupo_id,
                         tipo,
                         asignatura_id,
                         asignatura_txt,
                         departamento_id,
                         area_id
         from hor_v_items_creditos_Detalle) x,
        hor_v_asignaturas   a
   where asignatura_id = a.id
   group by asignatura_id,
            area_id,
            asignatura_txt,
            grupo_id,
            tipo,
            crd_item,
            asignatura,
            estudio,
            estudio_id
   having sum(crd_final) > crd_item;

ALTER TABLE UJI_HORARIOS.HOR_ITEMS
 ADD (nombre_item  VARCHAR2(1000))

 
 
CREATE OR REPLACE FORCE VIEW UJI_HORARIOS.HOR_V_ITEMS_PROF_DETALLE
(
   ID,
   DETALLE_ID,
   DET_PROFESOR_ID,
   ITEM_ID,
   INICIO,
   FIN,
   DESCRIPCION,
   PROFESOR_ID,
   DETALLE_MANUAL,
   CREDITOS,
   CREDITOS_DETALLE,
   CREDITOS_PACTADOS,
   SELECCIONADO,
   NOMBRE,
   PROFESOR_COMPLETO,
   AREA_ID
)
   BEQUEATH DEFINER AS
   select to_number(id || det_profesor_id || item_id || to_char(inicio, 'yyyymmddhh24miss')) id,
          id                                                                                 detalle_id,
          det_profesor_id,
          item_id,
          inicio,
          fin,
          descripcion,
          profesor_id,
          detalle_manual,
          creditos,
          creditos_detalle,
          creditos_pactados,
          decode(detalle_manual, 0, 'S', decode(min(orden),  -1, 'X',  1, 'S',  'N'))        seleccionado,
          nombre,
          profesor_completo,
          area_id
   from (                                                                          /* items no detallados manualmente */
         select d.*,
                profesor_id,
                p.detalle_manual,
                i.creditos,
                p.creditos   creditos_detalle,
                p.creditos_pactados,
                p.id         det_profesor_id,
                99           orden,
                nombre,
                (select listagg(nombre) within group (order by nombre)
                 from hor_items_profesores pr, hor_profesores p
                 where     item_id = i.id
                       and pr.profesor_id = p.id)
                   profesor_completo,
                area_id
         from hor_items i, hor_items_detalle d, hor_items_profesores p, hor_profesores pro
         where     i.id = d.item_id
               and i.id = p.item_id
               and p.profesor_id = pro.id
               and not exists
                      (select 1
                       from hor_items_det_profesores
                       where detalle_id = d.id)
         union all
         /* ????? */
         select i.id,
                i.id                            item_id,
                null                            inicio,
                null                            fin,
                null                            descripcion,
                profesor_id,
                p.detalle_manual,
                i.creditos,
                round(p.creditos, 2)            creditos_detalle,
                round(p.creditos_pactados, 2)   creditos_pactados,
                p.id                            det_profesor_id,
                99                              orden,
                nombre,
                (select listagg(nombre) within group (order by nombre)
                 from hor_items_profesores pr, hor_profesores p
                 where     item_id = i.id
                       and pr.profesor_id = p.id)
                   profesor_completo,
                area_id
         from hor_items i, hor_items_profesores p, hor_profesores pro
         where     i.id = p.item_id
               and p.profesor_id = pro.id
               and not exists
                      (select 1
                       from hor_items_detalle
                       where item_id = i.id)
         union all
         /* items detallados manualmente asignados a mi */
         select d.*,
                profesor_id,
                p.detalle_manual,
                i.creditos,
                p.creditos   creditos_detalle,
                p.creditos   creditos_pactados,
                p.id         det_profesor_id,
                1            orden,
                nombre,
                (select listagg(nombre) within group (order by nombre)
                 from hor_items_profesores pr, hor_profesores p
                 where     item_id = i.id
                       and pr.profesor_id = p.id)
                   profesor_completo,
                area_id
         from hor_items                    i,
              hor_items_detalle            d,
              hor_items_profesores         p,
              hor_items_det_profesores     dp,
              hor_profesores               pro
         where     i.id = d.item_id
               and i.id = p.item_id
               and p.detalle_manual = 1
               and dp.detalle_id = d.id
               and item_profesor_id = p.id
               and p.profesor_id = pro.id
         union all
         /* items detallados manualmente asignados a otro profesor */
         select d.*,
                profesor_id,
                p.detalle_manual,
                i.creditos,
                p.creditos   creditos_detalle,
                p.creditos_pactados,
                p.id         det_profesor_id,
                -1           orden,
                nombre,
                (select listagg(nombre) within group (order by nombre)
                 from hor_items_profesores pr, hor_profesores p
                 where     item_id = i.id
                       and pr.profesor_id = p.id)
                   profesor_completo,
                area_id
         from hor_items i, hor_items_detalle d, hor_items_profesores p, hor_profesores pro
         where     i.id = d.item_id
               and i.id = p.item_id
               and p.profesor_id = pro.id
               and exists
                      (select 1
                       from hor_items_det_profesores
                       where     detalle_id = d.id
                             and p.id <> item_profesor_id)
               and not exists
                      (select 1
                       from hor_items_det_profesores
                       where     detalle_id = d.id
                             and p.id = item_profesor_id))
   group by id,
            item_id,
            inicio,
            fin,
            descripcion,
            profesor_id,
            detalle_manual,
            creditos,
            creditos_detalle,
            creditos_pactados,
            det_profesor_id,
            nombre,
            profesor_completo,
            area_id;


CREATE INDEX UJI_HORARIOS.HOR_ITEM_PROF_DET_IDX ON UJI_HORARIOS.HOR_ITEMS_PROFESORES
(ITEM_ID, PROFESOR_ID, DETALLE_MANUAL, ID);

			
CREATE OR REPLACE procedure UJI_HORARIOS.pod_consulta_asignaturas_pdf(name_array    in euji_util.ident_arr2,
                                                                      value_array   in euji_util.ident_arr2) is
   v_aux                     number;
   -- pProfesor       number          := euji_util.euji_getparam (name_array, value_array, 'pProfesor');
   pDepartamento             number := euji_util.euji_getparam(name_array, value_array, 'pDepartamento');
   pArea                     number := euji_util.euji_getparam(name_array, value_array, 'pArea');
   pSesion                   varchar2(400) := euji_util.euji_getparam(name_array, value_array, 'pSesion');
   pDebug                    number := euji_util.euji_getparam(name_array, value_array, 'pDebug');
   v_nombre                  varchar2(2000);
   vSesion                   number;
   vCurso                    number;
   vCreditos                 number;
   vAcum                     number;
   vAcum2                    number;
   vAcum3                    number;
   vAcum4                    number;
   vAcum5                    number;
   vDepAcum                  number;
   vDepAcum2                 number;
   vDepAcum3                 number;
   vDepAcum4                 number;
   vDepAcum5                 number;
   vFechas                   t_horario;
   vRdo                      clob;


   type map_varchar_number is table of number
      index by varchar2(100);

   creditos_asi_area_map     map_varchar_number;

   creditos_asignatura_map   map_varchar_number;

   porcentaje_area           map_varchar_number;

   cursor lista_areas(p_Area in number) is
      select *
      from hor_areas
      where     departamento_id = pDepartamento
            and id = p_Area
      order by nombre;

   cursor lista_subgrupos(p_Area in number) is
      select *
      from (select nvl(comun_texto, ia.asignatura_id)   asignatura_txt,
                   min(ia.asignatura_id)                asignatura_id,
                   --hor_buscar_nombre_v2(nvl(comun_texto, ia.asignatura_id)) asignatura,
                   min(nombre_item)                     asignatura,
                   area_id,
                   grupo_id,
                   tipo_subgrupo_id,
                   subgrupo_id,
                   creditos
            from hor_v_asignaturas_area aa, hor_items i, hor_items_asignaturas ia
            where     i.id = ia.item_id
                  and area_id = p_area
                  and ia.asignatura_id = aa.asignatura_id
                  and grupo_id not in ('V', 'W')
            group by nvl(comun_texto, ia.asignatura_id),                                                   --asignatura,
                     area_id,
                     grupo_id,
                     tipo_subgrupo_id,
                     subgrupo_id,
                     creditos)
      --where rownum <= 30
      order by 1,
               2,
               3,
               4,
               5,
               decode(tipo_subgrupo_id,  'TE', 1,  'PR', 2,  'LA', 3,  'SE', 4,  'TU', 5,  6),
               7;


   cursor creditos_asignados_asi_area(p_area in number) is
      select asignatura_id,
             asignatura_txt,
             grupo_id,
             tipo,
             crd_final,
             id,
             item_id,
             cuantos,
             asignatura_id || '|' || grupo_id || '|' || tipo hash_subgrupo
      from HOR_V_ITEMS_CREDITOS_DETALLE
      where     area_id = p_area
            and item_id in (select min(item_id) item_id
                            from (select distinct i.id                                   item_id,
                                                  nvl(comun_texto, asi.asignatura_id)    asignatura_id,
                                                  semestre_id,
                                                  grupo_id,
                                                  tipo_subgrupo_id,
                                                  subgrupo_id
                                  from hor_items      i
                                       join hor_items_asignaturas asi on i.id = asi.item_id)
                            group by asignatura_id, semestre_id, grupo_id, tipo_subgrupo_id, subgrupo_id
                            having count(*) >= 1)
      order by asignatura_txt, grupo_id, tipo desc;

   cursor creditos_asignados is
      select sum(crd_final) creditos, hash_subgrupo
      from (select asignatura_id,
                   asignatura_txt,
                   grupo_id,
                   tipo,
                   crd_item,
                   crd_final,
                   id,
                   item_id,
                   cuantos,
                   asignatura_id || '|' || grupo_id || '|' || tipo   hash_subgrupo
            from hor_v_items_creditos_detalle
            where item_id in (select min(item_id) item_id
                              from (select distinct i.id                                     item_id,
                                                    nvl(comun_texto, asi.asignatura_id)      asignatura_id,
                                                    semestre_id,
                                                    grupo_id,
                                                    tipo_subgrupo_id,
                                                    subgrupo_id
                                    from hor_items        i
                                         join hor_items_asignaturas asi on i.id = asi.item_id)
                              group by asignatura_id, semestre_id, grupo_id, tipo_subgrupo_id, subgrupo_id))
      group by hash_subgrupo;

   procedure leerCreditosArea(p_area in number) is
   begin
      for x in creditos_asignados_asi_area(p_area) loop
         creditos_asi_area_map(x.hash_subgrupo) := x.crd_final;
      end loop;

      for x in creditos_asignados loop
         creditos_asignatura_map(x.hash_subgrupo) := x.creditos;
      end loop;
   end;

   function getCreditosAsiArea(p_asignatura          varchar2,
                               p_grupo               varchar2,
                               p_tipo_subgrupo_id    varchar2,
                               p_subgrupo_id         number)
      return number is
      hash_subgrupo   varchar2(100);
   begin
      begin
         hash_subgrupo := p_asignatura || '|' || p_grupo || '|' || p_tipo_subgrupo_id || p_subgrupo_id;
         return creditos_asi_area_map(hash_subgrupo);
      exception
         when others then
            return 0;
      end;
   end;

   function getCreditosAsi(p_asignatura varchar2, p_grupo varchar2, p_tipo_subgrupo_id varchar2, p_subgrupo_id number)
      return number is
      hash_subgrupo   varchar2(100);
   begin
      begin
         hash_subgrupo := p_asignatura || '|' || p_grupo || '|' || p_tipo_subgrupo_id || p_subgrupo_id;
         return creditos_asignatura_map(hash_subgrupo);
      exception
         when others then
            return 0;
      end;
   end;

   function formatear(p_entrada in number)
      return varchar2 is
      v_rdo   varchar2(100);
   begin
      v_rdo := replace(to_char(p_entrada, '9990.00'), '.', ',');
      return (v_rdo);
   end;

   procedure cabecera is
   begin
      owa_util.mime_header(ccontent_type => 'text/xml', bclose_header => true);
      fop.documentOpen(page_width             => 29.7,
                       page_height            => 21,
                       margin_left            => 1.5,
                       margin_right           => 1.5,
                       margin_top             => 0.5,
                       margin_bottom          => 0.5,
                       extent_before          => 3,
                       extent_after           => 1.5,
                       extent_margin_top      => 3,
                       extent_margin_bottom   => 1.5);
      fop.pageBodyOpen;
      htp.p
      (
         fof.documentheaderopen || fof.blockopen(space_after => 0.1) || fof.tableOpen || fof.tablecolumndefinition(
         column_width                                                                                           => 3)
         || fof.tablecolumndefinition(column_width => 24) || fof.tablebodyopen || fof.tableRowOpen || fof.tablecellopen
         || fof.block(fof.logo_uji,
         text_align   => 'center') || fof.tablecellclose || fof.tablecellopen(display_align => 'center', padding => 0.4)
         || fof.block(text => v_nombre, font_size => 16, font_weight => 'bold', font_family => 'Arial', text_align => 'center'
         ) || fof.block
         (
         text   => 'Informe POD per assignatura - Curs: ' || vCurso || '/' || substr
         (
         to_char                                                                 (vCurso + 1),
         3
         ),
         font_size   => 16,
         font_weight   => 'bold',
         font_family   => 'Arial',
         text_align   => 'center'
         ) || fof.tablecellclose || fof.tableRowClose || fof.tablebodyclose || fof.tableClose || fof.blockclose || fof
         .documentheaderclose
      );
      -- El numero paginas se escribe a mano para poner el texto m?s peque?o
      htp.p
      (
         fof.documentfooteropen || fof.tableOpen || fof.tablecolumndefinition(column_width => 18) || fof.tablebodyopen
         || fof.tableRowOpen || fof.tablecellopen || fof.block(text   =>
         'Pag. <fo:page-number /> de <fo:page-number-citation ref-id="lastBlock" />',
         text_align                                                   => 'right',
         font_weight                                                  => 'bold',
         font_size                                                    => 8) || fof.tablecellclose || fof.tableRowClose
         || fof.tablebodyclose || fof.tableClose || fof.documentfooterclose
      );
      fop.pageFlowOpen;
      fop.block(fof.white_space);
   end;

   procedure pie is
   begin
      htp.p('<fo:block  id="lastBlock" space-before="1cm" font-size="8pt">');
      htp.p('Data de generaci�: ' || to_char(sysdate, 'dd/mm/yyy hh24:mi'));
      fop.blockclose();
      fop.pageFlowClose;
      fop.pageBodyClose;
      fop.documentClose;
   end;

   function espacios(p_espacios in number)
      return varchar2 is
      v_rdo   varchar2(2000);
   begin
      v_rdo := null;

      for x in 1 .. p_espacios loop
         v_rdo := v_rdo || fof.white_space;
      end loop;

      return v_rdo;
   end;

   function getProcentageImpartidoArea(pAsignaturaId varchar2, pUestId number)
      return number is
      grupo_comun     number;
      resultado       number;
      area_asi_hash   varchar2(50);
   begin
      begin
         area_asi_hash := pAsignaturaId || pUestId;
         return porcentaje_area(area_asi_hash);
      exception
         when others then
            begin
               select grupo_comun_id
               into grupo_comun
               from hor_ext_asignaturas_comunes
               where asignatura_id = pAsignaturaId;

               select sum(ac.porcentaje * aa.porcentaje / 100)
               into resultado
               from hor_ext_asignaturas_comunes   ac
                    join hor_ext_asignaturas_area aa on AC.ASIGNATURA_ID = aa.asi_id
               where     ac.grupo_comun_id = grupo_comun
                     and uest_id = pUestId;

               porcentaje_area(area_asi_hash) := resultado;

               return resultado;
            exception
               when others then
                  select porcentaje
                  into resultado
                  from hor_ext_asignaturas_area
                  where     asi_id = pAsignaturaId
                        and uest_id = pUestId;

                  porcentaje_area(area_asi_hash) := resultado;

                  return resultado;
            end;
      end;
   end;

   procedure mostrar_subgrupos is
      v_asi_ant           varchar2(100);
      v_asi_id_ant        varchar2(100);
      v_border            varchar2(100);
      v_creditos          number;
      v_creditos_Area     number;
      v_porcentaje        number;
      v_porcentaje_area   number;
      v_acum_1            number;
      v_acum_2            number;
      v_acum_3            number;
      v_area_1            number;
      v_area_2            number;
      v_area_3            number;
      v_area_4            number;
      v_area_1_porcent    number;
      v_det_tipo_ant      varchar2(200);
      v_det_asi_ant       varchar2(200);
      v_det_grupo_ant     varchar2(200);
   begin
      for a in lista_areas(pArea) loop
         --leerCreditosArea(a.id);

         fop.block('Area de coneixement: ' || fof.bold(a.nombre), font_size => 16, space_after => 0.2);
         fop.tableOpen;
         fop.tablecolumndefinition(column_width => 4);
         fop.tablecolumndefinition(column_width => 8);
         fop.tablecolumndefinition(column_width => 1);
         fop.tablecolumndefinition(column_width => 1);
         fop.tablecolumndefinition(column_width => 2);
         fop.tablecolumndefinition(column_width => 2);
         fop.tablecolumndefinition(column_width => 2);
         fop.tablecolumndefinition(column_width => 2);
         fop.tablecolumndefinition(column_width => 3);
         fop.tablecolumndefinition(column_width => 2);
         fop.tablebodyopen;
         fop.tableRowOpen;
         fop.tablecellopen;
         fop.block(fof.bold('Codi'), font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block(fof.bold('Nom assignatura'), font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block(fof.bold('Grup'), font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block(fof.bold('Tipus'), font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block(fof.bold('Cr�dits a impartir (A)'), font_size => 10, text_align => 'center');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block(fof.bold('Cr�dits assignats (B)'), font_size => 10, text_align => 'center');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block(fof.bold('(B) - (A)'), font_size => 10, text_align => 'center');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block(fof.bold('Cr�dits assignats �rea (C)'), font_size => 10, text_align => 'center');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block(fof.bold('(C) / (A)'), font_size => 10, text_align => 'center');
         fop.tablecellclose;
         fop.tableRowClose;
         v_asi_ant := 'XX';
         v_acum_1 := 0;
         v_acum_2 := 0;
         v_acum_3 := 0;
         v_area_1 := 0;
         v_area_1_porcent := 0;
         v_area_2 := 0;
         v_area_3 := 0;
         v_area_4 := 0;

         v_det_tipo_ant := '';
         v_det_asi_ant := '';
         v_det_grupo_ant := '';

         for det in lista_subgrupos(a.id) loop
            v_porcentaje_Area := 0;

            v_porcentaje_area := getProcentageImpartidoArea(det.asignatura_id, a.id);

            if (v_porcentaje_area = 0) then
               continue;
               null;
            end if;

            if     v_det_tipo_ant = det.tipo_subgrupo_id
               and v_det_asi_ant = det.asignatura_id
               and v_det_grupo_ant = det.grupo_id then
               /* solo recalculamos si es necesario */
               null;
            else
               /*                select nvl(sum(nvl(creditos_detalle, creditos / cuantos)), 0) --nvl (sum (nvl (creditos_detalle, creditos / cuantos)), 0)
                                                                                            ,
                                      nvl(sum(decode(area_id, a.id, nvl(creditos_detalle, creditos / cuantos), 0)), 0)
                               into v_creditos, v_creditos_area
                               from (select distinct
                                            nvl(comun_texto, asignatura_id)   asignatura_id,
                                            grupo_id,
                                            tipo_subgrupo_id || subgrupo_id   subgrp,
                                            d.profesor_id,
                                            creditos_detalle,
                                            d.creditos,
                                            area_id,
                                            count(distinct profesor_id)
                                               over (partition by asignatura_id, grupo_id, tipo_subgrupo_id || subgrupo_id)
                                               cuantos
                                     from hor_v_items_prof_detalle d, hor_profesores p, hor_items_asignaturas asi, hor_items i
                                     where     d.item_id in (select i.id
                                                             from hor_items i, hor_items_asignaturas asi2
                                                             where     i.id = asi2.item_id
                                                                   and asignatura_id = det.asignatura_id
                                                                   and grupo_id = det.grupo_id
                                                                   and tipo_subgrupo_id = det.tipo_subgrupo_id
                                                                   and subgrupo_id = det.subgrupo_id)
                                           and profesor_id = p.id
                                           and d.item_id = asi.item_id
                                           and d.item_id = i.id
                                           and rownum <= 2
                                           and semestre_id = (select min(semestre_id)
                                                              from hor_items i, hor_items_asignaturas asi2
                                                              where     i.id = asi2.item_id
                                                                    and asignatura_id = det.asignatura_id
                                                                    and grupo_id = det.grupo_id
                                                                    and tipo_subgrupo_id = det.tipo_subgrupo_id
                                                                    and subgrupo_id = det.subgrupo_id))
                               where area_id = a.id;
             */

               if 1 = 2 then
                  select nvl(sum(nvl(creditos_detalle, creditos / cuantos)), 0) --nvl (sum (nvl (creditos_detalle, creditos / cuantos)), 0)
                                                                               ,
                         nvl(sum(decode(area_id, a.id, nvl(creditos_detalle, creditos / cuantos), 0)), 0)
                  into v_creditos, v_creditos_area
                  from (select distinct
                               nvl(comun_texto, asignatura_id)   asignatura_id,
                               grupo_id,
                               tipo_subgrupo_id || subgrupo_id   subgrp,
                               d.profesor_id,
                               creditos_detalle,
                               d.creditos,
                               area_id,
                               count(distinct profesor_id)
                                  over (partition by asignatura_id, grupo_id, tipo_subgrupo_id || subgrupo_id)
                                  cuantos
                        from (select to_number
                                     (
                                        id || det_profesor_id || item_id || to_char(inicio, 'yyyymmddhh24miss')
                                     )
                                        id,
                                     id     detalle_id,
                                     det_profesor_id,
                                     item_id,
                                     inicio,
                                     fin,
                                     descripcion,
                                     profesor_id,
                                     detalle_manual,
                                     creditos,
                                     creditos_detalle,
                                     creditos_pactados,
                                     decode(detalle_manual, 0, 'S', decode(min(orden),  -1, 'X',  1, 'S',  'N'))
                                        seleccionado,
                                     nombre,
                                     area_id,
                                     grupo_id,
                                     tipo_subgrupo_id,
                                     subgrupo_id,
                                     comun_Texto
                              from (                                               /* items no detallados manualmente */
                                    select d.*,
                                           profesor_id,
                                           p.detalle_manual,
                                           i.creditos,
                                           p.creditos       creditos_detalle,
                                           p.creditos_pactados,
                                           p.id             det_profesor_id,
                                           99               orden,
                                           nombre,
                                           area_id,
                                           grupo_id,
                                           tipo_subgrupo_id,
                                           subgrupo_id,
                                           comun_Texto
                                    from hor_items                    i,
                                         hor_items_detalle            d,
                                         hor_items_profesores         p,
                                         hor_profesores               pro
                                    where     i.id = d.item_id
                                          and i.id = p.item_id
                                          and p.profesor_id = pro.id
                                          and 0 = (select COUNT(*)
                                                   from hor_items_det_profesores
                                                   where detalle_id = d.id)
                                    union all
                                    /* ????? */
                                    select i.id,
                                           i.id                                item_id,
                                           null                                inicio,
                                           null                                fin,
                                           null                                descripcion,
                                           profesor_id,
                                           p.detalle_manual,
                                           i.creditos,
                                           round(p.creditos, 2)                creditos_detalle,
                                           round(p.creditos_pactados, 2)       creditos_pactados,
                                           p.id                                det_profesor_id,
                                           99                                  orden,
                                           nombre,
                                           area_id,
                                           grupo_id,
                                           tipo_subgrupo_id,
                                           subgrupo_id,
                                           comun_Texto
                                    from hor_items i, hor_items_profesores p, hor_profesores pro
                                    where     i.id = p.item_id
                                          and p.profesor_id = pro.id
                                          and 0 = (select count(*)
                                                   from hor_items_detalle
                                                   where item_id = i.id)
                                    union all
                                    /* items detallados manualmente asignados a mi */
                                    select d.*,
                                           profesor_id,
                                           p.detalle_manual,
                                           i.creditos,
                                           p.creditos       creditos_detalle,
                                           p.creditos       creditos_pactados,
                                           p.id             det_profesor_id,
                                           1                orden,
                                           nombre,
                                           area_id,
                                           grupo_id,
                                           tipo_subgrupo_id,
                                           subgrupo_id,
                                           comun_Texto
                                    from hor_items                        i,
                                         hor_items_detalle                d,
                                         hor_items_profesores             p,
                                         hor_items_det_profesores         dp,
                                         hor_profesores                   pro
                                    where     i.id = d.item_id
                                          and i.id = p.item_id
                                          and p.detalle_manual = 1
                                          and dp.detalle_id = d.id
                                          and item_profesor_id = p.id
                                          and p.profesor_id = pro.id
                                    union all
                                    /* items detallados manualmente asignados a otro profesor */
                                    select d.*,
                                           profesor_id,
                                           p.detalle_manual,
                                           i.creditos,
                                           p.creditos       creditos_detalle,
                                           p.creditos_pactados,
                                           p.id             det_profesor_id,
                                           -1               orden,
                                           nombre,
                                           area_id,
                                           grupo_id,
                                           tipo_subgrupo_id,
                                           subgrupo_id,
                                           comun_Texto
                                    from hor_items                    i,
                                         hor_items_detalle            d,
                                         hor_items_profesores         p,
                                         hor_profesores               pro
                                    where     i.id = d.item_id
                                          and i.id = p.item_id
                                          and p.profesor_id = pro.id
                                          and 0 < (select count(*)
                                                   from hor_items_det_profesores
                                                   where     detalle_id = d.id
                                                         and p.id <> item_profesor_id)
                                          and 0 = (select count(*)
                                                   from hor_items_det_profesores
                                                   where     detalle_id = d.id
                                                         and p.id = item_profesor_id))
                              group by id,
                                       item_id,
                                       inicio,
                                       fin,
                                       descripcion,
                                       profesor_id,
                                       detalle_manual,
                                       creditos,
                                       creditos_detalle,
                                       creditos_pactados,
                                       det_profesor_id,
                                       nombre,
                                       area_id,
                                       grupo_id,
                                       tipo_subgrupo_id,
                                       subgrupo_id,
                                       comun_Texto) d,
                             hor_items_asignaturas     asi
                        --hor_items                 i
                        where     d.item_id in (select i2.id
                                                from hor_items i2, hor_items_asignaturas asi2
                                                where     i2.id = asi2.item_id
                                                      and asignatura_id = det.asignatura_id
                                                      and i2.grupo_id = det.grupo_id
                                                      and i2.tipo_subgrupo_id = det.tipo_subgrupo_id
                                                      and i2.subgrupo_id = det.subgrupo_id)
                              and d.item_id = asi.item_id                                  --and d.item_id = asi.item_id
                                                         );
               end if;


               select nvl(sum(nvl(creditos_detalle, creditos / cuantos)), 0) --nvl (sum (nvl (creditos_detalle, creditos / cuantos)), 0)
                                                                            ,
                      nvl(sum(decode(area_id, a.id, nvl(creditos_detalle, creditos / cuantos), 0)), 0)
               into v_creditos, v_creditos_area
               from (select asignatura_id,
                            grupo_id,
                            tipo_subgrupo_id || subgrupo_id   subgrp,
                            profesor_id,
                            crd_prof                          creditos_Detalle,
                            --nvl(crd_prof, round(creditos / total, 2)) creditos,
                            creditos,
                            area_id,
                            total                             cuantos
                     from (select distinct
                                  i.creditos,
                                  nvl(comun_Texto, asignatura_id)     asignatura_id,
                                  grupo_id,
                                  tipo_subgrupo_id,
                                  subgrupo_id,
                                  profesor_id,
                                  ip.detalle_manual,
                                  ip.creditos                         crd_prof,
                                  departamento_id,
                                  area_id,
                                  count(distinct profesor_id)
                                     over (partition by asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo_id)
                                     total
                           from hor_items i, hor_items_asignaturas a, hor_items_profesores ip, hor_profesores p
                           where     i.id = a.item_id
                                 and i.id = ip.item_id
                                 and ip.profesor_id = p.id
                                 and a.item_id in (select i2.id
                                                   from hor_items i2, hor_items_asignaturas asi2
                                                   where     i2.id = asi2.item_id
                                                         and asignatura_id = det.asignatura_id
                                                         and i2.grupo_id = det.grupo_id
                                                         and i2.tipo_subgrupo_id = det.tipo_subgrupo_id
                                                         and i2.subgrupo_id = det.subgrupo_id)));
            end if;

            v_det_tipo_ant := det.tipo_subgrupo_id;
            v_det_asi_ant := det.asignatura_id;
            v_det_grupo_ant := det.grupo_id;

            --if (pDebug = 1) then
            --v_creditos_area := getCreditosAsiArea(det.asignatura_id, det.grupo_id, det.tipo_subgrupo_id, det.subgrupo_id);
            --v_creditos := getCreditosAsi(det.asignatura_id, det.grupo_id, det.tipo_subgrupo_id, det.subgrupo_id);
            --end if;

            fop.tableRowOpen;

            if v_asi_ant = det.asignatura_txt then
               fop.tablecellopen;
               fop.block(fof.white_space, font_size => 10);
               fop.tablecellclose;
               fop.tablecellopen;
               fop.block(fof.white_space, font_size => 10);
               fop.tablecellclose;
               v_border := '';
            else
               if v_asi_ant <> 'XX' then
                  -- linea 1
                  fop.tablecellopen(number_columns_spanned => 2);
                  fop.block(fof.bold('Total per assignatura'), font_size => 10, space_after => 0.2, text_align => 'right');
                  fop.tablecellclose;
                  fop.tablecellopen(number_columns_spanned => 2);
                  fop.block(fof.white_space, font_size => 10);
                  fop.tablecellclose;
                  fop.tablecellopen;
                  fop.block(fof.bold(formatear(v_acum_1)), font_size => 10, text_align => 'center');
                  fop.tablecellclose;
                  fop.tablecellopen;
                  fop.block(fof.bold(formatear(v_acum_2)), font_size => 10, text_align => 'center');
                  fop.tablecellclose;
                  fop.tablecellopen;
                  fop.block(fof.bold(formatear(v_acum_2 - v_acum_1)), font_size => 10, text_align => 'center');
                  fop.tablecellclose;
                  fop.tablecellopen;
                  fop.block(fof.bold(formatear(v_acum_3)), font_size => 10, text_align => 'center');
                  fop.tablecellclose;
                  fop.tablecellopen;

                  if v_acum_1 <> 0 then
                     fop.block(fof.bold(formatear(v_acum_2 / v_acum_1 * 100) || '%'),
                               font_size    => 10,
                               text_align   => 'center');
                  else
                     fop.block(fof.bold(formatear(0) || '%'), font_size => 10, text_align => 'center');
                  end if;

                  fop.tablecellclose;
                  fop.tableRowClose;
                  -- linea 2
                  fop.tableRowOpen;
                  fop.tablecellopen(number_columns_spanned => 2);

                  fop.block(fof.bold('Cr�dits a impartir per l''�rea de coneixement'),
                            font_size     => 10,
                            space_after   => 0.2,
                            text_align    => 'right');
                  fop.tablecellclose;
                  fop.tablecellopen(number_columns_spanned => 5);
                  fop.block(fof.white_space, font_size => 10);
                  fop.tablecellclose;
                  fop.tablecellopen;

                  if v_porcentaje <> 0 then
                     fop.block(fof.bold(formatear(v_acum_1 * v_porcentaje / 100)),
                               font_size    => 10,
                               text_align   => 'center');
                     v_area_4 := v_area_4 + v_acum_1 * v_porcentaje / 100;
                  else
                     fop.block(fof.bold(formatear(0)), font_size => 10, text_align => 'center');
                  end if;

                  fop.tablecellclose;
                  fop.tablecellOpen;

                  if ((v_acum_1 * v_porcentaje) > 0) then
                     fop.block(fof.bold(formatear(nvl((100 * 100 * v_acum_3 / (v_acum_1 * v_porcentaje)), 0)) || '%'),
                               font_size    => 10,
                               text_align   => 'center');
                  else
                     fop.block('-', font_size => 10, text_align => 'center');
                  end if;

                  --                  fop.block(fof.bold(formatear(nvl(v_porcentaje, 0)) || '%'), font_size => 10, text_align => 'center');

                  fop.tablecellclose;
                  fop.tableRowClose;
                  -- linea 3
                  --                  fop.tableRowOpen;
                  --                  fop.tablecellopen(number_columns_spanned => 2);
                  --                  fop.block(fof.bold('Estat de l''assignaci� docent'),
                  --                            font_size     => 10,
                  --                            space_after   => 0.2,
                  --                            text_align    => 'right');
                  --                  fop.tablecellclose;
                  --                  fop.tablecellopen(number_columns_spanned => 5);
                  --                  fop.block(fof.white_space, font_size => 10);
                  --                  fop.tablecellclose;
                  --
                  --                  if v_porcentaje <> 0 then
                  --                     fop.tablecellopen;
                  --                     fop.block(fof.bold(formatear((v_acum_1 * v_porcentaje / 100) - v_acum_3)),
                  --                               font_size    => 10,
                  --                               text_align   => 'center');
                  --                     fop.tablecellclose;
                  --
                  --                     -- CAU #31425
                  --                     fop.tablecellopen;
                  --
                  --                     if ((v_acum_1 * v_porcentaje) > 0) then
                  --                        fop.block
                  --                        (
                  --                           fof.bold(formatear(nvl((100 * 100 * v_acum_3 / (v_acum_1 * v_porcentaje)), 0)) || '%'),
                  --                           font_size    => 10,
                  --                           text_align   => 'center'
                  --                        );
                  --                     else
                  --                        fop.block('-', font_size => 10, text_align => 'center');
                  --                     end if;
                  --
                  --                     fop.tablecellclose;
                  --                  else
                  --                     fop.tablecellopen;
                  --                     fop.block(fof.bold(formatear(0)), font_size => 10, text_align => 'center');
                  --                     fop.tablecellclose;
                  --                  end if;
                  --
                  --                  fop.tableRowClose;
                  -- continuacion
                  fop.tableRowOpen;
               end if;

               v_acum_1 := 0;
               v_acum_2 := 0;
               v_acum_3 := 0;

               if v_asi_ant = 'XX' then
                  v_border := '';
               else
                  v_border := 'solid';
               end if;

               fop.tablecellopen(border_top => v_border);
               fop.block(det.asignatura_txt, font_size => 10);
               fop.tablecellclose;
               fop.tablecellopen(border_top => v_border);
               fop.block(det.asignatura, font_size => 10);
               fop.tablecellclose;
            end if;

            fop.tablecellopen(border_top => v_border);
            fop.block(det.grupo_id, font_size => 10, text_align => 'center');
            fop.tablecellclose;
            fop.tablecellopen(border_top => v_border);
            fop.block(det.tipo_subgrupo_id || det.subgrupo_id, font_size => 10, text_align => 'center');
            fop.tablecellclose;
            fop.tablecellopen(border_top => v_border);
            fop.block(formatear(det.creditos), font_size => 10, text_align => 'center');
            fop.tablecellclose;
            fop.tablecellopen(border_top => v_border);
            fop.block(formatear(v_creditos), font_size => 10, text_align => 'center');
            fop.tablecellclose;
            fop.tablecellopen(border_top => v_border);
            fop.block(formatear(v_creditos - det.creditos), font_size => 10, text_align => 'center');
            fop.tablecellclose;
            fop.tablecellopen(border_top => v_border);
            fop.block(formatear(v_creditos_Area), font_size => 10, text_align => 'center');
            fop.tablecellclose;
            fop.tablecellopen(border_top => v_border);

            if v_creditos > 0 then
               fop.block(formatear(v_creditos_area / det.creditos * 100) || '%', font_size => 10, text_align => 'center');
            else
               fop.block(formatear(0) || '%', font_size => 10, text_align => 'center');
            end if;

            fop.tablecellclose;
            fop.tableRowClose;
            v_asi_ant := det.asignatura_txt;                                             --det.asignatura_id (HOR-1024);
            v_asi_id_ant := det.asignatura_id;
            v_acum_1 := v_acum_1 + det.creditos;
            v_acum_2 := v_acum_2 + v_creditos;
            v_acum_3 := v_acum_3 + v_creditos_Area;
            v_area_1 := v_area_1 + det.creditos;
            v_area_2 := v_area_2 + v_creditos;
            v_area_3 := v_area_3 + v_creditos_Area;
            --v_area_4 := v_area_4 + v_creditos_Area * v_porcentaje;

            v_porcentaje := getProcentageImpartidoArea(det.asignatura_id, a.id);

            v_area_1_porcent := v_area_1_porcent + det.creditos * v_porcentaje / 100;
         end loop;

         -- linea 1
         fop.tableRowOpen;
         fop.tablecellopen(number_columns_spanned => 2);
         fop.block(fof.bold('Total per assignatura'), font_size => 10, space_after => 0.2, text_align => 'right');
         fop.tablecellclose;
         fop.tablecellopen(number_columns_spanned => 2);
         fop.block(fof.white_space, font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block(fof.bold(formatear(v_acum_1)), font_size => 10, text_align => 'center');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block(fof.bold(formatear(v_acum_2)), font_size => 10, text_align => 'center');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block(fof.bold(formatear(v_acum_2 - v_acum_1)), font_size => 10, text_align => 'center');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block(fof.bold(formatear(v_acum_3)), font_size => 10, text_align => 'center');
         fop.tablecellclose;
         fop.tablecellopen;

         if v_acum_1 <> 0 then
            fop.block(fof.bold(formatear(v_acum_2 / v_acum_1 * 100) || '%'), font_size => 10, text_align => 'center');
         else
            fop.block(fof.bold(formatear(0) || '%'), font_size => 10, text_align => 'center');
         end if;

         fop.tablecellclose;
         fop.tableRowClose;
         -- linea 2
         fop.tableRowOpen;
         fop.tablecellopen(number_columns_spanned => 2);
         fop.block(fof.bold('Cr�dits a impartir per l''�rea de coneixement'),
                   font_size     => 10,
                   space_after   => 0.2,
                   text_align    => 'right');
         fop.tablecellclose;
         fop.tablecellopen(number_columns_spanned => 5);
         fop.block(fof.white_space, font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;

         if nvl(v_porcentaje, 0) <> 0 then
            fop.block(fof.bold(formatear(v_acum_1 * v_porcentaje / 100)), font_size => 10, text_align => 'center');
            v_area_4 := v_area_4 + v_acum_1 * v_porcentaje / 100;
         else
            fop.block(fof.bold(formatear(0)), font_size => 10, text_align => 'center');
         end if;

         fop.tablecellclose;
         fop.tablecellopen;


         if (v_Acum_1 * v_porcentaje) <> 0 then
            fop.block(fof.bold(formatear(nvl((100 * 100 * v_acum_3 / (v_acum_1 * v_porcentaje)), 0)) || '%'),
                      font_size    => 10,
                      text_align   => 'center');
         else
            fop.block(fof.bold(formatear(0)) || '%', font_size => 10, text_align => 'center');
         end if;

         fop.tablecellclose;
         fop.tableRowClose;
         fop.tableRowOpen;
         fop.tablecellopen(number_columns_spanned => 2);
         fop.block(fof.bold('Total per �rea'),
                   font_size      => 13,
                   space_before   => 0.2,
                   space_after    => 0.2,
                   text_align     => 'right');
         fop.tablecellclose;
         fop.tablecellopen(number_columns_spanned => 2);
         fop.block(fof.white_space, font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block(fof.bold(formatear(v_area_1)), font_size => 13, text_align => 'center');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block(fof.bold(formatear(v_area_2)), font_size => 13, text_align => 'center');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block(fof.bold(formatear(v_area_2 - v_area_1)), font_size => 13, text_align => 'center');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block(fof.bold(formatear(v_area_3)), font_size => 13, text_align => 'center');
         fop.tablecellclose;
         fop.tablecellopen;

         if nvl(v_area_1, 0) <> 0 then
            fop.block(fof.bold(formatear(v_area_2 / v_area_1 * 100) || '%'), font_size => 13, text_align => 'center');
         else
            fop.block(fof.bold(formatear(0) || '%'), font_size => 10, text_align => 'center');
         end if;

         fop.tablecellclose;
         fop.tableRowClose;
         -- Total linea 2
         fop.tableRowOpen;
         fop.tablecellopen(number_columns_spanned => 2);
         fop.block(fof.bold('Total cr�dits a impartir per l''�rea de coneixement'),
                   font_size     => 13,
                   space_after   => 0.2,
                   text_align    => 'right');
         fop.tablecellclose;
         fop.tablecellopen(number_columns_spanned => 5);
         fop.block(fof.white_space, font_size => 13);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block(fof.bold(formatear(v_area_4)), font_size => 13, text_align => 'center');
         fop.tablecellclose;
         fop.tablecellopen;

         if v_area_4 <> 0 then
            fop.block(fof.bold(formatear(nvl(100 * v_area_3 / v_area_4, 0)) || '%'),
                      font_size    => 13,
                      text_align   => 'center');
         else
            fop.block(fof.bold(formatear(0) || '%'), font_size => 13, text_align => 'center');
         end if;


         fop.tablecellclose;
         fop.tableRowClose;
         -- Fin total linea 2

         fop.tablebodyClose;
         fop.tableClose;
         fop.block(fof.white_space, space_before => 0.5);
      end loop;
   end;
begin
   select count(*) + 1
   into vSesion
   from apa_sesiones
   where     sesion_id = pSesion
         and fecha < sysdate() - 0.33;

   select id into vCurso from hor_curso_academico;

   if vSesion > 0 then
      select nombre
      into v_nombre
      from hor_Departamentos
      where id = pDepartamento;

      cabecera;
      fop.block(fof.bold('----  Informe per assignatura  -----'),
                font_size     => 16,
                space_after   => 0.5,
                text_align    => 'center');
      mostrar_subgrupos;
      --htp.p ('<fo:block break-before="page" color="white">_</fo:block>');
      pie;
   else
      cabecera;
      fop.block('No tens perm�s per accedir a aquest document.',
                font_weight   => 'bold',
                font_size     => 14,
                space_after   => 0.5);
      pie;
   end if;
exception
   when others then
      --htp.print(dbms_utility.format_error_backtrace);
      htp.print(sqlerrm);
end;


ALTER TABLE UJI_HORARIOS.HOR_EXAMENES
 ADD (comentario_es  VARCHAR2(100))
;

ALTER TABLE UJI_HORARIOS.HOR_EXAMENES
 ADD (comentario_uk  VARCHAR2(100))
;

ALTER TABLE UJI_HORARIOS.HOR_EXT_ASIGNATURAS_DETALLE
 ADD (crd_total_computables  NUMBER);

