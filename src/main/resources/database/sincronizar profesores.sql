/* Formatted on 23/03/2014 09:41 (Formatter Plus v4.8.8) */
declare
   cursor lista is
      select c.per_id, p.apellido1 || ' ' || apellido2 || ', ' || p.nombre nombre, busca_cuenta (c.per_id) cuenta,
             ubicacion_id, area_id, cdo_max, reduccion, cdo_max - reduccion creditos,
             decode (ubicacion_id, null, 1, 0) por_contratar
      from   gra_pod.pod_carga_docente c,
             per_personas p,
             (select *
              from   grh_vw_contrataciones_ult
              where  act_id = 'PDI') cont
      where  curso_aca = 2014
      and    c.per_id = p.id
      and    c.per_id = cont.per_id(+);
begin
   for x in lista loop
      begin
         insert into uji_horarios.hor_profesores
                     (id, nombre, email, area_id, departamento_id,
                      pendiente_contratacion, creditos, creditos_maximos, creditos_reduccion
                     )
         values      (x.per_id, x.nombre, substr (x.cuenta, 1, instr (x.cuenta, '@') - 1), x.area_id, x.ubicacion_id,
                      x.por_contratar, x.creditos, x.cdo_max, x.reduccion
                     );
      exception
         when others then
            update uji_horarios.hor_profesores
            set nombre = x.nombre,
                creditos_maximos = x.cdo_max,
                creditos_reduccion = x.reduccion,
                creditos = x.creditos
            where  id = x.per_id;
      end;

      commit;
   end loop;
end;


