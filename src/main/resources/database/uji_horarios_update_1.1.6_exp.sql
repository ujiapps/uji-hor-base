/* Formatted on 09/05/2014 11:07 (Formatter Plus v4.8.8) */

ALTER TABLE UJI_HORARIOS.HOR_PERMISOS_EXTRA
 ADD (curso_id  NUMBER);

		   

		   
		   
/* Formatted on 09/05/2014 11:19 (Formatter Plus v4.8.8) */
CREATE OR REPLACE FORCE VIEW UJI_HORARIOS.HOR_EXT_ASIGNATURAS_AREA (ASI_ID, UEST_ID, RECIBE_ACTA, PORCENTAJE, CURSO_ACA) AS
   select "ASI_ID", "UEST_ID", "RECIBE_ACTA", "PORCENTAJE", "CURSO_ACA"
   from   pod_asignaturas_area p,
          hor_curso_academico c
   where  curso_aca = c.id
   and    porcentaje > 0
   union all   
   select pasi_id, uest_id, 'S' recibe_acta, porcentaje_comp, curso_aca
from   pop_asignaturas_area
where  substr (pasi_id, 1, 3) in ('SIX','SIV','SJA','SIU','SIW');


		   
		   
CREATE OR REPLACE FORCE VIEW UJI_HORARIOS.HOR_V_ITEMS_DETALLE (ID,
                                                               FECHA,
                                                               DOCENCIA_PASO_1,
                                                               DOCENCIA_PASO_2,
                                                               DOCENCIA,
                                                               ORDEN_ID,
                                                               NUMERO_ITERACIONES,
                                                               REPETIR_CADA_SEMANAS,
                                                               FECHA_INICIO,
                                                               FECHA_FIN,
                                                               ESTUDIO_ID,
                                                               SEMESTRE_ID,
                                                               CURSO_ID,
                                                               ASIGNATURA_ID,
                                                               GRUPO_ID,
                                                               TIPO_SUBGRUPO_ID,
                                                               SUBGRUPO_ID,
                                                               DIA_SEMANA_ID,
                                                               TIPO_DIA,
                                                               FESTIVOS
                                                              ) AS
   SELECT i.id, fecha, docencia docencia_paso_1,
          DECODE (NVL (d.repetir_cada_semanas, 1),
                  1, docencia,
                  DECODE (MOD (orden_id, d.repetir_cada_semanas), 1, docencia, 'N')
                 ) docencia_paso_2,
          decode (tipo_dia,
                  'F', 'N',
                  DECODE (d.numero_iteraciones,
                          NULL, DECODE (NVL (d.repetir_cada_semanas, 1),
                                        1, docencia,
                                        DECODE (MOD (orden_id, d.repetir_cada_semanas), 1, docencia, 'N')
                                       ),
                          DECODE (SIGN (((orden_id - festivos) / d.repetir_cada_Semanas) - (d.numero_iteraciones)),
                                  1, 'N',
                                  DECODE (NVL (d.repetir_cada_semanas, 1),
                                          1, docencia,
                                          DECODE (MOD (orden_id, d.repetir_cada_semanas), 1, docencia, 'N')
                                         )
                                 )
                         )
                 ) docencia,
          d.orden_id, d.numero_iteraciones, d.repetir_cada_semanas, d.fecha_inicio, d.fecha_fin, d.estudio_id,
          d.semestre_id, d.curso_id, d.asignatura_id, d.grupo_id, d.tipo_subgrupo_id, d.subgrupo_id, d.dia_semana_id,
          tipo_dia, festivos
   FROM   (SELECT id, fecha,
                  hor_contar_festivos (NVL (x.desde_el_dia, fecha_inicio), x.fecha, x.dia_semana_id,
                                       x.repetir_cada_semanas) festivos,
                  ROW_NUMBER () OVER (PARTITION BY DECODE
                                                       (decode (sign (x.fecha - fecha_inicio),
                                                                -1, 'N',
                                                                decode (sign (fecha_fin - x.fecha), -1, 'N', 'S')
                                                               ),
                                                        'S', DECODE (decode (sign (x.fecha
                                                                                   - NVL (x.desde_el_dia, fecha_inicio)),
                                                                             -1, 'N',
                                                                             decode (sign (NVL (x.hasta_el_dia,
                                                                                                fecha_fin)
                                                                                           - x.fecha),
                                                                                     -1, 'N',
                                                                                     'S'
                                                                                    )
                                                                            ),
                                                                     'S', 'S',
                                                                     'N'
                                                                    ),
                                                        'N'
                                                       ), id, estudio_id, semestre_id, curso_id, asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo_id, dia_semana_id ORDER BY fecha)
                                                                                                               orden_id,
                  DECODE (hor_f_fecha_entre (x.fecha, fecha_inicio, fecha_fin),
                          'S', DECODE (hor_f_fecha_entre (x.fecha, NVL (desde_el_dia, fecha_inicio),
                                                          NVL (hasta_el_dia, fecha_fin)),
                                       'S', 'S',
                                       'N'
                                      ),
                          'N'
                         ) docencia,
                  estudio_id, curso_id, semestre_id, grupo_id, tipo_subgrupo_id, subgrupo_id, dia_Semana_id,
                  asignatura_id, fecha_inicio, fecha_fin, fecha_examenes_inicio, fecha_examenes_fin, desde_el_dia,
                  hasta_el_dia, repetir_cada_semanas, numero_iteraciones, detalle_manual, tipo_dia, dia_semana
           FROM   (SELECT i.id, null estudio_id, i.curso_id, i.semestre_id, i.grupo_id, i.tipo_subgrupo_id,
                          i.subgrupo_id, i.dia_Semana_id, null asignatura_id, fecha_inicio, fecha_fin,
                          fecha_examenes_inicio, fecha_examenes_fin, i.desde_el_dia, hasta_el_dia, repetir_cada_semanas,
                          numero_iteraciones, detalle_manual, c.fecha, tipo_dia, dia_semana
                   FROM   hor_semestres_detalle s,
                          hor_items i,
                          hor_ext_calendario c
                   WHERE  i.semestre_id = s.semestre_id
                   AND    trunc (c.fecha) BETWEEN fecha_inicio AND NVL (fecha_examenes_fin, fecha_fin)
                   AND    c.dia_semana_id = i.dia_semana_id
                   AND    tipo_dia IN ('L', 'E', 'F')
                   and    vacaciones = 0
                   and    s.tipo_estudio_id = 'G'
                   AND    detalle_manual = 0) x) d,
          hor_items i
   WHERE  i.curso_id = d.curso_id
   AND    i.semestre_id = d.semestre_id
   AND    i.grupo_id = d.grupo_id
   AND    i.tipo_subgrupo_id = d.tipo_subgrupo_id
   AND    i.subgrupo_id = d.subgrupo_id
   AND    i.dia_semana_id = d.dia_semana_id
   AND    i.detalle_manual = 0
   AND    i.id = d.id
   UNION ALL
   SELECT c.id, c.fecha, 'N' docencia_paso_1, 'N' docencia_paso_2, DECODE (d.id, NULL, 'N', 'S') docencia, 1 orden_id,
          numero_iteraciones, repetir_cada_semanas, fecha_inicio, fecha_fin, estudio_id, semestre_id, curso_id,
          asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo_id, dia_semana_id, tipo_dia,
          decode (tipo_dia, 'F', 1, 0) festivos
   FROM   (SELECT i.id, c.fecha, numero_iteraciones, repetir_cada_semanas, s.fecha_inicio, s.fecha_fin, null estudio_id,
                  i.semestre_id, i.curso_id, null asignatura_id, i.grupo_id, i.tipo_subgrupo_id, i.subgrupo_id,
                  i.dia_semana_id, tipo_dia
           FROM   hor_semestres_detalle s,
                  hor_items i,
                  hor_ext_calendario c
           WHERE  i.semestre_id = s.semestre_id
           AND    trunc (c.fecha) BETWEEN fecha_inicio AND NVL (fecha_examenes_fin, fecha_fin)
           AND    c.dia_semana_id = i.dia_semana_id
           AND    tipo_dia IN ('L', 'E', 'F')
           and    s.tipo_estudio_id = 'G'
           and    vacaciones = 0
           AND    detalle_manual = 1) c,
          hor_items_detalle d
   WHERE  c.id = d.item_id(+)
   AND    trunc (c.fecha) = trunc (d.inicio(+));

  
CREATE OR REPLACE FORCE VIEW UJI_HORARIOS.HOR_V_PROFESOR_CREDITOS (ID,
                                                                   NOMBRE,
                                                                   CREDITOS_PROFESOR,
                                                                   CREDITOS_ASIGNADOS,
                                                                   CREDITOS_PENDIENTES,
                                                                   DEPARTAMENTO_ID,
                                                                   AREA_ID
                                                                  ) AS
   select   id, nombre, creditos_profesor, sum (crd_final) creditos_asignados,
            creditos_profesor - sum (crd_final) creditos_pendientes, departamento_id, area_id
   from     (select   id, nombre, creditos_profesor, grupo_id, tipo, asignatura_id, min (asignatura_txt), crd_final,
                      departamento_id, area_id
             from     hor_v_items_creditos_detalle
             group by id,
                      nombre,
                      creditos_profesor,
                      grupo_id,
                      tipo,
                      asignatura_id,
                      crd_final,
                      departamento_id,
                      area_id)
   group by id,
            nombre,
            creditos_profesor,
            departamento_id,
            area_id;
             
			 
CREATE OR REPLACE PROCEDURE UJI_HORARIOS.pod_consulta_detalle_pdf (
   name_array    IN   euji_util.ident_arr2,
   value_array   IN   euji_util.ident_arr2
) IS
   v_aux           NUMBER;
   pProfesor       number          := euji_util.euji_getparam (name_array, value_array, 'pProfesor');
   pArea           number          := euji_util.euji_getparam (name_array, value_array, 'pArea');
   pDepartamento   number          := euji_util.euji_getparam (name_array, value_array, 'pDepartamento');
   pSesion         VARCHAR2 (400)  := euji_util.euji_getparam (name_array, value_array, 'pSesion');
   v_nombre        varchar2 (2000);
   vSesion         number;
   vCurso          number;
   vCreditos       number;
   vFechas         t_horario;
   vRdo            clob;

   cursor lista_areas (p_area in number, p_prof in number) is
      select   *
      from     hor_areas
      where    departamento_id = pDepartamento
      and      (   id in (select area_id
                          from   hor_profesores
                          where  id = p_prof)
                or id = p_area
                or p_area = -1)
      order by nombre;

   cursor lista_profesores (p_area in number, p_prof in number) is
      select   id, nombre, creditos, pendiente_contratacion, creditos_maximos, creditos_reduccion, categoria_nombre
      from     hor_profesores
      where    departamento_id = pDepartamento
      and      area_id = p_area
      and      (   id = p_prof
                or p_prof = -1)
      order by nombre;

   cursor lista_items (p_prof in number, p_semestre in number, p_horario in varchar2 default 'S') is
      select distinct id, grupo_id, subgrupo_id, tipo_subgrupo, tipo_subgrupo_id, dia_semana_id, hora_inicio, hora_fin,
                      wm_concat (asignatura_id) asignatura_id, fin, inicio, creditos
      from            (select   items.id, items.grupo_id, items.tipo_subgrupo, items.tipo_subgrupo_id,
                                items.subgrupo_id, items.dia_semana_id, items.hora_inicio, items.hora_fin,
                                items.creditos,
                                rtrim (xmlagg (xmlelement (e, asi.asignatura_id || ', ')).extract
                                                                                           ('//text()'),
                                       ', ') as asignatura_id,
                                decode (p_horario,
                                        'S', to_date (dia_semana_id + 1 || '/01/2006 ' || to_char (hora_fin, 'hh24:mi'),
                                                      'dd/mm/yyyy hh24:mi')
                                       ) fin,
                                decode (p_horario,
                                        'S', to_date (dia_semana_id + 1 || '/01/2006 '
                                                      || to_char (hora_inicio, 'hh24:mi'),
                                                      'dd/mm/yyyy hh24:mi')
                                       ) inicio
                       from     uji_horarios.hor_items items,
                                uji_horarios.hor_items_asignaturas asi
                       where    items.id = asi.item_id
                       and      items.semestre_id = p_semestre
                       and      (   (    p_horario = 'S'
                                     and items.dia_semana_id is not null)
                                 or (    p_horario = 'N'
                                     and items.dia_semana_id is null)
                                )
                       and      items.id in (select item_id
                                             from   hor_items_profesores
                                             where  profesor_id = p_prof)
                       group by items.id,
                                items.grupo_id,
                                items.tipo_subgrupo,
                                items.tipo_subgrupo_id,
                                items.subgrupo_id,
                                items.dia_semana_id,
                                items.semestre_id,
                                items.hora_inicio,
                                items.hora_fin,
                                asi.asignatura_id,
                                items.creditos)
      group by        id,
                      grupo_id,
                      subgrupo_id,
                      tipo_subgrupo,
                      tipo_subgrupo_id,
                      dia_semana_id,
                      hora_inicio,
                      hora_fin,
                      fin,
                      inicio,
                      creditos;

   cursor lista_items_crd (p_prof in number, p_sem in number) is
      /*select   *
      from     (select   grupo_id, tipo_subgrupo, tipo_subgrupo_id, subgrupo_id, asignatura_id, creditos
                from     (select distinct items.grupo_id, items.tipo_subgrupo, items.tipo_subgrupo_id,
                                          items.subgrupo_id, nvl (items.comun_texto, asi.asignatura_id) asignatura_id,
                                          creditos
                          from            uji_horarios.hor_items items,
                                          uji_horarios.hor_items_asignaturas asi
                          where           items.id = asi.item_id
                          and             items.dia_semana_id is not null
                          and             items.semestre_id = p_sem
                          and             items.id in (select item_id
                                                       from   hor_items_profesores
                                                       where  profesor_id = p_prof)
                          group by        items.grupo_id,
                                          items.tipo_subgrupo,
                                          items.tipo_subgrupo_id,
                                          items.subgrupo_id,
                                          nvl (items.comun_texto, asi.asignatura_id),
                                          creditos)
                group by grupo_id,
                         tipo_subgrupo,
                         tipo_subgrupo_id,
                         subgrupo_id,
                         creditos,
                         asignatura_id)
      order by 5,
               1,
               2,
               3,
               4;*/
      select distinct grupo_id, substr (tipo, 1, 2) tipo_subgrupo_id, substr (tipo, 3) subgrupo_id,
                      asignatura_txt asignatura_id, crd_final creditos, departamento_id, area_id
      from            hor_v_items_creditos_detalle
      where           id = p_prof
      and             item_id in (select id
                                  from   hor_items
                                  where  semestre_id = p_sem)
      order by        5,
                      1,
                      2,
                      3,
                      4;

   cursor lista_solapamientos (p_prof in number, p_semestre in number) is
      select   id, min (asignatura_id) asignatura_id, grupo_id, tipo_subgrupo_id || subgrupo_id subgrupo_id,
               semestre_id, dia_semana_id,
               decode (dia_semana_id,
                       1, 'Dilluns',
                       2, 'Dimarts',
                       3, 'Dimecres',
                       4, 'Dijous',
                       5, 'Divendres',
                       6, 'Dissabte',
                       7, 'Diumenge',
                       'Error'
                      ) dia_semana,
               min (hora_inicio) inicio, max (hora_fin) fin
      from     (select   id, grupo_id, subgrupo_id, tipo_subgrupo, tipo_subgrupo_id, dia_semana_id, hora_inicio,
                         hora_fin, wm_concat (asignatura_id) asignatura_id, semestre_id,
                         count (*) over (partition by semestre_id, dia_semana_id) cuantos
                from     (select id, asignatura_id, semestre_id, grupo_id, tipo_subgrupo, tipo_subgrupo_id, subgrupo_id,
                                 dia_semana_id, hora_inicio, hora_fin
                          from   (select   items.id, items.grupo_id, items.tipo_subgrupo, items.tipo_subgrupo_id,
                                           items.subgrupo_id, items.dia_semana_id, items.hora_inicio, items.hora_fin,
                                           items.semestre_id, wm_concat (asi.asignatura_id) asignatura_id
                                  from     uji_horarios.hor_items items,
                                           uji_horarios.hor_items_asignaturas asi
                                  where    items.id = asi.item_id
                                  and      items.semestre_id = p_semestre
                                  and      items.dia_semana_id is not null
                                  and      items.id in (select item_id
                                                        from   hor_items_profesores
                                                        where  profesor_id = p_prof)
                                  group by asi.asignatura_id,
                                           items.id,
                                           items.grupo_id,
                                           items.tipo_subgrupo,
                                           items.tipo_subgrupo_id,
                                           items.subgrupo_id,
                                           items.dia_semana_id,
                                           items.semestre_id,
                                           items.hora_inicio,
                                           items.hora_fin) x
                          where  exists (
                                    select 1
                                    from   hor_items i,
                                           hor_items_profesores p
                                    where  i.id = p.item_id
                                    and    profesor_id = p_prof
                                    and    i.id <> x.id
                                    and    i.dia_semana_id = x.dia_Semana_id
                                    and    i.semestre_id = x.semestre_id
                                    and    (   to_number (to_char ((i.hora_inicio + 1 / 1440), 'hh24mi'))
                                                  between to_number (to_char (x.hora_inicio, 'hh24mi'))
                                                      and to_number (to_char (x.hora_fin, 'hh24mi'))
                                            or to_number (to_char ((i.hora_fin - 1 / 1440), 'hh24mi'))
                                                  between to_number (to_char (x.hora_inicio, 'hh24mi'))
                                                      and to_number (to_char (x.hora_fin, 'hh24mi'))
                                           )))
                group by id,
                         grupo_id,
                         subgrupo_id,
                         tipo_subgrupo,
                         tipo_subgrupo_id,
                         dia_semana_id,
                         semestre_id,
                         hora_inicio,
                         hora_fin)
      where    cuantos > 1
      group by id,
               semestre_id,
               dia_Semana_id,
               grupo_id,
               tipo_subgrupo_id || subgrupo_id
      order by 5,
               6,
               2,
               3,
               4,
               6;

   procedure cabecera is
   begin
      owa_util.mime_header (ccontent_type => 'text/xml', bclose_header => true);
      fop.documentOpen (page_width => 29.7, page_height => 21, margin_left => 1.5, margin_right => 1.5,
                        margin_top => 0.5, margin_bottom => 0.5, extent_before => 3, extent_after => 1.5,
                        extent_margin_top => 3, extent_margin_bottom => 1.5);
      fop.pageBodyOpen;
      htp.p (fof.documentheaderopen || fof.blockopen (space_after => 0.1) || fof.tableOpen
             || fof.tablecolumndefinition (column_width => 3) || fof.tablecolumndefinition (column_width => 24)
             || fof.tablebodyopen || fof.tableRowOpen || fof.tablecellopen
             || fof.block (fof.logo_uji, text_align => 'center') || fof.tablecellclose
             || fof.tablecellopen (display_align => 'center', padding => 0.4)
             || fof.block (text => v_nombre, font_size => 16, font_weight => 'bold', font_family => 'Arial',
                           text_align => 'center')
             || fof.block (text => 'POD provisional - Curs: ' || vCurso || '/' || substr (to_char (vCurso + 1), 3),
                           font_size => 16, font_weight => 'bold', font_family => 'Arial', text_align => 'center')
             || fof.tablecellclose || fof.tableRowClose || fof.tablebodyclose || fof.tableClose || fof.blockclose
             || fof.documentheaderclose);
      -- El numero paginas se escribe a mano para poner el texto m?s peque?o
      htp.p (fof.documentfooteropen || fof.tableOpen || fof.tablecolumndefinition (column_width => 18)
             || fof.tablebodyopen || fof.tableRowOpen || fof.tablecellopen
             || fof.block (text => 'Pag. <fo:page-number /> de <fo:page-number-citation ref-id="lastBlock" />',
                           text_align => 'right', font_weight => 'bold', font_size => 8)
             || fof.tablecellclose || fof.tableRowClose || fof.tablebodyclose || fof.tableClose
             || fof.documentfooterclose);
      fop.pageFlowOpen;
      fop.block (fof.white_space);
   end;

   procedure pie is
   begin
      htp.p ('<fo:block  id="lastBlock" space-before="1cm" font-size="8pt">');
      htp.p ('Data de generaci�: ' || to_char (sysdate, 'dd/mm/yyy hh24:mi'));
      fop.blockclose ();
      fop.pageFlowClose;
      fop.pageBodyClose;
      fop.documentClose;
   end;

   function espacios (p_espacios in number)
      return varchar2 is
      v_rdo   varchar2 (2000);
   begin
      v_rdo := null;

      for x in 1 .. p_espacios loop
         v_rdo := v_rdo || fof.white_space;
      end loop;

      return v_rdo;
   end;

   procedure mostrar_calendario (p_prof in number, p_semestre in number) is
      v_aux   number;
   begin
      vFechas := t_horario ();

      for item in lista_items (p_prof, p_semestre, 'S') loop
         vFechas.extend;
         vFechas (vFechas.count) :=
            ItemHorarios (hor_contenido_item (item.hora_inicio, item.hora_fin,
                                              item.asignatura_id || ' ' || item.grupo_id,
                                              '(' || item.tipo_subgrupo_id || item.subgrupo_id || ')'),
                          item.inicio, item.fin, null);
      end loop;

      if vFechas.count > 0 then
         fop.block (fof.white_space);
         fop.block (fof.white_space);
         fop.block (fof.bold ('Semestre ' || p_semestre) || '  _____________________________', font_size => 12);
         vRdo := xpfpkg_horarios_academicos.drawTimetable (vFechas, 5, null, 'CA', 'PDF_APP_HORARIOS');
         xup_utils.dump (vRdo);
         dbms_lob.freeTemporary (vRdo);
         --fop.block (fof.white_space);
         fop.block (fof.white_space);
      end if;

      fop.block (fof.white_space);
   end;

   procedure mostrar_pod (p_prof in number, p_semestre in number) is
      v_aux   number;
   begin
      begin
         v_aux := 0;

         for item in lista_items_crd (p_prof, p_semestre) loop
            v_aux := v_aux + 1;

            if v_aux = 1 then
               fop.block (fof.white_space);
               fop.block (espacios (8) || fof.bold ('Subgrups amb horari assignat al semestre ' || p_semestre),
                          font_size => 12);
            end if;

            fop.block (espacios (8) || espacios (5) || item.asignatura_id || ' ' || item.grupo_id || ' '
                       || item.tipo_subgrupo_id || item.subgrupo_id || ' - ' || item.creditos || ' crd.',
                       font_size => 10);
         end loop;
      exception
         when others then
            fop.block (sqlerrm);
      end;

      begin
         v_aux := 0;

         for item in lista_items (p_prof, p_semestre, 'N') loop
            v_aux := v_aux + 1;

            if v_aux = 1 then
               fop.block (fof.white_space);
               fop.block (espacios (8) || fof.bold ('Subgrups sense horari assignat al semestre ' || p_semestre),
                          font_size => 12);
            end if;

            fop.block (espacios (8) || espacios (5) || item.asignatura_id || ' ' || item.grupo_id || ' '
                       || item.tipo_subgrupo_id || item.subgrupo_id || ' - ' || item.creditos || ' crd.',
                       font_size => 10);
         end loop;
      exception
         when others then
            fop.block (sqlerrm);
      end;
   end;

   procedure mostrar_solapaments (p_prof in number, p_semestre in number) is
   begin
      begin
         v_aux := 0;

         for item in lista_solapamientos (p_prof, p_semestre) loop
            v_aux := v_aux + 1;

            if v_aux = 1 then
               fop.block (fof.white_space);
               fop.block (espacios (8) || fof.bold ('Solapaments al semestre ' || p_semestre), font_size => 12);
            end if;

            fop.block (espacios (8) || espacios (5) || item.dia_Semana || ' - ' || to_char (item.inicio, 'hh24:mi')
                       || '-' || to_char (item.fin, 'hh24:mi') || ' - ' || item.asignatura_id || ' ' || item.grupo_id
                       || ' ' || item.subgrupo_id,
                       font_size => 10);
         end loop;
      exception
         when others then
            fop.block (sqlerrm);
      end;
   end;

   procedure kk (p_prof in number) is
   begin
      fop.tableOpen;
      fop.tablecolumndefinition (column_width => 13);
      fop.tablecolumndefinition (column_width => 13);
      fop.tablebodyopen;
      fop.tableRowOpen;
      fop.tablecellopen (display_align => 'center');
      fop.blockOpen;
      mostrar_calendario (p_prof, 1);
      fop.blockClose;
      fop.tablecellclose;
      fop.tablecellopen (display_align => 'center');
      fop.blockOpen;
      mostrar_calendario (p_prof, 2);
      fop.blockClose;
      fop.tablecellclose;
      fop.tableRowClose;
      fop.tablebodyclose;
      fop.tableClose;
   end;
BEGIN
   select count (*) + 1
   into   vSesion
   from   apa_sesiones
   where  sesion_id = pSesion
   and    fecha < sysdate () - 0.33;

   select id
   into   vCurso
   from   hor_curso_academico;

   if vSesion > 0 then
      begin
         select nombre
         into   v_nombre
         from   hor_Departamentos
         where  id = pDepartamento;

         cabecera;

         if pArea = -1 then
            pProfesor := -1;
         end if;

         for a in lista_areas (pArea, pProfesor) loop
            fop.block ('Area de coneixement: ' || fof.bold (a.nombre), font_size => 16);
            fop.block (fof.white_space);

            for x in lista_profesores (a.id, pProfesor) loop
               begin
                  select nvl (sum (creditos), 0)
                  into   vCreditos
                  from   (select   grupo_id, tipo_subgrupo, tipo_subgrupo_id, subgrupo_id,
                                   wm_concat (asignatura_id) asignatura_id, creditos
                          from     (select distinct items.grupo_id, items.tipo_subgrupo, items.tipo_subgrupo_id,
                                                    items.subgrupo_id, asi.asignatura_id, creditos
                                    from            uji_horarios.hor_items items,
                                                    uji_horarios.hor_items_asignaturas asi
                                    where           items.id = asi.item_id
                                    --and             items.dia_semana_id is not null
                                    and             items.id in (select item_id
                                                                 from   hor_items_profesores
                                                                 where  profesor_id = x.id)
                                    group by        items.grupo_id,
                                                    items.tipo_subgrupo,
                                                    items.tipo_subgrupo_id,
                                                    items.subgrupo_id,
                                                    asi.asignatura_id,
                                                    creditos)
                          group by grupo_id,
                                   tipo_subgrupo,
                                   tipo_subgrupo_id,
                                   subgrupo_id,
                                   creditos);

                  select nvl (sum (creditos), 0)
                  into   vCreditos
                  from   (select distinct d.id, nvl (comun_Texto, d.asignatura_id) asignatura_id, d.grupo_id,
                                          tipo_subgrupo_id, subgrupo_id, crd_final creditos
                          from            hor_v_items_creditos_detalle d,
                                          hor_items i,
                                          hor_items_asignaturas a
                          where           d.id = x.id
                          and             d.item_id = i.id
                          and             i.id = a.item_id);
               exception
                  when others then
                     vCreditos := 0;
               end;

               fop.block (espacios (3) || fof.bold (x.nombre) || ' - ' || x.categoria_nombre, font_size => 14);
               fop.block (espacios (6) || 'Dedicaci� inicial ' || x.creditos_maximos || ' crd' || espacios (4)
                          || 'Reducci� ' || x.creditos_Reduccion || ' crd' || espacios (4) || 'Dedicaci� neta '
                          || fof.bold (x.creditos) || ' crd' || espacios (4) || 'Cr�dits a impartir ' || vCreditos
                          || ' crd' || espacios (4) || 'Cr�dits pendents d''assignar '
                          || fof.bold (x.creditos - vCreditos) || ' crd.',
                          font_size => 10);

               for semestre in 1 .. 2 loop
                  begin
                     mostrar_calendario (x.id, semestre);
                     mostrar_pod (x.id, semestre);
                     mostrar_solapaments (x.id, semestre);
                  exception
                     when others then
                        fop.block (sqlerrm);
                  end;
               end loop;
            --kk (x.id);
            end loop;
         end loop;

         pie;
      exception
         when others then
            cabecera;
            fop.block ('Error en el departament. ' || sqlerrm, font_weight => 'bold', text_align => 'center',
                       font_size => 14, space_after => 0.5);
            pie;
      end;
   else
      cabecera;
      fop.block ('No tens perm�s per accedir a aquest document.', font_weight => 'bold', text_align => 'center',
                 font_size => 14, space_after => 0.5);
      pie;
   end if;
END;


CREATE OR REPLACE PROCEDURE UJI_HORARIOS.pod_consulta_pdf (
   name_array    IN   euji_util.ident_arr2,
   value_array   IN   euji_util.ident_arr2
) IS
   v_aux           NUMBER;
   -- pProfesor       number          := euji_util.euji_getparam (name_array, value_array, 'pProfesor');
   pDepartamento   number          := euji_util.euji_getparam (name_array, value_array, 'pDepartamento');
   pSesion         VARCHAR2 (400)  := euji_util.euji_getparam (name_array, value_array, 'pSesion');
   v_nombre        varchar2 (2000);
   vSesion         number;
   vCurso          number;
   vCreditos       number;
   vAcum           number;
   vAcum2          number;
   vAcum3          number;
   vAcum4          number;
   vAcum5          number;
   vDepAcum        number;
   vDepAcum2       number;
   vDepAcum3       number;
   vDepAcum4       number;
   vDepAcum5       number;
   vFechas         t_horario;
   vRdo            clob;

   cursor lista_areas is
      select   *
      from     hor_areas
      where    departamento_id = pDepartamento
      order by nombre;

   cursor lista_profesores (p_area in number) is
      select   id, nombre, nvl (creditos, 0) creditos, pendiente_contratacion,
               nvl (creditos_maximos, 0) creditos_maximos, nvl (creditos_reduccion, 0) creditos_Reduccion,
               categoria_nombre
      from     hor_profesores
      where    departamento_id = pDepartamento
      and      area_id = p_area
      order by nombre,
               categoria_nombre,
               nvl (creditos_maximos, 0) desc,
               nombre;

   function formatear (p_entrada in number)
      return varchar2 is
      v_rdo   varchar2 (100);
   begin
      v_rdo := replace (to_char (p_entrada, '9990.00'), '.', ',');
      return (v_rdo);
   end;

   procedure cabecera is
   begin
      owa_util.mime_header (ccontent_type => 'text/xml', bclose_header => true);
      fop.documentOpen (page_width => 29.7, page_height => 21, margin_left => 1.5, margin_right => 1.5,
                        margin_top => 0.5, margin_bottom => 0.5, extent_before => 3, extent_after => 1.5,
                        extent_margin_top => 3, extent_margin_bottom => 1.5);
      fop.pageBodyOpen;
      htp.p (fof.documentheaderopen || fof.blockopen (space_after => 0.1) || fof.tableOpen
             || fof.tablecolumndefinition (column_width => 3) || fof.tablecolumndefinition (column_width => 24)
             || fof.tablebodyopen || fof.tableRowOpen || fof.tablecellopen
             || fof.block (fof.logo_uji, text_align => 'center') || fof.tablecellclose
             || fof.tablecellopen (display_align => 'center', padding => 0.4)
             || fof.block (text => v_nombre, font_size => 16, font_weight => 'bold', font_family => 'Arial',
                           text_align => 'center')
             || fof.block (text => 'Informe provisional POD - Curs: ' || vCurso || '/'
                            || substr (to_char (vCurso + 1), 3),
                           font_size => 16, font_weight => 'bold', font_family => 'Arial', text_align => 'center')
             || fof.tablecellclose || fof.tableRowClose || fof.tablebodyclose || fof.tableClose || fof.blockclose
             || fof.documentheaderclose);
      -- El numero paginas se escribe a mano para poner el texto m?s peque?o
      htp.p (fof.documentfooteropen || fof.tableOpen || fof.tablecolumndefinition (column_width => 18)
             || fof.tablebodyopen || fof.tableRowOpen || fof.tablecellopen
             || fof.block (text => 'Pag. <fo:page-number /> de <fo:page-number-citation ref-id="lastBlock" />',
                           text_align => 'right', font_weight => 'bold', font_size => 8)
             || fof.tablecellclose || fof.tableRowClose || fof.tablebodyclose || fof.tableClose
             || fof.documentfooterclose);
      fop.pageFlowOpen;
      fop.block (fof.white_space);
   end;

   procedure pie is
   begin
      htp.p ('<fo:block  id="lastBlock" space-before="1cm" font-size="8pt">');
      htp.p ('Data de generaci�: ' || to_char (sysdate, 'dd/mm/yyy hh24:mi'));
      fop.blockclose ();
      fop.pageFlowClose;
      fop.pageBodyClose;
      fop.documentClose;
   end;

   function espacios (p_espacios in number)
      return varchar2 is
      v_rdo   varchar2 (2000);
   begin
      v_rdo := null;

      for x in 1 .. p_espacios loop
         v_rdo := v_rdo || fof.white_space;
      end loop;

      return v_rdo;
   end;
BEGIN
   select count (*) + 1
   into   vSesion
   from   apa_sesiones
   where  sesion_id = pSesion
   and    fecha < sysdate () - 0.33;

   select id
   into   vCurso
   from   hor_curso_academico;

   if vSesion > 0 then
      select nombre
      into   v_nombre
      from   hor_Departamentos
      where  id = pDepartamento;

      cabecera;
      vDepAcum := 0;
      vDepacum2 := 0;
      vDepacum3 := 0;
      vDepacum4 := 0;
      vDepacum5 := 0;

      for a in lista_areas loop
         fop.block ('Area de coneixement: ' || fof.bold (a.nombre), font_size => 16);
         -- fop.block (fof.white_space);
         fop.tableOpen;
         fop.tablecolumndefinition (column_width => 6.5);
         fop.tablecolumndefinition (column_width => 7);
         fop.tablecolumndefinition (column_width => 2);
         fop.tablecolumndefinition (column_width => 2);
         fop.tablecolumndefinition (column_width => 2);
         fop.tablecolumndefinition (column_width => 2);
         fop.tablecolumndefinition (column_width => 2);
         fop.tablecolumndefinition (column_width => 2);
         fop.tablecolumndefinition (column_width => 2);
         fop.tablebodyopen;
         fop.tableRowOpen;
         fop.tablecellopen;
         fop.block (fof.bold ('Profesor/a'), font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold ('Categoria'), font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold ('Dedicaci� inicial'), font_size => 10, text_align => 'right');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold ('Reducci�'), font_size => 10, text_align => 'right');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold ('Dedicaci� neta'), font_size => 10, text_align => 'right');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold ('Cr�dits assignats'), font_size => 10, text_align => 'right');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold ('% assig- naci�'), font_size => 10, text_align => 'right');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold ('Cr�dits per assignar'), font_size => 10, text_align => 'right');
         fop.tablecellclose;
         fop.tableRowClose;
         vacum := 0;
         vacum2 := 0;
         vacum3 := 0;
         vacum4 := 0;
         vacum5 := 0;

         for x in lista_profesores (a.id) loop
            begin
               select nvl (sum (creditos), 0)
               into   vCreditos
               from   (select   grupo_id, tipo_subgrupo, tipo_subgrupo_id, subgrupo_id,
                                wm_concat (asignatura_id) asignatura_id, creditos
                       from     (select distinct items.grupo_id, items.tipo_subgrupo, items.tipo_subgrupo_id,
                                                 items.subgrupo_id, asi.asignatura_id, creditos
                                 from            uji_horarios.hor_items items,
                                                 uji_horarios.hor_items_asignaturas asi
                                 where           items.id = asi.item_id
                                 --and             items.dia_semana_id is not null
                                 and             items.id in (select item_id
                                                              from   hor_items_profesores
                                                              where  profesor_id = x.id)
                                 group by        items.grupo_id,
                                                 items.tipo_subgrupo,
                                                 items.tipo_subgrupo_id,
                                                 items.subgrupo_id,
                                                 asi.asignatura_id,
                                                 creditos)
                       group by grupo_id,
                                tipo_subgrupo,
                                tipo_subgrupo_id,
                                subgrupo_id,
                                creditos);

               select nvl (sum (creditos), 0)
               into   vCreditos
               from   (select distinct d.id, nvl (comun_Texto, d.asignatura_id) asignatura_id, d.grupo_id,
                                       tipo_subgrupo_id, subgrupo_id, crd_final creditos
                       from            hor_v_items_creditos_detalle d,
                                       hor_items i,
                                       hor_items_asignaturas a
                       where           d.id = x.id
                       and             d.item_id = i.id
                       and             i.id = a.item_id);
            exception
               when others then
                  vCreditos := 0;
            end;

            fop.tableRowOpen;
            fop.tablecellopen;
            fop.block (x.nombre, font_size => 10);
            fop.tablecellclose;
            fop.tablecellopen;
            fop.block (x.categoria_nombre, font_size => 10);
            fop.tablecellclose;
            fop.tablecellopen;
            fop.block (formatear (x.creditos_maximos), font_size => 10, text_align => 'right');
            fop.tablecellclose;
            fop.tablecellopen;
            fop.block (formatear (x.creditos_reduccion), font_size => 10, text_align => 'right');
            fop.tablecellclose;
            fop.tablecellopen;
            fop.block (formatear (x.creditos), font_size => 10, text_align => 'right');
            fop.tablecellclose;
            fop.tablecellopen;
            fop.block (formatear (vCreditos), font_size => 10, text_align => 'right');
            fop.tablecellclose;
            fop.tablecellopen;

            if x.creditos <> 0 then
               fop.block (formatear (round (vCreditos / x.creditos * 100, 2)) || '%', font_size => 10,
                          text_align => 'right');
            else
               fop.block ('0,00%', font_size => 10, text_align => 'right');
            end if;

            fop.tablecellclose;
            fop.tablecellopen;
            fop.block (formatear (x.creditos - vCreditos), font_size => 10, text_align => 'right');
            fop.tablecellclose;

            if (x.creditos - vCreditos) < 0 then
               fop.tablecellopen;
               fop.block (fof.bold ('> 100% dedicaci�'), font_size => 10, text_align => 'center');
               fop.tablecellclose;
            end if;

            fop.tableRowClose;
            vacum := vacum + x.creditos;
            vacum2 := vacum2 + vCreditos;
            vacum3 := vacum3 + (x.creditos - vCreditos);
            vacum4 := vacum4 + x.creditos_maximos;
            vacum5 := vacum5 + x.creditos_reduccion;
            vDepacum := vDepacum + x.creditos;
            vDepacum2 := vDepacum2 + vCreditos;
            vDepacum3 := vDepacum3 + (x.creditos - vCreditos);
            vDepacum4 := vDepacum4 + x.creditos_maximos;
            vDepacum5 := vDepacum5 + x.creditos_reduccion;
         end loop;

         fop.tableRowOpen;
         fop.tablecellopen;
         fop.block (fof.white_space, font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.white_space, font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold (formatear (vAcum4)), font_size => 10, text_align => 'right');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold (formatear (vAcum5)), font_size => 10, text_align => 'right');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold (formatear (vAcum)), font_size => 10, text_align => 'right');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold (formatear (vAcum2)), font_size => 10, text_align => 'right');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold (formatear (round (vAcum2 / vAcum * 100, 2))) || '%', font_size => 10,
                    text_align => 'right');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold (formatear (vAcum3)), font_size => 10, text_align => 'right');
         fop.tablecellclose;
         fop.tableRowClose;
         fop.tablebodyclose;
         fop.tableClose;
         fop.block (fof.white_space);
         fop.block (fof.white_space);
         fop.block (fof.white_space);
      end loop;

      fop.tableOpen;
      fop.tablecolumndefinition (column_width => 6.5);
      fop.tablecolumndefinition (column_width => 7);
      fop.tablecolumndefinition (column_width => 2);
      fop.tablecolumndefinition (column_width => 2);
      fop.tablecolumndefinition (column_width => 2);
      fop.tablecolumndefinition (column_width => 2);
      fop.tablecolumndefinition (column_width => 2);
      fop.tablecolumndefinition (column_width => 2);
      fop.tablecolumndefinition (column_width => 2);
      fop.tablebodyopen;
      fop.tableRowOpen;
      fop.tablecellopen;
      fop.block (fof.bold ('Total del departament'), font_size => 14);
      fop.tablecellclose;
      fop.tablecellopen;
      fop.block (fof.white_space, font_size => 14);
      fop.tablecellclose;
      fop.tablecellopen;
      fop.block (fof.bold (formatear (vDepAcum4)), font_size => 14, text_align => 'right');
      fop.tablecellclose;
      fop.tablecellopen;
      fop.block (fof.bold (formatear (vDepAcum5)), font_size => 14, text_align => 'right');
      fop.tablecellclose;
      fop.tablecellopen;
      fop.block (fof.bold (formatear (vDepAcum)), font_size => 14, text_align => 'right');
      fop.tablecellclose;
      fop.tablecellopen;
      fop.block (fof.bold (formatear (vDepAcum2)), font_size => 14, text_align => 'right');
      fop.tablecellclose;
      fop.tablecellopen;
      fop.block (fof.bold (formatear (round (vDepAcum2 / vDepAcum * 100, 2))) || '%', font_size => 14,
                 text_align => 'right');
      fop.tablecellclose;
      fop.tablecellopen;
      fop.block (fof.bold (formatear (vDepAcum3)), font_size => 14, text_align => 'right');
      fop.tablecellclose;
      fop.tableRowClose;
      fop.tablebodyclose;
      fop.tableClose;
      pie;
   else
      cabecera;
      fop.block ('No tens perm�s per accedir a aquest document.', font_weight => 'bold', text_align => 'center',
                 font_size => 14, space_after => 0.5);
      pie;
   end if;
END;





CREATE OR REPLACE PROCEDURE UJI_HORARIOS.pod_validacion_pdf (
   name_array    IN   euji_util.ident_arr2,
   value_array   IN   euji_util.ident_arr2
) IS
   v_aux           NUMBER;
   pDepartamento   number          := euji_util.euji_getparam (name_array, value_array, 'pDepartamento');
   pSesion         VARCHAR2 (400)  := euji_util.euji_getparam (name_array, value_array, 'pSesion');
   v_nombre        varchar2 (2000);
   vSesion         number;
   vCurso          number;
   pProfesor       number;

   cursor lista_areas (p_departamento in number) is
      select   *
      from     hor_areas
      where    departamento_id = p_Departamento
      order by nombre;

   cursor lista_profesores (p_departamento in number, p_area in number) is
      select   p.id, p.nombre, creditos, pendiente_contratacion, creditos_maximos, creditos_reduccion, categoria_nombre,
               a.nombre nombre_area
      from     hor_profesores p,
               hor_areas a
      where    p.departamento_id = p_Departamento
      and      area_id = p_area
      and      area_id = a.id
      order by nombre;

   procedure cabecera is
   begin
      begin
         select nombre
         into   v_nombre
         from   hor_Departamentos
         where  id = pDepartamento;
      exception
         when no_data_found then
            v_nombre := 'Desconegut';
         when others then
            v_nombre := sqlerrm;
      end;

      owa_util.mime_header (ccontent_type => 'text/xml', bclose_header => true);
      fop.documentOpen (margin_left => 1.5, margin_right => 1.5, margin_top => 0.5, margin_bottom => 0.5,
                        extent_before => 3, extent_after => 1.5, extent_margin_top => 3, extent_margin_bottom => 1.5);
      fop.pageBodyOpen;
      htp.p (fof.documentheaderopen || fof.blockopen (space_after => 0.1) || fof.tableOpen
             || fof.tablecolumndefinition (column_width => 3) || fof.tablecolumndefinition (column_width => 15)
             || fof.tablebodyopen || fof.tableRowOpen || fof.tablecellopen
             || fof.block (fof.logo_uji, text_align => 'center') || fof.tablecellclose
             || fof.tablecellopen (display_align => 'center', padding => 0.4)
             || fof.block (text => v_nombre, font_size => 16, font_weight => 'bold', font_family => 'Arial',
                           text_align => 'center')
             || fof.block (text => 'Validaci� POD - Curs: ' || vCurso || '/' || substr (to_char (vCurso + 1), 3),
                           font_size => 16, font_weight => 'bold', font_family => 'Arial', text_align => 'center')
             || fof.tablecellclose || fof.tableRowClose || fof.tablebodyclose || fof.tableClose || fof.blockclose
             || fof.documentheaderclose);
      -- El numero paginas se escribe a mano para poner el texto m?s peque?o
      htp.p (fof.documentfooteropen || fof.tableOpen || fof.tablecolumndefinition (column_width => 18)
             || fof.tablebodyopen || fof.tableRowOpen || fof.tablecellopen
             || fof.block (text => 'Pag. <fo:page-number /> de <fo:page-number-citation ref-id="lastBlock" />',
                           text_align => 'right', font_weight => 'bold', font_size => 8)
             || fof.tablecellclose || fof.tableRowClose || fof.tablebodyclose || fof.tableClose
             || fof.documentfooterclose);
      fop.pageFlowOpen;
      fop.block (fof.white_space);
   end;

   procedure pie is
   begin
      htp.p ('<fo:block  id="lastBlock" space-before="1cm" font-size="8pt">');
      htp.p ('Data de generaci�: ' || to_char (sysdate, 'dd/mm/yyy hh24:mi'));
      fop.blockclose ();
      fop.pageFlowClose;
      fop.pageBodyClose;
      fop.documentClose;
   end;

   function italic (p_entrada in varchar2)
      return varchar2 is
      v_rdo   varchar2 (2000);
   begin
      v_rdo := '<fo:inline font-style="italic">' || p_entrada || '</fo:inline>';
      return (v_rdo);
   end;

   function espacios (p_espacios in number)
      return varchar2 is
      v_rdo   varchar2 (2000);
   begin
      v_rdo := null;

      for x in 1 .. p_espacios loop
         v_rdo := v_rdo || fof.white_space;
      end loop;

      return v_rdo;
   end;

   function formatear (p_entrada in number)
      return varchar2 is
      v_rdo   varchar2 (100);
   begin
      v_rdo := replace (to_char (p_entrada, '9990.00'), '.', ',');
      return (v_rdo);
   end;

   procedure control_1 (p_dep in number, p_area in number) is
      vCreditos    number;
      v_aux        number;
      v_mostrada   number;
      v_nombre     varchar2 (2000);

      cursor lista_control_1 (p_dep in number, p_area in number) is
         select   id, nombre, creditos_maximos, creditos_Reduccion, creditos, creditos_asignados,
                  creditos - creditos_asignados creditos_pendientes, categoria_nombre
         from     (select p.id, p.nombre, nvl (creditos_maximos, 0) creditos_maximos,
                          nvl (creditos_reduccion, 0) creditos_reduccion, nvl (creditos, 0) creditos,
                          (select nvl (sum (nvl (ip.creditos, i.creditos)), 0) creditos
                           from   hor_items_profesores ip,
                                  hor_items i
                           where  profesor_id = p.id
                           and    ip.item_id = i.id) creditos_asignados, categoria_nombre
                   from   hor_profesores p
                   where  p.departamento_id = p_dep
                   and    area_id = p_area)
         order by nombre;
   begin
      select nombre
      into   v_nombre
      from   hor_areas
      where  id = p_area;

      v_aux := 0;
      v_mostrada := 0;

      for x in lista_control_1 (p_dep, p_area) loop
         begin
            select nvl (sum (creditos), 0)
            into   vCreditos
            from   (select   grupo_id, tipo_subgrupo, tipo_subgrupo_id, wm_concat (asignatura_id) asignatura_id,
                             creditos
                    from     (select   items.id, items.grupo_id, items.tipo_subgrupo, items.tipo_subgrupo_id,
                                       items.subgrupo_id, items.dia_semana_id, items.hora_inicio, items.hora_fin,
                                       rtrim
                                          (xmlagg (xmlelement (e, asi.asignatura_id || ', ')).extract
                                                                                           ('//text()'),
                                           ', ') as asignatura_id,
                                       to_date (dia_semana_id + 1 || '/01/2006 ' || to_char (hora_fin, 'hh24:mi'),
                                                'dd/mm/yyyy hh24:mi') fin,
                                       to_date (dia_semana_id + 1 || '/01/2006 '
                                                || to_char (hora_inicio, 'hh24:mi'),
                                                'dd/mm/yyyy hh24:mi') inicio,
                                       creditos
                              from     uji_horarios.hor_items items,
                                       uji_horarios.hor_items_asignaturas asi
                              where    items.id = asi.item_id
                              and      items.id in (select item_id
                                                    from   hor_items_profesores
                                                    where  profesor_id = x.id)
                              group by items.id,
                                       items.grupo_id,
                                       items.tipo_subgrupo,
                                       items.tipo_subgrupo_id,
                                       items.subgrupo_id,
                                       items.dia_semana_id,
                                       items.semestre_id,
                                       items.hora_inicio,
                                       items.hora_fin,
                                       asi.asignatura_id,
                                       creditos)
                    group by grupo_id,
                             tipo_subgrupo,
                             tipo_subgrupo_id,
                             creditos);

            select creditos_asignados
            into   vCreditos
            from   hor_v_profesor_creditos
            where  id = x.id;

            if (x.creditos - vCreditos) < 0 then
               v_aux := v_aux + 1;
            end if;
         exception
            when others then
               vCreditos := 0;
         end;

         if     v_aux = 1
            and v_mostrada = 0 then
            fop.block (espacios (2) || fof.bold (v_nombre), font_size => 12, space_before => 0.1);
            v_mostrada := 1;
         end if;

         if (x.creditos - vCreditos) < 0 then
            fop.block (espacios (5) || x.categoria_nombre || ' - ' || x.nombre || ': '
                       || fof.bold (formatear (x.creditos - vCreditos)),
                       font_size => 10);
         end if;
      end loop;
   end;

   procedure control_2 (p_dep in number, p_area in number) is
      v_aux      number;
      v_nombre   varchar2 (1000);

      cursor lista_solapamientos (p_prof in number, p_semestre in number) is
         select   id, min (asignatura_id) asignatura_id, grupo_id, tipo_subgrupo_id || subgrupo_id subgrupo_id,
                  semestre_id, dia_semana_id,
                  decode (dia_semana_id,
                          1, 'Dilluns',
                          2, 'Dimarts',
                          3, 'Dimecres',
                          4, 'Dijous',
                          5, 'Divendres',
                          6, 'Dissabte',
                          7, 'Diumenge',
                          'Error'
                         ) dia_semana,
                  min (hora_inicio) inicio, max (hora_fin) fin
         from     (select   id, grupo_id, subgrupo_id, tipo_subgrupo, tipo_subgrupo_id, dia_semana_id, hora_inicio,
                            hora_fin, wm_concat (asignatura_id) asignatura_id, semestre_id,
                            count (*) over (partition by semestre_id, dia_semana_id) cuantos
                   from     (select id, asignatura_id, semestre_id, grupo_id, tipo_subgrupo, tipo_subgrupo_id,
                                    subgrupo_id, dia_semana_id, hora_inicio, hora_fin
                             from   (select   items.id, items.grupo_id, items.tipo_subgrupo, items.tipo_subgrupo_id,
                                              items.subgrupo_id, items.dia_semana_id, items.hora_inicio, items.hora_fin,
                                              items.semestre_id, wm_concat (asi.asignatura_id) asignatura_id
                                     from     uji_horarios.hor_items items,
                                              uji_horarios.hor_items_asignaturas asi
                                     where    items.id = asi.item_id
                                     and      items.semestre_id = p_semestre
                                     and      items.dia_semana_id is not null
                                     and      items.id in (select item_id
                                                           from   hor_items_profesores
                                                           where  profesor_id = p_prof)
                                     group by asi.asignatura_id,
                                              items.id,
                                              items.grupo_id,
                                              items.tipo_subgrupo,
                                              items.tipo_subgrupo_id,
                                              items.subgrupo_id,
                                              items.dia_semana_id,
                                              items.semestre_id,
                                              items.hora_inicio,
                                              items.hora_fin) x
                             where  exists (
                                       select 1
                                       from   hor_items i,
                                              hor_items_profesores p
                                       where  i.id = p.item_id
                                       and    profesor_id = p_prof
                                       and    i.id <> x.id
                                       and    i.dia_semana_id = x.dia_Semana_id
                                       and    i.semestre_id = x.semestre_id
                                       and    (   to_number (to_char ((i.hora_inicio + 1 / 1440), 'hh24mi'))
                                                     between to_number (to_char (x.hora_inicio, 'hh24mi'))
                                                         and to_number (to_char (x.hora_fin, 'hh24mi'))
                                               or to_number (to_char ((i.hora_fin - 1 / 1440), 'hh24mi'))
                                                     between to_number (to_char (x.hora_inicio, 'hh24mi'))
                                                         and to_number (to_char (x.hora_fin, 'hh24mi'))
                                              )))
                   group by id,
                            grupo_id,
                            subgrupo_id,
                            tipo_subgrupo,
                            tipo_subgrupo_id,
                            dia_semana_id,
                            semestre_id,
                            hora_inicio,
                            hora_fin)
         where    cuantos > 1
         group by id,
                  semestre_id,
                  dia_Semana_id,
                  grupo_id,
                  tipo_subgrupo_id || subgrupo_id
         order by 5,
                  6,
                  2,
                  3,
                  4,
                  6;
   begin
      v_nombre := 'cap';

      for p in lista_profesores (p_dep, p_Area) loop
         for p_Sem in 1 .. 2 loop
            begin
               v_aux := 0;

               for item in lista_solapamientos (p.id, p_sem) loop
                  v_aux := v_aux + 1;

                  if v_aux = 1 then
                     if p.nombre_area <> v_nombre then
                        v_nombre := p.nombre_area;
                        fop.block (espacios (3) || fof.bold (p.nombre_area), font_size => 12, space_after => 0.2,
                                   space_before => 0.2);
                     end if;

                     fop.block (espacios (6) || fof.bold ('Semestre ' || p_sem || ' de ' || p.nombre), font_size => 10);
                  end if;

                  fop.block (espacios (9) || item.dia_semana || ' - ' || to_char (item.inicio, 'hh24:mi') || '-'
                             || to_char (item.fin, 'hh24:mi') || ' - ' || item.asignatura_id || ' - ' || item.grupo_id
                             || ' ' || item.subgrupo_id,
                             font_size => 10);
               end loop;

               if v_aux > 0 then
                  fop.block (fof.white_space);
               end if;
            exception
               when others then
                  fop.block (sqlerrm);
            end;
         end loop;
      end loop;
   end;

   procedure control_3 (p_dep in number) is
      v_control     varchar2 (200);
      v_crd_final   number;
      v_total       number;

      cursor lista_Descuadres (p_dep in number) is
         select   id, nombre, asignatura_id, grupo_id, tipo, crd_final, crd_item, total, nombre_area
         from     (select id, nombre, asignatura_id, grupo_id, tipo, crd_final, crd_item, nombre_area,
                          sum (crd_final) over (partition by asignatura_id, grupo_id, tipo) total
                   from   (select distinct d.id, d.nombre, asignatura_id, grupo_id, tipo, crd_final, crd_item,
                                           a.nombre nombre_area
                           from            hor_v_items_creditos_detalle d,
                                           hor_areas a
                           where           d.departamento_id = p_dep
                           and             area_id = a.id))
         where    crd_item <> total
         and      asignatura_id in (select asignatura_id
                                    from   hor_v_asignaturas_area aa,
                                           hor_Areas a
                                    where  area_id = a.id
                                    and    departamento_id = p_dep
                                    and    porcentaje > 0)
         order by asignatura_id,
                  grupo_id,
                  tipo,
                  nombre;
   begin
      v_aux := 0;
      v_control := 'x';

      for p in lista_descuadres (p_dep) loop
         v_aux := v_aux + 1;

         if v_aux = 1 then
            fop.tableOpen;
            fop.tablecolumndefinition (column_width => 2);
            fop.tablecolumndefinition (column_width => 2);
            fop.tablecolumndefinition (column_width => 7);
            fop.tablecolumndefinition (column_width => 2);
            fop.tablecolumndefinition (column_width => 2);
            fop.tablecolumndefinition (column_width => 2);
            fop.tablebodyopen;
            fop.tableRowOpen;
            fop.tablecellopen;
            fop.block (fof.white_space, font_size => 10);
            fop.tablecellclose;
            fop.tablecellopen;
            fop.block (fof.bold ('Crd Grup'), font_size => 10, text_align => 'center');
            fop.tablecellclose;
            fop.tablecellopen;
            fop.block (fof.white_space, font_size => 10);
            fop.tablecellclose;
            fop.tablecellopen;
            fop.block (fof.bold ('Grup'), font_size => 10, text_align => 'center');
            fop.tablecellclose;
            fop.tablecellopen;
            fop.block (fof.bold ('Crd Prof'), font_size => 10, text_align => 'right');
            fop.tablecellclose;
            fop.tablecellopen;
            fop.block (fof.bold ('Difer�ncia'), font_size => 10, text_align => 'right');
            fop.tablecellclose;
            fop.tableRowClose;
         else
            if v_control <> p.asignatura_id || ' ' || p.grupo_id || ' ' || p.tipo then
               fop.tableRowOpen;
               fop.tablecellopen;
               fop.block (fof.white_space, font_size => 10);
               fop.tablecellclose;
               fop.tablecellopen;
               fop.block (fof.white_space, font_size => 10);
               fop.tablecellclose;
               fop.tablecellopen;
               fop.block (fof.white_space, font_size => 10);
               fop.tablecellclose;
               fop.tablecellopen;
               fop.block (fof.white_space, font_size => 10);
               fop.tablecellclose;
               fop.tablecellopen;
               fop.block (formatear (v_total), font_size => 10, text_align => 'right');
               fop.tablecellclose;
               fop.tablecellopen;
               fop.block (fof.bold (formatear (v_total - v_crd_final)), font_size => 14, text_align => 'right');
               fop.tablecellclose;
               fop.tableRowClose;
               fop.tableRowOpen;
               fop.tablecellopen;
               fop.block (fof.white_space, font_size => 10);
               fop.tablecellclose;
               fop.tableRowClose;
            end if;
         end if;

         fop.tableRowOpen;
         fop.tablecellopen;
         fop.block (p.asignatura_id, font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (formatear (p.crd_item), font_size => 10, text_align => 'center');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (p.nombre || ' (' || italic (p.nombre_area) || ')', font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (p.grupo_id || ' - ' || p.tipo, font_size => 10, text_align => 'center');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (formatear (p.crd_final), font_size => 10, text_align => 'right');
         fop.tablecellclose;
         fop.tableRowClose;
         v_control := p.asignatura_id || ' ' || p.grupo_id || ' ' || p.tipo;
         v_crd_final := p.crd_item;
         v_total := p.total;
      end loop;

      if v_aux >= 1 then
         fop.tableRowOpen;
         fop.tablecellopen;
         fop.block (fof.white_space, font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.white_space, font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.white_space, font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.white_space, font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (formatear (v_total), font_size => 10, text_align => 'right');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold (formatear (nvl (v_total, 0) - nvl (v_crd_final, 0))), font_size => 14,
                    text_align => 'right');
         fop.tablecellclose;
         fop.tableRowClose;
      end if;

      if v_aux > 0 then
         fop.tablebodyclose;
         fop.tableClose;
      end if;
   end;

   procedure control_4 (p_dep in number) is
      v_total_area   number;
      v_area         number;
      v_acum1        number;
      v_acum2        number;
   begin
      fop.tableOpen;
      fop.tablecolumndefinition (column_width => 9);
      fop.tablecolumndefinition (column_width => 2);
      fop.tablecolumndefinition (column_width => 2);
      fop.tablecolumndefinition (column_width => 2);
      fop.tablecolumndefinition (column_width => 3);
      fop.tablebodyopen;
      fop.tableRowOpen;
      fop.tablecellopen;
      fop.block (fof.bold ('�rees de coneixement'), font_size => 10);
      fop.tablecellclose;
      fop.tablecellopen;
      fop.block (fof.bold ('Cr�dits a impartir'), font_size => 10, text_align => 'right');
      fop.tablecellclose;
      fop.tablecellopen;
      fop.block (fof.bold ('Cr�dits assignats'), font_size => 10, text_align => 'right');
      fop.tablecellclose;
      fop.tablecellopen;
      fop.block (fof.bold ('% assig- naci�'), font_size => 10, text_align => 'right');
      fop.tablecellclose;
      fop.tableRowClose;
      v_acum1 := 0;
      v_acum2 := 0;

      for x in lista_areas (p_dep) loop
         select nvl (sum (creditos * porcentaje / 100), 0) crd_total_area
         into   v_total_area
         from   (select distinct asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo_id, creditos, porcentaje
                 from            hor_items i,
                                 hor_items_asignaturas a,
                                 hor_ext_asignaturas_Area aa
                 where           i.id = a.item_id
                 and             asignatura_id = asi_id
                 and             uest_id = x.id);

         select nvl (sum (creditos), 0)
         into   v_Area
         from   (select distinct d.profesor_id, nvl (comun_Texto, asignatura_id) asignatura_id, grupo_id,
                                 tipo_subgrupo_id, subgrupo_id, uest_id area_id,
                                 round (nvl (creditos_detalle, d.creditos) * porcentaje / 100, 2) creditos
                 from            hor_v_items_prof_detalle d,
                                 hor_items i,
                                 hor_items_asignaturas a,
                                 hor_ext_asignaturas_area aa
                 where           d.item_id = i.id
                 and             i.id = a.item_id
                 and             asignatura_id = asi_id
                 and             uest_id = x.id);

         fop.tableRowOpen;
         fop.tablecellopen;
         fop.block (x.nombre, font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (formatear (v_total_area), font_size => 10, text_align => 'right');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (formatear (v_area), font_size => 10, text_align => 'right');
         fop.tablecellclose;
         fop.tablecellopen;

         if v_total_area <> 0 then
            fop.block (round (v_area / v_total_area * 100, 0) || '%', font_size => 10, text_align => 'right');
         else
            fop.block ('0%', font_size => 10, text_align => 'right');
         end if;

         fop.tablecellclose;

         if v_area > v_total_area then
            fop.tablecellopen;
            fop.block (fof.bold ('Exc�s de cr�dits'), font_size => 10, text_align => 'right');
            fop.tablecellclose;
         end if;

         fop.tableRowClose;
         v_acum1 := v_acum1 + v_total_Area;
         v_acum2 := v_acum2 + v_area;
      end loop;

      fop.tableRowOpen;
      fop.tablecellopen;
      fop.block (fof.white_space, font_size => 10);
      fop.tablecellclose;
      fop.tablecellopen;
      fop.block (fof.bold (formatear (v_acum1)), font_size => 10, text_align => 'right');
      fop.tablecellclose;
      fop.tablecellopen;
      fop.block (fof.bold (formatear (v_acum2)), font_size => 10, text_align => 'right');
      fop.tablecellclose;
      fop.tableRowClose;
      fop.tableBodyClose;
      fop.tableClose;
   end;
BEGIN
   select count (*) + 1
   into   vSesion
   from   apa_sesiones
   where  sesion_id = pSesion
   and    fecha < sysdate () - 0.33;

   select id
   into   vCurso
   from   hor_curso_academico;

   if vSesion > 0 then
      cabecera;

      begin
         fop.block (fof.bold ('Control 1 - Professors amb exc�s de cr�dits assignats'), font_size => 16);
         fop.block (fof.white_space);

         for a in lista_areas (pDepartamento) loop
            control_1 (pDepartamento, a.id);
         end loop;

         fop.block (fof.white_space);
         fop.block (fof.white_space);
      exception
         when others then
            fop.block ('Error control 1: ' || sqlerrm, font_size => 12);
      end;

      begin
         fop.block (fof.bold ('Control 2 - Solapaments'), font_size => 16);
         fop.block (fof.white_space);

         for a in lista_areas (pDepartamento) loop
            control_2 (pDepartamento, a.id);
         end loop;

         fop.block (fof.white_space);
         fop.block (fof.white_space);
      exception
         when others then
            fop.block ('Error control 2: ' || sqlerrm, font_size => 12);
      end;

      begin
         fop.block (fof.bold ('Control 3 - Desquadres de cr�dits per assignatura'), font_size => 16);
         fop.block (fof.white_space);
         control_3 (pDepartamento);
         fop.block (fof.white_space);
         fop.block (fof.white_space);
      exception
         when others then
            fop.block ('Error control 3: ' || sqlerrm, font_size => 12);
      end;

      begin
         fop.block (fof.bold ('Control 4 - Estat d''assignaci� de cr�dits per �rea de coneixement'), font_size => 16);
         fop.block (fof.white_space);
         control_4 (pDepartamento);
         fop.block (fof.white_space);
         fop.block (fof.white_space);
      exception
         when others then
            fop.block ('Error control 3: ' || sqlerrm, font_size => 12);
      end;

      pie;
   else
      cabecera;
      fop.block ('No tens perm�s per accedir a aquest document.', font_weight => 'bold', text_align => 'center',
                 font_size => 14, space_after => 0.5);
      pie;
   end if;
END;



CREATE OR REPLACE FORCE VIEW UJI_HORARIOS.HOR_EXT_CARGOS_PER (ID,
                                                              PERSONA_ID,
                                                              NOMBRE,
                                                              CENTRO_ID,
                                                              CENTRO,
                                                              ESTUDIO_ID,
                                                              ESTUDIO,
                                                              CARGO_ID,
                                                              CARGO,
                                                              DEPARTAMENTO_ID,
                                                              DEPARTAMENTO,
                                                              AREA_ID,
                                                              AREA,
                                                              CURSO_ID
                                                             ) AS
   select rownum id, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, TITULACION_ID estudio_id, TITULACION estudio, CARGO_ID,
          CARGO, DEPARTAMENTO_ID, DEPARTAMENTO, AREA_ID, AREA, CURSO_ID
   from   (
/* PDI de centro */
           select cp.per_id persona_id, p.nombre || ' ' || apellido1 || ' ' || apellido2 nombre, ulogica_id centro_id,
                  ubic.nombre centro, tit.id titulacion_id, tit.nombre titulacion, tc.id cargo_id, tc.nombre cargo,
                  d.id departamento_id, d.nombre departamento, a.id area_id, a.nombre area, null curso_id
           from   grh_grh.grh_cargos_per cp,
                  gri_est.est_ubic_estructurales ubic,
                  gra_Exp.exp_v_titu_todas tit,
                  uji_horarios.hor_tipos_Cargos tc,
                  gri_per.per_personas p,
                  uji_horarios.hor_departamentos d,
                  uji_horarios.hor_areas a
           where  (    f_fin is null
                   and (   f_fin is null
                        or f_fin >= sysdate)
                   and crg_id in (189, 195, 188, 30))
           and    ulogica_id = ubic.id
           and    ulogica_id = tit.uest_id
           and    tit.activa = 'S'
           and    tit.tipo = 'G'
           and    tc.id = 3
           and    cp.per_id = p.id
           and    ulogica_id = d.centro_id
           and    d.id = a.departamento_id
           union all
/* PAS de centro */
           select cp.per_id persona_id, p.nombre || ' ' || apellido1 || ' ' || apellido2 nombre, ubicacion_id centro_id,
                  ubic.nombre centro, tit.id titulacion_id, tit.nombre titulacion, tc.id cargo_id, tc.nombre cargo,
                  d.id departamento_id, d.nombre departamento, a.id area_id, a.nombre area, null curso_id
           from   grh_grh.grh_vw_contrataciones_ult cp,
                  gri_est.est_ubic_estructurales ubic,
                  gra_Exp.exp_v_titu_todas tit,
                  uji_horarios.hor_tipos_Cargos tc,
                  gri_per.per_personas p,
                  uji_horarios.hor_departamentos d,
                  uji_horarios.hor_areas a
           where  ubicacion_id = ubic.id
           and    ubicacion_id = tit.uest_id
           and    tit.activa = 'S'
           and    tit.tipo = 'G'
           and    tc.id = 4
           and    cp.per_id = p.id
           and    ubicacion_id = d.centro_id
           and    d.id = a.departamento_id
           union all
/* directores de titulacion */
           select cp.per_id persona_id, p.nombre || ' ' || apellido1 || ' ' || apellido2 nombre, ulogica_id centro_id,
                  ubic.nombre centro, tit.id titulacion_id, tit.nombre titulacion, tc.id cargo_id, tc.nombre cargo,
                  null departamento_id, '' departamento, null area_id, '' area, null curso_id
           from   grh_grh.grh_cargos_per cp,
                  gri_est.est_ubic_estructurales ubic,
                  gra_Exp.exp_v_titu_todas tit,
                  uji_horarios.hor_tipos_Cargos tc,
                  gri_per.per_personas p
           where  (    f_fin is null
                   and (   f_fin is null
                        or f_fin >= sysdate)
                   and crg_id in (192, 193))
           and    ulogica_id = ubic.id
           and    ulogica_id = tit.uest_id
           and    tit.activa = 'S'
           and    tit.tipo = 'G'
           and    tc.id = 1
           and    cp.per_id = p.id
           and    cp.tit_id = tit.id
           union all
/* coordinadores de curso */
           select cp.per_id persona_id, p.nombre || ' ' || apellido1 || ' ' || apellido2 nombre, ulogica_id centro_id,
                  ubic.nombre centro, tit.id titulacion_id, tit.nombre titulacion, tc.id cargo_id, tc.nombre cargo,
                  null departamento_id, '' departamento, null area_id, '' area, ciclo_cargo curso_id
           from   grh_grh.grh_cargos_per cp,
                  gri_est.est_ubic_estructurales ubic,
                  gra_Exp.exp_v_titu_todas tit,
                  uji_horarios.hor_tipos_Cargos tc,
                  gri_per.per_personas p
           where  (    f_fin is null
                   and (   f_fin is null
                        or f_fin >= sysdate)
                   and crg_id in (305))
           and    ulogica_id = ubic.id
           and    ulogica_id = tit.uest_id
           and    tit.activa = 'S'
           and    tit.tipo = 'G'
           and    tc.id = 2
           and    cp.per_id = p.id
           and    cp.tit_id = tit.id
           union all
/* directores de departamento */
           select cp.per_id persona_id, p.nombre || ' ' || apellido1 || ' ' || apellido2 nombre, c.id centro_id,
                  c.nombre centro, null titulacion_id, '' titulacion, tc.id cargo_id, tc.nombre cargo,
                  ulogica_id departamento_id, d.nombre departamento, a.id area_id, a.nombre area, null curso_id
           from   grh_grh.grh_cargos_per cp,
                  uji_horarios.hor_tipos_Cargos tc,
                  gri_per.per_personas p,
                  est_ubic_estructurales d,
                  est_relaciones_ulogicas r,
                  est_ubic_estructurales c,
                  uji_horarios.hor_Areas a
           where  (    f_fin is null
                   and (   f_fin is null
                        or f_fin >= sysdate)
                   and crg_id in (178, 194))
           and    tc.id = 6
           and    cp.per_id = p.id
           and    ulogica_id = d.id
           and    uest_id_relacionad = d.id
           and    d.tuest_id = 'DE'
           and    uest_id = c.id
           and    trel_id = 4
           and    ulogica_id = a.departamento_id
           union all
           /* pas de departamento */
           select cp.per_id persona_id, p.nombre || ' ' || apellido1 || ' ' || apellido2 nombre, c.id centro_id,
                  c.nombre centro, null titulacion_id, '' titulacion, tc.id cargo_id, tc.nombre cargo,
                  ubic.id departamento_id, ubic.nombre departamento, a.id area_id, a.nombre area, null curso_id
           from   grh_grh.grh_vw_contrataciones_ult cp,
                  gri_est.est_ubic_estructurales ubic,
                  uji_horarios.hor_tipos_Cargos tc,
                  gri_per.per_personas p,
                  est_relaciones_ulogicas r,
                  est_ubic_estructurales c,
                  uji_horarios.hor_areas a
           where  ubicacion_id = ubic.id
           and    tc.id = 5
           and    cp.per_id = p.id
           and    cp.act_id = 'PAS'
           and    ubicacion_id = uest_id_relacionad
           and    trel_id = 4
           and    uest_id = c.id
           and    ubic.id = departamento_id
           union all
           /* permisos extra */
           select x.persona_id, x.nombre, x.centro_id, x.centro, x.titulacion_id, titulacion, x.cargo_id, x.cargo,
                  x.departamento_id, x.departamento,
                  decode (cargo_id, 6, decode (area_id, null, a.id, area_id), area_id) area_id,
                  decode (cargo_id, 6, decode (area_id, null, a.nombre, area), area) area, curso_id
           from   (select per.persona_id, p.nombre || ' ' || apellido1 || ' ' || apellido2 nombre, ubic.id centro_id,
                          ubic.nombre centro, tit.id titulacion_id, tit.nombre titulacion, tc.id cargo_id,
                          tc.nombre cargo, departamento_id, dep.nombre departamento, area_id,
                          (select nombre
                           from   gri_est.est_ubic_estructurales
                           where  id = area_id) area, per.curso_id
                   from   uji_horarios.hor_permisos_extra per,
                          gri_per.per_personas p,
                          (select *
                           from   gra_Exp.exp_v_titu_todas
                           where  activa = 'S'
                           and    tipo = 'G') tit,
                          uji_horarios.hor_tipos_cargos tc,
                          gri_est.est_ubic_estructurales ubic,
                          gri_est.est_ubic_estructurales dep
                   where  persona_id = p.id
                   and    estudio_id = tit.id(+)
                   and    tipo_Cargo_id = tc.id
                   and    tit.uest_id = ubic.id(+)
                   and    per.departamento_id = dep.id(+)) x,
                  uji_horarios.hor_areas a
           where  (    cargo_id <> 6
                   and a.id = (select min (id)
                               from   uji_horarios.hor_areas))
           or     (    cargo_id = 6
                   and area_id is not null
                   and a.id = area_id)
           or     (    cargo_id = 6
                   and area_id is null
                   and x.departamento_id = a.departamento_id)
           union all
/* administradores */
           select distinct per.persona_id, p.nombre || ' ' || apellido1 || ' ' || apellido2 nombre, u.id centro_id,
                           u.nombre centro, tit.id titulacion_id, tit.nombre titulacion, tc.id cargo_id,
                           tc.nombre cargo, null departamento_id, '' departamento, null area_id, '' area, null curso_id
           from            uji_apa.apa_vw_personas_items per,
                           gri_per.per_personas p,
                           gra_Exp.exp_v_titu_todas tit,
                           uji_horarios.hor_tipos_cargos tc,
                           gri_Est.est_ubic_estructurales u
           where           persona_id = p.id
           and             tc.id = 1
           and             tit.activa = 'S'
           and             tit.tipo = 'G'
           and             aplicacion_id = 46
           and             role = 'ADMIN'
           and             tit.uest_id = u.id
           union all
           select distinct per.persona_id, p.nombre || ' ' || apellido1 || ' ' || apellido2 nombre, c.id centro_id,
                           c.nombre centro, null titulacion_id, '' titulacion, tc.id cargo_id, tc.nombre cargo,
                           dep.id departamento_id, dep.nombre departamento, null area_id, '' area, null curso_id
           from            uji_apa.apa_vw_personas_items per,
                           gri_per.per_personas p,
                           est_ubic_estructurales dep,
                           uji_horarios.hor_tipos_cargos tc,
                           gri_Est.est_ubic_estructurales c,
                           est_relaciones_ulogicas r
           where           persona_id = p.id
           and             tc.id = 6
           and             dep.status = 'A'
           and             dep.tuest_id = 'DE'
           and             aplicacion_id = 46
           and             role = 'ADMIN'
           and             dep.id = r.uest_id_relacionad
           and             r.trel_id = 4
           and             r.uest_id = c.id);
		   
		   


			 