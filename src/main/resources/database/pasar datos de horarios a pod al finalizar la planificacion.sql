revisar el camp comun que siga S N 




/* Formatted on 24/06/2014 10:26 (Formatter Plus v4.8.8) */


declare
   v_curso_aca   number        := 2014;
   v_existe      number;
   v_salida      boolean       := false;
   v_grupo       varchar2 (10);
   v_contar      number;

   cursor lista_inicial is
      select 'ALTER TRIGGER gra_pod.pod_horarios_blk DISABLE' accion
      from   dual
      union all
      select 'ALTER TRIGGER gra_pod.pod_gen_fecha_hor_at DISABLE' accion
      from   dual
      union all
      select 'ALTER TRIGGER gra_pod.pod_gen_fecha_hor DISABLE' accion
      from   dual
      union all
      select 'ALTER TRIGGER gra_pod.pod_horarios_fecha_t DISABLE' accion
      from   dual;

   cursor lista_final is
      select 'ALTER TRIGGER gra_pod.pod_horarios_blk enable' accion
      from   dual
      union all
      select 'ALTER TRIGGER gra_pod.pod_gen_fecha_hor enable' accion
      from   dual
      union all
      select 'ALTER TRIGGER gra_pod.pod_gen_fecha_hor_at enable' accion
      from   dual
      union all
      select 'ALTER TRIGGER gra_pod.pod_horarios_fecha_t enable' accion
      from   dual;

   cursor lista_items is
      select xx, id, asignatura_id, grupo_id, semestre_id, tipo_subgrupo_id, subgrupo_id, dia_semana_id,
             hora_inicio + (xx / 24 / 60) hora_inicio, hora_fin, aula_planificacion_id, estudio_id, curso_id, comun,
             id_estructura
      from   (select row_number () over (partition by asignatura_id, substr (grupo_id, 1, 1), semestre_id, subgrupo_id, tipo_subgrupo_id, dia_semana_id, to_char
                                                                                                                                                           (hora_inicio,
                                                                                                                                                            'hh24mi') order by i.id)
                     - 1 xx,
                     i.id, asignatura_id, substr (grupo_id, 1, 1) grupo_id, semestre_id, subgrupo_id, tipo_subgrupo_id,
                     dia_semana_id, hora_inicio, hora_fin, aula_planificacion_id, estudio_id, curso_id,
                     decode (comun, 0, 'N', 'S') comun, u.id id_estructura
              from   uji_horarios.hor_items i,
                     uji_horarios.hor_items_asignaturas a,
                     uji_horarios.hor_estudios e,
                     uji_horarios.hor_aulas au,
                     est_ubicaciones u
              where  i.id = a.item_id
              and    a.estudio_id = e.id
              --and    a.estudio_id != 225
--and    e.centro_id = 4
--and    estudio_id = 215
--and    asignatura_id in ( 'EM1007', 'EQ1007','EI1007','AG1007','EE1007')
              --and    asignatura_id = 'MD1103'
              --and    curso_id = 2
              and    dia_semana_id is not null
--and    tipo_subgrupo_id = 'LA'
--and    subgrupo_id = 1
--and dia_Semana_id=2
--and i.id = 881445
              and    aula_planificacion_id = au.id(+)
              and    aula_planificacion_id = u.id(+)
--and    u.id is null
--and    aula_planificacion_id is not null
             );

   cursor lista_items_detalle (p_id in number, p_estudio in number, p_asignatura in varchar2) is
      select   i.id item_id, asignatura_id, substr (grupo_id, 1, 1) grupo_id, semestre_id, subgrupo_id,
               tipo_subgrupo_id tipo_id, dia_semana_id dia_id, hora_inicio, hora_fin, aula_planificacion_id aula_id,
               estudio_id, curso_id, decode (comun, 0, 'N', 'S') comun, inicio, fin
      from     uji_horarios.hor_items i,
               uji_horarios.hor_items_asignaturas a,
               uji_horarios.hor_estudios e,
               uji_horarios.hor_items_detalle d
      where    i.id = a.item_id
      and      a.estudio_id = e.id
      and      i.id = d.item_id
      and      i.id = p_id
      and      a.estudio_id = p_estudio
      and      asignatura_id = p_asignatura
      and      i.detalle_manual = 1
      union
      select   i.id item_id, a.asignatura_id, substr (i.grupo_id, 1, 1) grupo_id, i.semestre_id, i.subgrupo_id,
               i.tipo_subgrupo_id tipo_id, i.dia_semana_id dia_id, hora_inicio, hora_fin, aula_planificacion_id aula_id,
               a.estudio_id, i.curso_id, decode (comun, 0, 'N', 'S') comun, fecha inicio, fecha fin
      from     uji_horarios.hor_items i,
               uji_horarios.hor_items_asignaturas a,
               uji_horarios.hor_estudios e,
               uji_horarios.hor_v_items_detalle d
      where    i.id = a.item_id
      and      a.estudio_id = e.id
      and      i.id = d.id
      and      i.id = p_id
      and      a.estudio_id = p_estudio
      and      a.asignatura_id = p_asignatura
      and      i.detalle_manual = 0
      and      d.docencia = 'S'
      order by dia_id,
               inicio;

   procedure ejecutar (p_accion in varchar2) is
   begin
      execute immediate p_accion;
   end;

   procedure desactivar_triggers is
   begin
      for x in lista_inicial loop
         ejecutar (x.accion);
      end loop;
   end;

   procedure activar_triggers is
   begin
      for x in lista_final loop
         ejecutar (x.accion);
      end loop;
   end;
begin
   desactivar_triggers;

   for x in lista_items loop
            /*delete      pod_horarios_fechas
            where       hor_sgr_grp_asi_id = x.asignatura_id
            and         hor_sgr_grp_id = x.grupo_id
            and         hor_sgr_grp_curso_aca = v_curso_aca
            and         hor_sgr_id = x.subgrupo_id
            and         hor_sgr_tipo = x.tipo_subgrupo_id
            and         hor_semestre = x.semestre_id
            and         hor_dia_sem = x.dia_semana_id
            and         hor_ini = to_date ('01012000' || to_char (x.hora_inicio, 'HH24MI'), 'ddmmyyyyhh24mi');

            delete      pod_horarios
            where       sgr_grp_asi_id = x.asignatura_id
            and         sgr_grp_id = x.grupo_id
            and         sgr_grp_curso_aca = v_curso_aca
            and         sgr_id = x.subgrupo_id
            and         sgr_tipo = x.tipo_subgrupo_id
            and         semestre = x.semestre_id
            and         dia_sem = x.dia_semana_id
            and         ini = to_date ('01012000' || to_char (x.hora_inicio, 'HH24MI'), 'ddmmyyyyhh24mi');
      */
      begin
         select count (*)
         into   v_existe
         from   pod_grupos_Teoria
         where  cur_tit_id = x.estudio_id
         and    cur_id = x.curso_id
         and    curso_aca = v_curso_aca
         and    grupo = x.grupo_id;

         if v_existe > 0 then
            v_grupo := x.grupo_id;
         else
            begin
               select grupo
               into   v_grupo
               from   pod_grupos_Teoria
               where  cur_tit_id = x.estudio_id
               and    curso_Aca = v_curso_aca
               and    cur_id = x.curso_id
               and    ascii (grupo) = ascii (x.grupo_id) - 5;
            exception
               when no_data_found then
                  select min (grupo)
                  into   v_grupo
                  from   pod_grupos_Teoria
                  where  cur_tit_id = x.estudio_id
                  and    curso_Aca = v_curso_aca
                  and    cur_id = x.curso_id;
            end;
         end if;

         /*
         insert into pod_horarios
                     (sgr_grp_asi_id, sgr_grp_id, semestre, sgr_grp_curso_aca, sgr_id, sgr_tipo,
                      dia_sem, ini,
                      fin, comentario, per_id, ubi_id, grc_curso_aca, grc_cur_tit_id, grc_cur_id, grc_grupo,
                      compartido
                     )
         values      (x.asignatura_id, x.grupo_id, x.semestre_id, v_curso_aca, x.subgrupo_id, x.tipo_subgrupo_id,
                      x.dia_semana_id, to_date ('01012000' || to_char (x.hora_inicio, 'HH24MI'), 'ddmmyyyyhh24mi'),
                      x.hora_fin, null, null, x.aula_planificacion_id, v_curso_aca, x.estudio_id, x.curso_id, v_grupo,
                      x.comun
                     );*/
         /*
         dbms_output.put_line (to_char (to_date ('01012000' || to_char (x.hora_inicio, 'HH24MI'), 'ddmmyyyyhh24mi'),
                                        'dd-mm-yyyy hh24:mi')
                               || ' - ' || to_char (x.hora_fin, 'dd-mm-yyyy hh24:mi'));
                               */
         insert into pod_horarios
                     (sgr_grp_asi_id, sgr_grp_id, semestre, sgr_grp_curso_aca, sgr_id, sgr_tipo,
                      dia_sem, ini,
                      fin, comentario, per_id, ubi_id, grc_curso_aca, grc_cur_tit_id, grc_cur_id, grc_grupo, compartido
                     )
         values      (x.asignatura_id, x.grupo_id, x.semestre_id, v_curso_aca, x.subgrupo_id, x.tipo_subgrupo_id,
                      x.dia_semana_id, to_date ('01012000' || to_char (x.hora_inicio, 'HH24MI'), 'ddmmyyyyhh24mi'),
                      x.hora_fin, null, null, x.id_estructura, v_curso_aca, x.estudio_id, x.curso_id, v_grupo, x.comun
                     );

         /*dbms_output.put_line
            ('insert into pod_horarios
                     (sgr_grp_asi_id, sgr_grp_id, semestre, sgr_grp_curso_aca, sgr_id, sgr_tipo,
                      dia_sem, ini,
                      fin, comentario, per_id, ubi_id, grc_curso_aca, grc_cur_tit_id, grc_cur_id,
                      grc_grupo, compartido
                     )
         values      ('''
             || x.asignatura_id || ''', ''' || x.grupo_id || ''', ' || x.semestre_id || ', ' || v_curso_aca || ', '
             || x.subgrupo_id || ', ''' || x.tipo_subgrupo_id || ''',
                      ' || x.dia_semana_id || ', to_date (''01012000' || to_char (x.hora_inicio, 'HH24MI')
             || ''', ''ddmmyyyyhh24mi''),
                     to_date( ''' || to_char (x.hora_fin, 'dd/mm/yyyy') || ''',''dd/mm/yyyy''), null, null, '
             || x.aula_planificacion_id || ', ' || v_curso_aca || ', ' || x.estudio_id || ', ' || x.curso_id
             || ',
                      ''' || v_grupo || ''', ''' || x.comun || '''
                     );');
                     */
         v_contar := 0;

         for d in lista_items_detalle (x.id, x.estudio_id, x.asignatura_id) loop
            v_contar := v_contar + 1;

            begin
               insert into pod_horarios_fechas
                           (hor_sgr_grp_asi_id, hor_sgr_grp_id, hor_sgr_grp_curso_aca, hor_sgr_id, hor_sgr_tipo,
                            hor_semestre, hor_dia_sem,
                            hor_ini,
                            fecha, comentario, ubi_id
                           )
               values      (x.asignatura_id, x.grupo_id, v_curso_aca, x.subgrupo_id, x.tipo_subgrupo_id,
                            x.semestre_id, x.dia_semana_id,
                            to_date ('01012000' || to_char (x.hora_inicio, 'HH24MI'), 'ddmmyyyyhh24mi'),
                            trunc (d.inicio), null, x.aula_planificacion_id
                           );
            exception
               when others then
                  dbms_output.put_line ('Detalle: (' || v_contar || ') ' || x.asignatura_id || ' ' || x.id || ' - '
                                        || sqlerrm);
                  dbms_output.put_line
                     ('insert into pod_horarios_fechas
                        (hor_sgr_grp_asi_id, hor_sgr_grp_id, hor_sgr_grp_curso_aca, hor_sgr_id, hor_sgr_tipo,
                         hor_semestre, hor_dia_sem, hor_ini, fecha, comentario, ubi_id
                        )
            values      ('''
                      || x.asignatura_id || ''', ''' || x.grupo_id || ''', ' || v_curso_aca || ', ' || x.subgrupo_id
                      || ', ''' || x.tipo_subgrupo_id || ''',
                         ' || x.semestre_id || ', ' || x.dia_semana_id || ', to_Date('''
                      || to_char (to_date ('01012000' || to_char (x.hora_inicio, 'HH24MI'), 'ddmmyyyyhh24mi'),
                                  'dd/mm/yyyy hh24:mi:ss')
                      || ''',''dd/mm/yyyy hh24:mi:ss''), trunc (to_date(''' || to_char (d.inicio, 'dd/mm/yyyy')
                      || ''',''dd/mm/yyyy'')), null, ' || x.aula_planificacion_id || '
                        );');
            end;
         end loop;
      exception
         when others then
            dbms_output.put_line ('Horario: ' || x.asignatura_id || ' - ' || sqlerrm);
            dbms_output.put_line
               ('insert into pod_horarios
                     (sgr_grp_asi_id, sgr_grp_id, semestre, sgr_grp_curso_aca, sgr_id, sgr_tipo,
                      dia_sem, ini,
                      fin, comentario, per_id, ubi_id, grc_curso_aca, grc_cur_tit_id, grc_cur_id,
                      grc_grupo, compartido
                     )
         values      ('''
                || x.asignatura_id || ''', ''' || x.grupo_id || ''', ' || x.semestre_id || ', ' || v_curso_aca || ', '
                || x.subgrupo_id || ', ''' || x.tipo_subgrupo_id || ''',
                      ' || x.dia_semana_id || ', to_date (''01012000' || to_char (x.hora_inicio, 'HH24MI')
                || ''', ''ddmmyyyyhh24mi''),
                     to_date( ''' || to_char (x.hora_fin, 'dd/mm/yyyy') || ''',''dd/mm/yyyy''), null, null, '
                || x.aula_planificacion_id || ', ' || v_curso_aca || ', ' || x.estudio_id || ', ' || x.curso_id
                || ',
                      ''' || v_grupo || ''', ''' || x.comun || '''
                     );');
      end;

      commit;
   end loop;

   activar_triggers;
end;


-------------------------




/* Formatted on 23/06/2014 10:44 (Formatter Plus v4.8.8) */
declare
   v_curso_aca   number        := 2014;
   v_existe      number;
   v_salida      boolean       := false;
   v_grupo       varchar2 (10);

   cursor lista_inicial is
      select 'ALTER TRIGGER gra_pod.pod_horarios_blk DISABLE' accion
      from   dual
      union all
      select 'ALTER TRIGGER gra_pod.pod_gen_fecha_hor_at DISABLE' accion
      from   dual
      union all
      select 'ALTER TRIGGER gra_pod.pod_gen_fecha_hor DISABLE' accion
      from   dual
      union all
      select 'ALTER TRIGGER gra_pod.pod_horarios_fecha_t DISABLE' accion
      from   dual;

   cursor lista_final is
      select 'ALTER TRIGGER gra_pod.pod_horarios_blk enable' accion
      from   dual
      union all
      select 'ALTER TRIGGER gra_pod.pod_gen_fecha_hor enable' accion
      from   dual
      union all
      select 'ALTER TRIGGER gra_pod.pod_gen_fecha_hor_at enable' accion
      from   dual
      union all
      select 'ALTER TRIGGER gra_pod.pod_horarios_fecha_t enable' accion
      from   dual;

   procedure ejecutar (p_accion in varchar2) is
   begin
      execute immediate p_accion;
   end;

   procedure desactivar_triggers is
   begin
      for x in lista_inicial loop
         ejecutar (x.accion);
      end loop;
   end;

   procedure activar_triggers is
   begin
      for x in lista_final loop
         ejecutar (x.accion);
      end loop;
   end;
begin
   desactivar_triggers;

   delete      pod_horarios_fechas
   where       hor_sgr_grp_curso_aca = v_curso_aca;

   delete      pod_horarios
   where       sgr_grp_curso_aca = v_curso_aca;

   commit;
   activar_triggers;
end;




----------------------


/* Formatted on 23/06/2014 11:11 (Formatter Plus v4.8.8) */
select xx, id, asignatura_id, grupo_id, semestre_id, subgrupo_id, dia_semana_id, hora_inicio + (xx / 24 / 60), hora_fin,
       aula_planificacion_id, estudio_id, curso_id, comun, id_estructura
from   (select row_number () over (partition by asignatura_id, grupo_id, semestre_id, subgrupo_id, tipo_subgrupo_id, dia_semana_id, hora_inicio order by i.id)
               - 1 xx,
               i.id, asignatura_id, grupo_id, semestre_id, subgrupo_id, tipo_subgrupo_id, dia_semana_id, hora_inicio,
               hora_fin, aula_planificacion_id, estudio_id, curso_id, decode (comun, 0, 'N', 'S') comun,
               u.id id_estructura
        from   uji_horarios.hor_items i,
               uji_horarios.hor_items_asignaturas a,
               uji_horarios.hor_estudios e,
               uji_horarios.hor_aulas au,
               est_ubicaciones u
        where  i.id = a.item_id
        and    a.estudio_id = e.id
        and    a.estudio_id = 201
--and    e.centro_id = 4
--and    estudio_id = 215
--and    asignatura_id in ( 'EM1007', 'EQ1007','EI1007','AG1007','EE1007')
        and    asignatura_id = 'RL0914'
        and    curso_id = 2
        and    dia_semana_id is not null
--and    tipo_subgrupo_id = 'LA'
--and    subgrupo_id = 19
        and    aula_planificacion_id = au.id(+)
        and    aula_planificacion_id = u.id(+)
--and    u.id is null
--and    aula_planificacion_id is not null
       );
       
       
       
       
       
select *
from   pod_horarios
where  sgr_grp_curso_aca = 2014


-----------------------------------------------------





------ control de fechas fuera de semestre

select distinct f.hor_Sgr_grp_asi_id, semestre, fecha
from   pod_horarios h,
       pod_horarios_fechas f
where  sgr_grp_curso_aca = 2014
--and    sgr_grp_asi_id = 'MI1001'
--and    semestre = 1
--and    sgr_tipo = 'PR'
and    sgr_id = 1
and    sgr_grp_asi_id = hor_sgr_grp_asi_id
and    sgr_grp_id = hor_sgr_grp_id
and    semestre = hor_semestre
and    sgr_grp_curso_aca = hor_sgr_grp_curso_aca
and    sgr_id = hor_sgr_id
and    sgr_tipo = hor_sgr_tipo
and    dia_sem = hor_dia_sem
and    ini = hor_ini
and decode(semestre,1,2014,2,2015) != to_number(to_char(fecha,'yyyy'))
order by 1,2,3




