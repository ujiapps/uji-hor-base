CREATE OR REPLACE TRIGGER UJI_HORARIOS.hor_permisos_Extra_items
BEFORE DELETE OR INSERT
ON UJI_HORARIOS.HOR_PERMISOS_EXTRA 
REFERENCING NEW AS New OLD AS Old
FOR EACH ROW
BEGIN
   if inserting then
      uji_apa.apa_permisos_horarios (:new.persona_id, :new.tipo_cargo_id, 'C');
   elsif deleting then
      uji_apa.apa_permisos_horarios (:old.persona_id, :old.tipo_cargo_id, 'D');
   end if;
END hor_permisos_Extra_items;



/*
CREATE OR REPLACE PROCEDURE UJI_APA.apa_permisos_horarios (p_persona in number, p_cargo in number, p_accion in varchar2) IS
   v_cuantos   number;
BEGIN
   if p_cargo = 6 then
      if p_accion = 'D' then
         delete      apa_items_extras
         where       persona_id = p_persona
         and         item_id = 4367;
      elsif p_accion = 'C' then
         select count (*)
         into   v_cuantos
         from   apa_items_extras
         where  persona_id = p_persona
         and    item_id = 4367;

         if v_cuantos = 0 then
            insert into apa_items_extras
                        (id, item_id, persona_id, plaza_id, role_id,
                         fecha_fin
                        )
            values      (hibernate_sequence.nextval, 4367, p_persona, null, 2,
                         to_date ('31-08-' || to_char (sysdate, 'yyyy'), 'dd/mm/yyyy')
                        );
         end if;
      end if;
   end if;
END apa_permisos_horarios;

grant execute on apa_permisos_horarios to uji_horarios;

*/



/* Formatted on 22/05/2014 17:30 (Formatter Plus v4.8.8) */
CREATE OR REPLACE PROCEDURE UJI_HORARIOS.horarios_examenes_pdf (
   name_array    IN   euji_util.ident_arr2,
   value_array   IN   euji_util.ident_arr2
) IS
   v_aux            NUMBER;
   pEstudio         number          := euji_util.euji_getparam (name_array, value_array, 'pEstudio');
   pConvocatoria    number          := euji_util.euji_getparam (name_array, value_array, 'pConvocatoria');
   pCurso           number          := euji_util.euji_getparam (name_array, value_array, 'pCurso');
   pSesion          VARCHAR2 (400)  := euji_util.euji_getparam (name_array, value_array, 'pSesion');
   v_nombre         varchar2 (2000);
   v_convocatoria   varchar2 (2000);
   vSesion          number;
   vCurso           number;
   vCuantos         number;
   vFechas          t_horario;
   vRdo             clob;

   cursor lista_semanas (p_estudio in number, p_convocatoria in number, p_Curso in number) is
      select   a�o, semana, min (fecha) lunes, max (fecha) domingo
      from     (select x.*, c.*, to_number (to_char (fecha, 'iw')) semana
                from   (select trunc (min (fecha)) inicio, trunc (max (fecha)) fin
                        from   hor_Examenes e,
                               hor_examenes_asignaturas a
                        where  convocatoria_id = p_convocatoria
                        and    e.id = examen_id
                        and    estudio_id = p_estudio
                        and    fecha is not null
                        and    (   p_Curso = -1
                                or p_Curso = curso_id)) x,
                       hor_ext_calendario c
                where  c.fecha between inicio and fin
                and    tipo_dia <> 'F')
      group by a�o,
               semana
      order by 1,
               2;

   cursor lista_items (
      p_estudio        in   number,
      p_convocatoria   in   number,
      p_curso          in   number,
      p_a�o            in   number,
      p_semana         in   number
   ) is
      select e.id, e.nombre,
             to_date (to_char (e.fecha, 'dd/mm/yyyy') || ' ' || to_char (e.hora_inicio, 'hh24:mi'),
                      'dd/mm/yyyy hh24:mi') inicio,
             to_date (to_char (e.fecha, 'dd/mm/yyyy') || ' ' || to_char (e.hora_fin, 'hh24:mi'),
                      'dd/mm/yyyy hh24:mi') fin,
             estudio_id, semestre, tipo_asignatura, curso_id
      from   hor_Examenes e,
             hor_examenes_asignaturas a
      where  convocatoria_id = p_convocatoria
      and    e.id = examen_id
      and    estudio_id = p_estudio
      and    fecha is not null
      and    to_number (to_char (fecha, 'yyyy')) = p_a�o
      and    to_number (to_char (fecha, 'iw')) = p_semana
      and    (   p_Curso = -1
              or p_Curso = curso_id);

   procedure cabecera is
      v_texto   varchar2 (100);
   begin
      if pCurso = -1 then
         v_texto := '';
      else
         v_texto := ' - Curs: ' || pCurso;
      end if;

      owa_util.mime_header (ccontent_type => 'text/xml', bclose_header => true);
      fop.documentOpen (page_width => 29.7, page_height => 21, margin_left => 1.5, margin_right => 1.5,
                        margin_top => 0.5, margin_bottom => 0.5, extent_before => 3, extent_after => 1.5,
                        extent_margin_top => 3, extent_margin_bottom => 1.5);
      fop.pageBodyOpen;
      htp.p (fof.documentheaderopen || fof.blockopen (space_after => 0.1) || fof.tableOpen
             || fof.tablecolumndefinition (column_width => 3) || fof.tablecolumndefinition (column_width => 24)
             || fof.tablebodyopen || fof.tableRowOpen || fof.tablecellopen
             || fof.block (fof.logo_uji, text_align => 'center') || fof.tablecellclose
             || fof.tablecellopen (display_align => 'center', padding => 0.4)
             || fof.block (text => v_nombre, font_size => 16, font_weight => 'bold', font_family => 'Arial',
                           text_align => 'center')
             || fof.block (text => 'Horaris d''ex�mens - Curs acad�mic: ' || vCurso || '/'
                            || substr (to_char (vCurso + 1), 3) || ' - ' || v_convocatoria||v_texto,
                           font_size => 16, font_weight => 'bold', font_family => 'Arial', text_align => 'center')
             || fof.tablecellclose || fof.tableRowClose || fof.tablebodyclose || fof.tableClose || fof.blockclose
             || fof.documentheaderclose);
      -- El numero paginas se escribe a mano para poner el texto m?s peque?o
      htp.p (fof.documentfooteropen || fof.tableOpen || fof.tablecolumndefinition (column_width => 25)
             || fof.tablebodyopen || fof.tableRowOpen || fof.tablecellopen
             || fof.block (text => 'Pag. <fo:page-number /> de <fo:page-number-citation ref-id="lastBlock" />',
                           text_align => 'right', font_weight => 'bold', font_size => 8)
             || fof.tablecellclose || fof.tableRowClose || fof.tablebodyclose || fof.tableClose
             || fof.documentfooterclose);
      fop.pageFlowOpen;
      fop.block (fof.white_space);
   end;

   procedure pie is
   begin
      htp.p ('<fo:block  id="lastBlock" space-before="1cm" font-size="8pt">');
      htp.p ('Data de generaci�: ' || to_char (sysdate, 'dd/mm/yyy hh24:mi'));
      fop.blockclose ();
      fop.pageFlowClose;
      fop.pageBodyClose;
      fop.documentClose;
   end;

   function espacios (p_espacios in number)
      return varchar2 is
      v_rdo   varchar2 (2000);
   begin
      v_rdo := null;

      for x in 1 .. p_espacios loop
         v_rdo := v_rdo || fof.white_space;
      end loop;

      return v_rdo;
   end;

   procedure mostrar_calendario (
      p_estudio        in   number,
      p_convocatoria   in   number,
      p_curso          in   number,
      p_a�o            in   number,
      p_semana         in   number
   ) is
      v_aux   number;
   begin
      vFechas := t_horario ();

      for item in lista_items (p_estudio, p_convocatoria, p_curso, p_a�o, p_semana) loop
         vFechas.extend;
         vFechas (vFechas.count) :=
            ItemHorarios (hor_contenido_item (item.inicio, item.fin, item.nombre || ' ' || item.semestre,
                                              '(' || item.tipo_asignatura || ') - Curs ' || item.curso_id),
                          item.inicio, item.fin, null);
      end loop;

      if vFechas.count > 0 then
         vRdo := xpfpkg_horarios_academicos.drawTimetable (vFechas, 5, null, 'CA', 'PDF_APP_HORARIOS_DET');
         xup_utils.dump (vRdo);
         dbms_lob.freeTemporary (vRdo);
         --fop.block (fof.white_space);
         fop.block (fof.white_space);
      end if;
   end;
BEGIN
   select count (*) + 1
   into   vSesion
   from   apa_sesiones
   where  sesion_id = pSesion
   and    fecha < sysdate () - 0.33;

   select id
   into   vCurso
   from   hor_curso_academico;

   if vSesion > 0 then
      begin
         select estudio
         into   v_nombre
         from   hor_items_asignaturas
         where  estudio_id = pEstudio
         and    rownum <= 1;

         select convocatoria_nombre
         into   v_convocatoria
         from   hor_examenes
         where  convocatoria_id = pConvocatoria
         and    rownum <= 1;

         cabecera;

         begin
            vCuantos := 0;

            for x in lista_semanas (pEstudio, pConvocatoria, pCurso) loop
               vCuantos := vCuantos + 1;

               if vCuantos > 1 then
                  htp.p ('<fo:block break-before="page" color="white">_</fo:block>');
               end if;

               fop.block ('Setmana del ' || to_char (x.lunes, 'dd/mm/yyyy') || ' al '
                          || to_char (x.domingo, 'dd/mm/yyyy') || ' ___________________________',
                          font_size => 12, space_after => 0.5, space_before => 0.5);
               mostrar_calendario (pEstudio, pConvocatoria, pCurso, x.a�o, x.semana);
            end loop;

            null;
         exception
            when others then
               fop.block (sqlerrm);
         end;

         pie;
      end loop;
   else
      cabecera;
      fop.block ('No tens perm�s per accedir a aquest document.', font_weight => 'bold', text_align => 'center',
                 font_size => 14, space_after => 0.5);
      pie;
   end if;
END;


CREATE OR REPLACE FORCE VIEW UJI_HORARIOS.HOR_EXT_ASIGNATURAS_COMUNES (ID, GRUPO_COMUN_ID, NOMBRE, ASIGNATURA_ID, PREFIJO) AS
   select rownum id, id grupo_comun_id, nombre, asi_id asignatura_id, decode (substr (asi_id, 1, 2),
               'AG', 'XX',
               'EM', 'XX',
               'ET', 'XX',
               'EE', 'XX',
               'EQ', 'XX',
               'ED', 'XX',
               'HP', 'YY',
               'HU', 'YY',
               'EI', 'ZZ',
               'MT', 'ZZ',
               'AE', 'TT',
               'EC', 'TT',
               'FC', 'TT',
               'QU', 'XX',
               'QQ'
              ) prefijo
   from   pod_grp_comunes g,
          pod_comunes c
   where  g.id = c.gco_id
   and    curso_aca = (select max (curso_academico_id)
                       from   uji_horarios.hor_semestres_detalle);


