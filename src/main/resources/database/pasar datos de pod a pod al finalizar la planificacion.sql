
/* Formatted on 07/07/2014 12:18 (Formatter Plus v4.8.8) */
declare
   v_txt           varchar2 (2000);
   v_cuantos       number;
   v_encontrados   number;
   v_aux           number;
   v_aux_fechas    number;
   v_curso_aca     number          := 2014;
   v_estudio       number          := 222;
   v_acciones      varchar2 (1)    := 'S';

   cursor lista is
            select   asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo_id, creditos, idioma, coste, comentario,
               profesor_id, area_id, detalle_manual, estudio_id, cuantos_prof
          from (select   asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo_id,
                         max (round (nvl (crd_profesor, crd_item / cuantos_prof) * porcentaje / 100, 2)) creditos,
                         idioma, coste, comentario, profesor_id, area_id, max (detalle_manual) detalle_manual,
                         estudio_id, cuantos_prof
                    from (select asignatura_id, substr (grupo_id, 1, 1) grupo_id, subgrupo_id, tipo_subgrupo_id,
                                 ip.creditos crd_profesor, i.creditos crd_item,
                                 (select count (*)
                                    from uji_horarios.hor_items_profesores itp
                                   where item_id = i.id) cuantos_prof, 100 porcentaje, 0 idioma, 'S' coste,
                                 null comentario, profesor_id, area_id, ip.detalle_manual, estudio_id
                            from uji_horarios.hor_items i,
                                 uji_horarios.hor_items_asignaturas a,
                                 uji_horarios.hor_items_profesores ip,
                                 uji_horarios.hor_profesores p
                           where i.id = a.item_id
                             and i.id = ip.item_id
                             and comun = 0
                             and ip.profesor_id = p.id
                          --and asignatura_id = 'EI1001'
                          --and estudio_id = v_estudio
                          --and tipo_subgrupo_id = 'TE'
                          --and subgrupo_id = 1
                          --and 1 = 2
                          union
                          select asignatura_id, grupo_id, subgrupo_id, tipo_subgrupo_id, crd_profesor, crd_item,
                                 cuantos_prof, porcentaje, idioma, coste, comentario, profesor_id, area_id,
                                 detalle_manual, estudio_id
                            from (select distinct asignatura_id, substr (grupo_id, 1, 1) grupo_id, subgrupo_id,
                                                  tipo_subgrupo_id, ip.creditos crd_profesor, i.creditos crd_item,
                                                  (select count (*)
                                                     from uji_horarios.hor_items_profesores itp
                                                    where item_id = i.id) cuantos_prof, 0 idioma, 'S' coste,
                                                  null comentario, profesor_id, area_id,
                                                  (select porcentaje
                                                     from pod_grp_comunes c1,
                                                          pod_comunes c2
                                                    where c1.id = c2.gco_id
                                                      and curso_aca = :v_curso_aca
                                                      and asi_id = asignatura_id) porcentaje,
                                                  ip.detalle_manual, estudio_id
                                             from uji_horarios.hor_items i,
                                                  uji_horarios.hor_items_asignaturas a,
                                                  uji_horarios.hor_items_profesores ip,
                                                  uji_horarios.hor_profesores p
                                            where i.id = a.item_id
                                              and i.id = ip.item_id
                                              and comun = 1
                                              and ip.profesor_id = p.id
                                              --and asignatura_id = 'EM1045'
                                              --and estudio_id = v_estudio
                                              and asignatura_id not in (select asi_id
                                                                          from pod_asignaturas_area_comunes
                                                                         where curso_aca = :v_curso_aca)
                                                                                                       --and asignatura_id in ('AE1002', 'EC1002', 'FC1002')
                                                                                                                                                          --and tipo_subgrupo_id= 'PR'
                                                                                                                                                          --and subgrupo_id= 1
                                 )
                          --where 1 = 2
                          union all
                          select asignatura_id, grupo_id, subgrupo_id, tipo_subgrupo_id, crd_profesor, crd_item,
                                 cuantos_prof, porcentaje, idioma, coste, comentario, profesor_id, area_id,
                                 detalle_manual, estudio_id
                            from (select distinct asignatura_id, substr (grupo_id, 1, 1) grupo_id, subgrupo_id,
                                                  tipo_subgrupo_id, ip.creditos crd_profesor, i.creditos crd_item,
                                                  (select count (*)
                                                     from uji_horarios.hor_items_profesores itp
                                                    where item_id = i.id) cuantos_prof, 0 idioma, 'S' coste,
                                                  null comentario, profesor_id, area_id,
                                                  (select porcentaje
                                                     from pod_asignaturas_area_comunes
                                                    where curso_aca = :v_curso_aca
                                                      and asi_id = asignatura_id
                                                      and grp_id = grupo_id) porcentaje,
                                                  ip.detalle_manual, estudio_id
                                             from uji_horarios.hor_items i,
                                                  uji_horarios.hor_items_asignaturas a,
                                                  uji_horarios.hor_items_profesores ip,
                                                  uji_horarios.hor_profesores p
                                            where i.id = a.item_id
                                              and i.id = ip.item_id
                                              and comun = 1
                                              and ip.profesor_id = p.id
                                              --and asignatura_id = 'EM1045'
                                              --and estudio_id = v_estudio
                                              and asignatura_id in (select asi_id
                                                                      from pod_asignaturas_area_comunes
                                                                     where curso_aca = :v_curso_aca)))
                  -- where asignatura_id in ('EI1001')
                --and grupo_id = 'A'
                --and tipo_subgrupo_id = 'TE'
                --and subgrupo_id = 1
                group by asignatura_id,
                         grupo_id,
                         subgrupo_id,
                         tipo_subgrupo_id,
                         idioma,
                         coste,
                         comentario,
                         profesor_id,
                         area_id,
                         estudio_id,
                         cuantos_prof)
         where estudio_id between 201 and 9999
           and (asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo_id, creditos, profesor_id, area_id) not in (
                                  select sgr_grp_asi_id, sgr_grp_id, sgr_tipo, sgr_id, creditos, cdo_per_id,
                                         cdo_uest_id
                                    from pod_subgrupo_pdi
                                   where sgr_grp_curso_aca = 2014)
      order by 1,
               2,
               4,
               3;

   cursor lista_fechas (
      p_asi        in   varchar2,
      p_grp        in   varchar2,
      p_tipo       in   varchar2,
      p_subgrp     in   number,
      p_profesor   in   number
   ) is
      select distinct trunc (id.inicio) fecha, semestre_id, dia_Semana_id,
                      to_date ('01012000' || to_char (hora_inicio, 'hh24mi'), 'ddmmyyyyhh24mi') hora_inicio
                 from uji_horarios.hor_items i,
                      uji_horarios.hor_items_asignaturas a,
                      uji_horarios.hor_items_profesores ip,
                      uji_horarios.hor_profesores p,
                      uji_horarios.hor_items_detalle id,
                      uji_horarios.hor_items_det_profesores idp
                where i.id = a.item_id
                  and i.id = ip.item_id
                  and ip.profesor_id = p.id
                  and i.id = id.item_id
                  and asignatura_id = p_asi
                  and grupo_id = p_grp
                  and tipo_subgrupo_id = p_tipo
                  and subgrupo_id = p_subgrp
                  and ip.profesor_id = p_profesor
                  and id.id = idp.detalle_id
                  and ip.id = idp.item_profesor_id
             order by 1;

   cursor lista_inicial is
      select 'ALTER TRIGGER gra_pod.POD_AREA_CERRADA_SGP DISABLE' accion
        from dual;

   cursor lista_final is
      select 'ALTER TRIGGER gra_pod.POD_AREA_CERRADA_SGP enable' accion
        from dual;

   procedure ejecutar (p_accion in varchar2) is
   begin
      execute immediate p_accion;
   end;

   procedure desactivar_triggers is
   begin
      for x in lista_inicial loop
         ejecutar (x.accion);
      end loop;
   end;

   procedure activar_triggers is
   begin
      for x in lista_final loop
         ejecutar (x.accion);
      end loop;
   end;
begin
   desactivar_triggers;
   dbms_output.put_line (' ');
   dbms_output.put_line ('---------------------------------------------------------------');
   dbms_output.put_line (v_curso_aca || ' - ' || v_estudio);
   dbms_output.put_line (' ');

   for x in lista loop
      begin
         dbms_output.put_line ('insertar ' || x.asignatura_id || ' - ' || x.grupo_id || ' - ' || v_curso_aca || ' - '
                               || x.subgrupo_id || ' - ' || x.tipo_subgrupo_id || ' - ' || x.creditos || ' - '
                               || x.idioma || ' - ' || x.coste || ' - ' || x.comentario || ' - ' || x.profesor_id
                               || ' - ' || v_curso_aca || ' - ' || x.area_id || ' - detalla ' || x.detalle_manual);

         if v_acciones = 'S' then
            insert into pod_subgrupo_pdi
                        (SGR_GRP_ASI_ID, SGR_GRP_ID, SGR_GRP_CURSO_ACA, SGR_ID, SGR_TIPO, CREDITOS,
                         IDIOMA, COSTE, COMENTARIO, CDO_PER_ID, CDO_CURSO_ACA, CDO_UEST_ID
                        )
            values      (x.asignatura_id, x.grupo_id, v_curso_aca, x.subgrupo_id, x.tipo_subgrupo_id, x.creditos,
                         x.idioma, x.coste, x.comentario, x.profesor_id, v_curso_aca, x.area_id
                        );

            commit;
         end if;

         if x.detalle_manual = 1 then
            v_txt := null;
            v_cuantos := 0;
            v_encontrados := 0;
            dbms_output.put_line (' ----- detalle ' || x.asignatura_id || ' - ' || x.grupo_id || ' - ' || v_curso_aca
                                  || ' - ' || x.subgrupo_id || ' - ' || x.tipo_subgrupo_id || ' - ' || x.creditos
                                  || ' - ' || x.idioma || ' - ' || x.coste || ' - ' || x.comentario || ' - '
                                  || x.profesor_id || ' - ' || v_curso_aca || ' - ' || x.area_id || ' - detalla '
                                  || x.detalle_manual);

            for det in lista_fechas (x.asignatura_id, x.grupo_id, x.tipo_subgrupo_id, x.subgrupo_id, x.profesor_id) loop
               if v_txt is null then
                  v_txt := to_char (det.fecha, 'dd/mm/yy');
               else
                  v_txt := v_txt || ', ' || to_char (det.fecha, 'dd/mm/yy');
               end if;

               select count (*)
                 into v_aux
                 from pod_horarios
                where sgr_grp_asi_id = x.asignatura_id
                  and sgr_grp_id = x.grupo_id
                  and semestre = det.semestre_id
                  and sgr_grp_curso_aca = v_curso_aca
                  and sgr_id = x.subgrupo_id
                  and sgr_tipo = x.tipo_subgrupo_id
                  and dia_sem = det.dia_semana_id
                  and ini = det.hora_inicio;

               select count (*)
                 into v_aux_fechas
                 from pod_horarios_fechas
                where hor_sgr_grp_asi_id = x.asignatura_id
                  and hor_sgr_grp_id = x.grupo_id
                  and hor_semestre = det.semestre_id
                  and hor_sgr_grp_curso_aca = v_curso_aca
                  and hor_sgr_id = x.subgrupo_id
                  and hor_sgr_tipo = x.tipo_subgrupo_id
                  and hor_dia_sem = det.dia_semana_id
                  and hor_ini = det.hora_inicio
                  and fecha = det.fecha;

/*

            select *
              from pod_horarios_fechas
             where hor_sgr_grp_asi_id = 'MT1047'
               and hor_sgr_grp_id = 'A'
               --and hor_semestre = det.semestre_id
               and hor_sgr_grp_curso_aca = 2014
               and hor_sgr_id = 1
               and hor_sgr_tipo = 'LA'

               and hor_dia_sem = det.dia_semana_id
               and hor_ini = det.hora_inicio
               and fecha = det.fecha;

select *
from pod_subgrupo_pdi
where sgr_grp_asi_id='MT1047'
and sgr_grp_curso_aca =2014
and sgr_tipo ='TE'


      select   asignatura_id, substr (grupo_id, 1, 1) grupo_id, subgrupo_id, tipo_subgrupo_id,
               max (nvl (ip.creditos, i.creditos)) creditos, 0 idioma, 'S' coste, null comentario, profesor_id,
               area_id, ip.detalle_manual, comun
          from uji_horarios.hor_items i,
               uji_horarios.hor_items_asignaturas a,
               uji_horarios.hor_items_profesores ip,
               uji_horarios.hor_profesores p
         where i.id = a.item_id
           and i.id = ip.item_id
  --         and comun = 0
           and ip.profesor_id = p.id
           and asignatura_id = 'MT1047'
           and estudio_id = 223
      --and tipo_subgrupo_id = 'TE'
      group by asignatura_id,
               grupo_id,
               subgrupo_id,
               tipo_subgrupo_id,
               profesor_id,
               area_id,
               ip.detalle_manual, comun



*/
               v_cuantos := v_cuantos + 1;
               v_encontrados := v_encontrados + v_aux;

               if v_aux = 1 then
                  dbms_output.put_line ('  ----- -----  update ' || det.semestre_id || ' ' || det.dia_semana_id || ' '
                                        || to_char (det.hora_inicio, 'dd/mm/yyyy hh24:mi') || ' '
                                        || to_char (det.fecha, 'dd/mm/yyyy hh24:mi') || ' ' || v_aux || ' '
                                        || v_aux_fechas);

                  if v_acciones = 'S' then
                     update pod_horarios_fechas
                     set sgp_cdo_uest_id = x.area_id,
                         sgp_sgr_tipo = x.tipo_subgrupo_id,
                         sgp_cdo_curso_aca = v_curso_aca,
                         sgp_sgr_id = x.subgrupo_id,
                         sgp_sgr_grp_curso_aca = v_curso_aca,
                         sgp_cdo_per_id = x.profesor_id,
                         sgp_sgr_grp_asi_id = x.asignatura_id,
                         sgp_sgr_grp_id = x.grupo_id,
                         sgp_coste = 'S'
                     where  hor_sgr_grp_asi_id = x.asignatura_id
                        and hor_sgr_grp_id = x.grupo_id
                        and hor_semestre = det.semestre_id
                        and hor_sgr_grp_curso_aca = v_curso_aca
                        and hor_sgr_id = x.subgrupo_id
                        and hor_sgr_tipo = x.tipo_subgrupo_id
                        and hor_dia_sem = det.dia_semana_id
                        and hor_ini = det.hora_inicio
                        and fecha = det.fecha;

                     commit;
                  end if;
               end if;
            end loop;

            dbms_output.put_line ('  ----- update en fechas ' || v_cuantos || ' ' || v_encontrados);
         -- || v_txt);
         end if;
      exception
         when others then
            dbms_output.put_line ('ERROR  ----- ' || x.asignatura_id || ' ' || x.grupo_id || ' ' || v_curso_aca || ' '
                                  || x.subgrupo_id || ' ' || x.tipo_subgrupo_id || ' ' || x.creditos || ' ' || x.idioma
                                  || ' ' || x.coste || ' ' || x.comentario || ' ' || x.profesor_id || ' '
                                  || v_curso_aca || ' ' || x.area_id || ' ' || sqlerrm);
      end;
   end loop;

   activar_triggers;
end;




----------------------------------------------------




/* Formatted on 04/07/2014 09:49 (Formatter Plus v4.8.8) */
declare
   v_curso_aca   number := 2014;

   cursor lista_inicial is
      select 'ALTER TRIGGER gra_pod.POD_AREA_CERRADA_SGP DISABLE' accion
        from dual;

   cursor lista_final is
      select 'ALTER TRIGGER gra_pod.POD_AREA_CERRADA_SGP enable' accion
        from dual;

   procedure ejecutar (p_accion in varchar2) is
   begin
      execute immediate p_accion;
   end;

   procedure desactivar_triggers is
   begin
      for x in lista_inicial loop
         ejecutar (x.accion);
      end loop;
   end;

   procedure activar_triggers is
   begin
      for x in lista_final loop
         ejecutar (x.accion);
      end loop;
   end;
begin
   desactivar_triggers;

   update pod_horarios_fechas
   set sgp_cdo_uest_id = null,
       sgp_sgr_tipo = null,
       sgp_cdo_curso_aca = null,
       sgp_sgr_id = null,
       sgp_sgr_grp_curso_aca = null,
       sgp_cdo_per_id = null,
       sgp_sgr_grp_asi_id = null,
       sgp_sgr_grp_id = null,
       sgp_coste = null
   where  hor_sgr_grp_curso_aca = v_curso_aca
      and sgp_sgr_grp_curso_aca is not null;

   delete      pod_subgrupo_pdi
   where       sgr_grp_curso_aca = v_curso_aca;

   commit;
   activar_triggers;
end;











-------------------------------------------------------------------

consulta fuera de 15 minutos

   
    select distinct trunc (id.inicio) fecha, semestre_id, dia_Semana_id,
                      to_date ('01012000' || to_char (i.hora_inicio, 'hh24mi'), 'ddmmyyyyhh24mi') hora_inicio
                 from uji_horarios.hor_items i,
                      uji_horarios.hor_items_asignaturas a,
                      uji_horarios.hor_items_profesores ip,
                      uji_horarios.hor_profesores p,
                      uji_horarios.hor_items_detalle id,
                      uji_horarios.hor_items_det_profesores idp
                where i.id = a.item_id
                  and i.id = ip.item_id
                  and ip.profesor_id = p.id
                  and i.id = id.item_id
                  and id.id = idp.detalle_id
                  and ip.id = idp.item_profesor_id
                  and to_char(hora_inicio,'mi') not in ('00','15','30','45')
             order by 1;
			 
			 
			 
			 
			 
			 
			 
			 
			 
			 
			 
			 
			 
			 