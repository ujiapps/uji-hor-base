-- Generado por Oracle SQL Developer Data Modeler 3.3.0.744
--   en:        2014-06-11 09:10:08 CEST
--   sitio:      Oracle Database 11g
--   tipo:      Oracle Database 11g



DROP VIEW uji_horarios.HOR_EXT_ASIGNATURAS_COMUNES 
;
DROP VIEW uji_horarios.HOR_EXT_ASIGNATURAS_DETALLE 
;
DROP VIEW uji_horarios.HOR_EXT_CARGOS_PER 
;
DROP VIEW uji_horarios.HOR_EXT_CIRCUITOS 
;
DROP VIEW uji_horarios.HOR_EXT_PERSONAS 
;
DROP VIEW uji_horarios.HOR_V_ITEMS_DETALLE_COMPLETA 
;
DROP VIEW uji_horarios.hor_Ext_Asignaturas_area 
;
DROP VIEW uji_horarios.hor_v_asignaturas_area 
;
DROP VIEW uji_horarios.hor_v_aulas_personas 
;
DROP VIEW hor_v_creditos_profesor 
;
DROP VIEW uji_horarios.hor_v_cursos 
;
DROP VIEW hor_v_fechas_convocatorias 
;
DROP VIEW uji_horarios.hor_v_grupos 
;
DROP VIEW hor_v_items_creditos_detalle 
;
DROP VIEW hor_v_items_det_profesores 
;
DROP VIEW uji_horarios.hor_v_items_detalle 
;
DROP TABLE uji_horarios.HOR_ITEMS_CURSO_7 CASCADE CONSTRAINTS 
;
DROP TABLE uji_horarios.HOR_ITEMS_CURSO_7_ASIGNATURAS CASCADE CONSTRAINTS 
;
DROP TABLE uji_horarios.hor_areas CASCADE CONSTRAINTS 
;
DROP TABLE uji_horarios.hor_aulas CASCADE CONSTRAINTS 
;
DROP TABLE uji_horarios.hor_aulas_estudio CASCADE CONSTRAINTS 
;
DROP TABLE uji_horarios.hor_aulas_planificacion CASCADE CONSTRAINTS 
;
DROP TABLE uji_horarios.hor_centros CASCADE CONSTRAINTS 
;
DROP TABLE uji_horarios.hor_circuitos CASCADE CONSTRAINTS 
;
DROP TABLE uji_horarios.hor_circuitos_estudios CASCADE CONSTRAINTS 
;
DROP TABLE uji_horarios.hor_curso_academico CASCADE CONSTRAINTS 
;
DROP TABLE uji_horarios.hor_departamentos CASCADE CONSTRAINTS 
;
DROP TABLE uji_horarios.hor_dias_semana CASCADE CONSTRAINTS 
;
DROP TABLE uji_horarios.hor_estudios CASCADE CONSTRAINTS 
;
DROP TABLE uji_horarios.hor_estudios_compartidos CASCADE CONSTRAINTS 
;
DROP TABLE uji_horarios.hor_examenes CASCADE CONSTRAINTS 
;
DROP TABLE uji_horarios.hor_examenes_asignaturas CASCADE CONSTRAINTS 
;
DROP TABLE uji_horarios.hor_examenes_aulas CASCADE CONSTRAINTS 
;
DROP TABLE uji_horarios.hor_ext_asignaturas_comunes CASCADE CONSTRAINTS 
;
DROP TABLE uji_horarios.hor_ext_asignaturas_detalle CASCADE CONSTRAINTS 
;
DROP TABLE uji_horarios.hor_ext_calendario CASCADE CONSTRAINTS 
;
DROP TABLE uji_horarios.hor_ext_cargos_per CASCADE CONSTRAINTS 
;
DROP TABLE uji_horarios.hor_ext_circuitos CASCADE CONSTRAINTS 
;
DROP TABLE uji_horarios.hor_ext_personas CASCADE CONSTRAINTS 
;
DROP TABLE uji_horarios.hor_horarios_horas CASCADE CONSTRAINTS 
;
DROP TABLE uji_horarios.hor_informaciones CASCADE CONSTRAINTS 
;
DROP TABLE uji_horarios.hor_items CASCADE CONSTRAINTS 
;
DROP TABLE uji_horarios.hor_items_asignaturas CASCADE CONSTRAINTS 
;
DROP TABLE uji_horarios.hor_items_circuitos CASCADE CONSTRAINTS 
;
DROP TABLE uji_horarios.hor_items_comunes CASCADE CONSTRAINTS 
;
DROP TABLE uji_horarios.hor_items_det_profesores CASCADE CONSTRAINTS 
;
DROP TABLE uji_horarios.hor_items_detalle CASCADE CONSTRAINTS 
;
DROP TABLE uji_horarios.hor_items_profesores CASCADE CONSTRAINTS 
;
DROP TABLE uji_horarios.hor_logs CASCADE CONSTRAINTS 
;
DROP TABLE uji_horarios.hor_permisos_extra CASCADE CONSTRAINTS 
;
DROP TABLE uji_horarios.hor_profesores CASCADE CONSTRAINTS 
;
DROP TABLE uji_horarios.hor_semestres CASCADE CONSTRAINTS 
;
DROP TABLE uji_horarios.hor_semestres_detalle CASCADE CONSTRAINTS 
;
DROP TABLE uji_horarios.hor_tipos_cargos CASCADE CONSTRAINTS 
;
DROP TABLE uji_horarios.hor_tipos_estudios CASCADE CONSTRAINTS 
;
DROP TABLE uji_horarios.hor_tipos_examenes CASCADE CONSTRAINTS 
;
CREATE TABLE uji_horarios.HOR_ITEMS_CURSO_7 
    ( 
     ID NUMBER  NOT NULL , 
     CURSO_ID NUMBER  NOT NULL , 
     CARACTER_ID VARCHAR2 (10 CHAR)  NOT NULL , 
     CARACTER VARCHAR2 (100 CHAR) , 
     SEMESTRE_ID NUMBER  NOT NULL , 
     AULA_PLANIFICACION_ID NUMBER , 
     PROFESOR_ID NUMBER , 
     COMUN NUMBER  NOT NULL , 
     PORCENTAJE_COMUN NUMBER , 
     GRUPO_ID VARCHAR2 (10 CHAR)  NOT NULL , 
     TIPO_SUBGRUPO_ID VARCHAR2 (10 CHAR)  NOT NULL , 
     TIPO_SUBGRUPO VARCHAR2 (100 CHAR) , 
     SUBGRUPO_ID NUMBER  NOT NULL , 
     DIA_SEMANA_ID NUMBER , 
     HORA_INICIO DATE , 
     HORA_FIN DATE , 
     DESDE_EL_DIA DATE , 
     HASTA_EL_DIA DATE , 
     TIPO_ASIGNATURA_ID VARCHAR2 (10 CHAR) , 
     TIPO_ASIGNATURA VARCHAR2 (100 CHAR) , 
     TIPO_ESTUDIO_ID VARCHAR2 (10 CHAR) , 
     TIPO_ESTUDIO VARCHAR2 (100 CHAR) , 
     PLAZAS NUMBER , 
     REPETIR_CADA_SEMANAS NUMBER , 
     NUMERO_ITERACIONES NUMBER , 
     DETALLE_MANUAL NUMBER  NOT NULL , 
     COMUN_TEXTO VARCHAR2 (1000 CHAR) , 
     AULA_PLANIFICACION_NOMBRE VARCHAR2 (100 CHAR) 
    ) 
;




CREATE TABLE uji_horarios.HOR_ITEMS_CURSO_7_ASIGNATURAS 
    ( 
     ID NUMBER  NOT NULL , 
     ITEM_ID NUMBER  NOT NULL , 
     ASIGNATURA_ID VARCHAR2 (10 CHAR)  NOT NULL , 
     ASIGNATURA VARCHAR2 (1000 CHAR) , 
     ESTUDIO_ID NUMBER  NOT NULL , 
     ESTUDIO VARCHAR2 (1000 CHAR) 
    ) 
;




CREATE TABLE uji_horarios.hor_areas 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2 (100 CHAR)  NOT NULL , 
     departamento_id NUMBER  NOT NULL , 
     activa NUMBER DEFAULT 1  NOT NULL 
    ) 
;



ALTER TABLE uji_horarios.hor_areas 
    ADD 
    CHECK ( activa IN (0, 1)) 
;

CREATE UNIQUE INDEX uji_horarios.HOR_AREAS_PK ON uji_horarios.hor_areas 
    ( 
     id ASC 
    ) 
;

ALTER TABLE uji_horarios.hor_areas 
    ADD CONSTRAINT hor_areas_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_horarios.hor_aulas 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2 (100 CHAR)  NOT NULL , 
     centro_id NUMBER  NOT NULL , 
     tipo VARCHAR2 (100 CHAR) , 
     plazas NUMBER , 
     codigo VARCHAR2 (100 CHAR) , 
     area VARCHAR2 (10 CHAR) , 
     edificio VARCHAR2 (10 CHAR) , 
     planta VARCHAR2 (10 CHAR) 
    ) 
;


CREATE INDEX uji_horarios.hor_aulas_centro_IDX ON uji_horarios.hor_aulas 
    ( 
     centro_id ASC 
    ) 
;
CREATE UNIQUE INDEX uji_horarios.HOR_AULAS_PK ON uji_horarios.hor_aulas 
    ( 
     id ASC 
    ) 
;

ALTER TABLE uji_horarios.hor_aulas 
    ADD CONSTRAINT hor_aulas_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_horarios.hor_aulas_estudio 
    ( 
     id NUMBER  NOT NULL , 
     aula_id NUMBER  NOT NULL , 
     estudio_id NUMBER  NOT NULL , 
     descripcion VARCHAR2 
    ) 
;



ALTER TABLE uji_horarios.hor_aulas_estudio 
    ADD CONSTRAINT hor_aulas_estudio_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_horarios.hor_aulas_planificacion 
    ( 
     id NUMBER  NOT NULL , 
     aula_id NUMBER , 
     estudio_id NUMBER  NOT NULL , 
     semestre_id NUMBER 
    ) 
;


CREATE INDEX uji_horarios.hor_aulas_plan_aula_IDX ON uji_horarios.hor_aulas_planificacion 
    ( 
     aula_id ASC 
    ) 
;
CREATE INDEX uji_horarios.hor_aulas_plan_est_IDX ON uji_horarios.hor_aulas_planificacion 
    ( 
     estudio_id ASC 
    ) 
;
CREATE INDEX uji_horarios.hor_aulas_planif_hor_est_FK ON uji_horarios.hor_aulas_planificacion 
    ( 
     estudio_id ASC 
    ) 
;
CREATE UNIQUE INDEX uji_horarios.HOR_AULAS_PLANIFICACION_PK ON uji_horarios.hor_aulas_planificacion 
    ( 
     id ASC 
    ) 
;

ALTER TABLE uji_horarios.hor_aulas_planificacion 
    ADD CONSTRAINT hor_aulas_planificacion_PK PRIMARY KEY ( id ) ;


ALTER TABLE uji_horarios.hor_aulas_planificacion 
    ADD CONSTRAINT hor_aulas_planificacion__UN UNIQUE ( aula_id , estudio_id , semestre_id ) ;



CREATE TABLE uji_horarios.hor_centros 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2 (100 CHAR)  NOT NULL 
    ) 
;


CREATE UNIQUE INDEX uji_horarios.HOR_CENTROS_PK ON uji_horarios.hor_centros 
    ( 
     id ASC 
    ) 
;

ALTER TABLE uji_horarios.hor_centros 
    ADD CONSTRAINT hor_centros_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_horarios.hor_circuitos 
    ( 
     id NUMBER  NOT NULL , 
     grupo_id VARCHAR2 (10 CHAR)  NOT NULL , 
     nombre VARCHAR2 (100 CHAR)  NOT NULL , 
     plazas NUMBER  NOT NULL 
    ) 
;


CREATE UNIQUE INDEX uji_horarios.HOR_CIRCUITOS_EST_PK ON uji_horarios.hor_circuitos 
    ( 
     id ASC 
    ) 
;

ALTER TABLE uji_horarios.hor_circuitos 
    ADD CONSTRAINT hor_circuitos_est_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_horarios.hor_circuitos_estudios 
    ( 
     id NUMBER  NOT NULL , 
     circuito_id NUMBER  NOT NULL , 
     estudio_id NUMBER  NOT NULL 
    ) 
;


CREATE INDEX uji_horarios.hor_circuitos_est_cir_IDX ON uji_horarios.hor_circuitos_estudios 
    ( 
     circuito_id ASC 
    ) 
;
CREATE INDEX uji_horarios.hor_circuitos_est_est_IDX ON uji_horarios.hor_circuitos_estudios 
    ( 
     estudio_id ASC 
    ) 
;
CREATE UNIQUE INDEX uji_horarios.HOR_CIRCUITOS_ESTUDIOS_PK ON uji_horarios.hor_circuitos_estudios 
    ( 
     id ASC 
    ) 
;

ALTER TABLE uji_horarios.hor_circuitos_estudios 
    ADD CONSTRAINT hor_circuitos_estudios_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_horarios.hor_curso_academico 
    ( 
     id NUMBER  NOT NULL 
    ) 
;


CREATE UNIQUE INDEX uji_horarios.HOR_CURSO_ACADEMICO_PK ON uji_horarios.hor_curso_academico 
    ( 
     id ASC 
    ) 
;

ALTER TABLE uji_horarios.hor_curso_academico 
    ADD CONSTRAINT hor_curso_academico_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_horarios.hor_departamentos 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2 (100 CHAR)  NOT NULL , 
     centro_id NUMBER  NOT NULL , 
     activo NUMBER DEFAULT 1  NOT NULL , 
     consultar_pod NUMBER DEFAULT 0 
    ) 
;



ALTER TABLE uji_horarios.hor_departamentos 
    ADD 
    CHECK ( activo IN (0, 1)) 
;

CREATE UNIQUE INDEX uji_horarios.HOR_DEPARTAMENTOS_PK ON uji_horarios.hor_departamentos 
    ( 
     id ASC 
    ) 
;

ALTER TABLE uji_horarios.hor_departamentos 
    ADD CONSTRAINT hor_departamentos_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_horarios.hor_dias_semana 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2 (10 CHAR)  NOT NULL 
    ) 
;


CREATE UNIQUE INDEX uji_horarios.HOR_DIAS_SEMANA_PK ON uji_horarios.hor_dias_semana 
    ( 
     id ASC 
    ) 
;

ALTER TABLE uji_horarios.hor_dias_semana 
    ADD CONSTRAINT hor_dias_semana_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_horarios.hor_estudios 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2 (1000 CHAR)  NOT NULL , 
     tipo_id VARCHAR2 (10 CHAR)  NOT NULL , 
     centro_id NUMBER  NOT NULL , 
     oficial NUMBER DEFAULT 1  NOT NULL , 
     numero_cursos NUMBER 
    ) 
;



ALTER TABLE uji_horarios.hor_estudios 
    ADD 
    CHECK ( oficial IN (0, 1)) 
;

CREATE UNIQUE INDEX uji_horarios.HOR_ESTUDIOS_PK ON uji_horarios.hor_estudios 
    ( 
     id ASC 
    ) 
;

ALTER TABLE uji_horarios.hor_estudios 
    ADD CONSTRAINT hor_estudios_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_horarios.hor_estudios_compartidos 
    ( 
     id NUMBER  NOT NULL , 
     estudio_id NUMBER  NOT NULL , 
     estudio_id_compartido NUMBER  NOT NULL 
    ) 
;


CREATE INDEX uji_horarios.hor_estudios_comp_est_IDX ON uji_horarios.hor_estudios_compartidos 
    ( 
     estudio_id ASC 
    ) 
;
CREATE UNIQUE INDEX uji_horarios.HOR_ESTUDIOS_COMPARTIDOS_PK ON uji_horarios.hor_estudios_compartidos 
    ( 
     id ASC 
    ) 
;

ALTER TABLE uji_horarios.hor_estudios_compartidos 
    ADD CONSTRAINT hor_estudios_compartidos_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_horarios.hor_examenes 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2 (1000) , 
     fecha DATE , 
     hora_inicio DATE , 
     hora_fin DATE , 
     comun NUMBER , 
     comun_texto VARCHAR2 (100) , 
     convocatoria_id NUMBER , 
     convocatoria_nombre VARCHAR2 (100) , 
     tipo_examen_id NUMBER  NOT NULL , 
     comentario VARCHAR2 (1000) 
    ) 
;


CREATE INDEX uji_horarios.hor_examenes_tipo_Exa_IDX ON uji_horarios.hor_examenes 
    ( 
     tipo_examen_id ASC 
    ) 
;

ALTER TABLE uji_horarios.hor_examenes 
    ADD CONSTRAINT hor_examenes_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_horarios.hor_examenes_asignaturas 
    ( 
     id NUMBER  NOT NULL , 
     examen_id NUMBER  NOT NULL , 
     asignatura_id VARCHAR2 (10) , 
     asignatura_nombre VARCHAR2 (1000) , 
     estudio_id NUMBER  NOT NULL , 
     estudio_nombre VARCHAR2 (1000) , 
     semestre VARCHAR2 (10) , 
     tipo_asignatura VARCHAR2 (10) , 
     centro_id NUMBER , 
     curso_id NUMBER 
    ) 
;


CREATE INDEX uji_horarios.hor_examenes_asi_exa_IDX ON uji_horarios.hor_examenes_asignaturas 
    ( 
     examen_id ASC 
    ) 
;
CREATE INDEX uji_horarios.hor_examenes_asi_est_IDX ON uji_horarios.hor_examenes_asignaturas 
    ( 
     estudio_id ASC 
    ) 
;

ALTER TABLE uji_horarios.hor_examenes_asignaturas 
    ADD CONSTRAINT hor_examenes_asignaturas_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_horarios.hor_examenes_aulas 
    ( 
     id NUMBER  NOT NULL , 
     examen_id NUMBER  NOT NULL , 
     aula_id NUMBER  NOT NULL 
    ) 
;


CREATE INDEX uji_horarios.hor_examenes_aulas_exa_IDX ON uji_horarios.hor_examenes_aulas 
    ( 
     examen_id ASC 
    ) 
;
CREATE INDEX uji_horarios.hor_examenes_aulas_aula_IDX ON uji_horarios.hor_examenes_aulas 
    ( 
     aula_id ASC 
    ) 
;

ALTER TABLE uji_horarios.hor_examenes_aulas 
    ADD CONSTRAINT hor_examenes_aulas_PK PRIMARY KEY ( id ) ;


ALTER TABLE uji_horarios.hor_examenes_aulas 
    ADD CONSTRAINT hor_examenes_aulas__UN UNIQUE ( examen_id , aula_id ) ;



CREATE TABLE uji_horarios.hor_ext_asignaturas_comunes 
    ( 
     id NUMBER  NOT NULL , 
     grupo_comun_id NUMBER  NOT NULL , 
     nombre VARCHAR2  NOT NULL , 
     asignatura_id VARCHAR2  NOT NULL , 
     prefijo VARCHAR2 (2 CHAR) 
    ) 
;


CREATE INDEX uji_horarios.hor_ext_asi_comunes_nom_IDX ON uji_horarios.hor_ext_asignaturas_comunes 
    ( 
     nombre ASC 
    ) 
;
CREATE INDEX uji_horarios.hor_ext_asi_comunes_ca_IDX ON uji_horarios.hor_ext_asignaturas_comunes 
    ( 
     asignatura_id ASC 
    ) 
;

ALTER TABLE uji_horarios.hor_ext_asignaturas_comunes 
    ADD CONSTRAINT hor_ext_asignaturas_comunes_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_horarios.hor_ext_asignaturas_detalle 
    ( 
     asignatura_id VARCHAR2  NOT NULL , 
     nombre_asignatura VARCHAR2 , 
     creditos NUMBER , 
     ciclo NUMBER , 
     caracter VARCHAR2 , 
     crd_Te NUMBER , 
     crd_pr NUMBER , 
     crd_la NUMBER , 
     crd_se NUMBER , 
     crd_tu NUMBER , 
     crd_ev NUMBER , 
     crd_ctotal NUMBER 
    ) 
;



ALTER TABLE uji_horarios.hor_ext_asignaturas_detalle 
    ADD CONSTRAINT hor_ext_asignaturas_detalle_PK PRIMARY KEY ( asignatura_id ) ;



CREATE TABLE uji_horarios.hor_ext_calendario 
    ( 
     id NUMBER  NOT NULL , 
     dia NUMBER  NOT NULL , 
     mes NUMBER  NOT NULL , 
     a�o NUMBER  NOT NULL , 
     dia_semana VARCHAR2 (100 BYTE)  NOT NULL , 
     dia_semana_id NUMBER  NOT NULL , 
     tipo_dia VARCHAR2 (10 BYTE)  NOT NULL , 
     fecha DATE  NOT NULL , 
     vacaciones NUMBER DEFAULT 0  NOT NULL 
    ) 
;


CREATE INDEX uji_horarios.hor_ext_cal_fecha_idx ON uji_horarios.hor_ext_calendario 
    ( 
     fecha ASC , 
     tipo_dia ASC , 
     dia_semana_id ASC 
    ) 
;
CREATE INDEX uji_horarios.hor_ext_cal_fecha_vac_IDX ON uji_horarios.hor_ext_calendario 
    ( 
     fecha ASC , 
     tipo_dia ASC , 
     dia_semana_id ASC , 
     vacaciones ASC 
    ) 
;
CREATE UNIQUE INDEX uji_horarios.HOR_EXT_CALENDARIO_PK ON uji_horarios.hor_ext_calendario 
    ( 
     id ASC 
    ) 
;
CREATE UNIQUE INDEX uji_horarios.HOR_EXT_CAL_FECHA_DET__UN ON uji_horarios.hor_ext_calendario 
    ( 
     dia ASC , 
     mes ASC , 
     a�o ASC 
    ) 
;

ALTER TABLE uji_horarios.hor_ext_calendario 
    ADD CONSTRAINT hor_ext_calendario_PK PRIMARY KEY ( id ) ;


ALTER TABLE uji_horarios.hor_ext_calendario 
    ADD CONSTRAINT hor_ext_cal_fecha_UN UNIQUE ( fecha ) ;


ALTER TABLE uji_horarios.hor_ext_calendario 
    ADD CONSTRAINT hor_ext_cal_fecha_det__UN UNIQUE ( dia , mes , a�o ) ;



CREATE TABLE uji_horarios.hor_ext_cargos_per 
    ( 
     id NUMBER  NOT NULL , 
     persona_id NUMBER  NOT NULL , 
     nombre VARCHAR2 , 
     centro_id NUMBER , 
     centro VARCHAR2 , 
     estudio_id NUMBER , 
     estudio VARCHAR2 , 
     cargo_id NUMBER  NOT NULL , 
     cargo VARCHAR2 
    ) 
;



ALTER TABLE uji_horarios.hor_ext_cargos_per 
    ADD CONSTRAINT hor_ext_cargos_per_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_horarios.hor_ext_circuitos 
    ( 
     id NUMBER  NOT NULL , 
     curso_aca NUMBER  NOT NULL , 
     tipo VARCHAR2  NOT NULL , 
     subgrupo_id NUMBER  NOT NULL , 
     grupo_id VARCHAR2  NOT NULL , 
     detalle_id NUMBER  NOT NULL , 
     asignatura_id VARCHAR2  NOT NULL , 
     circuito_id NUMBER  NOT NULL , 
     estudio_id NUMBER  NOT NULL , 
     plazas NUMBER  NOT NULL 
    ) 
;



ALTER TABLE uji_horarios.hor_ext_circuitos 
    ADD CONSTRAINT hor_ext_circuitos_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_horarios.hor_ext_personas 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2  NOT NULL , 
     email VARCHAR2  NOT NULL , 
     actividad_id VARCHAR2  NOT NULL , 
     departamento_id NUMBER  NOT NULL , 
     nombre_buscar VARCHAR2 (1000) 
    ) 
;



ALTER TABLE uji_horarios.hor_ext_personas 
    ADD CONSTRAINT hor_ext_personas_PK PRIMARY KEY ( id ) ;


ALTER TABLE uji_horarios.hor_ext_personas 
    ADD CONSTRAINT hor_ext_personas__UN UNIQUE ( email ) ;



CREATE TABLE uji_horarios.hor_horarios_horas 
    ( 
     id NUMBER  NOT NULL , 
     estudio_id NUMBER  NOT NULL , 
     curso_id NUMBER  NOT NULL , 
     semestre_id NUMBER  NOT NULL , 
     grupo_id VARCHAR2 (10 CHAR)  NOT NULL , 
     hora_inicio DATE , 
     hora_fin DATE 
    ) 
;


CREATE INDEX uji_horarios.hor_horarios_horas_est_FK ON uji_horarios.hor_horarios_horas 
    ( 
     estudio_id ASC 
    ) 
;
CREATE UNIQUE INDEX uji_horarios.HOR_HORARIOS_HORAS_PK ON uji_horarios.hor_horarios_horas 
    ( 
     id ASC 
    ) 
;

ALTER TABLE uji_horarios.hor_horarios_horas 
    ADD CONSTRAINT hor_horarios_horas_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_horarios.hor_informaciones 
    ( 
     id NUMBER  NOT NULL , 
     texto VARCHAR2 (2000 CHAR) , 
     orden NUMBER  NOT NULL , 
     fecha_inicio DATE  NOT NULL , 
     fecha_fin DATE 
    ) 
;


CREATE UNIQUE INDEX uji_horarios.HOR_INFORMACIONES_PK ON uji_horarios.hor_informaciones 
    ( 
     id ASC 
    ) 
;

ALTER TABLE uji_horarios.hor_informaciones 
    ADD CONSTRAINT hor_informaciones_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_horarios.hor_items 
    ( 
     id NUMBER  NOT NULL , 
     curso_id NUMBER  NOT NULL , 
     caracter_id VARCHAR2 (10 CHAR)  NOT NULL , 
     caracter VARCHAR2 (100 CHAR) , 
     semestre_id NUMBER  NOT NULL , 
     aula_planificacion_id NUMBER , 
     profesor_id NUMBER , 
     comun NUMBER DEFAULT 0  NOT NULL , 
     porcentaje_comun NUMBER , 
     grupo_id VARCHAR2 (10 CHAR)  NOT NULL , 
     tipo_subgrupo_id VARCHAR2 (10 CHAR)  NOT NULL , 
     tipo_subgrupo VARCHAR2 (100 CHAR) , 
     subgrupo_id NUMBER  NOT NULL , 
     dia_semana_id NUMBER , 
     hora_inicio DATE , 
     hora_fin DATE , 
     desde_el_dia DATE , 
     hasta_el_dia DATE , 
     tipo_asignatura_id VARCHAR2 (10 CHAR) , 
     tipo_asignatura VARCHAR2 (100 CHAR) , 
     tipo_estudio_id VARCHAR2 (10 CHAR) , 
     tipo_estudio VARCHAR2 (100 CHAR) , 
     plazas NUMBER , 
     repetir_cada_semanas NUMBER , 
     numero_iteraciones NUMBER , 
     detalle_manual NUMBER DEFAULT 0  NOT NULL , 
     comun_texto VARCHAR2 (1000 CHAR) , 
     aula_planificacion_nombre VARCHAR2 (100 CHAR) , 
     creditos NUMBER 
    ) 
;



ALTER TABLE uji_horarios.hor_items 
    ADD 
    CHECK ( comun IN (0, 1)) 
;

CREATE INDEX uji_horarios.hor_items_v_idx ON uji_horarios.hor_items 
    ( 
     curso_id ASC , 
     semestre_id ASC , 
     grupo_id ASC , 
     tipo_subgrupo_id ASC , 
     subgrupo_id ASC , 
     dia_semana_id ASC 
    ) 
;
CREATE INDEX uji_horarios.hor_items_v2_IDX ON uji_horarios.hor_items 
    ( 
     id ASC , 
     dia_semana_id ASC , 
     detalle_manual ASC 
    ) 
;
CREATE INDEX uji_horarios.hor_item_det_man_idx ON uji_horarios.hor_items 
    ( 
     id ASC , 
     detalle_manual ASC 
    ) 
;
CREATE INDEX uji_horarios.hor_items_hor_aulas_plan_FK ON uji_horarios.hor_items 
    ( 
     aula_planificacion_id ASC 
    ) 
;
CREATE INDEX uji_horarios.hor_items_hor_aulas_plan_FK ON uji_horarios.hor_items 
    ( 
     aula_planificacion_id ASC 
    ) 
;
CREATE INDEX uji_horarios.hor_items_hor_aulas_plan_FK ON uji_horarios.hor_items 
    ( 
     aula_planificacion_id ASC 
    ) 
;
CREATE UNIQUE INDEX uji_horarios.TABLE_1_PK ON uji_horarios.hor_items 
    ( 
     id ASC 
    ) 
;
CREATE INDEX uji_horarios.hor_items_hor_aulas_plan_FK ON uji_horarios.hor_items 
    ( 
     aula_planificacion_id ASC 
    ) 
;
CREATE INDEX uji_horarios.hor_items_hor_aulas_plan_FK ON uji_horarios.hor_items 
    ( 
     aula_planificacion_id ASC 
    ) 
;
CREATE INDEX uji_horarios.hor_items_hor_aulas_plan_FK ON uji_horarios.hor_items 
    ( 
     aula_planificacion_id ASC 
    ) 
;

ALTER TABLE uji_horarios.hor_items 
    ADD CONSTRAINT TABLE_1_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_horarios.hor_items_asignaturas 
    ( 
     id NUMBER  NOT NULL , 
     item_id NUMBER  NOT NULL , 
     asignatura_id VARCHAR2 (10 CHAR)  NOT NULL , 
     asignatura VARCHAR2 (1000 CHAR) , 
     estudio_id NUMBER  NOT NULL , 
     estudio VARCHAR2 (1000 CHAR) 
    ) 
;


CREATE INDEX uji_horarios.hor_items_asig_it_IDX ON uji_horarios.hor_items_asignaturas 
    ( 
     item_id ASC 
    ) 
;
CREATE INDEX uji_horarios.hor_items_asig_est_IDX ON uji_horarios.hor_items_asignaturas 
    ( 
     estudio_id ASC 
    ) 
;
CREATE INDEX uji_horarios.hor_items_asig_est_asi_IDX ON uji_horarios.hor_items_asignaturas 
    ( 
     asignatura_id ASC , 
     estudio_id ASC 
    ) 
;
CREATE INDEX uji_horarios.hor_items_asig_items_FK ON uji_horarios.hor_items_asignaturas 
    ( 
     item_id ASC 
    ) 
;
CREATE INDEX uji_horarios.hor_items_asig_estudios_FK ON uji_horarios.hor_items_asignaturas 
    ( 
     estudio_id ASC 
    ) 
;
CREATE UNIQUE INDEX uji_horarios.HOR_ITEMS_ASIGNATURAS_PK ON uji_horarios.hor_items_asignaturas 
    ( 
     id ASC 
    ) 
;

ALTER TABLE uji_horarios.hor_items_asignaturas 
    ADD CONSTRAINT hor_items_asignaturas_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_horarios.hor_items_circuitos 
    ( 
     id NUMBER  NOT NULL , 
     item_id NUMBER  NOT NULL , 
     circuito_id NUMBER  NOT NULL 
    ) 
;


CREATE INDEX uji_horarios.hor_items_circuitos_item_IDX ON uji_horarios.hor_items_circuitos 
    ( 
     item_id ASC 
    ) 
;
CREATE INDEX uji_horarios.hor_items_circuitos_cir_IDX ON uji_horarios.hor_items_circuitos 
    ( 
     circuito_id ASC 
    ) 
;
CREATE UNIQUE INDEX uji_horarios.HOR_ITEMS_CIRCUITOS_PK ON uji_horarios.hor_items_circuitos 
    ( 
     id ASC 
    ) 
;

ALTER TABLE uji_horarios.hor_items_circuitos 
    ADD CONSTRAINT hor_items_circuitos_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_horarios.hor_items_comunes 
    ( 
     id NUMBER  NOT NULL , 
     item_id NUMBER  NOT NULL , 
     asignatura_id VARCHAR2 (100 CHAR) , 
     asignatura_comun_id VARCHAR2 (100 CHAR) , 
     item_comun_id NUMBER  NOT NULL 
    ) 
;


CREATE INDEX uji_horarios.hor_items_comunes__IDX ON uji_horarios.hor_items_comunes 
    ( 
     item_id ASC 
    ) 
;
CREATE INDEX uji_horarios.hor_items_comunes_com_IDX ON uji_horarios.hor_items_comunes 
    ( 
     item_comun_id ASC 
    ) 
;
CREATE INDEX uji_horarios.hor_items_comunes_it_FK ON uji_horarios.hor_items_comunes 
    ( 
     item_id ASC 
    ) 
;
CREATE INDEX uji_horarios.hor_items_comunes_it_com_FK ON uji_horarios.hor_items_comunes 
    ( 
     item_comun_id ASC 
    ) 
;
CREATE UNIQUE INDEX uji_horarios.HOR_ITEMS_COMUNES_PK ON uji_horarios.hor_items_comunes 
    ( 
     id ASC 
    ) 
;

ALTER TABLE uji_horarios.hor_items_comunes 
    ADD CONSTRAINT hor_items_comunes_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_horarios.hor_items_det_profesores 
    ( 
     id NUMBER  NOT NULL , 
     detalle_id NUMBER  NOT NULL , 
     item_profesor_id NUMBER  NOT NULL 
    ) 
;


CREATE INDEX uji_horarios.hor_items_det_prof_det_IDX ON uji_horarios.hor_items_det_profesores 
    ( 
     detalle_id ASC 
    ) 
;
CREATE INDEX uji_horarios.hor_items_det_prof_prof_IDX ON uji_horarios.hor_items_det_profesores 
    ( 
     item_profesor_id ASC 
    ) 
;

ALTER TABLE uji_horarios.hor_items_det_profesores 
    ADD CONSTRAINT hor_items_det_profesores_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_horarios.hor_items_detalle 
    ( 
     id NUMBER  NOT NULL , 
     item_id NUMBER  NOT NULL , 
     inicio DATE  NOT NULL , 
     fin DATE  NOT NULL , 
     descripcion VARCHAR2 (1000 CHAR) 
    ) 
;


CREATE INDEX uji_horarios.hor_items_detalle_fechas_IDX ON uji_horarios.hor_items_detalle 
    ( 
     item_id ASC , 
     inicio ASC 
    ) 
;
CREATE UNIQUE INDEX uji_horarios.HOR_ITEMS_DETALLE_PK ON uji_horarios.hor_items_detalle 
    ( 
     id ASC 
    ) 
;

ALTER TABLE uji_horarios.hor_items_detalle 
    ADD CONSTRAINT hor_items_detalle_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_horarios.hor_items_profesores 
    ( 
     id NUMBER  NOT NULL , 
     item_id NUMBER  NOT NULL , 
     profesor_id NUMBER  NOT NULL , 
     detalle_manual NUMBER DEFAULT 0 , 
     creditos NUMBER 
    ) 
;


CREATE INDEX uji_horarios.hor_items_prof_item_IDX ON uji_horarios.hor_items_profesores 
    ( 
     item_id ASC 
    ) 
;
CREATE INDEX uji_horarios.hor_items_prof_prof_IDX ON uji_horarios.hor_items_profesores 
    ( 
     profesor_id ASC 
    ) 
;

ALTER TABLE uji_horarios.hor_items_profesores 
    ADD CONSTRAINT hor_items_profesores_PK PRIMARY KEY ( id ) ;


ALTER TABLE uji_horarios.hor_items_profesores 
    ADD CONSTRAINT hor_items_profesores_UN UNIQUE ( item_id , profesor_id ) ;



CREATE TABLE uji_horarios.hor_logs 
    ( 
     id NUMBER  NOT NULL , 
     item_id NUMBER  NOT NULL , 
     fecha DATE  NOT NULL , 
     usuario VARCHAR2 (100 CHAR)  NOT NULL , 
     descripcion VARCHAR2 (1000 CHAR)  NOT NULL 
    ) 
;


CREATE INDEX uji_horarios.hor_logs_fecha_IDX ON uji_horarios.hor_logs 
    ( 
     fecha ASC 
    ) 
;
CREATE INDEX uji_horarios.hor_logs_usu_IDX ON uji_horarios.hor_logs 
    ( 
     usuario ASC 
    ) 
;
CREATE UNIQUE INDEX uji_horarios.HOR_LOGS_PK ON uji_horarios.hor_logs 
    ( 
     id ASC 
    ) 
;

ALTER TABLE uji_horarios.hor_logs 
    ADD CONSTRAINT hor_logs_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_horarios.hor_permisos_extra 
    ( 
     id NUMBER  NOT NULL , 
     persona_id NUMBER  NOT NULL , 
     tipo_cargo_id NUMBER  NOT NULL , 
     estudio_id NUMBER , 
     departamento_id NUMBER , 
     area_id NUMBER 
    ) 
;


CREATE UNIQUE INDEX uji_horarios.HOR_PERMISOS_EXTRA_PK ON uji_horarios.hor_permisos_extra 
    ( 
     id ASC 
    ) 
;

ALTER TABLE uji_horarios.hor_permisos_extra 
    ADD CONSTRAINT hor_permisos_extra_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_horarios.hor_profesores 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2 (100 CHAR)  NOT NULL , 
     email VARCHAR2 (100 CHAR) , 
     area_id NUMBER  NOT NULL , 
     departamento_id NUMBER  NOT NULL , 
     pendiente_contratacion NUMBER DEFAULT 0  NOT NULL , 
     creditos NUMBER , 
     creditos_maximos NUMBER , 
     creditos_reduccion NUMBER , 
     CATEGORIA_ID VARCHAR2 (10 CHAR) , 
     CATEGORIA_NOMBRE VARCHAR2 (1000 CHAR) 
    ) 
;



ALTER TABLE uji_horarios.hor_profesores 
    ADD 
    CHECK ( pendiente_contratacion IN (0, 1)) 
;

CREATE UNIQUE INDEX uji_horarios.HOR_PROFESORES_PK ON uji_horarios.hor_profesores 
    ( 
     id ASC 
    ) 
;

ALTER TABLE uji_horarios.hor_profesores 
    ADD CONSTRAINT hor_profesores_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_horarios.hor_semestres 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2 (100 CHAR)  NOT NULL 
    ) 
;


CREATE UNIQUE INDEX uji_horarios.HOR_SEMESTRES_PK ON uji_horarios.hor_semestres 
    ( 
     id ASC 
    ) 
;

ALTER TABLE uji_horarios.hor_semestres 
    ADD CONSTRAINT hor_semestres_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_horarios.hor_semestres_detalle 
    ( 
     id NUMBER  NOT NULL , 
     semestre_id NUMBER  NOT NULL , 
     tipo_estudio_id VARCHAR2 (10 CHAR)  NOT NULL , 
     fecha_inicio DATE  NOT NULL , 
     fecha_fin DATE  NOT NULL , 
     fecha_examenes_inicio DATE , 
     fecha_examenes_fin DATE , 
     numero_semanas NUMBER  NOT NULL , 
     curso_academico_id NUMBER  NOT NULL , 
     fechas_examenes_inicio_c2 DATE , 
     fechas_examenes_fin_c2 DATE 
    ) 
;


CREATE INDEX uji_horarios.hor_semestres_detalle_v_idx ON uji_horarios.hor_semestres_detalle 
    ( 
     tipo_estudio_id ASC 
    ) 
;
CREATE UNIQUE INDEX uji_horarios.HOR_SEMESTRES_DETALLE_PK ON uji_horarios.hor_semestres_detalle 
    ( 
     id ASC 
    ) 
;
CREATE UNIQUE INDEX uji_horarios.HOR_SEMESTRES_DETALLE__UN ON uji_horarios.hor_semestres_detalle 
    ( 
     semestre_id ASC , 
     tipo_estudio_id ASC 
    ) 
;

ALTER TABLE uji_horarios.hor_semestres_detalle 
    ADD CONSTRAINT hor_semestres_detalle_PK PRIMARY KEY ( id ) ;


ALTER TABLE uji_horarios.hor_semestres_detalle 
    ADD CONSTRAINT hor_semestres_detalle__UN UNIQUE ( semestre_id , tipo_estudio_id ) ;



CREATE TABLE uji_horarios.hor_tipos_cargos 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2 (100 CHAR)  NOT NULL , 
     departamento NUMBER DEFAULT 0  NOT NULL , 
     estudio NUMBER DEFAULT 0  NOT NULL 
    ) 
;


CREATE UNIQUE INDEX uji_horarios.HOR_TIPOS_CARGOS_PK ON uji_horarios.hor_tipos_cargos 
    ( 
     id ASC 
    ) 
;

ALTER TABLE uji_horarios.hor_tipos_cargos 
    ADD CONSTRAINT hor_tipos_cargos_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_horarios.hor_tipos_estudios 
    ( 
     id VARCHAR2 (10 CHAR)  NOT NULL , 
     nombre VARCHAR2 (100 CHAR)  NOT NULL , 
     orden NUMBER 
    ) 
;


CREATE UNIQUE INDEX uji_horarios.HOR_TIPOS_ESTUDIOS_PK ON uji_horarios.hor_tipos_estudios 
    ( 
     id ASC 
    ) 
;

ALTER TABLE uji_horarios.hor_tipos_estudios 
    ADD CONSTRAINT hor_tipos_estudios_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_horarios.hor_tipos_examenes 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2 (100)  NOT NULL , 
     por_defecto NUMBER DEFAULT 0  NOT NULL , 
     codigo VARCHAR2 (10)  NOT NULL 
    ) 
;



ALTER TABLE uji_horarios.hor_tipos_examenes 
    ADD CONSTRAINT hor_tipos_examenes_PK PRIMARY KEY ( id ) ;




ALTER TABLE uji_horarios.hor_items_comunes 
    ADD CONSTRAINT HOR_ITEMS_COMUNES_IT_COM_FK FOREIGN KEY 
    ( 
     item_comun_id
    ) 
    REFERENCES uji_horarios.hor_items 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_items_comunes 
    ADD CONSTRAINT HOR_ITEMS_COMUNES_IT_FK FOREIGN KEY 
    ( 
     item_id
    ) 
    REFERENCES uji_horarios.hor_items 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_areas 
    ADD CONSTRAINT hor_areas_hor_dep_FK FOREIGN KEY 
    ( 
     departamento_id
    ) 
    REFERENCES uji_horarios.hor_departamentos 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_aulas_estudio 
    ADD CONSTRAINT hor_aulas_estudio_aulas_FK FOREIGN KEY 
    ( 
     aula_id
    ) 
    REFERENCES uji_horarios.hor_aulas 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_aulas_estudio 
    ADD CONSTRAINT hor_aulas_estudio_est_FK FOREIGN KEY 
    ( 
     estudio_id
    ) 
    REFERENCES uji_horarios.hor_estudios 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_aulas 
    ADD CONSTRAINT hor_aulas_hor_centros_FK FOREIGN KEY 
    ( 
     centro_id
    ) 
    REFERENCES uji_horarios.hor_centros 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_aulas_planificacion 
    ADD CONSTRAINT hor_aulas_planif_aulas_FK FOREIGN KEY 
    ( 
     aula_id
    ) 
    REFERENCES uji_horarios.hor_aulas 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_aulas_planificacion 
    ADD CONSTRAINT hor_aulas_planif_hor_est_FK FOREIGN KEY 
    ( 
     estudio_id
    ) 
    REFERENCES uji_horarios.hor_estudios 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_circuitos_estudios 
    ADD CONSTRAINT hor_circuitos_est_cir_FK FOREIGN KEY 
    ( 
     circuito_id
    ) 
    REFERENCES uji_horarios.hor_circuitos 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_circuitos_estudios 
    ADD CONSTRAINT hor_circuitos_est_est_FK FOREIGN KEY 
    ( 
     estudio_id
    ) 
    REFERENCES uji_horarios.hor_estudios 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_departamentos 
    ADD CONSTRAINT hor_dep_hor_centros_FK FOREIGN KEY 
    ( 
     centro_id
    ) 
    REFERENCES uji_horarios.hor_centros 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_estudios_compartidos 
    ADD CONSTRAINT hor_estudios_comp_est1_FK FOREIGN KEY 
    ( 
     estudio_id
    ) 
    REFERENCES uji_horarios.hor_estudios 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_estudios_compartidos 
    ADD CONSTRAINT hor_estudios_comp_est2_FK FOREIGN KEY 
    ( 
     estudio_id_compartido
    ) 
    REFERENCES uji_horarios.hor_estudios 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_estudios 
    ADD CONSTRAINT hor_estudios_hor_centros_FK FOREIGN KEY 
    ( 
     centro_id
    ) 
    REFERENCES uji_horarios.hor_centros 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_estudios 
    ADD CONSTRAINT hor_estudios_hor_tipos_est_FK FOREIGN KEY 
    ( 
     tipo_id
    ) 
    REFERENCES uji_horarios.hor_tipos_estudios 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_examenes 
    ADD CONSTRAINT hor_exa_tipos_exa_FK FOREIGN KEY 
    ( 
     tipo_examen_id
    ) 
    REFERENCES uji_horarios.hor_tipos_examenes 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_examenes_asignaturas 
    ADD CONSTRAINT hor_examenes_asi_est_FK FOREIGN KEY 
    ( 
     estudio_id
    ) 
    REFERENCES uji_horarios.hor_estudios 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_examenes_asignaturas 
    ADD CONSTRAINT hor_examenes_asi_exa_FK FOREIGN KEY 
    ( 
     examen_id
    ) 
    REFERENCES uji_horarios.hor_examenes 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_examenes_aulas 
    ADD CONSTRAINT hor_examenes_aulas_aula_FK FOREIGN KEY 
    ( 
     aula_id
    ) 
    REFERENCES uji_horarios.hor_aulas 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_examenes_aulas 
    ADD CONSTRAINT hor_examenes_aulas_exa_FK FOREIGN KEY 
    ( 
     examen_id
    ) 
    REFERENCES uji_horarios.hor_examenes 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_ext_cargos_per 
    ADD CONSTRAINT hor_ext_car_per_est_FK FOREIGN KEY 
    ( 
     estudio_id
    ) 
    REFERENCES uji_horarios.hor_estudios 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_ext_cargos_per 
    ADD CONSTRAINT hor_ext_car_per_per_FK FOREIGN KEY 
    ( 
     persona_id
    ) 
    REFERENCES uji_horarios.hor_ext_personas 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_ext_cargos_per 
    ADD CONSTRAINT hor_ext_car_per_tip_car_FK FOREIGN KEY 
    ( 
     cargo_id
    ) 
    REFERENCES uji_horarios.hor_tipos_cargos 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_ext_personas 
    ADD CONSTRAINT hor_ext_per_dept_FK FOREIGN KEY 
    ( 
     departamento_id
    ) 
    REFERENCES uji_horarios.hor_departamentos 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_horarios_horas 
    ADD CONSTRAINT hor_horarios_horas_est_FK FOREIGN KEY 
    ( 
     estudio_id
    ) 
    REFERENCES uji_horarios.hor_estudios 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_items_asignaturas 
    ADD CONSTRAINT hor_items_asig_estudios_FK FOREIGN KEY 
    ( 
     estudio_id
    ) 
    REFERENCES uji_horarios.hor_estudios 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_items_asignaturas 
    ADD CONSTRAINT hor_items_asig_items_FK FOREIGN KEY 
    ( 
     item_id
    ) 
    REFERENCES uji_horarios.hor_items 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_items_circuitos 
    ADD CONSTRAINT hor_items_cir_cir_FK FOREIGN KEY 
    ( 
     circuito_id
    ) 
    REFERENCES uji_horarios.hor_circuitos 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_items_circuitos 
    ADD CONSTRAINT hor_items_cir_items_FK FOREIGN KEY 
    ( 
     item_id
    ) 
    REFERENCES uji_horarios.hor_items 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_items_det_profesores 
    ADD CONSTRAINT hor_items_det_prof_det_FK FOREIGN KEY 
    ( 
     detalle_id
    ) 
    REFERENCES uji_horarios.hor_items_detalle 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_items_det_profesores 
    ADD CONSTRAINT hor_items_det_prof_it_prof_FK FOREIGN KEY 
    ( 
     item_profesor_id
    ) 
    REFERENCES uji_horarios.hor_items_profesores 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_items_detalle 
    ADD CONSTRAINT hor_items_detalle_hor_items_FK FOREIGN KEY 
    ( 
     item_id
    ) 
    REFERENCES uji_horarios.hor_items 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_items 
    ADD CONSTRAINT hor_items_hor_aulas_FK FOREIGN KEY 
    ( 
     aula_planificacion_id
    ) 
    REFERENCES uji_horarios.hor_aulas 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_items 
    ADD CONSTRAINT hor_items_hor_aulas_plan_FK FOREIGN KEY 
    ( 
     aula_planificacion_id
    ) 
    REFERENCES uji_horarios.hor_aulas_planificacion 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_items 
    ADD CONSTRAINT hor_items_hor_dias_semana_FK FOREIGN KEY 
    ( 
     dia_semana_id
    ) 
    REFERENCES uji_horarios.hor_dias_semana 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_items 
    ADD CONSTRAINT hor_items_hor_profesores_FK FOREIGN KEY 
    ( 
     semestre_id
    ) 
    REFERENCES uji_horarios.hor_semestres 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_items_profesores 
    ADD CONSTRAINT hor_items_prof_items_FK FOREIGN KEY 
    ( 
     item_id
    ) 
    REFERENCES uji_horarios.hor_items 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_items_profesores 
    ADD CONSTRAINT hor_items_prof_prof_FK FOREIGN KEY 
    ( 
     profesor_id
    ) 
    REFERENCES uji_horarios.hor_profesores 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_permisos_extra 
    ADD CONSTRAINT hor_perm_ext_areas_FK FOREIGN KEY 
    ( 
     area_id
    ) 
    REFERENCES uji_horarios.hor_areas 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_permisos_extra 
    ADD CONSTRAINT hor_perm_ext_dept_FK FOREIGN KEY 
    ( 
     departamento_id
    ) 
    REFERENCES uji_horarios.hor_departamentos 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_permisos_extra 
    ADD CONSTRAINT hor_perm_ext_est_FK FOREIGN KEY 
    ( 
     estudio_id
    ) 
    REFERENCES uji_horarios.hor_estudios 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_permisos_extra 
    ADD CONSTRAINT hor_perm_ext_per_FK FOREIGN KEY 
    ( 
     persona_id
    ) 
    REFERENCES uji_horarios.hor_ext_personas 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_permisos_extra 
    ADD CONSTRAINT hor_perm_ext_tip_car_FK FOREIGN KEY 
    ( 
     tipo_cargo_id
    ) 
    REFERENCES uji_horarios.hor_tipos_cargos 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_profesores 
    ADD CONSTRAINT hor_profesores_hor_areas_FK FOREIGN KEY 
    ( 
     area_id
    ) 
    REFERENCES uji_horarios.hor_areas 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_semestres_detalle 
    ADD CONSTRAINT hor_sem_det_sem_FK FOREIGN KEY 
    ( 
     semestre_id
    ) 
    REFERENCES uji_horarios.hor_semestres 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_semestres_detalle 
    ADD CONSTRAINT hor_sem_det_tipos_est_FK FOREIGN KEY 
    ( 
     tipo_estudio_id
    ) 
    REFERENCES uji_horarios.hor_tipos_estudios 
    ( 
     id
    ) 
;

CREATE OR REPLACE VIEW uji_horarios.HOR_EXT_ASIGNATURAS_COMUNES ( ID, GRUPO_COMUN_ID, NOMBRE, ASIGNATURA_ID ) AS
select rownum id, id grupo_comun_id, nombre, asi_id asignatura_id
   from   pod_grp_comunes g,
          pod_comunes c
   where  g.id = c.gco_id
   and    curso_aca = (select max (curso_academico_id)
                       from   uji_horarios.hor_semestres_detalle) ;



CREATE OR REPLACE VIEW uji_horarios.HOR_EXT_ASIGNATURAS_DETALLE ( ASIGNATURA_ID, NOMBRE_ASIGNATURA, CREDITOS, CICLO, CARACTER, CRD_TE, CRD_PR, CRD_LA, CRD_SE, CRD_TU, CRD_EV, CRD_CTOTAL, TIPO ) AS
select v.asi_id asignatura_id, nom_asi nombre_asignatura, a.creditos, cicle ciclo,
          decode (v.tit_id,
                  9, v.caracter,
                  26, v.caracter,
                  31, v.caracter,
                  decode (v.caracter, 'TR', 'FB', 'LC', 'PE', v.caracter)
                 ) caracter,
          crd_te, crd_pr, crd_la, crd_se, crd_tu, crd_ev, crd_ctotal, a.tipo
   from   gra_pod.pod_vsp_27 v,
          gra_pod.pod_asignaturas a,
          gra_pod.pod_asig_sin_coste sc,
          gra_pod.pod_asignaturas_titulaciones atit,
          uji_horarios.hor_curso_academico ca
   where  v.curso_Aca = ca.id
   and    v.asi_id = a.id
   and    v.curso_aca = sc.curso_aca(+)
   and    v.asi_id = sc.asi_id(+)
   and    v.tit_id = atit.tit_id
   and    v.asi_id = atit.asi_id ;



CREATE OR REPLACE VIEW uji_horarios.HOR_EXT_CARGOS_PER  AS
SELECT ROWNUM id,
  persona_id,
  nombre,
  centro_id,
  centro,
  titulacion_id estudio_id,
  titulacion estudio,
  cargo_id,
  cargo,
  departamento_id,
  departamento
FROM
  (SELECT cp.per_id persona_id,
    p.nombre
    || ' '
    || apellido1
    || ' '
    || apellido2 nombre,
    ulogica_id centro_id,
    ubic.nombre centro,
    tit.id titulacion_id,
    tit.nombre titulacion,
    tc.id cargo_id,
    tc.nombre cargo,
    NULL departamento_id,
    '' departamento
  FROM grh_grh.grh_cargos_per cp,
    gri_est.est_ubic_estructurales ubic,
    gra_Exp.exp_v_titu_todas tit,
    hor_tipos_cargos tc,
    gri_per.per_personas p
  WHERE cp.per_id = p.id
  AND (ulogica_id = ubic.id
  AND tc.id       = 3
  AND (f_fin     IS NULL
  AND (f_fin     IS NULL
  OR f_fin       >= SYSDATE)
  AND crg_id     IN (189, 195, 188, 30))
  AND ulogica_id  = tit.uest_id
  AND tit.activa  = 'S'
  AND tit.tipo    = 'G')
  UNION ALL
  SELECT cp.per_id persona_id,
    p.nombre
    || ' '
    || apellido1
    || ' '
    || apellido2 nombre,
    ubicacion_id centro_id,
    ubic.nombre centro,
    tit.id titulacion_id,
    tit.nombre titulacion,
    tc.id cargo_id,
    tc.nombre cargo,
    NULL departamento_id,
    '' departamento
  FROM grh_grh.grh_vw_contrataciones_ult cp,
    gri_est.est_ubic_estructurales ubic,
    gra_Exp.exp_v_titu_todas tit,
    hor_tipos_cargos tc,
    gri_per.per_personas p
  WHERE cp.per_id   = p.id
  AND (ubicacion_id = ubic.id
  AND tc.id         = 4
  AND ubicacion_id  = tit.uest_id
  AND tit.activa    = 'S'
  AND tit.tipo      = 'G')
  UNION ALL
  SELECT cp.per_id persona_id,
    p.nombre
    || ' '
    || apellido1
    || ' '
    || apellido2 nombre,
    ulogica_id centro_id,
    ubic.nombre centro,
    tit.id titulacion_id,
    tit.nombre titulacion,
    tc.id cargo_id,
    tc.nombre cargo,
    NULL departamento_id,
    '' departamento
  FROM grh_grh.grh_cargos_per cp,
    gri_est.est_ubic_estructurales ubic,
    gra_Exp.exp_v_titu_todas tit,
    hor_tipos_cargos tc,
    gri_per.per_personas p
  WHERE cp.per_id = p.id
  AND cp.tit_id   = tit.id
  AND (ulogica_id = ubic.id
  AND tc.id       = 1
  AND (f_fin     IS NULL
  AND (f_fin     IS NULL
  OR f_fin       >= SYSDATE)
  AND crg_id     IN (192, 193))
  AND ulogica_id  = tit.uest_id
  AND tit.activa  = 'S'
  AND tit.tipo    = 'G')
  UNION ALL
  SELECT cp.per_id persona_id,
    p.nombre
    || ' '
    || apellido1
    || ' '
    || apellido2 nombre,
    c.id centro_id,
    c.nombre centro,
    NULL titulacion_id,
    '' titulacion,
    tc.id cargo_id,
    tc.nombre cargo,
    ulogica_id departamento_id,
    d.nombre departamento
  FROM grh_grh.grh_cargos_per cp,
    hor_tipos_cargos tc,
    gri_per.per_personas p,
    est_ubic_estructurales d,
    est_relaciones_ulogicas r,
    est_ubic_estructurales c
  WHERE cp.per_id        = p.id
  AND (tc.id             = 6
  AND ulogica_id         = d.id
  AND (f_fin            IS NULL
  AND (f_fin            IS NULL
  OR f_fin              >= SYSDATE)
  AND crg_id            IN (178, 194))
  AND uest_id_relacionad = d.id
  AND d.tuest_id         = 'DE'
  AND uest_id            = c.id
  AND trel_id            = 4)
  UNION ALL
  SELECT cp.per_id persona_id,
    p.nombre
    || ' '
    || apellido1
    || ' '
    || apellido2 nombre,
    c.id centro_id,
    c.nombre centro,
    NULL titulacion_id,
    '' titulacion,
    tc.id cargo_id,
    tc.nombre cargo,
    ubic.id departamento_id,
    ubic.nombre departamento
  FROM grh_grh.grh_vw_contrataciones_ult cp,
    gri_est.est_ubic_estructurales ubic,
    hor_tipos_cargos tc,
    gri_per.per_personas p,
    est_relaciones_ulogicas r,
    est_ubic_estructurales c
  WHERE cp.per_id  = p.id
  AND (tc.id       = 5
  AND ubicacion_id = ubic.id
  AND cp.act_id    = 'PAS'
  AND ubicacion_id = uest_id_relacionad
  AND trel_id      = 4
  AND uest_id      = c.id)
  UNION ALL
  SELECT per.persona_id,
    p.nombre
    || ' '
    || apellido1
    || ' '
    || apellido2 nombre,
    ubic.id centro_id,
    ubic.nombre centro,
    tit.id titulacion_id,
    tit.nombre titulacion,
    tc.id cargo_id,
    tc.nombre cargo,
    per.departamento_id,
    dep.nombre departamento
  FROM hor_permisos_extra per,
    gri_per.per_personas p,
    gra_Exp.exp_v_titu_todas tit,
    hor_tipos_cargos tc,
    gri_est.est_ubic_estructurales ubic,
    gri_est.est_ubic_estructurales dep
  WHERE per.persona_id    = p.id
  AND per.estudio_id      = tit.id(+)
  AND per.tipo_cargo_id   = tc.id
  AND tit.uest_id         = ubic.id
  AND per.departamento_id = dep.id(+)
  AND (tit.activa         = 'S'
  AND tit.tipo            = 'G')
  UNION ALL
  SELECT DISTINCT per.persona_id,
    p.nombre
    || ' '
    || apellido1
    || ' '
    || apellido2 nombre,
    u.id centro_id,
    u.nombre centro,
    tit.id titulacion_id,
    tit.nombre titulacion,
    tc.id cargo_id,
    tc.nombre cargo,
    NULL departamento_id,
    '' departamento
  FROM uji_apa.apa_vw_personas_items per,
    gri_per.per_personas p,
    gra_Exp.exp_v_titu_todas tit,
    hor_tipos_cargos tc,
    gri_Est.est_ubic_estructurales u
  WHERE tit.uest_id = u.id
  AND (tc.id        = 1
  AND persona_id    = p.id
  AND tit.activa    = 'S'
  AND tit.tipo      = 'G'
  AND aplicacion_id = 46
  AND role          = 'ADMIN')
  UNION ALL
  SELECT DISTINCT per.persona_id,
    p.nombre
    || ' '
    || apellido1
    || ' '
    || apellido2 nombre,
    c.id centro_id,
    c.nombre centro,
    NULL titulacion_id,
    '' titulacion,
    tc.id cargo_id,
    tc.nombre cargo,
    dep.id departamento_id,
    dep.nombre departamento
  FROM uji_apa.apa_vw_personas_items per,
    gri_per.per_personas p,
    est_ubic_estructurales dep,
    hor_tipos_cargos tc,
    gri_Est.est_ubic_estructurales c,
    est_relaciones_ulogicas r
  WHERE dep.id      = r.uest_id_relacionad
  AND r.uest_id     = c.id
  AND (tc.id        = 6
  AND persona_id    = p.id
  AND dep.status    = 'A'
  AND dep.tuest_id  = 'DE'
  AND aplicacion_id = 46
  AND role          = 'ADMIN'
  AND r.trel_id     = 4)
  ) ;



CREATE OR REPLACE VIEW uji_horarios.HOR_EXT_CIRCUITOS ( ID, CURSO_ACA, ESTUDIO_ID, ASIGNATURA_ID, GRUPO_ID, TIPO, SUBGRUPO_ID, DETALLE_ID, CIRCUITO_ID, PLAZAS ) AS
select rownum id, SGD_SGR_GRP_CURSO_ACA curso_aca, CIC_TIT_ID estudio_id, SGD_SGR_GRP_ASI_ID asignatura_id,
          SGD_SGR_GRP_ID grupo_id, SGD_SGR_TIPO tipo, SGD_SGR_ID subgrupo_id, SGD_ID detalle_id, CIC_ID circuito_id,
          limite plazas
   from   pod_circuitos_det c,
          pod_subgrupos_det d
   where  sgd_sgr_grp_curso_aca >= 2012
   and    c.SGD_SGR_GRP_CURSO_ACA = d.sgr_grp_curso_aca
   and    c.SGD_SGR_TIPO = d.sgr_tipo
   and    c.SGD_SGR_ID = d.sgr_id
   and    c.SGD_SGR_GRP_ID = d.sgr_grp_id
   and    c.SGD_SGR_GRP_ASI_ID = d.sgr_grp_asi_id
   and    c.SGD_ID = d.id ;



CREATE OR REPLACE VIEW uji_horarios.HOR_EXT_PERSONAS ( ID, NOMBRE, EMAIL, ACTIVIDAD_ID, DEPARTAMENTO_ID, NOMBRE_BUSCAR ) AS
SELECT   per_id id,
            nombre,
            busca_cuenta (per_id) email,
            act_id actividad_id,
            ubicacion_id departamento_id,
            cmp_util.limpia_txt (nombre) nombre_buscar
     FROM   grh_vw_contrataciones_ult c
    WHERE   act_id IN ('PAS', 'PDI')
            /*AND ubicacion_id IN (SELECT   id
                                   FROM   est_ubic_estructurales
                                  WHERE   tuest_id = 'DE' AND status = 'A')*/
            AND act_id = (SELECT   MAX (act_id)
                            FROM   grh_vw_contrataciones_ult c2
                           WHERE   c.per_id = c2.per_id)
   UNION ALL
   SELECT   8126 id,
            'ALVARI�O GALDO, CRIST�BAL',
            'calvarin@uji.es',
            'PAS',
            218,
            cmp_util.limpia_txt ('ALVARI�O GALDO, CRIST�BAL') nombre_buscar
     FROM   DUAL
   UNION ALL
   SELECT   160900 id,
            'G�MEZ SIRVENT, MAR�A',
            'msirvent@uji.es',
            'PAS',
            218,
            cmp_util.limpia_txt ('G�MEZ SIRVENT, MAR�A') nombre_buscar
     FROM   DUAL ;



CREATE OR REPLACE VIEW uji_horarios.HOR_V_ITEMS_DETALLE_COMPLETA ( ID, FECHA, DOCENCIA_PASO_1, DOCENCIA_PASO_2, DOCENCIA, ORDEN_ID, NUMERO_ITERACIONES, REPETIR_CADA_SEMANAS, FECHA_INICIO, FECHA_FIN, ESTUDIO_ID, SEMESTRE_ID, CURSO_ID, ASIGNATURA_ID, GRUPO_ID, TIPO_SUBGRUPO_ID, SUBGRUPO_ID, DIA_SEMANA_ID, TIPO_DIA, FESTIVOS ) AS
SELECT i.id, fecha, docencia docencia_paso_1,
          DECODE (NVL (d.repetir_cada_semanas, 1),
                  1, docencia,
                  DECODE (MOD (orden_id, d.repetir_cada_semanas), 1, docencia, 'N')
                 ) docencia_paso_2,
          decode (tipo_dia,
                  'F', 'N',
                  DECODE (d.numero_iteraciones,
                          NULL, DECODE (NVL (d.repetir_cada_semanas, 1),
                                        1, docencia,
                                        DECODE (MOD (orden_id, d.repetir_cada_semanas), 1, docencia, 'N')
                                       ),
                          DECODE (SIGN (((orden_id - festivos) / d.repetir_cada_Semanas) - (d.numero_iteraciones)),
                                  1, 'N',
                                  DECODE (NVL (d.repetir_cada_semanas, 1),
                                          1, docencia,
                                          DECODE (MOD (orden_id, d.repetir_cada_semanas), 1, docencia, 'N')
                                         )
                                 )
                         )
                 ) docencia,
          d.orden_id, d.numero_iteraciones, d.repetir_cada_semanas, d.fecha_inicio, d.fecha_fin, d.estudio_id,
          d.semestre_id, d.curso_id, d.asignatura_id, d.grupo_id, d.tipo_subgrupo_id, d.subgrupo_id, d.dia_semana_id,
          tipo_dia, festivos
   FROM   (SELECT id, fecha,
                  hor_contar_festivos (NVL (x.desde_el_dia, fecha_inicio), x.fecha, x.dia_semana_id,
                                       x.repetir_cada_semanas) festivos,
                  ROW_NUMBER () OVER (PARTITION BY DECODE
                                                       (decode (sign (x.fecha - fecha_inicio),
                                                                -1, 'N',
                                                                decode (sign (fecha_fin - x.fecha), -1, 'N', 'S')
                                                               ),
                                                        'S', DECODE (decode (sign (x.fecha
                                                                                   - NVL (x.desde_el_dia, fecha_inicio)),
                                                                             -1, 'N',
                                                                             decode (sign (NVL (x.hasta_el_dia,
                                                                                                fecha_fin)
                                                                                           - x.fecha),
                                                                                     -1, 'N',
                                                                                     'S'
                                                                                    )
                                                                            ),
                                                                     'S', 'S',
                                                                     'N'
                                                                    ),
                                                        'N'
                                                       ), id, estudio_id, semestre_id, curso_id, asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo_id, dia_semana_id ORDER BY fecha)
                                                                                                               orden_id,
                  DECODE (hor_f_fecha_entre (x.fecha, fecha_inicio, fecha_fin),
                          'S', DECODE (hor_f_fecha_entre (x.fecha, NVL (desde_el_dia, fecha_inicio),
                                                          NVL (hasta_el_dia, fecha_fin)),
                                       'S', 'S',
                                       'N'
                                      ),
                          'N'
                         ) docencia,
                  estudio_id, curso_id, semestre_id, grupo_id, tipo_subgrupo_id, subgrupo_id, dia_Semana_id,
                  asignatura_id, fecha_inicio, fecha_fin, fecha_examenes_inicio, fecha_examenes_fin, desde_el_dia,
                  hasta_el_dia, repetir_cada_semanas, numero_iteraciones, detalle_manual, tipo_dia, dia_semana
           FROM   (SELECT i.id, ia.estudio_id, i.curso_id, i.semestre_id, i.grupo_id, i.tipo_subgrupo_id, i.subgrupo_id,
                          i.dia_Semana_id, ia.asignatura_id, fecha_inicio, fecha_fin, fecha_examenes_inicio,
                          fecha_examenes_fin, i.desde_el_dia, hasta_el_dia, repetir_cada_semanas, numero_iteraciones,
                          detalle_manual, c.fecha, tipo_dia, dia_semana
                   FROM   hor_estudios e,
                          hor_semestres_detalle s,
                          hor_items i,
                          hor_items_asignaturas ia,
                          hor_ext_calendario c
                   WHERE  i.id = ia.item_id
                   and    e.tipo_id = s.tipo_estudio_id
                   AND    ia.estudio_id = e.id
                   AND    i.semestre_id = s.semestre_id
                   AND    trunc (c.fecha) BETWEEN fecha_inicio AND NVL (fecha_examenes_fin, fecha_fin)
                   AND    c.dia_semana_id = i.dia_semana_id
                   AND    tipo_dia IN ('L', 'E', 'F')
                   and    vacaciones = 0
                   AND    detalle_manual = 0) x) d,
          hor_items i,
          hor_items_asignaturas ia
   WHERE  i.id = ia.item_id
   and    ia.estudio_id = d.estudio_id
   AND    i.curso_id = d.curso_id
   AND    i.semestre_id = d.semestre_id
   AND    ia.asignatura_id = d.asignatura_id
   AND    i.grupo_id = d.grupo_id
   AND    i.tipo_subgrupo_id = d.tipo_subgrupo_id
   AND    i.subgrupo_id = d.subgrupo_id
   AND    i.dia_semana_id = d.dia_semana_id
   AND    i.detalle_manual = 0
   AND    i.id = d.id
   UNION ALL
   SELECT c.id, c.fecha, 'N' docencia_paso_1, 'N' docencia_paso_2, DECODE (d.id, NULL, 'N', 'S') docencia, 1 orden_id,
          numero_iteraciones, repetir_cada_semanas, fecha_inicio, fecha_fin, estudio_id, semestre_id, curso_id,
          asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo_id, dia_semana_id, tipo_dia,
          decode (tipo_dia, 'F', 1, 0) festivos
   FROM   (SELECT i.id, c.fecha, numero_iteraciones, repetir_cada_semanas, s.fecha_inicio, s.fecha_fin, ia.estudio_id,
                  i.semestre_id, i.curso_id, ia.asignatura_id, i.grupo_id, i.tipo_subgrupo_id, i.subgrupo_id,
                  i.dia_semana_id, tipo_dia
           FROM   hor_estudios e,
                  hor_semestres_detalle s,
                  hor_items i,
                  hor_items_asignaturas ia,
                  hor_ext_calendario c
           WHERE  i.id = ia.item_id
           and    e.tipo_id = s.tipo_estudio_id
           AND    ia.estudio_id = e.id
           AND    i.semestre_id = s.semestre_id
           AND    trunc (c.fecha) BETWEEN fecha_inicio AND NVL (fecha_examenes_fin, fecha_fin)
           AND    c.dia_semana_id = i.dia_semana_id
           AND    tipo_dia IN ('L', 'E', 'F')
           and    vacaciones = 0
           AND    detalle_manual = 1) c,
          hor_items_detalle d
   WHERE  c.id = d.item_id(+)
   AND    trunc (c.fecha) = trunc (d.inicio(+)) ;



CREATE OR REPLACE VIEW uji_horarios.hor_Ext_Asignaturas_area  AS
SELECT ASI_ID,
  UEST_ID,
  RECIBE_ACTA,
  PORCENTAJE,
  CURSO_ACA
FROM pod_asignaturas_area p,
  hor_curso_academico c
WHERE curso_aca = c.id
AND porcentaje  > 0 ;



CREATE OR REPLACE VIEW uji_horarios.hor_v_asignaturas_area  AS
SELECT DISTINCT asignatura_id,
  uest_id area_id,
  porcentaje,
  recibe_acta
FROM hor_ext_asignaturas_area aa,
  hor_items_asignaturas a
WHERE asignatura_id = asi_id ;



CREATE OR REPLACE VIEW uji_horarios.hor_v_aulas_personas ( PERSONA_ID, AULA_ID, NOMBRE, CENTRO_ID, CENTRO, CODIGO, TIPO ) AS
SELECT   DISTINCT c.persona_id,
                     ap.aula_id,
                     a.nombre,
                     a.centro_id,
                     cen.nombre centro,
                     codigo,
                     tipo
     FROM   uji_horarios.hor_ext_Cargos_per c,
            uji_horarios.hor_aulas_planificacion ap,
            uji_horarios.hor_aulas a,
            uji_horarios.hor_Centros cen
    WHERE       c.estudio_id = ap.estudio_id
            AND ap.aula_id = a.id
            AND a.centro_id = cen.id ;



CREATE OR REPLACE VIEW hor_v_items_creditos_detalle  AS
SELECT id,
  nombre,
  creditos_profesor,
  item_id,
  crd_prof,
  crd_item,
  cuantos,
  NVL(crd_prof, TRUNC(crd_item / cuantos, 2)) crd_final,
  GRUPO_ID,
  TIPO_SUBGRUPO_ID
  || SUBGRUPO_ID tipo,
  asignatura_id,
  asignatura_txt,
  departamento_id,
  area_id
FROM
  (SELECT h.id,
    nombre,
    h.creditos creditos_profesor,
    ip.item_id,
    ip.creditos crd_prof,
    i.creditos crd_item,
    (SELECT COUNT(*) FROM hor_items_profesores ip2 WHERE item_id = ip.item_id
    ) cuantos,
    GRUPO_ID,
    TIPO_SUBGRUPO_ID,
    SUBGRUPO_ID,
    wm_concat(asignatura_id) asignatura_txt,
    MIN(asignatura_id) asignatura_id,
    departamento_id,
    area_id
  FROM hor_profesores h,
    hor_items_profesores ip,
    hor_items i,
    hor_items_asignaturas ia
  WHERE h.id     = ip.profesor_id
  AND ip.item_id = i.id
  AND i.id       = ia.item_id
  GROUP BY h.id,
    nombre,
    h.creditos,
    ip.item_id,
    ip.creditos,
    i.creditos,
    GRUPO_ID,
    TIPO_SUBGRUPO_ID,
    SUBGRUPO_ID,
    departamento_id,
    area_id
  ) ;



CREATE OR REPLACE VIEW hor_v_creditos_profesor  AS
SELECT id,
  nombre,
  creditos_profesor,
  SUM(crd_final) creditos_asignados,
  creditos_profesor - SUM(crd_final) creditos_pendientes,
  departamento_id,
  area_id
FROM
  (SELECT hor_v_items_creditos_detalle.id,
    hor_v_items_creditos_detalle.nombre,
    hor_v_items_creditos_detalle.creditos_profesor,
    hor_v_items_creditos_detalle.GRUPO_ID,
    hor_v_items_creditos_detalle.tipo,
    hor_v_items_creditos_detalle.asignatura_id,
    MIN(hor_v_items_creditos_detalle.asignatura_txt),
    hor_v_items_creditos_detalle.crd_final,
    hor_v_items_creditos_detalle.departamento_id,
    hor_v_items_creditos_detalle.area_id
  FROM hor_v_items_creditos_detalle
  GROUP BY hor_v_items_creditos_detalle.id,
    hor_v_items_creditos_detalle.nombre,
    hor_v_items_creditos_detalle.creditos_profesor,
    hor_v_items_creditos_detalle.GRUPO_ID,
    hor_v_items_creditos_detalle.tipo,
    hor_v_items_creditos_detalle.asignatura_id,
    hor_v_items_creditos_detalle.crd_final,
    hor_v_items_creditos_detalle.departamento_id,
    hor_v_items_creditos_detalle.area_id
  )
GROUP BY id,
  nombre,
  creditos_profesor,
  departamento_id,
  area_id ;



CREATE OR REPLACE VIEW uji_horarios.hor_v_cursos ( ESTUDIO_ID, ESTUDIO, CURSO_ID ) AS
SELECT   DISTINCT
            hor_items_asignaturas.estudio_id,
            hor_items_asignaturas.estudio,
            hor_items.curso_id
     FROM   hor_items_asignaturas, hor_items
    WHERE   hor_items.id = hor_items_asignaturas.item_id
    AND     hor_items.curso_id < 7 ;



CREATE OR REPLACE VIEW hor_v_fechas_convocatorias  AS
SELECT DISTINCT convocatoria_id id,
  convocatoria_nombre nombre,
  s.fecha_examenes_inicio,
  s.fecha_examenes_fin
FROM hor_examenes e,
  hor_semestres_detalle s
WHERE (e.convocatoria_id = 9
AND semestre_id          = 1)
OR (e.convocatoria_id    = 10
AND semestre_id          = 2)
UNION ALL
SELECT DISTINCT convocatoria_id,
  convocatoria_nombre,
  s.fechas_examenes_inicio_c2,
  s.fechas_examenes_fin_c2
FROM hor_examenes e,
  hor_semestres_detalle s
WHERE e.convocatoria_id = 11 ;



CREATE OR REPLACE VIEW uji_horarios.hor_v_grupos  AS
SELECT DISTINCT hor_items_asignaturas.estudio_id,
  hor_items_asignaturas.estudio,
  hor_items.grupo_id,
  DECODE(hor_items.grupo_id, 'Y', 'Grupo ARA', '') especial
FROM hor_items,
  hor_items_asignaturas
WHERE hor_items.id = hor_items_asignaturas.item_id ;



CREATE OR REPLACE VIEW hor_v_items_det_profesores  AS
SELECT to_number(id
  || det_profesor_id
  || item_id
  || TO_CHAR(inicio, 'yyyymmddhh24miss')) id,
  id detalle_id,
  det_profesor_id,
  item_id,
  inicio,
  fin,
  descripcion,
  profesor_id,
  detalle_manual,
  creditos,
  creditos_detalle,
  DECODE(detalle_manual, 0, 'S', DECODE(MIN(orden), 1, 'S', 'N')) seleccionado,
  nombre,
  profesor_completo
FROM
  (SELECT d.*,
    profesor_id,
    p.detalle_manual,
    i.creditos,
    p.creditos creditos_detalle,
    p.id det_profesor_id,
    99 orden,
    nombre,
    (SELECT wm_concat(nombre)
    FROM hor_items_profesores pr,
      hor_profesores p
    WHERE pr.profesor_id = p.id
    AND (item_id         = i.id)
    ) profesor_completo
  FROM hor_items i,
    hor_items_detalle d,
    hor_items_profesores p,
    hor_profesores pro
  WHERE i.id        = d.item_id
  AND i.id          = p.item_id
  AND p.profesor_id = pro.id
  UNION ALL
  SELECT d.*,
    profesor_id,
    p.detalle_manual,
    i.creditos,
    p.creditos creditos_detalle,
    p.id det_profesor_id,
    1 orden,
    nombre,
    (SELECT wm_concat(nombre)
    FROM hor_items_profesores pr,
      hor_profesores p
    WHERE pr.profesor_id = p.id
    AND (item_id         = i.id)
    ) profesor_completo
  FROM hor_items i,
    hor_items_detalle d,
    hor_items_profesores p,
    hor_items_det_profesores dp,
    hor_profesores pro
  WHERE i.id            = d.item_id
  AND i.id              = p.item_id
  AND dp.detalle_id     = d.id
  AND p.profesor_id     = pro.id
  AND (p.detalle_manual = 1
  AND item_profesor_id  = p.id)
  )
GROUP BY id,
  det_profesor_id,
  item_id,
  inicio,
  fin,
  descripcion,
  profesor_id,
  detalle_manual,
  creditos,
  creditos_detalle,
  nombre,
  profesor_completo ;



CREATE OR REPLACE VIEW uji_horarios.hor_v_items_detalle ( ID, FECHA, DOCENCIA_PASO_1, DOCENCIA_PASO_2, DOCENCIA, ORDEN_ID, NUMERO_ITERACIONES, REPETIR_CADA_SEMANAS, FECHA_INICIO, FECHA_FIN, ESTUDIO_ID, SEMESTRE_ID, CURSO_ID, ASIGNATURA_ID, GRUPO_ID, TIPO_SUBGRUPO_ID, SUBGRUPO_ID, DIA_SEMANA_ID, TIPO_DIA, FESTIVOS ) AS
SELECT i.id, fecha, docencia docencia_paso_1,
          DECODE (NVL (d.repetir_cada_semanas, 1),
                  1, docencia,
                  DECODE (MOD (orden_id, d.repetir_cada_semanas), 1, docencia, 'N')
                 ) docencia_paso_2,
          decode (tipo_dia,
                  'F', 'N',
                  DECODE (d.numero_iteraciones,
                          NULL, DECODE (NVL (d.repetir_cada_semanas, 1),
                                        1, docencia,
                                        DECODE (MOD (orden_id, d.repetir_cada_semanas), 1, docencia, 'N')
                                       ),
                          DECODE (SIGN (((orden_id - festivos) / d.repetir_cada_Semanas) - (d.numero_iteraciones)),
                                  1, 'N',
                                  DECODE (NVL (d.repetir_cada_semanas, 1),
                                          1, docencia,
                                          DECODE (MOD (orden_id, d.repetir_cada_semanas), 1, docencia, 'N')
                                         )
                                 )
                         )
                 ) docencia,
          d.orden_id, d.numero_iteraciones, d.repetir_cada_semanas, d.fecha_inicio, d.fecha_fin, d.estudio_id,
          d.semestre_id, d.curso_id, d.asignatura_id, d.grupo_id, d.tipo_subgrupo_id, d.subgrupo_id, d.dia_semana_id,
          tipo_dia, festivos
   FROM   (SELECT id, fecha,
                  hor_contar_festivos (NVL (x.desde_el_dia, fecha_inicio), x.fecha, x.dia_semana_id,
                                       x.repetir_cada_semanas) festivos,
                  ROW_NUMBER () OVER (PARTITION BY DECODE
                                                       (decode (sign (x.fecha - fecha_inicio),
                                                                -1, 'N',
                                                                decode (sign (fecha_fin - x.fecha), -1, 'N', 'S')
                                                               ),
                                                        'S', DECODE (decode (sign (x.fecha
                                                                                   - NVL (x.desde_el_dia, fecha_inicio)),
                                                                             -1, 'N',
                                                                             decode (sign (NVL (x.hasta_el_dia,
                                                                                                fecha_fin)
                                                                                           - x.fecha),
                                                                                     -1, 'N',
                                                                                     'S'
                                                                                    )
                                                                            ),
                                                                     'S', 'S',
                                                                     'N'
                                                                    ),
                                                        'N'
                                                       ), id, estudio_id, semestre_id, curso_id, asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo_id, dia_semana_id ORDER BY fecha)
                                                                                                               orden_id,
                  DECODE (hor_f_fecha_entre (x.fecha, fecha_inicio, fecha_fin),
                          'S', DECODE (hor_f_fecha_entre (x.fecha, NVL (desde_el_dia, fecha_inicio),
                                                          NVL (hasta_el_dia, fecha_fin)),
                                       'S', 'S',
                                       'N'
                                      ),
                          'N'
                         ) docencia,
                  estudio_id, curso_id, semestre_id, grupo_id, tipo_subgrupo_id, subgrupo_id, dia_Semana_id,
                  asignatura_id, fecha_inicio, fecha_fin, fecha_examenes_inicio, fecha_examenes_fin, desde_el_dia,
                  hasta_el_dia, repetir_cada_semanas, numero_iteraciones, detalle_manual, tipo_dia, dia_semana
           FROM   (SELECT i.id, null estudio_id, i.curso_id, i.semestre_id, i.grupo_id, i.tipo_subgrupo_id,
                          i.subgrupo_id, i.dia_Semana_id, null asignatura_id, fecha_inicio, fecha_fin,
                          fecha_examenes_inicio, fecha_examenes_fin, i.desde_el_dia, hasta_el_dia, repetir_cada_semanas,
                          numero_iteraciones, detalle_manual, c.fecha, tipo_dia, dia_semana
                   FROM   hor_semestres_detalle s,
                          hor_items i,
                          hor_ext_calendario c
                   WHERE  i.semestre_id = s.semestre_id
                   AND    trunc (c.fecha) BETWEEN fecha_inicio AND NVL (fecha_examenes_fin, fecha_fin)
                   AND    c.dia_semana_id = i.dia_semana_id
                   AND    tipo_dia IN ('L', 'E', 'F')
                   and    vacaciones = 0
                   AND    detalle_manual = 0) x) d,
          hor_items i
   WHERE  i.curso_id = d.curso_id
   AND    i.semestre_id = d.semestre_id
   AND    i.grupo_id = d.grupo_id
   AND    i.tipo_subgrupo_id = d.tipo_subgrupo_id
   AND    i.subgrupo_id = d.subgrupo_id
   AND    i.dia_semana_id = d.dia_semana_id
   AND    i.detalle_manual = 0
   AND    i.id = d.id
   UNION ALL
   SELECT c.id, c.fecha, 'N' docencia_paso_1, 'N' docencia_paso_2, DECODE (d.id, NULL, 'N', 'S') docencia, 1 orden_id,
          numero_iteraciones, repetir_cada_semanas, fecha_inicio, fecha_fin, estudio_id, semestre_id, curso_id,
          asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo_id, dia_semana_id, tipo_dia,
          decode (tipo_dia, 'F', 1, 0) festivos
   FROM   (SELECT i.id, c.fecha, numero_iteraciones, repetir_cada_semanas, s.fecha_inicio, s.fecha_fin, null estudio_id,
                  i.semestre_id, i.curso_id, null asignatura_id, i.grupo_id, i.tipo_subgrupo_id, i.subgrupo_id,
                  i.dia_semana_id, tipo_dia
           FROM   hor_semestres_detalle s,
                  hor_items i,
                  hor_ext_calendario c
           WHERE  i.semestre_id = s.semestre_id
           AND    trunc (c.fecha) BETWEEN fecha_inicio AND NVL (fecha_examenes_fin, fecha_fin)
           AND    c.dia_semana_id = i.dia_semana_id
           AND    tipo_dia IN ('L', 'E', 'F')
           and    vacaciones = 0
           AND    detalle_manual = 1) c,
          hor_items_detalle d
   WHERE  c.id = d.item_id(+)
   AND    trunc (c.fecha) = trunc (d.inicio(+)) ;





-- Informe de Resumen de Oracle SQL Developer Data Modeler: 
-- 
-- CREATE TABLE                            40
-- CREATE INDEX                            73
-- ALTER TABLE                             96
-- CREATE VIEW                             16
-- CREATE PACKAGE                           0
-- CREATE PACKAGE BODY                      0
-- CREATE PROCEDURE                         0
-- CREATE FUNCTION                          0
-- CREATE TRIGGER                           0
-- ALTER TRIGGER                            0
-- CREATE COLLECTION TYPE                   0
-- CREATE STRUCTURED TYPE                   0
-- CREATE STRUCTURED TYPE BODY              0
-- CREATE CLUSTER                           0
-- CREATE CONTEXT                           0
-- CREATE DATABASE                          0
-- CREATE DIMENSION                         0
-- CREATE DIRECTORY                         0
-- CREATE DISK GROUP                        0
-- CREATE ROLE                              0
-- CREATE ROLLBACK SEGMENT                  0
-- CREATE SEQUENCE                          0
-- CREATE MATERIALIZED VIEW                 0
-- CREATE SYNONYM                           0
-- CREATE TABLESPACE                        0
-- CREATE USER                              0
-- 
-- DROP TABLESPACE                          0
-- DROP DATABASE                            0
-- 
-- ERRORS                                   0
-- WARNINGS                                 0
