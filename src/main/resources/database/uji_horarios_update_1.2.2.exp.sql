CREATE TABLE UJI_HORARIOS.hor_Semestres_detalle_estudio
(
  id                        NUMBER              NOT NULL,
  detalle_id                NUMBER              NOT NULL,
  estudio_id                NUMBER              NOT NULL,
  curso_id                  NUMBER              NOT NULL,
  fecha_inicio              DATE,
  fecha_fin                 DATE,
  fecha_Examenes_inicio     DATE,
  fecha_examenes_fin        DATE,
  numero_semanas            NUMBER,
  fecha_examenes_inicio_c2  DATE,
  fecha_examenes_fin_c2     DATE
);


ALTER TABLE UJI_HORARIOS.hor_Semestres_detalle_estudio ADD (
  CONSTRAINT hor_Semestres_det_estu_PK
 PRIMARY KEY
 (id));


ALTER TABLE UJI_HORARIOS.HOR_SEMESTRES_DETALLE_ESTUDIO ADD (
  CONSTRAINT HOR_SEMESTRE_DET_EST_DET_FK 
 FOREIGN KEY (DETALLE_ID) 
 REFERENCES UJI_HORARIOS.HOR_SEMESTRES_DETALLE (ID));

 
Insert into UJI_HORARIOS.HOR_SEMESTRES_DETALLE_ESTUDIO
   (ID, DETALLE_ID, ESTUDIO_ID, CURSO_ID, FECHA_INICIO, FECHA_FIN, FECHA_EXAMENES_INICIO, FECHA_EXAMENES_FIN, NUMERO_SEMANAS, FECHA_EXAMENES_INICIO_C2, FECHA_EXAMENES_FIN_C2)
 Values
   (1, 1, 217, 3, TO_DATE('09/10/2014 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), TO_DATE('02/07/2015 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), TO_DATE('02/08/2015 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), TO_DATE('02/27/2015 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 19, TO_DATE('02/08/2015 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), TO_DATE('02/27/2015 00:00:00', 'MM/DD/YYYY HH24:MI:SS'));
COMMIT;

CREATE OR REPLACE FORCE VIEW UJI_HORARIOS.HOR_EXT_CONVOCATORIAS (ID,
                                                                 NOMBRE,
                                                                 NOMCAS,
                                                                 ORDEN,
                                                                 NOMANG,
                                                                 TIPO,
                                                                 EXTRA,
                                                                 NOMBRE_CERT_CA,
                                                                 NOMBRE_CERT_ES,
                                                                 NOMBRE_CERT_UK
                                                                ) AS
   select "ID", "NOMBRE", "NOMCAS", "ORDEN", "NOMANG", "TIPO", "EXTRA", "NOMBRE_CERT_CA", "NOMBRE_CERT_ES",
          "NOMBRE_CERT_UK"
     from pod_convocatorias
    where id in (9, 10, 11);

	
CREATE OR REPLACE FORCE VIEW UJI_HORARIOS.HOR_V_FECHAS_CONVOCATORIAS (ID,
                                                                      NOMBRE,
                                                                      FECHA_EXAMENES_INICIO,
                                                                      FECHA_EXAMENES_FIN
                                                                     ) AS
   SELECT DISTINCT convocatoria_id id, c.nombre nombre, s.fecha_examenes_inicio, s.fecha_examenes_fin
              FROM hor_examenes e,
                   hor_semestres_detalle s,
                   hor_ext_convocatorias c
             WHERE (   (    e.convocatoria_id = 9
                        AND semestre_id = 1)
                    OR (    e.convocatoria_id = 10
                        AND semestre_id = 2))
               and e.convocatoria_id = c.id
   UNION ALL
   SELECT DISTINCT convocatoria_id, c.nombre, s.fechas_examenes_inicio_c2, s.fechas_examenes_fin_c2
              FROM hor_examenes e,
                   hor_semestres_detalle s,
                   hor_ext_convocatorias c
             WHERE (e.convocatoria_id = 11)
               and e.convocatoria_id = c.id;


CREATE OR REPLACE FORCE VIEW UJI_HORARIOS.HOR_V_FECHAS_ESTUDIOS (ID,
                                                                 ESTUDIO_ID,
                                                                 ESTUDIO_NOMBRE,
                                                                 TIPO_ESTUDIO_ID,
                                                                 SEMESTRE_ID,
                                                                 SEMESTRE_NOMBRE,
                                                                 CURSO_ID,
                                                                 FECHA_INICIO,
                                                                 FECHA_FIN,
                                                                 FECHA_EXAMENES_INICIO,
                                                                 FECHA_EXAMENES_FIN,
                                                                 FECHAS_EXAMENES_INICIO_C2,
                                                                 FECHAS_EXAMENES_FIN_C2,
                                                                 NUMERO_SEMANAS
                                                                ) AS
   select   id, "ESTUDIO_ID", "ESTUDIO_NOMBRE", tipo_estudio_id, "SEMESTRE_ID", "SEMESTRE_NOMBRE", "CURSO_ID",
            "FECHA_INICIO", "FECHA_FIN", FECHA_EXAMENES_INICIO, FECHA_EXAMENES_FIN, FECHAS_EXAMENES_INICIO_C2,
            FECHAS_EXAMENES_FIN_C2, NUMERO_SEMANAS
       from (select distinct to_number (e.id || curso_id || semestre_id) id, e.id estudio_id, e.nombre estudio_nombre,
                             tipo_estudio_id, s.id semestre_id, s.nombre semestre_nombre, i.curso_id, d.fecha_inicio,
                             fecha_fin, FECHA_EXAMENES_INICIO, FECHA_EXAMENES_FIN, FECHAS_EXAMENES_INICIO_C2,
                             FECHAS_EXAMENES_FIN_C2, NUMERO_SEMANAS
                        from hor_Estudios e,
                             hor_semestres s,
                             hor_semestres_detalle d,
                             hor_items_asignaturas i
                       where s.id = d.semestre_id
                         and e.tipo_id = d.tipo_estudio_id
                         --and e.id in (217, 218)
                         and e.id = i.estudio_id)
      where (estudio_id, semestre_id, curso_id) not in (
               select e.id estudio_id, s.id semestre_id, curso_id
                 from hor_Estudios e,
                      hor_semestres s,
                      hor_semestres_detalle d,
                      hor_semestres_detalle_estudio de
                where s.id = d.semestre_id
                  and e.tipo_id = d.tipo_estudio_id
                  and d.id = de.detalle_id
                  and e.id = de.estudio_id)
   union
   select   to_number (e.id || curso_id || semestre_id) id, e.id estudio_id, e.nombre estudio_nombre, tipo_estudio_id,
            s.id semestre_id, s.nombre semestre_nombre, curso_id, de.fecha_inicio, de.fecha_fin,
            de.FECHA_EXAMENES_INICIO, de.FECHA_EXAMENES_FIN, de.FECHA_EXAMENES_INICIO_C2, de.FECHA_EXAMENES_FIN_C2,
            de.NUMERO_SEMANAS
       from hor_Estudios e,
            hor_semestres s,
            hor_semestres_detalle d,
            hor_semestres_detalle_estudio de
      where s.id = d.semestre_id
        and e.tipo_id = d.tipo_estudio_id
        and d.id = de.detalle_id
        and e.id = de.estudio_id
   order by 1,
            5,
            3;			
			
			
			
CREATE OR REPLACE FORCE VIEW UJI_HORARIOS.HOR_V_ITEMS_DETALLE (ID,
                                                               FECHA,
                                                               DOCENCIA_PASO_1,
                                                               DOCENCIA_PASO_2,
                                                               DOCENCIA,
                                                               ORDEN_ID,
                                                               NUMERO_ITERACIONES,
                                                               REPETIR_CADA_SEMANAS,
                                                               FECHA_INICIO,
                                                               FECHA_FIN,
                                                               ESTUDIO_ID,
                                                               SEMESTRE_ID,
                                                               ASIGNATURA_ID,
                                                               GRUPO_ID,
                                                               TIPO_SUBGRUPO_ID,
                                                               SUBGRUPO_ID,
                                                               DIA_SEMANA_ID,
                                                               TIPO_DIA,
                                                               FESTIVOS
                                                              ) AS
   SELECT i.id, fecha, docencia docencia_paso_1,
          DECODE (NVL (d.repetir_cada_semanas, 1),
                  1, docencia,
                  DECODE (MOD (orden_id, d.repetir_cada_semanas), 1, docencia, 'N')
                 ) docencia_paso_2,
          decode (tipo_dia,
                  'F', 'N',
                  DECODE (d.numero_iteraciones,
                          NULL, DECODE (NVL (d.repetir_cada_semanas, 1),
                                        1, docencia,
                                        DECODE (MOD (orden_id, d.repetir_cada_semanas), 1, docencia, 'N')
                                       ),
                          DECODE (SIGN (((orden_id - festivos) / d.repetir_cada_Semanas) - (d.numero_iteraciones)),
                                  1, 'N',
                                  DECODE (NVL (d.repetir_cada_semanas, 1),
                                          1, docencia,
                                          DECODE (MOD (orden_id, d.repetir_cada_semanas), 1, docencia, 'N')
                                         )
                                 )
                         )
                 ) docencia,
          d.orden_id, d.numero_iteraciones, d.repetir_cada_semanas, d.fecha_inicio, d.fecha_fin, d.estudio_id,
          d.semestre_id, d.asignatura_id, d.grupo_id, d.tipo_subgrupo_id, d.subgrupo_id, d.dia_semana_id, tipo_dia,
          festivos
     FROM (SELECT id, fecha,
                  hor_contar_festivos (NVL (x.desde_el_dia, fecha_inicio), x.fecha, x.dia_semana_id,
                                       x.repetir_cada_semanas) festivos,
                  ROW_NUMBER () OVER (PARTITION BY DECODE
                                                       (decode (sign (x.fecha - fecha_inicio),
                                                                -1, 'N',
                                                                decode (sign (fecha_fin - x.fecha), -1, 'N', 'S')
                                                               ),
                                                        'S', DECODE (decode (sign (x.fecha
                                                                                   - NVL (x.desde_el_dia, fecha_inicio)),
                                                                             -1, 'N',
                                                                             decode (sign (NVL (x.hasta_el_dia,
                                                                                                fecha_fin)
                                                                                           - x.fecha),
                                                                                     -1, 'N',
                                                                                     'S'
                                                                                    )
                                                                            ),
                                                                     'S', 'S',
                                                                     'N'
                                                                    ),
                                                        'N'
                                                       ), id, estudio_id, semestre_id, asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo_id, dia_semana_id ORDER BY fecha)
                                                                                                               orden_id,
                  DECODE (hor_f_fecha_entre (x.fecha, fecha_inicio, fecha_fin),
                          'S', DECODE (hor_f_fecha_entre (x.fecha, NVL (desde_el_dia, fecha_inicio),
                                                          NVL (hasta_el_dia, fecha_fin)),
                                       'S', 'S',
                                       'N'
                                      ),
                          'N'
                         ) docencia,
                  estudio_id, semestre_id, grupo_id, tipo_subgrupo_id, subgrupo_id, dia_Semana_id, asignatura_id,
                  fecha_inicio, fecha_fin, fecha_examenes_inicio, fecha_examenes_fin, desde_el_dia, hasta_el_dia,
                  repetir_cada_semanas, numero_iteraciones, detalle_manual, tipo_dia, dia_semana
             FROM (SELECT i.id, null estudio_id, i.semestre_id, i.grupo_id, i.tipo_subgrupo_id, i.subgrupo_id,
                          i.dia_Semana_id, null asignatura_id, fecha_inicio, fecha_fin, fecha_examenes_inicio,
                          fecha_examenes_fin, i.desde_el_dia, hasta_el_dia, repetir_cada_semanas, numero_iteraciones,
                          detalle_manual, c.fecha, tipo_dia, dia_semana
                     FROM hor_v_fechas_estudios s,
                          hor_items i,
                          hor_ext_calendario c
                    WHERE i.semestre_id = s.semestre_id
                      AND trunc (c.fecha) BETWEEN fecha_inicio AND NVL (fecha_examenes_fin, fecha_fin)
                      AND c.dia_semana_id = i.dia_semana_id
                      AND tipo_dia IN ('L', 'E', 'F')
                      and vacaciones = 0
                      and s.tipo_estudio_id = 'G'
                      AND detalle_manual = 0
                      and s.estudio_id in (select estudio_id
                                             from hor_items_asignaturas
                                            where item_id = i.id
                                              and curso_id = s.curso_id)
                                                                        --and i.id = 559274
                  ) x) d,
          hor_items i
    WHERE i.semestre_id = d.semestre_id
      AND i.grupo_id = d.grupo_id
      AND i.tipo_subgrupo_id = d.tipo_subgrupo_id
      AND i.subgrupo_id = d.subgrupo_id
      AND i.dia_semana_id = d.dia_semana_id
      AND i.detalle_manual = 0
      AND i.id = d.id
   UNION ALL
   SELECT c.id, c.fecha, 'N' docencia_paso_1, 'N' docencia_paso_2, DECODE (d.id, NULL, 'N', 'S') docencia, 1 orden_id,
          numero_iteraciones, repetir_cada_semanas, fecha_inicio, fecha_fin, estudio_id, semestre_id, asignatura_id,
          grupo_id, tipo_subgrupo_id, subgrupo_id, dia_semana_id, tipo_dia, decode (tipo_dia, 'F', 1, 0) festivos
     FROM (SELECT distinct i.id, c.fecha, numero_iteraciones, repetir_cada_semanas, s.fecha_inicio, s.fecha_fin,
                           null estudio_id, i.semestre_id, null asignatura_id, i.grupo_id, i.tipo_subgrupo_id,
                           i.subgrupo_id, i.dia_semana_id, tipo_dia
                      FROM hor_v_fechas_estudios s,
                           hor_items i,
                           hor_ext_calendario c
                     WHERE i.semestre_id = s.semestre_id
                       AND trunc (c.fecha) BETWEEN fecha_inicio AND NVL (fecha_examenes_fin, fecha_fin)
                       AND c.dia_semana_id = i.dia_semana_id
                       AND tipo_dia IN ('L', 'E', 'F')
                       and s.tipo_estudio_id = 'G'
                       and vacaciones = 0
                       AND detalle_manual = 1
                       and s.estudio_id in (select estudio_id
                                              from hor_items_asignaturas
                                             where item_id = i.id
                                               and curso_id = s.curso_id)) c,
          hor_items_detalle d
    WHERE c.id = d.item_id(+)
      AND trunc (c.fecha) = trunc (d.inicio(+));

			