DROP VIEW UJI_HORARIOS.HOR_EXT_ASIGNATURAS_AREA;

/* Formatted on 30/06/2015 15:57:41 (QP5 v5.115.810.9015) */
create or replace force view UJI_HORARIOS.HOR_EXT_ASIGNATURAS_AREA(ASI_ID, UEST_ID, RECIBE_ACTA, PORCENTAJE, CURSO_ACA) as
   select "ASI_ID", "UEST_ID", "RECIBE_ACTA", "PORCENTAJE", "CURSO_ACA"
     from pod_asignaturas_area p, hor_curso_academico c
    where curso_aca = c.id
      and porcentaje > 0
   union all
   select pasi_id, uest_id, 'S' recibe_acta, porcentaje, curso_aca
     from pop_asignaturas_area
    where substr(pasi_id, 1, 3) in ('SIX', 'SIV', 'SJA', 'SIU', 'SIW');

DROP VIEW UJI_HORARIOS.HOR_EXT_ASIGNATURAS_COMUNES;

/* Formatted on 30/06/2015 16:03:03 (QP5 v5.115.810.9015) */
create or replace force view UJI_HORARIOS.HOR_EXT_ASIGNATURAS_COMUNES(ID, GRUPO_COMUN_ID, NOMBRE, ASIGNATURA_ID, PREFIJO, PORCENTAJE) as
   select rownum id, id grupo_comun_id, nombre, asi_id asignatura_id,
          decode(substr(asi_id, 1, 2), 'AG', 'XX', 'EM', 'XX', 'ET', 'XX', 'EE', 'XX', 'EQ', 'XX', 'ED', 'XX', 'HP', 'YY', 'HU', 'YY', 'EI', 'ZZ', 'MT', 'ZZ', 'AE', 'TT', 'EC', 'TT', 'FC', 'TT', 'QU', 'XX', 'QQ') prefijo, porcentaje
     from pod_grp_comunes g, pod_comunes c
    where g.id = c.gco_id
      and curso_aca = (select max(curso_academico_id)
                         from uji_horarios.hor_semestres_detalle);

DROP VIEW UJI_HORARIOS.HOR_EXT_ASIGNATURAS_DETALLE;

/* Formatted on 30/06/2015 16:03:54 (QP5 v5.115.810.9015) */
create or replace force view UJI_HORARIOS.HOR_EXT_ASIGNATURAS_DETALLE(ASIGNATURA_ID, NOMBRE_ASIGNATURA, CREDITOS, CICLO, CARACTER, CRD_TE, CRD_PR, CRD_LA, CRD_SE, CRD_TU, CRD_EV, CRD_CTOTAL, TIPO) as
   select v.asi_id asignatura_id, nom_asi nombre_asignatura, a.creditos, cicle ciclo, decode(v.tit_id, 9, v.caracter, 26, v.caracter, 31, v.caracter, decode(v.caracter, 'TR', 'FB', 'LC', 'PE', v.caracter)) caracter,
          nvl((select sum(crd_pac_te)
                 from gra_pod.pod_grupos g
                where curso_aca = ca.id
                  and asi_id = v.asi_id
                  and crd_pac_te is not null),
              crd_te)
             crd_te, nvl((select sum(crd_pac_pr)
                            from gra_pod.pod_grupos g
                           where curso_aca = ca.id
                             and asi_id = v.asi_id
                             and crd_pac_pr is not null),
                         crd_pr)
                        crd_pr, nvl((select sum(crd_pac_la)
                                       from gra_pod.pod_grupos g
                                      where curso_aca = ca.id
                                        and asi_id = v.asi_id
                                        and crd_pac_la is not null),
                                    crd_la)
                                   crd_la, nvl((select sum(crd_pac_se)
                                                  from gra_pod.pod_grupos g
                                                 where curso_aca = ca.id
                                                   and asi_id = v.asi_id
                                                   and crd_pac_se is not null),
                                               crd_se)
                                              crd_se, nvl((select sum(crd_pac_tu)
                                                             from gra_pod.pod_grupos g
                                                            where curso_aca = ca.id
                                                              and asi_id = v.asi_id
                                                              and crd_pac_tu is not null),
                                                          crd_tu)
                                                         crd_tu, nvl((select sum(crd_pac_av)
                                                                        from gra_pod.pod_grupos g
                                                                       where curso_aca = ca.id
                                                                         and asi_id = v.asi_id
                                                                         and crd_pac_av is not null),
                                                                     crd_ev)
                                                                    crd_ev, crd_totals, a.tipo
     from gra_pod.pod_vsp_27 v, gra_pod.pod_asignaturas a, gra_pod.pod_asig_sin_coste sc, gra_pod.pod_asignaturas_titulaciones atit, uji_horarios.hor_curso_academico ca
    where v.curso_Aca = ca.id
      and v.asi_id = a.id
      and v.curso_aca = sc.curso_aca(+)
      and v.asi_id = sc.asi_id(+)
      and v.tit_id = atit.tit_id
      and v.asi_id = atit.asi_id;

DROP VIEW UJI_HORARIOS.HOR_EXT_CARGOS_PER;

/* Formatted on 30/06/2015 16:04:17 (QP5 v5.115.810.9015) */
create or replace force view UJI_HORARIOS.HOR_EXT_CARGOS_PER(ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO, DEPARTAMENTO_ID, DEPARTAMENTO, AREA_ID, AREA, CURSO_ID, ORIGEN) as
   select "ID", "PERSONA_ID", "NOMBRE", "CENTRO_ID", "CENTRO", "ESTUDIO_ID", "ESTUDIO", "CARGO_ID", "CARGO", "DEPARTAMENTO_ID", "DEPARTAMENTO", "AREA_ID", "AREA", "CURSO_ID", "ORIGEN"
     from (select rownum id, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, TITULACION_ID estudio_id, TITULACION estudio, CARGO_ID, CARGO, DEPARTAMENTO_ID, DEPARTAMENTO, AREA_ID, AREA, CURSO_ID, origen
             from ( /* PDI de centro */
                   select cp.per_id persona_id, p.nombre || ' ' || apellido1 || ' ' || apellido2 nombre, ulogica_id centro_id, ubic.nombre centro, tit.id titulacion_id, tit.nombre titulacion, tc.id cargo_id, tc.nombre cargo,
                          d.id departamento_id, d.nombre departamento, a.id area_id, a.nombre area, null curso_id, 1 origen
                     from grh_grh.grh_cargos_per cp, gri_est.est_ubic_estructurales ubic, gra_Exp.exp_v_titu_todas tit, uji_horarios.hor_tipos_Cargos tc, gri_per.per_personas p, uji_horarios.hor_departamentos d, uji_horarios.hor_areas a
                    where (f_fin is null
                       and (f_fin is null
                         or f_fin >= sysdate)
                       and crg_id in (189, 195, 188, 30))
                      and ulogica_id = ubic.id
                      and ulogica_id = tit.uest_id
                      and tit.activa = 'S'
                      and tit.tipo = 'G'
                      and tc.id = 3
                      and cp.per_id = p.id
                      and ulogica_id = d.centro_id
                      and d.id = a.departamento_id
                   union all
                   /* PAS de centro */
                   select cp.per_id persona_id, p.nombre || ' ' || apellido1 || ' ' || apellido2 nombre, ubicacion_id centro_id, ubic.nombre centro, tit.id titulacion_id, tit.nombre titulacion, tc.id cargo_id, tc.nombre cargo,
                          d.id departamento_id, d.nombre departamento, a.id area_id, a.nombre area, null curso_id, 2 origen
                     from grh_grh.grh_vw_contrataciones_ult cp, gri_est.est_ubic_estructurales ubic, gra_Exp.exp_v_titu_todas tit, uji_horarios.hor_tipos_Cargos tc, gri_per.per_personas p, uji_horarios.hor_departamentos d,
                          uji_horarios.hor_areas a
                    where ubicacion_id = ubic.id
                      and ubicacion_id = tit.uest_id
                      and tit.activa = 'S'
                      and tit.tipo = 'G'
                      and tc.id = 4
                      and cp.per_id = p.id
                      --and ubicacion_id = d.centro_id
                      and decode(ubicacion_id, 97, 2922, 96, 2922, ubicacion_id) = d.centro_id
                      and d.id = a.departamento_id
                   union all
                   /* directores de titulacion */
                   select cp.per_id persona_id, p.nombre || ' ' || apellido1 || ' ' || apellido2 nombre, ulogica_id centro_id, ubic.nombre centro, tit.id titulacion_id, tit.nombre titulacion, tc.id cargo_id, tc.nombre cargo,
                          null departamento_id, '' departamento, null area_id, '' area, null curso_id, 3 origen
                     from grh_grh.grh_cargos_per cp, gri_est.est_ubic_estructurales ubic, gra_Exp.exp_v_titu_todas tit, uji_horarios.hor_tipos_Cargos tc, gri_per.per_personas p
                    where (f_fin is null
                       and (f_fin is null
                         or f_fin >= sysdate)
                       and crg_id in (192, 193))
                      and ulogica_id = ubic.id
                      and ulogica_id = tit.uest_id
                      and tit.activa = 'S'
                      and tit.tipo = 'G'
                      and tc.id = 1
                      and cp.per_id = p.id
                      and cp.tit_id = tit.id
                   union all
                   /* coordinadores de curso */
                   select cp.per_id persona_id, p.nombre || ' ' || apellido1 || ' ' || apellido2 nombre, ulogica_id centro_id, ubic.nombre centro, tit.id titulacion_id, tit.nombre titulacion, tc.id cargo_id, tc.nombre cargo,
                          null departamento_id, '' departamento, null area_id, '' area, ciclo_cargo curso_id, 4 origen
                     from grh_grh.grh_cargos_per cp, gri_est.est_ubic_estructurales ubic, gra_Exp.exp_v_titu_todas tit, uji_horarios.hor_tipos_Cargos tc, gri_per.per_personas p
                    where (f_fin is null
                       and (f_fin is null
                         or f_fin >= sysdate)
                       and crg_id in (305))
                      and ulogica_id = ubic.id
                      and ulogica_id = tit.uest_id
                      and tit.activa = 'S'
                      and tit.tipo = 'G'
                      and tc.id = 2
                      and cp.per_id = p.id
                      and cp.tit_id = tit.id
                   union all
                   /* directores de departamento */
                   select cp.per_id persona_id, p.nombre || ' ' || apellido1 || ' ' || apellido2 nombre, c.id centro_id, c.nombre centro, null titulacion_id, '' titulacion, tc.id cargo_id, tc.nombre cargo, ulogica_id departamento_id,
                          d.nombre departamento, a.id area_id, a.nombre area, null curso_id, 5 origen
                     from grh_grh.grh_cargos_per cp, uji_horarios.hor_tipos_Cargos tc, gri_per.per_personas p, est_ubic_estructurales d, est_relaciones_ulogicas r, est_ubic_estructurales c, uji_horarios.hor_Areas a
                    where (f_fin is null
                       and (f_fin is null
                         or f_fin >= sysdate)
                       and crg_id in (178, 194))
                      and tc.id = 6
                      and cp.per_id = p.id
                      and ulogica_id = d.id
                      and uest_id_relacionad = d.id
                      and d.tuest_id = 'DE'
                      --and uest_id = c.id
                      and decode(ulogica_id, 97, 2922, 96, 2922, uest_id) = c.id
                      and trel_id = 4
                      and ulogica_id = a.departamento_id
                   union all
                   /* pas de departamento */
                   select cp.per_id persona_id, p.nombre || ' ' || apellido1 || ' ' || apellido2 nombre, c.id centro_id, c.nombre centro, null titulacion_id, '' titulacion, tc.id cargo_id, tc.nombre cargo, ubic.id departamento_id,
                          ubic.nombre departamento, a.id area_id, a.nombre area, null curso_id, 6 origen
                     from grh_grh.grh_vw_contrataciones_ult cp, gri_est.est_ubic_estructurales ubic, uji_horarios.hor_tipos_Cargos tc, gri_per.per_personas p, est_relaciones_ulogicas r, est_ubic_estructurales c, uji_horarios.hor_areas a
                    where ubicacion_id = ubic.id
                      and tc.id = 5
                      and cp.per_id = p.id
                      and cp.act_id = 'PAS'
                      and ubicacion_id = uest_id_relacionad
                      and trel_id = 4
                      --and uest_id = c.id
                      and decode(ubic.id, 97, 2922, 96, 2922, uest_id) = c.id
                      and ubic.id = departamento_id
                   union all
                   /* permisos extra */
                   select x.persona_id, x.nombre, x.centro_id, x.centro, x.titulacion_id, titulacion, x.cargo_id, x.cargo, x.departamento_id, x.departamento, decode(cargo_id, 6, decode(area_id, null, a.id, area_id), area_id) area_id,
                          decode(cargo_id, 6, decode(area_id, null, a.nombre, area), area) area, curso_id, 7 origen
                     from (select per.persona_id, p.nombre || ' ' || apellido1 || ' ' || apellido2 nombre, ubic.id centro_id, ubic.nombre centro, tit.id titulacion_id, tit.nombre titulacion, tc.id cargo_id, tc.nombre cargo, departamento_id,
                                  dep.nombre departamento, area_id, (select nombre
                                                                       from gri_est.est_ubic_estructurales
                                                                      where id = area_id)
                                                                       area, per.curso_id
                             from uji_horarios.hor_permisos_extra per, gri_per.per_personas p, (select *
                                                                                                  from gra_Exp.exp_v_titu_todas
                                                                                                 where activa = 'S'
                                                                                                   and tipo = 'G') tit, uji_horarios.hor_tipos_cargos tc, gri_est.est_ubic_estructurales ubic, gri_est.est_ubic_estructurales dep
                            where persona_id = p.id
                              and estudio_id = tit.id(+)
                              and tipo_Cargo_id = tc.id
                              and tit.uest_id = ubic.id(+)
                              and per.departamento_id = dep.id(+)) x, uji_horarios.hor_areas a
                    where ((cargo_id <> 6
                        and a.id = (select min(id)
                                      from uji_horarios.hor_areas))
                        or (cargo_id = 6
                        and area_id is not null
                        and a.id = area_id)
                        or (cargo_id = 6
                        and area_id is null
                        and x.departamento_id = a.departamento_id))
                   union all
                   /* administradores */
                   select distinct per.persona_id, p.nombre || ' ' || apellido1 || ' ' || apellido2 nombre, u.id centro_id, u.nombre centro, tit.id titulacion_id, tit.nombre titulacion, tc.id cargo_id, tc.nombre cargo, null departamento_id,
                                   '' departamento, null area_id, '' area, null curso_id, 8 origen
                     from uji_apa.apa_vw_personas_items per, gri_per.per_personas p, gra_Exp.exp_v_titu_todas tit, uji_horarios.hor_tipos_cargos tc, gri_Est.est_ubic_estructurales u
                    where persona_id = p.id
                      and tc.id = 1
                      and tit.activa = 'S'
                      and tit.tipo = 'G'
                      and aplicacion_id = 46
                      and role = 'ADMIN'
                      and tit.uest_id = u.id
                   union all
                   select distinct per.persona_id, p.nombre || ' ' || apellido1 || ' ' || apellido2 nombre, c.id centro_id, c.nombre centro, null titulacion_id, '' titulacion, tc.id cargo_id, tc.nombre cargo, dep.id departamento_id,
                                   dep.nombre departamento, null area_id, '' area, null curso_id, 9 origen
                     from uji_apa.apa_vw_personas_items per, gri_per.per_personas p, est_ubic_estructurales dep, uji_horarios.hor_tipos_cargos tc, gri_Est.est_ubic_estructurales c, est_relaciones_ulogicas r
                    where persona_id = p.id
                      and tc.id = 6
                      and dep.status = 'A'
                      and dep.tuest_id = 'DE'
                      and aplicacion_id = 46
                      and role = 'ADMIN'
                      --and decode(dep.id,97,2943,dep.id)
                      and dep.id = r.uest_id_relacionad
                      and r.trel_id = 4
                      and decode(dep.id, 97, 2922, 96, 2922, r.uest_id) = c.id));

DROP VIEW UJI_HORARIOS.HOR_EXT_CIRCUITOS;

/* Formatted on 30/06/2015 16:04:30 (QP5 v5.115.810.9015) */
create or replace force view UJI_HORARIOS.HOR_EXT_CIRCUITOS(ID, CURSO_ACA, ESTUDIO_ID, ASIGNATURA_ID, GRUPO_ID, TIPO, SUBGRUPO_ID, DETALLE_ID, CIRCUITO_ID, PLAZAS) as
   select rownum id, SGD_SGR_GRP_CURSO_ACA curso_aca, CIC_TIT_ID estudio_id, SGD_SGR_GRP_ASI_ID asignatura_id, SGD_SGR_GRP_ID grupo_id, SGD_SGR_TIPO tipo, SGD_SGR_ID subgrupo_id, SGD_ID detalle_id, CIC_ID circuito_id, limite plazas
     from pod_circuitos_det c, pod_subgrupos_det d
    where sgd_sgr_grp_curso_aca >= 2012
      and c.SGD_SGR_GRP_CURSO_ACA = d.sgr_grp_curso_aca
      and c.SGD_SGR_TIPO = d.sgr_tipo
      and c.SGD_SGR_ID = d.sgr_id
      and c.SGD_SGR_GRP_ID = d.sgr_grp_id
      and c.SGD_SGR_GRP_ASI_ID = d.sgr_grp_asi_id
      and c.SGD_ID = d.id;

DROP VIEW UJI_HORARIOS.HOR_EXT_CONVOCATORIAS;

/* Formatted on 30/06/2015 16:04:48 (QP5 v5.115.810.9015) */
create or replace force view UJI_HORARIOS.HOR_EXT_CONVOCATORIAS(ID, NOMBRE, NOMCAS, ORDEN, NOMANG, TIPO, EXTRA, NOMBRE_CERT_CA, NOMBRE_CERT_ES, NOMBRE_CERT_UK) as
   select "ID", "NOMBRE", "NOMCAS", "ORDEN", "NOMANG", "TIPO", "EXTRA", "NOMBRE_CERT_CA", "NOMBRE_CERT_ES", "NOMBRE_CERT_UK"
     from pod_convocatorias
    where id in (9, 10, 11);

DROP VIEW UJI_HORARIOS.HOR_EXT_PERSONAS;

/* Formatted on 30/06/2015 16:04:58 (QP5 v5.115.810.9015) */
create or replace force view UJI_HORARIOS.HOR_EXT_PERSONAS(ID, NOMBRE, EMAIL, ACTIVIDAD_ID, UBICACION_ID, NOMBRE_BUSCAR) as
   select per_id id, nombre, busca_cuenta(per_id) email, act_id actividad_id, ubicacion_id, cmp_util.limpia_txt(nombre) nombre_buscar
     from grh_vw_contrataciones_ult c
    where act_id in ('PAS', 'PDI')
      /*AND ubicacion_id IN (SELECT   id
                             FROM   est_ubic_estructurales
                            WHERE   tuest_id = 'DE' AND status = 'A')*/
      and act_id = (select max(act_id)
                      from grh_vw_contrataciones_ult c2
                     where c.per_id = c2.per_id);

DROP VIEW UJI_HORARIOS.HOR_V_AGRUPACIONES_ITEMS;

/* Formatted on 30/06/2015 16:05:32 (QP5 v5.115.810.9015) */
create or replace force view UJI_HORARIOS.HOR_V_AGRUPACIONES_ITEMS(ID, AGRUPACION_ID, NOMBRE_AGRUPACION, ASIGNATURA_ID, ESTUDIO_ID, CURSO_ID, CARACTER_ID, ITEM_ID) as
   select distinct a.id || agrupacion_id || estudio_id || item_id id, a.id agrupacion_id, nombre nombre_agrupacion, aa.asignatura_id, estudio_id, ia.curso_id, ia.caracter_id, item_id /*, i.semestre_id,
                                                                                                                aula_planificacion_id, comun, porcentaje_comun, grupo_id, i.tipo_subgrupo_id, tipo_subgrupo,
                                                                                                                i.subgrupo_id, dia_semana_id, hora_inicio, hora_fin, desde_el_dia, hasta_el_dia, tipo_asignatura_id,
                                                                                                                tipo_asignatura, tipo_estudio_id, tipo_estudio, plazas, repetir_cada_semanas, numero_iteraciones,
                                                                                                                detalle_manual, comun_texto, aula_planificacion_nombre, creditos */
     from hor_agrupaciones a, hor_agrupaciones_asignaturas aa, hor_items_asignaturas ia, hor_items i
    where a.id = aa.agrupacion_id
      and aa.asignatura_id = ia.asignatura_id
      and ia.item_id = i.id
      and nvl(aa.tipo_subgrupo_id, i.tipo_subgrupo_id) = i.tipo_subgrupo_id
      and nvl(aa.subgrupo_id, i.subgrupo_id) = i.subgrupo_id;

DROP VIEW UJI_HORARIOS.HOR_V_ASIGNATURAS;

/* Formatted on 30/06/2015 16:05:43 (QP5 v5.115.810.9015) */
create or replace force view UJI_HORARIOS.HOR_V_ASIGNATURAS(ID, ASIGNATURA, ESTUDIO_ID, ESTUDIO, CURSO_ID, CARACTER_ID, CARACTER) as
   select distinct asignatura_id id, asignatura, estudio_id, estudio, curso_id, caracter_id, caracter
     from hor_items_asignaturas;

DROP VIEW UJI_HORARIOS.HOR_V_ASIGNATURAS_AREA;

/* Formatted on 30/06/2015 16:05:53 (QP5 v5.115.810.9015) */
create or replace force view UJI_HORARIOS.HOR_V_ASIGNATURAS_AREA(ASIGNATURA_ID, AREA_ID, PORCENTAJE, RECIBE_ACTA) as
   select distinct asignatura_id, uest_id area_id, porcentaje, recibe_acta
     from hor_ext_asignaturas_area aa, hor_items_asignaturas a
    where asignatura_id = asi_id;

DROP VIEW UJI_HORARIOS.HOR_V_AULAS_PERSONAS;

/* Formatted on 30/06/2015 16:06:01 (QP5 v5.115.810.9015) */
create or replace force view UJI_HORARIOS.HOR_V_AULAS_PERSONAS(PERSONA_ID, AULA_ID, NOMBRE, CENTRO_ID, CENTRO, CODIGO, TIPO) as
   select distinct c.persona_id, ap.aula_id, a.nombre, a.centro_id, cen.nombre centro, codigo, tipo
     from uji_horarios.hor_ext_Cargos_per c, uji_horarios.hor_aulas_planificacion ap, uji_horarios.hor_aulas a, uji_horarios.hor_Centros cen
    where c.estudio_id = ap.estudio_id
      and ap.aula_id = a.id
      and a.centro_id = cen.id;

DROP VIEW UJI_HORARIOS.HOR_V_CURSOS;

/* Formatted on 30/06/2015 16:06:09 (QP5 v5.115.810.9015) */
create or replace force view UJI_HORARIOS.HOR_V_CURSOS(ESTUDIO_ID, ESTUDIO, CURSO_ID) as
   select distinct hor_items_asignaturas.estudio_id, hor_items_asignaturas.estudio, hor_items_asignaturas.curso_id
     from hor_items_asignaturas, hor_items
    where hor_items.id = hor_items_asignaturas.item_id
      and hor_items_asignaturas.curso_id < 7;

DROP VIEW UJI_HORARIOS.HOR_V_FECHAS_CONVOCATORIAS;

/* Formatted on 30/06/2015 16:06:18 (QP5 v5.115.810.9015) */
create or replace force view UJI_HORARIOS.HOR_V_FECHAS_CONVOCATORIAS(ID, NOMBRE, FECHA_EXAMENES_INICIO, FECHA_EXAMENES_FIN) as
   select distinct convocatoria_id id, c.nombre nombre, s.fecha_examenes_inicio, s.fecha_examenes_fin
     from hor_examenes e, hor_semestres_detalle s, hor_ext_convocatorias c
    where ((e.convocatoria_id = 9
        and semestre_id = 1)
        or (e.convocatoria_id = 10
        and semestre_id = 2))
      and e.convocatoria_id = c.id
   union all
   select distinct convocatoria_id, c.nombre, s.fechas_examenes_inicio_c2, s.fechas_examenes_fin_c2
     from hor_examenes e, hor_semestres_detalle s, hor_ext_convocatorias c
    where (e.convocatoria_id = 11)
      and e.convocatoria_id = c.id;

DROP VIEW UJI_HORARIOS.HOR_V_FECHAS_CONVOCATORIAS_EST;

/* Formatted on 30/06/2015 16:06:34 (QP5 v5.115.810.9015) */
create or replace force view UJI_HORARIOS.HOR_V_FECHAS_CONVOCATORIAS_EST(ID, CONVOCATORIA_ID, NOMBRE, FECHA_EXAMENES_INICIO, FECHA_EXAMENES_FIN, ESTUDIO_ID) as
   select e.id * 10000 + c.id id, c.id convocatoria_id, c.nombre, c.fecha_examenes_inicio, c.fecha_examenes_fin, e.id estudio_id
     from hor_v_Fechas_convocatorias c, hor_estudios e
    where e.id not in (select estudio_id
                         from hor_semestres_detalle_estudio de)
   union all
   select distinct de.estudio_id * 10000 + c.id id, c.id convocatoria_id, c.nombre, de.fecha_examenes_inicio, de.fecha_examenes_fin, de.estudio_id
     from hor_v_Fechas_convocatorias c, hor_estudios e, hor_semestres_detalle d, hor_semestres_detalle_estudio de
    where e.id = de.estudio_id
      and d.id = de.detalle_id
      and c.id = 9
      and semestre_id = 1
   union all
   select distinct de.estudio_id * 10000 + c.id id, c.id convocatoria_id, c.nombre, de.fecha_examenes_inicio, de.fecha_examenes_fin, de.estudio_id
     from hor_v_Fechas_convocatorias c, hor_estudios e, hor_semestres_detalle d, hor_semestres_detalle_estudio de
    where e.id = de.estudio_id
      and d.id = de.detalle_id
      and c.id = 10
      and semestre_id = 2
   union all
   select distinct de.estudio_id * 10000 + c.id id, c.id convocatoria_id, c.nombre, de.fecha_examenes_inicio_c2, de.fecha_examenes_fin_c2, de.estudio_id
     from hor_v_Fechas_convocatorias c, hor_estudios e, hor_semestres_detalle d, hor_semestres_detalle_estudio de
    where e.id = de.estudio_id
      and d.id = de.detalle_id
      and c.id = 11;

DROP VIEW UJI_HORARIOS.HOR_V_FECHAS_ESTUDIOS;

/* Formatted on 30/06/2015 16:06:48 (QP5 v5.115.810.9015) */
create or replace force view UJI_HORARIOS.HOR_V_FECHAS_ESTUDIOS(ID, ESTUDIO_ID, ESTUDIO_NOMBRE, TIPO_ESTUDIO_ID, SEMESTRE_ID, SEMESTRE_NOMBRE, CURSO_ID, FECHA_INICIO, FECHA_FIN, FECHA_EXAMENES_INICIO, FECHA_EXAMENES_FIN,
FECHAS_EXAMENES_INICIO_C2, FECHAS_EXAMENES_FIN_C2, NUMERO_SEMANAS) as
   select id, "ESTUDIO_ID", "ESTUDIO_NOMBRE", tipo_estudio_id, "SEMESTRE_ID", "SEMESTRE_NOMBRE", "CURSO_ID", "FECHA_INICIO", "FECHA_FIN", FECHA_EXAMENES_INICIO, FECHA_EXAMENES_FIN, FECHAS_EXAMENES_INICIO_C2, FECHAS_EXAMENES_FIN_C2,
          NUMERO_SEMANAS
     from (select distinct to_number(e.id || curso_id || semestre_id) id, e.id estudio_id, e.nombre estudio_nombre, tipo_estudio_id, s.id semestre_id, s.nombre semestre_nombre, i.curso_id, d.fecha_inicio, fecha_fin, FECHA_EXAMENES_INICIO,
                           FECHA_EXAMENES_FIN, FECHAS_EXAMENES_INICIO_C2, FECHAS_EXAMENES_FIN_C2, NUMERO_SEMANAS
             from hor_Estudios e, hor_semestres s, hor_semestres_detalle d, hor_items_asignaturas i
            where s.id = d.semestre_id
              and e.tipo_id = d.tipo_estudio_id
              --and e.id in (217, 218)
              and e.id = i.estudio_id)
    where (estudio_id, semestre_id, curso_id) not in (select e.id estudio_id, s.id semestre_id, curso_id
                                                        from hor_Estudios e, hor_semestres s, hor_semestres_detalle d, hor_semestres_detalle_estudio de
                                                       where s.id = d.semestre_id
                                                         and e.tipo_id = d.tipo_estudio_id
                                                         and d.id = de.detalle_id
                                                         and e.id = de.estudio_id)
   union
   select to_number(e.id || curso_id || semestre_id) id, e.id estudio_id, e.nombre estudio_nombre, tipo_estudio_id, s.id semestre_id, s.nombre semestre_nombre, curso_id, de.fecha_inicio, de.fecha_fin, de.FECHA_EXAMENES_INICIO,
          de.FECHA_EXAMENES_FIN, de.FECHA_EXAMENES_INICIO_C2, de.FECHA_EXAMENES_FIN_C2, de.NUMERO_SEMANAS
     from hor_Estudios e, hor_semestres s, hor_semestres_detalle d, hor_semestres_detalle_estudio de
    where s.id = d.semestre_id
      and e.tipo_id = d.tipo_estudio_id
      and d.id = de.detalle_id
      and e.id = de.estudio_id
   order by 1, 5, 3;

DROP VIEW UJI_HORARIOS.HOR_V_GRUPOS;

/* Formatted on 30/06/2015 16:07:00 (QP5 v5.115.810.9015) */
create or replace force view UJI_HORARIOS.HOR_V_GRUPOS(ESTUDIO_ID, ESTUDIO, GRUPO_ID, ESPECIAL) as
   select distinct hor_items_asignaturas.estudio_id, hor_items_asignaturas.estudio, hor_items.grupo_id, decode(hor_items.grupo_id, 'Y', 'Grupo ARA', '') especial
     from hor_items, hor_items_asignaturas
    where hor_items.id = hor_items_asignaturas.item_id;

DROP VIEW UJI_HORARIOS.HOR_V_ITEMS_CREDITOS_DETALLE;

/* Formatted on 30/06/2015 16:07:12 (QP5 v5.115.810.9015) */
create or replace force view UJI_HORARIOS.HOR_V_ITEMS_CREDITOS_DETALLE(ID, NOMBRE, CREDITOS_PROFESOR, ITEM_ID, CRD_PROF, CRD_ITEM, CUANTOS, CRD_FINAL, GRUPO_ID, TIPO, ASIGNATURA_ID, ASIGNATURA_TXT, DEPARTAMENTO_ID, AREA_ID) as
   select id, nombre, creditos_profesor, item_id, round(crd_prof, 2) crd_prof, crd_item, cuantos, round(nvl(crd_prof, trunc(crd_item / cuantos, 2)), 2) crd_final, GRUPO_ID, TIPO_SUBGRUPO_ID || SUBGRUPO_ID tipo, asignatura_id,
          asignatura_txt, departamento_id, area_id
     from (  select h.id, nombre, h.creditos creditos_profesor, ip.item_id, ip.creditos crd_prof, i.creditos crd_item, (select count( * )
                                                                                                                          from hor_items_profesores ip2
                                                                                                                         where item_id = ip.item_id)
                                                                                                                          cuantos, GRUPO_ID, TIPO_SUBGRUPO_ID, SUBGRUPO_ID, wm_concat(asignatura_id) asignatura_txt,
                    min(asignatura_id) asignatura_id, departamento_id, area_id
               from hor_profesores h, hor_items_profesores ip, hor_items i, hor_items_asignaturas ia
              where h.id = ip.profesor_id
                and ip.item_id = i.id
                and i.id = ia.item_id
           group by h.id, nombre, h.creditos, ip.item_id, ip.creditos, i.creditos, GRUPO_ID, TIPO_SUBGRUPO_ID, SUBGRUPO_ID, departamento_id, area_id) x
   union all
   select id, nombre, creditos creditos_profesor, null item_id, null crd_prof, null crd_item, 0 cuantos, 0 crd_final, null grupo_id, null tipo, null asignatura_id, null asignatura_txt, departamento_id, area_id
     from hor_profesores h
    where not exists (select profesor_id
                        from hor_items_profesores
                       where profesor_id = h.id);

DROP VIEW UJI_HORARIOS.HOR_V_ITEMS_DETALLE;

/* Formatted on 30/06/2015 16:07:19 (QP5 v5.115.810.9015) */
create or replace force view UJI_HORARIOS.HOR_V_ITEMS_DETALLE(ID, FECHA, DOCENCIA_PASO_1, DOCENCIA_PASO_2, DOCENCIA, ORDEN_ID, NUMERO_ITERACIONES, REPETIR_CADA_SEMANAS, FECHA_INICIO, FECHA_FIN, ESTUDIO_ID, SEMESTRE_ID, ASIGNATURA_ID,
GRUPO_ID, TIPO_SUBGRUPO_ID, SUBGRUPO_ID, DIA_SEMANA_ID, TIPO_DIA, FESTIVOS) as
   select i.id, fecha, docencia docencia_paso_1, decode(nvl(d.repetir_cada_semanas, 1), 1, docencia, decode(mod(orden_id, d.repetir_cada_semanas), 1, docencia, 'N')) docencia_paso_2,
          decode(tipo_dia, 'F', 'N', decode(d.numero_iteraciones, null, decode(nvl(d.repetir_cada_semanas, 1), 1, docencia, decode(mod(orden_id, d.repetir_cada_semanas), 1, docencia, 'N')), decode(sign(((orden_id - festivos) / d.repetir_cada_Semanas) - (d.numero_iteraciones)), 1, 'N', decode(nvl(d.repetir_cada_semanas, 1), 1, docencia, decode(mod(orden_id, d.repetir_cada_semanas), 1, docencia, 'N')))))
             docencia, d.orden_id, d.numero_iteraciones, d.repetir_cada_semanas, d.fecha_inicio, d.fecha_fin, d.estudio_id, d.semestre_id, d.asignatura_id, d.grupo_id, d.tipo_subgrupo_id, d.subgrupo_id, d.dia_semana_id, tipo_dia, festivos
     from (select id, fecha, hor_contar_festivos(nvl(x.desde_el_dia, fecha_inicio), x.fecha, x.dia_semana_id, x.repetir_cada_semanas) festivos,
                  row_number() over (partition by decode(decode(sign(x.fecha - fecha_inicio), -1, 'N', decode(sign(fecha_fin - x.fecha), -1, 'N', 'S')), 'S', decode(decode(sign(x.fecha - nvl(x.desde_el_dia, fecha_inicio)), -1, 'N', decode(sign(nvl(x.hasta_el_dia, fecha_fin) - x.fecha), -1, 'N', 'S')), 'S', 'S', 'N'), 'N'), id, estudio_id, semestre_id, asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo_id, dia_semana_id order by fecha) orden_id, decode(hor_f_fecha_entre(x.fecha, fecha_inicio, fecha_fin), 'S', decode(hor_f_fecha_entre(x.fecha, nvl(desde_el_dia, fecha_inicio), nvl(hasta_el_dia, fecha_fin)), 'S', 'S', 'N'), 'N') docencia, estudio_id,
                  semestre_id, grupo_id, tipo_subgrupo_id, subgrupo_id, dia_Semana_id, asignatura_id, fecha_inicio, fecha_fin, fecha_examenes_inicio, fecha_examenes_fin, desde_el_dia, hasta_el_dia, repetir_cada_semanas, numero_iteraciones,
                  detalle_manual, tipo_dia, dia_semana
             from (select distinct i.id, null estudio_id, i.semestre_id, i.grupo_id, i.tipo_subgrupo_id, i.subgrupo_id, i.dia_Semana_id, null asignatura_id, fecha_inicio, fecha_fin, fecha_examenes_inicio, fecha_examenes_fin,
                                   decode(sign(nvl(i.desde_el_dia, fecha_inicio) - fecha_inicio), -1, fecha_inicio, i.desde_el_dia) desde_el_dia, hasta_el_dia, repetir_cada_semanas, numero_iteraciones, detalle_manual, c.fecha, tipo_dia,
                                   dia_semana
                     from hor_v_fechas_estudios s, hor_items i, hor_ext_calendario c
                    where i.semestre_id = s.semestre_id
                      and trunc(c.fecha) between fecha_inicio and nvl(fecha_examenes_fin, fecha_fin)
                      and c.dia_semana_id = i.dia_semana_id
                      and tipo_dia in ('L', 'E', 'F')
                      and vacaciones = 0
                      and s.tipo_estudio_id = 'G'
                      and detalle_manual = 0
                      --and i.id = 283676
                      and s.estudio_id in (select estudio_id
                                             from hor_items_asignaturas
                                            where item_id = i.id
                                              and curso_id = s.curso_id) --and i.id = 559274
                                                                        ) x) d, hor_items i
    where i.semestre_id = d.semestre_id
      and i.grupo_id = d.grupo_id
      and i.tipo_subgrupo_id = d.tipo_subgrupo_id
      and i.subgrupo_id = d.subgrupo_id
      and i.dia_semana_id = d.dia_semana_id
      and i.detalle_manual = 0
      and i.id = d.id
   union all
   select c.id, c.fecha, 'N' docencia_paso_1, 'N' docencia_paso_2, decode(d.id, null, 'N', 'S') docencia, 1 orden_id, numero_iteraciones, repetir_cada_semanas, fecha_inicio, fecha_fin, estudio_id, semestre_id, asignatura_id, grupo_id,
          tipo_subgrupo_id, subgrupo_id, dia_semana_id, tipo_dia, decode(tipo_dia, 'F', 1, 0) festivos
     from (select distinct i.id, c.fecha, numero_iteraciones, repetir_cada_semanas, s.fecha_inicio, s.fecha_fin, null estudio_id, i.semestre_id, null asignatura_id, i.grupo_id, i.tipo_subgrupo_id, i.subgrupo_id, i.dia_semana_id, tipo_dia
             from hor_v_fechas_estudios s, hor_items i, hor_ext_calendario c
            where i.semestre_id = s.semestre_id
              and trunc(c.fecha) between fecha_inicio and nvl(fecha_examenes_fin, fecha_fin)
              and c.dia_semana_id = i.dia_semana_id
              and tipo_dia in ('L', 'E', 'F')
              and s.tipo_estudio_id = 'G'
              and vacaciones = 0
              and detalle_manual = 1
              and s.estudio_id in (select estudio_id
                                     from hor_items_asignaturas
                                    where item_id = i.id
                                      and curso_id = s.curso_id)) c, hor_items_detalle d
    where c.id = d.item_id(+)
      and trunc(c.fecha) = trunc(d.inicio(+));

DROP VIEW UJI_HORARIOS.HOR_V_ITEMS_DETALLE_COMPLETA;

/* Formatted on 30/06/2015 16:07:31 (QP5 v5.115.810.9015) */
create or replace force view UJI_HORARIOS.HOR_V_ITEMS_DETALLE_COMPLETA(ID, FECHA, DOCENCIA_PASO_1, DOCENCIA_PASO_2, DOCENCIA, ORDEN_ID, NUMERO_ITERACIONES, REPETIR_CADA_SEMANAS, FECHA_INICIO, FECHA_FIN, ESTUDIO_ID, SEMESTRE_ID, CURSO_ID,
ASIGNATURA_ID, GRUPO_ID, TIPO_SUBGRUPO_ID, SUBGRUPO_ID, DIA_SEMANA_ID, TIPO_DIA, FESTIVOS) as
   select i.id, fecha, docencia docencia_paso_1, decode(nvl(d.repetir_cada_semanas, 1), 1, docencia, decode(mod(orden_id, d.repetir_cada_semanas), 1, docencia, 'N')) docencia_paso_2,
          decode(tipo_dia, 'F', 'N', decode(d.numero_iteraciones, null, decode(nvl(d.repetir_cada_semanas, 1), 1, docencia, decode(mod(orden_id, d.repetir_cada_semanas), 1, docencia, 'N')), decode(sign(((orden_id - festivos) / d.repetir_cada_Semanas) - (d.numero_iteraciones)), 1, 'N', decode(nvl(d.repetir_cada_semanas, 1), 1, docencia, decode(mod(orden_id, d.repetir_cada_semanas), 1, docencia, 'N'))))) docencia, d.orden_id, d.numero_iteraciones, d.repetir_cada_semanas, d.fecha_inicio, d.fecha_fin, d.estudio_id, d.semestre_id, d.curso_id, d.asignatura_id, d.grupo_id, d.tipo_subgrupo_id, d.subgrupo_id, d.dia_semana_id,
          tipo_dia, festivos
     from (select id, fecha, hor_contar_festivos(nvl(x.desde_el_dia, fecha_inicio), x.fecha, x.dia_semana_id, x.repetir_cada_semanas) festivos,
                  row_number() over (partition by decode(decode(sign(x.fecha - fecha_inicio), -1, 'N', decode(sign(fecha_fin - x.fecha), -1, 'N', 'S')), 'S', decode(decode(sign(x.fecha - nvl(x.desde_el_dia, fecha_inicio)), -1, 'N', decode(sign(nvl(x.hasta_el_dia, fecha_fin) - x.fecha), -1, 'N', 'S')), 'S', 'S', 'N'), 'N'), id, estudio_id, semestre_id, curso_id, asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo_id, dia_semana_id order by fecha) orden_id, decode(hor_f_fecha_entre(x.fecha, fecha_inicio, fecha_fin), 'S', decode(hor_f_fecha_entre(x.fecha, nvl(desde_el_dia, fecha_inicio), nvl(hasta_el_dia, fecha_fin)), 'S', 'S', 'N'), 'N') docencia, estudio_id,
                  curso_id, semestre_id, grupo_id, tipo_subgrupo_id, subgrupo_id, dia_Semana_id, asignatura_id, fecha_inicio, fecha_fin, fecha_examenes_inicio, fecha_examenes_fin, desde_el_dia, hasta_el_dia, repetir_cada_semanas,
                  numero_iteraciones, detalle_manual, tipo_dia, dia_semana
           from (select i.id, ia.estudio_id, ia.curso_id, i.semestre_id, i.grupo_id, i.tipo_subgrupo_id, i.subgrupo_id, i.dia_Semana_id, ia.asignatura_id, fecha_inicio, fecha_fin, fecha_examenes_inicio, fecha_examenes_fin, i.desde_el_dia,
                        hasta_el_dia, repetir_cada_semanas, numero_iteraciones, detalle_manual, c.fecha, tipo_dia, dia_semana
                 from hor_estudios e, hor_semestres_detalle s, hor_items i, hor_items_asignaturas ia, hor_ext_calendario c
                 where i.id = ia.item_id
                   and e.tipo_id = s.tipo_estudio_id
                   and ia.estudio_id = e.id
                   and i.semestre_id = s.semestre_id
                   and trunc(c.fecha) between fecha_inicio and nvl(fecha_examenes_fin, fecha_fin)
                   and c.dia_semana_id = i.dia_semana_id
                   and tipo_dia in ('L', 'E', 'F')
                   and vacaciones = 0
                   and detalle_manual = 0) x) d, hor_items i, hor_items_asignaturas ia
    where i.id = ia.item_id
      and ia.estudio_id = d.estudio_id
      and ia.curso_id = d.curso_id
      and i.semestre_id = d.semestre_id
      and ia.asignatura_id = d.asignatura_id
      and i.grupo_id = d.grupo_id
      and i.tipo_subgrupo_id = d.tipo_subgrupo_id
      and i.subgrupo_id = d.subgrupo_id
      and i.dia_semana_id = d.dia_semana_id
      and i.detalle_manual = 0
      and i.id = d.id
   union all
   select c.id, c.fecha, 'N' docencia_paso_1, 'N' docencia_paso_2, decode(d.id, null, 'N', 'S') docencia, 1 orden_id, numero_iteraciones, repetir_cada_semanas, fecha_inicio, fecha_fin, estudio_id, semestre_id, curso_id, asignatura_id,
          grupo_id, tipo_subgrupo_id, subgrupo_id, dia_semana_id, tipo_dia, decode(tipo_dia, 'F', 1, 0) festivos
   from (select i.id, c.fecha, numero_iteraciones, repetir_cada_semanas, s.fecha_inicio, s.fecha_fin, ia.estudio_id, i.semestre_id, ia.curso_id, ia.asignatura_id, i.grupo_id, i.tipo_subgrupo_id, i.subgrupo_id, i.dia_semana_id, tipo_dia
           from hor_estudios e, hor_semestres_detalle s, hor_items i, hor_items_asignaturas ia, hor_ext_calendario c
          where i.id = ia.item_id
            and e.tipo_id = s.tipo_estudio_id
            and ia.estudio_id = e.id
            and i.semestre_id = s.semestre_id
            and trunc(c.fecha) between fecha_inicio and nvl(fecha_examenes_fin, fecha_fin)
            and c.dia_semana_id = i.dia_semana_id
            and tipo_dia in ('L', 'E', 'F')
            and vacaciones = 0
            and detalle_manual = 1) c, hor_items_detalle d
   where c.id = d.item_id(+)
     and trunc(c.fecha) = trunc(d.inicio(+));

DROP VIEW UJI_HORARIOS.HOR_V_ITEMS_PROF_DETALLE;

/* Formatted on 30/06/2015 16:07:40 (QP5 v5.115.810.9015) */
create or replace force view UJI_HORARIOS.HOR_V_ITEMS_PROF_DETALLE(ID, DETALLE_ID, DET_PROFESOR_ID, ITEM_ID, INICIO, FIN, DESCRIPCION, PROFESOR_ID, DETALLE_MANUAL, CREDITOS, CREDITOS_DETALLE, SELECCIONADO, NOMBRE, PROFESOR_COMPLETO) as
     select to_number(id || det_profesor_id || item_id || to_char(inicio, 'yyyymmddhh24miss')) id, id detalle_id, det_profesor_id, item_id, inicio, fin, descripcion, profesor_id, detalle_manual, creditos, creditos_detalle,
            decode(detalle_manual, 0, 'S', decode(min(orden), 1, 'S', 'N')) seleccionado, nombre, profesor_completo
       from (select d.*, profesor_id, p.detalle_manual, i.creditos, p.creditos creditos_detalle, p.id det_profesor_id, 99 orden, nombre, (select wm_concat(nombre)
                                                                                                                                            from hor_items_profesores pr, hor_profesores p
                                                                                                                                           where item_id = i.id
                                                                                                                                             and pr.profesor_id = p.id)
                                                                                                                                            profesor_completo
               from hor_items i, hor_items_detalle d, hor_items_profesores p, hor_profesores pro
              where i.id = d.item_id
                and i.id = p.item_id
                and p.profesor_id = pro.id
             union all
             select i.id, i.id item_id, null inicio, null fin, null descripcion, profesor_id, p.detalle_manual, i.creditos, round(p.creditos, 2) creditos_detalle, p.id det_profesor_id, 99 orden, nombre, (select wm_concat(nombre)
                                                                                                                                                                                                              from hor_items_profesores pr,
                                                                                                                                                                                                                   hor_profesores p
                                                                                                                                                                                                             where item_id = i.id
                                                                                                                                                                                                               and pr.profesor_id = p.id)
                                                                                                                                                                                                              profesor_completo
               from hor_items i, hor_items_profesores p, hor_profesores pro
              where i.id = p.item_id
                and p.profesor_id = pro.id
                and not exists (select 1
                                  from hor_items_detalle
                                 where item_id = i.id)
             union all
             select d.*, profesor_id, p.detalle_manual, i.creditos, p.creditos creditos_detalle, p.id det_profesor_id, 1 orden, nombre, (select wm_concat(nombre)
                                                                                                                                           from hor_items_profesores pr, hor_profesores p
                                                                                                                                          where item_id = i.id
                                                                                                                                            and pr.profesor_id = p.id)
                                                                                                                                           profesor_completo
               from hor_items i, hor_items_detalle d, hor_items_profesores p, hor_items_det_profesores dp, hor_profesores pro
              where i.id = d.item_id
                and i.id = p.item_id
                and p.detalle_manual = 1
                and dp.detalle_id = d.id
                and item_profesor_id = p.id
                and p.profesor_id = pro.id)
   group by id, item_id, inicio, fin, descripcion, profesor_id, detalle_manual, creditos, creditos_detalle, det_profesor_id, nombre, profesor_completo;

DROP VIEW UJI_HORARIOS.HOR_V_PROFESOR_CREDITOS;

/* Formatted on 30/06/2015 16:07:50 (QP5 v5.115.810.9015) */
create or replace force view UJI_HORARIOS.HOR_V_PROFESOR_CREDITOS(ID, NOMBRE, CREDITOS_PROFESOR, CREDITOS_ASIGNADOS, CREDITOS_PENDIENTES, DEPARTAMENTO_ID, AREA_ID) as
     select id, nombre, creditos_profesor, sum(crd_final) creditos_asignados, creditos_profesor - sum(crd_final) creditos_pendientes, departamento_id, area_id
       from (  select id, nombre, creditos_profesor, grupo_id, tipo, asignatura_id, min(asignatura_txt), crd_final, departamento_id, area_id
                 from hor_v_items_creditos_detalle
             group by id, nombre, creditos_profesor, grupo_id, tipo, asignatura_id, crd_final, departamento_id, area_id)
   group by id, nombre, creditos_profesor, departamento_id, area_id;

CREATE OR REPLACE FUNCTION UJI_HORARIOS.hor_buscar_dia (p_ini date, p_fin date, p_dia_semana in number, p_orden in number)
   RETURN date IS
   v_rdo   date;
   v_aux   number;

   cursor lista is
      select   fecha
          from hor_ext_calendario
         where fecha between p_ini and p_fin
           and p_dia_semana = dia_semana_id
           and tipo_dia not in ('N', 'F')
      order by 1;
BEGIN
   v_aux := 0;

   for x in lista loop
      v_aux := v_aux + 1;

      if v_aux = p_orden then
         v_rdo := x.fecha;
      end if;
   end loop;

   return (trunc (v_rdo));
END hor_buscar_dia;

CREATE OR REPLACE FUNCTION UJI_HORARIOS.hor_buscar_nombre (p_asignatura in varchar2)
   RETURN varchar2 IS
   v_rdo   varchar2 (1000);
BEGIN
   select wm_concat (asignatura)
   into   v_rdo
   from   (select distinct asignatura
           from            hor_items i,
                           hor_items_asignaturas a
           where           i.id = a.item_id
           and             p_asignatura like '%' || asignatura_id || '%');

   return (v_rdo);
exception
   when others then
      return (sqlerrm);
END hor_buscar_nombre;

CREATE OR REPLACE FUNCTION UJI_HORARIOS.hor_contar_festivos (
   p_f_ini               date,
   p_f_fin               date,
   p_dia_semana     in   number,
   p_cada_semanas   in   number
)
   RETURN NUMBER IS
   v_rdo   NUMBER;
BEGIN
   v_rdo := 0;

   select count (*)
   into   v_rdo
   from   (select id, dia, mes, año, dia_semana, dia_semana_id, tipo_dia, fecha, vacaciones
           from   hor_ext_calendario c2
           where  c2.fecha between p_f_ini and p_f_fin
           and    tipo_dia = 'F'
           and    vacaciones = 0
           and    dia_semana_id = p_dia_semana
           and    p_cada_semanas = 1
           union all
           select id, dia, mes, año, dia_semana, dia_semana_id, tipo_dia, fecha, vacaciones
           from   (select id, dia, mes, año, dia_semana, dia_semana_id, tipo_dia, fecha, vacaciones,
                          row_number () over (partition by dia_semana_id order by fecha) orden
                   from   hor_ext_calendario c2
                   where  c2.fecha between p_f_ini and p_f_fin
                   and    dia_semana_id = p_dia_semana
                   and    vacaciones = 0
                   and    p_cada_semanas = 2) x
           where  tipo_dia = 'F'
           and    decode (mod (orden, 2), 1, 'S', 'N') = 'S'
           union all
           select id, dia, mes, año, dia_semana, dia_semana_id, tipo_dia, fecha, vacaciones
           from   (select id, dia, mes, año, dia_semana, dia_semana_id, tipo_dia, fecha, vacaciones,
                          row_number () over (partition by dia_semana_id order by fecha) orden
                   from   hor_ext_calendario c2
                   where  c2.fecha between p_f_ini and p_f_fin
                   and    dia_semana_id = p_dia_semana
                   and    vacaciones = 0
                   and    p_cada_semanas = 3) x
           where  tipo_dia = 'F'
           and    decode (mod (orden, 3), 1, 'S', 'N') = 'S'
           union all
           select id, dia, mes, año, dia_semana, dia_semana_id, tipo_dia, fecha, vacaciones
           from   (select id, dia, mes, año, dia_semana, dia_semana_id, tipo_dia, fecha, vacaciones,
                          row_number () over (partition by dia_semana_id order by fecha) orden
                   from   hor_ext_calendario c2
                   where  c2.fecha between p_f_ini and p_f_fin
                   and    dia_semana_id = p_dia_semana
                   and    vacaciones = 0
                   and    p_cada_semanas = 4) x
           where  tipo_dia = 'F'
           and    decode (mod (orden, 4), 1, 'S', 'N') = 'S');

   RETURN v_rdo;
END hor_contar_festivos;

CREATE OR REPLACE function UJI_HORARIOS.hor_contenido_item (hora_ini date, hora_fin date, texto varchar2, subtexto varchar2)
   return varchar2 is
   contenido   varchar2 (4000);
   vEspaciado  varchar2(100);
   vHorasEvento  number(8,3);
begin
   vHorasEvento := round(hora_fin-hora_ini, 3);
   if (vHorasEvento < 0.022) then  -- 1/2 hora o menos
      vEspaciado := '0';
      return '<fo:block font-size="6pt" text-align="center">'
                || to_char (hora_ini, 'hh24:mi') ||' - '||to_char (hora_fin, 'hh24:mi')
                ||' '||fof.bold (texto)||' '||subtexto
                ||'</fo:block>';
   elsif (vHorasEvento >= 0.022 and vHorasEvento < 0.043) then -- 1h
      vEspaciado := '0';
   elsif (vHorasEvento >= 0.043 and vHorasEvento < 0.064) then -- 1h y media
      vEspaciado := '2';
   elsif (vHorasEvento >= 0.064 and vHorasEvento < 0.084) then -- 2h
      vEspaciado := '5';
   elsif (vHorasEvento >= 0.084 and vHorasEvento < 0.105) then -- 2h y media
      vEspaciado := '7';
   elsif (vHorasEvento >= 0.105) then -- 3h
      vEspaciado := '10';
   else
      vEspaciado := to_char(trunc(50*(hora_fin-hora_ini)));
   end if;
   contenido := '<fo:block font-size="6pt" text-align="left">'
             || to_char (hora_ini, 'hh24:mi')--||' '|| vEspaciado
             ||'</fo:block>'
             ||'<fo:block font-size="6pt" text-align="center" space-before="'||vEspaciado||'mm" space-after="'||vEspaciado||'mm">'
             ||' '||fof.bold (texto)||' '||subtexto
             ||'</fo:block>'
             ||'<fo:block font-size="6pt" text-align="left">'
             || to_char (hora_fin, 'hh24:mi')--||' ' ||vHorasEvento
             ||'</fo:block>'
             ;
   return contenido;
end;

CREATE OR REPLACE FUNCTION UJI_HORARIOS.hor_f_fecha_entre (p_fecha in date, p_fecha_inicio in date, p_fecha_fin in date)
   RETURN varchar2 IS
BEGIN
   if p_fecha between trunc (p_Fecha_inicio) and trunc (p_fecha_fin) then
      return ('S');
   else
      return ('N');
   end if;
END hor_f_fecha_entre;

CREATE OR REPLACE FUNCTION UJI_HORARIOS.hor_f_fecha_orden (
   p_estudio         in   number,
   p_semestre        in   number,
   p_curso           in   number,
   p_asignatura      in   varchar2,
   p_grupo           in   varchar2,
   p_subgrupo_tipo   in   varchar2,
   p_subgrupo        in   number,
   p_dia_semana      in   number,
   p_fecha           in   date
)
   RETURN NUMBER IS
   v_aux   NUMBER;
BEGIN
   select count (*)
   into   v_aux
   from   (select ia.estudio_id, ia.curso_id, i.semestre_id, i.grupo_id, i.tipo_subgrupo_id, i.subgrupo_id,
                  i.dia_Semana_id, ia.asignatura_id, fecha_inicio, fecha_fin, fecha_examenes_inicio, fecha_examenes_fin,
                  i.desde_el_dia, hasta_el_dia, repetir_cada_semanas, numero_iteraciones, detalle_manual, c.fecha,
                  tipo_dia, dia_semana
           from   hor_estudios e,
                  hor_semestres_detalle s,
                  hor_items i,
                  hor_items_asignaturas ia,
                  hor_ext_calendario c
           where  e.tipo_id = s.tipo_estudio_id
           and    i.semestre_id = s.semestre_id
           and    c.fecha between fecha_inicio and nvl (fecha_examenes_fin, fecha_fin)
           and    c.dia_semana_id = i.dia_semana_id
           and    tipo_dia = 'L'
           and    detalle_manual = 0
           and    i.semestre_id = p_semestre
           and    ia.curso_id = p_curso
           and    i.id = ia.item_id
           and    ia.asignatura_id = p_asignatura
           and    ia.estudio_id = p_estudio
           and    ia.estudio_id = e.id
           and    i.grupo_id = p_grupo
           and    i.tipo_subgrupo_id = p_subgrupo_tipo
           and    i.subgrupo_id = p_subgrupo
           and    i.dia_semana_id = p_dia_semana
           and    fecha <= p_fecha);

   RETURN v_aux;
END hor_f_fecha_orden;

CREATE OR REPLACE PROCEDURE UJI_HORARIOS.CIRCUITOS_VALIDACION_PDF (
   name_array    IN   euji_util.ident_arr2,
   value_array   IN   euji_util.ident_arr2
) IS
   -- Parametros
   pEstudio    CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pEstudio');
   pCircuito   CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pCircuito');
   pGrupo      CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pGrupo');
   --pCurso      CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pCurso');
   pSesion     CONSTANT VARCHAR2 (4000) := euji_util.euji_getparam (name_array, value_array, 'pSesion');
   v_nombre             varchar2 (2000);
   vSesion              number;

   cursor lista_circuito is
      select grupo_id, nombre, plazas
      from   hor_circuitos
      where  id = pCircuito
      and    grupo_id = pGrupo;

   cursor lista_semestres is
      select   1 id
      from     dual
      union all
      select   2 id
      from     dual
      order by 1;

   procedure cabecera is
   begin
      owa_util.mime_header (ccontent_type => 'text/xml', bclose_header => true);
      fop.documentOpen (margin_left => 1.5, margin_right => 1.5, margin_top => 0.5, margin_bottom => 0.5,
                        extent_before => 3, extent_after => 1.5, extent_margin_top => 3, extent_margin_bottom => 1.5);
      fop.pageBodyOpen;

      for x in lista_circuito loop
         htp.p (fof.documentheaderopen || fof.blockopen (space_after => 0.1) || fof.tableOpen
                || fof.tablecolumndefinition (column_width => 3) || fof.tablecolumndefinition (column_width => 15)
                || fof.tablebodyopen || fof.tableRowOpen || fof.tablecellopen
                || fof.block (fof.logo_uji, text_align => 'center') || fof.tablecellclose
                || fof.tablecellopen (display_align => 'center', padding => 0.4)
                || fof.block (text => 'Controls Circuits', font_size => 12, font_weight => 'bold',
                              font_family => 'Arial', text_align => 'center')
                || fof.block (text => v_nombre, font_size => 12, font_weight => 'bold', font_family => 'Arial',
                              text_align => 'center')
                || fof.block (text => 'Curs: 1 - Grup: ' || pGrupo || ' - Circuit: ' || x.nombre || ' (' || x.plazas
                               || ' pla?es)',
                              font_size => 12, font_weight => 'bold', font_family => 'Arial', text_align => 'center')
                || fof.tablecellclose || fof.tableRowClose || fof.tablebodyclose || fof.tableClose || fof.blockclose
                || fof.documentheaderclose);
         -- El numero paginas se escribe a mano para poner el texto m?s peque?o
         htp.p (fof.documentfooteropen || fof.tableOpen || fof.tablecolumndefinition (column_width => 18)
                || fof.tablebodyopen || fof.tableRowOpen || fof.tablecellopen
                || fof.block (text => 'Pag. <fo:page-number /> de <fo:page-number-citation ref-id="lastBlock" />',
                              text_align => 'right', font_weight => 'bold', font_size => 8)
                || fof.tablecellclose || fof.tableRowClose || fof.tablebodyclose || fof.tableClose
                || fof.documentfooterclose);
      end loop;

      fop.pageFlowOpen;
      fop.block (fof.white_space);
   end;

   procedure pie is
   begin
      htp.p ('<fo:block  id="lastBlock" space-before="1cm" font-size="8pt">');
      htp.p ('Data de generaci?: ' || to_char (sysdate, 'dd/mm/yyy hh24:mi'));
      fop.blockclose ();
      fop.pageFlowClose;
      fop.pageBodyClose;
      fop.documentClose;
   end;

   function espacios (p_espacios in number)
      return varchar2 is
      v_rdo   varchar2 (2000);
   begin
      v_rdo := null;

      for x in 1 .. p_espacios loop
         v_rdo := v_rdo || fof.white_space;
      end loop;

      return v_rdo;
   end;

   procedure p (p_texto in varchar2) is
   begin
      fop.block (espacios (3) || p_texto);
   end;

   procedure control_1 (pSemestre in number) is
      v_aux   number := 0;

      cursor lista_errores_1 is
         select   *
         from     (select distinct ia.asignatura_id, tipo_subgrupo_id, grupo_id
                   from            hor_items i,
                                   hor_items_asignaturas ia
                   where           ia.item_id = i.id
                   and             ia.estudio_id = pEstudio
                   and             semestre_id = pSemestre
                   and             grupo_id = pGrupo
                   and             tipo_subgrupo_id not in ('AV', 'TU')
                   and             ia.curso_id = 1
                   minus
                   select distinct ia.asignatura_id, tipo_subgrupo_id, grupo_id
                   from            hor_items_circuitos ic,
                                   hor_items i,
                                   hor_items_asignaturas ia
                   where           circuito_id = pCircuito
                   and             i.id = ic.item_id
                   and             ia.item_id = i.id
                   and             ia.estudio_id = pEstudio
                   and             semestre_id = pSemestre
                   and             grupo_id = pGrupo)
         order by 1,
                  decode (tipo_subgrupo_id, 'TE', 1, 'PR', 2, 'LA', 3, 'SE', 4, 5);
   begin
      fop.block ('Control 1 - Subgrups que no estan en el circuit', font_size => 12, border_bottom => 'solid',
                 space_after => 0.1);

      for x in lista_errores_1 loop
         v_aux := v_aux + 1;
         p (x.asignatura_id || ' ' || x.tipo_subgrupo_id);
      end loop;

      if v_aux = 0 then
         fop.block ('No hi ha cap error.', space_before => 0.5);
      elsif v_aux = 1 then
         fop.block ('Trovat ' || v_aux || ' error.', space_before => 0.5, space_after => 0.5);
      else
         fop.block ('Trovats ' || v_aux || ' errors.', space_before => 0.5, space_after => 0.5);
      end if;
   exception
      when others then
         p (sqlerrm);
   end;

   procedure control_2 (pSemestre in number) is
      v_aux   number := 0;

      cursor lista_errores_2 is
         select distinct ia.asignatura_id, tipo_subgrupo_id, subgrupo_id, grupo_id
         from            hor_items_circuitos ic,
                         hor_items i,
                         hor_items_asignaturas ia
         where           circuito_id = pCircuito
         and             i.id = ic.item_id
         and             ia.item_id = i.id
         and             ia.estudio_id = pEstudio
         and             semestre_id = pSemestre
         and             grupo_id = pGrupo
         and             (asignatura_id, grupo_id, tipo_subgrupo_id) in (
                            select   asignatura_id, grupo_id, tipo_subgrupo_id
                            from     (select distinct ia.asignatura_id, tipo_subgrupo_id, subgrupo_id, grupo_id
                                      from            hor_items_circuitos ic,
                                                      hor_items i,
                                                      hor_items_asignaturas ia
                                      where           circuito_id = pCircuito
                                      and             i.id = ic.item_id
                                      and             ia.item_id = i.id
                                      and             ia.estudio_id = pEstudio
                                      and             semestre_id = pSemestre
                                      and             grupo_id = pGrupo)
                            group by asignatura_id,
                                     grupo_id,
                                     tipo_subgrupo_id
                            having   count (*) > 1)
         order by        1,
                         decode (tipo_subgrupo_id, 'TE', 1, 'PR', 2, 'LA', 3, 'SE', 4, 5),
                         3;
   begin
      fop.block ('Control 2 - Assignatures amb m?s d''un subgrup per tipus al circuit', font_size => 12,
                 border_bottom => 'solid', space_after => 0.1);

      for x in lista_errores_2 loop
         v_aux := v_aux + 1;
         p (x.asignatura_id || ' ' || x.tipo_subgrupo_id || x.subgrupo_id);
      end loop;

      if v_aux = 0 then
         fop.block ('No hi ha cap error.', space_before => 0.5);
      elsif v_aux = 1 then
         fop.block ('Trovat ' || v_aux || ' error.', space_before => 0.5, space_after => 0.5);
      else
         fop.block ('Trovats ' || v_aux || ' errors.', space_before => 0.5, space_after => 0.5);
      end if;
   exception
      when others then
         p (sqlerrm);
   end;

   procedure control_3 is
      v_aux   number := 0;

      cursor lista_errores_3 is
         select a.estudio_id, a.asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo_id, i.id
         from   hor_items_circuitos c,
                hor_items i,
                hor_circuitos_estudios e,
                hor_items_asignaturas a
         where  c.circuito_id = e.circuito_id
         and    c.item_id = i.id
         and    dia_semana_id is null
         and    c.circuito_id = pCircuito
         and    e.estudio_id = pEstudio
         and    i.id = a.item_id
         and    e.estudio_id = a.estudio_id;
   begin
      fop.block ('Control 3 - Assignatures o clases sense planificar en el circuit', font_size => 12,
                 border_bottom => 'solid', space_after => 0.1, space_before => 0.5);

      for x in lista_errores_3 loop
         v_aux := v_aux + 1;
         p (x.asignatura_id || ' ' || x.grupo_id || ' ' || x.tipo_subgrupo_id || x.subgrupo_id || ' (' || x.id || ')');
      end loop;

      if v_aux = 0 then
         fop.block ('No hi ha cap error.', space_before => 0.5);
      elsif v_aux = 1 then
         fop.block ('Trovat ' || v_aux || ' error.', space_before => 0.5, space_after => 0.5);
      else
         fop.block ('Trovats ' || v_aux || ' errors.', space_before => 0.5, space_after => 0.5);
      end if;
   exception
      when others then
         p (sqlerrm);
   end;
BEGIN
   select count (*)
   into   vSesion
   from   apa_sesiones
   where  sesion_id = pSesion
   and    fecha < sysdate () - 0.33;

   if vSesion > 0 then
      select nombre
      into   v_nombre
      from   hor_estudios
      where  id = pEstudio;

      cabecera;

      for x in lista_semestres loop
         fop.block ('Semestre ' || x.id, font_weight => 'bold', text_align => 'center', font_size => 14,
                    space_after => 0.5);
         control_1 (x.id);
         control_2 (x.id);
      end loop;

      control_3;
      pie;
   else
      cabecera;
      fop.block ('No tens perm?s per accedir a aquest document.', font_weight => 'bold', text_align => 'center',
                 font_size => 14, space_after => 0.5);
      pie;
   end if;
EXCEPTION
   WHEN OTHERS THEN
      htp.p (sqlerrm);
END;

CREATE OR REPLACE PROCEDURE UJI_HORARIOS.examenes_validacion_pdf (
   name_array    IN   euji_util.ident_arr2,
   value_array   IN   euji_util.ident_arr2
) IS
   v_aux           NUMBER;
   pEstudio        number          := euji_util.euji_getparam (name_array, value_array, 'pEstudio');
   pConvocatoria   number          := euji_util.euji_getparam (name_array, value_array, 'pConvocatoria');
   pSesion         VARCHAR2 (400)  := euji_util.euji_getparam (name_array, value_array, 'pSesion');
   v_nombre        varchar2 (2000);
   vSesion         number;
   vCurso          number;

   procedure cabecera is
   begin
      owa_util.mime_header (ccontent_type => 'text/xml', bclose_header => true);
      fop.documentOpen (margin_left => 1.5, margin_right => 1.5, margin_top => 0.5, margin_bottom => 0.5,
                        extent_before => 3, extent_after => 1.5, extent_margin_top => 3, extent_margin_bottom => 1.5);
      fop.pageBodyOpen;
      htp.p (fof.documentheaderopen || fof.blockopen (space_after => 0.1) || fof.tableOpen
             || fof.tablecolumndefinition (column_width => 3) || fof.tablecolumndefinition (column_width => 15)
             || fof.tablebodyopen || fof.tableRowOpen || fof.tablecellopen
             || fof.block (fof.logo_uji, text_align => 'center') || fof.tablecellclose
             || fof.tablecellopen (display_align => 'center', padding => 0.4)
             || fof.block (text => v_nombre, font_size => 16, font_weight => 'bold', font_family => 'Arial',
                           text_align => 'center')
             || fof.block (text => 'Control d''exàmens - Curs: ' || vCurso || '/' || substr (to_char (vCurso + 1), 3),
                           font_size => 16, font_weight => 'bold', font_family => 'Arial', text_align => 'center')
             || fof.tablecellclose || fof.tableRowClose || fof.tablebodyclose || fof.tableClose || fof.blockclose
             || fof.documentheaderclose);
      -- El numero paginas se escribe a mano para poner el texto m?s peque?o
      htp.p (fof.documentfooteropen || fof.tableOpen || fof.tablecolumndefinition (column_width => 18)
             || fof.tablebodyopen || fof.tableRowOpen || fof.tablecellopen
             || fof.block (text => 'Pag. <fo:page-number /> de <fo:page-number-citation ref-id="lastBlock" />',
                           text_align => 'right', font_weight => 'bold', font_size => 8)
             || fof.tablecellclose || fof.tableRowClose || fof.tablebodyclose || fof.tableClose
             || fof.documentfooterclose);
      fop.pageFlowOpen;
      fop.block (fof.white_space);
   end;

   procedure pie is
   begin
      htp.p ('<fo:block  id="lastBlock" space-before="1cm" font-size="8pt">');
      htp.p ('Data de generació: ' || to_char (sysdate, 'dd/mm/yyy hh24:mi'));
      fop.blockclose ();
      fop.pageFlowClose;
      fop.pageBodyClose;
      fop.documentClose;
   end;

   function espacios (p_espacios in number)
      return varchar2 is
      v_rdo   varchar2 (2000);
   begin
      v_rdo := null;

      for x in 1 .. p_espacios loop
         v_rdo := v_rdo || fof.white_space;
      end loop;

      return v_rdo;
   end;

   procedure control_1 (p_estudio in number, p_convocatoria in number) is
      v_conv   number;

      cursor lista_examenes_sin_fecha (p_estudio in number, p_convocatoria in number) is
         select distinct convocatoria_id, convocatoria_nombre, asignatura_id, asignatura_nombre
         from            hor_examenes e,
                         hor_examenes_asignaturas ia
         where           (   convocatoria_id = p_convocatoria
                          or p_convocatoria = -1)
         and             e.id = ia.examen_id
         and             estudio_id = p_estudio
         and             fecha is null
         order by        1,
                         2,
                         3,
                         4;
   begin
      v_conv := 0;

      for x in lista_Examenes_sin_fecha (p_estudio, p_convocatoria) loop
         if v_conv <> x.convocatoria_id then
            fop.block (fof.white_space);
            fop.block (espacios (1) || x.convocatoria_nombre, font_size => 14, font_weight => 'bold');
            fop.block (fof.white_space);
            v_conv := x.convocatoria_id;
         end if;

         fop.block (espacios (4) || x.asignatura_id || ' - ' || x.asignatura_nombre, font_size => 12);
      end loop;
   end;

   procedure control_2 (p_estudio in number, p_convocatoria in number) is
      v_conv    number;
      v_fecha   date   := to_Date ('1-1-1', 'dd/mm/yyyy');

      cursor lista_examenes_menos_24 (p_estudio in number, p_convocatoria in number) is
         with base as
              (select e.id, convocatoria_id, convocatoria_nombre, asignatura_id, asignatura_nombre, trunc (fecha) fecha,
                      to_date (to_char (e.fecha, 'dd/mm/yyyy') || ' ' || to_char (e.hora_inicio, 'hh24:mi'),
                               'dd/mm/yyyy hh24:mi') hora_inicio,
                      to_date (to_char (e.fecha, 'dd/mm/yyyy') || ' ' || to_char (e.hora_fin, 'hh24:mi'),
                               'dd/mm/yyyy hh24:mi') hora_fin,
                      curso_id
               from   hor_examenes e,
                      hor_examenes_asignaturas ia
               where  (   convocatoria_id = p_convocatoria
                       or p_convocatoria = -1)
               and    e.id = ia.examen_id
               and    estudio_id = p_estudio
               and    fecha is not null)
         select distinct uno.*
         from            (select *
                          from   base) uno,
                         (select *
                          from   base) dos
         where           uno.id <> dos.id
         and             abs (uno.hora_inicio - dos.hora_inicio) < 1
         and             uno.curso_id = dos.curso_id
         order by        uno.fecha,
                         uno.hora_inicio,
                         uno.asignatura_id;
   begin
      v_conv := 0;

      for x in lista_examenes_menos_24 (p_estudio, p_convocatoria) loop
         if v_conv <> x.convocatoria_id then
            fop.block (fof.white_space);
            fop.block (espacios (1) || x.convocatoria_nombre, font_size => 14, font_weight => 'bold');
            fop.block (fof.white_space);
         else
            if v_Fecha <> x.fecha then
               fop.block (fof.white_space);
            end if;
         end if;

         fop.block (espacios (4) || x.asignatura_id || ' - ' || to_char (x.fecha, 'dd/mm/yyyy') || ' '
                    || to_char (x.hora_inicio, 'hh24:mi') || '-' || to_char (x.hora_fin, 'hh24:mi') || ' ('
                    || x.asignatura_nombre || ')',
                    font_size => 12);
         v_conv := x.convocatoria_id;
         v_fecha := x.fecha;
      end loop;
   end;
BEGIN
   select count (*) + 1
   into   vSesion
   from   apa_sesiones
   where  sesion_id = pSesion
   and    fecha < sysdate () - 0.33;

   select id
   into   vCurso
   from   hor_curso_academico;

   select estudio
   into   v_nombre
   from   hor_items_asignaturas
   where  estudio_id = pEstudio
   and    rownum <= 1;

   if vSesion > 0 then
      cabecera;

      begin
         fop.block (fof.bold ('Control 1 - Exàmens pendents d''assignar data'), font_size => 16);
         control_1 (pEstudio, pConvocatoria);
         fop.block (fof.white_space);
         fop.block (fof.white_space);
      exception
         when others then
            fop.block ('Error control 1: ' || sqlerrm, font_size => 12);
      end;

      begin
         fop.block (fof.bold ('Control 2 - Exàmens d''un curs amb menys de 24 hores'), font_size => 16);
         control_2 (pEstudio, pConvocatoria);
         fop.block (fof.white_space);
         fop.block (fof.white_space);
      exception
         when others then
            fop.block ('Error control 2: ' || sqlerrm, font_size => 12);
      end;

      pie;
   else
      cabecera;
      fop.block ('No tens permís per accedir a aquest document.', font_weight => 'bold', text_align => 'center',
                 font_size => 14, space_after => 0.5);
      pie;
   end if;
END;

CREATE OR REPLACE PROCEDURE UJI_HORARIOS.HORARIOS_CIRCUITO_PDF (
   name_array    IN   euji_util.ident_arr2,
   value_array   IN   euji_util.ident_arr2
) IS
   -- Parametros
   pEstudio    CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pEstudio');
   pCircuito   CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pCircuito');
   pSesion     CONSTANT VARCHAR2 (4000) := euji_util.euji_getparam (name_array, value_array, 'pSesion');
   pSemestre   CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pSemestre');
   pGrupo      CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pGrupo');
   pSemana     CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pSemana');
   pCurso               number          := 1;
   vCursoAcaNum         number;
   vSesion              number;
   vFechaIniSemestre    date;
   vFechaFinSemestre    date;
   vEsGenerica          boolean         := pSemana = 'G';

     -- Variables
   --  vPerId              per_personas.id%TYPE := euji_get_perid ();
   CURSOR c_items_gen IS
      SELECT asignatura_id, asignatura, estudio, asi.caracter, semestre_id, comun, grupo_id, tipo_subgrupo,
             tipo_subgrupo_id, subgrupo_id, dia_semana_id, hora_inicio, hora_fin, tipo_asignatura_id, tipo_asignatura,
             to_date (dia_semana_id + 1 || '/01/2006 ' || to_char (hora_fin, 'hh24:mi'), 'dd/mm/yyyy hh24:mi') fin,
             to_date (dia_semana_id + 1 || '/01/2006 ' || to_char (hora_inicio, 'hh24:mi'),
                      'dd/mm/yyyy hh24:mi') inicio
      FROM   UJI_HORARIOS.HOR_ITEMS items,
             uji_horarios.hor_items_asignaturas asi,
             uji_horarios.hor_items_circuitos cir
      WHERE  items.id = asi.item_id
      and    cir.item_id = items.id
      and    cir.circuito_id = pCircuito
      and    asi.estudio_id = pEstudio
      AND    asi.curso_id = pCurso
      AND    items.grupo_id = pGrupo
      AND    items.semestre_id = pSemestre
      AND    items.dia_semana_id IS NOT NULL
      GROUP BY   asignatura_id,
           asignatura,
           estudio,
           asi.caracter,
           semestre_id,
           comun,
           grupo_id,
           tipo_subgrupo,
           tipo_subgrupo_id,
           subgrupo_id,
           dia_semana_id,
           hora_inicio,
           hora_fin,
           tipo_asignatura_id,
           tipo_asignatura;

   CURSOR c_items_det (vIniDate date, vFinDate date) IS
      SELECT   asignatura_id, asignatura, estudio, grupo_id, tipo_subgrupo, tipo_subgrupo_id, subgrupo_id,
               dia_semana_id, hora_inicio, hora_fin, inicio, fin
      FROM     UJI_HORARIOS.HOR_ITEMS items,
               UJI_HORARIOS.HOR_ITEMS_DETALLE det,
               uji_horarios.hor_items_asignaturas asi,
               uji_horarios.hor_items_circuitos cir
      WHERE    items.id = asi.item_id
      and      cir.item_id = items.id
      and      cir.circuito_id = pCircuito
      and      asi.estudio_id = pEstudio
      AND      asi.curso_id = pCurso
      AND      items.grupo_id = pGrupo
      AND      items.semestre_id = pSemestre
      AND      items.dia_semana_id IS NOT NULL
      and      det.item_id = items.id
      and      det.inicio > vIniDate
      and      det.fin < vFinDate
      GROUP BY asignatura_id,
               asignatura,
               estudio,
               grupo_id,
               tipo_subgrupo,
               tipo_subgrupo_id,
               subgrupo_id,
               dia_semana_id,
               hora_inicio,
               hora_fin,
               inicio,
               fin
      order by det.inicio;

   CURSOR c_asigs is
      SELECT distinct (asignatura_id), asignatura
      FROM            UJI_HORARIOS.HOR_ITEMS items,
                      uji_horarios.hor_items_asignaturas asi,
                      uji_horarios.hor_items_circuitos itemcir
      WHERE           items.id = asi.item_id
      and             items.id = itemcir.item_id
      and             itemcir.circuito_id = pCircuito
      and             asi.estudio_id = pEstudio
      AND             asi.curso_id = pCurso
      AND             items.grupo_id = pGrupo
      AND             items.semestre_id = pSemestre
      AND             items.dia_semana_id IS NOT NULL
      order by        asignatura_id;

   CURSOR c_clases_asig (vAsignaturaId varchar2) is
      SELECT   tipo_subgrupo_id || subgrupo_id subgrupo,
               decode (dia_semana_id, 1, 'L', 2, 'M', 3, 'X', 4, 'J', 5, 'V', '?') dia_sem,
               to_char (hora_fin, 'hh24:mi') hora_fin_str, to_char (hora_inicio, 'hh24:mi') hora_inicio_str
      FROM     UJI_HORARIOS.HOR_ITEMS items,
               uji_horarios.hor_items_asignaturas asi,
               uji_horarios.hor_items_circuitos itemcir
      WHERE    items.id = asi.item_id
      and      items.id = itemcir.item_id
      and      itemcir.circuito_id = pCircuito
      and      asi.estudio_id = pEstudio
      AND      asi.curso_id = pCurso
      AND      items.grupo_id = pGrupo
      AND      items.semestre_id = pSemestre
      and      asi.asignatura_id = vAsignaturaId
      AND      items.dia_semana_id IS NOT NULL
      order by dia_semana_id,
               hora_inicio,
               subgrupo;

   vFechas              t_horario;
   vRdo                 clob;

   procedure cabecera is
   begin
      owa_util.mime_header (ccontent_type => 'text/xml', bclose_header => true);
      fop.documentOpen (page_width => 29.7, page_height => 21, margin_left => 0.5, margin_right => 0.5,
                        margin_top => 0.5, margin_bottom => 0, extent_before => 0, extent_after => 0.5,
                        extent_margin_top => 0, extent_margin_bottom => 0.5);
      fop.pageBodyOpen;
      fop.documentfooteropen;
      fop.block ('Fecha documento: ' || to_char (sysdate, 'yyyy/mm/dd HH24:MI'), font_size => 6,
                 font_style => 'italic');
      fop.documentfooterclose;
      fop.pageFlowOpen;
   end;

   procedure calcula_curso_aca is
   begin
      SELECT   curso_academico_id
      into     vCursoAcaNum
      FROM     hor_semestres_detalle d,
               hor_estudios e
      WHERE    e.id = pEstudio
      AND      e.tipo_id = d.tipo_estudio_id
      AND      ROWNUM = 1
      ORDER BY fecha_inicio;
   end;

   procedure info_calendario_gen is
      vCursoAca           varchar2 (4000);
      vTitulacionNombre   varchar2 (4000);
      vCircuitoNombre     varchar2 (4000);
      vCursoNombre        varchar2 (4000) := pCurso || '?';
      vTextoCabecera      varchar2 (4000);
   begin
      select nombre
      into   vTitulacionNombre
      from   hor_estudios
      where  id = pEstudio;

      select nombre
      into   vCircuitoNombre
      from   hor_circuitos
      where  id = pCircuito;

      vCursoAca := to_char (vCursoAcaNum) || '/' || to_char (vCursoAcaNum + 1);

      if pCurso in ('1', '3') then
         vCursoNombre := pCurso || 'er';
      elsif pCurso = '2' then
         vCursoNombre := '2on';
      elsif pCurso = '4' then
         vCursoNombre := '4t';
      else
         vCursoNombre := pCurso || '?';
      end if;

      vTextoCabecera :=
         vTitulacionNombre || ' - Circuit ' || vCircuitoNombre || ' - ' || vCursoNombre || ' curs - Grup ' || pGrupo
         || ' (semestre ' || pSemestre || ') - ' || vCursoAca;
      fop.block (text => vTextoCabecera, border_bottom => 'solid', border_width => 0.05);
   end;

   procedure muestra_leyenda_asig (item c_asigs%ROWTYPE) is
      vClasesText   varchar2 (4000);
   begin
      fop.block (item.asignatura_id || ', ' || item.asignatura || ':', font_size => 7, padding_top => 0.1);
      vClasesText := '<fo:inline color="white">___</fo:inline>';

      for c in c_clases_asig (item.asignatura_id) loop
         vClasesText :=
            vClasesText || c.dia_sem || ', ' || c.hora_inicio_str || '-' || c.hora_fin_str || ' (' || c.subgrupo
            || '); ';
      end loop;

      --vClasesText := substr (vClasesText, 1, length (vClasesText) - 2);
      fop.block (vClasesText, font_size => 7, padding_top => 0.06);
   end;

   procedure leyenda_asigs is
   begin
      fop.block (' ', padding_top => 1);

      for item in c_asigs loop
         muestra_leyenda_asig (item);
      end loop;
   end;

   procedure pie is
   begin
      fop.pageFlowClose;
      fop.pageBodyClose;
      fop.documentClose;
   end;

   procedure calendario_gen is
   begin
      vFechas := t_horario ();

      for item in c_items_gen loop
         vFechas.extend;
         vFechas (vFechas.count) :=
            ItemHorarios (hor_contenido_item (item.hora_inicio, item.hora_fin, item.asignatura_id,
                                              '(' || item.tipo_subgrupo_id || item.subgrupo_id || ')'),
                          redondea_fecha_a_cuartos(item.inicio), redondea_fecha_a_cuartos(item.fin), null);
      end loop;

      if vFechas.count > 0 then
         vRdo := xpfpkg_horarios_academicos.drawTimetable (vFechas, 5, null, 'CA', 'PDF_APP_HORARIOS');
         xup_utils.dump (vRdo);
         dbms_lob.freeTemporary (vRdo);
      end if;
   end;

   procedure calendario_det (vFechaIni date, vFechaFin date) is
   begin
      vFechas := t_horario ();

      for item in c_items_det (vFechaIni, vFechaFin) loop
         vFechas.extend;
         vFechas (vFechas.count) :=
            ItemHorarios (hor_contenido_item (item.hora_inicio, item.hora_fin, item.asignatura_id,
                                              '(' || item.tipo_subgrupo_id || item.subgrupo_id || ')'),
                          redondea_fecha_a_cuartos(item.inicio), redondea_fecha_a_cuartos(item.fin), null);
      end loop;

      if vFechas.count > 0 then
         vRdo := xpfpkg_horarios_academicos.drawTimetable (vFechas, 5, null, 'CA', 'PDF_APP_HORARIOS_DET');
         xup_utils.dump (vRdo);
         dbms_lob.freeTemporary (vRdo);
      end if;
   end;

   procedure calcula_fechas_semestre is
   begin
      SELECT fecha_inicio, fecha_examenes_fin
      into   vFechaIniSemestre, vFechaFinSemestre
      FROM   hor_semestres_detalle,
             hor_estudios e
      WHERE  semestre_id = pSemestre
      and    e.id = pEstudio
      and    e.tipo_id = tipo_estudio_id;
   end;

   function semana_tiene_clase (vIniDate date, vFinDate date)
      return boolean is
      num_clases   number;
   begin
      SELECT   count (*)
      into     num_clases
      FROM     UJI_HORARIOS.HOR_ITEMS items,
               UJI_HORARIOS.HOR_ITEMS_DETALLE det,
               uji_horarios.hor_items_asignaturas asi
      WHERE    items.id = asi.item_id
      and      asi.estudio_id = pEstudio
      AND      asi.curso_id = pCurso
      AND      items.grupo_id = pGrupo
      AND      items.semestre_id = pSemestre
      AND      items.dia_semana_id IS NOT NULL
      and      det.item_id = items.id
      and      det.inicio > vIniDate
      and      det.fin < vFinDate
      order by det.inicio;

      return num_clases > 0;
   end;

   procedure info_calendario_det (vFechaIni date, vFechaFin date) is
      vCursoAca           varchar2 (4000);
      vTitulacionNombre   varchar2 (4000);
      vCursoNombre        varchar2 (4000) := pCurso || '?';
      vCircuitoNombre     varchar2 (4000);
      vTextoCabecera      varchar2 (4000);
      vFechaIniStr        varchar2 (4000) := to_char (vFechaIni, 'dd/mm/yyyy');
      vFechaFinStr        varchar2 (4000) := to_char (vFechaFin - 1, 'dd/mm/yyyy');
   begin
      select nombre
      into   vTitulacionNombre
      from   hor_estudios
      where  id = pEstudio;

      select nombre
      into   vCircuitoNombre
      from   hor_circuitos
      where  id = pCircuito;

      vCursoAca := to_char (vCursoAcaNum) || '/' || to_char (vCursoAcaNum + 1);

      if pCurso in ('1', '3') then
         vCursoNombre := pCurso || 'er';
      elsif pCurso = '2' then
         vCursoNombre := '2on';
      elsif pCurso = '4' then
         vCursoNombre := '4t';
      else
         vCursoNombre := pCurso || '?';
      end if;

      vTextoCabecera :=
         vTitulacionNombre || ' - Circuit ' || vCircuitoNombre || ' - ' || vCursoNombre || ' curs - Grup ' || pGrupo
         || ' (semestre ' || pSemestre || ') - Del ' || vFechaIniStr || ' al ' || vFechaFinStr;
      fop.block (text => vTextoCabecera, border_bottom => 'solid', border_width => 0.05);
   end;

   procedure paginas_calendario_det is
      vFechaIniSemana   date;
      vFechaFinSemana   date;
   begin
      calcula_fechas_semestre;
      vFechaIniSemana := vFechaIniSemestre;
      vFechaFinSemana := vFechaIniSemana + 7;

      while vFechaFinSemana <= vFechaFinSemestre loop
         if semana_tiene_clase (vFechaIniSemana, vFechaFinSemana) then
            info_calendario_det (vFechaIniSemana, vFechaFinSemana);
            calendario_det (vFechaIniSemana, vFechaFinSemana);
            htp.p ('<fo:block break-before="page" color="white">_</fo:block>');
         end if;

         vFechaIniSemana := vFechaFinSemana;
         vFechaFinSemana := vFechaFinSemana + 7;
      end loop;
   end;
BEGIN
   select count (*)
   into   vSesion
   from   apa_sesiones
   where  sesion_id = pSesion
   and    fecha < sysdate () - 0.33;

   if vSesion > 0 then
      calcula_curso_aca;
      cabecera;

      if vEsGenerica then
         info_calendario_gen;
         calendario_gen;
         leyenda_asigs;
      else
         paginas_calendario_det;
         leyenda_asigs;
      end if;

      pie;
   else
      cabecera;
      fop.block ('No tens perm?s per accedir a aquest document.', font_weight => 'bold', text_align => 'center',
                 font_size => 14, space_after => 0.5);
      pie;
   end if;
EXCEPTION
   WHEN OTHERS THEN
      htp.p (sqlerrm);
END;

CREATE OR REPLACE PROCEDURE UJI_HORARIOS.horarios_examenes_pdf (
   name_array    IN   euji_util.ident_arr2,
   value_array   IN   euji_util.ident_arr2
) IS
   v_aux            NUMBER;
   pEstudio         number          := euji_util.euji_getparam (name_array, value_array, 'pEstudio');
   pConvocatoria    number          := euji_util.euji_getparam (name_array, value_array, 'pConvocatoria');
   pCurso           number          := euji_util.euji_getparam (name_array, value_array, 'pCurso');
   pSesion          VARCHAR2 (400)  := euji_util.euji_getparam (name_array, value_array, 'pSesion');
   v_nombre         varchar2 (2000);
   v_convocatoria   varchar2 (2000);
   vSesion          number;
   vCurso           number;
   vCuantos         number;
   vFechas          t_horario;
   vRdo             clob;

   cursor lista_semanas (p_estudio in number, p_convocatoria in number, p_Curso in number) is
      select   año, semana, min(fecha) lunes, max(fecha) domingo
      from     (select x.*, c.*, to_number (to_char (fecha, 'iw')) semana
                from   (select trunc (min (fecha)) inicio, trunc (max (fecha)) fin
                        from   hor_Examenes e,
                               hor_examenes_asignaturas a
                        where  convocatoria_id = p_convocatoria
                        and    e.id = examen_id
                        and    estudio_id = p_estudio
                        and    fecha is not null
                        and    (   p_Curso = -1
                                or p_Curso = curso_id)) x,
                       hor_ext_calendario c
                where  c.fecha between inicio and fin
                and    tipo_dia <> 'F')
      group by año,
               semana
      order by 1,
               2;

   cursor lista_items (
      p_estudio        in   number,
      p_convocatoria   in   number,
      p_curso          in   number,
      p_año            in   number,
      p_semana         in   number
   ) is
      select e.id, e.nombre,
             to_date (to_char (e.fecha, 'dd/mm/yyyy') || ' ' || to_char (e.hora_inicio, 'hh24:mi'),
                      'dd/mm/yyyy hh24:mi') inicio,
             to_date (to_char (e.fecha, 'dd/mm/yyyy') || ' ' || to_char (e.hora_fin, 'hh24:mi'),
                      'dd/mm/yyyy hh24:mi') fin,
             estudio_id, semestre, tipo_asignatura, curso_id
      from   hor_Examenes e,
             hor_examenes_asignaturas a
      where  convocatoria_id = p_convocatoria
      and    e.id = examen_id
      and    estudio_id = p_estudio
      and    fecha is not null
      and    to_number (to_char (fecha, 'yyyy')) = p_año
      and    to_number (to_char (fecha, 'iw')) = p_semana
      and    (   p_Curso = -1
              or p_Curso = curso_id);

   cursor lista_items_asignaturas (
      p_estudio        in   number,
      p_convocatoria   in   number,
      p_curso          in   number,
      p_año            in   number,
      p_semana         in   number
   ) is
      select distinct e.nombre asignatura_id, (select asignatura
                                               from   hor_items_asignaturas
                                               where  asignatura_id = a.asignatura_id
                                               and    rownum <= 1) nombre
      from            hor_Examenes e,
                      hor_examenes_asignaturas a
      where           convocatoria_id = p_convocatoria
      and             e.id = examen_id
      and             estudio_id = p_estudio
      and             fecha is not null
      and             to_number (to_char (fecha, 'yyyy')) = p_año
      and             to_number (to_char (fecha, 'iw')) = p_semana
      and             (   p_Curso = -1
                       or p_Curso = curso_id)
      order by        1;


   procedure cabecera is
      v_texto   varchar2 (100);
   begin
      if pCurso = -1 then
         v_texto := '';
      else
         v_texto := ' - Curs: ' || pCurso;
      end if;

      owa_util.mime_header (ccontent_type => 'text/xml', bclose_header => true);
      fop.documentOpen (page_width => 29.7, page_height => 21, margin_left => 1.5, margin_right => 1.5,
                        margin_top => 0.5, margin_bottom => 0.5, extent_before => 3, extent_after => 1.5,
                        extent_margin_top => 3, extent_margin_bottom => 1.5);
      fop.pageBodyOpen;
      htp.p (fof.documentheaderopen || fof.blockopen (space_after => 0.1) || fof.tableOpen
             || fof.tablecolumndefinition (column_width => 3) || fof.tablecolumndefinition (column_width => 24)
             || fof.tablebodyopen || fof.tableRowOpen || fof.tablecellopen
             || fof.block (fof.logo_uji, text_align => 'center') || fof.tablecellclose
             || fof.tablecellopen (display_align => 'center', padding => 0.4)
             || fof.block (text => v_nombre, font_size => 16, font_weight => 'bold', font_family => 'Arial',
                           text_align => 'center')
             || fof.block (text => 'Horari d''exàmens - Curs acadèmic: ' || vCurso || '/'
                            || substr (to_char (vCurso + 1), 3) || ' - ' || v_convocatoria || v_texto,
                           font_size => 16, font_weight => 'bold', font_family => 'Arial', text_align => 'center')
             || fof.tablecellclose || fof.tableRowClose || fof.tablebodyclose || fof.tableClose || fof.blockclose
             || fof.documentheaderclose);
      -- El numero paginas se escribe a mano para poner el texto m?s peque?o
      htp.p (fof.documentfooteropen || fof.tableOpen || fof.tablecolumndefinition (column_width => 25)
             || fof.tablebodyopen || fof.tableRowOpen || fof.tablecellopen
             || fof.block (text => 'Pag. <fo:page-number /> de <fo:page-number-citation ref-id="lastBlock" />',
                           text_align => 'right', font_weight => 'bold', font_size => 8)
             || fof.tablecellclose || fof.tableRowClose || fof.tablebodyclose || fof.tableClose
             || fof.documentfooterclose);
      fop.pageFlowOpen;
      fop.block (fof.white_space);
   end;

   procedure pie is
   begin
      htp.p ('<fo:block  id="lastBlock" space-before="1cm" font-size="8pt">');
      htp.p ('Data de generació: ' || to_char (sysdate, 'dd/mm/yyy hh24:mi'));
      fop.blockclose ();
      fop.pageFlowClose;
      fop.pageBodyClose;
      fop.documentClose;
   end;

   function espacios (p_espacios in number)
      return varchar2 is
      v_rdo   varchar2 (2000);
   begin
      v_rdo := null;

      for x in 1 .. p_espacios loop
         v_rdo := v_rdo || fof.white_space;
      end loop;

      return v_rdo;
   end;

   procedure mostrar_calendario (
      p_estudio        in   number,
      p_convocatoria   in   number,
      p_curso          in   number,
      p_año            in   number,
      p_semana         in   number
   ) is
      v_aux   number;
   begin
      vFechas := t_horario ();

      for item in lista_items (p_estudio, p_convocatoria, p_curso, p_año, p_semana) loop
         vFechas.extend;
         if item.tipo_asignatura = 'Anual' then
         vFechas (vFechas.count) :=
            ItemHorarios (hor_contenido_item (item.inicio, item.fin, item.nombre || ' - Curs '||item.curso_id,
                                              '(' || item.tipo_asignatura || ')'),
                          redondea_fecha_a_cuartos(item.inicio), redondea_fecha_a_cuartos(item.fin), null);
         else
         vFechas (vFechas.count) :=
            ItemHorarios (hor_contenido_item (item.inicio, item.fin, item.nombre || ' - Curs '||item.curso_id|| ' - Semestre '|| item.semestre,
                                              '(' || item.tipo_asignatura || ')'),
                          redondea_fecha_a_cuartos(item.inicio), redondea_fecha_a_cuartos(item.fin), null);

            --ItemHorarios (hor_contenido_item (item.inicio, item.fin, item.nombre || ' ' || item.semestre,
            --                                  '(' || item.tipo_asignatura || ') - Curs ' || item.curso_id),
            --              item.inicio, item.fin, null);
end if;
      end loop;

      if vFechas.count > 0 then
         vRdo := xpfpkg_horarios_academicos.drawTimetable (vFechas, 5, null, 'CA', 'PDF_APP_HORARIOS_DET');
         xup_utils.dump (vRdo);
         dbms_lob.freeTemporary (vRdo);
         --fop.block (fof.white_space);
         fop.block (fof.white_space);
      end if;
   end;

   procedure mostrar_asignaturas (
      p_estudio        in   number,
      p_convocatoria   in   number,
      p_curso          in   number,
      p_año            in   number,
      p_semana         in   number
   ) is
      v_aux   number;
   begin
      for item in lista_items_asignaturas (p_estudio, p_convocatoria, p_curso, p_año, p_semana) loop
         fop.block (item.asignatura_id || ' - ' || item.nombre, font_size => 10, font_family => 'Arial');
      end loop;
   end;
BEGIN
   select count (*) + 1
   into   vSesion
   from   apa_sesiones
   where  sesion_id = pSesion
   and    fecha < sysdate () - 0.33;

   select id
   into   vCurso
   from   hor_curso_academico;

   if vSesion > 0 then
      begin
         select estudio
         into   v_nombre
         from   hor_items_asignaturas
         where  estudio_id = pEstudio
         and    rownum <= 1;

         select convocatoria_nombre
         into   v_convocatoria
         from   hor_examenes
         where  convocatoria_id = pConvocatoria
         and    rownum <= 1;

         cabecera;

         begin
            vCuantos := 0;

            for x in lista_semanas (pEstudio, pConvocatoria, pCurso) loop
               vCuantos := vCuantos + 1;

               if vCuantos > 1 then
                  htp.p ('<fo:block break-before="page" color="white">_</fo:block>');
               end if;

               fop.block ('Setmana del ' || to_char (Next_Day(Trunc(x.lunes) - 7,'LUN'), 'dd/mm/yyyy') || ' al '
                          || to_char (x.domingo, 'dd/mm/yyyy') || ' ___________________________',
                          font_size => 12, space_after => 0.5, space_before => 0.5);
               mostrar_calendario (pEstudio, pConvocatoria, pCurso, x.año, x.semana);
               mostrar_asignaturas (pEstudio, pConvocatoria, pCurso, x.año, x.semana);
            end loop;

            null;
         exception
            when others then
               fop.block (sqlerrm);
         end;

         pie;
      end loop;
   else
      cabecera;
      fop.block ('No tens permís per accedir a aquest document.', font_weight => 'bold', text_align => 'center',
                 font_size => 14, space_after => 0.5);
      pie;
   end if;
END;

CREATE OR REPLACE PROCEDURE UJI_HORARIOS.HORARIOS_MENSUAL_PDF (
   name_array    IN   euji_util.ident_arr2,
   value_array   IN   euji_util.ident_arr2
) IS
   -- Parametros
   pEstudio    CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pEstudio');
   pMes        CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pMes');
   pSemestre   CONSTANT VARCHAR2 (40)   := '1';
   pSesion     CONSTANT VARCHAR2 (4000) := euji_util.euji_getparam (name_array, value_array, 'pSesion');
   pGrupo      CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pGrupo');
   pCurso      CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pCurso');
   pSemana     CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pSemana');
   vCursoAcaNum         number;
   vSesion              number;
   vFechaIniSemestre    date;
   vFechaFinSemestre    date;
   vEsGenerica          boolean         := pSemana = 'G';

     -- Variables
   --  vPerId              per_personas.id%TYPE := euji_get_perid ();
   CURSOR c_items_det (vIniDate date, vFinDate date) IS
      SELECT   aulas.codigo as codigo_aula, asignatura_id, asignatura, estudio, grupo_id, tipo_subgrupo,
               tipo_subgrupo_id, subgrupo_id, dia_semana_id, hora_inicio, hora_fin, inicio, fin
      FROM     UJI_HORARIOS.HOR_ITEMS items LEFT OUTER JOIN hor_aulas aulas ON (aulas.id = items.aula_planificacion_id)
               INNER JOIN uji_horarios.hor_items_asignaturas asi ON (items.id = asi.item_id)
               INNER JOIN UJI_HORARIOS.HOR_ITEMS_DETALLE det ON (det.item_id = items.id)
      WHERE    asi.estudio_id = pEstudio
      AND      asi.curso_id = pCurso
      AND      items.grupo_id = pGrupo
      AND      items.semestre_id = pSemestre
      AND      items.dia_semana_id IS NOT NULL
      and      det.inicio > vIniDate
      and      det.fin < vFinDate
      order by det.inicio;

   CURSOR c_asigs is
      SELECT distinct (asignatura_id), asignatura
      FROM            UJI_HORARIOS.HOR_ITEMS items,
                      uji_horarios.hor_items_asignaturas asi
      WHERE           items.id = asi.item_id
      and             asi.estudio_id = pEstudio
      AND             asi.curso_id = pCurso
      AND             items.grupo_id = pGrupo
      AND             items.semestre_id = pSemestre
      AND             items.dia_semana_id IS NOT NULL
      order by        asignatura_id;

   CURSOR c_clases_asig (vAsignaturaId varchar2) is
      SELECT   tipo_subgrupo_id || subgrupo_id subgrupo,
               decode (dia_semana_id, 1, 'L', 2, 'M', 3, 'X', 4, 'J', 5, 'V', '?') dia_sem,
               to_char (hora_fin, 'hh24:mi') hora_fin_str, to_char (hora_inicio, 'hh24:mi') hora_inicio_str
      FROM     UJI_HORARIOS.HOR_ITEMS items,
               uji_horarios.hor_items_asignaturas asi
      WHERE    items.id = asi.item_id
      and      asi.estudio_id = pEstudio
      AND      asi.curso_id = pCurso
      AND      items.grupo_id = pGrupo
      AND      items.semestre_id = pSemestre
      and      asi.asignatura_id = vAsignaturaId
      AND      items.dia_semana_id IS NOT NULL
      order by dia_semana_id,
               hora_inicio,
               subgrupo;

   vFechas              t_horario;
   vRdo                 clob;

   procedure cabecera is
   begin
      owa_util.mime_header (ccontent_type => 'text/xml', bclose_header => true);
      fop.documentOpen (page_width => 28.5*5, page_height => 18, margin_left => 0.5, margin_right => 0.5,
                        margin_top => 0.5, margin_bottom => 0, extent_before => 0, extent_after => 0.5,
                        extent_margin_top => 0, extent_margin_bottom => 0.5);
      fop.pageBodyOpen;
      fop.documentfooteropen;
      fop.block ('Fecha documento: ' || to_char (sysdate, 'yyyy/mm/dd HH24:MI'), font_size => 6,
                 font_style => 'italic');
      fop.documentfooterclose;
      fop.pageFlowOpen;
   end;

   procedure calcula_curso_aca is
   begin
      SELECT   curso_academico_id
      into     vCursoAcaNum
      FROM     hor_semestres_detalle d,
               hor_estudios e
      WHERE    e.id = pEstudio
      AND      e.tipo_id = d.tipo_estudio_id
      AND      ROWNUM = 1
      ORDER BY fecha_inicio;
   end;

   procedure info_calendario_gen is
      vCursoAca           varchar2 (4000);
      vTitulacionNombre   varchar2 (4000);
      vCursoNombre        varchar2 (4000) := pCurso || 'º';
      vTextoCabecera      varchar2 (4000);
      vTextoGrupo         varchar2 (4000);
   begin
      select nombre
      into   vTitulacionNombre
      from   hor_estudios
      where  id = pEstudio;

      vCursoAca := to_char (vCursoAcaNum) || '/' || to_char (vCursoAcaNum + 1);

      if pCurso in ('1', '3') then
         vCursoNombre := pCurso || 'er';
      elsif pCurso = '2' then
         vCursoNombre := '2on';
      elsif pCurso = '4' then
         vCursoNombre := '4t';
      else
         vCursoNombre := pCurso || 'è';
      end if;

      if pGrupo like '%,%' then
         vTextoGrupo := 'Grups';
      else
         vTextoGrupo := 'Grup';
      end if;

      vTextoCabecera :=
         vTitulacionNombre || ' - ' || vCursoNombre || ' curs - ' || vTextoGrupo || ' ' || pGrupo || ' (semestre '
         || pSemestre || ') - ' || vCursoAca;
      fop.block (text => vTextoCabecera, border_bottom => 'solid', border_width => 0.05);
   end;

   procedure muestra_leyenda_asig (item c_asigs%ROWTYPE) is
      vClasesText   varchar2 (4000);
   begin
      fop.block (item.asignatura_id || ', ' || item.asignatura || ':', font_size => 7, padding_top => 0.1);
      vClasesText := '<fo:inline color="white">___</fo:inline>';

      for c in c_clases_asig (item.asignatura_id) loop
         vClasesText :=
            vClasesText || c.dia_sem || ', ' || c.hora_inicio_str || '-' || c.hora_fin_str || ' (' || c.subgrupo
            || '); ';
      end loop;

      --vClasesText := substr (vClasesText, 1, length (vClasesText) - 2);
      fop.block (vClasesText, font_size => 7, padding_top => 0.06);
   end;

   procedure leyenda_asigs is
   begin
      fop.block (' ', padding_top => 1);

      for item in c_asigs loop
         muestra_leyenda_asig (item);
      end loop;
   end;

   procedure pie is
   begin
      fop.pageFlowClose;
      fop.pageBodyClose;
      fop.documentClose;
   end;

   procedure calendario_det (vFechaIni date, vFechaFin date) is
   begin
      vFechas := t_horario ();

      for item in c_items_det (vFechaIni, vFechaFin) loop
         vFechas.extend;
         vFechas (vFechas.count) :=
            ItemHorarios (hor_contenido_item (item.hora_inicio, item.hora_fin, item.asignatura_id,
                                              '(' || item.tipo_subgrupo_id || item.subgrupo_id || ') '
                                              || item.codigo_aula),
                          redondea_fecha_a_cuartos(item.inicio), redondea_fecha_a_cuartos(item.fin), null);
      end loop;

      if vFechas.count > 0 then
         vRdo := XPFDM.xpfpkg_horarios_academicos.drawTimetable (vFechas, 7, null, 'CA', 'PDF_APP_HORARIOS_DET');
         xup_utils.dump (vRdo);
         dbms_lob.freeTemporary (vRdo);
      end if;
   end;

   function semana_tiene_clase (vIniDate date, vFinDate date)
      return boolean is
      num_clases   number;
   begin
      SELECT   count (*)
      into     num_clases
      FROM     UJI_HORARIOS.HOR_ITEMS items,
               UJI_HORARIOS.HOR_ITEMS_DETALLE det,
               uji_horarios.hor_items_asignaturas asi
      WHERE    items.id = asi.item_id
      and      asi.estudio_id = pEstudio
      AND      asi.curso_id = pCurso
      AND      items.grupo_id = pGrupo
      AND      items.semestre_id = pSemestre
      AND      items.dia_semana_id IS NOT NULL
      and      det.item_id = items.id
      and      det.inicio > vIniDate
      and      det.fin < vFinDate
      order by det.inicio;

      return num_clases > 0;
   end;

   procedure info_calendario_det (vFechaIni date, vFechaFin date) is
      vCursoAca           varchar2 (4000);
      vTitulacionNombre   varchar2 (4000);
      vCursoNombre        varchar2 (4000) := pCurso || 'º';
      vTextoCabecera      varchar2 (4000);
      vFechaIniStr        varchar2 (4000) := to_char (vFechaIni, 'dd/mm/yyyy');
      vFechaFinStr        varchar2 (4000) := to_char (vFechaFin - 1, 'dd/mm/yyyy');
   begin
      select nombre
      into   vTitulacionNombre
      from   hor_estudios
      where  id = pEstudio;

      vCursoAca := to_char (vCursoAcaNum) || '/' || to_char (vCursoAcaNum + 1);

      if pCurso in ('1', '3') then
         vCursoNombre := pCurso || 'er';
      elsif pCurso = '2' then
         vCursoNombre := '2on';
      elsif pCurso = '4' then
         vCursoNombre := '4t';
      else
         vCursoNombre := pCurso || 'è';
      end if;

      vTextoCabecera :=
         vTitulacionNombre || ' - ' || vCursoNombre || ' curs - Grup ' || pGrupo || ' (semestre ' || pSemestre
         || ') - Del ' || vFechaIniStr || ' al ' || vFechaFinStr;
      fop.block (text => vTextoCabecera, border_bottom => 'solid', border_width => 0.05);
   end;

   procedure paginas_calendario_det(mes number) is
      vFechaIni  date;
      vFechaFin  date;
   begin
      vFechaIni := to_Date('01/' || mes || '/2014');
      vFechaFin := vFechaIni + 7;

      -- Abrir tabla
      fop.tableOpen(border_style=> 'none', padding=> 0);

      for i in 1..5 loop
         fop.tableColumnDefinition(column_width=> 28);
      end loop;

      fop.tableBodyOpen;
      fop.tableRowOpen;

      for i in 1..5 loop
          fop.tableCellOpen(border=> 'none', display_align => 'top');

          info_calendario_det (vFechaIni, vFechaFin);
          calendario_det (vFechaIni, vFechaFin);

          vFechaIni := vFechaIni + 7;

          vFechaFin := vFechaFin + 7;

          if (vFechaFin > LAST_DAY( vFechaIni )) then
            vFechaFin := LAST_DAY( vFechaIni ) + 1;
          end if;

          fop.tableCellClose;
      end loop;

      fop.tableRowClose;

      fop.tableBodyClose;
      fop.tableClose;


   end;
BEGIN
   --euji_control_acceso (vItem);
   select count (*)
   into   vSesion
   from   apa_sesiones
   where  sesion_id = pSesion
   and    fecha < sysdate () - 0.33;

   if vSesion > 0 then
      calcula_curso_aca;
      cabecera;
      --fop.block ('HOLA');

      paginas_calendario_det(pMes);

      pie;
   else
      cabecera;
      fop.block ('No tens permís per accedir a aquest document.', font_weight => 'bold', text_align => 'center',
                 font_size => 14, space_after => 0.5);
      pie;
   end if;
EXCEPTION
   WHEN OTHERS THEN
      htp.p (sqlerrm);
END;

CREATE OR REPLACE PROCEDURE UJI_HORARIOS.horarios_ocupacion_pdf (
   name_array    IN   euji_util.ident_arr2,
   value_array   IN   euji_util.ident_arr2
) IS
   pAula       CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pAula');
   pSemestre   CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pSemestre');
   pSesion     CONSTANT VARCHAR2 (4000) := euji_util.euji_getparam (name_array, value_array, 'pSesion');
   pSemana     CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pSemana');
   vCursoAcaNum         number;
   vFechaIniSemestre    date;
   vFechaFinSemestre    date;
   vSesion              number;
   vEsGenerica          boolean         := pSemana = 'G';
   vFechas              t_horario;
   vRdo                 clob;

   CURSOR c_items_det_ant (vIniDate date, vFinDate date) IS
      SELECT   items.id, items.grupo_id, items.tipo_subgrupo, items.tipo_subgrupo_id, items.subgrupo_id,
               items.dia_semana_id, items.hora_inicio, items.hora_fin, det.inicio, det.fin,
               rtrim (xmlagg (xmlelement (e, asi.asignatura_id || ', ')).extract ('//text()'), ', ') as asignatura_id
      FROM     UJI_HORARIOS.HOR_ITEMS items,
               UJI_HORARIOS.HOR_ITEMS_DETALLE det,
               uji_horarios.hor_items_asignaturas asi,
               hor_aulas aula,
               hor_aulas_planificacion p
      WHERE    items.id = asi.item_id
      AND      items.aula_planificacion_id = aula.id
      AND      aula.id = pAula
      AND      p.aula_id = aula.id
      AND      items.semestre_id = pSemestre
      AND      items.dia_semana_id IS NOT NULL
      and      det.item_id = items.id
      and      det.inicio > vIniDate
      and      det.fin < vFinDate
      group by items.id,
               items.grupo_id,
               items.tipo_subgrupo,
               items.tipo_subgrupo_id,
               items.subgrupo_id,
               items.dia_semana_id,
               items.semestre_id,
               items.hora_inicio,
               items.hora_fin,
               det.inicio,
               det.fin,
               asi.asignatura_id
      order by det.inicio;

   CURSOR c_items_det (vIniDate date, vFinDate date) IS
      SELECT   items.id, items.grupo_id, items.tipo_subgrupo, items.tipo_subgrupo_id, items.subgrupo_id,
               items.dia_semana_id, items.hora_inicio, items.hora_fin, det.inicio, det.fin,
               nvl (comun_texto, asi.asignatura_id) asignatura_id
      FROM     UJI_HORARIOS.HOR_ITEMS items,
               UJI_HORARIOS.HOR_ITEMS_DETALLE det,
               uji_horarios.hor_items_asignaturas asi,
               hor_aulas aula,
               hor_aulas_planificacion p
      WHERE    items.id = asi.item_id
      AND      items.aula_planificacion_id = aula.id
      AND      aula.id = pAula
      AND      p.aula_id = aula.id
      AND      items.semestre_id = pSemestre
      AND      items.dia_semana_id IS NOT NULL
      and      det.item_id = items.id
      and      det.inicio > vIniDate
      and      det.fin < vFinDate
      group by items.id,
               items.grupo_id,
               items.tipo_subgrupo,
               items.tipo_subgrupo_id,
               items.subgrupo_id,
               items.dia_semana_id,
               items.semestre_id,
               items.hora_inicio,
               items.hora_fin,
               det.inicio,
               det.fin,
               nvl (comun_texto, asi.asignatura_id)
      order by det.inicio;

   CURSOR c_items_gen_ant IS
      SELECT   items.id, items.grupo_id, items.tipo_subgrupo, items.tipo_subgrupo_id, items.subgrupo_id,
               items.dia_semana_id, items.hora_inicio, items.hora_fin,
               rtrim (xmlagg (xmlelement (e, asi.asignatura_id || ', ')).extract ('//text()'), ', ') as asignatura_id,
               to_date (dia_semana_id + 1 || '/01/2006 ' || to_char (hora_fin, 'hh24:mi'), 'dd/mm/yyyy hh24:mi') fin,
               to_date (dia_semana_id + 1 || '/01/2006 ' || to_char (hora_inicio, 'hh24:mi'),
                        'dd/mm/yyyy hh24:mi') inicio
      FROM     UJI_HORARIOS.HOR_ITEMS items,
               uji_horarios.hor_items_asignaturas asi,
               hor_aulas aula,
               hor_aulas_planificacion p
      WHERE    items.id = asi.item_id
      AND      items.aula_planificacion_id = aula.id
      AND      aula.id = p.aula_id
      AND      aula.id = pAula
      AND      items.semestre_id = pSemestre
      AND      items.dia_semana_id IS NOT NULL
      group by items.id,
               items.grupo_id,
               items.tipo_subgrupo,
               items.tipo_subgrupo_id,
               items.subgrupo_id,
               items.dia_semana_id,
               items.semestre_id,
               items.hora_inicio,
               items.hora_fin,
               asi.asignatura_id
      order by items.hora_inicio;

   CURSOR c_items_gen IS
      SELECT   items.id, items.grupo_id, items.tipo_subgrupo, items.tipo_subgrupo_id, items.subgrupo_id,
               items.dia_semana_id, items.hora_inicio, items.hora_fin,
               nvl (comun_texto, asi.asignatura_id) asignatura_id,
               to_date (dia_semana_id + 1 || '/01/2006 ' || to_char (hora_fin, 'hh24:mi'), 'dd/mm/yyyy hh24:mi') fin,
               to_date (dia_semana_id + 1 || '/01/2006 ' || to_char (hora_inicio, 'hh24:mi'),
                        'dd/mm/yyyy hh24:mi') inicio
      FROM     UJI_HORARIOS.HOR_ITEMS items,
               uji_horarios.hor_items_asignaturas asi,
               hor_aulas aula,
               hor_aulas_planificacion p
      WHERE    items.id = asi.item_id
      AND      items.aula_planificacion_id = aula.id
      AND      aula.id = p.aula_id
      AND      aula.id = pAula
      AND      items.semestre_id = pSemestre
      AND      items.dia_semana_id IS NOT NULL
      group by items.id,
               items.grupo_id,
               items.tipo_subgrupo,
               items.tipo_subgrupo_id,
               items.subgrupo_id,
               items.dia_semana_id,
               items.semestre_id,
               items.hora_inicio,
               items.hora_fin,
               nvl (comun_texto, asi.asignatura_id)
      order by items.hora_inicio;

   CURSOR c_asigs is
      SELECT distinct (asignatura_id), asignatura
      FROM            UJI_HORARIOS.HOR_ITEMS items,
                      uji_horarios.hor_items_asignaturas asi,
                      hor_aulas aula,
                      hor_aulas_planificacion p
      WHERE           items.id = asi.item_id
      AND             items.aula_planificacion_id = aula.id
      AND             p.aula_id = aula.id
      AND             aula.id = pAula
      AND             p.semestre_id = pSemestre
      AND             items.dia_semana_id IS NOT NULL
      order by        asignatura_id;

   CURSOR c_clases_asig (vAsignaturaId varchar2) is
      SELECT   tipo_subgrupo_id || subgrupo_id subgrupo,
               decode (dia_semana_id, 1, 'L', 2, 'M', 3, 'X', 4, 'J', 5, 'V', '?') dia_sem,
               to_char (hora_fin, 'hh24:mi') hora_fin_str, to_char (hora_inicio, 'hh24:mi') hora_inicio_str
      FROM     UJI_HORARIOS.HOR_ITEMS items,
               uji_horarios.hor_items_asignaturas asi,
               hor_aulas aula,
               hor_aulas_planificacion p
      WHERE    items.id = asi.item_id
      AND      items.aula_planificacion_id = aula.id
      AND      p.aula_id = aula.id
      AND      aula.id = pAula
      AND      items.semestre_id = pSemestre
      and      asi.asignatura_id = vAsignaturaId
      AND      items.dia_semana_id IS NOT NULL
      order by dia_semana_id,
               hora_inicio,
               subgrupo;

   function redondea_fecha_a_cuartos (vIniDate date)
      return date is
      fecha_redondeada   date;
   begin
      select trunc (vIniDate, 'HH') + (15 * round (to_char (trunc (vIniDate, 'MI'), 'MI') / 15)) / 1440
      into   fecha_redondeada
      from   dual;

      return fecha_redondeada;
   end;

   function semana_tiene_clase (vIniDate date, vFinDate date)
      return boolean is
      num_clases   number;
   begin
      SELECT count (*)
      into   num_clases
      FROM   UJI_HORARIOS.HOR_ITEMS items,
             UJI_HORARIOS.HOR_ITEMS_DETALLE det,
             hor_aulas aula,
             hor_aulas_planificacion p
      WHERE  items.aula_planificacion_id = aula.id
      AND    p.aula_id = aula.id
      AND    aula.id = pAula
      AND    p.semestre_id = pSemestre
      AND    items.dia_semana_id IS NOT NULL
      and    det.item_id = items.id
      and    det.inicio > vIniDate
      and    det.fin < vFinDate;

      return num_clases > 0;
   end;

   procedure calcula_curso_aca is
   begin
      SELECT distinct (curso_academico_id)
      into            vCursoAcaNum
      FROM            hor_semestres_detalle d,
                      hor_estudios e
      WHERE           ROWNUM = 1
      ORDER BY        curso_academico_id;
   end;

   procedure calcula_fechas_semestre is
   begin
      SELECT fecha_inicio, fecha_examenes_fin
      into   vFechaIniSemestre, vFechaFinSemestre
      FROM   hor_semestres_detalle,
             hor_estudios e
      WHERE  semestre_id = pSemestre
      and    rownum = 1;
   end;

   procedure calendario_det (vFechaIni date, vFechaFin date) is
   begin
      vFechas := t_horario ();

      for item in c_items_det (vFechaIni, vFechaFin) loop
         vFechas.extend;
         vFechas (vFechas.count) :=
            ItemHorarios (hor_contenido_item (redondea_fecha_a_cuartos (item.hora_inicio),
                                              redondea_fecha_a_cuartos (item.hora_fin),
                                              item.asignatura_id || ' ' || item.grupo_id,
                                              '(' || item.tipo_subgrupo_id || item.subgrupo_id || ')'),
                          redondea_fecha_a_cuartos (item.inicio), redondea_fecha_a_cuartos (item.fin), null);
      end loop;

      if vFechas.count > 0 then
         vRdo := xpfpkg_horarios_academicos.drawTimetable (vFechas, 5, null, 'CA', 'PDF_APP_HORARIOS_DET');
         xup_utils.dump (vRdo);
         dbms_lob.freeTemporary (vRdo);
      end if;
   end;

   procedure calendario_gen is
   begin
      vFechas := t_horario ();

      for item in c_items_gen loop
         vFechas.extend;
         vFechas (vFechas.count) :=
            ItemHorarios (hor_contenido_item (redondea_fecha_a_cuartos (item.hora_inicio),
                                              redondea_fecha_a_cuartos (item.hora_fin),
                                              item.asignatura_id || ' ' || item.grupo_id,
                                              '(' || item.tipo_subgrupo_id || item.subgrupo_id || ')'),
                          redondea_fecha_a_cuartos (item.inicio), redondea_fecha_a_cuartos (item.fin), null);
      end loop;

      if vFechas.count > 0 then
         vRdo := xpfpkg_horarios_academicos.drawTimetable (vFechas, 5, null, 'CA', 'PDF_APP_HORARIOS');
         xup_utils.dump (vRdo);
         dbms_lob.freeTemporary (vRdo);
      end if;
   end;

   procedure info_calendario_det (vFechaIni date, vFechaFin date) is
      vCursoAca        varchar2 (4000);
      vTextoCabecera   varchar2 (4000);
      vFechaIniStr     varchar2 (4000) := to_char (vFechaIni, 'dd/mm/yyyy');
      vFechaFinStr     varchar2 (4000) := to_char (vFechaFin - 1, 'dd/mm/yyyy');
      vAulaNombre      varchar2 (4000);
      vEdificio        varchar2 (4000);
      vPlanta          varchar2 (4000);
      vCentroNombre    varchar2 (4000);
   begin
      vCursoAca := to_char (vCursoAcaNum) || '/' || to_char (vCursoAcaNum + 1);

      SELECT a.nombre AS aulaNombre, a.edificio, a.planta, c.nombre AS centroNombre
      INTO   vAulaNombre, vEdificio, vPlanta, vCentroNombre
      FROM   hor_aulas a,
             hor_centros c
      WHERE  a.id = pAula
      AND    c.id = a.centro_id;

      vTextoCabecera :=
         vAulaNombre || ' - ' || vCentroNombre || ' - Edifici ' || vEdificio || ' - Planta ' || vPlanta
         || '  - (semestre ' || pSemestre || ') - Del ' || vFechaIniStr || ' al ' || vFechaFinStr;
      fop.block (text => vTextoCabecera, border_bottom => 'solid', border_width => 0.05);
   end;

   procedure info_calendario_gen is
      vCursoAca        varchar2 (4000);
      vTextoCabecera   varchar2 (4000);
      vAulaNombre      varchar2 (4000);
      vEdificio        varchar2 (4000);
      vPlanta          varchar2 (4000);
      vCentroNombre    varchar2 (4000);
   begin
      vCursoAca := to_char (vCursoAcaNum) || '/' || to_char (vCursoAcaNum + 1);

      SELECT a.nombre AS aulaNombre, a.edificio, a.planta, c.nombre AS centroNombre
      INTO   vAulaNombre, vEdificio, vPlanta, vCentroNombre
      FROM   hor_aulas a,
             hor_centros c
      WHERE  a.id = pAula
      AND    c.id = a.centro_id;

      vTextoCabecera :=
         vAulaNombre || ' - ' || vCentroNombre || ' - Edifici ' || vEdificio || ' - Planta ' || vPlanta
         || '  - (semestre ' || pSemestre || ') ';
      fop.block (text => vTextoCabecera, border_bottom => 'solid', border_width => 0.05);
   end;

   procedure paginas_calendario_det is
      vFechaIniSemana   date;
      vFechaFinSemana   date;
      vDiaSemana        number;
   begin
      calcula_fechas_semestre;
      vDiaSemana := to_number (to_char (vFechaIniSemestre, 'd'));

      if vDiaSemana = 1 then
         vFechaIniSemana := vFechaIniSemestre;
      else
         vFechaIniSemana := vFechaIniSemestre - vDiaSemana + 1;
      end if;

      vFechaFinSemana := vFechaIniSemana + 7;

      while vFechaFinSemana <= vFechaFinSemestre loop
         if semana_tiene_clase (vFechaIniSemana, vFechaFinSemana) then
            info_calendario_det (vFechaIniSemana, vFechaFinSemana);
            calendario_det (vFechaIniSemana, vFechaFinSemana);
            htp.p ('<fo:block break-before="page" color="white">_</fo:block>');
         end if;

         vFechaIniSemana := vFechaFinSemana;
         vFechaFinSemana := vFechaFinSemana + 7;
      end loop;
   end;

   procedure cabecera is
   begin
      owa_util.mime_header (ccontent_type => 'text/xml', bclose_header => true);
      fop.documentOpen (page_width => 29.7, page_height => 21, margin_left => 0.5, margin_right => 0.5,
                        margin_top => 0.5, margin_bottom => 0, extent_before => 0, extent_after => 0.5,
                        extent_margin_top => 0, extent_margin_bottom => 0.5);
      fop.pageBodyOpen;
      fop.documentfooteropen;
      fop.block ('Fecha documento: ' || to_char (sysdate, 'yyyy/mm/dd HH24:MI'), font_size => 6,
                 font_style => 'italic');
      fop.documentfooterclose;
      fop.pageFlowOpen;
   end;

   procedure muestra_leyenda_asig (item c_asigs%ROWTYPE) is
      vClasesText   varchar2 (32000);
   begin
      fop.block (item.asignatura_id || ', ' || item.asignatura || ':', font_size => 7, padding_top => 0.1);
      vClasesText := '<fo:inline color="white">___</fo:inline>';

      for c in c_clases_asig (item.asignatura_id) loop
         vClasesText :=
            vClasesText || c.dia_sem || ', ' || c.hora_inicio_str || '-' || c.hora_fin_str || ' (' || c.subgrupo
            || '); ';
      end loop;

      --vClasesText := substr (vClasesText, 1, length (vClasesText));
      fop.block (vClasesText, font_size => 7, padding_top => 0.06);
   end;

   procedure leyenda_asigs is
   begin
      fop.block (' ', padding_top => 1);

      for item in c_asigs loop
         muestra_leyenda_asig (item);
      end loop;
   end;

   procedure pie is
   begin
      fop.pageFlowClose;
      fop.pageBodyClose;
      fop.documentClose;
   end;
BEGIN
   select count (*)
   into   vSesion
   from   apa_sesiones
   where  sesion_id = pSesion
   and    fecha < sysdate () - 0.33;

   if vSesion > 0 then
      calcula_curso_aca;
      cabecera;

      if vEsGenerica then
         info_calendario_gen;
         calendario_gen;
         leyenda_asigs;
      else
         paginas_calendario_det;
         leyenda_asigs;
      end if;

      pie;
   else
      cabecera;
      fop.block ('No tens permís per accedir a aquest document.', font_weight => 'bold', text_align => 'center',
                 font_size => 14, space_after => 0.5);
      pie;
   end if;
EXCEPTION
   WHEN OTHERS THEN
      htp.p (sqlerrm);
END;

CREATE OR REPLACE PROCEDURE UJI_HORARIOS.HORARIOS_SEMANA_PDF (

   name_array    IN   euji_util.ident_arr2,
   value_array   IN   euji_util.ident_arr2
) IS
   -- Parametros
   pEstudio    CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pEstudio');
   pSemestre   CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pSemestre');
   pSesion     CONSTANT VARCHAR2 (4000) := euji_util.euji_getparam (name_array, value_array, 'pSesion');
   pGrupo      CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pGrupo');
   pCurso      CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pCurso');
   pSemana     CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pSemana');
   pAgrupacion CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pAgrupacion');
   vCursoAcaNum         number;
   vSesion              number;
   vFechaIniSemestre    date;
   vFechaFinSemestre    date;
   vEsGenerica          boolean         := pSemana = 'G';

     -- Variables
   --  vPerId              per_personas.id%TYPE := euji_get_perid ();
   CURSOR c_items_gen (pSemestre in number) IS
      SELECT aulas.codigo as codigo_aula, asignatura_id, asignatura, estudio, asi.caracter, semestre_id, comun, grupo_id,
             tipo_subgrupo, tipo_subgrupo_id, subgrupo_id, dia_semana_id, hora_inicio, hora_fin, tipo_asignatura_id,
             tipo_asignatura,
             to_date (dia_semana_id + 1 || '/01/2006 ' || to_char (hora_fin, 'hh24:mi'), 'dd/mm/yyyy hh24:mi') fin,
             to_date (dia_semana_id + 1 || '/01/2006 ' || to_char (hora_inicio, 'hh24:mi'),
                      'dd/mm/yyyy hh24:mi') inicio
      FROM   UJI_HORARIOS.HOR_ITEMS items LEFT OUTER JOIN hor_aulas aulas ON (aulas.id = items.aula_planificacion_id)
             INNER JOIN uji_horarios.hor_items_asignaturas asi ON (items.id = asi.item_id)
      WHERE  asi.estudio_id = pEstudio
      AND    asi.curso_id = pCurso
      AND    ',' || pGrupo || ',' like '%,' || items.grupo_id || ',%'
      AND    items.semestre_id = pSemestre
      AND    items.dia_semana_id IS NOT NULL;

   CURSOR c_items_gen_agrupacion (pSemestre in number) IS
      SELECT DISTINCT aulas.codigo as codigo_aula, asi.asignatura_id, asignatura, estudio, asi.caracter, semestre_id, comun, grupo_id,
             tipo_subgrupo, tipo_subgrupo_id, subgrupo_id, dia_semana_id, hora_inicio, hora_fin, tipo_asignatura_id,
             tipo_asignatura,
             to_date (dia_semana_id + 1 || '/01/2006 ' || to_char (hora_fin, 'hh24:mi'), 'dd/mm/yyyy hh24:mi') fin,
             to_date (dia_semana_id + 1 || '/01/2006 ' || to_char (hora_inicio, 'hh24:mi'),
                      'dd/mm/yyyy hh24:mi') inicio
      FROM   UJI_HORARIOS.HOR_ITEMS items LEFT OUTER JOIN hor_aulas aulas ON (aulas.id = items.aula_planificacion_id)
             INNER JOIN uji_horarios.hor_items_asignaturas asi ON (items.id = asi.item_id) JOIN uji_horarios.HOR_V_AGRUPACIONES_ITEMS agr_item on (items.id = AGR_ITEM.ITEM_ID)
      WHERE  asi.estudio_id = pEstudio
      AND    agr_item.agrupacion_id = pAgrupacion
      AND    ',' || pGrupo || ',' like '%,' || items.grupo_id || ',%'
      AND    items.semestre_id = pSemestre
      AND    items.dia_semana_id IS NOT NULL;

   CURSOR c_items_det (vIniDate date, vFinDate date) IS
      SELECT   aulas.codigo as codigo_aula, asignatura_id, asignatura, estudio, grupo_id, tipo_subgrupo,
               tipo_subgrupo_id, subgrupo_id, dia_semana_id, hora_inicio, hora_fin, inicio, fin
      FROM     UJI_HORARIOS.HOR_ITEMS items LEFT OUTER JOIN hor_aulas aulas ON (aulas.id = items.aula_planificacion_id)
               INNER JOIN uji_horarios.hor_items_asignaturas asi ON (items.id = asi.item_id)
               INNER JOIN UJI_HORARIOS.HOR_ITEMS_DETALLE det ON (det.item_id = items.id)
      WHERE    asi.estudio_id = pEstudio
      AND      asi.curso_id = pCurso
      AND      ',' || pGrupo || ',' like '%,' || items.grupo_id || ',%'
      AND      items.semestre_id = pSemestre
      AND      items.dia_semana_id IS NOT NULL
      and      det.inicio > vIniDate
      and      det.fin < vFinDate
      order by det.inicio;

   CURSOR c_items_det_agrupacion (vIniDate date, vFinDate date) IS
      SELECT   DISTINCT aulas.codigo as codigo_aula, asi.asignatura_id, asignatura, estudio, grupo_id, tipo_subgrupo,
               tipo_subgrupo_id, subgrupo_id, dia_semana_id, hora_inicio, hora_fin, inicio, fin
      FROM     UJI_HORARIOS.HOR_ITEMS items LEFT OUTER JOIN hor_aulas aulas ON (aulas.id = items.aula_planificacion_id)
               INNER JOIN uji_horarios.hor_items_asignaturas asi ON (items.id = asi.item_id)
               INNER JOIN UJI_HORARIOS.HOR_ITEMS_DETALLE det ON (det.item_id = items.id)
               JOIN uji_horarios.HOR_V_AGRUPACIONES_ITEMS agr_item on (items.id = AGR_ITEM.ITEM_ID)
      WHERE    asi.estudio_id = pEstudio
      AND      agr_item.agrupacion_id = pAgrupacion
      AND      ',' || pGrupo || ',' like '%,' || items.grupo_id || ',%'
      AND      items.semestre_id = pSemestre
      AND      items.dia_semana_id IS NOT NULL
      and      det.inicio > vIniDate
      and      det.fin < vFinDate
      order by det.inicio;

   CURSOR c_asigs (pSemestre in number) is
      SELECT distinct (asignatura_id), asignatura
      FROM            UJI_HORARIOS.HOR_ITEMS items,
                      uji_horarios.hor_items_asignaturas asi
      WHERE           items.id = asi.item_id
      and             asi.estudio_id = pEstudio
      AND             asi.curso_id = pCurso
      AND             ',' || pGrupo || ',' like '%,' || items.grupo_id || ',%'
      AND             items.semestre_id = pSemestre
      AND             items.dia_semana_id IS NOT NULL
      order by        asignatura_id;

    CURSOR c_asigs_agrupacion (pSemestre in number) is
      SELECT distinct (asi.asignatura_id), asi.asignatura
      FROM            UJI_HORARIOS.HOR_ITEMS items,
                      uji_horarios.hor_items_asignaturas asi,
                      uji_horarios.HOR_V_AGRUPACIONES_ITEMS agr_item
      WHERE           items.id = asi.item_id
      and             items.id = agr_item.item_id
      and             asi.estudio_id = pEstudio
      and             agr_item.agrupacion_id = pAgrupacion
      AND             ',' || pGrupo || ',' like '%,' || items.grupo_id || ',%'
      AND             items.semestre_id = pSemestre
      AND             items.dia_semana_id IS NOT NULL
      order by        asignatura_id;

   CURSOR c_clases_asig (vAsignaturaId varchar2, vSemestre varchar2) is
      SELECT   tipo_subgrupo_id || subgrupo_id subgrupo,
               decode (dia_semana_id, 1, 'L', 2, 'M', 3, 'X', 4, 'J', 5, 'V', '?') dia_sem,
               to_char (hora_fin, 'hh24:mi') hora_fin_str, to_char (hora_inicio, 'hh24:mi') hora_inicio_str
      FROM     UJI_HORARIOS.HOR_ITEMS items,
               uji_horarios.hor_items_asignaturas asi
      WHERE    items.id = asi.item_id
      and      asi.estudio_id = pEstudio
      AND      asi.curso_id = pCurso
      AND      ',' || pGrupo || ',' like '%,' || items.grupo_id || ',%'
      AND      items.semestre_id = vSemestre
      and      asi.asignatura_id = vAsignaturaId
      AND      items.dia_semana_id IS NOT NULL
      order by dia_semana_id,
               hora_inicio,
               subgrupo;

   CURSOR c_clases_asig_agrupacion (vAsignaturaId varchar2, vSemestre varchar2) is
      SELECT   tipo_subgrupo_id || subgrupo_id subgrupo,
               decode (dia_semana_id, 1, 'L', 2, 'M', 3, 'X', 4, 'J', 5, 'V', '?') dia_sem,
               to_char (hora_fin, 'hh24:mi') hora_fin_str, to_char (hora_inicio, 'hh24:mi') hora_inicio_str
      FROM     UJI_HORARIOS.HOR_ITEMS items,
               uji_horarios.hor_items_asignaturas asi
      WHERE    items.id = asi.item_id
      and      asi.estudio_id = pEstudio
      AND      ',' || pGrupo || ',' like '%,' || items.grupo_id || ',%'
      AND      items.semestre_id = vSemestre
      and      asi.asignatura_id = vAsignaturaId
      AND      items.dia_semana_id IS NOT NULL
      and items.id in (SELECT item_id from HOR_V_AGRUPACIONES_ITEMS agr_item where agr_item.agrupacion_id = pAgrupacion)
      order by dia_semana_id,
               hora_inicio,
               subgrupo;

   vFechas              t_horario;
   vRdo                 clob;

   procedure cabecera is
   begin
      owa_util.mime_header (ccontent_type => 'text/xml', bclose_header => true);
      fop.documentOpen (page_width => 29.7, page_height => 21, margin_left => 0.5, margin_right => 0.5,
                        margin_top => 0.5, margin_bottom => 0, extent_before => 0, extent_after => 0.5,
                        extent_margin_top => 0, extent_margin_bottom => 0.5);
      fop.pageBodyOpen;
      fop.documentfooteropen;
      fop.block ('Fecha documento: ' || to_char (sysdate, 'yyyy/mm/dd HH24:MI'), font_size => 6,
                 font_style => 'italic');
      fop.documentfooterclose;
      fop.pageFlowOpen;
   end;

   procedure calcula_curso_aca is
   begin
      SELECT   curso_academico_id
      into     vCursoAcaNum
      FROM     hor_semestres_detalle d,
               hor_estudios e
      WHERE    e.id = pEstudio
      AND      e.tipo_id = d.tipo_estudio_id
      AND      ROWNUM = 1
      ORDER BY fecha_inicio;
   end;

   procedure info_calendario_gen (pSemestre in number) is
      vCursoAca           varchar2 (4000);
      vTitulacionNombre   varchar2 (4000);
      vCursoNombre        varchar2 (4000) := '';
      vTextoCabecera      varchar2 (4000);
      vTextoGrupo         varchar2 (4000);
      vNombreAgrupacion   varchar2 (4000) := '';
   begin
      select nombre
      into   vTitulacionNombre
      from   hor_estudios
      where  id = pEstudio;

      if (pAgrupacion is not null) then
         select 'Agrupació ' || nombre
         into vNombreAgrupacion
         from hor_agrupaciones
         where id = pAgrupacion;
      end if;

      vCursoAca := to_char (vCursoAcaNum) || '/' || to_char (vCursoAcaNum + 1);

      if pCurso in ('1', '3') then
         vCursoNombre := pCurso || 'er curs';
      elsif pCurso = '2' then
         vCursoNombre := '2on  curs';
      elsif pCurso = '4' then
         vCursoNombre := '4t curs';
      elsif pCurso is not null then
         vCursoNombre := pCurso || '?  curs';
      end if;

      if pGrupo like '%,%' then
         vTextoGrupo := 'Grups';
      else
         vTextoGrupo := 'Grup';
      end if;

      vTextoCabecera :=
         vTitulacionNombre || ' - ' || vCursoNombre || vNombreAgrupacion || ' - ' || vTextoGrupo || ' ' || pGrupo || ' (semestre '
         || pSemestre || ') - ' || vCursoAca;
      fop.block (text => vTextoCabecera, border_bottom => 'solid', border_width => 0.05);
   end;

   procedure muestra_leyenda_asig (item c_asigs%ROWTYPE, vSemestre varchar2) is
      vClasesText   varchar2 (4000);
   begin
      fop.block (item.asignatura_id || ', ' || item.asignatura || ':', font_size => 7, padding_top => 0.1);
      vClasesText := '<fo:inline color="white">___</fo:inline>';

      for c in c_clases_asig (item.asignatura_id, vSemestre) loop
         vClasesText :=
            vClasesText || c.dia_sem || ', ' || c.hora_inicio_str || '-' || c.hora_fin_str || ' (' || c.subgrupo
            || '); ';
      end loop;

      fop.block (vClasesText, font_size => 7, padding_top => 0.06);
   end;

   procedure leyenda_asigs (pSemestre in number) is
   begin
      fop.block (' ', padding_top => 1);

      for item in c_asigs (pSemestre) loop
         muestra_leyenda_asig (item, pSemestre);
      end loop;
   end;

   procedure muestra_leyenda_asig_agr (item c_asigs%ROWTYPE, vSemestre varchar2) is
      vClasesText   varchar2 (4000);
   begin
      fop.block (item.asignatura_id || ', ' || item.asignatura || ':', font_size => 7, padding_top => 0.1);
      vClasesText := '<fo:inline color="white">___</fo:inline>';

      for c in c_clases_asig_agrupacion (item.asignatura_id, vSemestre) loop
         vClasesText :=
           vClasesText || c.dia_sem || ', ' || c.hora_inicio_str || '-' || c.hora_fin_str || ' (' || c.subgrupo
             || '); ';
      end loop;

      fop.block (vClasesText, font_size => 7, padding_top => 0.06);
   end;

   procedure leyenda_asigs_agrupacion (pSemestre in number) is
   begin
      fop.block (' ', padding_top => 1);

      for item in c_asigs_agrupacion (pSemestre) loop
         muestra_leyenda_asig_agr (item, pSemestre);
      end loop;
   end;

   procedure pie is
   begin
      fop.pageFlowClose;
      fop.pageBodyClose;
      fop.documentClose;
   end;

   function redondea_fecha_a_cuartos (vIniDate date)
      return date is
      fecha_redondeada   date;
   begin
      select trunc(vIniDate,'HH')+(15*round(to_char( trunc(vIniDate,'MI'),'MI')/15))/1440
        into fecha_redondeada
        from dual;

      return fecha_redondeada;
   end;

   procedure calendario_gen (pSemestre in number) is
   begin
      vFechas := t_horario ();

      for item in c_items_gen (pSemestre) loop
         vFechas.extend;
         vFechas (vFechas.count) :=
            ItemHorarios (hor_contenido_item (redondea_fecha_a_cuartos(item.hora_inicio), redondea_fecha_a_cuartos(item.hora_fin), item.asignatura_id,
                                              '(' || item.tipo_subgrupo_id || item.subgrupo_id || ') '
                                              || item.codigo_aula),
                          redondea_fecha_a_cuartos(item.inicio), redondea_fecha_a_cuartos(item.fin), null);
      end loop;

      if vFechas.count > 0 then
         vRdo := xpfpkg_horarios_academicos.drawTimetable (vFechas, 5, null, 'CA', 'PDF_APP_HORARIOS');
         xup_utils.dump (vRdo);
         dbms_lob.freeTemporary (vRdo);
      end if;
   end;

   procedure calendario_gen_agrupacion (pSemestre in number) is
   begin
      vFechas := t_horario ();

      for item in c_items_gen_agrupacion (pSemestre) loop
         vFechas.extend;
         vFechas (vFechas.count) :=
            ItemHorarios (hor_contenido_item (redondea_fecha_a_cuartos(item.hora_inicio), redondea_fecha_a_cuartos(item.hora_fin), item.asignatura_id,
                                              '(' || item.tipo_subgrupo_id || item.subgrupo_id || ') '
                                              || item.codigo_aula),
                          redondea_fecha_a_cuartos(item.inicio), redondea_fecha_a_cuartos(item.fin), null);
      end loop;

      if vFechas.count > 0 then
         vRdo := xpfpkg_horarios_academicos.drawTimetable (vFechas, 5, null, 'CA', 'PDF_APP_HORARIOS');
         xup_utils.dump (vRdo);
         dbms_lob.freeTemporary (vRdo);
      end if;
   end;

   procedure calendario_det (vFechaIni date, vFechaFin date) is
   begin
      vFechas := t_horario ();

      for item in c_items_det (vFechaIni, vFechaFin) loop
         vFechas.extend;
         vFechas (vFechas.count) :=
            ItemHorarios (hor_contenido_item (redondea_fecha_a_cuartos(item.hora_inicio), redondea_fecha_a_cuartos(item.hora_fin), item.asignatura_id,
                                              '(' || item.tipo_subgrupo_id || item.subgrupo_id || ') '
                                              || item.codigo_aula),
                          redondea_fecha_a_cuartos(item.inicio), redondea_fecha_a_cuartos(item.fin), null);
      end loop;

      if vFechas.count > 0 then
         vRdo := xpfpkg_horarios_academicos.drawTimetable (vFechas, 5, null, 'CA', 'PDF_APP_HORARIOS_DET');
         xup_utils.dump (vRdo);
         dbms_lob.freeTemporary (vRdo);
      end if;
   end;

   procedure calendario_det_agrupacion (vFechaIni date, vFechaFin date) is
   begin
      vFechas := t_horario ();

      for item in c_items_det_agrupacion (vFechaIni, vFechaFin) loop
         vFechas.extend;
         vFechas (vFechas.count) :=
            ItemHorarios (hor_contenido_item (redondea_fecha_a_cuartos(item.hora_inicio), redondea_fecha_a_cuartos(item.hora_fin), item.asignatura_id,
                                              '(' || item.tipo_subgrupo_id || item.subgrupo_id || ') '
                                              || item.codigo_aula),
                          redondea_fecha_a_cuartos(item.inicio), redondea_fecha_a_cuartos(item.fin), null);
      end loop;

      if vFechas.count > 0 then
         vRdo := xpfpkg_horarios_academicos.drawTimetable (vFechas, 5, null, 'CA', 'PDF_APP_HORARIOS_DET');
         xup_utils.dump (vRdo);
         dbms_lob.freeTemporary (vRdo);
      end if;
   end;

   procedure calcula_fechas_semestre (pSemestre in number) is
   begin
      SELECT fecha_inicio, fecha_examenes_fin
      into   vFechaIniSemestre, vFechaFinSemestre
      FROM   hor_semestres_detalle,
             hor_estudios e
      WHERE  semestre_id = pSemestre
      and    e.id = pEstudio
      and    e.tipo_id = tipo_estudio_id;

      SELECT Next_Day(Trunc(vFechaIniSemestre) - 7,'LUN') into vFechaIniSemestre FROM dual;

   end;

   function semana_tiene_clase (vIniDate date, vFinDate date)
      return boolean is
      num_clases   number;
   begin
      SELECT   count (*)
      into     num_clases
      FROM     UJI_HORARIOS.HOR_ITEMS items,
               UJI_HORARIOS.HOR_ITEMS_DETALLE det,
               uji_horarios.hor_items_asignaturas asi
      WHERE    items.id = asi.item_id
      and      asi.estudio_id = pEstudio
      AND      asi.curso_id = pCurso
      AND      ',' || pGrupo || ',' like '%,' || items.grupo_id || ',%'
      AND      items.semestre_id = pSemestre
      AND      items.dia_semana_id IS NOT NULL
      and      det.item_id = items.id
      and      det.inicio > vIniDate
      and      det.fin < vFinDate
      order by det.inicio;

      return num_clases > 0;
   end;

      function semana_tiene_clase_agr (vIniDate date, vFinDate date)
      return boolean is
      num_clases   number;
   begin
      SELECT   count (*)
      into     num_clases
      FROM     UJI_HORARIOS.HOR_ITEMS items,
               UJI_HORARIOS.HOR_ITEMS_DETALLE det,
               uji_horarios.hor_items_asignaturas asi,
               HOR_V_AGRUPACIONES_ITEMS agr_items
      WHERE    items.id = asi.item_id
      and      asi.estudio_id = pEstudio
      AND      ',' || pGrupo || ',' like '%,' || items.grupo_id || ',%'
      AND      items.semestre_id = pSemestre
      AND      items.dia_semana_id IS NOT NULL
      and      det.item_id = items.id
      and      items.id = agr_items.item_id
      and      agr_items.agrupacion_id = pAgrupacion
      and      det.inicio > vIniDate
      and      det.fin < vFinDate
      order by det.inicio;

      return num_clases > 0;
   end;

   procedure info_calendario_det (vFechaIni date, vFechaFin date) is
      vCursoAca           varchar2 (4000);
      vTitulacionNombre   varchar2 (4000);
      vCursoNombre        varchar2 (4000) := '';
      vTextoCabecera      varchar2 (4000);
      vFechaIniStr        varchar2 (4000) := to_char (vFechaIni, 'dd/mm/yyyy');
      vFechaFinStr        varchar2 (4000) := to_char (vFechaFin - 1, 'dd/mm/yyyy');
      vNombreAgrupacion   varchar2 (4000) := '';
   begin
      select nombre
      into   vTitulacionNombre
      from   hor_estudios
      where  id = pEstudio;

      if (pAgrupacion is not null) then
         select 'Agrupació ' || nombre
         into vNombreAgrupacion
         from hor_agrupaciones
         where id = pAgrupacion;
      end if;

      vCursoAca := to_char (vCursoAcaNum) || '/' || to_char (vCursoAcaNum + 1);

      if pCurso in ('1', '3') then
         vCursoNombre := pCurso || 'er curs';
      elsif pCurso = '2' then
         vCursoNombre := '2on  curs';
      elsif pCurso = '4' then
         vCursoNombre := '4t curs';
      elsif pCurso is not null then
         vCursoNombre := pCurso || '?  curs';
      end if;

      vTextoCabecera :=
         vTitulacionNombre || ' - ' || vCursoNombre || vNombreAgrupacion || ' - Grup ' || pGrupo || ' (semestre ' || pSemestre
         || ') - Del ' || vFechaIniStr || ' al ' || vFechaFinStr;
      fop.block (text => vTextoCabecera, border_bottom => 'solid', border_width => 0.05);
   end;

   procedure paginas_calendario_det (pSemestre in number) is
      vFechaIniSemana   date;
      vFechaFinSemana   date;
   begin
      calcula_fechas_semestre (pSemestre);
      vFechaIniSemana := vFechaIniSemestre;
      vFechaFinSemana := vFechaIniSemana + 7;

      while vFechaFinSemana <= vFechaFinSemestre loop
         if semana_tiene_clase (vFechaIniSemana, vFechaFinSemana) then
            info_calendario_det (vFechaIniSemana, vFechaFinSemana);
            calendario_det (vFechaIniSemana, vFechaFinSemana);
            htp.p ('<fo:block break-before="page" color="white">_</fo:block>');
         end if;

         vFechaIniSemana := vFechaFinSemana;
         vFechaFinSemana := vFechaFinSemana + 7;
      end loop;
   end;

   procedure paginas_calendario_det_agr (pSemestre in number) is
      vFechaIniSemana   date;
      vFechaFinSemana   date;
   begin
      calcula_fechas_semestre (pSemestre);
      vFechaIniSemana := vFechaIniSemestre;
      vFechaFinSemana := vFechaIniSemana + 7;

      while vFechaFinSemana <= vFechaFinSemestre loop
         if semana_tiene_clase_agr (vFechaIniSemana, vFechaFinSemana) then
            info_calendario_det (vFechaIniSemana, vFechaFinSemana);
            calendario_det_agrupacion (vFechaIniSemana, vFechaFinSemana);
            htp.p ('<fo:block break-before="page" color="white">_</fo:block>');
         end if;
         vFechaIniSemana := vFechaFinSemana;
         vFechaFinSemana := vFechaFinSemana + 7;
      end loop;
   end;


BEGIN
   --euji_control_acceso (vItem);
   select count (*)
   into   vSesion
   from   apa_sesiones
   where  sesion_id = pSesion
   and    fecha < sysdate () - 0.33;

   if vSesion > 0 then
      calcula_curso_aca;
      cabecera;

      if vEsGenerica then
         info_calendario_gen (1);

         if (pCurso is not null) then
            calendario_gen (1);
            leyenda_asigs (1);
         else
            calendario_gen_agrupacion(1);
            leyenda_asigs_agrupacion (1);
         end if;


         htp.p ('<fo:block break-before="page" color="white">_</fo:block>');
         info_calendario_gen (2);

         if (pCurso is not null) then
            calendario_gen (2);
            leyenda_asigs (2);
         else
            calendario_gen_agrupacion(2);
            leyenda_asigs_agrupacion (2);
         end if;
      else
         if (pCurso is not null) then
            paginas_calendario_det (pSemestre);
         else
            paginas_calendario_det_agr(pSemestre);
         end if;

         leyenda_asigs (pSemestre);
      end if;

      pie;
   else
      cabecera;
      fop.block ('No tens perm?s per accedir a aquest document.', font_weight => 'bold', text_align => 'center',
                 font_size => 14, space_after => 0.5);
      pie;
   end if;
EXCEPTION
   WHEN OTHERS THEN
      htp.p (sqlerrm);
END;

CREATE OR REPLACE PROCEDURE UJI_HORARIOS.HORARIOS_VALIDACION_PDF (
   name_array    IN   euji_util.ident_arr2,
   value_array   IN   euji_util.ident_arr2
) IS
   -- Parametros
   pEstudio    CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pEstudio');
   pSemestre   CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pSemestre');
   pGrupo      CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pGrupo');
   pCurso      CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pCurso');
   pSesion     CONSTANT VARCHAR2 (400)  := euji_util.euji_getparam (name_array, value_array, 'pSesion');
   v_nombre             varchar2 (2000);
   vSesion              number;

   procedure cabecera is
   begin
      owa_util.mime_header (ccontent_type => 'text/xml', bclose_header => true);
      fop.documentOpen (margin_left => 1.5, margin_right => 1.5, margin_top => 0.5, margin_bottom => 0.5,
                        extent_before => 3, extent_after => 1.5, extent_margin_top => 3, extent_margin_bottom => 1.5);
      fop.pageBodyOpen;
      htp.p (fof.documentheaderopen || fof.blockopen (space_after => 0.1) || fof.tableOpen
             || fof.tablecolumndefinition (column_width => 3) || fof.tablecolumndefinition (column_width => 15)
             || fof.tablebodyopen || fof.tableRowOpen || fof.tablecellopen
             || fof.block (fof.logo_uji, text_align => 'center') || fof.tablecellclose
             || fof.tablecellopen (display_align => 'center', padding => 0.4)
             || fof.block (text => 'Controls horaris', font_size => 12, font_weight => 'bold', font_family => 'Arial',
                           text_align => 'center')
             || fof.block (text => v_nombre, font_size => 12, font_weight => 'bold', font_family => 'Arial',
                           text_align => 'center')
             || fof.block (text => 'Curs: ' || pCurso || ' - Semestre: ' || pSemestre || ' - Grup: ' || pGrupo,
                           font_size => 12, font_weight => 'bold', font_family => 'Arial', text_align => 'center')
             || fof.tablecellclose || fof.tableRowClose || fof.tablebodyclose || fof.tableClose || fof.blockclose
             || fof.documentheaderclose);
      -- El numero paginas se escribe a mano para poner el texto m?s peque?o
      htp.p (fof.documentfooteropen || fof.tableOpen || fof.tablecolumndefinition (column_width => 18)
             || fof.tablebodyopen || fof.tableRowOpen || fof.tablecellopen
             || fof.block (text => 'Pag. <fo:page-number /> de <fo:page-number-citation ref-id="lastBlock" />',
                           text_align => 'right', font_weight => 'bold', font_size => 8)
             || fof.tablecellclose || fof.tableRowClose || fof.tablebodyclose || fof.tableClose
             || fof.documentfooterclose);
      fop.pageFlowOpen;
      fop.block (fof.white_space);
   end;

   procedure pie is
   begin
      htp.p ('<fo:block  id="lastBlock" space-before="1cm" font-size="8pt">');
      htp.p ('Data de generació: ' || to_char (sysdate, 'dd/mm/yyy hh24:mi'));
      fop.blockclose ();
      fop.pageFlowClose;
      fop.pageBodyClose;
      fop.documentClose;
   end;

   function espacios (p_espacios in number)
      return varchar2 is
      v_rdo   varchar2 (2000);
   begin
      v_rdo := null;

      for x in 1 .. p_espacios loop
         v_rdo := v_rdo || fof.white_space;
      end loop;

      return v_rdo;
   end;

   procedure p (p_texto in varchar2) is
   begin
      fop.block (espacios (3) || p_texto);
   end;

   procedure control_1 is
      v_aux   number := 0;

      cursor lista_errores_1 is
         select distinct asignatura_id, grupo_id, tipo_subgrupo_id || subgrupo_id subgrupo,
                         decode (tipo_subgrupo_id, 'TE', 1, 'PR', 2, 'LA', 3, 'SE', 4, 0) tipo
         from            uji_horarios.hor_items i,
                         hor_items_asignaturas asi
         where           i.id = asi.item_id
         and             asi.curso_id = pCurso
         and             semestre_id = pSemestre
         --and             grupo_id = pGrupo
         and             pGrupo like '%' || grupo_id || '%'
         and             tipo_subgrupo_id not in ('AV', 'TU')
         and             dia_semana_id is null
         and             estudio_id = pEstudio
         order by        1,
                         2,
                         decode (tipo_subgrupo_id, 'TE', 1, 'PR', 2, 'LA', 3, 'SE', 4, 0);
   begin
      fop.block ('Control 1 - Subgrups sense planificar', font_size => 12, font_weight => 'bold',
                 border_bottom => 'solid');

      for x in lista_errores_1 loop
         v_aux := v_aux + 1;
         p (x.asignatura_id || ' ' || x.grupo_id || ' ' || x.subgrupo);
      end loop;

      if v_aux = 0 then
         fop.block ('No hi ha cap error.', space_before => 0.5);
      elsif v_aux = 1 then
         fop.block ('Trovat ' || v_aux || ' error.', space_before => 0.5);
      else
         fop.block ('Trovats ' || v_aux || ' errors.', space_before => 0.5);
      end if;
   exception
      when others then
         p (sqlerrm);
   end;

   procedure control_2 is
      v_aux          number        := 0;
      v_asignatura   varchar2 (20);

      cursor lista_errores_2_v1 is
         select   asignatura_id, grupo_id, tipo_subgrupo_id || subgrupo_id subgrupo,
                  decode (dia_semana_id,
                          1, 'Dilluns',
                          2, 'Dimarts',
                          3, 'Dimecres',
                          4, 'Dijous',
                          5, 'Divendres',
                          6, 'Dissabte',
                          7, 'Diumenge',
                          'Error'
                         ) dia_semana,
                  hora_inicio, hora_fin,
                  (select asi2.asignatura_id || ' ' || i2.tipo_subgrupo_id || i2.subgrupo_id grupo
                   from   uji_horarios.hor_items i2,
                          uji_horarios.hor_items_asignaturas asi2
                   where  i.id <> i2.id
                   and    i2.id = asi2.item_id
                   --and    i.curso_id = i2.curso_id
                   and    i.semestre_id = i2.semestre_id
                   and    i.grupo_id = i2.grupo_id
                   and    i.tipo_subgrupo_id <> i2.tipo_subgrupo_id
                   and    i.dia_semana_id = i2.dia_semana_id
                   and    asi.estudio_id = asi2.estudio_id
                   and    asi.asignatura_id = asi2.asignatura_id
                   and    (   to_number (to_char (i.hora_inicio, 'hh24mi')) between to_number (to_char (i2.hora_inicio,
                                                                                                        'hh24mi'))
                                                                                and to_number (to_char (i2.hora_fin,
                                                                                                        'hh24mi'))
                           or to_number (to_char (i.hora_fin, 'hh24mi')) between to_number (to_char (i2.hora_inicio,
                                                                                                     'hh24mi'))
                                                                             and to_number (to_char (i2.hora_fin,
                                                                                                     'hh24mi'))
                          )
                   and    rownum <= 1) solape
         from     uji_horarios.hor_items i,
                  uji_horarios.hor_items_asignaturas asi
         where    i.id = asi.item_id
         and      asi.curso_id = pCurso
         and      semestre_id = pSemestre
         --and      grupo_id = pGrupo
         and      pGrupo like '%' || grupo_id || '%'
         and      tipo_subgrupo_id in ('TE')
         and      estudio_id = pEstudio
         and      exists (
                     select 1
                     from   uji_horarios.hor_items i2,
                            uji_horarios.hor_items_asignaturas asi2
                     where  i.id <> i2.id
                     and    i2.id = asi2.item_id
                     --and    i.curso_id = i2.curso_id
                     and    i.semestre_id = i2.semestre_id
                     and    i.grupo_id = i2.grupo_id
                     and    i.tipo_subgrupo_id <> i2.tipo_subgrupo_id
                     and    i.dia_semana_id = i2.dia_semana_id
                     and    asi.estudio_id = asi2.estudio_id
                     and    asi.asignatura_id = asi2.asignatura_id
                     and    (   to_number (to_char (i.hora_inicio, 'hh24mi')) between to_number
                                                                                               (to_char (i2.hora_inicio,
                                                                                                         'hh24mi'))
                                                                                  and to_number (to_char (i2.hora_fin,
                                                                                                          'hh24mi'))
                             or to_number (to_char (i.hora_fin, 'hh24mi')) between to_number (to_char (i2.hora_inicio,
                                                                                                       'hh24mi'))
                                                                               and to_number (to_char (i2.hora_fin,
                                                                                                       'hh24mi'))
                            ))
         order by 1,
                  2,
                  3;

      cursor lista_errores_2 is
         select distinct asignatura_id, grupo_id, tipo_subgrupo_id || subgrupo_id subgrupo,
                         decode (dia_semana_id,
                                 1, 'Dilluns',
                                 2, 'Dimarts',
                                 3, 'Dimecres',
                                 4, 'Dijous',
                                 5, 'Divendres',
                                 6, 'Dissabte',
                                 7, 'Diumenge',
                                 'Error'
                                ) dia_semana,
                         to_char (inicio, 'dd/mm/yyyy') fecha, to_char (inicio, 'hh24:mi') hora_inicio,
                         to_char (fin, 'hh24:mi') hora_fin,
                         (select asi2.asignatura_id || ' ' || i2.tipo_subgrupo_id || i2.subgrupo_id || ' - '
                                 || to_char (inicio, 'hh24:mi') || '-' || to_char (fin, 'hh24:mi') grupo
                          from   uji_horarios.hor_items i2,
                                 uji_horarios.hor_items_asignaturas asi2,
                                 uji_horarios.hor_items_detalle det2
                          where  i.id <> i2.id
                          and    i2.id = det2.item_id
                          and    i2.id = asi2.item_id
                          --and    i.curso_id = i2.curso_id
                          and    i.semestre_id = i2.semestre_id
                          and    i.grupo_id = i2.grupo_id
                          and    i.tipo_subgrupo_id <> i2.tipo_subgrupo_id
                          and    i.dia_semana_id = i2.dia_semana_id
                          and    asi.estudio_id = asi2.estudio_id
                          and    asi.asignatura_id = asi2.asignatura_id
                          and    trunc (det.inicio) = trunc (det2.inicio)
                          and    (   to_number (to_char (det.inicio + 1 / 1440, 'hh24mi'))
                                        between to_number (to_char (det2.inicio, 'hh24mi'))
                                            and to_number (to_char (det2.fin, 'hh24mi'))
                                  or to_number (to_char (det.fin - 1 / 1440, 'hh24mi'))
                                        between to_number (to_char (det2.inicio, 'hh24mi'))
                                            and to_number (to_char (det2.fin, 'hh24mi'))
                                 )
                          and    rownum <= 1) solape
         from            uji_horarios.hor_items i,
                         uji_horarios.hor_items_asignaturas asi,
                         uji_horarios.hor_items_detalle det
         where           i.id = asi.item_id
         and             i.id = det.item_id
         and             asi.curso_id = pCurso
         and             semestre_id = pSemestre
         --and             grupo_id = pGrupo
         and             pGrupo like '%' || grupo_id || '%'
         and             tipo_subgrupo_id in ('TE')
         and             estudio_id = pEstudio
         and             exists (
                            select 1
                            from   uji_horarios.hor_items i2,
                                   uji_horarios.hor_items_asignaturas asi2,
                                   uji_horarios.hor_items_detalle det2
                            where  i.id <> i2.id
                            and    i2.id = det2.item_id
                            and    i2.id = asi2.item_id
                            --and    i.curso_id = i2.curso_id
                            and    i.semestre_id = i2.semestre_id
                            and    i.grupo_id = i2.grupo_id
                            and    i.tipo_subgrupo_id <> i2.tipo_subgrupo_id
                            and    i.dia_semana_id = i2.dia_semana_id
                            and    asi.estudio_id = asi2.estudio_id
                            and    asi.asignatura_id = asi2.asignatura_id
                            and    trunc (det.inicio) = trunc (det2.inicio)
                            and    (   to_number (to_char (det.inicio + 1 / 1440, 'hh24mi'))
                                          between to_number (to_char (det2.inicio, 'hh24mi'))
                                              and to_number (to_char (det2.fin, 'hh24mi'))
                                    or to_number (to_char (det.fin - 1 / 1440, 'hh24mi'))
                                          between to_number (to_char (det2.inicio, 'hh24mi'))
                                              and to_number (to_char ((det2.fin), 'hh24mi'))
                                   ))
         order by        1,
                         2,
                         3,
                         5;
   begin
      fop.block ('Control 2 - Solpament subgrups TE amb la resta', font_size => 12, font_weight => 'bold',
                 border_bottom => 'solid', space_before => 0.5);
      v_asignatura := 'PRIMERA';

      for x in lista_errores_2 loop
         v_aux := v_aux + 1;

         if     v_aux <> 1
            and v_asignatura <> x.asignatura_id then
            p (' ');
         end if;

         p (x.asignatura_id || ' ' || x.grupo_id || ' ' || x.subgrupo || ' ' || x.dia_semana || ' - ' || x.fecha || ' '
            || x.hora_inicio || '  ->  solapa amb: ' || x.solape);
         v_asignatura := x.asignatura_id;
      end loop;

      if v_aux = 0 then
         fop.block ('No hi ha cap error.', space_before => 0.5);
      elsif v_aux = 1 then
         fop.block ('Trovat ' || v_aux || ' error.', space_before => 0.5);
      else
         fop.block ('Trovats ' || v_aux || ' errors.', space_before => 0.5);
      end if;
   exception
      when others then
         p (sqlerrm);
   end;

   procedure control_3 is
      v_aux          number        := 0;
      v_asignatura   varchar2 (20);
      v_control      number;

      cursor lista_asignaturas is
         select distinct asi.asignatura_id
         from            uji_horarios.hor_items i,
                         uji_horarios.hor_items_asignaturas asi,
                         uji_horarios.hor_items_detalle det
         where           i.id = asi.item_id
         and             i.id = det.item_id
         and             asi.curso_id = pCurso
         and             semestre_id = pSemestre
         --and             grupo_id = pGrupo
         and             pGrupo like '%' || grupo_id || '%'
         and             estudio_id = pEstudio
         order by        1;

      cursor lista_errores_3 (p_asignatura in varchar2) is
         select   *
         from     (select   asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo, creditos, horas_totales,
                            sum (horas_detalle) horas_detalle, decode (tipo, 'S', 'Semestral', 'A', 'Anual') tipo
                   from     (select asi.asignatura_id, grupo_id, tipo_subgrupo_id,
                                    tipo_subgrupo_id || subgrupo_id subgrupo,
                                    decode (tipo_subgrupo_id,
                                            'TE', crd_te,
                                            'PR', crd_pr,
                                            'LA', crd_la,
                                            'SE', crd_se,
                                            'TU', crd_tu,
                                            crd_ev
                                           ) creditos,
                                    decode (tipo_subgrupo_id,
                                            'TE', crd_te,
                                            'PR', crd_pr,
                                            'LA', crd_la,
                                            'SE', crd_se,
                                            'TU', crd_tu,
                                            crd_ev
                                           )
                                    / decode (ad.tipo, 'A', 2, 1) * 10 horas_totales,
                                    (fin - inicio) * 24 horas_detalle, ad.tipo tipo
                             from   uji_horarios.hor_items i,
                                    uji_horarios.hor_items_asignaturas asi,
                                    uji_horarios.hor_items_detalle det,
                                    uji_horarios.hor_ext_asignaturas_detalle ad
                             where  i.id = asi.item_id
                             and    i.id = det.item_id
                             and    asi.curso_id = pCurso
                             and    semestre_id = pSemestre
                             --and    grupo_id = pGrupo
                             and    pGrupo like '%' || grupo_id || '%'
                             and    estudio_id = pEstudio
                             and    asi.asignatura_id = ad.asignatura_id
                             and    asi.asignatura_id = p_asignatura
                             and    ad.tipo = 'S'
                             and    dia_semana_id is not null)
                   group by asignatura_id,
                            grupo_id,
                            tipo_subgrupo_id,
                            subgrupo,
                            creditos,
                            tipo,
                            horas_totales
                   having   horas_totales not between sum (horas_detalle) * 0.95 and sum (horas_detalle) * 1.05
                   union all
                   select   asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo, max (creditos) creditos,
                            max (horas_totales) horas_totales, sum (horas_detalle) horas_detalle,
                            decode (tipo, 'S', 'Semestral', 'A', 'Anual') tipo
                   from     (select   asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo, creditos, horas_totales,
                                      sum (horas_detalle) horas_detalle, tipo, null semestre_id
                             from     (select asi.asignatura_id, grupo_id, tipo_subgrupo_id,
                                              tipo_subgrupo_id || subgrupo_id subgrupo,
                                              decode (tipo_subgrupo_id,
                                                      'TE', crd_te,
                                                      'PR', crd_pr,
                                                      'LA', crd_la,
                                                      'SE', crd_se,
                                                      'TU', crd_tu,
                                                      crd_ev
                                                     ) creditos,
                                              decode (tipo_subgrupo_id,
                                                      'TE', crd_te,
                                                      'PR', crd_pr,
                                                      'LA', crd_la,
                                                      'SE', crd_se,
                                                      'TU', crd_tu,
                                                      crd_ev
                                                     )
                                              * 10 horas_totales,
                                              (fin - inicio) * 24 horas_detalle, ad.tipo tipo, semestre_id
                                       from   uji_horarios.hor_items i,
                                              uji_horarios.hor_items_asignaturas asi,
                                              uji_horarios.hor_items_detalle det,
                                              uji_horarios.hor_ext_asignaturas_detalle ad
                                       where  i.id = asi.item_id
                                       and    i.id = det.item_id
                                       and    asi.curso_id = pCurso
                                       --and    semestre_id = pSemestre
                                       --and    grupo_id = pGrupo
                                       and    pGrupo like '%' || grupo_id || '%'
                                       and    estudio_id = pEstudio
                                       and    asi.asignatura_id = ad.asignatura_id
                                       and    asi.asignatura_id = p_asignatura
                                       and    ad.tipo = 'A'
                                       and    dia_semana_id is not null)
                             group by asignatura_id,
                                      grupo_id,
                                      tipo_subgrupo_id,
                                      subgrupo,
                                      creditos,
                                      tipo,
                                      horas_totales                                                                  --,
                             --semestre_id
                             having   horas_totales not between sum (horas_detalle) * 0.95 and sum (horas_detalle)
                                                                                               * 1.05)
                   group by asignatura_id,
                            grupo_id,
                            tipo_subgrupo_id,
                            subgrupo,
                            tipo)
         order by 1,
                  decode (tipo_subgrupo_id, 'TE', 1, 'PR', 2, 'LA', 3, 'SE', 4, 'TU', 5, 6),
                  3;
   begin
      v_control := 0;
      fop.block ('Control 3 - Control d''hores planificades per subgrup', font_size => 12, font_weight => 'bold',
                 border_bottom => 'solid', space_before => 0.5);
      v_asignatura := 'PRIMERA';
      v_control := 1;

      for asi in lista_asignaturas loop
         v_control := 2;

         for x in lista_errores_3 (asi.asignatura_id) loop
            v_control := 3;
            v_aux := v_aux + 1;

            if     v_aux <> 1
               and v_asignatura <> x.asignatura_id then
               p (' ');
            end if;

            if x.horas_totales > x.horas_detalle then
               p (x.asignatura_id || ' ' || x.grupo_id || ' ' || x.subgrupo || ' -' || x.tipo || '- '
                  || 'Planificades ' || round (to_char (x.horas_totales - x.horas_detalle), 3)
                  || ' hores de menys, cal planificar-ne ' || x.horas_totales);
            else
               p (x.asignatura_id || ' ' || x.grupo_id || ' ' || x.subgrupo || ' -' || x.tipo || '- '
                  || 'Planificades ' || round (to_char (x.horas_detalle - x.horas_totales), 3)
                  || ' hores de més, cal planificar-ne ' || x.horas_totales);
            end if;

            v_asignatura := x.asignatura_id;
         end loop;
      end loop;

      v_control := 6;

      if v_aux = 0 then
         fop.block ('No hi ha cap error.', space_before => 0.5);
      elsif v_aux = 1 then
         fop.block ('Trovat ' || v_aux || ' error.', space_before => 0.5);
      else
         fop.block ('Trovats ' || v_aux || ' errors.', space_before => 0.5);
      end if;
   exception
      when others then
         p (v_control || '-' || sqlerrm || DBMS_UTILITY.format_error_backtrace);
   end;

   procedure control_4 is
      v_aux          number        := 0;
      v_asignatura   varchar2 (20);

      cursor lista_errores_4 is
         select   asi.asignatura_id, grupo_id, i.tipo_subgrupo_id, subgrupo_id,
                  to_char (hora_inicio, 'hh24:mi') hora_inicio,
                  decode (dia_semana_id,
                          1, 'Dilluns',
                          2, 'Dimarts',
                          3, 'Dimecres',
                          4, 'Dijous',
                          5, 'Divendres',
                          6, 'Dissabte',
                          7, 'Diumenge',
                          'Error'
                         ) dia_semana,
                  dia_semana_id
         from     uji_horarios.hor_items i,
                  uji_horarios.hor_items_asignaturas asi
         where    i.id = asi.item_id
         and      asi.curso_id = pCurso
         and      semestre_id = pSemestre
         --and      grupo_id = pGrupo
         and      pGrupo like '%' || grupo_id || '%'
         and      estudio_id = pEstudio
         and      dia_semana_id is not null
         and      aula_planificacion_id is null
         order by 1,
                  decode (tipo_subgrupo_id, 'TE', 1, 'PR', 2, 'LA', 3, 'SE', 4, 'TU', 5, 6),
                  4;
   begin
      fop.block ('Control 4 - Control d''assignació d''aules per subgrup', font_size => 12, font_weight => 'bold',
                 border_bottom => 'solid', space_before => 0.5);
      v_asignatura := 'PRIMERA';

      for x in lista_errores_4 loop
         v_aux := v_aux + 1;

         if     v_aux <> 1
            and v_asignatura <> x.asignatura_id then
            p (' ');
         end if;

         p (x.asignatura_id || ' ' || x.grupo_id || ' ' || x.tipo_subgrupo_id || x.subgrupo_id || ' ' || x.dia_semana
            || ' ' || x.hora_inicio || ' - no te aula assignada');
         v_asignatura := x.asignatura_id;
      end loop;
   exception
      when others then
         p (sqlerrm);
   end;

   procedure control_5 is
      v_aux          number          := 0;
      v_aula         number;
      v_dia_semana   number;
      v_hora         varchar2 (20);
      v_txt          varchar2 (2000);

      cursor lista_errores_5 is
         select   dia_semana_id, trunc (det.inicio) fecha, to_char (hora_inicio, 'hh24:mi') hora_inicio,
                  aula_planificacion_id,
                  decode (dia_semana_id,
                          1, 'Dilluns',
                          2, 'Dimarts',
                          3, 'Dimecres',
                          4, 'Dijous',
                          5, 'Divendres',
                          6, 'Dissabte',
                          7, 'Diumenge',
                          'Error'
                         ) dia_semana,
                  nvl (a.nombre, 'Aula mal assignada a l''estudi (' || aula_planificacion_id || ')') nombre_aula
         from     uji_horarios.hor_items i,
                  uji_horarios.hor_items_asignaturas asi,
                  uji_horarios.hor_items_detalle det,
                  uji_horarios.hor_aulas a
         where    i.id = asi.item_id
         and      i.id = det.item_id
         and      asi.curso_id = pCurso
         and      semestre_id = pSemestre
         --and      grupo_id = pGrupo
         and      pGrupo like '%' || grupo_id || '%'
         and      estudio_id = pEstudio
         and      dia_semana_id is not null
         and      aula_planificacion_id is not null
         and      aula_planificacion_id = a.id(+)
         group by                                                                                   --asi.asignatura_id,
                  grupo_id,
                  dia_semana_id,
                  trunc (det.inicio),
                  to_char (hora_inicio, 'hh24:mi'),
                  aula_planificacion_id,
                  a.nombre
         having   count (*) > 1
         order by 5,
                  1,
                  3,
                  2;

      cursor lista_subgrupos (p_dia_Semana in number, p_fecha in date, p_hora in varchar2, p_aula in number) is
         select   nvl (comun_texto, asignatura_id) asignatura_id, grupo_id, tipo_subgrupo_id || subgrupo_id subgrupo
         from     uji_horarios.hor_items i,
                  uji_horarios.hor_items_asignaturas asi,
                  uji_horarios.hor_items_detalle det
         where    i.id = asi.item_id
         and      i.id = det.item_id
         and      dia_semana_id = p_dia_semana
         and      trunc (inicio) = p_fecha
         and      to_char (hora_inicio, 'hh24:mi') = p_hora
         and      aula_planificacion_id = p_aula
         and      estudio_id = pEstudio
         order by 1,
                  2;
   begin
      fop.block ('Control 5 - Control de solapament en aules', font_size => 12, font_weight => 'bold',
                 border_bottom => 'solid', space_before => 0.5);
      v_aula := 0;
      v_dia_semana := 0;
      v_hora := '';

      for x in lista_errores_5 loop
         v_aux := v_aux + 1;

         if     v_aux <> 1
            and not (    v_aula = x.aula_planificacion_id
                     and v_dia_semana = x.dia_semana_id
                     and v_hora = x.hora_inicio) then
            p (' ');
         end if;

         v_txt := null;

         if 1 = 2 then
            p (x.nombre_aula || ' - ' || to_char (x.fecha, 'dd/mm/yyyy') || ' ' || x.dia_semana || ' ' || x.hora_inicio);

            for det in lista_subgrupos (x.dia_semana_id, x.fecha, x.hora_inicio, x.aula_planificacion_id) loop
               p (espacios (30) || det.grupo_id || ' ' || det.asignatura_id || '-' || det.subgrupo);
            end loop;
         else
            for det in lista_subgrupos (x.dia_semana_id, x.fecha, x.hora_inicio, x.aula_planificacion_id) loop
               v_txt := v_txt || det.asignatura_id || '-' || det.grupo_id || ' ' || det.subgrupo || ' | ';
            end loop;

            v_txt := trim (v_txt);
            v_txt := substr (v_txt, 1, length (v_txt) - 2);
            p (x.nombre_aula || ' - ' || to_char (x.fecha, 'dd/mm/yyyy') || ' ' || x.dia_semana || ' ' || x.hora_inicio
               || ' ==> ' || v_txt);
         end if;

         v_aula := x.aula_planificacion_id;
         v_dia_semana := x.dia_semana_id;
         v_hora := x.hora_inicio;
      end loop;
   exception
      when others then
         p (sqlerrm);
   end;

   procedure control_6 is
      v_aux   number;

      cursor control_6 is
         select distinct asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo_id, trunc (inicio) fecha,
                         decode (dia_semana_id,
                                 1, 'Dilluns',
                                 2, 'Dimarts',
                                 3, 'Dimecres',
                                 4, 'Dijous',
                                 5, 'Divendres',
                                 6, 'Dissabte',
                                 7, 'Diumenge',
                                 'Error'
                                ) dia_semana
         from            hor_items i,
                         hor_items_asignaturas a,
                         hor_items_detalle d
         where           i.id = a.item_id
         and             detalle_manual = 0
         and             estudio_id = pEstudio
         and             curso_id = pCurso
         and             semestre_id = pSemestre
         and             pGrupo like '%' || grupo_id || '%'
         and             i.id = d.item_id
         and             trunc (d.inicio) in (select fecha
                                              from   hor_v_items_DEtalle
                                              where  id = i.id
                                              and    docencia = 'N')
         order by        asignatura_id,
                         grupo_id,
                         decode (tipo_subgrupo_id, 'TE', 1, 'PR', 2, 'LA', 3, 'SE', 4, 'TU', 5, 6),
                         subgrupo_id;
   begin
      fop.block ('Control 6 - Control de items amb data incorrecta', font_size => 12, font_weight => 'bold',
                 border_bottom => 'solid', space_before => 0.5);
      v_aux := 0;

      for x in control_6 loop
         p (x.asignatura_id || ' ' || x.grupo_id || ' ' || x.tipo_subgrupo_id || x.subgrupo_id || ' del '
            || x.dia_semana || ' te problemes amb la data ' || to_char (x.fecha, 'dd/mm/yyyy'));
         v_aux := v_aux + 1;
      end loop;

      if v_aux > 0 then
         p (' ');
         p ('Trovats ' || v_aux || ' errors.');
         p (' ');
      end if;
   exception
      when others then
         p (sqlerrm);
   end;

   procedure advertencia_1 is
      v_aux             number        := 0;
      v_asignatura      varchar2 (20);
      v_tipo_subgrupo   varchar2 (20);
      v_subgrupo        number;

      cursor lista_advertencia_1 is
         select   asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo_id
         from     uji_horarios.hor_items i,
                  uji_horarios.hor_items_asignaturas asi
         where    i.id = asi.item_id
         and      asi.curso_id = pCurso
         and      semestre_id = pSemestre
         --and      grupo_id = pGrupo
         and      pGrupo like '%' || grupo_id || '%'
         and      estudio_id = pEstudio
         and      dia_semana_id is not null
         and      aula_planificacion_id is not null
         group by asignatura_id,
                  grupo_id,
                  tipo_subgrupo_id,
                  subgrupo_id
         having   count (distinct aula_planificacion_id) > 1;

      cursor lista_aulas (p_asignatura in varchar2, p_tipo_subgrupo in varchar2, p_subgrupo in number) is
         select distinct nvl (a.nombre, 'Aula mal assignada a l''estudi (' || aula_planificacion_id || ')') nombre_aula
         from            uji_horarios.hor_items i,
                         uji_horarios.hor_items_asignaturas asi,
                         uji_horarios.hor_aulas a
         where           i.id = asi.item_id
         and             i.aula_planificacion_id = a.id(+)
         and             asi.curso_id = pCurso
         and             semestre_id = pSemestre
         --and             grupo_id = pGrupo
         and             pGrupo like '%' || grupo_id || '%'
         and             estudio_id = pEstudio
         and             dia_semana_id is not null
         and             aula_planificacion_id is not null
         and             asignatura_id = p_asignatura
         and             tipo_subgrupo_id = p_tipo_subgrupo
         and             subgrupo_id = p_subgrupo
         order by        1;
   begin
      fop.block ('Advertencia 1 - Classes d''un subgrup en aules diferents', font_size => 12, font_weight => 'bold',
                 border_bottom => 'solid', space_before => 0.5);
      v_aux := v_aux + 1;
      v_asignatura := 'PRIMERA';
      v_tipo_subgrupo := 'XX';
      v_subgrupo := 0;

      for x in lista_advertencia_1 loop
         v_aux := v_aux + 1;

         if     v_aux <> 1
            and not (    v_asignatura = x.asignatura_id
                     and v_tipo_subgrupo = x.tipo_subgrupo_id
                     and v_subgrupo = x.subgrupo_id
                    ) then
            p (' ');
         end if;

         for det in lista_aulas (x.asignatura_id, x.tipo_subgrupo_id, x.subgrupo_id) loop
            p (x.asignatura_id || ' - ' || x.grupo_id || ' ' || x.tipo_subgrupo_id || x.subgrupo_id || ' '
               || det.nombre_aula);
         end loop;

         v_asignatura := x.asignatura_id;
         v_tipo_subgrupo := x.tipo_subgrupo_id;
         v_subgrupo := x.subgrupo_id;
      end loop;
   exception
      when others then
         p (sqlerrm);
   end;
BEGIN
   select count (*)
   into   vSesion
   from   apa_sesiones
   where  sesion_id = pSesion
   and    fecha < sysdate () - 0.33;

   if vSesion > 0 then
      select nombre
      into   v_nombre
      from   hor_estudios
      where  id = pEstudio;

      cabecera;
      control_1;
      control_2;
      control_3;
      control_4;
      control_5;
      control_6;
      advertencia_1;
      pie;
   else
      cabecera;
      fop.block ('No tens permís per accedir a aquest document.', font_weight => 'bold', text_align => 'center',
                 font_size => 14, space_after => 0.5);
      pie;
   end if;
EXCEPTION
   WHEN OTHERS THEN
      htp.p (sqlerrm || DBMS_UTILITY.format_error_backtrace);
END;

CREATE OR REPLACE PROCEDURE UJI_HORARIOS.pod_consulta_asignaturas_pdf (
   name_array    IN   euji_util.ident_arr2,
   value_array   IN   euji_util.ident_arr2
) IS
   v_aux           NUMBER;
   -- pProfesor       number          := euji_util.euji_getparam (name_array, value_array, 'pProfesor');
   pDepartamento   number          := euji_util.euji_getparam (name_array, value_array, 'pDepartamento');
   pArea           number          := euji_util.euji_getparam (name_array, value_array, 'pArea');
   pSesion         VARCHAR2 (400)  := euji_util.euji_getparam (name_array, value_array, 'pSesion');
   v_nombre        varchar2 (2000);
   vSesion         number;
   vCurso          number;
   vCreditos       number;
   vAcum           number;
   vAcum2          number;
   vAcum3          number;
   vAcum4          number;
   vAcum5          number;
   vDepAcum        number;
   vDepAcum2       number;
   vDepAcum3       number;
   vDepAcum4       number;
   vDepAcum5       number;
   vFechas         t_horario;
   vRdo            clob;

   cursor lista_areas (p_Area in number) is
      select   *
      from     hor_areas
      where    departamento_id = pDepartamento
      and      id = p_Area
      order by nombre;

   cursor lista_subgrupos (p_Area in number) is
      select   nvl (comun_texto, ia.asignatura_id) asignatura_txt, min (ia.asignatura_id) asignatura_id,
               hor_buscar_nombre (nvl (comun_texto, ia.asignatura_id)) asignatura, area_id, grupo_id, tipo_subgrupo_id,
               subgrupo_id, creditos
      from     hor_v_asignaturas_area aa,
               hor_items i,
               hor_items_asignaturas ia
      where    i.id = ia.item_id
      and      area_id = p_area
      and      ia.asignatura_id = aa.asignatura_id
      --and             (   ia.asignatura_id = 'AG1023'
      --                 or ia.asignatura_id = 'DI1029')
      --and             ia.asignatura_id = 'EE1012'
      --and             area_id = 195
      --and      ia.asignatura_id like '%1003'
      --and      ia.asignatura_id in ('EE1012', 'EM1012', 'ET1012')
      --and      ia.asignatura_id in ('SIX500')
      --and      ia.asignatura_id like 'SIX%'
      group by nvl (comun_texto, ia.asignatura_id),
               --asignatura,
               area_id,
               grupo_id,
               tipo_subgrupo_id,
               subgrupo_id,
               creditos
      order by 1,
               2,
               3,
               4,
               5,
               decode (tipo_subgrupo_id, 'TE', 1, 'PR', 2, 'LA', 3, 'SE', 4, 'TU', 5, 6),
               7;

   function formatear (p_entrada in number)
      return varchar2 is
      v_rdo   varchar2 (100);
   begin
      v_rdo := replace (to_char (p_entrada, '9990.00'), '.', ',');
      return (v_rdo);
   end;

   procedure cabecera is
   begin
      owa_util.mime_header (ccontent_type => 'text/xml', bclose_header => true);
      fop.documentOpen (page_width => 29.7, page_height => 21, margin_left => 1.5, margin_right => 1.5,
                        margin_top => 0.5, margin_bottom => 0.5, extent_before => 3, extent_after => 1.5,
                        extent_margin_top => 3, extent_margin_bottom => 1.5);
      fop.pageBodyOpen;
      htp.p (fof.documentheaderopen || fof.blockopen (space_after => 0.1) || fof.tableOpen
             || fof.tablecolumndefinition (column_width => 3) || fof.tablecolumndefinition (column_width => 24)
             || fof.tablebodyopen || fof.tableRowOpen || fof.tablecellopen
             || fof.block (fof.logo_uji, text_align => 'center') || fof.tablecellclose
             || fof.tablecellopen (display_align => 'center', padding => 0.4)
             || fof.block (text => v_nombre, font_size => 16, font_weight => 'bold', font_family => 'Arial',
                           text_align => 'center')
             || fof.block (text => 'Informe POD per assignatura - Curs: ' || vCurso || '/'
                            || substr (to_char (vCurso + 1), 3),
                           font_size => 16, font_weight => 'bold', font_family => 'Arial', text_align => 'center')
             || fof.tablecellclose || fof.tableRowClose || fof.tablebodyclose || fof.tableClose || fof.blockclose
             || fof.documentheaderclose);
      -- El numero paginas se escribe a mano para poner el texto m?s peque?o
      htp.p (fof.documentfooteropen || fof.tableOpen || fof.tablecolumndefinition (column_width => 18)
             || fof.tablebodyopen || fof.tableRowOpen || fof.tablecellopen
             || fof.block (text => 'Pag. <fo:page-number /> de <fo:page-number-citation ref-id="lastBlock" />',
                           text_align => 'right', font_weight => 'bold', font_size => 8)
             || fof.tablecellclose || fof.tableRowClose || fof.tablebodyclose || fof.tableClose
             || fof.documentfooterclose);
      fop.pageFlowOpen;
      fop.block (fof.white_space);
   end;

   procedure pie is
   begin
      htp.p ('<fo:block  id="lastBlock" space-before="1cm" font-size="8pt">');
      htp.p ('Data de generació: ' || to_char (sysdate, 'dd/mm/yyy hh24:mi'));
      fop.blockclose ();
      fop.pageFlowClose;
      fop.pageBodyClose;
      fop.documentClose;
   end;

   function espacios (p_espacios in number)
      return varchar2 is
      v_rdo   varchar2 (2000);
   begin
      v_rdo := null;

      for x in 1 .. p_espacios loop
         v_rdo := v_rdo || fof.white_space;
      end loop;

      return v_rdo;
   end;

   procedure mostrar_subgrupos is
      v_asi_ant         varchar2 (100);
      v_border          varchar2 (100);
      v_creditos        number;
      v_creditos_Area   number;
      v_porcentaje      number;
      v_acum_1          number;
      v_acum_2          number;
      v_acum_3          number;
      v_area_1          number;
      v_area_2          number;
      v_area_3          number;
   begin
      for a in lista_areas (pArea) loop
         fop.block ('Area de coneixement: ' || fof.bold (a.nombre), font_size => 16, space_after => 0.2);
         fop.tableOpen;
         fop.tablecolumndefinition (column_width => 4);
         fop.tablecolumndefinition (column_width => 8);
         fop.tablecolumndefinition (column_width => 1);
         fop.tablecolumndefinition (column_width => 1);
         fop.tablecolumndefinition (column_width => 2);
         fop.tablecolumndefinition (column_width => 2);
         fop.tablecolumndefinition (column_width => 2);
         fop.tablecolumndefinition (column_width => 2);
         fop.tablecolumndefinition (column_width => 3);
         fop.tablecolumndefinition (column_width => 2);
         fop.tablebodyopen;
         fop.tableRowOpen;
         fop.tablecellopen;
         fop.block (fof.bold ('Codi'), font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold ('Nom assignatura'), font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold ('Grup'), font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold ('Tipus'), font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold ('Crèdits a impartir (A)'), font_size => 10, text_align => 'center');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold ('Crèdits assignats (B)'), font_size => 10, text_align => 'center');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold ('(B) - (A)'), font_size => 10, text_align => 'center');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold ('Crèdits assignats àrea (C)'), font_size => 10, text_align => 'center');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold ('(C) / (A)'), font_size => 10, text_align => 'center');
         fop.tablecellclose;
         fop.tableRowClose;
         v_asi_ant := 'XX';
         v_acum_1 := 0;
         v_acum_2 := 0;
         v_acum_3 := 0;
         v_area_1 := 0;
         v_area_2 := 0;
         v_area_3 := 0;

         for det in lista_subgrupos (a.id) loop
            /*select nvl (sum (creditos), 0), nvl (sum (creditos / cuantos), 0)
              into   v_creditos, v_creditos_Area
              from   (select distinct asignatura_id, grupo_id, tipo_subgrupo_id || subgrupo_id subgrp, d.profesor_id,
                                      nvl (creditos_detalle, d.creditos) creditos, area_id,
                                      count (distinct profesor_id) over (partition by asignatura_id, grupo_id, tipo_subgrupo_id
                                                                                                               || subgrupo_id)
                                                                                                                  cuantos*/
            select nvl (sum (nvl (creditos_detalle, creditos / cuantos)), 0)
            --nvl (sum (nvl (creditos_detalle, creditos / cuantos)), 0)
            into   v_creditos
            --v_creditos_Area
            from   (select distinct nvl (comun_texto, asignatura_id) asignatura_id, grupo_id,
                                    tipo_subgrupo_id || subgrupo_id subgrp, d.profesor_id, creditos_detalle, d.creditos,
                                    area_id,
                                    count (distinct profesor_id) over (partition by asignatura_id, grupo_id, tipo_subgrupo_id
                                                                                                             || subgrupo_id)
                                                                                                                cuantos
                    from            hor_v_items_prof_detalle d,
                                    hor_profesores p,
                                    hor_items_asignaturas asi,
                                    hor_items i
                    where           d.item_id in (
                                       select i.id
                                       from   hor_items i,
                                              hor_items_asignaturas asi2
                                       where  i.id = asi2.item_id
                                       and    asignatura_id = det.asignatura_id
                                       and    grupo_id = det.grupo_id
                                       and    tipo_subgrupo_id = det.tipo_subgrupo_id
                                       and    subgrupo_id = det.subgrupo_id)
                    and             profesor_id = p.id
                    and             d.item_id = asi.item_id
                    and             d.item_id = i.id
                    and             semestre_id =
                                       (select min (semestre_id)
                                        from   hor_items i,
                                               hor_items_asignaturas asi2
                                        where  i.id = asi2.item_id
                                        and    asignatura_id = det.asignatura_id
                                        and    grupo_id = det.grupo_id
                                        and    tipo_subgrupo_id = det.tipo_subgrupo_id
                                        and    subgrupo_id = det.subgrupo_id))
                                                                              --where  area_id = a.id
            ;

            select                                                    --nvl (sum (nvl (creditos_detalle, creditos)), 0),
                   nvl (sum (nvl (creditos_detalle, creditos / cuantos)), 0)
            into                                                                                           --v_creditos,
                   v_creditos_Area
            from   (select distinct nvl (comun_texto, asignatura_id) asignatura_id, grupo_id,
                                    tipo_subgrupo_id || subgrupo_id subgrp, d.profesor_id, creditos_detalle, d.creditos,
                                    area_id,
                                    count (distinct profesor_id) over (partition by asignatura_id, grupo_id, tipo_subgrupo_id
                                                                                                             || subgrupo_id)
                                                                                                                cuantos
                    from            hor_v_items_prof_detalle d,
                                    hor_profesores p,
                                    hor_items_asignaturas asi,
                                    hor_items i
                    where           d.item_id in (
                                       select i.id
                                       from   hor_items i,
                                              hor_items_asignaturas asi2
                                       where  i.id = asi2.item_id
                                       and    asignatura_id = det.asignatura_id
                                       and    grupo_id = det.grupo_id
                                       and    tipo_subgrupo_id = det.tipo_subgrupo_id
                                       and    subgrupo_id = det.subgrupo_id)
                    and             profesor_id = p.id
                    and             d.item_id = asi.item_id
                    and             d.item_id = i.id
                    and             semestre_id =
                                       (select min (semestre_id)
                                        from   hor_items i,
                                               hor_items_asignaturas asi2
                                        where  i.id = asi2.item_id
                                        and    asignatura_id = det.asignatura_id
                                        and    grupo_id = det.grupo_id
                                        and    tipo_subgrupo_id = det.tipo_subgrupo_id
                                        and    subgrupo_id = det.subgrupo_id))
            where  area_id = a.id;

            fop.tableRowOpen;

            if v_asi_ant = det.asignatura_id then
               fop.tablecellopen;
               fop.block (fof.white_space, font_size => 10);
               fop.tablecellclose;
               fop.tablecellopen;
               fop.block (fof.white_space, font_size => 10);
               fop.tablecellclose;
               v_border := '';
            else
               if v_asi_ant <> 'XX' then
                  -- linea 1
                  fop.tablecellopen (number_columns_spanned => 2);
                  fop.block (fof.bold ('Total per assignatura'), font_size => 10, space_after => 0.2,
                             text_align => 'right');
                  fop.tablecellclose;
                  fop.tablecellopen (number_columns_spanned => 2);
                  fop.block (fof.white_space, font_size => 10);
                  fop.tablecellclose;
                  fop.tablecellopen;
                  fop.block (fof.bold (formatear (v_acum_1)), font_size => 10, text_align => 'center');
                  fop.tablecellclose;
                  fop.tablecellopen;
                  fop.block (fof.bold (formatear (v_acum_2)), font_size => 10, text_align => 'center');
                  fop.tablecellclose;
                  fop.tablecellopen;
                  fop.block (fof.bold (formatear (v_acum_2 - v_acum_1)), font_size => 10, text_align => 'center');
                  fop.tablecellclose;
                  fop.tablecellopen;
                  fop.block (fof.bold (formatear (v_acum_3)), font_size => 10, text_align => 'center');
                  fop.tablecellclose;
                  fop.tablecellopen;

                  if v_acum_1 <> 0 then
                     fop.block (fof.bold (formatear (v_acum_3 / v_acum_1 * 100) || '%'), font_size => 10,
                                text_align => 'center');
                  else
                     fop.block (fof.bold (formatear (0) || '%'), font_size => 10, text_align => 'center');
                  end if;

                  fop.tablecellclose;
                  fop.tableRowClose;
                  -- linea 2
                  fop.tableRowOpen;
                  fop.tablecellopen (number_columns_spanned => 2);
                  fop.block (fof.bold ('Crèdits a impartir per l''àrea de coneixement'), font_size => 10,
                             space_after => 0.2, text_align => 'right');
                  fop.tablecellclose;
                  fop.tablecellopen (number_columns_spanned => 5);
                  fop.block (fof.white_space, font_size => 10);
                  fop.tablecellclose;
                  fop.tablecellopen;

                  if v_porcentaje <> 0 then
                     fop.block (fof.bold (formatear (v_acum_1 * v_porcentaje / 100)), font_size => 10,
                                text_align => 'center');
                  else
                     fop.block (fof.bold (formatear (0)), font_size => 10, text_align => 'center');
                  end if;

                  fop.tablecellclose;
                  fop.tablecellOpen;
                  fop.block (fof.bold (formatear (nvl (v_porcentaje, 0)) || '%'), font_size => 10,
                             text_align => 'center');
                  fop.tablecellclose;
                  fop.tableRowClose;
                  -- linea 3
                  fop.tableRowOpen;
                  fop.tablecellopen (number_columns_spanned => 2);
                  fop.block (fof.bold ('Estat de l''assignació docent'), font_size => 10, space_after => 0.2,
                             text_align => 'right');
                  fop.tablecellclose;
                  fop.tablecellopen (number_columns_spanned => 5);
                  fop.block (fof.white_space, font_size => 10);
                  fop.tablecellclose;

                  if v_porcentaje <> 0 then
                     fop.tablecellopen;
                     fop.block (fof.bold (formatear ((v_acum_1 * v_porcentaje / 100) - v_acum_3)), font_size => 10,
                                text_align => 'center');
                     fop.tablecellclose;

                     if round (((v_acum_1 * v_porcentaje / 100) - v_acum_3), 2) < 0 then
                        fop.tablecellopen;
                        fop.block (fof.bold ('Excés de crèdits'), font_size => 10, text_align => 'center');
                        fop.tablecellclose;
                     elsif round (((v_acum_1 * v_porcentaje / 100) - v_acum_3), 2) > 0 then
                        fop.tablecellopen;
                        fop.block (fof.bold ('Defecte de crèdits'), font_size => 10, text_align => 'center');
                        fop.tablecellclose;
                     end if;
                  else
                     fop.tablecellopen;
                     fop.block (fof.bold (formatear (0)), font_size => 10, text_align => 'center');
                     fop.tablecellclose;
                  end if;

                  fop.tableRowClose;
                  -- continuacion
                  fop.tableRowOpen;
               end if;

               v_acum_1 := 0;
               v_acum_2 := 0;
               v_acum_3 := 0;

               if v_asi_ant = 'XX' then
                  v_border := '';
               else
                  v_border := 'solid';
               end if;

               fop.tablecellopen (border_top => v_border);
               fop.block (det.asignatura_txt, font_size => 10);
               fop.tablecellclose;
               fop.tablecellopen (border_top => v_border);
               fop.block (det.asignatura, font_size => 10);
               fop.tablecellclose;
            end if;

            fop.tablecellopen (border_top => v_border);
            fop.block (det.grupo_id, font_size => 10, text_align => 'center');
            fop.tablecellclose;
            fop.tablecellopen (border_top => v_border);
            fop.block (det.tipo_subgrupo_id || det.subgrupo_id, font_size => 10, text_align => 'center');
            fop.tablecellclose;
            fop.tablecellopen (border_top => v_border);
            fop.block (formatear (det.creditos), font_size => 10, text_align => 'center');
            fop.tablecellclose;
            fop.tablecellopen (border_top => v_border);
            fop.block (formatear (v_creditos), font_size => 10, text_align => 'center');
            fop.tablecellclose;
            fop.tablecellopen (border_top => v_border);
            fop.block (formatear (v_creditos - det.creditos), font_size => 10, text_align => 'center');
            fop.tablecellclose;
            fop.tablecellopen (border_top => v_border);
            fop.block (formatear (v_creditos_Area), font_size => 10, text_align => 'center');
            fop.tablecellclose;
            fop.tablecellopen (border_top => v_border);

            if v_creditos > 0 then
               fop.block (formatear (v_creditos_area / det.creditos * 100) || '%', font_size => 10,
                          text_align => 'center');
            /*
            fop.block (formatear (v_creditos_area / v_creditos * 100) || '%', font_size => 10,
                       text_align => 'center');
            */
            else
               fop.block (formatear (0) || '%', font_size => 10, text_align => 'center');
            end if;

            fop.tablecellclose;
            fop.tableRowClose;
            v_asi_ant := det.asignatura_id;
            v_acum_1 := v_acum_1 + det.creditos;
            v_acum_2 := v_acum_2 + v_creditos;
            v_acum_3 := v_acum_3 + v_creditos_Area;
            v_area_1 := v_area_1 + det.creditos;
            v_area_2 := v_area_2 + v_creditos;
            v_area_3 := v_area_3 + v_creditos_Area;

            select porcentaje
            into   v_porcentaje
            from   hor_ext_asignaturas_area
            where  asi_id = det.asignatura_id
            and    uest_id = a.id;
         end loop;

         -- linea 1
         fop.tableRowOpen;
         fop.tablecellopen (number_columns_spanned => 2);
         fop.block (fof.bold ('Total per assignatura'), font_size => 10, space_after => 0.2, text_align => 'right');
         fop.tablecellclose;
         fop.tablecellopen (number_columns_spanned => 2);
         fop.block (fof.white_space, font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold (formatear (v_acum_1)), font_size => 10, text_align => 'center');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold (formatear (v_acum_2)), font_size => 10, text_align => 'center');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold (formatear (v_acum_2 - v_acum_1)), font_size => 10, text_align => 'center');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold (formatear (v_acum_3)), font_size => 10, text_align => 'center');
         fop.tablecellclose;
         fop.tablecellopen;

         if v_acum_1 <> 0 then
            fop.block (fof.bold (formatear (v_acum_3 / v_acum_1 * 100) || '%'), font_size => 10,
                       text_align => 'center');
         else
            fop.block (fof.bold (formatear (0) || '%'), font_size => 10, text_align => 'center');
         end if;

         fop.tablecellclose;
         fop.tableRowClose;
         -- linea 2
         fop.tableRowOpen;
         fop.tablecellopen (number_columns_spanned => 2);
         fop.block (fof.bold ('Crèdits a impartir per l''àrea de coneixement'), font_size => 10, space_after => 0.2,
                    text_align => 'right');
         fop.tablecellclose;
         fop.tablecellopen (number_columns_spanned => 5);
         fop.block (fof.white_space, font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;

         if nvl (v_porcentaje, 0) <> 0 then
            fop.block (fof.bold (formatear (v_acum_1 * v_porcentaje / 100)), font_size => 10, text_align => 'center');
         else
            fop.block (fof.bold (formatear (0)), font_size => 10, text_align => 'center');
         end if;

         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold (formatear (nvl (v_porcentaje, 0)) || '%'), font_size => 10, text_align => 'center');
         fop.tablecellclose;
         fop.tableRowClose;
         -- linea 3
         fop.tableRowOpen;
         fop.tablecellopen (number_columns_spanned => 2);
         fop.block (fof.bold ('Estat de l''assignació docent'), font_size => 10, space_after => 0.2,
                    text_align => 'right');
         fop.tablecellclose;
         fop.tablecellopen (number_columns_spanned => 5);
         fop.block (fof.white_space, font_size => 10);
         fop.tablecellclose;

         if nvl (v_porcentaje, 0) <> 0 then
            fop.tablecellopen;
            fop.block (fof.bold (formatear ((v_acum_1 * v_porcentaje / 100) - v_acum_3)), font_size => 10,
                       text_align => 'center');
            fop.tablecellclose;

            if round (((v_acum_1 * v_porcentaje / 100) - v_acum_3), 2) < 0 then
               fop.tablecellopen;
               fop.block (fof.bold ('Excés de crèdits'), font_size => 10, text_align => 'center');
               fop.tablecellclose;
            elsif round (((v_acum_1 * v_porcentaje / 100) - v_acum_3), 0) > 0 then
               fop.tablecellopen;
               fop.block (fof.bold ('Defecte de crèdits'), font_size => 10, text_align => 'center');
               fop.tablecellclose;
            end if;
         else
            fop.tablecellopen;
            fop.block (fof.bold (formatear (0)), font_size => 10, text_align => 'center');
            fop.tablecellclose;
         end if;

         fop.tableRowClose;
         /*
         fop.tableRowOpen;
         fop.tablecellopen (number_columns_spanned => 2);
         fop.block (fof.bold ('Total per àrea'), font_size => 14, space_before => 0.2, space_after => 0.2,
                    text_align => 'right');
         fop.tablecellclose;
         fop.tablecellopen (number_columns_spanned => 2);
         fop.block (fof.white_space, font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold (formatear (v_area_1)), font_size => 14, text_align => 'center');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold (formatear (v_area_2)), font_size => 14, text_align => 'center');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold (formatear (v_area_2 - v_area_1)), font_size => 14, text_align => 'center');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold (formatear (v_area_3)), font_size => 14, text_align => 'center');
         fop.tablecellclose;
         fop.tablecellopen;

         if nvl (v_area_1, 0) <> 0 then
            fop.block (fof.bold (formatear (v_area_3 / v_area_1 * 100) || '%'), font_size => 10,
                       text_align => 'center');
         else
            fop.block (fof.bold (formatear (0) || '%'), font_size => 10, text_align => 'center');
         end if;

         fop.tablecellclose;
         fop.tableRowClose;
         */
         fop.tablebodyClose;
         fop.tableClose;
         fop.block (fof.white_space, space_before => 0.5);
      end loop;
   end;
BEGIN
   select count (*) + 1
   into   vSesion
   from   apa_sesiones
   where  sesion_id = pSesion
   and    fecha < sysdate () - 0.33;

   select id
   into   vCurso
   from   hor_curso_academico;

   if vSesion > 0 then
      select nombre
      into   v_nombre
      from   hor_Departamentos
      where  id = pDepartamento;

      cabecera;
      fop.block (fof.bold ('----  Informe per assignatura  -----'), font_size => 16, space_after => 0.5,
                 text_align => 'center');
      mostrar_subgrupos;
      --htp.p ('<fo:block break-before="page" color="white">_</fo:block>');
      pie;
   else
      cabecera;
      fop.block ('No tens permís per accedir a aquest document.', font_weight => 'bold', font_size => 14,
                 space_after => 0.5);
      pie;
   end if;
END;

CREATE OR REPLACE PROCEDURE UJI_HORARIOS.pod_consulta_detalle_pdf (
   name_array    IN   euji_util.ident_arr2,
   value_array   IN   euji_util.ident_arr2
) IS
   v_aux           NUMBER;
   pProfesor       number          := euji_util.euji_getparam (name_array, value_array, 'pProfesor');
   pArea           number          := euji_util.euji_getparam (name_array, value_array, 'pArea');
   pDepartamento   number          := euji_util.euji_getparam (name_array, value_array, 'pDepartamento');
   pSesion         VARCHAR2 (400)  := euji_util.euji_getparam (name_array, value_array, 'pSesion');
   v_nombre        varchar2 (2000);
   vSesion         number;
   vCurso          number;
   vCreditos       number;
   vFechas         t_horario;
   vRdo            clob;

   cursor lista_areas (p_area in number, p_prof in number) is
      select   *
          from hor_areas
         where departamento_id = pDepartamento
           and (   id in (select area_id
                            from hor_profesores
                           where id = p_prof)
                or id = p_area
                or p_area = -1)
      order by nombre;

   cursor lista_profesores (p_area in number, p_prof in number) is
      select   id, nombre, creditos, pendiente_contratacion, creditos_maximos, creditos_reduccion, categoria_nombre
          from hor_profesores
         where departamento_id = pDepartamento
           and area_id = p_area
           and (   id = p_prof
                or p_prof = -1)
      order by nombre;

   cursor lista_items (p_prof in number, p_semestre in number, p_horario in varchar2 default 'S') is
      select distinct id, grupo_id, subgrupo_id, tipo_subgrupo, tipo_subgrupo_id, dia_semana_id, hora_inicio, hora_fin,
                      wm_concat (asignatura_id) asignatura_id, fin, inicio, creditos
                 from (select   items.id, items.grupo_id, items.tipo_subgrupo, items.tipo_subgrupo_id,
                                items.subgrupo_id, items.dia_semana_id, items.hora_inicio, items.hora_fin,
                                items.creditos,
                                rtrim (xmlagg (xmlelement (e, asi.asignatura_id || ', ')).extract
                                                                                           ('//text()'),
                                       ', ') as asignatura_id,
                                decode (p_horario,
                                        'S', to_date (dia_semana_id + 1 || '/01/2006 ' || to_char (hora_fin, 'hh24:mi'),
                                                      'dd/mm/yyyy hh24:mi')
                                       ) fin,
                                decode (p_horario,
                                        'S', to_date (dia_semana_id + 1 || '/01/2006 '
                                                      || to_char (hora_inicio, 'hh24:mi'),
                                                      'dd/mm/yyyy hh24:mi')
                                       ) inicio
                           from uji_horarios.hor_items items,
                                uji_horarios.hor_items_asignaturas asi
                          where items.id = asi.item_id
                            and items.semestre_id = p_semestre
                            and (   (    p_horario = 'S'
                                     and items.dia_semana_id is not null)
                                 or (    p_horario = 'N'
                                     and items.dia_semana_id is null)
                                )
                            and items.id in (select item_id
                                               from hor_items_profesores
                                              where profesor_id = p_prof)
                       group by items.id,
                                items.grupo_id,
                                items.tipo_subgrupo,
                                items.tipo_subgrupo_id,
                                items.subgrupo_id,
                                items.dia_semana_id,
                                items.semestre_id,
                                items.hora_inicio,
                                items.hora_fin,
                                asi.asignatura_id,
                                items.creditos)
             group by id,
                      grupo_id,
                      subgrupo_id,
                      tipo_subgrupo,
                      tipo_subgrupo_id,
                      dia_semana_id,
                      hora_inicio,
                      hora_fin,
                      fin,
                      inicio,
                      creditos;

   cursor lista_items_planificados (p_prof in number, p_semestre in number, p_horario in varchar2) is
      select   *
          from (select   asignatura_id, grupo_id, tipo_subgrupo, tipo_subgrupo_id, subgrupo_id,
                         min (nvl (dia_Semana_id, 9999)) dia_semana_id, nvl (crd_profesor, creditos / cuantos) creditos,
                         cuantos
                    from (select   items.id, items.grupo_id, items.tipo_subgrupo, items.tipo_subgrupo_id,
                                   items.subgrupo_id, items.dia_semana_id, items.hora_inicio, items.hora_fin,
                                   prof.creditos crd_profesor, items.creditos,
                                   nvl (comun_Texto, asignatura_id) asignatura_id,
                                   (select count (*)
                                      from hor_items_profesores prof2
                                     where items.id = prof2.item_id) cuantos
                              from uji_horarios.hor_items items,
                                   uji_horarios.hor_items_asignaturas asi,
                                   uji_horarios.hor_items_profesores prof
                             where items.id = asi.item_id
                               and items.id = prof.item_id
                               and prof.profesor_id = p_prof
                               and items.semestre_id = p_semestre
                          group by items.id,
                                   items.grupo_id,
                                   items.tipo_subgrupo,
                                   items.tipo_subgrupo_id,
                                   items.subgrupo_id,
                                   items.dia_semana_id,
                                   items.semestre_id,
                                   items.hora_inicio,
                                   items.hora_fin,
                                   nvl (comun_Texto, asignatura_id),
                                   prof.creditos,
                                   items.creditos)
                group by asignatura_id,
                         grupo_id,
                         tipo_subgrupo,
                         tipo_subgrupo_id,
                         subgrupo_id,
                         nvl (crd_profesor, creditos / cuantos),
                         cuantos)
         where (    p_horario = 'S'
                and dia_Semana_id < 9999)
            or (    p_horario = 'N'
                and dia_Semana_id = 9999)
      order by asignatura_id,
               grupo_id,
               decode (tipo_subgrupo_id, 'TE', 1, 'PR', 2, 'LA', 3, 'SE', 4, 'TU', 5, 6),
               subgrupo_id;

   cursor lista_items_crd (p_prof in number, p_sem in number) is
      /*select   *
      from     (select   grupo_id, tipo_subgrupo, tipo_subgrupo_id, subgrupo_id, asignatura_id, creditos
                from     (select distinct items.grupo_id, items.tipo_subgrupo, items.tipo_subgrupo_id,
                                          items.subgrupo_id, nvl (items.comun_texto, asi.asignatura_id) asignatura_id,
                                          creditos
                          from            uji_horarios.hor_items items,
                                          uji_horarios.hor_items_asignaturas asi
                          where           items.id = asi.item_id
                          and             items.dia_semana_id is not null
                          and             items.semestre_id = p_sem
                          and             items.id in (select item_id
                                                       from   hor_items_profesores
                                                       where  profesor_id = p_prof)
                          group by        items.grupo_id,
                                          items.tipo_subgrupo,
                                          items.tipo_subgrupo_id,
                                          items.subgrupo_id,
                                          nvl (items.comun_texto, asi.asignatura_id),
                                          creditos)
                group by grupo_id,
                         tipo_subgrupo,
                         tipo_subgrupo_id,
                         subgrupo_id,
                         creditos,
                         asignatura_id)
      order by 5,
               1,
               2,
               3,
               4;*/
      select distinct grupo_id, substr (tipo, 1, 2) tipo_subgrupo_id, substr (tipo, 3) subgrupo_id,
                      asignatura_txt asignatura_id, crd_final creditos, departamento_id, area_id
                 from hor_v_items_creditos_detalle
                where id = p_prof
                  and item_id in (select id
                                    from hor_items
                                   where semestre_id = p_sem)
             order by 5,
                      1,
                      2,
                      3,
                      4;

   cursor lista_solapamientos (p_prof in number, p_semestre in number) is
      select   id, min (asignatura_id) asignatura_id, grupo_id, tipo_subgrupo_id || subgrupo_id subgrupo_id,
               semestre_id, dia_semana_id,
               decode (dia_semana_id,
                       1, 'Dilluns',
                       2, 'Dimarts',
                       3, 'Dimecres',
                       4, 'Dijous',
                       5, 'Divendres',
                       6, 'Dissabte',
                       7, 'Diumenge',
                       'Error'
                      ) dia_semana,
               min (hora_inicio) inicio, max (hora_fin) fin
          from (select   id, grupo_id, subgrupo_id, tipo_subgrupo, tipo_subgrupo_id, dia_semana_id, hora_inicio,
                         hora_fin, wm_concat (asignatura_id) asignatura_id, semestre_id,
                         count (*) over (partition by semestre_id, dia_semana_id) cuantos
                    from (select id, asignatura_id, semestre_id, grupo_id, tipo_subgrupo, tipo_subgrupo_id, subgrupo_id,
                                 dia_semana_id, hora_inicio, hora_fin
                            from (select   items.id, items.grupo_id, items.tipo_subgrupo, items.tipo_subgrupo_id,
                                           items.subgrupo_id, items.dia_semana_id, items.hora_inicio, items.hora_fin,
                                           items.semestre_id, wm_concat (asi.asignatura_id) asignatura_id
                                      from uji_horarios.hor_items items,
                                           uji_horarios.hor_items_asignaturas asi
                                     where items.id = asi.item_id
                                       and items.semestre_id = p_semestre
                                       and items.dia_semana_id is not null
                                       and items.id in (select item_id
                                                          from hor_items_profesores
                                                         where profesor_id = p_prof)
                                  group by asi.asignatura_id,
                                           items.id,
                                           items.grupo_id,
                                           items.tipo_subgrupo,
                                           items.tipo_subgrupo_id,
                                           items.subgrupo_id,
                                           items.dia_semana_id,
                                           items.semestre_id,
                                           items.hora_inicio,
                                           items.hora_fin) x
                           where exists (
                                    select 1
                                      from hor_items i,
                                           hor_items_profesores p
                                     where i.id = p.item_id
                                       and profesor_id = p_prof
                                       and i.id <> x.id
                                       and i.dia_semana_id = x.dia_Semana_id
                                       and i.semestre_id = x.semestre_id
                                       and (   to_number (to_char ((i.hora_inicio + 1 / 1440), 'hh24mi'))
                                                  between to_number (to_char (x.hora_inicio, 'hh24mi'))
                                                      and to_number (to_char (x.hora_fin, 'hh24mi'))
                                            or to_number (to_char ((i.hora_fin - 1 / 1440), 'hh24mi'))
                                                  between to_number (to_char (x.hora_inicio, 'hh24mi'))
                                                      and to_number (to_char (x.hora_fin, 'hh24mi'))
                                           )))
                group by id,
                         grupo_id,
                         subgrupo_id,
                         tipo_subgrupo,
                         tipo_subgrupo_id,
                         dia_semana_id,
                         semestre_id,
                         hora_inicio,
                         hora_fin)
         where cuantos > 1
      group by id,
               semestre_id,
               dia_Semana_id,
               grupo_id,
               tipo_subgrupo_id || subgrupo_id
      order by 5,
               6,
               2,
               3,
               4,
               6;

   function redondea_fecha_a_cuartos (vIniDate date)
      return date is
      fecha_redondeada   date;
   begin
      select trunc (vIniDate, 'HH') + (15 * round (to_char (trunc (vIniDate, 'MI'), 'MI') / 15)) / 1440
        into fecha_redondeada
        from dual;

      return fecha_redondeada;
   end;

   procedure cabecera is
   begin
      owa_util.mime_header (ccontent_type => 'text/xml', bclose_header => true);
      fop.documentOpen (page_width => 29.7, page_height => 21, margin_left => 1.5, margin_right => 1.5,
                        margin_top => 0.5, margin_bottom => 0.5, extent_before => 3, extent_after => 1.5,
                        extent_margin_top => 3, extent_margin_bottom => 1.5);
      fop.pageBodyOpen;
      htp.p (fof.documentheaderopen || fof.blockopen (space_after => 0.1) || fof.tableOpen
             || fof.tablecolumndefinition (column_width => 3) || fof.tablecolumndefinition (column_width => 24)
             || fof.tablebodyopen || fof.tableRowOpen || fof.tablecellopen
             || fof.block (fof.logo_uji, text_align => 'center') || fof.tablecellclose
             || fof.tablecellopen (display_align => 'center', padding => 0.4)
             || fof.block (text => v_nombre, font_size => 16, font_weight => 'bold', font_family => 'Arial',
                           text_align => 'center')
             || fof.block (text => 'POD provisional - Curs: ' || vCurso || '/' || substr (to_char (vCurso + 1), 3),
                           font_size => 16, font_weight => 'bold', font_family => 'Arial', text_align => 'center')
             || fof.tablecellclose || fof.tableRowClose || fof.tablebodyclose || fof.tableClose || fof.blockclose
             || fof.documentheaderclose);
      -- El numero paginas se escribe a mano para poner el texto m?s peque?o
      htp.p (fof.documentfooteropen || fof.tableOpen || fof.tablecolumndefinition (column_width => 18)
             || fof.tablebodyopen || fof.tableRowOpen || fof.tablecellopen
             || fof.block (text => 'Pag. <fo:page-number /> de <fo:page-number-citation ref-id="lastBlock" />',
                           text_align => 'right', font_weight => 'bold', font_size => 8)
             || fof.tablecellclose || fof.tableRowClose || fof.tablebodyclose || fof.tableClose
             || fof.documentfooterclose);
      fop.pageFlowOpen;
      fop.block (fof.white_space);
   end;

   procedure pie is
   begin
      htp.p ('<fo:block  id="lastBlock" space-before="1cm" font-size="8pt">');
      htp.p ('Data de generació: ' || to_char (sysdate, 'dd/mm/yyy hh24:mi'));
      fop.blockclose ();
      fop.pageFlowClose;
      fop.pageBodyClose;
      fop.documentClose;
   end;

   function formatear (p_entrada in number)
      return varchar2 is
      v_rdo   varchar2 (100);
   begin
      v_rdo := replace (to_char (p_entrada, '9990.00'), '.', ',');
      return (v_rdo);
   end;

   function espacios (p_espacios in number)
      return varchar2 is
      v_rdo   varchar2 (2000);
   begin
      v_rdo := null;

      for x in 1 .. p_espacios loop
         v_rdo := v_rdo || fof.white_space;
      end loop;

      return v_rdo;
   end;

   procedure mostrar_calendario (p_prof in number, p_semestre in number) is
      v_aux   number;
   begin
      vFechas := t_horario ();

      for item in lista_items (p_prof, p_semestre, 'S') loop
         vFechas.extend;
         vFechas (vFechas.count) :=
            ItemHorarios (hor_contenido_item (item.hora_inicio, item.hora_fin,
                                              item.asignatura_id || ' ' || item.grupo_id,
                                              '(' || item.tipo_subgrupo_id || item.subgrupo_id || ')'),
                          redondea_fecha_a_cuartos (item.inicio), redondea_fecha_a_cuartos (item.fin), null);
      end loop;

      if vFechas.count > 0 then
         fop.block (fof.white_space);
         fop.block (fof.white_space);
         fop.block (fof.bold ('Semestre ' || p_semestre) || '  _____________________________', font_size => 12);
         vRdo := xpfpkg_horarios_academicos.drawTimetable (vFechas, 5, null, 'CA', 'PDF_APP_HORARIOS');
         xup_utils.dump (vRdo);
         dbms_lob.freeTemporary (vRdo);
         --fop.block (fof.white_space);
         fop.block (fof.white_space);
      end if;
   --fop.block (fof.white_space);
   end;

   procedure mostrar_pod (p_prof in number, p_semestre in number) is
      v_aux   number;
   begin
      begin
         v_aux := 0;

         --for item in lista_items_crd (p_prof, p_semestre) loop
         for item in lista_items_planificados (p_prof, p_semestre, 'S') loop
            v_aux := v_aux + 1;

            if v_aux = 1 then
               fop.block (fof.white_space);
               fop.block (espacios (8) || fof.bold ('Subgrups amb horari assignat al semestre ' || p_semestre),
                          font_size => 12);
            end if;

            fop.block (espacios (8) || espacios (5) || item.asignatura_id || ' ' || item.grupo_id || ' '
                       || item.tipo_subgrupo_id || item.subgrupo_id || '  ' || formatear (item.creditos) || ' crd.',
                       font_size => 10);
         end loop;
      exception
         when others then
            fop.block (sqlerrm);
      end;

      begin
         v_aux := 0;

         for item in lista_items_planificados (p_prof, p_semestre, 'N') loop
            v_aux := v_aux + 1;

            if v_aux = 1 then
               fop.block (fof.white_space);
               fop.block (espacios (8) || fof.bold ('Subgrups sense horari assignat al semestre ' || p_semestre),
                          font_size => 12);
            end if;

            fop.block (espacios (8) || espacios (5) || item.asignatura_id || ' ' || item.grupo_id || ' '
                       || item.tipo_subgrupo_id || item.subgrupo_id || '  ' || formatear (item.creditos) || ' crd.',
                       font_size => 10);
         end loop;
      exception
         when others then
            fop.block (sqlerrm);
      end;
   end;

   procedure mostrar_solapaments (p_prof in number, p_semestre in number) is
   begin
      begin
         v_aux := 0;

         for item in lista_solapamientos (p_prof, p_semestre) loop
            v_aux := v_aux + 1;

            if v_aux = 1 then
               fop.block (fof.white_space);
               fop.block (espacios (8) || fof.bold ('Solapaments al semestre ' || p_semestre), font_size => 12);
            end if;

            fop.block (espacios (8) || espacios (5) || item.dia_Semana || ' - ' || to_char (item.inicio, 'hh24:mi')
                       || '-' || to_char (item.fin, 'hh24:mi') || ' - ' || item.asignatura_id || ' ' || item.grupo_id
                       || ' ' || item.subgrupo_id,
                       font_size => 10);
         end loop;
      exception
         when others then
            fop.block (sqlerrm);
      end;
   end;
BEGIN
   select count (*) + 1
     into vSesion
     from apa_sesiones
    where sesion_id = pSesion
      and fecha < sysdate () - 0.33;

   select id
     into vCurso
     from hor_curso_academico;

   if vSesion > 0 then
      begin
         select nombre
           into v_nombre
           from hor_Departamentos
          where id = pDepartamento;

         cabecera;

         if pArea = -1 then
            pProfesor := -1;
         end if;

         for a in lista_areas (pArea, pProfesor) loop
            fop.block ('Area de coneixement: ' || fof.bold (a.nombre), font_size => 16);
            fop.block (fof.white_space);

            for x in lista_profesores (a.id, pProfesor) loop
               begin
                  select nvl (sum (creditos), 0)
                    into vCreditos
                    from (select   grupo_id, tipo_subgrupo, tipo_subgrupo_id, subgrupo_id,
                                   wm_concat (asignatura_id) asignatura_id, creditos
                              from (select distinct items.grupo_id, items.tipo_subgrupo, items.tipo_subgrupo_id,
                                                    items.subgrupo_id, asi.asignatura_id, creditos
                                               from uji_horarios.hor_items items,
                                                    uji_horarios.hor_items_asignaturas asi
                                              where items.id = asi.item_id
                                                --and             items.dia_semana_id is not null
                                                and items.id in (select item_id
                                                                   from hor_items_profesores
                                                                  where profesor_id = x.id)
                                           group by items.grupo_id,
                                                    items.tipo_subgrupo,
                                                    items.tipo_subgrupo_id,
                                                    items.subgrupo_id,
                                                    asi.asignatura_id,
                                                    creditos)
                          group by grupo_id,
                                   tipo_subgrupo,
                                   tipo_subgrupo_id,
                                   subgrupo_id,
                                   creditos);

                  select nvl (sum (creditos), 0)
                    into vCreditos
                    from (select distinct d.id, nvl (comun_Texto, d.asignatura_id) asignatura_id, d.grupo_id,
                                          tipo_subgrupo_id, subgrupo_id, crd_final creditos
                                     from hor_v_items_creditos_detalle d,
                                          hor_items i,
                                          hor_items_asignaturas a
                                    where d.id = x.id
                                      and d.item_id = i.id
                                      and i.id = a.item_id);
               exception
                  when others then
                     vCreditos := 0;
               end;

               fop.block (espacios (3) || fof.bold (x.nombre) || ' - ' || x.categoria_nombre, space_before => 1,
                          font_size => 14);
               fop.tableOpen;
               fop.tablecolumndefinition (column_width => 2);
               fop.tablecolumndefinition (column_width => 3);
               fop.tablecolumndefinition (column_width => 3);
               fop.tablecolumndefinition (column_width => 3);
               fop.tablecolumndefinition (column_width => 3);
               fop.tablecolumndefinition (column_width => 5);
               fop.tablecolumndefinition (column_width => 5);
               fop.tablebodyopen;
               fop.tableRowOpen;
               fop.tablecellopen;
               fop.block (fof.white_space, text_align => 'center');
               fop.tablecellclose;
               fop.tablecellopen;
               fop.block ('Dedicació inicial', text_align => 'center');
               fop.tablecellclose;
               fop.tablecellopen;
               fop.block ('Reducció', text_align => 'center');
               fop.tablecellclose;
               fop.tablecellopen;
               fop.block ('Dedicació neta', text_align => 'center');
               fop.tablecellclose;
               fop.tablecellopen;
               fop.block ('Crèdits a impartir', text_align => 'center');
               fop.tablecellclose;
               fop.tablecellopen;
               fop.block ('% d''assignació docent', text_align => 'center');
               fop.tablecellclose;
               fop.tablecellopen;
               fop.block ('Crèdits pendents d''assignar', text_align => 'center');
               fop.tablecellclose;
               fop.tableRowClose;
               fop.tableRowOpen;
               fop.tablecellopen;
               fop.block (fof.white_space, text_align => 'center');
               fop.tablecellclose;
               fop.tablecellopen;
               fop.block (formatear (x.creditos_maximos) || ' crd', text_align => 'center');
               fop.tablecellclose;
               fop.tablecellopen;
               fop.block (formatear (x.creditos_Reduccion) || ' crd', text_align => 'center');
               fop.tablecellclose;
               fop.tablecellopen;
               fop.block (fof.bold (formatear (x.creditos)) || ' crd', text_align => 'center');
               fop.tablecellclose;
               fop.tablecellopen;
               fop.block (formatear (vCreditos) || ' crd', text_align => 'center');
               fop.tablecellclose;
               fop.tablecellopen;

               if x.creditos <> 0 then
                  fop.block (formatear (round (vCreditos / x.creditos * 100, 2)) || '%', text_align => 'center');
               else
                  fop.block (formatear (0) || '%', text_align => 'center');
               end if;

               fop.tablecellclose;
               fop.tablecellopen;
               fop.block (fof.bold (formatear (x.creditos - vCreditos)) || ' crd.', text_align => 'center');
               fop.tablecellclose;
               fop.tableRowClose;
               fop.tablebodyclose;
               fop.tableClose;

               /*
               fop.block (espacios (6) || ' ' || x.creditos_maximos || ' crd' || espacios (4) || 'Reducció '
                          || x.creditos_Reduccion || ' crd' || espacios (4) || 'Dedicació neta '
                          || fof.bold (x.creditos) || ' crd' || espacios (4) || 'Crèdits a impartir ' || vCreditos
                          || ' crd' || espacios (4) || 'Crèdits pendents d''assignar '
                          || fof.bold (x.creditos - vCreditos) || ' crd.',
                          font_size => 10);
                          */
               for semestre in 1 .. 2 loop
                  begin
                     mostrar_calendario (x.id, semestre);
                     mostrar_pod (x.id, semestre);
                     mostrar_solapaments (x.id, semestre);
                     null;
                  exception
                     when others then
                        fop.block (sqlerrm);
                  end;
               end loop;
            end loop;
         end loop;

         pie;
      exception
         when others then
            --cabecera;
            fop.block ('Error en el departament. ' || sqlerrm, font_weight => 'bold', text_align => 'center',
                       font_size => 14, space_after => 0.5);
            pie;
      end;
   else
      cabecera;
      fop.block ('No tens permís per accedir a aquest document.', font_weight => 'bold', text_align => 'center',
                 font_size => 14, space_after => 0.5);
      pie;
   end if;
END;

CREATE OR REPLACE PROCEDURE UJI_HORARIOS.pod_consulta_pdf (
   name_array    IN   euji_util.ident_arr2,
   value_array   IN   euji_util.ident_arr2
) IS
   v_aux           NUMBER;
   -- pProfesor       number          := euji_util.euji_getparam (name_array, value_array, 'pProfesor');
   pDepartamento   number          := euji_util.euji_getparam (name_array, value_array, 'pDepartamento');
   pSesion         VARCHAR2 (400)  := euji_util.euji_getparam (name_array, value_array, 'pSesion');
   v_nombre        varchar2 (2000);
   vSesion         number;
   vCurso          number;
   vCreditos       number;
   vAcum           number;
   vAcum2          number;
   vAcum3          number;
   vAcum4          number;
   vAcum5          number;
   vDepAcum        number;
   vDepAcum2       number;
   vDepAcum3       number;
   vDepAcum4       number;
   vDepAcum5       number;
   vFechas         t_horario;
   vRdo            clob;

   cursor lista_areas is
      select   *
      from     hor_areas
      where    departamento_id = pDepartamento
      order by nombre;

   cursor lista_profesores (p_area in number) is
      select   id, nombre, nvl (creditos, 0) creditos, pendiente_contratacion,
               nvl (creditos_maximos, 0) creditos_maximos, nvl (creditos_reduccion, 0) creditos_Reduccion,
               categoria_nombre
      from     hor_profesores
      where    departamento_id = pDepartamento
      and      area_id = p_area
      order by nombre,
               categoria_nombre,
               nvl (creditos_maximos, 0) desc,
               nombre;

   function formatear (p_entrada in number)
      return varchar2 is
      v_rdo   varchar2 (100);
   begin
      v_rdo := replace (to_char (p_entrada, '9990.00'), '.', ',');
      return (v_rdo);
   end;

   procedure cabecera is
   begin
      owa_util.mime_header (ccontent_type => 'text/xml', bclose_header => true);
      fop.documentOpen (page_width => 29.7, page_height => 21, margin_left => 1.5, margin_right => 1.5,
                        margin_top => 0.5, margin_bottom => 0.5, extent_before => 3, extent_after => 1.5,
                        extent_margin_top => 3, extent_margin_bottom => 1.5);
      fop.pageBodyOpen;
      htp.p (fof.documentheaderopen || fof.blockopen (space_after => 0.1) || fof.tableOpen
             || fof.tablecolumndefinition (column_width => 3) || fof.tablecolumndefinition (column_width => 24)
             || fof.tablebodyopen || fof.tableRowOpen || fof.tablecellopen
             || fof.block (fof.logo_uji, text_align => 'center') || fof.tablecellclose
             || fof.tablecellopen (display_align => 'center', padding => 0.4)
             || fof.block (text => v_nombre, font_size => 16, font_weight => 'bold', font_family => 'Arial',
                           text_align => 'center')
             || fof.block (text => 'Informe provisional POD - Curs: ' || vCurso || '/'
                            || substr (to_char (vCurso + 1), 3),
                           font_size => 16, font_weight => 'bold', font_family => 'Arial', text_align => 'center')
             || fof.tablecellclose || fof.tableRowClose || fof.tablebodyclose || fof.tableClose || fof.blockclose
             || fof.documentheaderclose);
      -- El numero paginas se escribe a mano para poner el texto m?s peque?o
      htp.p (fof.documentfooteropen || fof.tableOpen || fof.tablecolumndefinition (column_width => 18)
             || fof.tablebodyopen || fof.tableRowOpen || fof.tablecellopen
             || fof.block (text => 'Pag. <fo:page-number /> de <fo:page-number-citation ref-id="lastBlock" />',
                           text_align => 'right', font_weight => 'bold', font_size => 8)
             || fof.tablecellclose || fof.tableRowClose || fof.tablebodyclose || fof.tableClose
             || fof.documentfooterclose);
      fop.pageFlowOpen;
      fop.block (fof.white_space);
   end;

   procedure pie is
   begin
      htp.p ('<fo:block  id="lastBlock" space-before="1cm" font-size="8pt">');
      htp.p ('Data de generació: ' || to_char (sysdate, 'dd/mm/yyy hh24:mi'));
      fop.blockclose ();
      fop.pageFlowClose;
      fop.pageBodyClose;
      fop.documentClose;
   end;

   function espacios (p_espacios in number)
      return varchar2 is
      v_rdo   varchar2 (2000);
   begin
      v_rdo := null;

      for x in 1 .. p_espacios loop
         v_rdo := v_rdo || fof.white_space;
      end loop;

      return v_rdo;
   end;

   procedure mostrar_por_profesor is
   begin
      vDepAcum := 0;
      vDepacum2 := 0;
      vDepacum3 := 0;
      vDepacum4 := 0;
      vDepacum5 := 0;

      for a in lista_areas loop
         fop.block ('Area de coneixement: ' || fof.bold (a.nombre), font_size => 16);
         -- fop.block (fof.white_space);
         fop.tableOpen;
         fop.tablecolumndefinition (column_width => 6.5);
         fop.tablecolumndefinition (column_width => 7);
         fop.tablecolumndefinition (column_width => 2);
         fop.tablecolumndefinition (column_width => 2);
         fop.tablecolumndefinition (column_width => 2);
         fop.tablecolumndefinition (column_width => 2);
         fop.tablecolumndefinition (column_width => 2);
         fop.tablecolumndefinition (column_width => 2);
         fop.tablecolumndefinition (column_width => 2);
         fop.tablebodyopen;
         fop.tableRowOpen;
         fop.tablecellopen;
         fop.block (fof.bold ('Profesor/a'), font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold ('Categoria'), font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold ('Dedicació inicial'), font_size => 10, text_align => 'right');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold ('Reducció'), font_size => 10, text_align => 'right');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold ('Dedicació neta'), font_size => 10, text_align => 'right');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold ('Crèdits assignats'), font_size => 10, text_align => 'right');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold ('% assig- nació'), font_size => 10, text_align => 'right');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold ('Crèdits per assignar'), font_size => 10, text_align => 'right');
         fop.tablecellclose;
         fop.tableRowClose;
         vacum := 0;
         vacum2 := 0;
         vacum3 := 0;
         vacum4 := 0;
         vacum5 := 0;

         for x in lista_profesores (a.id) loop
            begin
               select nvl (sum (creditos), 0)
               into   vCreditos
               from   (select   grupo_id, tipo_subgrupo, tipo_subgrupo_id, subgrupo_id,
                                wm_concat (asignatura_id) asignatura_id, creditos
                       from     (select distinct items.grupo_id, items.tipo_subgrupo, items.tipo_subgrupo_id,
                                                 items.subgrupo_id, asi.asignatura_id, creditos
                                 from            uji_horarios.hor_items items,
                                                 uji_horarios.hor_items_asignaturas asi
                                 where           items.id = asi.item_id
                                 --and             items.dia_semana_id is not null
                                 and             items.id in (select item_id
                                                              from   hor_items_profesores
                                                              where  profesor_id = x.id)
                                 group by        items.grupo_id,
                                                 items.tipo_subgrupo,
                                                 items.tipo_subgrupo_id,
                                                 items.subgrupo_id,
                                                 asi.asignatura_id,
                                                 creditos)
                       group by grupo_id,
                                tipo_subgrupo,
                                tipo_subgrupo_id,
                                subgrupo_id,
                                creditos);

               select nvl (sum (creditos), 0)
               into   vCreditos
               from   (select distinct d.id, nvl (comun_Texto, d.asignatura_id) asignatura_id, d.grupo_id,
                                       tipo_subgrupo_id, subgrupo_id, crd_final creditos
                       from            hor_v_items_creditos_detalle d,
                                       hor_items i,
                                       hor_items_asignaturas a
                       where           d.id = x.id
                       and             d.item_id = i.id
                       and             i.id = a.item_id);
            exception
               when others then
                  vCreditos := 0;
            end;

            fop.tableRowOpen;
            fop.tablecellopen;
            fop.block (x.nombre, font_size => 10);
            fop.tablecellclose;
            fop.tablecellopen;
            fop.block (x.categoria_nombre, font_size => 10);
            fop.tablecellclose;
            fop.tablecellopen;
            fop.block (formatear (x.creditos_maximos), font_size => 10, text_align => 'right');
            fop.tablecellclose;
            fop.tablecellopen;
            fop.block (formatear (x.creditos_reduccion), font_size => 10, text_align => 'right');
            fop.tablecellclose;
            fop.tablecellopen;
            fop.block (formatear (x.creditos), font_size => 10, text_align => 'right');
            fop.tablecellclose;
            fop.tablecellopen;
            fop.block (formatear (vCreditos), font_size => 10, text_align => 'right');
            fop.tablecellclose;
            fop.tablecellopen;

            if x.creditos <> 0 then
               fop.block (formatear (round (vCreditos / x.creditos * 100, 2)) || '%', font_size => 10,
                          text_align => 'right');
            else
               fop.block ('0,00%', font_size => 10, text_align => 'right');
            end if;

            fop.tablecellclose;
            fop.tablecellopen;
            fop.block (formatear (x.creditos - vCreditos), font_size => 10, text_align => 'right');
            fop.tablecellclose;

            if (x.creditos - vCreditos) < 0 then
               fop.tablecellopen;
               fop.block (fof.bold ('> 100% dedicació'), font_size => 10, text_align => 'center');
               fop.tablecellclose;
            end if;

            fop.tableRowClose;
            vacum := vacum + x.creditos;
            vacum2 := vacum2 + vCreditos;
            vacum3 := vacum3 + (x.creditos - vCreditos);
            vacum4 := vacum4 + x.creditos_maximos;
            vacum5 := vacum5 + x.creditos_reduccion;
            vDepacum := vDepacum + x.creditos;
            vDepacum2 := vDepacum2 + vCreditos;
            vDepacum3 := vDepacum3 + (x.creditos - vCreditos);
            vDepacum4 := vDepacum4 + x.creditos_maximos;
            vDepacum5 := vDepacum5 + x.creditos_reduccion;
         end loop;

         fop.tableRowOpen;
         fop.tablecellopen;
         fop.block (fof.white_space, font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.white_space, font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold (formatear (vAcum4)), font_size => 10, text_align => 'right');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold (formatear (vAcum5)), font_size => 10, text_align => 'right');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold (formatear (vAcum)), font_size => 10, text_align => 'right');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold (formatear (vAcum2)), font_size => 10, text_align => 'right');
         fop.tablecellclose;
         fop.tablecellopen;

         if vAcum > 0 then
            fop.block (fof.bold (formatear (round (vAcum2 / vAcum * 100, 2))) || '%', font_size => 10,
                       text_align => 'right');
         else
            fop.block (fof.white_space (), font_size => 10, text_align => 'right');
         end if;

         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold (formatear (vAcum3)), font_size => 10, text_align => 'right');
         fop.tablecellclose;
         fop.tableRowClose;
         fop.tablebodyclose;
         fop.tableClose;
         fop.block (fof.white_space);
         fop.block (fof.white_space);
         fop.block (fof.white_space);
      end loop;

      fop.tableOpen;
      fop.tablecolumndefinition (column_width => 6.5);
      fop.tablecolumndefinition (column_width => 7);
      fop.tablecolumndefinition (column_width => 2);
      fop.tablecolumndefinition (column_width => 2);
      fop.tablecolumndefinition (column_width => 2);
      fop.tablecolumndefinition (column_width => 2);
      fop.tablecolumndefinition (column_width => 2);
      fop.tablecolumndefinition (column_width => 2);
      fop.tablecolumndefinition (column_width => 2);
      fop.tablebodyopen;
      fop.tableRowOpen;
      fop.tablecellopen;
      fop.block (fof.bold ('Total del departament'), font_size => 14);
      fop.tablecellclose;
      fop.tablecellopen;
      fop.block (fof.white_space, font_size => 14);
      fop.tablecellclose;
      fop.tablecellopen;
      fop.block (fof.bold (formatear (vDepAcum4)), font_size => 14, text_align => 'right');
      fop.tablecellclose;
      fop.tablecellopen;
      fop.block (fof.bold (formatear (vDepAcum5)), font_size => 14, text_align => 'right');
      fop.tablecellclose;
      fop.tablecellopen;
      fop.block (fof.bold (formatear (vDepAcum)), font_size => 14, text_align => 'right');
      fop.tablecellclose;
      fop.tablecellopen;
      fop.block (fof.bold (formatear (vDepAcum2)), font_size => 14, text_align => 'right');
      fop.tablecellclose;
      fop.tablecellopen;

      if vDepAcum > 0 then
         fop.block (fof.bold (formatear (round (vDepAcum2 / vDepAcum * 100, 2))) || '%', font_size => 14,
                    text_align => 'right');
      else
         fop.block (fof.bold ('ERROR'), font_size => 14, text_align => 'right');
      end if;

      fop.tablecellclose;
      fop.tablecellopen;
      fop.block (fof.bold (formatear (vDepAcum3)), font_size => 14, text_align => 'right');
      fop.tablecellclose;
      fop.tableRowClose;
      fop.tablebodyclose;
      fop.tableClose;
   end;
BEGIN
   select count (*) + 1
   into   vSesion
   from   apa_sesiones
   where  sesion_id = pSesion
   and    fecha < sysdate () - 0.33;

   select id
   into   vCurso
   from   hor_curso_academico;

   if vSesion > 0 then
      select nombre
      into   v_nombre
      from   hor_Departamentos
      where  id = pDepartamento;

      cabecera;
      fop.block (fof.bold ('----  Informe per professor  -----'), font_size => 16, space_after => 0.5,
                 text_align => 'center');
      mostrar_por_profesor;
      pie;
   else
      cabecera;
      fop.block ('No tens permís per accedir a aquest document.', font_weight => 'bold', font_size => 14,
                 space_after => 0.5);
      pie;
   end if;
exception
   when others then
      htp.p (sqlerrm);
END;

CREATE OR REPLACE PROCEDURE UJI_HORARIOS.pod_validacion_pdf (
   name_array    IN   euji_util.ident_arr2,
   value_array   IN   euji_util.ident_arr2
) IS
   v_aux           NUMBER;
   pDepartamento   number          := euji_util.euji_getparam (name_array, value_array, 'pDepartamento');
   pSesion         VARCHAR2 (400)  := euji_util.euji_getparam (name_array, value_array, 'pSesion');
   v_nombre        varchar2 (2000);
   vSesion         number;
   vCurso          number;
   pProfesor       number;

   cursor lista_areas (p_departamento in number) is
      select   *
      from     hor_areas
      where    departamento_id = p_Departamento
      order by nombre;

   cursor lista_profesores (p_departamento in number, p_area in number) is
      select   p.id, p.nombre, creditos, pendiente_contratacion, creditos_maximos, creditos_reduccion, categoria_nombre,
               a.nombre nombre_area
      from     hor_profesores p,
               hor_areas a
      where    p.departamento_id = p_Departamento
      and      area_id = p_area
      and      area_id = a.id
      order by nombre;

   procedure cabecera is
   begin
      begin
         select nombre
         into   v_nombre
         from   hor_Departamentos
         where  id = pDepartamento;
      exception
         when no_data_found then
            v_nombre := 'Desconegut';
         when others then
            v_nombre := sqlerrm;
      end;

      owa_util.mime_header (ccontent_type => 'text/xml', bclose_header => true);
      fop.documentOpen (margin_left => 1.5, margin_right => 1.5, margin_top => 0.5, margin_bottom => 0.5,
                        extent_before => 3, extent_after => 1.5, extent_margin_top => 3, extent_margin_bottom => 1.5);
      fop.pageBodyOpen;
      htp.p (fof.documentheaderopen || fof.blockopen (space_after => 0.1) || fof.tableOpen
             || fof.tablecolumndefinition (column_width => 3) || fof.tablecolumndefinition (column_width => 15)
             || fof.tablebodyopen || fof.tableRowOpen || fof.tablecellopen
             || fof.block (fof.logo_uji, text_align => 'center') || fof.tablecellclose
             || fof.tablecellopen (display_align => 'center', padding => 0.4)
             || fof.block (text => v_nombre, font_size => 16, font_weight => 'bold', font_family => 'Arial',
                           text_align => 'center')
             || fof.block (text => 'Validació POD - Curs: ' || vCurso || '/' || substr (to_char (vCurso + 1), 3),
                           font_size => 16, font_weight => 'bold', font_family => 'Arial', text_align => 'center')
             || fof.tablecellclose || fof.tableRowClose || fof.tablebodyclose || fof.tableClose || fof.blockclose
             || fof.documentheaderclose);
      -- El numero paginas se escribe a mano para poner el texto m?s peque?o
      htp.p (fof.documentfooteropen || fof.tableOpen || fof.tablecolumndefinition (column_width => 18)
             || fof.tablebodyopen || fof.tableRowOpen || fof.tablecellopen
             || fof.block (text => 'Pag. <fo:page-number /> de <fo:page-number-citation ref-id="lastBlock" />',
                           text_align => 'right', font_weight => 'bold', font_size => 8)
             || fof.tablecellclose || fof.tableRowClose || fof.tablebodyclose || fof.tableClose
             || fof.documentfooterclose);
      fop.pageFlowOpen;
      fop.block (fof.white_space);
   end;

   procedure pie is
   begin
      htp.p ('<fo:block  id="lastBlock" space-before="1cm" font-size="8pt">');
      htp.p ('Data de generació: ' || to_char (sysdate, 'dd/mm/yyy hh24:mi'));
      fop.blockclose ();
      fop.pageFlowClose;
      fop.pageBodyClose;
      fop.documentClose;
   end;

   function italic (p_entrada in varchar2)
      return varchar2 is
      v_rdo   varchar2 (2000);
   begin
      v_rdo := '<fo:inline font-style="italic">' || p_entrada || '</fo:inline>';
      return (v_rdo);
   end;

   function espacios (p_espacios in number)
      return varchar2 is
      v_rdo   varchar2 (2000);
   begin
      v_rdo := null;

      for x in 1 .. p_espacios loop
         v_rdo := v_rdo || fof.white_space;
      end loop;

      return v_rdo;
   end;

   function formatear (p_entrada in number)
      return varchar2 is
      v_rdo   varchar2 (100);
   begin
      v_rdo := replace (to_char (p_entrada, '9990.00'), '.', ',');
      return (v_rdo);
   end;

   procedure control_1 (p_dep in number, p_area in number) is
      vCreditos    number;
      v_aux        number;
      v_mostrada   number;
      v_nombre     varchar2 (2000);

      cursor lista_control_1 (p_dep in number, p_area in number) is
         select   id, nombre, creditos_maximos, creditos_Reduccion, creditos, creditos_asignados,
                  creditos - creditos_asignados creditos_pendientes, categoria_nombre
         from     (select p.id, p.nombre, nvl (creditos_maximos, 0) creditos_maximos,
                          nvl (creditos_reduccion, 0) creditos_reduccion, nvl (creditos, 0) creditos,
                          (select nvl (sum (nvl (ip.creditos, i.creditos)), 0) creditos
                           from   hor_items_profesores ip,
                                  hor_items i
                           where  profesor_id = p.id
                           and    ip.item_id = i.id) creditos_asignados, categoria_nombre
                   from   hor_profesores p
                   where  p.departamento_id = p_dep
                   and    area_id = p_area)
         order by nombre;
   begin
      select nombre
      into   v_nombre
      from   hor_areas
      where  id = p_area;

      v_aux := 0;
      v_mostrada := 0;

      for x in lista_control_1 (p_dep, p_area) loop
         begin
            select nvl (sum (creditos), 0)
            into   vCreditos
            from   (select   grupo_id, tipo_subgrupo, tipo_subgrupo_id, wm_concat (asignatura_id) asignatura_id,
                             creditos
                    from     (select   items.id, items.grupo_id, items.tipo_subgrupo, items.tipo_subgrupo_id,
                                       items.subgrupo_id, items.dia_semana_id, items.hora_inicio, items.hora_fin,
                                       rtrim
                                          (xmlagg (xmlelement (e, asi.asignatura_id || ', ')).extract
                                                                                           ('//text()'),
                                           ', ') as asignatura_id,
                                       to_date (dia_semana_id + 1 || '/01/2006 ' || to_char (hora_fin, 'hh24:mi'),
                                                'dd/mm/yyyy hh24:mi') fin,
                                       to_date (dia_semana_id + 1 || '/01/2006 '
                                                || to_char (hora_inicio, 'hh24:mi'),
                                                'dd/mm/yyyy hh24:mi') inicio,
                                       creditos
                              from     uji_horarios.hor_items items,
                                       uji_horarios.hor_items_asignaturas asi
                              where    items.id = asi.item_id
                              and      items.id in (select item_id
                                                    from   hor_items_profesores
                                                    where  profesor_id = x.id)
                              group by items.id,
                                       items.grupo_id,
                                       items.tipo_subgrupo,
                                       items.tipo_subgrupo_id,
                                       items.subgrupo_id,
                                       items.dia_semana_id,
                                       items.semestre_id,
                                       items.hora_inicio,
                                       items.hora_fin,
                                       asi.asignatura_id,
                                       creditos)
                    group by grupo_id,
                             tipo_subgrupo,
                             tipo_subgrupo_id,
                             creditos);

            select creditos_asignados
            into   vCreditos
            from   hor_v_profesor_creditos
            where  id = x.id;

            if (x.creditos - vCreditos) < 0 then
               v_aux := v_aux + 1;
            end if;
         exception
            when others then
               vCreditos := 0;
         end;

         if     v_aux = 1
            and v_mostrada = 0 then
            fop.block (espacios (2) || fof.bold (v_nombre), font_size => 12, space_before => 0.1);
            v_mostrada := 1;
         end if;

         if (x.creditos - vCreditos) < 0 then
            fop.block (espacios (5) || x.categoria_nombre || ' - ' || x.nombre || ': '
                       || fof.bold (formatear (x.creditos - vCreditos)),
                       font_size => 10);
         end if;
      end loop;
   end;

   procedure control_2 (p_dep in number, p_area in number) is
      v_aux        number;
      v_nombre     varchar2 (1000);
      v_sesiones   number;

      cursor lista_solapamientos (p_prof in number, p_semestre in number) is
         select   id, min (asignatura_id) asignatura_id, grupo_id, tipo_subgrupo_id || subgrupo_id subgrupo_id,
                  semestre_id, dia_semana_id,
                  decode (dia_semana_id,
                          1, 'Dilluns',
                          2, 'Dimarts',
                          3, 'Dimecres',
                          4, 'Dijous',
                          5, 'Divendres',
                          6, 'Dissabte',
                          7, 'Diumenge',
                          'Error'
                         ) dia_semana,
                  min (hora_inicio) inicio, max (hora_fin) fin
         from     (select   id, grupo_id, subgrupo_id, tipo_subgrupo, tipo_subgrupo_id, dia_semana_id, hora_inicio,
                            hora_fin, wm_concat (asignatura_id) asignatura_id, semestre_id,
                            count (*) over (partition by semestre_id, dia_semana_id) cuantos
                   from     (select id, asignatura_id, semestre_id, grupo_id, tipo_subgrupo, tipo_subgrupo_id,
                                    subgrupo_id, dia_semana_id, hora_inicio, hora_fin
                             from   (select   items.id, items.grupo_id, items.tipo_subgrupo, items.tipo_subgrupo_id,
                                              items.subgrupo_id, items.dia_semana_id, items.hora_inicio, items.hora_fin,
                                              items.semestre_id, wm_concat (asi.asignatura_id) asignatura_id
                                     from     uji_horarios.hor_items items,
                                              uji_horarios.hor_items_asignaturas asi
                                     where    items.id = asi.item_id
                                     and      items.semestre_id = p_semestre
                                     and      items.dia_semana_id is not null
                                     and      items.id in (select item_id
                                                           from   hor_items_profesores
                                                           where  profesor_id = p_prof)
                                     group by asi.asignatura_id,
                                              items.id,
                                              items.grupo_id,
                                              items.tipo_subgrupo,
                                              items.tipo_subgrupo_id,
                                              items.subgrupo_id,
                                              items.dia_semana_id,
                                              items.semestre_id,
                                              items.hora_inicio,
                                              items.hora_fin) x
                             where  exists (
                                       select 1
                                       from   hor_items i,
                                              hor_items_profesores p
                                       where  i.id = p.item_id
                                       and    profesor_id = p_prof
                                       and    i.id <> x.id
                                       and    i.dia_semana_id = x.dia_Semana_id
                                       and    i.semestre_id = x.semestre_id
                                       and    (   to_number (to_char ((i.hora_inicio + 1 / 1440), 'hh24mi'))
                                                     between to_number (to_char (x.hora_inicio, 'hh24mi'))
                                                         and to_number (to_char (x.hora_fin, 'hh24mi'))
                                               or to_number (to_char ((i.hora_fin - 1 / 1440), 'hh24mi'))
                                                     between to_number (to_char (x.hora_inicio, 'hh24mi'))
                                                         and to_number (to_char (x.hora_fin, 'hh24mi'))
                                              )))
                   group by id,
                            grupo_id,
                            subgrupo_id,
                            tipo_subgrupo,
                            tipo_subgrupo_id,
                            dia_semana_id,
                            semestre_id,
                            hora_inicio,
                            hora_fin)
         where    cuantos > 1
         group by id,
                  semestre_id,
                  dia_Semana_id,
                  grupo_id,
                  tipo_subgrupo_id || subgrupo_id
         order by 5,
                  6,
                  2,
                  3,
                  4,
                  6;

      cursor lista_solapes_Semana_Detalle (p_prof in number, p_semestre in number, p_item in number) is
         select distinct inicio, fin
         from            hor_items i,
                         hor_items_detalle d,
                         hor_items_profesores p
         where           i.id = d.item_id
         and             i.id = p.item_id
         and             profesor_id = p_prof
         and             (d.inicio, d.fin) in (
                            select   inicio, fin
                            from     (select d.inicio, fin
                                      from   hor_items i,
                                             hor_items_profesores p,
                                             hor_items_detalle d,
                                             hor_items_det_profesores dp
                                      where  i.id = p.item_id
                                      and    profesor_id = p_prof
                                      and    i.id in (
                                                select   id
                                                from     (select   id, grupo_id, subgrupo_id, tipo_subgrupo,
                                                                   tipo_subgrupo_id, dia_semana_id, hora_inicio,
                                                                   hora_fin, wm_concat (asignatura_id) asignatura_id,
                                                                   semestre_id,
                                                                   count (*) over (partition by semestre_id, dia_semana_id)
                                                                                                                cuantos,
                                                                   detalle_manual
                                                          from     (select id, asignatura_id, semestre_id, grupo_id,
                                                                           tipo_subgrupo, tipo_subgrupo_id, subgrupo_id,
                                                                           dia_semana_id, hora_inicio, hora_fin,
                                                                           detalle_manual
                                                                    from   (select   items.id, items.grupo_id,
                                                                                     items.tipo_subgrupo,
                                                                                     items.tipo_subgrupo_id,
                                                                                     items.subgrupo_id,
                                                                                     items.dia_semana_id,
                                                                                     items.hora_inicio, items.hora_fin,
                                                                                     items.semestre_id,
                                                                                     wm_concat
                                                                                        (asi.asignatura_id)
                                                                                                          asignatura_id,
                                                                                     detalle_manual
                                                                            from     uji_horarios.hor_items items,
                                                                                     uji_horarios.hor_items_asignaturas asi
                                                                            where    items.id = asi.item_id
                                                                            and      items.semestre_id = p_semestre
                                                                            and      items.dia_semana_id is not null
                                                                            and      items.id in (
                                                                                              select item_id
                                                                                              from   hor_items_profesores
                                                                                              where  profesor_id =
                                                                                                                  p_prof)
                                                                            group by asi.asignatura_id,
                                                                                     items.id,
                                                                                     items.grupo_id,
                                                                                     items.tipo_subgrupo,
                                                                                     items.tipo_subgrupo_id,
                                                                                     items.subgrupo_id,
                                                                                     items.dia_semana_id,
                                                                                     items.semestre_id,
                                                                                     items.hora_inicio,
                                                                                     items.hora_fin,
                                                                                     items.detalle_manual) x
                                                                    where  exists (
                                                                              select 1
                                                                              from   hor_items i,
                                                                                     hor_items_profesores p
                                                                              where  i.id = p.item_id
                                                                              and    profesor_id = p_prof
                                                                              and    i.id <> x.id
                                                                              and    i.dia_semana_id = x.dia_Semana_id
                                                                              and    i.semestre_id = x.semestre_id
                                                                              and    i.detalle_manual = x.detalle_manual
                                                                              --and i.detalle_manual = p.detalle_manual
                                                                              and    (   to_number
                                                                                             (to_char ((i.hora_inicio
                                                                                                        + 1 / 1440
                                                                                                       ),
                                                                                                       'hh24mi'))
                                                                                            between to_number
                                                                                                      (to_char
                                                                                                          (x.hora_inicio,
                                                                                                           'hh24mi'))
                                                                                                and to_number
                                                                                                      (to_char
                                                                                                            (x.hora_fin,
                                                                                                             'hh24mi'))
                                                                                      or to_number
                                                                                                (to_char ((i.hora_fin
                                                                                                           - 1 / 1440
                                                                                                          ),
                                                                                                          'hh24mi'))
                                                                                            between to_number
                                                                                                      (to_char
                                                                                                          (x.hora_inicio,
                                                                                                           'hh24mi'))
                                                                                                and to_number
                                                                                                      (to_char
                                                                                                            (x.hora_fin,
                                                                                                             'hh24mi'))
                                                                                     )))
                                                          group by id,
                                                                   grupo_id,
                                                                   subgrupo_id,
                                                                   tipo_subgrupo,
                                                                   tipo_subgrupo_id,
                                                                   dia_semana_id,
                                                                   semestre_id,
                                                                   hora_inicio,
                                                                   hora_fin,
                                                                   detalle_manual)
                                                where    cuantos > 1
                                                group by id,
                                                         semestre_id,
                                                         dia_Semana_id,
                                                         grupo_id,
                                                         tipo_subgrupo_id || subgrupo_id,
                                                         detalle_manual)
                                      and    i.id = d.item_id
                                      and    d.id = dp.detalle_id
                                      and    p.id = dp.item_profesor_id
                                      and    p.detalle_manual = 1
                                      union all
                                      select d.inicio, fin
                                      from   hor_items i,
                                             hor_items_profesores p,
                                             hor_items_detalle d
                                      where  i.id = p.item_id
                                      and    profesor_id = p_prof
                                      and    i.id in (
                                                select   id
                                                from     (select   id, grupo_id, subgrupo_id, tipo_subgrupo,
                                                                   tipo_subgrupo_id, dia_semana_id, hora_inicio,
                                                                   hora_fin, wm_concat (asignatura_id) asignatura_id,
                                                                   semestre_id,
                                                                   count (*) over (partition by semestre_id, dia_semana_id)
                                                                                                                cuantos,
                                                                   detalle_manual
                                                          from     (select id, asignatura_id, semestre_id, grupo_id,
                                                                           tipo_subgrupo, tipo_subgrupo_id, subgrupo_id,
                                                                           dia_semana_id, hora_inicio, hora_fin,
                                                                           detalle_manual
                                                                    from   (select   items.id, items.grupo_id,
                                                                                     items.tipo_subgrupo,
                                                                                     items.tipo_subgrupo_id,
                                                                                     items.subgrupo_id,
                                                                                     items.dia_semana_id,
                                                                                     items.hora_inicio, items.hora_fin,
                                                                                     items.semestre_id,
                                                                                     wm_concat
                                                                                        (asi.asignatura_id)
                                                                                                          asignatura_id,
                                                                                     detalle_manual
                                                                            from     uji_horarios.hor_items items,
                                                                                     uji_horarios.hor_items_asignaturas asi
                                                                            where    items.id = asi.item_id
                                                                            and      items.semestre_id = p_semestre
                                                                            and      items.dia_semana_id is not null
                                                                            and      items.id in (
                                                                                              select item_id
                                                                                              from   hor_items_profesores
                                                                                              where  profesor_id =
                                                                                                                  p_prof)
                                                                            group by asi.asignatura_id,
                                                                                     items.id,
                                                                                     items.grupo_id,
                                                                                     items.tipo_subgrupo,
                                                                                     items.tipo_subgrupo_id,
                                                                                     items.subgrupo_id,
                                                                                     items.dia_semana_id,
                                                                                     items.semestre_id,
                                                                                     items.hora_inicio,
                                                                                     items.hora_fin,
                                                                                     items.detalle_manual) x
                                                                    where  exists (
                                                                              select 1
                                                                              from   hor_items i,
                                                                                     hor_items_profesores p
                                                                              where  i.id = p.item_id
                                                                              and    profesor_id = p_prof
                                                                              and    i.id <> x.id
                                                                              and    i.dia_semana_id = x.dia_Semana_id
                                                                              and    i.semestre_id = x.semestre_id
                                                                              and    i.detalle_manual = x.detalle_manual
                                                                              --and i.detalle_manual = p.detalle_manual
                                                                              and    (   to_number
                                                                                             (to_char ((i.hora_inicio
                                                                                                        + 1 / 1440
                                                                                                       ),
                                                                                                       'hh24mi'))
                                                                                            between to_number
                                                                                                      (to_char
                                                                                                          (x.hora_inicio,
                                                                                                           'hh24mi'))
                                                                                                and to_number
                                                                                                      (to_char
                                                                                                            (x.hora_fin,
                                                                                                             'hh24mi'))
                                                                                      or to_number
                                                                                                (to_char ((i.hora_fin
                                                                                                           - 1 / 1440
                                                                                                          ),
                                                                                                          'hh24mi'))
                                                                                            between to_number
                                                                                                      (to_char
                                                                                                          (x.hora_inicio,
                                                                                                           'hh24mi'))
                                                                                                and to_number
                                                                                                      (to_char
                                                                                                            (x.hora_fin,
                                                                                                             'hh24mi'))
                                                                                     )))
                                                          group by id,
                                                                   grupo_id,
                                                                   subgrupo_id,
                                                                   tipo_subgrupo,
                                                                   tipo_subgrupo_id,
                                                                   dia_semana_id,
                                                                   semestre_id,
                                                                   hora_inicio,
                                                                   hora_fin,
                                                                   detalle_manual)
                                                where    cuantos > 1
                                                group by id,
                                                         semestre_id,
                                                         dia_Semana_id,
                                                         grupo_id,
                                                         tipo_subgrupo_id || subgrupo_id,
                                                         detalle_manual)
                                      and    i.id = d.item_id
                                      and    p.detalle_manual = 0)
                            group by inicio,
                                     fin
                            having   count (*) > 1)
         order by        1;
   begin
      v_nombre := 'cap';

      for p in lista_profesores (p_dep, p_Area) loop
         for p_Sem in 1 .. 2 loop
            begin
               v_aux := 0;

               for item in lista_solapamientos (p.id, p_sem) loop
                  v_aux := v_aux + 1;

                  if v_aux = 1 then
                     if p.nombre_area <> v_nombre then
                        v_nombre := p.nombre_area;
                        fop.block (espacios (3) || fof.bold (p.nombre_area), font_size => 12, space_after => 0.2,
                                   space_before => 0.2);
                     end if;

                     fop.block (espacios (6) || fof.bold ('Semestre ' || p_sem || ' de ' || p.nombre), font_size => 10);
                  end if;

                  fop.block (espacios (9) || item.dia_semana || ' - ' || to_char (item.inicio, 'hh24:mi') || '-'
                             || to_char (item.fin, 'hh24:mi') || ' - ' || item.asignatura_id || ' - ' || item.grupo_id
                             || ' ' || item.subgrupo_id,
                             font_size => 10);
                  v_sesiones := 0;

                  for sesiones in lista_solapes_Semana_Detalle (p.id, p_sem, item.id) loop
                     fop.block (espacios (12) || to_char (sesiones.inicio, 'dd/mm/yyyy') || '    '
                                || to_char (sesiones.inicio, 'hh24:mi') || ' - ' || to_char (sesiones.fin, 'hh24:mi'),
                                font_size => 10);
                     v_sesiones := v_sesiones + 1;
                  end loop;

                  if v_sesiones = 0 then
                     fop.block (espacios (12) || 'Sols solapa en setmana generica, en semana detall no.',
                                font_size => 10);
                  end if;
               end loop;

               if v_aux > 0 then
                  fop.block (fof.white_space);
               end if;
            exception
               when others then
                  fop.block (sqlerrm);
            end;
         end loop;
      end loop;
   end;

   procedure control_3 (p_dep in number) is
      v_control     varchar2 (200);
      v_crd_final   number;
      v_total       number;
      v_txt         varchar2 (2000);

      cursor lista_Descuadres_ant (p_dep in number) is
         select   id, nombre, nvl (asignatura_txt, asignatura_id) asignatura_id, grupo_id, tipo, crd_final, crd_item,
                  total, nombre_area, asignatura_txt
         from     (select id, nombre, asignatura_id, grupo_id, tipo, crd_final, crd_item, nombre_area,
                          sum (crd_final) over (partition by asignatura_id, grupo_id, tipo) total, asignatura_txt
                   from   (select distinct d.id, d.nombre, asignatura_id, grupo_id, tipo, crd_final, crd_item,
                                           a.nombre nombre_area, asignatura_txt
                           from            hor_v_items_creditos_detalle d,
                                           hor_areas a
                           where           d.departamento_id = p_dep
                           and             area_id = a.id))
         where    crd_item <> total
         and      asignatura_id in (select asignatura_id
                                    from   hor_v_asignaturas_area aa,
                                           hor_Areas a
                                    where  area_id = a.id
                                    and    departamento_id = p_dep
                                    and    porcentaje > 0)
         order by asignatura_id,
                  grupo_id,
                  tipo,
                  nombre;

      cursor lista_Descuadres (p_dep in number) is
         select   id, nombre, nvl (asignatura_txt, asignatura_id) asignatura_id, grupo_id, tipo, crd_final, crd_item,
                  total, nombre_area, asignatura_Txt
         from     (select   id, nombre, asignatura_id, grupo_id, tipo, crd_final, crd_item, total, nombre_area,
                            min (asignatura_txt) asignatura_txt
                   from     (select id, nombre, asignatura_id, grupo_id, tipo, crd_final, crd_item, nombre_area,
                                    sum (crd_final) over (partition by asignatura_id, grupo_id, tipo) total,
                                    asignatura_txt
                             from   (select distinct d.id, d.nombre, asignatura_id, grupo_id, tipo, crd_final, crd_item,
                                                     a.nombre nombre_area, asignatura_txt
                                     from            hor_v_items_creditos_detalle d,
                                                     hor_areas a
                                     where           d.departamento_id = p_dep
                                     and             area_id = a.id))
                   where    crd_item <> total
                   and      asignatura_id in (select asignatura_id
                                              from   hor_v_asignaturas_area aa,
                                                     hor_Areas a
                                              where  area_id = a.id
                                              and    departamento_id = p_dep
                                              and    porcentaje > 0)
                   group by id,
                            nombre,
                            asignatura_id,
                            grupo_id,
                            tipo,
                            crd_final,
                            crd_item,
                            total,
                            nombre_area)
         order by asignatura_id,
                  grupo_id,
                  tipo,
                  nombre;
   begin
      v_aux := 0;
      v_control := 'x';

      for p in lista_descuadres (p_dep) loop
         v_aux := v_aux + 1;

         if v_aux = 1 then
            fop.tableOpen;
            fop.tablecolumndefinition (column_width => 2);
            fop.tablecolumndefinition (column_width => 2);
            fop.tablecolumndefinition (column_width => 7);
            fop.tablecolumndefinition (column_width => 2);
            fop.tablecolumndefinition (column_width => 2);
            fop.tablecolumndefinition (column_width => 2);
            fop.tablebodyopen;
            fop.tableRowOpen;
            fop.tablecellopen;
            fop.block (fof.white_space, font_size => 10);
            fop.tablecellclose;
            fop.tablecellopen;
            fop.block (fof.bold ('Crd Grup'), font_size => 10, text_align => 'center');
            fop.tablecellclose;
            fop.tablecellopen;
            fop.block (fof.white_space, font_size => 10);
            fop.tablecellclose;
            fop.tablecellopen;
            fop.block (fof.bold ('Grup'), font_size => 10, text_align => 'center');
            fop.tablecellclose;
            fop.tablecellopen;
            fop.block (fof.bold ('Crd Prof'), font_size => 10, text_align => 'right');
            fop.tablecellclose;
            fop.tablecellopen;
            fop.block (fof.bold ('Diferència'), font_size => 10, text_align => 'right');
            fop.tablecellclose;
            fop.tableRowClose;
         else
            if v_control <> p.asignatura_id || ' ' || p.grupo_id || ' ' || p.tipo then
               fop.tableRowOpen;
               fop.tablecellopen;
               fop.block (fof.white_space, font_size => 10);
               fop.tablecellclose;
               fop.tablecellopen;
               fop.block (fof.white_space, font_size => 10);
               fop.tablecellclose;
               fop.tablecellopen;
               fop.block (fof.white_space, font_size => 10);
               fop.tablecellclose;
               fop.tablecellopen;
               fop.block (fof.white_space, font_size => 10);
               fop.tablecellclose;
               fop.tablecellopen;
               fop.block (formatear (v_total), font_size => 10, text_align => 'right');
               fop.tablecellclose;
               fop.tablecellopen;
               fop.block (fof.bold (formatear (v_total - v_crd_final)), font_size => 14, text_align => 'right');
               fop.tablecellclose;
               fop.tableRowClose;
               fop.tableRowOpen;
               fop.tablecellopen;
               fop.block (fof.white_space, font_size => 10);
               fop.tablecellclose;
               fop.tableRowClose;
            end if;
         end if;

         v_txt := p.asignatura_id;

         if instr (p.asignatura_id, ',') > 0 then
            v_txt := replace (p.asignatura_id, ',', ', ');
         end if;

         fop.tableRowOpen;
         fop.tablecellopen;
         fop.block (v_txt, font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (formatear (p.crd_item), font_size => 10, text_align => 'center');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (p.nombre || ' (' || italic (p.nombre_area) || ')', font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (p.grupo_id || ' - ' || p.tipo, font_size => 10, text_align => 'center');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (formatear (p.crd_final), font_size => 10, text_align => 'right');
         fop.tablecellclose;
         fop.tableRowClose;
         v_control := p.asignatura_id || ' ' || p.grupo_id || ' ' || p.tipo;
         v_crd_final := p.crd_item;
         v_total := p.total;
      end loop;

      if v_aux >= 1 then
         fop.tableRowOpen;
         fop.tablecellopen;
         fop.block (fof.white_space, font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.white_space, font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.white_space, font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.white_space, font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (formatear (v_total), font_size => 10, text_align => 'right');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (fof.bold (formatear (nvl (v_total, 0) - nvl (v_crd_final, 0))), font_size => 14,
                    text_align => 'right');
         fop.tablecellclose;
         fop.tableRowClose;
      end if;

      if v_aux > 0 then
         fop.tablebodyclose;
         fop.tableClose;
      end if;
   end;

   procedure control_4 (p_dep in number) is
      v_total_area   number;
      v_area         number;
      v_acum1        number;
      v_acum2        number;
   begin
      fop.tableOpen;
      fop.tablecolumndefinition (column_width => 9);
      fop.tablecolumndefinition (column_width => 2);
      fop.tablecolumndefinition (column_width => 2);
      fop.tablecolumndefinition (column_width => 2);
      fop.tablecolumndefinition (column_width => 3);
      fop.tablebodyopen;
      fop.tableRowOpen;
      fop.tablecellopen;
      fop.block (fof.bold ('Àrees de coneixement'), font_size => 10);
      fop.tablecellclose;
      fop.tablecellopen;
      fop.block (fof.bold ('Crèdits a impartir'), font_size => 10, text_align => 'right');
      fop.tablecellclose;
      fop.tablecellopen;
      fop.block (fof.bold ('Crèdits assignats'), font_size => 10, text_align => 'right');
      fop.tablecellclose;
      fop.tablecellopen;
      fop.block (fof.bold ('% assig- nació'), font_size => 10, text_align => 'right');
      fop.tablecellclose;
      fop.tableRowClose;
      v_acum1 := 0;
      v_acum2 := 0;

      for x in lista_areas (p_dep) loop
         select nvl (sum (creditos * porcentaje / 100), 0) crd_total_area
         into   v_total_area
         from   (select distinct nvl (comun_texto, asignatura_id) asignatura_id, grupo_id, tipo_subgrupo_id,
                                 subgrupo_id, creditos, porcentaje
                 from            hor_items i,
                                 hor_items_asignaturas a,
                                 hor_ext_asignaturas_Area aa
                 where           i.id = a.item_id
                 and             asignatura_id = asi_id
                 and             uest_id = x.id);

         select round (nvl (sum (creditos * porcentaje / 100 * porcentaje_comun / 100), 0), 2) crd_total_area
         into   v_total_area
         from   (select distinct asignatura_id, asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo_id, creditos,
                                 porcentaje, 100 porcentaje_comun
                 from            hor_items i,
                                 hor_items_asignaturas a,
                                 hor_ext_asignaturas_Area aa
                 where           i.id = a.item_id
                 and             asignatura_id = asi_id
                 and             uest_id = x.id
                 and             comun = 0
                 union all
                 select distinct comun_texto, a.asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo_id, creditos,
                                 aa.porcentaje, c.porcentaje porcentaje_comun
                 from            hor_items i,
                                 hor_items_asignaturas a,
                                 hor_ext_asignaturas_Area aa,
                                 hor_ext_asignaturas_comunes c
                 where           i.id = a.item_id
                 and             a.asignatura_id = asi_id
                 and             uest_id = x.id
                 and             comun = 1
                 and             a.asignatura_id = c.asignatura_id);

         /*
         select nvl (sum (creditos), 0)
         into   v_Area
         from   (select distinct d.profesor_id, nvl (comun_Texto, asignatura_id) asignatura_id, grupo_id,
                                 tipo_subgrupo_id, subgrupo_id, uest_id area_id,
                                 round (nvl (creditos_detalle, d.creditos) * porcentaje / 100, 2) creditos
                 from            hor_v_items_prof_detalle d,
                                 hor_items i,
                                 hor_items_asignaturas a,
                                 hor_ext_asignaturas_area aa
                 where           d.item_id = i.id
                 and             i.id = a.item_id
                 and             asignatura_id = asi_id
                 and             uest_id = x.id);
                 */
         select nvl (sum (creditos), 0)
         into   v_area
         from   (select distinct d.profesor_id, min (nvl (comun_Texto, asignatura_id)) asignatura_id, grupo_id,
                                 tipo_subgrupo_id, subgrupo_id, uest_id area_id,

                                 --nvl (creditos_detalle, d.creditos) * porcentaje / 100 creditos
                                 nvl (creditos_detalle, d.creditos) creditos
                 from            hor_v_items_prof_detalle d,
                                 hor_items i,
                                 hor_items_asignaturas a,
                                 hor_ext_asignaturas_area aa,
                                 hor_profesores h
                 where           d.item_id = i.id
                 and             i.id = a.item_id
                 and             asignatura_id = asi_id
                 and             uest_id = x.id
                 and             d.profesor_id = h.id
                 and             h.area_id = uest_id
                 group by        d.profesor_id,
                                 grupo_id,
                                 tipo_subgrupo_id,
                                 subgrupo_id,
                                 uest_id,
                                 creditos_detalle,
                                 d.creditos,
                                 porcentaje);

         fop.tableRowOpen;
         fop.tablecellopen;
         fop.block (x.nombre, font_size => 10);
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (formatear (v_total_area), font_size => 10, text_align => 'right');
         fop.tablecellclose;
         fop.tablecellopen;
         fop.block (formatear (v_area), font_size => 10, text_align => 'right');
         fop.tablecellclose;
         fop.tablecellopen;

         if v_total_area <> 0 then
            fop.block (formatear (round (v_area / v_total_area * 100, 2)) || '%', font_size => 10,
                       text_align => 'right');
         else
            fop.block (formatear (0) || '%', font_size => 10, text_align => 'right');
         end if;

         fop.tablecellclose;

         if v_area > v_total_area then
            fop.tablecellopen;
            fop.block (fof.bold ('Excés de crèdits'), font_size => 10, text_align => 'right');
            fop.tablecellclose;
         end if;

         fop.tableRowClose;
         v_acum1 := v_acum1 + v_total_Area;
         v_acum2 := v_acum2 + v_area;
      end loop;

      fop.tableRowOpen;
      fop.tablecellopen;
      fop.block (fof.white_space, font_size => 10);
      fop.tablecellclose;
      fop.tablecellopen;
      fop.block (fof.bold (formatear (v_acum1)), font_size => 10, text_align => 'right');
      fop.tablecellclose;
      fop.tablecellopen;
      fop.block (fof.bold (formatear (v_acum2)), font_size => 10, text_align => 'right');
      fop.tablecellclose;
      fop.tableRowClose;
      fop.tableBodyClose;
      fop.tableClose;
   end;
BEGIN
   select count (*) + 1
   into   vSesion
   from   apa_sesiones
   where  sesion_id = pSesion
   and    fecha < sysdate () - 0.33;

   select id
   into   vCurso
   from   hor_curso_academico;

   if vSesion > 0 then
      cabecera;

      begin
         fop.block (fof.bold ('Control 1 - Professors amb excés de crèdits assignats'), font_size => 16);
         fop.block (fof.white_space);

         for a in lista_areas (pDepartamento) loop
            control_1 (pDepartamento, a.id);
         end loop;

         fop.block (fof.white_space);
         fop.block (fof.white_space);
      exception
         when others then
            fop.block ('Error control 1: ' || sqlerrm, font_size => 12);
      end;

      begin
         fop.block (fof.bold ('Control 2 - Solapaments (a setmana generica)'), font_size => 16);
         fop.block (fof.white_space);

         for a in lista_areas (pDepartamento) loop
            control_2 (pDepartamento, a.id);
         end loop;

         fop.block (fof.white_space);
         fop.block (fof.white_space);
      exception
         when others then
            fop.block ('Error control 2: ' || sqlerrm, font_size => 12);
      end;

      begin
         fop.block (fof.bold ('Control 3 - Desquadres de crèdits per assignatura'), font_size => 16);
         fop.block (fof.white_space);
         control_3 (pDepartamento);
         fop.block (fof.white_space);
         fop.block (fof.white_space);
      exception
         when others then
            fop.block ('Error control 3: ' || sqlerrm, font_size => 12);
      end;

      begin
         fop.block (fof.bold ('Control 4 - Estat d''assignació de crèdits per àrea de coneixement'), font_size => 16);
         fop.block (fof.white_space);
         control_4 (pDepartamento);
         fop.block (fof.white_space);
         fop.block (fof.white_space);
      exception
         when others then
            fop.block ('Error control 3: ' || sqlerrm, font_size => 12);
      end;

      pie;
   else
      cabecera;
      fop.block ('No tens permís per accedir a aquest document.', font_weight => 'bold', text_align => 'center',
                 font_size => 14, space_after => 0.5);
      pie;
   end if;
END;

CREATE OR REPLACE PROCEDURE UJI_HORARIOS.X_horarios_ocupacion_aulas_pdf
(
   name_array    IN   euji_util.ident_arr2,
   value_array   IN   euji_util.ident_arr2
) IS

   pAula    CONSTANT VARCHAR2 (40) := euji_util.euji_getparam (name_array, value_array, 'pAula');
   pSemestre   CONSTANT VARCHAR2 (40) := euji_util.euji_getparam (name_array, value_array, 'pSemestre');
   pSemana     CONSTANT VARCHAR2 (40) := euji_util.euji_getparam (name_array, value_array, 'pSemana');
   vCursoAcaNum         number;
   vFechaIniSemestre    date;
   vFechaFinSemestre    date;
   vEsGenerica          boolean       := pSemana = 'G';
   vFechas              t_horario;
   vRdo                 clob;

   CURSOR c_items_det (vIniDate date, vFinDate date) IS
      SELECT   items.id, items.grupo_id, items.tipo_subgrupo, items.tipo_subgrupo_id, items.subgrupo_id,
            items.dia_semana_id, items.hora_inicio, items.hora_fin, det.inicio, det.fin,  rtrim (xmlagg (xmlelement (e, asi.asignatura_id || ', ')).extract ('//text()'), ', ') as asignatura_id
      FROM     UJI_HORARIOS.HOR_ITEMS items,
               UJI_HORARIOS.HOR_ITEMS_DETALLE det,
               uji_horarios.hor_items_asignaturas asi,
               hor_aulas_planificacion p
      WHERE    items.id = asi.item_id
            AND items.aula_planificacion_id = p.id
            AND p.aula_id = pAula
            AND p.semestre_id = pSemestre
            AND      items.dia_semana_id IS NOT NULL
            and      det.item_id = items.id
            and      det.inicio > vIniDate
            and      det.fin < vFinDate
            group by items.id, items.grupo_id, items.tipo_subgrupo, items.tipo_subgrupo_id, items.subgrupo_id,  items.dia_semana_id, items.hora_inicio, items.hora_fin, det.inicio, det.fin
            order by det.inicio;


   CURSOR c_items_gen IS
      SELECT   items.id, items.grupo_id, items.tipo_subgrupo, items.tipo_subgrupo_id, items.subgrupo_id,
            items.dia_semana_id, items.hora_inicio, items.hora_fin,  rtrim (xmlagg (xmlelement (e, asi.asignatura_id || ', ')).extract ('//text()'), ', ') as asignatura_id,
              to_date (dia_semana_id + 1 || '/01/2006 ' || to_char (hora_fin, 'hh24:mi'), 'dd/mm/yyyy hh24:mi') fin,
             to_date (dia_semana_id + 1 || '/01/2006 ' || to_char (hora_inicio, 'hh24:mi'), 'dd/mm/yyyy hh24:mi') inicio
      FROM     UJI_HORARIOS.HOR_ITEMS items,
               uji_horarios.hor_items_asignaturas asi,
               hor_aulas_planificacion p
      WHERE    items.id = asi.item_id
            AND items.aula_planificacion_id = p.id
            AND p.aula_id = pAula
            AND p.semestre_id = pSemestre
            AND      items.dia_semana_id IS NOT NULL
            group by items.id, items.grupo_id, items.tipo_subgrupo, items.tipo_subgrupo_id, items.subgrupo_id,  items.dia_semana_id, items.hora_inicio, items.hora_fin
            order by items.hora_inicio;

   CURSOR c_asigs is
      SELECT distinct (asignatura_id), asignatura
      FROM            UJI_HORARIOS.HOR_ITEMS items,
                      uji_horarios.hor_items_asignaturas asi,
                      hor_aulas_planificacion p
      WHERE           items.id = asi.item_id
            AND items.aula_planificacion_id = p.id
            AND p.aula_id = pAula
            AND p.semestre_id = pSemestre
          AND             items.dia_semana_id IS NOT NULL
          order by        asignatura_id;

   CURSOR c_clases_asig (vAsignaturaId varchar2) is
      SELECT   tipo_subgrupo_id || subgrupo_id subgrupo,
               decode (dia_semana_id, 1, 'L', 2, 'M', 3, 'X', 4, 'J', 5, 'V', '?') dia_sem,
               to_char (hora_fin, 'hh24:mi') hora_fin_str, to_char (hora_inicio, 'hh24:mi') hora_inicio_str
      FROM     UJI_HORARIOS.HOR_ITEMS items,
               uji_horarios.hor_items_asignaturas asi,
               hor_aulas_planificacion p
      WHERE    items.id = asi.item_id
            AND items.aula_planificacion_id = p.id
            AND p.aula_id = pAula
          AND      items.semestre_id = pSemestre
          and      asi.asignatura_id = vAsignaturaId
          AND      items.dia_semana_id IS NOT NULL
          order by dia_semana_id,
                   hora_inicio,
                   subgrupo;

function semana_tiene_clase (vIniDate date, vFinDate date)
      return boolean is
      num_clases   number;
   begin
      SELECT   count(*)
      into     num_clases
      FROM     UJI_HORARIOS.HOR_ITEMS items,
               UJI_HORARIOS.HOR_ITEMS_DETALLE det,
               hor_aulas_planificacion p
      WHERE     items.aula_planificacion_id = p.id
            AND p.aula_id = pAula
            AND p.semestre_id = pSemestre
            AND      items.dia_semana_id IS NOT NULL
            and      det.item_id = items.id
            and      det.inicio > vIniDate
            and      det.fin < vFinDate;


      return num_clases > 0;
   end;

procedure calcula_curso_aca is
   begin
      SELECT   distinct(curso_academico_id)
      into     vCursoAcaNum
      FROM     hor_semestres_detalle d,
               hor_estudios e
      WHERE    ROWNUM = 1
      ORDER BY curso_academico_id;
   end;

   procedure calcula_fechas_semestre is
   begin
      SELECT fecha_inicio, fecha_examenes_fin
      into   vFechaIniSemestre, vFechaFinSemestre
      FROM   hor_semestres_detalle,
             hor_estudios e
      WHERE  semestre_id = pSemestre
             and rownum = 1;
   end;

   function contenido_item (hora_ini date, hora_fin date, texto varchar2, subtexto varchar2)
      return varchar2 is
      contenido   varchar2 (4000);
   begin
      --contenido := '<fo:block font-size="6pt" text-align="center">' || to_char (hora_ini, 'hh24:mi') ||' - '||to_char (hora_fin, 'hh24:mi')||' '||texto||' ('||subtexto||')</fo:block>';
      contenido := '<fo:table border="none" width="100%" height="100%" text-align="start">';
      contenido := contenido || '<fo:table-column column-width="25%"/>';
      contenido := contenido || '<fo:table-column column-width="50%"/>';
      contenido := contenido || '<fo:table-column column-width="25%"/>';
      contenido := contenido || fof.tableBodyOpen;
      contenido := contenido || fof.tableRowOpen;
      contenido := contenido || fof.tableCellOpen;
      contenido :=
         contenido
         || fof.block (to_char (hora_ini, 'hh24:mi'), font_size => 6, text_align => 'start', padding_bottom => 0.3);
      contenido := contenido || fof.tableCellClose;
      contenido := contenido || fof.tableRowClose;
      contenido := contenido || fof.tableRowOpen;
      contenido := contenido || fof.tableCellOpen;
      contenido := contenido || fof.tableCellClose;
      contenido := contenido || fof.tableCellOpen;
      contenido := contenido || fof.block (fof.bold (texto), font_size => 6, text_align => 'center');
      contenido := contenido || fof.block (subtexto, font_size => 5, text_align => 'center');
      contenido := contenido || fof.tableCellClose;
      contenido := contenido || fof.tableRowClose;
      contenido := contenido || fof.tableRowOpen;
      contenido := contenido || fof.tableCellOpen;
      contenido :=
         contenido
         || fof.block (to_char (hora_fin, 'hh24:mi'), font_size => 6, text_align => 'start', padding_top => 0.3);
      contenido := contenido || fof.tableCellClose;
      contenido := contenido || fof.tableRowClose;
      contenido := contenido || fof.tableBodyClose;
      contenido := contenido || fof.tableClose;
      return contenido;
   end;

   procedure calendario_det (vFechaIni date, vFechaFin date) is
   begin
      vFechas := t_horario ();

      for item in c_items_det (vFechaIni, vFechaFin) loop
         vFechas.extend;
         vFechas (vFechas.count) :=
            ItemHorarios (contenido_item (item.hora_inicio, item.hora_fin, item.asignatura_id || ' ' || item.grupo_id,
                                          '(' || item.tipo_subgrupo_id || item.subgrupo_id || ')'),
                          item.inicio, item.fin, null);
      end loop;

      if vFechas.count > 0 then
         vRdo := xpfpkg_horarios2.drawTimetable (vFechas, 5, null, 'CA', 'PDF_APP_HORARIOS_DET');
         xup_utils.dump (vRdo);
         dbms_lob.freeTemporary (vRdo);
      end if;
   end;

   procedure calendario_gen is
   begin
      vFechas := t_horario ();

      for item in c_items_gen loop
         vFechas.extend;
         vFechas (vFechas.count) :=
            ItemHorarios (contenido_item (item.hora_inicio, item.hora_fin, item.asignatura_id || ' ' || item.grupo_id,
                                          '(' || item.tipo_subgrupo_id || item.subgrupo_id || ')'),
                          item.inicio, item.fin, null);
      end loop;

      if vFechas.count > 0 then
         vRdo := xpfpkg_horarios2.drawTimetable (vFechas, 5, null, 'CA', 'PDF_APP_HORARIOS');
         xup_utils.dump (vRdo);
         dbms_lob.freeTemporary (vRdo);
      end if;
   end;


   procedure info_calendario_det (vFechaIni date, vFechaFin date) is
      vCursoAca           varchar2 (4000);
      vTextoCabecera      varchar2 (4000);
      vFechaIniStr        varchar2 (4000) := to_char (vFechaIni, 'dd/mm/yyyy');
      vFechaFinStr        varchar2 (4000) := to_char (vFechaFin - 1, 'dd/mm/yyyy');
      vAulaNombre         varchar2 (4000);
      vEdificio         varchar2 (4000);
      vPlanta         varchar2 (4000);
      vCentroNombre         varchar2 (4000);
   begin

      vCursoAca := to_char (vCursoAcaNum) || '/' || to_char (vCursoAcaNum + 1);

      SELECT  a.nombre AS aulaNombre,
         a.edificio,
         a.planta,
         c.nombre AS centroNombre
      INTO vAulaNombre, vEdificio, vPlanta, vCentroNombre
      FROM   hor_aulas a, hor_centros c
      WHERE   a.id = pAula AND c.id = a.centro_id;

      vTextoCabecera :=
         vAulaNombre || ' - ' || vCentroNombre || ' - Edifici ' || vEdificio || ' - Planta '||vPlanta ||'  - (semestre ' || pSemestre || ') - Del ' || vFechaIniStr || ' al ' || vFechaFinStr;
      fop.block (text => vTextoCabecera, border_bottom => 'solid', border_width => 0.05);
   end;


procedure info_calendario_gen  is
      vCursoAca           varchar2 (4000);
      vTextoCabecera      varchar2 (4000);
      vAulaNombre         varchar2 (4000);
      vEdificio         varchar2 (4000);
      vPlanta         varchar2 (4000);
      vCentroNombre         varchar2 (4000);
   begin

      vCursoAca := to_char (vCursoAcaNum) || '/' || to_char (vCursoAcaNum + 1);

      SELECT  a.nombre AS aulaNombre,
         a.edificio,
         a.planta,
         c.nombre AS centroNombre
      INTO vAulaNombre, vEdificio, vPlanta, vCentroNombre
      FROM   hor_aulas a, hor_centros c
      WHERE   a.id = pAula AND c.id = a.centro_id;

      vTextoCabecera :=
         vAulaNombre || ' - ' || vCentroNombre || ' - Edifici ' || vEdificio || ' - Planta '||vPlanta ||'  - (semestre ' || pSemestre || ') ';
      fop.block (text => vTextoCabecera, border_bottom => 'solid', border_width => 0.05);
   end;

procedure paginas_calendario_det is
      vFechaIniSemana   date;
      vFechaFinSemana   date;
   begin
      calcula_fechas_semestre;
      vFechaIniSemana := vFechaIniSemestre;
      vFechaFinSemana := vFechaIniSemana + 7;

      while vFechaFinSemana <= vFechaFinSemestre loop
         if semana_tiene_clase (vFechaIniSemana, vFechaFinSemana) then
            info_calendario_det (vFechaIniSemana, vFechaFinSemana);
            calendario_det (vFechaIniSemana, vFechaFinSemana);
            htp.p ('<fo:block break-before="page" color="white">_</fo:block>');
         end if;

         vFechaIniSemana := vFechaFinSemana;
         vFechaFinSemana := vFechaFinSemana + 7;
      end loop;
   end;

procedure cabecera is
   begin
      owa_util.mime_header (ccontent_type => 'text/xml', bclose_header => true);
      fop.documentOpen (page_width => 29.7, page_height => 21, margin_left => 0.5, margin_right => 0.5,
                        margin_top => 0.5, margin_bottom => 0, extent_before => 0, extent_after => 0.5,
                        extent_margin_top => 0, extent_margin_bottom => 0.5);
      fop.pageBodyOpen;
      fop.documentfooteropen;
      fop.block ('Fecha documento: ' || to_char (sysdate, 'yyyy/mm/dd HH24:MI'), font_size => 6,
                 font_style => 'italic');
      fop.documentfooterclose;
      fop.pageFlowOpen;
   end;


procedure muestra_leyenda_asig (item c_asigs%ROWTYPE) is
      vClasesText   varchar2 (4000);
   begin
      fop.block (item.asignatura_id || ', ' || item.asignatura || ':', font_size => 7, padding_top => 0.1);
      vClasesText := '<fo:inline color="white">___</fo:inline>';

      for c in c_clases_asig (item.asignatura_id) loop
         vClasesText :=
            vClasesText || c.dia_sem || ', ' || c.hora_inicio_str || '-' || c.hora_fin_str || ' (' || c.subgrupo
            || '); ';
      end loop;

      vClasesText := substr (vClasesText, 1, length (vClasesText) - 2);
      fop.block (vClasesText, font_size => 7, padding_top => 0.06);
   end;

   procedure leyenda_asigs is
   begin
      fop.block (' ', padding_top => 1);

      for item in c_asigs loop
         muestra_leyenda_asig (item);
      end loop;
   end;

procedure pie is
   begin
      fop.pageFlowClose;
      fop.pageBodyClose;
      fop.documentClose;
   end;



BEGIN
   calcula_curso_aca;
   cabecera;

   if vEsGenerica then
      info_calendario_gen;
      calendario_gen;
      leyenda_asigs;
   else
        paginas_calendario_det;
        leyenda_asigs;
   end if;

   pie;
EXCEPTION
   WHEN OTHERS THEN
      htp.p (sqlerrm);
END;

CREATE OR REPLACE PROCEDURE UJI_HORARIOS.X_HORARIOS_PDF_SEMGEN (
   name_array    IN   euji_util.ident_arr2,
   value_array   IN   euji_util.ident_arr2
) IS
   -- Parametros
   pEstudio    CONSTANT VARCHAR2 (40) := euji_util.euji_getparam (name_array, value_array, 'pEstudio');
   pSemestre   CONSTANT VARCHAR2 (40) := euji_util.euji_getparam (name_array, value_array, 'pSemestre');
   pGrupo      CONSTANT VARCHAR2 (40) := euji_util.euji_getparam (name_array, value_array, 'pGrupo');
   pCurso      CONSTANT VARCHAR2 (40) := euji_util.euji_getparam (name_array, value_array, 'pCurso');
   pSemana     CONSTANT VARCHAR2 (40) := euji_util.euji_getparam (name_array, value_array, 'pSemana');
   vCursoAcaNum         number;
   vFechaIniSemestre    date;
   vFechaFinSemestre    date;
   vEsGenerica          boolean       := pSemana = 'G';

     -- Variables
   --  vPerId              per_personas.id%TYPE := euji_get_perid ();
   CURSOR c_items_gen IS
      SELECT asignatura_id, asignatura, estudio, asi.caracter, semestre_id, comun, grupo_id, tipo_subgrupo,
             tipo_subgrupo_id, subgrupo_id, dia_semana_id, hora_inicio, hora_fin, tipo_asignatura_id, tipo_asignatura,
             to_date (dia_semana_id + 1 || '/01/2006 ' || to_char (hora_fin, 'hh24:mi'), 'dd/mm/yyyy hh24:mi') fin,
             to_date (dia_semana_id + 1 || '/01/2006 ' || to_char (hora_inicio, 'hh24:mi'),
                      'dd/mm/yyyy hh24:mi') inicio
      FROM   UJI_HORARIOS.HOR_ITEMS items,
             uji_horarios.hor_items_asignaturas asi
      WHERE  items.id = asi.item_id
      and    asi.estudio_id = pEstudio
      AND    asi.curso_id = pCurso
      AND    items.grupo_id = pGrupo
      AND    items.semestre_id = pSemestre
      AND    items.dia_semana_id IS NOT NULL;

   CURSOR c_items_det (vIniDate date, vFinDate date) IS
      SELECT   asignatura_id, asignatura, estudio, grupo_id, tipo_subgrupo, tipo_subgrupo_id, subgrupo_id,
               dia_semana_id, hora_inicio, hora_fin, inicio, fin
      FROM     UJI_HORARIOS.HOR_ITEMS items,
               UJI_HORARIOS.HOR_ITEMS_DETALLE det,
               uji_horarios.hor_items_asignaturas asi
      WHERE    items.id = asi.item_id
      and      asi.estudio_id = pEstudio
      AND      asi.curso_id = pCurso
      AND      items.grupo_id = pGrupo
      AND      items.semestre_id = pSemestre
      AND      items.dia_semana_id IS NOT NULL
      and      det.item_id = items.id
      and      det.inicio > vIniDate
      and      det.fin < vFinDate
      order by det.inicio;

   CURSOR c_asigs is
      SELECT distinct (asignatura_id), asignatura
      FROM            UJI_HORARIOS.HOR_ITEMS items,
                      uji_horarios.hor_items_asignaturas asi
      WHERE           items.id = asi.item_id
      and             asi.estudio_id = pEstudio
      AND             asi.curso_id = pCurso
      AND             items.grupo_id = pGrupo
      AND             items.semestre_id = pSemestre
      AND             items.dia_semana_id IS NOT NULL
      order by        asignatura_id;

   CURSOR c_clases_asig (vAsignaturaId varchar2) is
      SELECT   tipo_subgrupo_id || subgrupo_id subgrupo,
               decode (dia_semana_id, 1, 'L', 2, 'M', 3, 'X', 4, 'J', 5, 'V', '?') dia_sem,
               to_char (hora_fin, 'hh24:mi') hora_fin_str, to_char (hora_inicio, 'hh24:mi') hora_inicio_str
      FROM     UJI_HORARIOS.HOR_ITEMS items,
               uji_horarios.hor_items_asignaturas asi
      WHERE    items.id = asi.item_id
      and      asi.estudio_id = pEstudio
      AND      asi.curso_id = pCurso
      AND      items.grupo_id = pGrupo
      AND      items.semestre_id = pSemestre
      and      asi.asignatura_id = vAsignaturaId
      AND      items.dia_semana_id IS NOT NULL
      order by dia_semana_id,
               hora_inicio,
               subgrupo;

   vFechas              t_horario;
   vRdo                 clob;

   function contenido_item (hora_ini date, hora_fin date, texto varchar2, subtexto varchar2)
      return varchar2 is
      contenido   varchar2 (4000);
   begin
      --contenido := '<fo:block font-size="6pt" text-align="center">' || to_char (hora_ini, 'hh24:mi') ||' - '||to_char (hora_fin, 'hh24:mi')||' '||texto||' ('||subtexto||')</fo:block>';
      contenido := '<fo:table border="none" width="100%" height="100%" text-align="start">';
      contenido := contenido || '<fo:table-column column-width="25%"/>';
      contenido := contenido || '<fo:table-column column-width="50%"/>';
      contenido := contenido || '<fo:table-column column-width="25%"/>';
      contenido := contenido || fof.tableBodyOpen;
      contenido := contenido || fof.tableRowOpen;
      contenido := contenido || fof.tableCellOpen;
      contenido :=
         contenido
         || fof.block (to_char (hora_ini, 'hh24:mi'), font_size => 6, text_align => 'start', padding_bottom => 0.3);
      contenido := contenido || fof.tableCellClose;
      contenido := contenido || fof.tableRowClose;
      contenido := contenido || fof.tableRowOpen;
      contenido := contenido || fof.tableCellOpen;
      contenido := contenido || fof.tableCellClose;
      contenido := contenido || fof.tableCellOpen;
      contenido := contenido || fof.block (fof.bold (texto), font_size => 6, text_align => 'center');
      contenido := contenido || fof.block (subtexto, font_size => 5, text_align => 'center');
      contenido := contenido || fof.tableCellClose;
      contenido := contenido || fof.tableRowClose;
      contenido := contenido || fof.tableRowOpen;
      contenido := contenido || fof.tableCellOpen;
      contenido :=
         contenido
         || fof.block (to_char (hora_fin, 'hh24:mi'), font_size => 6, text_align => 'start', padding_top => 0.3);
      contenido := contenido || fof.tableCellClose;
      contenido := contenido || fof.tableRowClose;
      contenido := contenido || fof.tableBodyClose;
      contenido := contenido || fof.tableClose;
      return contenido;
   end;

   procedure cabecera is
   begin
      owa_util.mime_header (ccontent_type => 'text/xml', bclose_header => true);
      fop.documentOpen (page_width => 29.7, page_height => 21, margin_left => 0.5, margin_right => 0.5,
                        margin_top => 0.5, margin_bottom => 0, extent_before => 0, extent_after => 0.5,
                        extent_margin_top => 0, extent_margin_bottom => 0.5);
      fop.pageBodyOpen;
      fop.documentfooteropen;
      fop.block ('Fecha documento: ' || to_char (sysdate, 'yyyy/mm/dd HH24:MI'), font_size => 6,
                 font_style => 'italic');
      fop.documentfooterclose;
      fop.pageFlowOpen;
   end;

   procedure calcula_curso_aca is
   begin
      SELECT   curso_academico_id
      into     vCursoAcaNum
      FROM     hor_semestres_detalle d,
               hor_estudios e
      WHERE    e.id = pEstudio
      AND      e.tipo_id = d.tipo_estudio_id
      AND      ROWNUM = 1
      ORDER BY fecha_inicio;
   end;

   procedure info_calendario_gen is
      vCursoAca           varchar2 (4000);
      vTitulacionNombre   varchar2 (4000);
      vCursoNombre        varchar2 (4000) := pCurso || '?';
      vTextoCabecera      varchar2 (4000);
   begin
      select nombre
      into   vTitulacionNombre
      from   hor_estudios
      where  id = pEstudio;

      vCursoAca := to_char (vCursoAcaNum) || '/' || to_char (vCursoAcaNum + 1);

      if pCurso in ('1', '3') then
         vCursoNombre := pCurso || 'er';
      elsif pCurso = '2' then
         vCursoNombre := '2on';
      elsif pCurso = '4' then
         vCursoNombre := '4t';
      else
         vCursoNombre := pCurso || '?';
      end if;

      vTextoCabecera :=
         vTitulacionNombre || ' - ' || vCursoNombre || ' curs - Grup ' || pGrupo || ' (semestre ' || pSemestre || ') - '
         || vCursoAca;
      fop.block (text => vTextoCabecera, border_bottom => 'solid', border_width => 0.05);
   end;

   procedure muestra_leyenda_asig (item c_asigs%ROWTYPE) is
      vClasesText   varchar2 (4000);
   begin
      fop.block (item.asignatura_id || ', ' || item.asignatura || ':', font_size => 7, padding_top => 0.1);
      vClasesText := '<fo:inline color="white">___</fo:inline>';

      for c in c_clases_asig (item.asignatura_id) loop
         vClasesText :=
            vClasesText || c.dia_sem || ', ' || c.hora_inicio_str || '-' || c.hora_fin_str || ' (' || c.subgrupo
            || '); ';
      end loop;

      vClasesText := substr (vClasesText, 1, length (vClasesText) - 2);
      fop.block (vClasesText, font_size => 7, padding_top => 0.06);
   end;

   procedure leyenda_asigs is
   begin
      fop.block (' ', padding_top => 1);

      for item in c_asigs loop
         muestra_leyenda_asig (item);
      end loop;
   end;

   procedure pie is
   begin
      fop.pageFlowClose;
      fop.pageBodyClose;
      fop.documentClose;
   end;

   procedure calendario_gen is
   begin
      vFechas := t_horario ();

      for item in c_items_gen loop
         vFechas.extend;
         vFechas (vFechas.count) :=
            ItemHorarios (contenido_item (item.hora_inicio, item.hora_fin, item.asignatura_id,
                                          '(' || item.tipo_subgrupo_id || item.subgrupo_id || ')'),
                          item.inicio, item.fin, null);
      end loop;

      if vFechas.count > 0 then
         vRdo := xpfpkg_horarios2.drawTimetable (vFechas, 5, null, 'CA', 'PDF_APP_HORARIOS');
         xup_utils.dump (vRdo);
         dbms_lob.freeTemporary (vRdo);
      end if;
   end;

   procedure calendario_det (vFechaIni date, vFechaFin date) is
   begin
      vFechas := t_horario ();

      for item in c_items_det (vFechaIni, vFechaFin) loop
         vFechas.extend;
         vFechas (vFechas.count) :=
            ItemHorarios (contenido_item (item.hora_inicio, item.hora_fin, item.asignatura_id,
                                          '(' || item.tipo_subgrupo_id || item.subgrupo_id || ')'),
                          item.inicio, item.fin, null);
      end loop;

      if vFechas.count > 0 then
         vRdo := xpfpkg_horarios2.drawTimetable (vFechas, 5, null, 'CA', 'PDF_APP_HORARIOS_DET');
         xup_utils.dump (vRdo);
         dbms_lob.freeTemporary (vRdo);
      end if;
   end;

   procedure calcula_fechas_semestre is
   begin
      SELECT fecha_inicio, fecha_examenes_fin
      into   vFechaIniSemestre, vFechaFinSemestre
      FROM   hor_semestres_detalle,
             hor_estudios e
      WHERE  semestre_id = pSemestre
      and    e.id = pEstudio
      and    e.tipo_id = tipo_estudio_id;
   end;

   function semana_tiene_clase (vIniDate date, vFinDate date)
      return boolean is
      num_clases   number;
   begin
      SELECT   count (*)
      into     num_clases
      FROM     UJI_HORARIOS.HOR_ITEMS items,
               UJI_HORARIOS.HOR_ITEMS_DETALLE det,
               uji_horarios.hor_items_asignaturas asi
      WHERE    items.id = asi.item_id
      and      asi.estudio_id = pEstudio
      AND      asi.curso_id = pCurso
      AND      items.grupo_id = pGrupo
      AND      items.semestre_id = pSemestre
      AND      items.dia_semana_id IS NOT NULL
      and      det.item_id = items.id
      and      det.inicio > vIniDate
      and      det.fin < vFinDate
      order by det.inicio;

      return num_clases > 0;
   end;

   procedure info_calendario_det (vFechaIni date, vFechaFin date) is
      vCursoAca           varchar2 (4000);
      vTitulacionNombre   varchar2 (4000);
      vCursoNombre        varchar2 (4000) := pCurso || '?';
      vTextoCabecera      varchar2 (4000);
      vFechaIniStr        varchar2 (4000) := to_char (vFechaIni, 'dd/mm/yyyy');
      vFechaFinStr        varchar2 (4000) := to_char (vFechaFin - 1, 'dd/mm/yyyy');
   begin
      select nombre
      into   vTitulacionNombre
      from   hor_estudios
      where  id = pEstudio;

      vCursoAca := to_char (vCursoAcaNum) || '/' || to_char (vCursoAcaNum + 1);

      if pCurso in ('1', '3') then
         vCursoNombre := pCurso || 'er';
      elsif pCurso = '2' then
         vCursoNombre := '2on';
      elsif pCurso = '4' then
         vCursoNombre := '4t';
      else
         vCursoNombre := pCurso || '?';
      end if;

      vTextoCabecera :=
         vTitulacionNombre || ' - ' || vCursoNombre || ' curs - Grup ' || pGrupo || ' (semestre ' || pSemestre
         || ') - Del ' || vFechaIniStr || ' al ' || vFechaFinStr;
      fop.block (text => vTextoCabecera, border_bottom => 'solid', border_width => 0.05);
   end;

   procedure paginas_calendario_det is
      vFechaIniSemana   date;
      vFechaFinSemana   date;
   begin
      calcula_fechas_semestre;
      vFechaIniSemana := vFechaIniSemestre;
      vFechaFinSemana := vFechaIniSemana + 7;

      while vFechaFinSemana <= vFechaFinSemestre loop
         if semana_tiene_clase (vFechaIniSemana, vFechaFinSemana) then
            info_calendario_det (vFechaIniSemana, vFechaFinSemana);
            calendario_det (vFechaIniSemana, vFechaFinSemana);
            htp.p ('<fo:block break-before="page" color="white">_</fo:block>');
         end if;

         vFechaIniSemana := vFechaFinSemana;
         vFechaFinSemana := vFechaFinSemana + 7;
      end loop;
   end;
BEGIN
   --euji_control_acceso (vItem);
   calcula_curso_aca;
   cabecera;

   if vEsGenerica then
      info_calendario_gen;
      calendario_gen;
      leyenda_asigs;
   else
      paginas_calendario_det;
      leyenda_asigs;
   end if;

   pie;
EXCEPTION
   WHEN OTHERS THEN
      htp.p (sqlerrm);
END;

DROP TRIGGER UJI_HORARIOS.BLOQUEAR_HOR_ITEMS;

CREATE OR REPLACE TRIGGER UJI_HORARIOS.bloquear_hor_items
BEFORE DELETE OR INSERT OR UPDATE
ON UJI_HORARIOS.HOR_ITEMS
REFERENCING NEW AS New OLD AS Old
FOR EACH ROW
DISABLE
DECLARE
BEGIN
       -- Consider logging the error and then re-raise
       RAISE_application_error(-20001,'Bloqueado');
END bloquear_hor_items;

DROP TRIGGER UJI_HORARIOS.HOR_ITEMS_DELETE;

CREATE OR REPLACE TRIGGER UJI_HORARIOS.hor_items_delete
   before delete
   ON UJI_HORARIOS.HOR_ITEMS    referencing new as new old as old
   for each row
begin
   delete      uji_horarios.hor_items_detalle
   where       item_id = :old.id;
end hor_items_delete;

DROP TRIGGER UJI_HORARIOS.HOR_PERMISOS_EXTRA_ITEMS;

CREATE OR REPLACE TRIGGER UJI_HORARIOS.hor_permisos_Extra_items
BEFORE DELETE OR INSERT
ON UJI_HORARIOS.HOR_PERMISOS_EXTRA
REFERENCING NEW AS New OLD AS Old
FOR EACH ROW
/* Formatted on 22/05/2014 12:11 (Formatter Plus v4.8.8) */
BEGIN
   if inserting then
      uji_apa.apa_permisos_horarios (:new.persona_id, :new.tipo_cargo_id, 'C');
   elsif deleting then
      uji_apa.apa_permisos_horarios (:old.persona_id, :old.tipo_cargo_id, 'D');
   end if;
END hor_permisos_Extra_items;

DROP TRIGGER UJI_HORARIOS.HOT_ITEMS_DETALLLE_DELETE;

CREATE OR REPLACE TRIGGER UJI_HORARIOS.hot_items_detallle_delete
   BEFORE DELETE
   ON UJI_HORARIOS.HOR_ITEMS_DETALLE    REFERENCING NEW AS New OLD AS Old
   FOR EACH ROW
DECLARE
   tmpVar   NUMBER;
BEGIN
   if deleting then
      delete      hor_items_det_profesores
      where       detalle_id = :old.id;
   end if;
END hot_items_detallle_delete;

DROP TRIGGER UJI_HORARIOS.MUTANTE_1_INICIAL;

CREATE OR REPLACE TRIGGER UJI_HORARIOS.MUTANTE_1_INICIAL BEFORE INSERT OR UPDATE ON UJI_HORARIOS.HOR_ITEMS
BEGIN
  MUTANTE_ITEMS.V_NUM := 0;
END;

DROP TRIGGER UJI_HORARIOS.MUTANTE_2_POR_FILA;

CREATE OR REPLACE TRIGGER UJI_HORARIOS.mutante_2_por_fila
   after insert or update of dia_semana_id,
                             desde_el_dia,
                             hasta_el_dia,
                             repetir_cada_semanas,
                             numero_iteraciones,
                             hora_inicio,
                             hora_fin
   ON UJI_HORARIOS.HOR_ITEMS    referencing old as new new as old
   for each row
begin
   mutante_items.v_num := mutante_items.v_num + 1;
   mutante_items.v_var_tabla (mutante_items.v_num) := :new.rowid;
end;

DROP TRIGGER UJI_HORARIOS.MUTANTE_3_FINAL_2014;

CREATE OR REPLACE TRIGGER UJI_HORARIOS.mutante_3_final_2014
   after insert or update of dia_semana_id,
                             desde_el_dia,
                             hasta_el_dia,
                             repetir_cada_semanas,
                             numero_iteraciones,
                             hora_inicio,
                             hora_fin
   ON UJI_HORARIOS.HOR_ITEMS
begin
   declare
      v_aux           number;
      v_cuantos       number;
      v_num_items     number;
      v_id            number;
      v_id_dest       number;
      v_hora_ini      date;
      v_hora_fin      date;
      v_dia_ini       date;
      v_dia_fin       date;
      v_ult_orden     number;
      v_dia_item      number;
      v_dia_Detalle   number;
      v_semestre      number;
      v_dia_primero   date;
      v_matr_id       horarios_util.ident_arr2;

      cursor reg (v_rowid rowid) is
         select *
         from   uji_horarios.hor_items
         where  rowid = v_rowid;

      cursor lista_detalle (
         p_id                     in   number,
         p_detalle_manual         in   number,
         p_dia_semana_id          in   number,
         p_semestre_id            in   number,
         p_numero_iteraciones     in   number,
         p_repetir_cada_Semanas   in   number,
         p_hasta_el_dia           in   date,
         p_desde_el_dia           in   date,
         p_subgrupo_id            in   number,
         p_tipo_subgrupo_id       in   varchar2,
         p_grupo_id               in   varchar2
      ) is
         select   todo.*, row_number () over (partition by id order by fecha) orden_final
         from     (select distinct id, fecha, docencia_paso_1, docencia_paso_2, docencia, orden_id, numero_iteraciones,
                                   repetir_cada_semanas, fecha_inicio, fecha_fin, semestre_id, grupo_id,
                                   tipo_subgrupo_id, subgrupo_id, dia_semana_id, tipo_dia, festivos
                   from            (select i.id, fecha, docencia docencia_paso_1,
                                           decode (nvl (d.repetir_cada_semanas, 1),
                                                   1, docencia,
                                                   decode (mod (orden_id, d.repetir_cada_semanas), 1, docencia, 'N')
                                                  ) docencia_paso_2,
                                           decode
                                                 (tipo_dia,
                                                  'F', 'N',
                                                  decode (d.numero_iteraciones,
                                                          null, decode (nvl (d.repetir_cada_semanas, 1),
                                                                        1, docencia,
                                                                        decode (mod (orden_id, d.repetir_cada_semanas),
                                                                                1, docencia,
                                                                                'N'
                                                                               )
                                                                       ),
                                                          decode (sign (((orden_id - festivos) / d.repetir_cada_semanas
                                                                        )
                                                                        - (d.numero_iteraciones)),
                                                                  1, 'N',
                                                                  decode (nvl (d.repetir_cada_semanas, 1),
                                                                          1, docencia,
                                                                          decode (mod (orden_id, d.repetir_cada_semanas),
                                                                                  1, docencia,
                                                                                  'N'
                                                                                 )
                                                                         )
                                                                 )
                                                         )
                                                 ) docencia,
                                           d.orden_id, d.numero_iteraciones, d.repetir_cada_semanas, d.fecha_inicio,
                                           d.fecha_fin, d.estudio_id, d.semestre_id, d.asignatura_id, d.grupo_id,
                                           d.tipo_subgrupo_id, d.subgrupo_id, d.dia_semana_id, tipo_dia, festivos
                                    from   (select id, fecha,
                                                   hor_contar_festivos (decode (x.desde_el_dia,
                                                                                null, fecha_inicio,
                                                                                x.desde_el_dia
                                                                               ),
                                                                        x.fecha, x.dia_semana_id,
                                                                        x.repetir_cada_semanas) festivos,
                                                   row_number () over (partition by decode
                                                                                      (decode
                                                                                             (sign (x.fecha
                                                                                                    - fecha_inicio),
                                                                                              -1, 'N',
                                                                                              decode (sign (fecha_fin
                                                                                                            - x.fecha),
                                                                                                      -1, 'N',
                                                                                                      'S'
                                                                                                     )
                                                                                             ),
                                                                                       'S', decode
                                                                                          (decode
                                                                                              (sign
                                                                                                  (x.fecha
                                                                                                   - decode
                                                                                                        (x.desde_el_dia,
                                                                                                         null, fecha_inicio,
                                                                                                         x.desde_el_dia
                                                                                                        )),
                                                                                               -1, 'N',
                                                                                               decode
                                                                                                  (sign
                                                                                                      (decode
                                                                                                          (x.hasta_el_dia,
                                                                                                           null, fecha_fin,
                                                                                                           x.hasta_el_dia
                                                                                                          )
                                                                                                       - x.fecha),
                                                                                                   -1, 'N',
                                                                                                   'S'
                                                                                                  )
                                                                                              ),
                                                                                           'S', 'S',
                                                                                           'N'
                                                                                          ),
                                                                                       'N'
                                                                                      ), id, estudio_id, semestre_id, asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo_id, dia_semana_id order by fecha)
                                                                                                               orden_id,
                                                   decode (hor_f_fecha_entre (x.fecha, fecha_inicio, fecha_fin),
                                                           'S', decode (hor_f_fecha_entre (x.fecha,
                                                                                           decode (desde_el_dia,
                                                                                                   null, fecha_inicio,
                                                                                                   desde_el_dia
                                                                                                  ),
                                                                                           decode (hasta_el_dia,
                                                                                                   null, fecha_fin,
                                                                                                   hasta_el_dia
                                                                                                  )),
                                                                        'S', 'S',
                                                                        'N'
                                                                       ),
                                                           'N'
                                                          ) docencia,
                                                   estudio_id, semestre_id, grupo_id, tipo_subgrupo_id, subgrupo_id,
                                                   dia_semana_id, asignatura_id, fecha_inicio, fecha_fin,
                                                   fecha_examenes_inicio, fecha_examenes_fin, desde_el_dia,
                                                   hasta_el_dia, repetir_cada_semanas, numero_iteraciones,
                                                   detalle_manual, tipo_dia, dia_semana
                                            from   (select distinct i.id, null estudio_id, i.semestre_id, i.grupo_id,
                                                                    i.tipo_subgrupo_id, i.subgrupo_id, i.dia_semana_id,
                                                                    null asignatura_id, fecha_inicio, fecha_fin,
                                                                    fecha_examenes_inicio, fecha_examenes_fin,
                                                                    decode (sign (i.desde_el_dia - fecha_inicio),
                                                                            -1, fecha_inicio,
                                                                            i.desde_el_dia
                                                                           ) desde_el_dia,
                                                                    hasta_el_dia, repetir_cada_semanas,
                                                                    numero_iteraciones, detalle_manual, c.fecha,
                                                                    tipo_dia, dia_semana
                                                    from            hor_v_fechas_estudios s,
                                                                    (select p_id id, p_detalle_manual detalle_manual,
                                                                            p_dia_semana_id dia_semana_id,
                                                                            p_semestre_id semestre_id,
                                                                            p_numero_iteraciones numero_iteraciones,
                                                                            p_repetir_cada_semanas repetir_cada_semanas,
                                                                            p_hasta_el_dia hasta_el_dia,
                                                                            p_desde_el_dia desde_el_dia,
                                                                            p_subgrupo_id subgrupo_id,
                                                                            p_tipo_subgrupo_id tipo_subgrupo_id,
                                                                            p_grupo_id grupo_id
                                                                     from   dual) i,
                                                                    hor_ext_calendario c
                                                    where           i.semestre_id = s.semestre_id
                                                    and             trunc (c.fecha) between fecha_inicio
                                                                                        and decode (fecha_examenes_fin,
                                                                                                    null, fecha_fin,
                                                                                                    fecha_examenes_fin
                                                                                                   )
                                                    and             c.dia_semana_id = i.dia_semana_id
                                                    and             tipo_dia in ('L', 'E', 'F')
                                                    and             vacaciones = 0
                                                    and             s.tipo_estudio_id = 'G'
                                                    and             detalle_manual = 0
                                                    and             s.estudio_id in (
                                                                          select estudio_id
                                                                          from   hor_items_asignaturas
                                                                          where  item_id = i.id
                                                                          and    curso_id = s.curso_id)
                                                                                                       --and i.id = 559274
                                                   ) x) d,
                                           (select p_id id, p_detalle_manual detalle_manual,
                                                   p_dia_semana_id dia_semana_id, p_semestre_id semestre_id,
                                                   p_numero_iteraciones numero_iteraciones,
                                                   p_repetir_cada_semanas repetir_cada_semanas,
                                                   p_hasta_el_dia hasta_el_dia, p_desde_el_dia desde_el_dia,
                                                   p_subgrupo_id subgrupo_id, p_tipo_subgrupo_id tipo_subgrupo_id,
                                                   p_grupo_id grupo_id
                                            from   dual) i
                                    where  i.semestre_id = d.semestre_id
                                    and    i.grupo_id = d.grupo_id
                                    and    i.tipo_subgrupo_id = d.tipo_subgrupo_id
                                    and    i.subgrupo_id = d.subgrupo_id
                                    and    i.dia_semana_id = d.dia_semana_id
                                    and    i.detalle_manual = 0
                                    and    i.id = d.id
                                    union all
                                    select distinct c.id, c.fecha, 'N' docencia_paso_1, 'N' docencia_paso_2,
                                                    decode (d.id, null, 'N', 'S') docencia,
                                                    row_number () over (partition by d.item_id order by d.inicio)
                                                                                                               orden_id,
                                                    numero_iteraciones, repetir_cada_semanas, fecha_inicio, fecha_fin,
                                                    estudio_id, semestre_id, asignatura_id, grupo_id, tipo_subgrupo_id,
                                                    subgrupo_id, dia_semana_id, tipo_dia,
                                                    decode (tipo_dia, 'F', 1, 0) festivos
                                    from            (select distinct i.id, c.fecha, numero_iteraciones,
                                                                     repetir_cada_semanas, s.fecha_inicio, s.fecha_fin,
                                                                     null estudio_id, i.semestre_id, null asignatura_id,
                                                                     i.grupo_id, i.tipo_subgrupo_id, i.subgrupo_id,
                                                                     i.dia_semana_id, tipo_dia
                                                     from            hor_v_fechas_estudios s,
                                                                     (select p_id id, p_detalle_manual detalle_manual,
                                                                             p_dia_semana_id dia_semana_id,
                                                                             p_semestre_id semestre_id,
                                                                             p_numero_iteraciones numero_iteraciones,
                                                                             p_repetir_cada_semanas
                                                                                                   repetir_cada_semanas,
                                                                             p_hasta_el_dia hasta_el_dia,
                                                                             p_desde_el_dia desde_el_dia,
                                                                             p_subgrupo_id subgrupo_id,
                                                                             p_tipo_subgrupo_id tipo_subgrupo_id,
                                                                             p_grupo_id grupo_id
                                                                      from   dual) i,
                                                                     hor_ext_calendario c
                                                     where           i.semestre_id = s.semestre_id
                                                     and             trunc (c.fecha) between fecha_inicio
                                                                                         and decode (fecha_examenes_fin,
                                                                                                     null, fecha_fin,
                                                                                                     fecha_examenes_fin
                                                                                                    )
                                                     and             c.dia_semana_id = i.dia_semana_id
                                                     and             tipo_dia in ('L', 'E', 'F')
                                                     and             s.tipo_estudio_id = 'G'
                                                     and             vacaciones = 0
                                                     and             detalle_manual = 1
                                                     and             s.estudio_id in (
                                                                          select estudio_id
                                                                          from   hor_items_asignaturas
                                                                          where  item_id = i.id
                                                                          and    curso_id = s.curso_id)) c,
                                                    hor_items_detalle d
                                    where           c.id = d.item_id(+)
                                    and             trunc (c.fecha) = trunc (d.inicio(+)))
                   where           id = p_id
                   and             docencia = 'S') todo
         order by 18;

      cursor lista_detalla (p_item in number) is
         select   id, inicio, fin
         from     hor_items_Detalle
         where    item_id = p_item
         order by id;

      function buscar_item (p_item_id in number, p_orden in number)
         return number is
         v_aux   number;

         cursor lista_detalle_ordenado is
            select id, row_number () over (partition by item_id order by trunc (inicio)) orden
            from   hor_items_detalle
            where  item_id = p_item_id;
      begin
         v_aux := -1;

         for det in lista_detalle_ordenado loop
            if det.orden = p_orden then
               v_aux := det.id;
            end if;
         end loop;

         return v_aux;
      exception
         when no_data_found then
            return (-1);
         when others then
            return (0);
      end;

      procedure preparar_Fechas (p_item_id in number) is
         cursor lista_detalle_ordenado is
            select   id, inicio
            from     hor_items_detalle
            where    item_id = p_item_id
            order by trunc (inicio);

         v_aux   number;
      begin
         v_aux := 0;

         for det in lista_detalle_ordenado loop
            v_aux := v_aux + 1;
            v_matr_id (v_aux) := det.id;
         end loop;
      end;

      function buscar_item_v2 (p_item_id in number, p_orden in number)
         return number is
         v_aux   number;
      begin
         if p_orden <= v_matr_id.count then
            v_aux := v_matr_id (p_orden);
         else
            v_aux := 0;
         end if;

         return v_aux;
      exception
         when others then
            return (0);
      end;
   begin
      for i in 1 .. mutante_items.v_num loop
         for v_reg in reg (mutante_items.v_var_tabla (i)) loop
            if v_reg.detalle_manual = 0 then
               v_hora_ini := v_Reg.hora_inicio;
               v_hora_fin := v_Reg.hora_fin;
               preparar_fechas (v_reg.id);

               for x in lista_detalle (v_reg.id, v_reg.detalle_manual, v_reg.dia_semana_id, v_reg.semestre_id,
                                       v_reg.numero_iteraciones, v_reg.repetir_cada_Semanas, v_reg.hasta_el_dia,
                                       v_Reg.desde_el_dia, v_reg.subgrupo_id, v_reg.tipo_subgrupo_id, v_reg.grupo_id) loop
                  --v_id := buscar_item (v_Reg.id, x.orden_final);
                  v_id := buscar_item_v2 (v_Reg.id, x.orden_final);
                  dbms_output.put_line ('--------------------');
                  /*
                  dbms_output.put_line (v_reg.id || ', ' || v_reg.detalle_manual || ', ' || v_reg.dia_semana_id || ', '
                                        || v_reg.semestre_id || ', ' || v_reg.numero_iteraciones || ', '
                                        || v_reg.repetir_cada_Semanas || ', ' || v_reg.hasta_el_dia || ', '
                                        || v_Reg.desde_el_dia || ', ' || v_reg.subgrupo_id || ', '
                                        || v_reg.tipo_subgrupo_id || ', ' || v_reg.grupo_id);
                                        */
                  dbms_output.put_line (v_Reg.id || ' ' || v_id || ' ' || to_char (x.fecha, 'dd/mm/yyyy') || ' '
                                        || x.orden_final);

                  if v_id > 0 then
                     /* por defecto actualiza fechas */
                     select inicio, fin
                     into   v_dia_ini, v_dia_fin
                     from   hor_items_detalle
                     where  id = v_id;

                     dbms_output.put_line ('update ' || x.orden_final || ' - de ' || to_char (v_dia_ini, 'dd/mm/yyyy')
                                           || ' a ' || to_char (x.fecha, 'dd/mm/yyyy') || ' - '
                                           || to_char (v_Reg.desde_el_dia, 'dd/mm/yyyy'));

                     update hor_items_detalle
                     set inicio =
                            to_date (to_char (x.fecha, 'dd/mm/yyyy') || ' ' || to_char (v_hora_ini, 'hh24:mi:ss'),
                                     'dd/mm/yyyy hh24:mi:ss'),
                         fin =
                            to_date (to_char (x.fecha, 'dd/mm/yyyy') || ' ' || to_char (v_hora_fin, 'hh24:mi:ss'),
                                     'dd/mm/yyyy hh24:mi:ss')
                     where  id = v_id;
                  elsif v_id = -1 then
                     dbms_output.put_line ('insertar ' || x.orden_final);

                     begin
                        /* solo inserta si no le quedan fechas por actualizar */
                        v_aux := uji_horarios.hibernate_sequence.nextval;

                        insert into hor_items_detalle
                                    (id, item_id,
                                     inicio,
                                     fin
                                    )
                        values      (v_aux, v_Reg.id,
                                     to_date (to_char (x.fecha, 'dd/mm/yyyy') || ' '
                                              || to_char (v_hora_ini, 'hh24:mi:ss'),
                                              'dd/mm/yyyy hh24:mi:ss'),
                                     to_date (to_char (x.fecha, 'dd/mm/yyyy') || ' '
                                              || to_char (v_hora_fin, 'hh24:mi:ss'),
                                              'dd/mm/yyyy hh24:mi:ss')
                                    );
                     exception
                        when others then
                           null;
                     end;
                  end if;

                  if x.orden_id > nvl (v_ult_orden, 0) then
                     v_ult_orden := x.orden_final;
                  end if;
               end loop;

               /* y solo borra si sobran al final */
               delete      hor_items_detalle
               where       id in (
                              select id
                              from   (select id, inicio, fin,
                                             row_number () over (partition by item_id order by trunc (inicio)) orden
                                      from   hor_items_detalle
                                      where  item_id = v_reg.id)
                              where  orden > v_ult_orden);
            else
               /* detalle manual, si ha cambiado de dia de semana u hora, aplicar este cambio a las sesiones detalladas */
               dbms_output.put_line (1);

               begin
                  v_cuantos := 0;
                  --v_dia_item := to_number (to_char (v_Reg.hora_inicio, 'd'));
                  v_dia_item := v_reg.dia_Semana_id;

                  /*
                  select distinct to_number(to_char(inicio, 'd'))
                  into v_dia_detalle
                  from hor_items_detalle
                  where item_id = v_reg.id
                  and to_number(to_char(v_Reg.hora_inicio, 'd')) <> v_dia_item
                  and rownum <= 1;
                  */
                  declare
                     cursor lista_dias_semana_2 is
                        select distinct to_number (to_char (inicio, 'd')) dia_semana
                        from            hor_items_detalle
                        where           item_id = v_reg.id
                        and             to_number (to_char (inicio, 'd')) <> v_dia_item;
                  begin
                     dbms_output.put_line ('11-' || v_dia_item);

                     for det in lista_dias_Semana_2 loop
                        dbms_output.put_line (111);
                        v_cuantos := v_cuantos + 1;
                        v_dia_detalle := det.dia_semana;

                        if v_dia_item <> v_dia_detalle then
                           update hor_items_detalle
                           set inicio = inicio + (v_dia_item - v_dia_detalle),
                               fin = fin + (v_dia_item - v_dia_detalle)
                           where  item_id = v_reg.id
                           and    to_number (to_char (inicio, 'd')) = det.dia_Semana;
                        end if;
                     end loop;
                  end;

                  if v_cuantos > 0 then
                     delete      uji_horarios.hor_items_detalle
                     where       id in (select d.id
                                        from   uji_horarios.hor_items_detalle d,
                                               uji_horarios.hor_ext_calendario c
                                        where  item_id = v_Reg.id
                                        and    trunc (inicio) = fecha
                                        and    tipo_dia in ('F', 'N'));
                  end if;
               exception
                  when no_data_found then
                     null;
               end;

               dbms_output.put_line (2);

               /* detalle manual, tambien hay que actualizar la hora inicio y hora final */
               begin
                  select inicio, fin
                  into   v_hora_ini, v_hora_fin
                  from   hor_items_detalle
                  where  item_id = v_Reg.id
                  and    rownum <= 1;

                  if    to_char (v_hora_ini, 'hh24:mi:ss') <> to_char (v_reg.hora_inicio, 'hh24:mi:ss')
                     or to_char (v_hora_fin, 'hh24:mi:ss') <> to_char (v_reg.hora_fin, 'hh24:mi:ss') then
                     update hor_items_detalle
                     set inicio =
                            to_date (to_char (inicio, 'dd/mm/yyyy') || ' ' || to_char (v_reg.hora_inicio, 'hh24:mi:ss'),
                                     'dd/mm/yyyy hh24:mi:ss'),
                         fin =
                            to_date (to_char (fin, 'dd/mm/yyyy') || ' ' || to_char (v_reg.hora_fin, 'hh24:mi:ss'),
                                     'dd/mm/yyyy hh24:mi:ss')
                     where  item_id = v_reg.id;
                  end if;
               exception
                  when no_data_found then
                     null;
               end;
            end if;
         end loop;
      end loop;
   end;
end mutante_3_final_2014;




