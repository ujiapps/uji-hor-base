create or replace force view UJI_HORARIOS.HOR_V_ITEMS_DETALLE(ID, FECHA, DOCENCIA_PASO_1, DOCENCIA_PASO_2, DOCENCIA, ORDEN_ID, NUMERO_ITERACIONES, REPETIR_CADA_SEMANAS, FECHA_INICIO, FECHA_FIN, ESTUDIO_ID, SEMESTRE_ID, ASIGNATURA_ID,
GRUPO_ID, TIPO_SUBGRUPO_ID, SUBGRUPO_ID, DIA_SEMANA_ID, TIPO_DIA, FESTIVOS) as
   select i.id, fecha, docencia docencia_paso_1, decode(nvl(d.repetir_cada_semanas, 1), 1, docencia, decode(mod(orden_id, d.repetir_cada_semanas), 1, docencia, 'N')) docencia_paso_2,
          decode(tipo_dia, 'F', 'N', decode(d.numero_iteraciones, null, decode(nvl(d.repetir_cada_semanas, 1), 1, docencia, decode(mod(orden_id, d.repetir_cada_semanas), 1, docencia, 'N')), decode(sign(((orden_id - festivos) / d.repetir_cada_Semanas) - (d.numero_iteraciones)), 1, 'N', decode(nvl(d.repetir_cada_semanas, 1), 1, docencia, decode(mod(orden_id, d.repetir_cada_semanas), 1, docencia, 'N')))))
             docencia, d.orden_id, d.numero_iteraciones, d.repetir_cada_semanas, d.fecha_inicio, d.fecha_fin, d.estudio_id, d.semestre_id, d.asignatura_id, d.grupo_id, d.tipo_subgrupo_id, d.subgrupo_id, d.dia_semana_id, tipo_dia, festivos
     from (select id, fecha, hor_contar_festivos(nvl(x.desde_el_dia, fecha_inicio), x.fecha, x.dia_semana_id, x.repetir_cada_semanas) festivos,
                  row_number() over (partition by decode(decode(sign(x.fecha - fecha_inicio), -1, 'N', decode(sign(fecha_fin - x.fecha), -1, 'N', 'S')), 'S', decode(decode(sign(x.fecha - nvl(x.desde_el_dia, fecha_inicio)), -1, 'N', decode(sign(nvl(x.hasta_el_dia, fecha_fin) - x.fecha), -1, 'N', 'S')), 'S', 'S', 'N'), 'N'), id, estudio_id, semestre_id, asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo_id, dia_semana_id order by fecha) orden_id, decode(hor_f_fecha_entre(x.fecha, fecha_inicio, fecha_fin), 'S', decode(hor_f_fecha_entre(x.fecha, nvl(desde_el_dia, fecha_inicio), nvl(hasta_el_dia, fecha_fin)), 'S', 'S', 'N'), 'N') docencia, estudio_id,
                  semestre_id, grupo_id, tipo_subgrupo_id, subgrupo_id, dia_Semana_id, asignatura_id, fecha_inicio, fecha_fin, fecha_examenes_inicio, fecha_examenes_fin, desde_el_dia, hasta_el_dia, repetir_cada_semanas, numero_iteraciones,
                  detalle_manual, tipo_dia, dia_semana
             from (select distinct i.id, null estudio_id, i.semestre_id, i.grupo_id, i.tipo_subgrupo_id, i.subgrupo_id, i.dia_Semana_id, null asignatura_id, fecha_inicio, fecha_fin, fecha_examenes_inicio, fecha_examenes_fin,
                                   decode(sign(nvl(i.desde_el_dia, fecha_inicio) - fecha_inicio), -1, fecha_inicio, i.desde_el_dia) desde_el_dia, hasta_el_dia, repetir_cada_semanas, numero_iteraciones, detalle_manual, c.fecha, tipo_dia,
                                   dia_semana
                     from hor_v_fechas_estudios s, hor_items i, hor_ext_calendario c
                    where i.semestre_id = s.semestre_id
                      and trunc(c.fecha) between fecha_inicio and nvl(fecha_examenes_fin, fecha_fin)
                      and c.dia_semana_id = i.dia_semana_id
                      and tipo_dia in ('L', 'E', 'F')
                      and vacaciones = 0
                      and s.tipo_estudio_id = 'G'
                      and detalle_manual = 0
                      and s.estudio_id in (select estudio_id
                                             from hor_items_asignaturas
                                            where item_id = i.id
                                              and curso_id = s.curso_id) --and i.id = 559274
                                                                        ) x) d, hor_items i
    where i.semestre_id = d.semestre_id
      and i.grupo_id = d.grupo_id
      and i.tipo_subgrupo_id = d.tipo_subgrupo_id
      and i.subgrupo_id = d.subgrupo_id
      and i.dia_semana_id = d.dia_semana_id
      and i.detalle_manual = 0
      and i.id = d.id
   union all
   select c.id, c.fecha, 'N' docencia_paso_1, 'N' docencia_paso_2, decode(d.id, null, 'N', 'S') docencia, 1 orden_id, numero_iteraciones, repetir_cada_semanas, fecha_inicio, fecha_fin, estudio_id, semestre_id, asignatura_id, grupo_id,
          tipo_subgrupo_id, subgrupo_id, dia_semana_id, tipo_dia, decode(tipo_dia, 'F', 1, 0) festivos
     from (select distinct i.id, c.fecha, numero_iteraciones, repetir_cada_semanas, s.fecha_inicio, s.fecha_fin, null estudio_id, i.semestre_id, null asignatura_id, i.grupo_id, i.tipo_subgrupo_id, i.subgrupo_id, i.dia_semana_id, tipo_dia
             from hor_v_fechas_estudios s, hor_items i, hor_ext_calendario c
            where i.semestre_id = s.semestre_id
              and trunc(c.fecha) between fecha_inicio and nvl(fecha_examenes_fin, fecha_fin)
              and c.dia_semana_id = i.dia_semana_id
              and tipo_dia in ('L', 'E', 'F')
              and s.tipo_estudio_id = 'G'
              and vacaciones = 0
              and detalle_manual = 1
              and s.estudio_id in (select estudio_id
                                     from hor_items_asignaturas
                                    where item_id = i.id
                                      and curso_id = s.curso_id)) c, hor_items_detalle d
    where c.id = d.item_id(+)
      and trunc(c.fecha) = trunc(d.inicio(+));


create or replace force view UJI_HORARIOS.HOR_V_ITEMS_DETALLE_COMPLETA(ID, FECHA, DOCENCIA_PASO_1, DOCENCIA_PASO_2, DOCENCIA, ORDEN_ID, NUMERO_ITERACIONES, REPETIR_CADA_SEMANAS, FECHA_INICIO, FECHA_FIN, ESTUDIO_ID, SEMESTRE_ID, CURSO_ID,
ASIGNATURA_ID, GRUPO_ID, TIPO_SUBGRUPO_ID, SUBGRUPO_ID, DIA_SEMANA_ID, TIPO_DIA, FESTIVOS) as
   select i.id, fecha, docencia docencia_paso_1, decode(nvl(d.repetir_cada_semanas, 1), 1, docencia, decode(mod(orden_id, d.repetir_cada_semanas), 1, docencia, 'N')) docencia_paso_2,
          decode(tipo_dia, 'F', 'N', decode(d.numero_iteraciones, null, decode(nvl(d.repetir_cada_semanas, 1), 1, docencia, decode(mod(orden_id, d.repetir_cada_semanas), 1, docencia, 'N')), decode(sign(((orden_id - festivos) / d.repetir_cada_Semanas) - (d.numero_iteraciones)), 1, 'N', decode(nvl(d.repetir_cada_semanas, 1), 1, docencia, decode(mod(orden_id, d.repetir_cada_semanas), 1, docencia, 'N'))))) docencia, d.orden_id, d.numero_iteraciones, d.repetir_cada_semanas, d.fecha_inicio, d.fecha_fin, d.estudio_id, d.semestre_id, d.curso_id, d.asignatura_id, d.grupo_id, d.tipo_subgrupo_id, d.subgrupo_id, d.dia_semana_id,
          tipo_dia, festivos
     from (select id, fecha, hor_contar_festivos(nvl(x.desde_el_dia, fecha_inicio), x.fecha, x.dia_semana_id, x.repetir_cada_semanas) festivos,
                  row_number() over (partition by decode(decode(sign(x.fecha - fecha_inicio), -1, 'N', decode(sign(fecha_fin - x.fecha), -1, 'N', 'S')), 'S', decode(decode(sign(x.fecha - nvl(x.desde_el_dia, fecha_inicio)), -1, 'N', decode(sign(nvl(x.hasta_el_dia, fecha_fin) - x.fecha), -1, 'N', 'S')), 'S', 'S', 'N'), 'N'), id, estudio_id, semestre_id, curso_id, asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo_id, dia_semana_id order by fecha) orden_id, decode(hor_f_fecha_entre(x.fecha, fecha_inicio, fecha_fin), 'S', decode(hor_f_fecha_entre(x.fecha, nvl(desde_el_dia, fecha_inicio), nvl(hasta_el_dia, fecha_fin)), 'S', 'S', 'N'), 'N') docencia, estudio_id,
                  curso_id, semestre_id, grupo_id, tipo_subgrupo_id, subgrupo_id, dia_Semana_id, asignatura_id, fecha_inicio, fecha_fin, fecha_examenes_inicio, fecha_examenes_fin, desde_el_dia, hasta_el_dia, repetir_cada_semanas,
                  numero_iteraciones, detalle_manual, tipo_dia, dia_semana
           from (select i.id, ia.estudio_id, ia.curso_id, i.semestre_id, i.grupo_id, i.tipo_subgrupo_id, i.subgrupo_id, i.dia_Semana_id, ia.asignatura_id, fecha_inicio, fecha_fin, fecha_examenes_inicio, fecha_examenes_fin, i.desde_el_dia,
                        hasta_el_dia, repetir_cada_semanas, numero_iteraciones, detalle_manual, c.fecha, tipo_dia, dia_semana
                 from hor_estudios e, hor_semestres_detalle s, hor_items i, hor_items_asignaturas ia, hor_ext_calendario c
                 where i.id = ia.item_id
                   and e.tipo_id = s.tipo_estudio_id
                   and ia.estudio_id = e.id
                   and i.semestre_id = s.semestre_id
                   and trunc(c.fecha) between fecha_inicio and nvl(fecha_examenes_fin, fecha_fin)
                   and c.dia_semana_id = i.dia_semana_id
                   and tipo_dia in ('L', 'E', 'F')
                   and vacaciones = 0
                   and detalle_manual = 0) x) d, hor_items i, hor_items_asignaturas ia
    where i.id = ia.item_id
      and ia.estudio_id = d.estudio_id
      and ia.curso_id = d.curso_id
      and i.semestre_id = d.semestre_id
      and ia.asignatura_id = d.asignatura_id
      and i.grupo_id = d.grupo_id
      and i.tipo_subgrupo_id = d.tipo_subgrupo_id
      and i.subgrupo_id = d.subgrupo_id
      and i.dia_semana_id = d.dia_semana_id
      and i.detalle_manual = 0
      and i.id = d.id
   union all
   select c.id, c.fecha, 'N' docencia_paso_1, 'N' docencia_paso_2, decode(d.id, null, 'N', 'S') docencia, 1 orden_id, numero_iteraciones, repetir_cada_semanas, fecha_inicio, fecha_fin, estudio_id, semestre_id, curso_id, asignatura_id,
          grupo_id, tipo_subgrupo_id, subgrupo_id, dia_semana_id, tipo_dia, decode(tipo_dia, 'F', 1, 0) festivos
   from (select i.id, c.fecha, numero_iteraciones, repetir_cada_semanas, s.fecha_inicio, s.fecha_fin, ia.estudio_id, i.semestre_id, ia.curso_id, ia.asignatura_id, i.grupo_id, i.tipo_subgrupo_id, i.subgrupo_id, i.dia_semana_id, tipo_dia
           from hor_estudios e, hor_semestres_detalle s, hor_items i, hor_items_asignaturas ia, hor_ext_calendario c
          where i.id = ia.item_id
            and e.tipo_id = s.tipo_estudio_id
            and ia.estudio_id = e.id
            and i.semestre_id = s.semestre_id
            and trunc(c.fecha) between fecha_inicio and nvl(fecha_examenes_fin, fecha_fin)
            and c.dia_semana_id = i.dia_semana_id
            and tipo_dia in ('L', 'E', 'F')
            and vacaciones = 0
            and detalle_manual = 1) c, hor_items_detalle d
   where c.id = d.item_id(+)
     and trunc(c.fecha) = trunc(d.inicio(+));

