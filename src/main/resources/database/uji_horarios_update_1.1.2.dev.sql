ALTER TABLE UJI_HORARIOS.HOR_PROFESORES
 ADD (creditos_maximos  NUMBER);

ALTER TABLE UJI_HORARIOS.HOR_PROFESORES
 ADD (creditos_reduccion  NUMBER);

CREATE TABLE uji_horarios.hor_tipos_examenes 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2 (100)  NOT NULL , 
     por_defecto NUMBER DEFAULT 0  NOT NULL 
    ) 
;



ALTER TABLE uji_horarios.hor_tipos_examenes 
    ADD CONSTRAINT hor_tipos_examenes_PK PRIMARY KEY ( id ) ;



Insert into UJI_HORARIOS.HOR_TIPOS_EXAMENES
   (ID, NOMBRE, POR_DEFECTO)
 Values
   (1, 'TEO', 1);
Insert into UJI_HORARIOS.HOR_TIPOS_EXAMENES
   (ID, NOMBRE, POR_DEFECTO)
 Values
   (2, 'PRO', 0);
Insert into UJI_HORARIOS.HOR_TIPOS_EXAMENES
   (ID, NOMBRE, POR_DEFECTO)
 Values
   (3, 'LAB', 0);
Insert into UJI_HORARIOS.HOR_TIPOS_EXAMENES
   (ID, NOMBRE, POR_DEFECTO)
 Values
   (4, 'ORA', 0);
Insert into UJI_HORARIOS.HOR_TIPOS_EXAMENES
   (ID, NOMBRE, POR_DEFECTO)
 Values
   (5, 'PRA', 0);
Insert into UJI_HORARIOS.HOR_TIPOS_EXAMENES
   (ID, NOMBRE, POR_DEFECTO)
 Values
   (6, 'ALT', 0);
COMMIT;

ALTER TABLE UJI_HORARIOS.HOR_EXAMENES
 ADD (tipo_examen_id  NUMBER                        DEFAULT 1                     NOT NULL);

 
CREATE INDEX uji_horarios.hor_examenes_tipo_Exa_IDX ON uji_horarios.hor_examenes 
    ( 
     tipo_examen_id ASC 
    ) 
;


ALTER TABLE uji_horarios.hor_examenes 
    ADD CONSTRAINT hor_exa_tipos_exa_FK FOREIGN KEY 
    ( 
     tipo_examen_id
    ) 
    REFERENCES uji_horarios.hor_tipos_examenes 
    ( 
     id
    ) 
;


CREATE OR REPLACE VIEW hor_v_fechas_convocatorias  AS
SELECT DISTINCT convocatoria_id id,
  convocatoria_nombre nombre,
  s.fecha_examenes_inicio,
  s.fecha_examenes_fin
FROM hor_examenes e,
  hor_semestres_detalle s
WHERE (e.convocatoria_id = 9
AND semestre_id          = 1)
OR (e.convocatoria_id    = 11
AND semestre_id          = 2)
UNION ALL
SELECT DISTINCT convocatoria_id,
  convocatoria_nombre,
  s.fechas_examenes_inicio_c2,
  s.fechas_examenes_fin_c2
FROM hor_examenes e,
  hor_semestres_detalle s
WHERE e.convocatoria_id = 10
AND semestre_id         = 1 ;


ALTER TABLE UJI_HORARIOS.HOR_PROFESORES
 ADD (categoria_id  VARCHAR2(10));

ALTER TABLE UJI_HORARIOS.HOR_PROFESORES
 ADD (categoria_nombre  VARCHAR2(1000));

ALTER TABLE UJI_HORARIOS.HOR_TIPOS_EXAMENES
 ADD (codigo  VARCHAR2(10));


update hor_tipos_examenes
set codigo= nombre;

commit;


 