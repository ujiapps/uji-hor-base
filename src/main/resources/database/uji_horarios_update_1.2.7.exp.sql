ALTER TABLE UJI_HORARIOS.HOR_ITEMS DROP COLUMN CURSO_ID;

ALTER TABLE UJI_HORARIOS.HOR_ITEMS DROP COLUMN CARACTER_ID;

ALTER TABLE UJI_HORARIOS.HOR_ITEMS DROP COLUMN CARACTER;

create or replace force view UJI_HORARIOS.HOR_V_CURSOS(ESTUDIO_ID, ESTUDIO, CURSO_ID) as
   select distinct hor_items_asignaturas.estudio_id, hor_items_asignaturas.estudio, hor_items_asignaturas.curso_id
     from hor_items_asignaturas, hor_items
    where hor_items.id = hor_items_asignaturas.item_id
      and hor_items_asignaturas.curso_id < 7;

create or replace force view UJI_HORARIOS.HOR_V_ITEMS_DETALLE_COMPLETA(ID, FECHA, DOCENCIA_PASO_1, DOCENCIA_PASO_2, DOCENCIA, ORDEN_ID, NUMERO_ITERACIONES, REPETIR_CADA_SEMANAS, FECHA_INICIO, FECHA_FIN, ESTUDIO_ID, SEMESTRE_ID, CURSO_ID,
ASIGNATURA_ID, GRUPO_ID, TIPO_SUBGRUPO_ID, SUBGRUPO_ID, DIA_SEMANA_ID, TIPO_DIA, FESTIVOS) as
   select i.id, fecha, docencia docencia_paso_1, decode(nvl(d.repetir_cada_semanas, 1), 1, docencia, decode(mod(orden_id, d.repetir_cada_semanas), 1, docencia, 'N')) docencia_paso_2,
          decode(tipo_dia, 'F', 'N', decode(d.numero_iteraciones, null, decode(nvl(d.repetir_cada_semanas, 1), 1, docencia, decode(mod(orden_id, d.repetir_cada_semanas), 1, docencia, 'N')), decode(sign(((orden_id - festivos) / d.repetir_cada_Semanas) - (d.numero_iteraciones)), 1, 'N', decode(nvl(d.repetir_cada_semanas, 1), 1, docencia, decode(mod(orden_id, d.repetir_cada_semanas), 1, docencia, 'N'))))) docencia, d.orden_id, d.numero_iteraciones, d.repetir_cada_semanas, d.fecha_inicio, d.fecha_fin, d.estudio_id, d.semestre_id, d.curso_id, d.asignatura_id, d.grupo_id, d.tipo_subgrupo_id, d.subgrupo_id, d.dia_semana_id,
          tipo_dia, festivos
     from (select id, fecha, hor_contar_festivos(nvl(x.desde_el_dia, fecha_inicio), x.fecha, x.dia_semana_id, x.repetir_cada_semanas) festivos,
                  row_number() over (partition by decode(decode(sign(x.fecha - fecha_inicio), -1, 'N', decode(sign(fecha_fin - x.fecha), -1, 'N', 'S')), 'S', decode(decode(sign(x.fecha - nvl(x.desde_el_dia, fecha_inicio)), -1, 'N', decode(sign(nvl(x.hasta_el_dia, fecha_fin) - x.fecha), -1, 'N', 'S')), 'S', 'S', 'N'), 'N'), id, estudio_id, semestre_id, curso_id, asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo_id, dia_semana_id order by fecha) orden_id, decode(hor_f_fecha_entre(x.fecha, fecha_inicio, fecha_fin), 'S', decode(hor_f_fecha_entre(x.fecha, nvl(desde_el_dia, fecha_inicio), nvl(hasta_el_dia, fecha_fin)), 'S', 'S', 'N'), 'N') docencia, estudio_id,
                  curso_id, semestre_id, grupo_id, tipo_subgrupo_id, subgrupo_id, dia_Semana_id, asignatura_id, fecha_inicio, fecha_fin, fecha_examenes_inicio, fecha_examenes_fin, desde_el_dia, hasta_el_dia, repetir_cada_semanas,
                  numero_iteraciones, detalle_manual, tipo_dia, dia_semana
           from (select i.id, ia.estudio_id, ia.curso_id, i.semestre_id, i.grupo_id, i.tipo_subgrupo_id, i.subgrupo_id, i.dia_Semana_id, ia.asignatura_id, fecha_inicio, fecha_fin, fecha_examenes_inicio, fecha_examenes_fin, i.desde_el_dia,
                        hasta_el_dia, repetir_cada_semanas, numero_iteraciones, detalle_manual, c.fecha, tipo_dia, dia_semana
                 from hor_estudios e, hor_semestres_detalle s, hor_items i, hor_items_asignaturas ia, hor_ext_calendario c
                 where i.id = ia.item_id
                   and e.tipo_id = s.tipo_estudio_id
                   and ia.estudio_id = e.id
                   and i.semestre_id = s.semestre_id
                   and trunc(c.fecha) between fecha_inicio and nvl(fecha_examenes_fin, fecha_fin)
                   and c.dia_semana_id = i.dia_semana_id
                   and tipo_dia in ('L', 'E', 'F')
                   and vacaciones = 0
                   and detalle_manual = 0) x) d, hor_items i, hor_items_asignaturas ia
    where i.id = ia.item_id
      and ia.estudio_id = d.estudio_id
      and ia.curso_id = d.curso_id
      and i.semestre_id = d.semestre_id
      and ia.asignatura_id = d.asignatura_id
      and i.grupo_id = d.grupo_id
      and i.tipo_subgrupo_id = d.tipo_subgrupo_id
      and i.subgrupo_id = d.subgrupo_id
      and i.dia_semana_id = d.dia_semana_id
      and i.detalle_manual = 0
      and i.id = d.id
   union all
   select c.id, c.fecha, 'N' docencia_paso_1, 'N' docencia_paso_2, decode(d.id, null, 'N', 'S') docencia, 1 orden_id, numero_iteraciones, repetir_cada_semanas, fecha_inicio, fecha_fin, estudio_id, semestre_id, curso_id, asignatura_id,
          grupo_id, tipo_subgrupo_id, subgrupo_id, dia_semana_id, tipo_dia, decode(tipo_dia, 'F', 1, 0) festivos
   from (select i.id, c.fecha, numero_iteraciones, repetir_cada_semanas, s.fecha_inicio, s.fecha_fin, ia.estudio_id, i.semestre_id, ia.curso_id, ia.asignatura_id, i.grupo_id, i.tipo_subgrupo_id, i.subgrupo_id, i.dia_semana_id, tipo_dia
           from hor_estudios e, hor_semestres_detalle s, hor_items i, hor_items_asignaturas ia, hor_ext_calendario c
          where i.id = ia.item_id
            and e.tipo_id = s.tipo_estudio_id
            and ia.estudio_id = e.id
            and i.semestre_id = s.semestre_id
            and trunc(c.fecha) between fecha_inicio and nvl(fecha_examenes_fin, fecha_fin)
            and c.dia_semana_id = i.dia_semana_id
            and tipo_dia in ('L', 'E', 'F')
            and vacaciones = 0
            and detalle_manual = 1) c, hor_items_detalle d
   where c.id = d.item_id(+)
     and trunc(c.fecha) = trunc(d.inicio(+));

CREATE OR REPLACE PROCEDURE UJI_HORARIOS.HORARIOS_SEMANA_PDF (

   name_array    IN   euji_util.ident_arr2,
   value_array   IN   euji_util.ident_arr2
) IS
   -- Parametros
   pEstudio    CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pEstudio');
   pSemestre   CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pSemestre');
   pSesion     CONSTANT VARCHAR2 (4000) := euji_util.euji_getparam (name_array, value_array, 'pSesion');
   pGrupo      CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pGrupo');
   pCurso      CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pCurso');
   pSemana     CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pSemana');
   pAgrupacion CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pAgrupacion');
   vCursoAcaNum         number;
   vSesion              number;
   vFechaIniSemestre    date;
   vFechaFinSemestre    date;
   vEsGenerica          boolean         := pSemana = 'G';

     -- Variables
   --  vPerId              per_personas.id%TYPE := euji_get_perid ();
   CURSOR c_items_gen (pSemestre in number) IS
      SELECT aulas.codigo as codigo_aula, asignatura_id, asignatura, estudio, asi.caracter, semestre_id, comun, grupo_id,
             tipo_subgrupo, tipo_subgrupo_id, subgrupo_id, dia_semana_id, hora_inicio, hora_fin, tipo_asignatura_id,
             tipo_asignatura,
             to_date (dia_semana_id + 1 || '/01/2006 ' || to_char (hora_fin, 'hh24:mi'), 'dd/mm/yyyy hh24:mi') fin,
             to_date (dia_semana_id + 1 || '/01/2006 ' || to_char (hora_inicio, 'hh24:mi'),
                      'dd/mm/yyyy hh24:mi') inicio
      FROM   UJI_HORARIOS.HOR_ITEMS items LEFT OUTER JOIN hor_aulas aulas ON (aulas.id = items.aula_planificacion_id)
             INNER JOIN uji_horarios.hor_items_asignaturas asi ON (items.id = asi.item_id)
      WHERE  asi.estudio_id = pEstudio
      AND    asi.curso_id = pCurso
      AND    ',' || pGrupo || ',' like '%,' || items.grupo_id || ',%'
      AND    items.semestre_id = pSemestre
      AND    items.dia_semana_id IS NOT NULL;

   CURSOR c_items_gen_agrupacion (pSemestre in number) IS
      SELECT DISTINCT aulas.codigo as codigo_aula, asi.asignatura_id, asignatura, estudio, asi.caracter, semestre_id, comun, grupo_id,
             tipo_subgrupo, tipo_subgrupo_id, subgrupo_id, dia_semana_id, hora_inicio, hora_fin, tipo_asignatura_id,
             tipo_asignatura,
             to_date (dia_semana_id + 1 || '/01/2006 ' || to_char (hora_fin, 'hh24:mi'), 'dd/mm/yyyy hh24:mi') fin,
             to_date (dia_semana_id + 1 || '/01/2006 ' || to_char (hora_inicio, 'hh24:mi'),
                      'dd/mm/yyyy hh24:mi') inicio
      FROM   UJI_HORARIOS.HOR_ITEMS items LEFT OUTER JOIN hor_aulas aulas ON (aulas.id = items.aula_planificacion_id)
             INNER JOIN uji_horarios.hor_items_asignaturas asi ON (items.id = asi.item_id) JOIN uji_horarios.HOR_V_AGRUPACIONES_ITEMS agr_item on (items.id = AGR_ITEM.ITEM_ID)
      WHERE  asi.estudio_id = pEstudio
      AND    agr_item.agrupacion_id = pAgrupacion
      AND    ',' || pGrupo || ',' like '%,' || items.grupo_id || ',%'
      AND    items.semestre_id = pSemestre
      AND    items.dia_semana_id IS NOT NULL;

   CURSOR c_items_det (vIniDate date, vFinDate date) IS
      SELECT   aulas.codigo as codigo_aula, asignatura_id, asignatura, estudio, grupo_id, tipo_subgrupo,
               tipo_subgrupo_id, subgrupo_id, dia_semana_id, hora_inicio, hora_fin, inicio, fin
      FROM     UJI_HORARIOS.HOR_ITEMS items LEFT OUTER JOIN hor_aulas aulas ON (aulas.id = items.aula_planificacion_id)
               INNER JOIN uji_horarios.hor_items_asignaturas asi ON (items.id = asi.item_id)
               INNER JOIN UJI_HORARIOS.HOR_ITEMS_DETALLE det ON (det.item_id = items.id)
      WHERE    asi.estudio_id = pEstudio
      AND      asi.curso_id = pCurso
      AND      ',' || pGrupo || ',' like '%,' || items.grupo_id || ',%'
      AND      items.semestre_id = pSemestre
      AND      items.dia_semana_id IS NOT NULL
      and      det.inicio > vIniDate
      and      det.fin < vFinDate
      order by det.inicio;

   CURSOR c_items_det_agrupacion (vIniDate date, vFinDate date) IS
      SELECT   DISTINCT aulas.codigo as codigo_aula, asi.asignatura_id, asignatura, estudio, grupo_id, tipo_subgrupo,
               tipo_subgrupo_id, subgrupo_id, dia_semana_id, hora_inicio, hora_fin, inicio, fin
      FROM     UJI_HORARIOS.HOR_ITEMS items LEFT OUTER JOIN hor_aulas aulas ON (aulas.id = items.aula_planificacion_id)
               INNER JOIN uji_horarios.hor_items_asignaturas asi ON (items.id = asi.item_id)
               INNER JOIN UJI_HORARIOS.HOR_ITEMS_DETALLE det ON (det.item_id = items.id)
               JOIN uji_horarios.HOR_V_AGRUPACIONES_ITEMS agr_item on (items.id = AGR_ITEM.ITEM_ID)
      WHERE    asi.estudio_id = pEstudio
      AND      agr_item.agrupacion_id = pAgrupacion
      AND      ',' || pGrupo || ',' like '%,' || items.grupo_id || ',%'
      AND      items.semestre_id = pSemestre
      AND      items.dia_semana_id IS NOT NULL
      and      det.inicio > vIniDate
      and      det.fin < vFinDate
      order by det.inicio;

   CURSOR c_asigs (pSemestre in number) is
      SELECT distinct (asignatura_id), asignatura
      FROM            UJI_HORARIOS.HOR_ITEMS items,
                      uji_horarios.hor_items_asignaturas asi
      WHERE           items.id = asi.item_id
      and             asi.estudio_id = pEstudio
      AND             asi.curso_id = pCurso
      AND             ',' || pGrupo || ',' like '%,' || items.grupo_id || ',%'
      AND             items.semestre_id = pSemestre
      AND             items.dia_semana_id IS NOT NULL
      order by        asignatura_id;

    CURSOR c_asigs_agrupacion (pSemestre in number) is
      SELECT distinct (asi.asignatura_id), asi.asignatura
      FROM            UJI_HORARIOS.HOR_ITEMS items,
                      uji_horarios.hor_items_asignaturas asi,
                      uji_horarios.HOR_V_AGRUPACIONES_ITEMS agr_item
      WHERE           items.id = asi.item_id
      and             items.id = agr_item.item_id
      and             asi.estudio_id = pEstudio
      and             agr_item.agrupacion_id = pAgrupacion
      AND             ',' || pGrupo || ',' like '%,' || items.grupo_id || ',%'
      AND             items.semestre_id = pSemestre
      AND             items.dia_semana_id IS NOT NULL
      order by        asignatura_id;

   CURSOR c_clases_asig (vAsignaturaId varchar2, vSemestre varchar2) is
      SELECT   tipo_subgrupo_id || subgrupo_id subgrupo,
               decode (dia_semana_id, 1, 'L', 2, 'M', 3, 'X', 4, 'J', 5, 'V', '?') dia_sem,
               to_char (hora_fin, 'hh24:mi') hora_fin_str, to_char (hora_inicio, 'hh24:mi') hora_inicio_str
      FROM     UJI_HORARIOS.HOR_ITEMS items,
               uji_horarios.hor_items_asignaturas asi
      WHERE    items.id = asi.item_id
      and      asi.estudio_id = pEstudio
      AND      asi.curso_id = pCurso
      AND      ',' || pGrupo || ',' like '%,' || items.grupo_id || ',%'
      AND      items.semestre_id = vSemestre
      and      asi.asignatura_id = vAsignaturaId
      AND      items.dia_semana_id IS NOT NULL
      order by dia_semana_id,
               hora_inicio,
               subgrupo;

   CURSOR c_clases_asig_agrupacion (vAsignaturaId varchar2, vSemestre varchar2) is
      SELECT   tipo_subgrupo_id || subgrupo_id subgrupo,
               decode (dia_semana_id, 1, 'L', 2, 'M', 3, 'X', 4, 'J', 5, 'V', '?') dia_sem,
               to_char (hora_fin, 'hh24:mi') hora_fin_str, to_char (hora_inicio, 'hh24:mi') hora_inicio_str
      FROM     UJI_HORARIOS.HOR_ITEMS items,
               uji_horarios.hor_items_asignaturas asi
      WHERE    items.id = asi.item_id
      and      asi.estudio_id = pEstudio
      AND      ',' || pGrupo || ',' like '%,' || items.grupo_id || ',%'
      AND      items.semestre_id = vSemestre
      and      asi.asignatura_id = vAsignaturaId
      AND      items.dia_semana_id IS NOT NULL
      and items.id in (SELECT item_id from HOR_V_AGRUPACIONES_ITEMS agr_item where agr_item.agrupacion_id = pAgrupacion)
      order by dia_semana_id,
               hora_inicio,
               subgrupo;

   vFechas              t_horario;
   vRdo                 clob;

   procedure cabecera is
   begin
      owa_util.mime_header (ccontent_type => 'text/xml', bclose_header => true);
      fop.documentOpen (page_width => 29.7, page_height => 21, margin_left => 0.5, margin_right => 0.5,
                        margin_top => 0.5, margin_bottom => 0, extent_before => 0, extent_after => 0.5,
                        extent_margin_top => 0, extent_margin_bottom => 0.5);
      fop.pageBodyOpen;
      fop.documentfooteropen;
      fop.block ('Fecha documento: ' || to_char (sysdate, 'yyyy/mm/dd HH24:MI'), font_size => 6,
                 font_style => 'italic');
      fop.documentfooterclose;
      fop.pageFlowOpen;
   end;

   procedure calcula_curso_aca is
   begin
      SELECT   curso_academico_id
      into     vCursoAcaNum
      FROM     hor_semestres_detalle d,
               hor_estudios e
      WHERE    e.id = pEstudio
      AND      e.tipo_id = d.tipo_estudio_id
      AND      ROWNUM = 1
      ORDER BY fecha_inicio;
   end;

   procedure info_calendario_gen (pSemestre in number) is
      vCursoAca           varchar2 (4000);
      vTitulacionNombre   varchar2 (4000);
      vCursoNombre        varchar2 (4000) := '';
      vTextoCabecera      varchar2 (4000);
      vTextoGrupo         varchar2 (4000);
      vNombreAgrupacion   varchar2 (4000) := '';
   begin
      select nombre
      into   vTitulacionNombre
      from   hor_estudios
      where  id = pEstudio;

      if (pAgrupacion is not null) then
         select 'Agrupació ' || nombre
         into vNombreAgrupacion
         from hor_agrupaciones
         where id = pAgrupacion;
      end if;

      vCursoAca := to_char (vCursoAcaNum) || '/' || to_char (vCursoAcaNum + 1);

      if pCurso in ('1', '3') then
         vCursoNombre := pCurso || 'er curs';
      elsif pCurso = '2' then
         vCursoNombre := '2on  curs';
      elsif pCurso = '4' then
         vCursoNombre := '4t curs';
      elsif pCurso is not null then
         vCursoNombre := pCurso || '?  curs';
      end if;

      if pGrupo like '%,%' then
         vTextoGrupo := 'Grups';
      else
         vTextoGrupo := 'Grup';
      end if;

      vTextoCabecera :=
         vTitulacionNombre || ' - ' || vCursoNombre || vNombreAgrupacion || ' - ' || vTextoGrupo || ' ' || pGrupo || ' (semestre '
         || pSemestre || ') - ' || vCursoAca;
      fop.block (text => vTextoCabecera, border_bottom => 'solid', border_width => 0.05);
   end;

   procedure muestra_leyenda_asig (item c_asigs%ROWTYPE, vSemestre varchar2) is
      vClasesText   varchar2 (4000);
   begin
      fop.block (item.asignatura_id || ', ' || item.asignatura || ':', font_size => 7, padding_top => 0.1);
      vClasesText := '<fo:inline color="white">___</fo:inline>';

      for c in c_clases_asig (item.asignatura_id, vSemestre) loop
         vClasesText :=
            vClasesText || c.dia_sem || ', ' || c.hora_inicio_str || '-' || c.hora_fin_str || ' (' || c.subgrupo
            || '); ';
      end loop;

      fop.block (vClasesText, font_size => 7, padding_top => 0.06);
   end;

   procedure leyenda_asigs (pSemestre in number) is
   begin
      fop.block (' ', padding_top => 1);

      for item in c_asigs (pSemestre) loop
         muestra_leyenda_asig (item, pSemestre);
      end loop;
   end;

   procedure muestra_leyenda_asig_agr (item c_asigs%ROWTYPE, vSemestre varchar2) is
      vClasesText   varchar2 (4000);
   begin
      fop.block (item.asignatura_id || ', ' || item.asignatura || ':', font_size => 7, padding_top => 0.1);
      vClasesText := '<fo:inline color="white">___</fo:inline>';

      for c in c_clases_asig_agrupacion (item.asignatura_id, vSemestre) loop
         vClasesText :=
           vClasesText || c.dia_sem || ', ' || c.hora_inicio_str || '-' || c.hora_fin_str || ' (' || c.subgrupo
             || '); ';
      end loop;

      fop.block (vClasesText, font_size => 7, padding_top => 0.06);
   end;

   procedure leyenda_asigs_agrupacion (pSemestre in number) is
   begin
      fop.block (' ', padding_top => 1);

      for item in c_asigs_agrupacion (pSemestre) loop
         muestra_leyenda_asig_agr (item, pSemestre);
      end loop;
   end;

   procedure pie is
   begin
      fop.pageFlowClose;
      fop.pageBodyClose;
      fop.documentClose;
   end;

   function redondea_fecha_a_cuartos (vIniDate date)
      return date is
      fecha_redondeada   date;
   begin
      select trunc(vIniDate,'HH')+(15*round(to_char( trunc(vIniDate,'MI'),'MI')/15))/1440
        into fecha_redondeada
        from dual;

      return fecha_redondeada;
   end;

   procedure calendario_gen (pSemestre in number) is
   begin
      vFechas := t_horario ();

      for item in c_items_gen (pSemestre) loop
         vFechas.extend;
         vFechas (vFechas.count) :=
            ItemHorarios (hor_contenido_item (redondea_fecha_a_cuartos(item.hora_inicio), redondea_fecha_a_cuartos(item.hora_fin), item.asignatura_id,
                                              '(' || item.tipo_subgrupo_id || item.subgrupo_id || ') '
                                              || item.codigo_aula),
                          redondea_fecha_a_cuartos(item.inicio), redondea_fecha_a_cuartos(item.fin), null);
      end loop;

      if vFechas.count > 0 then
         vRdo := xpfpkg_horarios_academicos.drawTimetable (vFechas, 5, null, 'CA', 'PDF_APP_HORARIOS');
         xup_utils.dump (vRdo);
         dbms_lob.freeTemporary (vRdo);
      end if;
   end;

   procedure calendario_gen_agrupacion (pSemestre in number) is
   begin
      vFechas := t_horario ();

      for item in c_items_gen_agrupacion (pSemestre) loop
         vFechas.extend;
         vFechas (vFechas.count) :=
            ItemHorarios (hor_contenido_item (redondea_fecha_a_cuartos(item.hora_inicio), redondea_fecha_a_cuartos(item.hora_fin), item.asignatura_id,
                                              '(' || item.tipo_subgrupo_id || item.subgrupo_id || ') '
                                              || item.codigo_aula),
                          redondea_fecha_a_cuartos(item.inicio), redondea_fecha_a_cuartos(item.fin), null);
      end loop;

      if vFechas.count > 0 then
         vRdo := xpfpkg_horarios_academicos.drawTimetable (vFechas, 5, null, 'CA', 'PDF_APP_HORARIOS');
         xup_utils.dump (vRdo);
         dbms_lob.freeTemporary (vRdo);
      end if;
   end;

   procedure calendario_det (vFechaIni date, vFechaFin date) is
   begin
      vFechas := t_horario ();

      for item in c_items_det (vFechaIni, vFechaFin) loop
         vFechas.extend;
         vFechas (vFechas.count) :=
            ItemHorarios (hor_contenido_item (redondea_fecha_a_cuartos(item.hora_inicio), redondea_fecha_a_cuartos(item.hora_fin), item.asignatura_id,
                                              '(' || item.tipo_subgrupo_id || item.subgrupo_id || ') '
                                              || item.codigo_aula),
                          redondea_fecha_a_cuartos(item.inicio), redondea_fecha_a_cuartos(item.fin), null);
      end loop;

      if vFechas.count > 0 then
         vRdo := xpfpkg_horarios_academicos.drawTimetable (vFechas, 5, null, 'CA', 'PDF_APP_HORARIOS_DET');
         xup_utils.dump (vRdo);
         dbms_lob.freeTemporary (vRdo);
      end if;
   end;

   procedure calendario_det_agrupacion (vFechaIni date, vFechaFin date) is
   begin
      vFechas := t_horario ();

      for item in c_items_det_agrupacion (vFechaIni, vFechaFin) loop
         vFechas.extend;
         vFechas (vFechas.count) :=
            ItemHorarios (hor_contenido_item (redondea_fecha_a_cuartos(item.hora_inicio), redondea_fecha_a_cuartos(item.hora_fin), item.asignatura_id,
                                              '(' || item.tipo_subgrupo_id || item.subgrupo_id || ') '
                                              || item.codigo_aula),
                          redondea_fecha_a_cuartos(item.inicio), redondea_fecha_a_cuartos(item.fin), null);
      end loop;

      if vFechas.count > 0 then
         vRdo := xpfpkg_horarios_academicos.drawTimetable (vFechas, 5, null, 'CA', 'PDF_APP_HORARIOS_DET');
         xup_utils.dump (vRdo);
         dbms_lob.freeTemporary (vRdo);
      end if;
   end;

   procedure calcula_fechas_semestre (pSemestre in number) is
   begin
      SELECT fecha_inicio, fecha_examenes_fin
      into   vFechaIniSemestre, vFechaFinSemestre
      FROM   hor_semestres_detalle,
             hor_estudios e
      WHERE  semestre_id = pSemestre
      and    e.id = pEstudio
      and    e.tipo_id = tipo_estudio_id;

      SELECT Next_Day(Trunc(vFechaIniSemestre) - 7,'LUN') into vFechaIniSemestre FROM dual;

   end;

   function semana_tiene_clase (vIniDate date, vFinDate date)
      return boolean is
      num_clases   number;
   begin
      SELECT   count (*)
      into     num_clases
      FROM     UJI_HORARIOS.HOR_ITEMS items,
               UJI_HORARIOS.HOR_ITEMS_DETALLE det,
               uji_horarios.hor_items_asignaturas asi
      WHERE    items.id = asi.item_id
      and      asi.estudio_id = pEstudio
      AND      asi.curso_id = pCurso
      AND      ',' || pGrupo || ',' like '%,' || items.grupo_id || ',%'
      AND      items.semestre_id = pSemestre
      AND      items.dia_semana_id IS NOT NULL
      and      det.item_id = items.id
      and      det.inicio > vIniDate
      and      det.fin < vFinDate
      order by det.inicio;

      return num_clases > 0;
   end;

      function semana_tiene_clase_agr (vIniDate date, vFinDate date)
      return boolean is
      num_clases   number;
   begin
      SELECT   count (*)
      into     num_clases
      FROM     UJI_HORARIOS.HOR_ITEMS items,
               UJI_HORARIOS.HOR_ITEMS_DETALLE det,
               uji_horarios.hor_items_asignaturas asi,
               HOR_V_AGRUPACIONES_ITEMS agr_items
      WHERE    items.id = asi.item_id
      and      asi.estudio_id = pEstudio
      AND      ',' || pGrupo || ',' like '%,' || items.grupo_id || ',%'
      AND      items.semestre_id = pSemestre
      AND      items.dia_semana_id IS NOT NULL
      and      det.item_id = items.id
      and      items.id = agr_items.item_id
      and      agr_items.agrupacion_id = pAgrupacion
      and      det.inicio > vIniDate
      and      det.fin < vFinDate
      order by det.inicio;

      return num_clases > 0;
   end;

   procedure info_calendario_det (vFechaIni date, vFechaFin date) is
      vCursoAca           varchar2 (4000);
      vTitulacionNombre   varchar2 (4000);
      vCursoNombre        varchar2 (4000) := '';
      vTextoCabecera      varchar2 (4000);
      vFechaIniStr        varchar2 (4000) := to_char (vFechaIni, 'dd/mm/yyyy');
      vFechaFinStr        varchar2 (4000) := to_char (vFechaFin - 1, 'dd/mm/yyyy');
      vNombreAgrupacion   varchar2 (4000) := '';
   begin
      select nombre
      into   vTitulacionNombre
      from   hor_estudios
      where  id = pEstudio;

      if (pAgrupacion is not null) then
         select 'Agrupació ' || nombre
         into vNombreAgrupacion
         from hor_agrupaciones
         where id = pAgrupacion;
      end if;

      vCursoAca := to_char (vCursoAcaNum) || '/' || to_char (vCursoAcaNum + 1);

      if pCurso in ('1', '3') then
         vCursoNombre := pCurso || 'er curs';
      elsif pCurso = '2' then
         vCursoNombre := '2on  curs';
      elsif pCurso = '4' then
         vCursoNombre := '4t curs';
      elsif pCurso is not null then
         vCursoNombre := pCurso || '?  curs';
      end if;

      vTextoCabecera :=
         vTitulacionNombre || ' - ' || vCursoNombre || vNombreAgrupacion || ' - Grup ' || pGrupo || ' (semestre ' || pSemestre
         || ') - Del ' || vFechaIniStr || ' al ' || vFechaFinStr;
      fop.block (text => vTextoCabecera, border_bottom => 'solid', border_width => 0.05);
   end;

   procedure paginas_calendario_det (pSemestre in number) is
      vFechaIniSemana   date;
      vFechaFinSemana   date;
   begin
      calcula_fechas_semestre (pSemestre);
      vFechaIniSemana := vFechaIniSemestre;
      vFechaFinSemana := vFechaIniSemana + 7;

      while vFechaFinSemana <= vFechaFinSemestre loop
         if semana_tiene_clase (vFechaIniSemana, vFechaFinSemana) then
            info_calendario_det (vFechaIniSemana, vFechaFinSemana);
            calendario_det (vFechaIniSemana, vFechaFinSemana);
            htp.p ('<fo:block break-before="page" color="white">_</fo:block>');
         end if;

         vFechaIniSemana := vFechaFinSemana;
         vFechaFinSemana := vFechaFinSemana + 7;
      end loop;
   end;

   procedure paginas_calendario_det_agr (pSemestre in number) is
      vFechaIniSemana   date;
      vFechaFinSemana   date;
   begin
      calcula_fechas_semestre (pSemestre);
      vFechaIniSemana := vFechaIniSemestre;
      vFechaFinSemana := vFechaIniSemana + 7;

      while vFechaFinSemana <= vFechaFinSemestre loop
         if semana_tiene_clase_agr (vFechaIniSemana, vFechaFinSemana) then
            info_calendario_det (vFechaIniSemana, vFechaFinSemana);
            calendario_det_agrupacion (vFechaIniSemana, vFechaFinSemana);
            htp.p ('<fo:block break-before="page" color="white">_</fo:block>');
         end if;
         vFechaIniSemana := vFechaFinSemana;
         vFechaFinSemana := vFechaFinSemana + 7;
      end loop;
   end;


BEGIN
   --euji_control_acceso (vItem);
   select count (*)
   into   vSesion
   from   apa_sesiones
   where  sesion_id = pSesion
   and    fecha < sysdate () - 0.33;

   if vSesion > 0 then
      calcula_curso_aca;
      cabecera;

      if vEsGenerica then
         info_calendario_gen (1);

         if (pCurso is not null) then
            calendario_gen (1);
            leyenda_asigs (1);
         else
            calendario_gen_agrupacion(1);
            leyenda_asigs_agrupacion (1);
         end if;


         htp.p ('<fo:block break-before="page" color="white">_</fo:block>');
         info_calendario_gen (2);

         if (pCurso is not null) then
            calendario_gen (2);
            leyenda_asigs (2);
         else
            calendario_gen_agrupacion(2);
            leyenda_asigs_agrupacion (2);
         end if;
      else
         if (pCurso is not null) then
            paginas_calendario_det (pSemestre);
         else
            paginas_calendario_det_agr(pSemestre);
         end if;

         leyenda_asigs (pSemestre);
      end if;

      pie;
   else
      cabecera;
      fop.block ('No tens perm?s per accedir a aquest document.', font_weight => 'bold', text_align => 'center',
                 font_size => 14, space_after => 0.5);
      pie;
   end if;
EXCEPTION
   WHEN OTHERS THEN
      htp.p (sqlerrm);
END;

CREATE OR REPLACE PROCEDURE UJI_HORARIOS.HORARIOS_CIRCUITO_PDF (
   name_array    IN   euji_util.ident_arr2,
   value_array   IN   euji_util.ident_arr2
) IS
   -- Parametros
   pEstudio    CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pEstudio');
   pCircuito   CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pCircuito');
   pSesion     CONSTANT VARCHAR2 (4000) := euji_util.euji_getparam (name_array, value_array, 'pSesion');
   pSemestre   CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pSemestre');
   pGrupo      CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pGrupo');
   pSemana     CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pSemana');
   pCurso               number          := 1;
   vCursoAcaNum         number;
   vSesion              number;
   vFechaIniSemestre    date;
   vFechaFinSemestre    date;
   vEsGenerica          boolean         := pSemana = 'G';

     -- Variables
   --  vPerId              per_personas.id%TYPE := euji_get_perid ();
   CURSOR c_items_gen IS
      SELECT asignatura_id, asignatura, estudio, asi.caracter, semestre_id, comun, grupo_id, tipo_subgrupo,
             tipo_subgrupo_id, subgrupo_id, dia_semana_id, hora_inicio, hora_fin, tipo_asignatura_id, tipo_asignatura,
             to_date (dia_semana_id + 1 || '/01/2006 ' || to_char (hora_fin, 'hh24:mi'), 'dd/mm/yyyy hh24:mi') fin,
             to_date (dia_semana_id + 1 || '/01/2006 ' || to_char (hora_inicio, 'hh24:mi'),
                      'dd/mm/yyyy hh24:mi') inicio
      FROM   UJI_HORARIOS.HOR_ITEMS items,
             uji_horarios.hor_items_asignaturas asi,
             uji_horarios.hor_items_circuitos cir
      WHERE  items.id = asi.item_id
      and    cir.item_id = items.id
      and    cir.circuito_id = pCircuito
      and    asi.estudio_id = pEstudio
      AND    asi.curso_id = pCurso
      AND    items.grupo_id = pGrupo
      AND    items.semestre_id = pSemestre
      AND    items.dia_semana_id IS NOT NULL
      GROUP BY   asignatura_id,
           asignatura,
           estudio,
           asi.caracter,
           semestre_id,
           comun,
           grupo_id,
           tipo_subgrupo,
           tipo_subgrupo_id,
           subgrupo_id,
           dia_semana_id,
           hora_inicio,
           hora_fin,
           tipo_asignatura_id,
           tipo_asignatura;

   CURSOR c_items_det (vIniDate date, vFinDate date) IS
      SELECT   asignatura_id, asignatura, estudio, grupo_id, tipo_subgrupo, tipo_subgrupo_id, subgrupo_id,
               dia_semana_id, hora_inicio, hora_fin, inicio, fin
      FROM     UJI_HORARIOS.HOR_ITEMS items,
               UJI_HORARIOS.HOR_ITEMS_DETALLE det,
               uji_horarios.hor_items_asignaturas asi,
               uji_horarios.hor_items_circuitos cir
      WHERE    items.id = asi.item_id
      and      cir.item_id = items.id
      and      cir.circuito_id = pCircuito
      and      asi.estudio_id = pEstudio
      AND      asi.curso_id = pCurso
      AND      items.grupo_id = pGrupo
      AND      items.semestre_id = pSemestre
      AND      items.dia_semana_id IS NOT NULL
      and      det.item_id = items.id
      and      det.inicio > vIniDate
      and      det.fin < vFinDate
      GROUP BY asignatura_id,
               asignatura,
               estudio,
               grupo_id,
               tipo_subgrupo,
               tipo_subgrupo_id,
               subgrupo_id,
               dia_semana_id,
               hora_inicio,
               hora_fin,
               inicio,
               fin
      order by det.inicio;

   CURSOR c_asigs is
      SELECT distinct (asignatura_id), asignatura
      FROM            UJI_HORARIOS.HOR_ITEMS items,
                      uji_horarios.hor_items_asignaturas asi,
                      uji_horarios.hor_items_circuitos itemcir
      WHERE           items.id = asi.item_id
      and             items.id = itemcir.item_id
      and             itemcir.circuito_id = pCircuito
      and             asi.estudio_id = pEstudio
      AND             asi.curso_id = pCurso
      AND             items.grupo_id = pGrupo
      AND             items.semestre_id = pSemestre
      AND             items.dia_semana_id IS NOT NULL
      order by        asignatura_id;

   CURSOR c_clases_asig (vAsignaturaId varchar2) is
      SELECT   tipo_subgrupo_id || subgrupo_id subgrupo,
               decode (dia_semana_id, 1, 'L', 2, 'M', 3, 'X', 4, 'J', 5, 'V', '?') dia_sem,
               to_char (hora_fin, 'hh24:mi') hora_fin_str, to_char (hora_inicio, 'hh24:mi') hora_inicio_str
      FROM     UJI_HORARIOS.HOR_ITEMS items,
               uji_horarios.hor_items_asignaturas asi,
               uji_horarios.hor_items_circuitos itemcir
      WHERE    items.id = asi.item_id
      and      items.id = itemcir.item_id
      and      itemcir.circuito_id = pCircuito
      and      asi.estudio_id = pEstudio
      AND      asi.curso_id = pCurso
      AND      items.grupo_id = pGrupo
      AND      items.semestre_id = pSemestre
      and      asi.asignatura_id = vAsignaturaId
      AND      items.dia_semana_id IS NOT NULL
      order by dia_semana_id,
               hora_inicio,
               subgrupo;

   vFechas              t_horario;
   vRdo                 clob;

   procedure cabecera is
   begin
      owa_util.mime_header (ccontent_type => 'text/xml', bclose_header => true);
      fop.documentOpen (page_width => 29.7, page_height => 21, margin_left => 0.5, margin_right => 0.5,
                        margin_top => 0.5, margin_bottom => 0, extent_before => 0, extent_after => 0.5,
                        extent_margin_top => 0, extent_margin_bottom => 0.5);
      fop.pageBodyOpen;
      fop.documentfooteropen;
      fop.block ('Fecha documento: ' || to_char (sysdate, 'yyyy/mm/dd HH24:MI'), font_size => 6,
                 font_style => 'italic');
      fop.documentfooterclose;
      fop.pageFlowOpen;
   end;

   procedure calcula_curso_aca is
   begin
      SELECT   curso_academico_id
      into     vCursoAcaNum
      FROM     hor_semestres_detalle d,
               hor_estudios e
      WHERE    e.id = pEstudio
      AND      e.tipo_id = d.tipo_estudio_id
      AND      ROWNUM = 1
      ORDER BY fecha_inicio;
   end;

   procedure info_calendario_gen is
      vCursoAca           varchar2 (4000);
      vTitulacionNombre   varchar2 (4000);
      vCircuitoNombre     varchar2 (4000);
      vCursoNombre        varchar2 (4000) := pCurso || '?';
      vTextoCabecera      varchar2 (4000);
   begin
      select nombre
      into   vTitulacionNombre
      from   hor_estudios
      where  id = pEstudio;

      select nombre
      into   vCircuitoNombre
      from   hor_circuitos
      where  id = pCircuito;

      vCursoAca := to_char (vCursoAcaNum) || '/' || to_char (vCursoAcaNum + 1);

      if pCurso in ('1', '3') then
         vCursoNombre := pCurso || 'er';
      elsif pCurso = '2' then
         vCursoNombre := '2on';
      elsif pCurso = '4' then
         vCursoNombre := '4t';
      else
         vCursoNombre := pCurso || '?';
      end if;

      vTextoCabecera :=
         vTitulacionNombre || ' - Circuit ' || vCircuitoNombre || ' - ' || vCursoNombre || ' curs - Grup ' || pGrupo
         || ' (semestre ' || pSemestre || ') - ' || vCursoAca;
      fop.block (text => vTextoCabecera, border_bottom => 'solid', border_width => 0.05);
   end;

   procedure muestra_leyenda_asig (item c_asigs%ROWTYPE) is
      vClasesText   varchar2 (4000);
   begin
      fop.block (item.asignatura_id || ', ' || item.asignatura || ':', font_size => 7, padding_top => 0.1);
      vClasesText := '<fo:inline color="white">___</fo:inline>';

      for c in c_clases_asig (item.asignatura_id) loop
         vClasesText :=
            vClasesText || c.dia_sem || ', ' || c.hora_inicio_str || '-' || c.hora_fin_str || ' (' || c.subgrupo
            || '); ';
      end loop;

      --vClasesText := substr (vClasesText, 1, length (vClasesText) - 2);
      fop.block (vClasesText, font_size => 7, padding_top => 0.06);
   end;

   procedure leyenda_asigs is
   begin
      fop.block (' ', padding_top => 1);

      for item in c_asigs loop
         muestra_leyenda_asig (item);
      end loop;
   end;

   procedure pie is
   begin
      fop.pageFlowClose;
      fop.pageBodyClose;
      fop.documentClose;
   end;

   procedure calendario_gen is
   begin
      vFechas := t_horario ();

      for item in c_items_gen loop
         vFechas.extend;
         vFechas (vFechas.count) :=
            ItemHorarios (hor_contenido_item (item.hora_inicio, item.hora_fin, item.asignatura_id,
                                              '(' || item.tipo_subgrupo_id || item.subgrupo_id || ')'),
                          redondea_fecha_a_cuartos(item.inicio), redondea_fecha_a_cuartos(item.fin), null);
      end loop;

      if vFechas.count > 0 then
         vRdo := xpfpkg_horarios_academicos.drawTimetable (vFechas, 5, null, 'CA', 'PDF_APP_HORARIOS');
         xup_utils.dump (vRdo);
         dbms_lob.freeTemporary (vRdo);
      end if;
   end;

   procedure calendario_det (vFechaIni date, vFechaFin date) is
   begin
      vFechas := t_horario ();

      for item in c_items_det (vFechaIni, vFechaFin) loop
         vFechas.extend;
         vFechas (vFechas.count) :=
            ItemHorarios (hor_contenido_item (item.hora_inicio, item.hora_fin, item.asignatura_id,
                                              '(' || item.tipo_subgrupo_id || item.subgrupo_id || ')'),
                          redondea_fecha_a_cuartos(item.inicio), redondea_fecha_a_cuartos(item.fin), null);
      end loop;

      if vFechas.count > 0 then
         vRdo := xpfpkg_horarios_academicos.drawTimetable (vFechas, 5, null, 'CA', 'PDF_APP_HORARIOS_DET');
         xup_utils.dump (vRdo);
         dbms_lob.freeTemporary (vRdo);
      end if;
   end;

   procedure calcula_fechas_semestre is
   begin
      SELECT fecha_inicio, fecha_examenes_fin
      into   vFechaIniSemestre, vFechaFinSemestre
      FROM   hor_semestres_detalle,
             hor_estudios e
      WHERE  semestre_id = pSemestre
      and    e.id = pEstudio
      and    e.tipo_id = tipo_estudio_id;
   end;

   function semana_tiene_clase (vIniDate date, vFinDate date)
      return boolean is
      num_clases   number;
   begin
      SELECT   count (*)
      into     num_clases
      FROM     UJI_HORARIOS.HOR_ITEMS items,
               UJI_HORARIOS.HOR_ITEMS_DETALLE det,
               uji_horarios.hor_items_asignaturas asi
      WHERE    items.id = asi.item_id
      and      asi.estudio_id = pEstudio
      AND      asi.curso_id = pCurso
      AND      items.grupo_id = pGrupo
      AND      items.semestre_id = pSemestre
      AND      items.dia_semana_id IS NOT NULL
      and      det.item_id = items.id
      and      det.inicio > vIniDate
      and      det.fin < vFinDate
      order by det.inicio;

      return num_clases > 0;
   end;

   procedure info_calendario_det (vFechaIni date, vFechaFin date) is
      vCursoAca           varchar2 (4000);
      vTitulacionNombre   varchar2 (4000);
      vCursoNombre        varchar2 (4000) := pCurso || '?';
      vCircuitoNombre     varchar2 (4000);
      vTextoCabecera      varchar2 (4000);
      vFechaIniStr        varchar2 (4000) := to_char (vFechaIni, 'dd/mm/yyyy');
      vFechaFinStr        varchar2 (4000) := to_char (vFechaFin - 1, 'dd/mm/yyyy');
   begin
      select nombre
      into   vTitulacionNombre
      from   hor_estudios
      where  id = pEstudio;

      select nombre
      into   vCircuitoNombre
      from   hor_circuitos
      where  id = pCircuito;

      vCursoAca := to_char (vCursoAcaNum) || '/' || to_char (vCursoAcaNum + 1);

      if pCurso in ('1', '3') then
         vCursoNombre := pCurso || 'er';
      elsif pCurso = '2' then
         vCursoNombre := '2on';
      elsif pCurso = '4' then
         vCursoNombre := '4t';
      else
         vCursoNombre := pCurso || '?';
      end if;

      vTextoCabecera :=
         vTitulacionNombre || ' - Circuit ' || vCircuitoNombre || ' - ' || vCursoNombre || ' curs - Grup ' || pGrupo
         || ' (semestre ' || pSemestre || ') - Del ' || vFechaIniStr || ' al ' || vFechaFinStr;
      fop.block (text => vTextoCabecera, border_bottom => 'solid', border_width => 0.05);
   end;

   procedure paginas_calendario_det is
      vFechaIniSemana   date;
      vFechaFinSemana   date;
   begin
      calcula_fechas_semestre;
      vFechaIniSemana := vFechaIniSemestre;
      vFechaFinSemana := vFechaIniSemana + 7;

      while vFechaFinSemana <= vFechaFinSemestre loop
         if semana_tiene_clase (vFechaIniSemana, vFechaFinSemana) then
            info_calendario_det (vFechaIniSemana, vFechaFinSemana);
            calendario_det (vFechaIniSemana, vFechaFinSemana);
            htp.p ('<fo:block break-before="page" color="white">_</fo:block>');
         end if;

         vFechaIniSemana := vFechaFinSemana;
         vFechaFinSemana := vFechaFinSemana + 7;
      end loop;
   end;
BEGIN
   select count (*)
   into   vSesion
   from   apa_sesiones
   where  sesion_id = pSesion
   and    fecha < sysdate () - 0.33;

   if vSesion > 0 then
      calcula_curso_aca;
      cabecera;

      if vEsGenerica then
         info_calendario_gen;
         calendario_gen;
         leyenda_asigs;
      else
         paginas_calendario_det;
         leyenda_asigs;
      end if;

      pie;
   else
      cabecera;
      fop.block ('No tens perm?s per accedir a aquest document.', font_weight => 'bold', text_align => 'center',
                 font_size => 14, space_after => 0.5);
      pie;
   end if;
EXCEPTION
   WHEN OTHERS THEN
      htp.p (sqlerrm);
END;

CREATE OR REPLACE PROCEDURE UJI_HORARIOS.HORARIOS_MENSUAL_PDF (
   name_array    IN   euji_util.ident_arr2,
   value_array   IN   euji_util.ident_arr2
) IS
   -- Parametros
   pEstudio    CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pEstudio');
   pMes        CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pMes');
   pSemestre   CONSTANT VARCHAR2 (40)   := '1';
   pSesion     CONSTANT VARCHAR2 (4000) := euji_util.euji_getparam (name_array, value_array, 'pSesion');
   pGrupo      CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pGrupo');
   pCurso      CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pCurso');
   pSemana     CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pSemana');
   vCursoAcaNum         number;
   vSesion              number;
   vFechaIniSemestre    date;
   vFechaFinSemestre    date;
   vEsGenerica          boolean         := pSemana = 'G';

     -- Variables
   --  vPerId              per_personas.id%TYPE := euji_get_perid ();
   CURSOR c_items_det (vIniDate date, vFinDate date) IS
      SELECT   aulas.codigo as codigo_aula, asignatura_id, asignatura, estudio, grupo_id, tipo_subgrupo,
               tipo_subgrupo_id, subgrupo_id, dia_semana_id, hora_inicio, hora_fin, inicio, fin
      FROM     UJI_HORARIOS.HOR_ITEMS items LEFT OUTER JOIN hor_aulas aulas ON (aulas.id = items.aula_planificacion_id)
               INNER JOIN uji_horarios.hor_items_asignaturas asi ON (items.id = asi.item_id)
               INNER JOIN UJI_HORARIOS.HOR_ITEMS_DETALLE det ON (det.item_id = items.id)
      WHERE    asi.estudio_id = pEstudio
      AND      asi.curso_id = pCurso
      AND      items.grupo_id = pGrupo
      AND      items.semestre_id = pSemestre
      AND      items.dia_semana_id IS NOT NULL
      and      det.inicio > vIniDate
      and      det.fin < vFinDate
      order by det.inicio;

   CURSOR c_asigs is
      SELECT distinct (asignatura_id), asignatura
      FROM            UJI_HORARIOS.HOR_ITEMS items,
                      uji_horarios.hor_items_asignaturas asi
      WHERE           items.id = asi.item_id
      and             asi.estudio_id = pEstudio
      AND             asi.curso_id = pCurso
      AND             items.grupo_id = pGrupo
      AND             items.semestre_id = pSemestre
      AND             items.dia_semana_id IS NOT NULL
      order by        asignatura_id;

   CURSOR c_clases_asig (vAsignaturaId varchar2) is
      SELECT   tipo_subgrupo_id || subgrupo_id subgrupo,
               decode (dia_semana_id, 1, 'L', 2, 'M', 3, 'X', 4, 'J', 5, 'V', '?') dia_sem,
               to_char (hora_fin, 'hh24:mi') hora_fin_str, to_char (hora_inicio, 'hh24:mi') hora_inicio_str
      FROM     UJI_HORARIOS.HOR_ITEMS items,
               uji_horarios.hor_items_asignaturas asi
      WHERE    items.id = asi.item_id
      and      asi.estudio_id = pEstudio
      AND      asi.curso_id = pCurso
      AND      items.grupo_id = pGrupo
      AND      items.semestre_id = pSemestre
      and      asi.asignatura_id = vAsignaturaId
      AND      items.dia_semana_id IS NOT NULL
      order by dia_semana_id,
               hora_inicio,
               subgrupo;

   vFechas              t_horario;
   vRdo                 clob;

   procedure cabecera is
   begin
      owa_util.mime_header (ccontent_type => 'text/xml', bclose_header => true);
      fop.documentOpen (page_width => 28.5*5, page_height => 18, margin_left => 0.5, margin_right => 0.5,
                        margin_top => 0.5, margin_bottom => 0, extent_before => 0, extent_after => 0.5,
                        extent_margin_top => 0, extent_margin_bottom => 0.5);
      fop.pageBodyOpen;
      fop.documentfooteropen;
      fop.block ('Fecha documento: ' || to_char (sysdate, 'yyyy/mm/dd HH24:MI'), font_size => 6,
                 font_style => 'italic');
      fop.documentfooterclose;
      fop.pageFlowOpen;
   end;

   procedure calcula_curso_aca is
   begin
      SELECT   curso_academico_id
      into     vCursoAcaNum
      FROM     hor_semestres_detalle d,
               hor_estudios e
      WHERE    e.id = pEstudio
      AND      e.tipo_id = d.tipo_estudio_id
      AND      ROWNUM = 1
      ORDER BY fecha_inicio;
   end;

   procedure info_calendario_gen is
      vCursoAca           varchar2 (4000);
      vTitulacionNombre   varchar2 (4000);
      vCursoNombre        varchar2 (4000) := pCurso || 'º';
      vTextoCabecera      varchar2 (4000);
      vTextoGrupo         varchar2 (4000);
   begin
      select nombre
      into   vTitulacionNombre
      from   hor_estudios
      where  id = pEstudio;

      vCursoAca := to_char (vCursoAcaNum) || '/' || to_char (vCursoAcaNum + 1);

      if pCurso in ('1', '3') then
         vCursoNombre := pCurso || 'er';
      elsif pCurso = '2' then
         vCursoNombre := '2on';
      elsif pCurso = '4' then
         vCursoNombre := '4t';
      else
         vCursoNombre := pCurso || 'è';
      end if;

      if pGrupo like '%,%' then
         vTextoGrupo := 'Grups';
      else
         vTextoGrupo := 'Grup';
      end if;

      vTextoCabecera :=
         vTitulacionNombre || ' - ' || vCursoNombre || ' curs - ' || vTextoGrupo || ' ' || pGrupo || ' (semestre '
         || pSemestre || ') - ' || vCursoAca;
      fop.block (text => vTextoCabecera, border_bottom => 'solid', border_width => 0.05);
   end;

   procedure muestra_leyenda_asig (item c_asigs%ROWTYPE) is
      vClasesText   varchar2 (4000);
   begin
      fop.block (item.asignatura_id || ', ' || item.asignatura || ':', font_size => 7, padding_top => 0.1);
      vClasesText := '<fo:inline color="white">___</fo:inline>';

      for c in c_clases_asig (item.asignatura_id) loop
         vClasesText :=
            vClasesText || c.dia_sem || ', ' || c.hora_inicio_str || '-' || c.hora_fin_str || ' (' || c.subgrupo
            || '); ';
      end loop;

      --vClasesText := substr (vClasesText, 1, length (vClasesText) - 2);
      fop.block (vClasesText, font_size => 7, padding_top => 0.06);
   end;

   procedure leyenda_asigs is
   begin
      fop.block (' ', padding_top => 1);

      for item in c_asigs loop
         muestra_leyenda_asig (item);
      end loop;
   end;

   procedure pie is
   begin
      fop.pageFlowClose;
      fop.pageBodyClose;
      fop.documentClose;
   end;

   procedure calendario_det (vFechaIni date, vFechaFin date) is
   begin
      vFechas := t_horario ();

      for item in c_items_det (vFechaIni, vFechaFin) loop
         vFechas.extend;
         vFechas (vFechas.count) :=
            ItemHorarios (hor_contenido_item (item.hora_inicio, item.hora_fin, item.asignatura_id,
                                              '(' || item.tipo_subgrupo_id || item.subgrupo_id || ') '
                                              || item.codigo_aula),
                          redondea_fecha_a_cuartos(item.inicio), redondea_fecha_a_cuartos(item.fin), null);
      end loop;

      if vFechas.count > 0 then
         vRdo := XPFDM.xpfpkg_horarios_academicos.drawTimetable (vFechas, 7, null, 'CA', 'PDF_APP_HORARIOS_DET');
         xup_utils.dump (vRdo);
         dbms_lob.freeTemporary (vRdo);
      end if;
   end;

   function semana_tiene_clase (vIniDate date, vFinDate date)
      return boolean is
      num_clases   number;
   begin
      SELECT   count (*)
      into     num_clases
      FROM     UJI_HORARIOS.HOR_ITEMS items,
               UJI_HORARIOS.HOR_ITEMS_DETALLE det,
               uji_horarios.hor_items_asignaturas asi
      WHERE    items.id = asi.item_id
      and      asi.estudio_id = pEstudio
      AND      asi.curso_id = pCurso
      AND      items.grupo_id = pGrupo
      AND      items.semestre_id = pSemestre
      AND      items.dia_semana_id IS NOT NULL
      and      det.item_id = items.id
      and      det.inicio > vIniDate
      and      det.fin < vFinDate
      order by det.inicio;

      return num_clases > 0;
   end;

   procedure info_calendario_det (vFechaIni date, vFechaFin date) is
      vCursoAca           varchar2 (4000);
      vTitulacionNombre   varchar2 (4000);
      vCursoNombre        varchar2 (4000) := pCurso || 'º';
      vTextoCabecera      varchar2 (4000);
      vFechaIniStr        varchar2 (4000) := to_char (vFechaIni, 'dd/mm/yyyy');
      vFechaFinStr        varchar2 (4000) := to_char (vFechaFin - 1, 'dd/mm/yyyy');
   begin
      select nombre
      into   vTitulacionNombre
      from   hor_estudios
      where  id = pEstudio;

      vCursoAca := to_char (vCursoAcaNum) || '/' || to_char (vCursoAcaNum + 1);

      if pCurso in ('1', '3') then
         vCursoNombre := pCurso || 'er';
      elsif pCurso = '2' then
         vCursoNombre := '2on';
      elsif pCurso = '4' then
         vCursoNombre := '4t';
      else
         vCursoNombre := pCurso || 'è';
      end if;

      vTextoCabecera :=
         vTitulacionNombre || ' - ' || vCursoNombre || ' curs - Grup ' || pGrupo || ' (semestre ' || pSemestre
         || ') - Del ' || vFechaIniStr || ' al ' || vFechaFinStr;
      fop.block (text => vTextoCabecera, border_bottom => 'solid', border_width => 0.05);
   end;

   procedure paginas_calendario_det(mes number) is
      vFechaIni  date;
      vFechaFin  date;
   begin
      vFechaIni := to_Date('01/' || mes || '/2014');
      vFechaFin := vFechaIni + 7;

      -- Abrir tabla
      fop.tableOpen(border_style=> 'none', padding=> 0);

      for i in 1..5 loop
         fop.tableColumnDefinition(column_width=> 28);
      end loop;

      fop.tableBodyOpen;
      fop.tableRowOpen;

      for i in 1..5 loop
          fop.tableCellOpen(border=> 'none', display_align => 'top');

          info_calendario_det (vFechaIni, vFechaFin);
          calendario_det (vFechaIni, vFechaFin);

          vFechaIni := vFechaIni + 7;

          vFechaFin := vFechaFin + 7;

          if (vFechaFin > LAST_DAY( vFechaIni )) then
            vFechaFin := LAST_DAY( vFechaIni ) + 1;
          end if;

          fop.tableCellClose;
      end loop;

      fop.tableRowClose;

      fop.tableBodyClose;
      fop.tableClose;


   end;
BEGIN
   --euji_control_acceso (vItem);
   select count (*)
   into   vSesion
   from   apa_sesiones
   where  sesion_id = pSesion
   and    fecha < sysdate () - 0.33;

   if vSesion > 0 then
      calcula_curso_aca;
      cabecera;
      --fop.block ('HOLA');

      paginas_calendario_det(pMes);

      pie;
   else
      cabecera;
      fop.block ('No tens permís per accedir a aquest document.', font_weight => 'bold', text_align => 'center',
                 font_size => 14, space_after => 0.5);
      pie;
   end if;
EXCEPTION
   WHEN OTHERS THEN
      htp.p (sqlerrm);
END;



CREATE OR REPLACE PROCEDURE UJI_HORARIOS.HORARIOS_VALIDACION_PDF (
   name_array    IN   euji_util.ident_arr2,
   value_array   IN   euji_util.ident_arr2
) IS
   -- Parametros
   pEstudio    CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pEstudio');
   pSemestre   CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pSemestre');
   pGrupo      CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pGrupo');
   pCurso      CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pCurso');
   pSesion     CONSTANT VARCHAR2 (400)  := euji_util.euji_getparam (name_array, value_array, 'pSesion');
   v_nombre             varchar2 (2000);
   vSesion              number;

   procedure cabecera is
   begin
      owa_util.mime_header (ccontent_type => 'text/xml', bclose_header => true);
      fop.documentOpen (margin_left => 1.5, margin_right => 1.5, margin_top => 0.5, margin_bottom => 0.5,
                        extent_before => 3, extent_after => 1.5, extent_margin_top => 3, extent_margin_bottom => 1.5);
      fop.pageBodyOpen;
      htp.p (fof.documentheaderopen || fof.blockopen (space_after => 0.1) || fof.tableOpen
             || fof.tablecolumndefinition (column_width => 3) || fof.tablecolumndefinition (column_width => 15)
             || fof.tablebodyopen || fof.tableRowOpen || fof.tablecellopen
             || fof.block (fof.logo_uji, text_align => 'center') || fof.tablecellclose
             || fof.tablecellopen (display_align => 'center', padding => 0.4)
             || fof.block (text => 'Controls horaris', font_size => 12, font_weight => 'bold', font_family => 'Arial',
                           text_align => 'center')
             || fof.block (text => v_nombre, font_size => 12, font_weight => 'bold', font_family => 'Arial',
                           text_align => 'center')
             || fof.block (text => 'Curs: ' || pCurso || ' - Semestre: ' || pSemestre || ' - Grup: ' || pGrupo,
                           font_size => 12, font_weight => 'bold', font_family => 'Arial', text_align => 'center')
             || fof.tablecellclose || fof.tableRowClose || fof.tablebodyclose || fof.tableClose || fof.blockclose
             || fof.documentheaderclose);
      -- El numero paginas se escribe a mano para poner el texto m?s peque?o
      htp.p (fof.documentfooteropen || fof.tableOpen || fof.tablecolumndefinition (column_width => 18)
             || fof.tablebodyopen || fof.tableRowOpen || fof.tablecellopen
             || fof.block (text => 'Pag. <fo:page-number /> de <fo:page-number-citation ref-id="lastBlock" />',
                           text_align => 'right', font_weight => 'bold', font_size => 8)
             || fof.tablecellclose || fof.tableRowClose || fof.tablebodyclose || fof.tableClose
             || fof.documentfooterclose);
      fop.pageFlowOpen;
      fop.block (fof.white_space);
   end;

   procedure pie is
   begin
      htp.p ('<fo:block  id="lastBlock" space-before="1cm" font-size="8pt">');
      htp.p ('Data de generaci?: ' || to_char (sysdate, 'dd/mm/yyy hh24:mi'));
      fop.blockclose ();
      fop.pageFlowClose;
      fop.pageBodyClose;
      fop.documentClose;
   end;

   function espacios (p_espacios in number)
      return varchar2 is
      v_rdo   varchar2 (2000);
   begin
      v_rdo := null;

      for x in 1 .. p_espacios loop
         v_rdo := v_rdo || fof.white_space;
      end loop;

      return v_rdo;
   end;

   procedure p (p_texto in varchar2) is
   begin
      fop.block (espacios (3) || p_texto);
   end;

   procedure control_1 is
      v_aux   number := 0;

      cursor lista_errores_1 is
         select distinct asignatura_id, grupo_id, tipo_subgrupo_id || subgrupo_id subgrupo,
                         decode (tipo_subgrupo_id, 'TE', 1, 'PR', 2, 'LA', 3, 'SE', 4, 0) tipo
         from            uji_horarios.hor_items i,
                         hor_items_asignaturas asi
         where           i.id = asi.item_id
         and             asi.curso_id = pCurso
         and             semestre_id = pSemestre
         --and             grupo_id = pGrupo
         and             pGrupo like '%' || grupo_id || '%'
         and             tipo_subgrupo_id not in ('AV', 'TU')
         and             dia_semana_id is null
         and             estudio_id = pEstudio
         order by        1,
                         2,
                         decode (tipo_subgrupo_id, 'TE', 1, 'PR', 2, 'LA', 3, 'SE', 4, 0);
   begin
      fop.block ('Control 1 - Subgrups sense planificar', font_size => 12, font_weight => 'bold',
                 border_bottom => 'solid');

      for x in lista_errores_1 loop
         v_aux := v_aux + 1;
         p (x.asignatura_id || ' ' || x.grupo_id || ' ' || x.subgrupo);
      end loop;

      if v_aux = 0 then
         fop.block ('No hi ha cap error.', space_before => 0.5);
      elsif v_aux = 1 then
         fop.block ('Trovat ' || v_aux || ' error.', space_before => 0.5);
      else
         fop.block ('Trovats ' || v_aux || ' errors.', space_before => 0.5);
      end if;
   exception
      when others then
         p (sqlerrm);
   end;

   procedure control_2 is
      v_aux          number        := 0;
      v_asignatura   varchar2 (20);

      cursor lista_errores_2_v1 is
         select   asignatura_id, grupo_id, tipo_subgrupo_id || subgrupo_id subgrupo,
                  decode (dia_semana_id,
                          1, 'Dilluns',
                          2, 'Dimarts',
                          3, 'Dimecres',
                          4, 'Dijous',
                          5, 'Divendres',
                          6, 'Dissabte',
                          7, 'Diumenge',
                          'Error'
                         ) dia_semana,
                  hora_inicio, hora_fin,
                  (select asi2.asignatura_id || ' ' || i2.tipo_subgrupo_id || i2.subgrupo_id grupo
                   from   uji_horarios.hor_items i2,
                          uji_horarios.hor_items_asignaturas asi2
                   where  i.id <> i2.id
                   and    i2.id = asi2.item_id
                   --and    i.curso_id = i2.curso_id
                   and    i.semestre_id = i2.semestre_id
                   and    i.grupo_id = i2.grupo_id
                   and    i.tipo_subgrupo_id <> i2.tipo_subgrupo_id
                   and    i.dia_semana_id = i2.dia_semana_id
                   and    asi.estudio_id = asi2.estudio_id
                   and    asi.asignatura_id = asi2.asignatura_id
                   and    (   to_number (to_char (i.hora_inicio, 'hh24mi')) between to_number (to_char (i2.hora_inicio,
                                                                                                        'hh24mi'))
                                                                                and to_number (to_char (i2.hora_fin,
                                                                                                        'hh24mi'))
                           or to_number (to_char (i.hora_fin, 'hh24mi')) between to_number (to_char (i2.hora_inicio,
                                                                                                     'hh24mi'))
                                                                             and to_number (to_char (i2.hora_fin,
                                                                                                     'hh24mi'))
                          )
                   and    rownum <= 1) solape
         from     uji_horarios.hor_items i,
                  uji_horarios.hor_items_asignaturas asi
         where    i.id = asi.item_id
         and      asi.curso_id = pCurso
         and      semestre_id = pSemestre
         --and      grupo_id = pGrupo
         and      pGrupo like '%' || grupo_id || '%'
         and      tipo_subgrupo_id in ('TE')
         and      estudio_id = pEstudio
         and      exists (
                     select 1
                     from   uji_horarios.hor_items i2,
                            uji_horarios.hor_items_asignaturas asi2
                     where  i.id <> i2.id
                     and    i2.id = asi2.item_id
                     --and    i.curso_id = i2.curso_id
                     and    i.semestre_id = i2.semestre_id
                     and    i.grupo_id = i2.grupo_id
                     and    i.tipo_subgrupo_id <> i2.tipo_subgrupo_id
                     and    i.dia_semana_id = i2.dia_semana_id
                     and    asi.estudio_id = asi2.estudio_id
                     and    asi.asignatura_id = asi2.asignatura_id
                     and    (   to_number (to_char (i.hora_inicio, 'hh24mi')) between to_number
                                                                                               (to_char (i2.hora_inicio,
                                                                                                         'hh24mi'))
                                                                                  and to_number (to_char (i2.hora_fin,
                                                                                                          'hh24mi'))
                             or to_number (to_char (i.hora_fin, 'hh24mi')) between to_number (to_char (i2.hora_inicio,
                                                                                                       'hh24mi'))
                                                                               and to_number (to_char (i2.hora_fin,
                                                                                                       'hh24mi'))
                            ))
         order by 1,
                  2,
                  3;

      cursor lista_errores_2 is
         select distinct asignatura_id, grupo_id, tipo_subgrupo_id || subgrupo_id subgrupo,
                         decode (dia_semana_id,
                                 1, 'Dilluns',
                                 2, 'Dimarts',
                                 3, 'Dimecres',
                                 4, 'Dijous',
                                 5, 'Divendres',
                                 6, 'Dissabte',
                                 7, 'Diumenge',
                                 'Error'
                                ) dia_semana,
                         to_char (inicio, 'dd/mm/yyyy') fecha, to_char (inicio, 'hh24:mi') hora_inicio,
                         to_char (fin, 'hh24:mi') hora_fin,
                         (select asi2.asignatura_id || ' ' || i2.tipo_subgrupo_id || i2.subgrupo_id || ' - '
                                 || to_char (inicio, 'hh24:mi') || '-' || to_char (fin, 'hh24:mi') grupo
                          from   uji_horarios.hor_items i2,
                                 uji_horarios.hor_items_asignaturas asi2,
                                 uji_horarios.hor_items_detalle det2
                          where  i.id <> i2.id
                          and    i2.id = det2.item_id
                          and    i2.id = asi2.item_id
                          --and    i.curso_id = i2.curso_id
                          and    i.semestre_id = i2.semestre_id
                          and    i.grupo_id = i2.grupo_id
                          and    i.tipo_subgrupo_id <> i2.tipo_subgrupo_id
                          and    i.dia_semana_id = i2.dia_semana_id
                          and    asi.estudio_id = asi2.estudio_id
                          and    asi.asignatura_id = asi2.asignatura_id
                          and    trunc (det.inicio) = trunc (det2.inicio)
                          and    (   to_number (to_char (det.inicio + 1 / 1440, 'hh24mi'))
                                        between to_number (to_char (det2.inicio, 'hh24mi'))
                                            and to_number (to_char (det2.fin, 'hh24mi'))
                                  or to_number (to_char (det.fin - 1 / 1440, 'hh24mi'))
                                        between to_number (to_char (det2.inicio, 'hh24mi'))
                                            and to_number (to_char (det2.fin, 'hh24mi'))
                                 )
                          and    rownum <= 1) solape
         from            uji_horarios.hor_items i,
                         uji_horarios.hor_items_asignaturas asi,
                         uji_horarios.hor_items_detalle det
         where           i.id = asi.item_id
         and             i.id = det.item_id
         and             asi.curso_id = pCurso
         and             semestre_id = pSemestre
         --and             grupo_id = pGrupo
         and             pGrupo like '%' || grupo_id || '%'
         and             tipo_subgrupo_id in ('TE')
         and             estudio_id = pEstudio
         and             exists (
                            select 1
                            from   uji_horarios.hor_items i2,
                                   uji_horarios.hor_items_asignaturas asi2,
                                   uji_horarios.hor_items_detalle det2
                            where  i.id <> i2.id
                            and    i2.id = det2.item_id
                            and    i2.id = asi2.item_id
                            --and    i.curso_id = i2.curso_id
                            and    i.semestre_id = i2.semestre_id
                            and    i.grupo_id = i2.grupo_id
                            and    i.tipo_subgrupo_id <> i2.tipo_subgrupo_id
                            and    i.dia_semana_id = i2.dia_semana_id
                            and    asi.estudio_id = asi2.estudio_id
                            and    asi.asignatura_id = asi2.asignatura_id
                            and    trunc (det.inicio) = trunc (det2.inicio)
                            and    (   to_number (to_char (det.inicio + 1 / 1440, 'hh24mi'))
                                          between to_number (to_char (det2.inicio, 'hh24mi'))
                                              and to_number (to_char (det2.fin, 'hh24mi'))
                                    or to_number (to_char (det.fin - 1 / 1440, 'hh24mi'))
                                          between to_number (to_char (det2.inicio, 'hh24mi'))
                                              and to_number (to_char ((det2.fin), 'hh24mi'))
                                   ))
         order by        1,
                         2,
                         3,
                         5;
   begin
      fop.block ('Control 2 - Solpament subgrups TE amb la resta', font_size => 12, font_weight => 'bold',
                 border_bottom => 'solid', space_before => 0.5);
      v_asignatura := 'PRIMERA';

      for x in lista_errores_2 loop
         v_aux := v_aux + 1;

         if     v_aux <> 1
            and v_asignatura <> x.asignatura_id then
            p (' ');
         end if;

         p (x.asignatura_id || ' ' || x.grupo_id || ' ' || x.subgrupo || ' ' || x.dia_semana || ' - ' || x.fecha || ' '
            || x.hora_inicio || '  ->  solapa amb: ' || x.solape);
         v_asignatura := x.asignatura_id;
      end loop;

      if v_aux = 0 then
         fop.block ('No hi ha cap error.', space_before => 0.5);
      elsif v_aux = 1 then
         fop.block ('Trovat ' || v_aux || ' error.', space_before => 0.5);
      else
         fop.block ('Trovats ' || v_aux || ' errors.', space_before => 0.5);
      end if;
   exception
      when others then
         p (sqlerrm);
   end;

   procedure control_3 is
      v_aux          number        := 0;
      v_asignatura   varchar2 (20);
      v_control      number;

      cursor lista_asignaturas is
         select distinct asi.asignatura_id
         from            uji_horarios.hor_items i,
                         uji_horarios.hor_items_asignaturas asi,
                         uji_horarios.hor_items_detalle det
         where           i.id = asi.item_id
         and             i.id = det.item_id
         and             asi.curso_id = pCurso
         and             semestre_id = pSemestre
         --and             grupo_id = pGrupo
         and             pGrupo like '%' || grupo_id || '%'
         and             estudio_id = pEstudio
         order by        1;

      cursor lista_errores_3 (p_asignatura in varchar2) is
         select   *
         from     (select   asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo, creditos, horas_totales,
                            sum (horas_detalle) horas_detalle, decode (tipo, 'S', 'Semestral', 'A', 'Anual') tipo
                   from     (select asi.asignatura_id, grupo_id, tipo_subgrupo_id,
                                    tipo_subgrupo_id || subgrupo_id subgrupo,
                                    decode (tipo_subgrupo_id,
                                            'TE', crd_te,
                                            'PR', crd_pr,
                                            'LA', crd_la,
                                            'SE', crd_se,
                                            'TU', crd_tu,
                                            crd_ev
                                           ) creditos,
                                    decode (tipo_subgrupo_id,
                                            'TE', crd_te,
                                            'PR', crd_pr,
                                            'LA', crd_la,
                                            'SE', crd_se,
                                            'TU', crd_tu,
                                            crd_ev
                                           )
                                    / decode (ad.tipo, 'A', 2, 1) * 10 horas_totales,
                                    (fin - inicio) * 24 horas_detalle, ad.tipo tipo
                             from   uji_horarios.hor_items i,
                                    uji_horarios.hor_items_asignaturas asi,
                                    uji_horarios.hor_items_detalle det,
                                    uji_horarios.hor_ext_asignaturas_detalle ad
                             where  i.id = asi.item_id
                             and    i.id = det.item_id
                             and    asi.curso_id = pCurso
                             and    semestre_id = pSemestre
                             --and    grupo_id = pGrupo
                             and    pGrupo like '%' || grupo_id || '%'
                             and    estudio_id = pEstudio
                             and    asi.asignatura_id = ad.asignatura_id
                             and    asi.asignatura_id = p_asignatura
                             and    ad.tipo = 'S'
                             and    dia_semana_id is not null)
                   group by asignatura_id,
                            grupo_id,
                            tipo_subgrupo_id,
                            subgrupo,
                            creditos,
                            tipo
                   having   horas_totales not between sum (horas_detalle) * 0.95 and sum (horas_detalle) * 1.05
                   union all
                   select   asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo, max (creditos) creditos,
                            max (horas_totales) horas_totales, sum (horas_detalle) horas_detalle,
                            decode (tipo, 'S', 'Semestral', 'A', 'Anual') tipo
                   from     (select   asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo, creditos, horas_totales,
                                      sum (horas_detalle) horas_detalle, tipo, null semestre_id
                             from     (select asi.asignatura_id, grupo_id, tipo_subgrupo_id,
                                              tipo_subgrupo_id || subgrupo_id subgrupo,
                                              decode (tipo_subgrupo_id,
                                                      'TE', crd_te,
                                                      'PR', crd_pr,
                                                      'LA', crd_la,
                                                      'SE', crd_se,
                                                      'TU', crd_tu,
                                                      crd_ev
                                                     ) creditos,
                                              decode (tipo_subgrupo_id,
                                                      'TE', crd_te,
                                                      'PR', crd_pr,
                                                      'LA', crd_la,
                                                      'SE', crd_se,
                                                      'TU', crd_tu,
                                                      crd_ev
                                                     )
                                              * 10 horas_totales,
                                              (fin - inicio) * 24 horas_detalle, ad.tipo tipo, semestre_id
                                       from   uji_horarios.hor_items i,
                                              uji_horarios.hor_items_asignaturas asi,
                                              uji_horarios.hor_items_detalle det,
                                              uji_horarios.hor_ext_asignaturas_detalle ad
                                       where  i.id = asi.item_id
                                       and    i.id = det.item_id
                                       and    asi.curso_id = pCurso
                                       --and    semestre_id = :pSemestre
                                       --and    grupo_id = pGrupo
                                       and    pGrupo like '%' || grupo_id || '%'
                                       and    estudio_id = pEstudio
                                       and    asi.asignatura_id = ad.asignatura_id
                                       and    asi.asignatura_id = p_asignatura
                                       and    ad.tipo = 'A'
                                       and    dia_semana_id is not null)
                             group by asignatura_id,
                                      grupo_id,
                                      tipo_subgrupo_id,
                                      subgrupo,
                                      creditos,
                                      tipo                                                                           --,
                             --semestre_id
                             having   horas_totales not between sum (horas_detalle) * 0.95 and sum (horas_detalle)
                                                                                               * 1.05)
                   group by asignatura_id,
                            grupo_id,
                            tipo_subgrupo_id,
                            subgrupo,
                            tipo)
         order by 1,
                  decode (tipo_subgrupo_id, 'TE', 1, 'PR', 2, 'LA', 3, 'SE', 4, 'TU', 5, 6),
                  3;
   begin
      v_control := 0;
      fop.block ('Control 3 - Control d''hores planificades per subgrup', font_size => 12, font_weight => 'bold',
                 border_bottom => 'solid', space_before => 0.5);
      v_asignatura := 'PRIMERA';
      v_control := 1;

      for asi in lista_asignaturas loop
         v_control := 2;

         for x in lista_errores_3 (asi.asignatura_id) loop
            v_control := 3;
            v_aux := v_aux + 1;

            if     v_aux <> 1
               and v_asignatura <> x.asignatura_id then
               p (' ');
            end if;

            if x.horas_totales > x.horas_detalle then
               p (x.asignatura_id || ' ' || x.grupo_id || ' ' || x.subgrupo || ' -' || x.tipo || '- '
                  || 'Planificades ' || to_char (x.horas_totales - x.horas_detalle)
                  || ' hores de menys, cal planificar-ne ' || x.horas_totales);
            else
               p (x.asignatura_id || ' ' || x.grupo_id || ' ' || x.subgrupo || ' -' || x.tipo || '- '
                  || 'Planificades ' || to_char (x.horas_detalle - x.horas_totales)
                  || ' hores de m?s, cal planificar-ne ' || x.horas_totales);
            end if;

            v_asignatura := x.asignatura_id;
         end loop;
      end loop;

      v_control := 6;

      if v_aux = 0 then
         fop.block ('No hi ha cap error.', space_before => 0.5);
      elsif v_aux = 1 then
         fop.block ('Trovat ' || v_aux || ' error.', space_before => 0.5);
      else
         fop.block ('Trovats ' || v_aux || ' errors.', space_before => 0.5);
      end if;
   exception
      when others then
         p (v_control || '-' || sqlerrm);
   end;

   procedure control_4 is
      v_aux          number        := 0;
      v_asignatura   varchar2 (20);

      cursor lista_errores_4 is
         select   asi.asignatura_id, grupo_id, i.tipo_subgrupo_id, subgrupo_id,
                  to_char (hora_inicio, 'hh24:mi') hora_inicio,
                  decode (dia_semana_id,
                          1, 'Dilluns',
                          2, 'Dimarts',
                          3, 'Dimecres',
                          4, 'Dijous',
                          5, 'Divendres',
                          6, 'Dissabte',
                          7, 'Diumenge',
                          'Error'
                         ) dia_semana,
                  dia_semana_id
         from     uji_horarios.hor_items i,
                  uji_horarios.hor_items_asignaturas asi
         where    i.id = asi.item_id
         and      asi.curso_id = pCurso
         and      semestre_id = pSemestre
         --and      grupo_id = pGrupo
         and      pGrupo like '%' || grupo_id || '%'
         and      estudio_id = pEstudio
         and      dia_semana_id is not null
         and      aula_planificacion_id is null
         order by 1,
                  decode (tipo_subgrupo_id, 'TE', 1, 'PR', 2, 'LA', 3, 'SE', 4, 'TU', 5, 6),
                  4;
   begin
      fop.block ('Control 4 - Control d''assignaci? d''aules per subgrup', font_size => 12, font_weight => 'bold',
                 border_bottom => 'solid', space_before => 0.5);
      v_asignatura := 'PRIMERA';

      for x in lista_errores_4 loop
         v_aux := v_aux + 1;

         if     v_aux <> 1
            and v_asignatura <> x.asignatura_id then
            p (' ');
         end if;

         p (x.asignatura_id || ' ' || x.grupo_id || ' ' || x.tipo_subgrupo_id || x.subgrupo_id || ' ' || x.dia_semana
            || ' ' || x.hora_inicio || ' - no te aula assignada');
         v_asignatura := x.asignatura_id;
      end loop;
   exception
      when others then
         p (sqlerrm);
   end;

   procedure control_5 is
      v_aux          number          := 0;
      v_aula         number;
      v_dia_semana   number;
      v_hora         varchar2 (20);
      v_txt          varchar2 (2000);

      cursor lista_errores_5 is
         select   dia_semana_id, trunc (det.inicio) fecha, to_char (hora_inicio, 'hh24:mi') hora_inicio,
                  aula_planificacion_id,
                  decode (dia_semana_id,
                          1, 'Dilluns',
                          2, 'Dimarts',
                          3, 'Dimecres',
                          4, 'Dijous',
                          5, 'Divendres',
                          6, 'Dissabte',
                          7, 'Diumenge',
                          'Error'
                         ) dia_semana,
                  nvl (a.nombre, 'Aula mal assignada a l''estudi (' || aula_planificacion_id || ')') nombre_aula
         from     uji_horarios.hor_items i,
                  uji_horarios.hor_items_asignaturas asi,
                  uji_horarios.hor_items_detalle det,
                  uji_horarios.hor_aulas a
         where    i.id = asi.item_id
         and      i.id = det.item_id
         and      asi.curso_id = pCurso
         and      semestre_id = pSemestre
         --and      grupo_id = pGrupo
         and      pGrupo like '%' || grupo_id || '%'
         and      estudio_id = pEstudio
         and      dia_semana_id is not null
         and      aula_planificacion_id is not null
         and      aula_planificacion_id = a.id(+)
         group by                                                                                   --asi.asignatura_id,
                  grupo_id,
                  dia_semana_id,
                  trunc (det.inicio),
                  to_char (hora_inicio, 'hh24:mi'),
                  aula_planificacion_id,
                  a.nombre
         having   count (*) > 1
         order by 5,
                  1,
                  3,
                  2;

      cursor lista_subgrupos (p_dia_Semana in number, p_fecha in date, p_hora in varchar2, p_aula in number) is
         select   nvl (comun_texto, asignatura_id) asignatura_id, grupo_id, tipo_subgrupo_id || subgrupo_id subgrupo
         from     uji_horarios.hor_items i,
                  uji_horarios.hor_items_asignaturas asi,
                  uji_horarios.hor_items_detalle det
         where    i.id = asi.item_id
         and      i.id = det.item_id
         and      dia_semana_id = p_dia_semana
         and      trunc (inicio) = p_fecha
         and      to_char (hora_inicio, 'hh24:mi') = p_hora
         and      aula_planificacion_id = p_aula
         and      estudio_id = pEstudio
         order by 1,
                  2;
   begin
      fop.block ('Control 5 - Control de solapament en aules', font_size => 12, font_weight => 'bold',
                 border_bottom => 'solid', space_before => 0.5);
      v_aula := 0;
      v_dia_semana := 0;
      v_hora := '';

      for x in lista_errores_5 loop
         v_aux := v_aux + 1;

         if     v_aux <> 1
            and not (    v_aula = x.aula_planificacion_id
                     and v_dia_semana = x.dia_semana_id
                     and v_hora = x.hora_inicio) then
            p (' ');
         end if;

         v_txt := null;

         if 1 = 2 then
            p (x.nombre_aula || ' - ' || to_char (x.fecha, 'dd/mm/yyyy') || ' ' || x.dia_semana || ' ' || x.hora_inicio);

            for det in lista_subgrupos (x.dia_semana_id, x.fecha, x.hora_inicio, x.aula_planificacion_id) loop
               p (espacios (30) || det.grupo_id || ' ' || det.asignatura_id || '-' || det.subgrupo);
            end loop;
         else
            for det in lista_subgrupos (x.dia_semana_id, x.fecha, x.hora_inicio, x.aula_planificacion_id) loop
               v_txt := v_txt || det.asignatura_id || '-' || det.grupo_id || ' ' || det.subgrupo || ' | ';
            end loop;

            v_txt := trim (v_txt);
            v_txt := substr (v_txt, 1, length (v_txt) - 2);
            p (x.nombre_aula || ' - ' || to_char (x.fecha, 'dd/mm/yyyy') || ' ' || x.dia_semana || ' ' || x.hora_inicio
               || ' ==> ' || v_txt);
         end if;

         v_aula := x.aula_planificacion_id;
         v_dia_semana := x.dia_semana_id;
         v_hora := x.hora_inicio;
      end loop;
   exception
      when others then
         p (sqlerrm);
   end;

   procedure advertencia_1 is
      v_aux             number        := 0;
      v_asignatura      varchar2 (20);
      v_tipo_subgrupo   varchar2 (20);
      v_subgrupo        number;

      cursor lista_advertencia_1 is
         select   asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo_id
         from     uji_horarios.hor_items i,
                  uji_horarios.hor_items_asignaturas asi
         where    i.id = asi.item_id
         and      asi.curso_id = pCurso
         and      semestre_id = pSemestre
         --and      grupo_id = pGrupo
         and      pGrupo like '%' || grupo_id || '%'
         and      estudio_id = pEstudio
         and      dia_semana_id is not null
         and      aula_planificacion_id is not null
         group by asignatura_id,
                  grupo_id,
                  tipo_subgrupo_id,
                  subgrupo_id
         having   count (distinct aula_planificacion_id) > 1;

      cursor lista_aulas (p_asignatura in varchar2, p_tipo_subgrupo in varchar2, p_subgrupo in number) is
         select distinct nvl (a.nombre, 'Aula mal assignada a l''estudi (' || aula_planificacion_id || ')') nombre_aula
         from            uji_horarios.hor_items i,
                         uji_horarios.hor_items_asignaturas asi,
                         uji_horarios.hor_aulas a
         where           i.id = asi.item_id
         and             i.aula_planificacion_id = a.id(+)
         and             asi.curso_id = pCurso
         and             semestre_id = pSemestre
         --and             grupo_id = pGrupo
         and             pGrupo like '%' || grupo_id || '%'
         and             estudio_id = pEstudio
         and             dia_semana_id is not null
         and             aula_planificacion_id is not null
         and             asignatura_id = p_asignatura
         and             tipo_subgrupo_id = p_tipo_subgrupo
         and             subgrupo_id = p_subgrupo
         order by        1;
   begin
      fop.block ('Advertencia 1 - Classes d''un subgrup en aules diferents', font_size => 12, font_weight => 'bold',
                 border_bottom => 'solid', space_before => 0.5);
      v_aux := v_aux + 1;
      v_asignatura := 'PRIMERA';
      v_tipo_subgrupo := 'XX';
      v_subgrupo := 0;

      for x in lista_advertencia_1 loop
         v_aux := v_aux + 1;

         if     v_aux <> 1
            and not (    v_asignatura = x.asignatura_id
                     and v_tipo_subgrupo = x.tipo_subgrupo_id
                     and v_subgrupo = x.subgrupo_id
                    ) then
            p (' ');
         end if;

         for det in lista_aulas (x.asignatura_id, x.tipo_subgrupo_id, x.subgrupo_id) loop
            p (x.asignatura_id || ' - ' || x.grupo_id || ' ' || x.tipo_subgrupo_id || x.subgrupo_id || ' '
               || det.nombre_aula);
         end loop;

         v_asignatura := x.asignatura_id;
         v_tipo_subgrupo := x.tipo_subgrupo_id;
         v_subgrupo := x.subgrupo_id;
      end loop;
   exception
      when others then
         p (sqlerrm);
   end;
BEGIN
   select count (*)
   into   vSesion
   from   apa_sesiones
   where  sesion_id = pSesion
   and    fecha < sysdate () - 0.33;

   if vSesion > 0 then
      select nombre
      into   v_nombre
      from   hor_estudios
      where  id = pEstudio;

      cabecera;
      control_1;
      control_2;
      control_3;
      control_4;
      control_5;
      advertencia_1;
      pie;
   else
      cabecera;
      fop.block ('No tens perm?s per accedir a aquest document.', font_weight => 'bold', text_align => 'center',
                 font_size => 14, space_after => 0.5);
      pie;
   end if;
EXCEPTION
   WHEN OTHERS THEN
      htp.p (sqlerrm);
END;

CREATE OR REPLACE TRIGGER UJI_HORARIOS.mutante_3_final_2014
   after insert or update of dia_semana_id,
                             desde_el_dia,
                             hasta_el_dia,
                             repetir_cada_semanas,
                             numero_iteraciones,
                             hora_inicio,
                             hora_fin
   ON UJI_HORARIOS.HOR_ITEMS
begin
   declare
      v_aux           number;
      v_num_items     number;
      v_id            number;
      v_hora_ini      date;
      v_hora_fin      date;
      v_ult_orden     number;
      v_dia_item      number;
      v_dia_Detalle   number;

      cursor reg (v_rowid rowid) is
         select *
           from uji_horarios.hor_items
          where rowid = v_rowid;

      cursor lista_detalle (
         p_id                     in   number,
         p_detalle_manual         in   number,
         p_dia_semana_id          in   number,
         p_semestre_id            in   number,
         p_numero_iteraciones     in   number,
         p_repetir_cada_Semanas   in   number,
         p_hasta_el_dia           in   date,
         p_desde_el_dia           in   date,
         p_subgrupo_id            in   number,
         p_tipo_subgrupo_id       in   varchar2,
         p_grupo_id               in   varchar2
      ) is
         select   todo.*, row_number () over (partition by id order by fecha) orden_final
             from (select distinct id, fecha, docencia_paso_1, docencia_paso_2, docencia, orden_id, numero_iteraciones,
                                   repetir_cada_semanas, fecha_inicio, fecha_fin, semestre_id, grupo_id,
                                   tipo_subgrupo_id, subgrupo_id, dia_semana_id, tipo_dia, festivos
                              from (select i.id, fecha, docencia docencia_paso_1,
                                           decode (nvl (d.repetir_cada_semanas, 1),
                                                   1, docencia,
                                                   decode (mod (orden_id, d.repetir_cada_semanas), 1, docencia, 'N')
                                                  ) docencia_paso_2,
                                           decode
                                                 (tipo_dia,
                                                  'F', 'N',
                                                  decode (d.numero_iteraciones,
                                                          null, decode (nvl (d.repetir_cada_semanas, 1),
                                                                        1, docencia,
                                                                        decode (mod (orden_id, d.repetir_cada_semanas),
                                                                                1, docencia,
                                                                                'N'
                                                                               )
                                                                       ),
                                                          decode (sign (((orden_id - festivos) / d.repetir_cada_semanas
                                                                        )
                                                                        - (d.numero_iteraciones)),
                                                                  1, 'N',
                                                                  decode (nvl (d.repetir_cada_semanas, 1),
                                                                          1, docencia,
                                                                          decode (mod (orden_id, d.repetir_cada_semanas),
                                                                                  1, docencia,
                                                                                  'N'
                                                                                 )
                                                                         )
                                                                 )
                                                         )
                                                 ) docencia,
                                           d.orden_id, d.numero_iteraciones, d.repetir_cada_semanas, d.fecha_inicio,
                                           d.fecha_fin, d.estudio_id, d.semestre_id, d.asignatura_id, d.grupo_id,
                                           d.tipo_subgrupo_id, d.subgrupo_id, d.dia_semana_id, tipo_dia, festivos
                                      from (select id, fecha,
                                                   hor_contar_festivos (decode (x.desde_el_dia,
                                                                                null, fecha_inicio,
                                                                                x.desde_el_dia
                                                                               ),
                                                                        x.fecha, x.dia_semana_id,
                                                                        x.repetir_cada_semanas) festivos,
                                                   row_number () over (partition by decode
                                                                                      (decode
                                                                                             (sign (x.fecha
                                                                                                    - fecha_inicio),
                                                                                              -1, 'N',
                                                                                              decode (sign (fecha_fin
                                                                                                            - x.fecha),
                                                                                                      -1, 'N',
                                                                                                      'S'
                                                                                                     )
                                                                                             ),
                                                                                       'S', decode
                                                                                          (decode
                                                                                              (sign
                                                                                                  (x.fecha
                                                                                                   - decode
                                                                                                        (x.desde_el_dia,
                                                                                                         null, fecha_inicio,
                                                                                                         x.desde_el_dia
                                                                                                        )),
                                                                                               -1, 'N',
                                                                                               decode
                                                                                                  (sign
                                                                                                      (decode
                                                                                                          (x.hasta_el_dia,
                                                                                                           null, fecha_fin,
                                                                                                           x.hasta_el_dia
                                                                                                          )
                                                                                                       - x.fecha),
                                                                                                   -1, 'N',
                                                                                                   'S'
                                                                                                  )
                                                                                              ),
                                                                                           'S', 'S',
                                                                                           'N'
                                                                                          ),
                                                                                       'N'
                                                                                      ), id, estudio_id, semestre_id, asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo_id, dia_semana_id order by fecha)
                                                                                                               orden_id,
                                                   decode (hor_f_fecha_entre (x.fecha, fecha_inicio, fecha_fin),
                                                           'S', decode (hor_f_fecha_entre (x.fecha,
                                                                                           decode (desde_el_dia,
                                                                                                   null, fecha_inicio,
                                                                                                   desde_el_dia
                                                                                                  ),
                                                                                           decode (hasta_el_dia,
                                                                                                   null, fecha_fin,
                                                                                                   hasta_el_dia
                                                                                                  )),
                                                                        'S', 'S',
                                                                        'N'
                                                                       ),
                                                           'N'
                                                          ) docencia,
                                                   estudio_id, semestre_id, grupo_id, tipo_subgrupo_id, subgrupo_id,
                                                   dia_semana_id, asignatura_id, fecha_inicio, fecha_fin,
                                                   fecha_examenes_inicio, fecha_examenes_fin, desde_el_dia,
                                                   hasta_el_dia, repetir_cada_semanas, numero_iteraciones,
                                                   detalle_manual, tipo_dia, dia_semana
                                              from (select distinct i.id, null estudio_id, i.semestre_id, i.grupo_id,
                                                                    i.tipo_subgrupo_id, i.subgrupo_id, i.dia_semana_id,
                                                                    null asignatura_id, fecha_inicio, fecha_fin,
                                                                    fecha_examenes_inicio, fecha_examenes_fin,
                                                                    i.desde_el_dia, hasta_el_dia, repetir_cada_semanas,
                                                                    numero_iteraciones, detalle_manual, c.fecha,
                                                                    tipo_dia, dia_semana
                                                               from hor_v_fechas_estudios s,
                                                                    (select p_id id, p_detalle_manual detalle_manual,
                                                                            p_dia_semana_id dia_semana_id,
                                                                            p_semestre_id semestre_id,
                                                                            p_numero_iteraciones numero_iteraciones,
                                                                            p_repetir_cada_semanas repetir_cada_semanas,
                                                                            p_hasta_el_dia hasta_el_dia,
                                                                            p_desde_el_dia desde_el_dia,
                                                                            p_subgrupo_id subgrupo_id,
                                                                            p_tipo_subgrupo_id tipo_subgrupo_id,
                                                                            p_grupo_id grupo_id
                                                                       from dual) i,
                                                                    hor_ext_calendario c
                                                              where i.semestre_id = s.semestre_id
                                                                and trunc (c.fecha) between fecha_inicio
                                                                                        and decode (fecha_examenes_fin,
                                                                                                    null, fecha_fin,
                                                                                                    fecha_examenes_fin
                                                                                                   )
                                                                and c.dia_semana_id = i.dia_semana_id
                                                                and tipo_dia in ('L', 'E', 'F')
                                                                and vacaciones = 0
                                                                and s.tipo_estudio_id = 'G'
                                                                and detalle_manual = 0
                                                                and s.estudio_id in (
                                                                          select estudio_id
                                                                            from hor_items_asignaturas
                                                                           where item_id = i.id
                                                                             and curso_id = s.curso_id)
                                                                                                       --and i.id = 559274
                                                   ) x) d,
                                           (select p_id id, p_detalle_manual detalle_manual,
                                                   p_dia_semana_id dia_semana_id, p_semestre_id semestre_id,
                                                   p_numero_iteraciones numero_iteraciones,
                                                   p_repetir_cada_semanas repetir_cada_semanas,
                                                   p_hasta_el_dia hasta_el_dia, p_desde_el_dia desde_el_dia,
                                                   p_subgrupo_id subgrupo_id, p_tipo_subgrupo_id tipo_subgrupo_id,
                                                   p_grupo_id grupo_id
                                              from dual) i
                                     where i.semestre_id = d.semestre_id
                                       and i.grupo_id = d.grupo_id
                                       and i.tipo_subgrupo_id = d.tipo_subgrupo_id
                                       and i.subgrupo_id = d.subgrupo_id
                                       and i.dia_semana_id = d.dia_semana_id
                                       and i.detalle_manual = 0
                                       and i.id = d.id
                                    union all
                                    select distinct c.id, c.fecha, 'N' docencia_paso_1, 'N' docencia_paso_2,
                                                    decode (d.id, null, 'N', 'S') docencia,
                                                    row_number () over (partition by d.item_id order by d.inicio)
                                                                                                               orden_id,
                                                    numero_iteraciones, repetir_cada_semanas, fecha_inicio, fecha_fin,
                                                    estudio_id, semestre_id, asignatura_id, grupo_id, tipo_subgrupo_id,
                                                    subgrupo_id, dia_semana_id, tipo_dia,
                                                    decode (tipo_dia, 'F', 1, 0) festivos
                                               from (select distinct i.id, c.fecha, numero_iteraciones,
                                                                     repetir_cada_semanas, s.fecha_inicio, s.fecha_fin,
                                                                     null estudio_id, i.semestre_id, null asignatura_id,
                                                                     i.grupo_id, i.tipo_subgrupo_id, i.subgrupo_id,
                                                                     i.dia_semana_id, tipo_dia
                                                                from hor_v_fechas_estudios s,
                                                                     (select p_id id, p_detalle_manual detalle_manual,
                                                                             p_dia_semana_id dia_semana_id,
                                                                             p_semestre_id semestre_id,
                                                                             p_numero_iteraciones numero_iteraciones,
                                                                             p_repetir_cada_semanas
                                                                                                   repetir_cada_semanas,
                                                                             p_hasta_el_dia hasta_el_dia,
                                                                             p_desde_el_dia desde_el_dia,
                                                                             p_subgrupo_id subgrupo_id,
                                                                             p_tipo_subgrupo_id tipo_subgrupo_id,
                                                                             p_grupo_id grupo_id
                                                                        from dual) i,
                                                                     hor_ext_calendario c
                                                               where i.semestre_id = s.semestre_id
                                                                 and trunc (c.fecha) between fecha_inicio
                                                                                         and decode (fecha_examenes_fin,
                                                                                                     null, fecha_fin,
                                                                                                     fecha_examenes_fin
                                                                                                    )
                                                                 and c.dia_semana_id = i.dia_semana_id
                                                                 and tipo_dia in ('L', 'E', 'F')
                                                                 and s.tipo_estudio_id = 'G'
                                                                 and vacaciones = 0
                                                                 and detalle_manual = 1
                                                                 and s.estudio_id in (
                                                                          select estudio_id
                                                                            from hor_items_asignaturas
                                                                           where item_id = i.id
                                                                             and curso_id = s.curso_id)) c,
                                                    hor_items_detalle d
                                              where c.id = d.item_id(+)
                                                and trunc (c.fecha) = trunc (d.inicio(+)))
                             where id = p_id
                               and docencia = 'S') todo
         order by 18;

      function buscar_item (p_item_id in number, p_orden in number)
         return number is
         v_aux   number;

         cursor lista_detalle_ordenado is
            select id, row_number () over (partition by item_id order by trunc (inicio)) orden
              from hor_items_detalle
             where item_id = p_item_id;
      begin
         v_aux := -1;

         for det in lista_detalle_ordenado loop
            if det.orden = p_orden then
               v_aux := det.id;
            end if;
         end loop;

         return v_aux;
      exception
         when no_data_found then
            return (-1);
         when others then
            return (0);
      end;
   begin
      for i in 1 .. mutante_items.v_num loop
         for v_reg in reg (mutante_items.v_var_tabla (i)) loop
            if v_reg.detalle_manual = 0 then
               v_hora_ini := v_Reg.hora_inicio;
               v_hora_fin := v_Reg.hora_fin;

               for x in lista_detalle (v_reg.id, v_reg.detalle_manual, v_reg.dia_semana_id, v_reg.semestre_id,
                                       v_reg.numero_iteraciones, v_reg.repetir_cada_Semanas, v_reg.hasta_el_dia,
                                       v_reg.desde_el_dia, v_reg.subgrupo_id, v_reg.tipo_subgrupo_id, v_reg.grupo_id) loop
                  v_id := buscar_item (v_Reg.id, x.orden_final);

                  --dbms_output.put_line (v_Reg.id || ' ' || to_char (x.fecha, 'dd/mm/yyyyy') || ' ' || x.orden_final);
                  if v_id > 0 then
                     /* por defecto actualiza fechas */
                     --dbms_output.put_line ('update ' || x.orden_final);
                     update hor_items_detalle
                     set inicio =
                            to_Date (to_char (x.fecha, 'dd/mm/yyyy') || ' ' || to_char (v_hora_ini, 'hh24:mi:ss'),
                                     'dd/mm/yyyy hh24:mi:ss'),
                         fin =
                            to_Date (to_char (x.fecha, 'dd/mm/yyyy') || ' ' || to_char (v_hora_fin, 'hh24:mi:ss'),
                                     'dd/mm/yyyy hh24:mi:ss')
                     where  id = v_id;
                  elsif v_id = -1 then
                     --dbms_output.put_line ('insertar ' || x.orden_final);
                     begin
                        /* solo inserta si no le quedan fechas por actualizar */
                        v_aux := uji_horarios.hibernate_sequence.nextval;

                        insert into hor_items_detalle
                                    (id, item_id,
                                     inicio,
                                     fin
                                    )
                        values      (v_aux, v_Reg.id,
                                     to_date (to_char (x.fecha, 'dd/mm/yyyy') || ' '
                                              || to_char (v_hora_ini, 'hh24:mi:ss'),
                                              'dd/mm/yyyy hh24:mi:ss'),
                                     to_date (to_char (x.fecha, 'dd/mm/yyyy') || ' '
                                              || to_char (v_hora_fin, 'hh24:mi:ss'),
                                              'dd/mm/yyyy hh24:mi:ss')
                                    );
                     exception
                        when others then
                           null;
                     end;
                  end if;

                  if x.orden_id > nvl (v_ult_orden, 0) then
                     v_ult_orden := x.orden_final;
                  end if;
               end loop;

               /* y solo borra si sobran al final */
               delete      hor_items_detalle
               where       id in (
                              select id
                                from (select id, inicio, fin,
                                             row_number () over (partition by item_id order by trunc (inicio)) orden
                                        from hor_items_detalle
                                       where item_id = v_reg.id)
                               where orden > v_ult_orden);
            else
               /* detalle manual, si ha cambiado de dia de semana u hora, aplicar este cambio a las sesiones detalladas */
               begin
                  select distinct to_number (to_char (inicio, 'd'))
                             into v_dia_detalle
                             from hor_items_detalle
                            where item_id = v_reg.id;

                  v_dia_item := to_number (to_char (v_Reg.hora_inicio, 'd'));

                  if v_dia_item <> v_dia_detalle then
                     update hor_items_detalle
                     set inicio = inicio + (v_dia_item - v_dia_detalle),
                         fin = fin + (v_dia_item - v_dia_detalle)
                     where  item_id = v_reg.id;

                     delete      uji_horarios.hor_items_detalle
                     where       id in (select d.id
                                          from uji_horarios.hor_items_detalle d,
                                               uji_horarios.hor_ext_calendario c
                                         where item_id = v_Reg.id
                                           and trunc (inicio) = fecha
                                           and tipo_dia in ('F', 'N'));
                  end if;
               exception
                  when no_data_found then
                     null;
               end;

               /* detalle manual, tambien hay que actualizar la hora inicio y hora final */
               begin
                  select inicio, fin
                    into v_hora_ini, v_hora_fin
                    from hor_items_detalle
                   where item_id = v_Reg.id
                     and rownum <= 1;

                  if    to_char (v_hora_ini, 'hh24:mi:ss') <> to_char (v_reg.hora_inicio, 'hh24:mi:ss')
                     or to_char (v_hora_fin, 'hh24:mi:ss') <> to_char (v_reg.hora_fin, 'hh24:mi:ss') then
                     update hor_items_detalle
                     set inicio =
                            to_date (to_char (inicio, 'dd/mm/yyyy') || ' ' || to_char (v_reg.hora_inicio, 'hh24:mi:ss'),
                                     'dd/mm/yyyy hh24:mi:ss'),
                         fin =
                            to_date (to_char (fin, 'dd/mm/yyyy') || ' ' || to_char (v_reg.hora_fin, 'hh24:mi:ss'),
                                     'dd/mm/yyyy hh24:mi:ss')
                     where  item_id = v_reg.id;
                  end if;
               exception
                  when no_data_found then
                     null;
               end;
            end if;
         end loop;
      end loop;
   end;
end mutante_3_final_2014;




CREATE OR REPLACE PACKAGE JIDBA.pack_horarios AS
   PROCEDURE borrar_datos (p_error in out varchar2);

   PROCEDURE generar_datos_base (p_error in out varchar2, p_curso in number);

   PROCEDURE sin_planificar (p_error in out varchar2, p_curso in number, p_tit in number);

   PROCEDURE todas_las_sesiones (p_error in out varchar2, p_curso in number, p_tit in number);

   PROCEDURE ajustar_sesiones (
      p_error   in out   varchar2,
      p_curso   in       number,
      p_tit     in       number,
      p_modo    in       varchar2 default 'X'
   );

   PROCEDURE borrar_items (p_error in out varchar2, p_tit in number);

   PROCEDURE generar_examenes (p_error in out varchar2, p_curso in number, p_tit in number);

   PROCEDURE borrar_examenes (p_error in out varchar2, p_tit in number);
END pack_horarios;


CREATE OR REPLACE PACKAGE BODY JIDBA.pack_horarios AS
   function busca_Fecha (p_fecha in date)
      return date is
      v_dia          number;
      v_dia_2        number;
      v_diferencia   number;
      v_rdo          date;
   begin
      v_rdo := add_months (p_fecha, 12);
      v_dia := to_number (to_char (p_fecha, 'd'));
      v_dia_2 := to_number (to_char (v_rdo, 'd'));

      if v_dia <> v_dia_2 then
         v_diferencia := v_dia_2 - v_dia;
         v_rdo := v_rdo - v_diferencia;
      end if;

      return (v_rdo);
   end;

   function busca_tipo_aca (p_fecha in date)
      return varchar2 is
      v_rdo   varchar2 (100);
   begin
      select tipo_aca
        into v_rdo
        from grh_calendario
       where fecha_completa = p_fecha;

      return (v_rdo);
   end;

   function contar_festivos (p_desde in date, p_hasta in date, p_dia in number)
      return number is
      v_rdo   number;
   begin
      select count (*)
        into v_rdo
        from grh_calendario
       where fecha_completa between p_Desde and p_hasta
         and to_number (to_char (fecha_completa, 'd')) = p_dia
         and tipo_aca <> 'L';

      return (v_rdo);
   end;

   function principio_semestre (p_Fecha in date, p_curso in number, p_semestre in number)
      return varchar2 is
      v_ini       date;
      v_cuantos   number;
      v_rdo       varchar2 (1);
   begin
      select decode (p_semestre, 1, ini_sem1_g, ini_sem2_g)
        into v_ini
        from pod_cursos_aca
       where curso_aca = p_curso;

      select decode (count (*), 0, 'S', 'N')
        into v_rdo
        from grh_calendario
       where fecha_completa >= v_ini
         and to_number (to_char (fecha_completa, 'd')) = to_number (to_char (p_fecha, 'd'))
         and fecha_completa < p_fecha;

      return (v_rdo);
   end;

   function fin_semestre (p_Fecha in date, p_curso in number, p_semestre in number, p_tit in number)
      return varchar2 is
      v_fin       date;
      v_cuantos   number;
      v_rdo       varchar2 (1);
      v_fin_s1    date         := to_date ('23/12/2015', 'dd/mm/yyyy');
      v_fin_s2    date         := to_date ('26/05/2016', 'dd/mm/yyyy');
   begin
      select fecha_fin
        into v_fin_s1
        from uji_horarios.hor_semestres_detalle
       where tipo_estudio_id = 'G'
         and semestre_id = 1;

      select fecha_fin
        into v_fin_s2
        from uji_horarios.hor_semestres_detalle
       where tipo_estudio_id = 'G'
         and semestre_id = 2;

      begin
         select distinct e.fecha_fin
                    into v_fin_s1
                    from uji_horarios.hor_semestres_detalle d,
                         uji_horarios.hor_semestres_detalle_estudio e
                   where d.id = e.detalle_id
                     and tipo_estudio_id = 'G'
                     and semestre_id = 1
                     and estudio_id = p_tit;

         select distinct e.fecha_fin
                    into v_fin_s2
                    from uji_horarios.hor_semestres_detalle d,
                         uji_horarios.hor_semestres_detalle_estudio e
                   where d.id = e.detalle_id
                     and tipo_estudio_id = 'G'
                     and semestre_id = 2
                     and estudio_id = p_tit;
      exception
         when others then
            null;
      end;

      if p_semestre = 1 then
         v_fin := v_fin_s1;
      else
         v_fin := v_fin_s2;
      end if;

      if p_tit is not null then
         begin
            select distinct e.fecha_fin
                       into v_fin
                       from uji_horarios.hor_semestres_detalle d,
                            uji_horarios.hor_semestres_detalle_estudio e
                      where d.id = e.detalle_id
                        and tipo_estudio_id = 'G'
                        and semestre_id = p_semestre
                        and estudio_id = p_tit;
         exception
            when no_data_found then
               null;
         end;
      end if;

      select decode (count (*), 0, 'S', 'N')
        into v_rdo
        from grh_calendario
       where fecha_completa <= v_fin
         and to_number (to_char (fecha_completa, 'd')) = to_number (to_char (p_fecha, 'd'))
         and fecha_completa > p_fecha;

      return (v_rdo);
   end;

   PROCEDURE borrar_datos (p_error in out varchar2) IS
      v_control   number := 0;
   BEGIN
      v_control := 1;

      delete      uji_horarios.HOR_LOGS;

      v_control := 2;

      delete      uji_horarios.HOR_ITEMS_CIRCUITOS;

      v_control := 3;

      delete      uji_horarios.HOR_ITEMS_COMUNES;

      v_control := 4;

      delete      uji_horarios.HOR_ITEMS_DETALLE;

      v_control := 5;

      delete      uji_horarios.HOR_ITEMS_ASIGNATURAS;

      v_control := 6;

      delete      uji_horarios.HOR_ITEMS_PROFESORES;

      v_control := 61;

      delete      uji_horarios.HOR_ITEMS;

      v_control := 7;
      --delete      uji_horarios.HOR_PERMISOS_EXTRA;
      v_control := 8;

      delete      uji_horarios.HOR_CIRCUITOS_ESTUDIOS;

      v_control := 9;

      delete      uji_horarios.HOR_CIRCUITOS;

      v_control := 10;

      delete      uji_horarios.HOR_HORARIOS_HORAS;

      v_control := 11;

      delete      uji_horarios.HOR_AULAS_PLANIFICACION;

      v_control := 12;
      --delete      uji_horarios.HOR_PERMISOS_EXTRA;
      v_control := 13;

      delete      uji_horarios.HOR_EXT_CALENDARIO;

      v_control := 14;

      delete      uji_horarios.HOR_DIAS_SEMANA;

      v_control := 15;

      delete      uji_horarios.HOR_PROFESORES;

      v_control := 16;

      delete      uji_horarios.HOR_SEMESTRES_DETALLE_ESTUDIO;

      v_control := 161;

      delete      uji_horarios.HOR_SEMESTRES_DETALLE;

      v_control := 17;

      delete      uji_horarios.HOR_SEMESTRES;

      v_control := 18;

      delete      uji_horarios.HOR_AREAS;

      v_control := 19;

      delete      uji_horarios.HOR_DEPARTAMENTOS;

      v_control := 20;

      delete      uji_horarios.HOR_EXAMENES_ASIGNATURAS;

      v_control := 201;

      delete      uji_horarios.HOR_AGRUPACIONES_ASIGNATURAS;

      v_control := 202;

      delete      uji_horarios.HOR_AGRUPACIONES_ESTUDIOS;

      v_control := 2021;

      delete      uji_horarios.HOR_AGRUPACIONES;

      v_control := 203;

      delete      uji_horarios.HOR_ESTUDIOS_COMPARTIDOS;

      v_control := 2031;

      delete      uji_horarios.HOR_ESTUDIOS;

      v_control := 21;

      delete      uji_horarios.HOR_CURSO_ACADEMICO;

      v_control := 22;

      delete      uji_horarios.HOR_AULAS;

      v_control := 23;

      delete      uji_horarios.HOR_CENTROS;

      v_control := 24;

      delete      uji_horarios.HOR_TIPOS_CARGOS;

      v_control := 25;

      delete      uji_horarios.HOR_EXAMENES;

      v_control := 25;

      delete      uji_horarios.HOR_TIPOS_EXAMENES;

      v_control := 26;

      delete      uji_horarios.HOR_TIPOS_ESTUDIOS;

      v_control := 27;
      commit;
      p_Error := '1Dades esborrades correctament';
   exception
      when others then
         p_error := '0' || sqlerrm || ' (' || v_control || ')';
         rollback;
   END;

   PROCEDURE generar_datos_base (p_error in out varchar2, p_curso in number) is
      v_control   number;
   begin
      v_control := 1;

      insert into uji_horarios.hor_dias_semana
         select   dia id, decode (dia, 1, 'Dilluns', 2, 'Dimarts', 3, 'Dimecres', 4, 'Dijous', 5, 'Divendres') nombre
             from (select to_char (sysdate, 'd') dia, to_char (sysdate, 'Day') dia_txt_es
                     from dual
                   union all
                   select to_char (sysdate + 1, 'd'), to_char (sysdate + 1, 'Day')
                     from dual
                   union all
                   select to_char (sysdate + 2, 'd'), to_char (sysdate + 2, 'Day')
                     from dual
                   union all
                   select to_char (sysdate + 3, 'd'), to_char (sysdate + 3, 'Day')
                     from dual
                   union all
                   select to_char (sysdate + 4, 'd'), to_char (sysdate + 4, 'Day')
                     from dual
                   union all
                   select to_char (sysdate + 5, 'd'), to_char (sysdate + 5, 'Day')
                     from dual
                   union all
                   select to_char (sysdate + 6, 'd'), to_char (sysdate + 6, 'Day')
                     from dual)
            where dia between 1 and 5
         order by 1;

      commit;
      v_control := 2;

      insert into uji_horarios.hor_semestres
         select   1 id, 'Primer semestre' nombre
             from dual
         union all
         select   2 id, 'Segon semestre' nombre
             from dual
         order by 2;

      commit;
      v_control := 3;

      insert into uji_horarios.hor_tipos_estudios
         select 'G' id, 'Graus' nombre, 1 orden
           from dual;

      commit;
      v_control := 4;

      Insert into UJI_HORARIOS.HOR_TIPOS_EXAMENES
                  (ID, NOMBRE, POR_DEFECTO, CODIGO
                  )
      Values      (1, 'Teoria', 1, 'TEO'
                  );

      Insert into UJI_HORARIOS.HOR_TIPOS_EXAMENES
                  (ID, NOMBRE, POR_DEFECTO, CODIGO
                  )
      Values      (2, 'Problemes', 0, 'PRO'
                  );

      Insert into UJI_HORARIOS.HOR_TIPOS_EXAMENES
                  (ID, NOMBRE, POR_DEFECTO, CODIGO
                  )
      Values      (3, 'Laboratori', 0, 'LAB'
                  );

      Insert into UJI_HORARIOS.HOR_TIPOS_EXAMENES
                  (ID, NOMBRE, POR_DEFECTO, CODIGO
                  )
      Values      (4, 'Oral', 0, 'ORA'
                  );

      Insert into UJI_HORARIOS.HOR_TIPOS_EXAMENES
                  (ID, NOMBRE, POR_DEFECTO, CODIGO
                  )
      Values      (5, 'Pràctiques', 0, 'PRA'
                  );

      COMMIT;
      v_control := 5;

      insert into uji_horarios.hor_curso_academico
      values      (p_curso);

      commit;
      v_control := 6;

      insert into uji_horarios.hor_semestres_detalle
                  (id, semestre_id, tipo_estudio_id, fecha_inicio, fecha_fin, fecha_examenes_inicio, fecha_examenes_fin,
                   numero_semanas, curso_academico_id)
         select   1 id, 1 semestre_id, 'G' tipo_estudio_id, ini_sem1_g fecha_inicio, fin_sem1_g fecha_fin, fin_sem1_g,
                  fin_sem1_g, round (((fin_sem1_g - ini_sem1_g) / 7 + 0.49999), 0) numero_semanas, cursos_aca
             from pod_cursos_aca
            where cursos_aca = (select id
                                  from uji_horarios.hor_curso_academico)
         union all
         select   2 id, 2 semestre_id, 'G' tipo_estudio_id, ini_sem2_g fecha_inicio, fin_sem2_g fecha_fin, fin_sem2_g,
                  fin_sem2_g, round (((fin_sem2_g - ini_sem2_g) / 7 + 0.49999), 0) numero_semanas, cursos_aca
             from pod_cursos_aca
            where cursos_aca = (select id
                                  from uji_horarios.hor_curso_academico)
         order by 1;

      commit;
      v_control := 7;

      insert into uji_horarios.hor_centros
         select id, nombre
           from est_ubic_estructurales
          where (    tuest_id = 'CE'
                 and id not in (3165))
             or id = 318;

      commit;
      v_control := 8;

      insert into uji_horarios.hor_estudios
         select   id, nombre, tipo_estudio tipo_id, uest_id centro_id,
                  decode (id, 51001, 0, 51002, 0, 51003, 0, 1) oficial, decode (id, 229, 6, 4) numero_cursos
             from pod_titulaciones
            where tipo_estudio in ('G')
              and activa = 'S'
         -- and      id in (210, 211, 212)
         order by 1;

      commit;
      v_control := 9;

      insert into uji_horarios.hor_departamentos
         select   id, nombre, centro_id, activo, 0 consulta_pod
             from (select   uest.id, uest.nombre, uest_id centro_id, estado activo, trel_id,
                            row_number () over (partition by id order by trel_id desc) orden
                       from est_estructuras e,
                            est_ubic_estructurales uest
                      where uest_id_relacionad = uest.id
                        and uest.id in (select ubicacion_id
                                          from grh_vw_contrataciones_ult
                                         where act_id = 'PDI')
                        and id not in (720)
                   union all
                   select   id, nombre, 2, 1, 1, 1
                       from est_ubic_estructurales
                      where id = 2603
                   union all
                   select   id, nombre, 2, 1, 1, 1
                       from est_ubic_estructurales
                      where id in (218, 2262)
                   order by 1)
            where orden = 1
         order by 1;

      commit;
      v_control := 10;

      insert into uji_horarios.hor_areas
         select uest.id, uest.nombre, uest_id departamento_id, estado activa
           from est_estructuras e,
                est_ubic_estructurales uest
          where estado = 1
            and uest_id_relacionad = uest.id
            and tuest_id = 'AC'
            and status = 'A'
            and uest_id in (
                   select uest.id
                     from est_estructuras e,
                          est_ubic_estructurales uest
                    where estado = 1
                      and uest_id_relacionad = uest.id
                      and tuest_id = 'DE'
                      and status = 'A'
                      and trel_id = 4)
            and uest_id in (select id
                              from uji_horarios.hor_departamentos)
         /*union all
         select 0 id, 'Desconeguda' nombre, 318 departamento_id, 0 activa
         from dual*/
         union all
         select 0 id, 'Desconeguda' nombre, 2603 departamento_id, 0 activa
           from dual;

      commit;
      v_control := 11;

      insert into uji_horarios.hor_profesores
         select   x.per_id, x.nombre, cuenta, area_id, ubicacion_id, por_contratar, creditos, cdo_max, reduccion,
                  cod_grh, cod_grh_nombre
             from (select c.per_id, p.apellido1 || ' ' || apellido2 || ', ' || p.nombre nombre,
                          busca_cuenta (c.per_id) cuenta,
                          nvl (ubicacion_id,
                               (select uest_id
                                  from est_estructuras e
                                 where uest_id_relacionad = c.uest_id
                                   and trel_id = 2
                                   and estado = 1)) ubicacion_id,
                          decode (area_id, null, uest_id, area_id) area_id, cdo_max, reduccion,
                          cdo_max - reduccion creditos, decode (ubicacion_id, null, 1, 0) por_contratar, cod_grh,
                          cat.nombre cod_grh_nombre
                     from gra_pod.pod_carga_docente c,
                          pod_categorias cat,
                          per_personas p,
                          (select *
                             from grh_vw_contrataciones_ult
                            where act_id = 'PDI') cont
                    where curso_aca = p_curso
                      and c.per_id = p.id
                      and c.per_id = cont.per_id(+)
                      and cdo_id = cat.id
                      and cdo_max > 0
                      and ubicacion_id in (select id
                                             from uji_horarios.hor_departamentos)) x,
                  est_ubic_estructurales u
            where ubicacion_id = u.id
         order by u.nombre,
                  ubicacion_id,
                  area_id,
                  2;

      commit;
      v_control := 12;

      insert into uji_horarios.hor_aulas
                  (id, nombre, centro_id, tipo, plazas, codigo, area, edificio, planta)
         select u.id id,
                u.descripcion || ' ' || edi_are_area || edi_edificio || planta || dependencia || tubic_id nombre,
                decode (edi_Are_area, 'M', 2922, 'D', 2, uest_id) centro_id, t.nombre tipo, num_alumnos plazas,
                edi_are_area || edi_edificio || planta || dependencia || tubic_id codigo, edi_are_area,
                edi_are_area || edi_edificio, planta
           from est_ubicaciones u,
                est_areas_ubicacion a,
                est_tipos_ubicacion t
          where edi_are_area = a.area
            and u.tubic_id = t.id
            and (   (    area in ('J', 'T', 'H', 'M', 'D')
                     and tubic_id in ('AA', 'AI', 'AL', 'DS', 'TA', 'TL', 'TS', 'CC', 'DL')
                    )
                 or (    area in ('D')
                     and tubic_id in ('AA', 'AI', 'AL', 'PA', 'PC', 'DS'))
                )
            and u.descripcion <> 'BAJA'
         minus
         select id, nombre, centro_id, tipo, plazas, codigo, area, edificio, planta
           from uji_horarios.hor_aulas;

      /* limpiar los codigos duplicados */
      declare
         cursor lista_repetidos is
            select   id, x.nombre, trim (substr (nombre, 1, repe - 1)) nombre_limpio
                from (select a.*, instr (nombre, codigo, 1, 2) repe
                        from uji_horarios.hor_aulas a) x
               where repe > 0
            order by 2;
      begin
         for x in lista_repetidos loop
            update uji_horarios.hor_aulas
            set nombre = x.nombre_limpio
            where  id = x.id;

            commit;
         end loop;
      end;

      commit;
      v_control := 13;

      insert into uji_horarios.hor_ext_calendario
                  (id, dia, mes, año, tipo_dia, dia_semana, dia_semana_id, fecha)
         select   rownum id, dia, mes, año, tipo_aca tipo_dia, dia_semana,
                  decode (dia_semana,
                          'LUNES', 1,
                          'MARTES', 2,
                          'MIÉRCOLES', 3,
                          'JUEVES', 4,
                          'VIERNES', 5,
                          'SÁBADO', 6,
                          'DOMINGO', 7,
                          8
                         ) dia_Semana_id,
                  fecha_completa fecha
             from grh_calendario c,
                  uji_horarios.hor_curso_academico ca
            where fecha_completa between (select min (fecha_completa)
                                            from grh_calendario
                                           where año = ca.id
                                             and semana = to_char (to_Date ('1/9/' || ca.id, 'dd/mm/yyyy'), 'ww'))
                                     and (select max (fecha_completa)
                                            from grh_calendario
                                           where año = ca.id + 1
                                             and semana =
                                                    to_char (to_Date ('30/9/' || to_char (ca.id + 1), 'dd/mm/yyyy'),
                                                             'ww'))
         order by año,
                  mes,
                  dia;

      commit;
      v_control := 14;

      insert into uji_horarios.hor_tipos_cargos
         select 1 id, 'Director d''estudi' nombre, 0, 1
           from dual
         union all
         select 2 id, 'Coordinador de curs' nombre, 0, 1
           from dual
         union all
         select 3 id, 'Director, Dega o Secretari de Centre' nombre, 0, 0
           from dual
         union all
         select 4 id, 'PAS de centre' nombre, 0, 0
           from dual
         union all
         select 5 id, 'PAS de departament' nombre, 1, 0
           from dual
         union all
         select 6 id, 'Director de departament' nombre, 1, 0
           from dual
         union all
         select 0 id, 'Error' nombre, 0, 0
           from dual;

      commit;
      v_control := 15;

      insert into uji_horarios.hor_estudios_compartidos
         select rownum, id, comp
           from (select t.id, t2.id comp
                   from pod_titulaciones t,
                        pod_titulaciones t2
                  where t.id in (210, 211, 212)
                    and t2.id in (210, 211, 212)
                    and t.id <> t2.id
                 union
                 select t.id, t2.id comp
                   from pod_titulaciones t,
                        pod_titulaciones t2
                  where t.id in (220, 221, 222, 227, 228)
                    and t2.id in (220, 221, 222, 227, 228)
                    and t.id <> t2.id
                 union
                 select t.id, t2.id comp
                   from pod_titulaciones t,
                        pod_titulaciones t2
                  where t.id in (215, 216)
                    and t2.id in (215, 216)
                    and t.id <> t2.id
                 union
                 select t.id, t2.id comp
                   from pod_titulaciones t,
                        pod_titulaciones t2
                  where t.id in (223, 225)
                    and t2.id in (223, 225)
                    and t.id <> t2.id
                 union
                 select t.id, t2.id comp
                   from pod_titulaciones t,
                        pod_titulaciones t2
                  where t.id in (233, 234)
                    and t2.id in (233, 234)
                    and t.id <> t2.id);

      p_Error := '1Dades generades correctament';
   exception
      when others then
         p_error := '0' || sqlerrm || ' (' || v_control || ')';
         rollback;
   end;

   PROCEDURE sin_planificar (p_error in out varchar2, p_curso in number, p_tit in number) is
      v_control   number;
      v_aux       number;
      v_aux2      number;

      cursor lista_sin_planificar (p_tit in number) is
         select   sem.id SEMESTRE_ID, grp_id GRUPO_ID, sg.tipo TIPO_SUBGRUPO_ID, tsg.nombre TIPO_SUBGRUPO,
                  sg.id SUBGRUPO_ID, asi.tipo TIPO_ASIGNATURA_ID, ta.nombre TIPO_ASIGNATURA, te.id TIPO_ESTUDIO_ID,
                  te.nombre TIPO_ESTUDIO, 0 DETALLE_MANUAL, creditos CREDITOS, g.asi_id asignatura_id,
                  asi.nombre asignatura, tit_id estudio_id, tit.nombre estudio, cur_id curso_id, caracter caracter_id,
                  cr.nombre caracter, decode (sg.tipo, 'TE', 1, 'PR', 2, 'LA', 3, 'SE', 4, 'TU', 5, 6) orden,
                  g.curso_aca, (select decode (count (*), 0, 0, 1)
                                  from pod_comunes com,
                                       pod_grp_comunes gc
                                 where gc.id = com.gco_id
                                   and curso_aca = g.curso_aca
                                   and asi_id = g.asi_id) comun,
                  (select porcentaje
                     from pod_comunes com,
                          pod_grp_comunes gc
                    where gc.id = com.gco_id
                      and curso_aca = g.curso_aca
                      and asi_id = g.asi_id) comun_porcentaje,
                  (select nombre
                     from pod_comunes com,
                          pod_grp_comunes gc
                    where gc.id = com.gco_id
                      and curso_aca = g.curso_aca
                      and asi_id = g.asi_id) comun_texto, sg.limite_nuevos plazas
             from pod_grupos g,
                  pod_asignaturas_titulaciones a,
                  pod_asi_cursos ac,
                  pod_subgrupos sg,
                  pod_asignaturas asi,
                  pod_titulaciones tit,
                  (select 'AV' id, 'Avaluació' nombre, 7 orden
                     from dual
                   union all
                   select 'LA' id, 'Laboratori' nombre, 4 orden
                     from dual
                   union all
                   select 'PR' id, 'Problemes' nombre, 3 orden
                     from dual
                   union all
                   select 'SE' id, 'Seminari' nombre, 5 orden
                     from dual
                   union all
                   select 'TE' id, 'Teoria' nombre, 1 orden
                     from dual
                   union all
                   select 'TP' id, 'Teoria i problemes' nombre, 2 orden
                     from dual
                   union all
                   select 'TU' id, 'Tutories' nombre, 6 orden
                     from dual) tsg,
                  (select 'S' id, 'Semestral' nombre, 1 orden
                     from dual
                   union all
                   select 'A' id, 'Anual' nombre, 2 orden
                     from dual) ta,
                  (select 'TR' id, 'Troncal' nombre, 1 orden
                     from dual
                   union all
                   select 'FB' id, 'Formació bàsica' nombre, 2 orden
                     from dual
                   union all
                   select 'OB' id, 'Obligatoria' nombre, 3 orden
                     from dual
                   union all
                   select 'OP' id, 'Optativa' nombre, 4 orden
                     from dual
                   union all
                   select 'LC' id, 'Lliure configuració' nombre, 5 orden
                     from dual
                   union all
                   select 'PR' id, 'Pràctiques externes' nombre, 6 orden
                     from dual
                   union all
                   select 'PF' id, 'Treball fi de grau' nombre, 7 orden
                     from dual) cr,
                  (select '12C' id, 'Primer i segon cicle' nombre, 2 orden
                     from dual
                   union all
                   select 'G' id, 'Grau' nombre, 1 orden
                     from dual
                   union all
                   select 'M' id, 'Màster' nombre, 3 orden
                     from dual) te,
                  (select 1 id
                     from dual
                   union
                   select 2
                     from dual) sem
            where g.curso_aca = p_curso
              --and g.asi_id = 'ED0904'
              --and g.asi_id = 'CS1001'
              --and g.asi_id like '%1021'
              --and g.asi_id like '%1002'
              and g.asi_id = a.asi_id
              and a.tit_id = p_tit
              and a.tit_id = tit.id
              and g.asi_id = ac.asi_id
              and a.tit_id = cur_tit_id
              and g.curso_aca = ac.curso_aca
              and g.curso_aca = sg.grp_curso_aca
              and g.asi_id = sg.grp_asi_id
              and g.id = sg.grp_id
              and g.asi_id = asi.id
              and caracter = cr.id
              and sg.tipo = tsg.id
              and asi.tipo = ta.id
              and cur_id <> 7
              and te.id = 'G'
              and (   asi.tipo = 'A'
                   or g.semestre = sem.id)
         group by g.curso_aca,
                  sem.id,
                  grp_id,
                  sg.tipo,
                  tsg.nombre,
                  sg.id,
                  asi.tipo,
                  ta.nombre,
                  te.id,
                  te.nombre,
                  creditos,
                  g.asi_id,
                  asi.nombre,
                  tit_id,
                  tit.nombre,
                  cur_id,
                  caracter,
                  cr.nombre,
                  decode (sg.tipo, 'TE', 1, 'PR', 2, 'LA', 3, 'SE', 4, 'TU', 5, 6),
                  sg.limite_nuevos
         order by g.curso_aca,
                  g.asi_id,
                  sem.id,
                  sg.grp_id,
                  decode (sg.tipo, 'TE', 1, 'PR', 2, 'LA', 3, 'SE', 4, 'TU', 5, 6),
                  sg.id;
   begin
      v_control := 1;
      htpx.br;
      htp.p (p_tit || ' ' || p_curso);

      for x in lista_sin_planificar (p_tit) loop
         v_control := 2;

         if x.comun = 0 then
            v_control := 3;
            htp.p ('Insertar ' || x.asignatura_id || ' ' || x.grupo_id || ' ' || x.tipo_subgrupo_id || x.subgrupo_id
                   || htfx.br);
            v_aux := uji_horarios.hibernate_Sequence.nextval;

            insert into uji_horarios.hor_items
                        (id, semestre_id, aula_planificacion_id, comun, porcentaje_comun, grupo_id, tipo_subgrupo_id,
                         tipo_subgrupo, subgrupo_id, dia_semana_id, hora_inicio, hora_fin, desde_el_dia, hasta_el_dia,
                         tipo_asignatura_id, tipo_asignatura, tipo_estudio_id, tipo_estudio, plazas,
                         repetir_cada_semanas, numero_iteraciones, detalle_manual, comun_texto,
                         aula_planificacion_nombre, creditos
                        )
            values      (v_aux, x.semestre_id, null, x.comun, x.comun_porcentaje, x.grupo_id, x.tipo_subgrupo_id,
                         x.tipo_subgrupo, x.subgrupo_id, null, null, null, null, null,
                         x.tipo_asignatura_id, x.tipo_asignatura, x.tipo_estudio_id, x.tipo_estudio, x.plazas,
                         null, null, 0, x.comun_texto,
                         null, x.creditos
                        );

            v_control := 4;
            v_aux2 := uji_horarios.hibernate_Sequence.nextval;
            v_control := 5;

            insert into uji_horarios.hor_items_asignaturas
                        (id, item_id, asignatura_id, asignatura, estudio_id, estudio, curso_id,
                         caracter_id, caracter
                        )
            values      (v_aux2, v_aux, x.asignatura_id, x.asignatura, x.estudio_id, x.estudio, x.curso_id,
                         x.caracter_id, x.caracter
                        );

            commit;
         else
            begin
               v_control := 11;
               /*htp.p
                  ('   select distinct i.id
                                  from uji_horarios.hor_items i,
                      uji_horarios.hor_items_asignaturas a
                where i.id = a.item_id
                  and semestre_id = '
                   || x.semestre_id || '
                  and grupo_id = ' || x.grupo_id || '
                  and tipo_subgrupo_id = ' || x.tipo_subgrupo_id || '
                  and subgrupo_id = ' || x.subgrupo_id || '
                  and comun_texto like ''%''||' || x.asignatura_id || '||''%'';');
               htpx.br;
               */
               v_control := 12;

               select distinct i.id
                          into v_aux
                          from uji_horarios.hor_items i,
                               uji_horarios.hor_items_asignaturas a
                         where i.id = a.item_id
                           and semestre_id = x.semestre_id
                           and grupo_id = x.grupo_id
                           and tipo_subgrupo_id = x.tipo_subgrupo_id
                           and subgrupo_id = x.subgrupo_id
                           and comun_texto like '%' || x.asignatura_id || '%';

               v_control := 13;
               htp.p ('Insertar COMUN DETALLE ' || x.asignatura_id || ' ' || x.grupo_id || ' ' || x.tipo_subgrupo_id
                      || x.subgrupo_id || htfx.br);
            exception
               when no_data_found then
                  v_aux := uji_horarios.hibernate_Sequence.nextval;
                  htp.p ('Insertar COMUN ' || x.asignatura_id || ' ' || x.grupo_id || ' ' || x.tipo_subgrupo_id
                         || x.subgrupo_id || htfx.br);
                  v_control := 14;

                  insert into uji_horarios.hor_items
                              (id, semestre_id, aula_planificacion_id, comun, porcentaje_comun, grupo_id,
                               tipo_subgrupo_id, tipo_subgrupo, subgrupo_id, dia_semana_id, hora_inicio, hora_fin,
                               desde_el_dia, hasta_el_dia, tipo_asignatura_id, tipo_asignatura, tipo_estudio_id,
                               tipo_estudio, plazas, repetir_cada_semanas, numero_iteraciones, detalle_manual,
                               comun_texto, aula_planificacion_nombre, creditos
                              )
                  values      (v_aux, x.semestre_id, null, x.comun, x.comun_porcentaje, x.grupo_id,
                               x.tipo_subgrupo_id, x.tipo_subgrupo, x.subgrupo_id, null, null, null,
                               null, null, x.tipo_asignatura_id, x.tipo_asignatura, x.tipo_estudio_id,
                               x.tipo_estudio, x.plazas, null, null, 0,
                               x.comun_texto, null, x.creditos
                              );
            end;

            v_control := 15;
            v_aux2 := uji_horarios.hibernate_Sequence.nextval;

            insert into uji_horarios.hor_items_asignaturas
                        (id, item_id, asignatura_id, asignatura, estudio_id, estudio, curso_id,
                         caracter_id, caracter
                        )
            values      (v_aux2, v_aux, x.asignatura_id, x.asignatura, x.estudio_id, x.estudio, x.curso_id,
                         x.caracter_id, x.caracter
                        );

            commit;
         end if;
      end loop;

      p_Error := '1Dades generades correctament';
   exception
      when others then
         p_error := '0' || sqlerrm || ' (' || v_control || ')';
         rollback;
   end;

   PROCEDURE todas_las_Sesiones (p_error in out varchar2, p_curso in number, p_tit in number) is
      v_control     number;
      v_aux         number;
      v_aux2        number;
      v_cuantos     number;
      v_cuantos_2   number;

      cursor lista (p_tit in number) is
         select   sem.id SEMESTRE_ID, grp_id GRUPO_ID, sg.tipo TIPO_SUBGRUPO_ID, tsg.nombre TIPO_SUBGRUPO,
                  sg.id SUBGRUPO_ID, asi.tipo TIPO_ASIGNATURA_ID, ta.nombre TIPO_ASIGNATURA, te.id TIPO_ESTUDIO_ID,
                  te.nombre TIPO_ESTUDIO, 0 DETALLE_MANUAL, creditos CREDITOS, g.asi_id asignatura_id,
                  asi.nombre asignatura, tit_id estudio_id, tit.nombre estudio, cur_id curso_id, caracter caracter_id,
                  cr.nombre caracter, decode (sg.tipo, 'TE', 1, 'PR', 2, 'LA', 3, 'SE', 4, 'TU', 5, 6) orden,
                  g.curso_aca, (select decode (count (*), 0, 0, 1)
                                  from pod_comunes com,
                                       pod_grp_comunes gc
                                 where gc.id = com.gco_id
                                   and curso_aca = g.curso_aca
                                   and asi_id = g.asi_id) comun,
                  (select porcentaje
                     from pod_comunes com,
                          pod_grp_comunes gc
                    where gc.id = com.gco_id
                      and curso_aca = g.curso_aca
                      and asi_id = g.asi_id) comun_porcentaje,
                  (select nombre
                     from pod_comunes com,
                          pod_grp_comunes gc
                    where gc.id = com.gco_id
                      and curso_aca = g.curso_aca
                      and asi_id = g.asi_id) comun_texto, sg.limite_nuevos plazas
             from pod_grupos g,
                  pod_asignaturas_titulaciones a,
                  pod_asi_cursos ac,
                  pod_subgrupos sg,
                  pod_asignaturas asi,
                  pod_titulaciones tit,
                  (select 'AV' id, 'Avaluació' nombre, 7 orden
                     from dual
                   union all
                   select 'LA' id, 'Laboratori' nombre, 4 orden
                     from dual
                   union all
                   select 'PR' id, 'Problemes' nombre, 3 orden
                     from dual
                   union all
                   select 'SE' id, 'Seminari' nombre, 5 orden
                     from dual
                   union all
                   select 'TE' id, 'Teoria' nombre, 1 orden
                     from dual
                   union all
                   select 'TP' id, 'Teoria i problemes' nombre, 2 orden
                     from dual
                   union all
                   select 'TU' id, 'Tutories' nombre, 6 orden
                     from dual) tsg,
                  (select 'S' id, 'Semestral' nombre, 1 orden
                     from dual
                   union all
                   select 'A' id, 'Anual' nombre, 2 orden
                     from dual) ta,
                  (select 'TR' id, 'Troncal' nombre, 1 orden
                     from dual
                   union all
                   select 'FB' id, 'Formació bàsica' nombre, 2 orden
                     from dual
                   union all
                   select 'OB' id, 'Obligatoria' nombre, 3 orden
                     from dual
                   union all
                   select 'OP' id, 'Optativa' nombre, 4 orden
                     from dual
                   union all
                   select 'LC' id, 'Lliure configuració' nombre, 5 orden
                     from dual
                   union all
                   select 'PR' id, 'Pràctiques externes' nombre, 6 orden
                     from dual
                   union all
                   select 'PF' id, 'Treball fi de grau' nombre, 7 orden
                     from dual) cr,
                  (select '12C' id, 'Primer i segon cicle' nombre, 2 orden
                     from dual
                   union all
                   select 'G' id, 'Grau' nombre, 1 orden
                     from dual
                   union all
                   select 'M' id, 'Màster' nombre, 3 orden
                     from dual) te,
                  (select 1 id
                     from dual
                   union
                   select 2
                     from dual) sem
            where g.curso_aca = p_curso
              --and g.asi_id = 'ED0904'
              --and g.asi_id = 'CS1001'
              --and g.asi_id like '%1021'
              --and g.asi_id like '%1002'
              --and g.asi_id in ('EI1043','EI1059')
              and g.asi_id = a.asi_id
              and a.tit_id = p_tit
              and a.tit_id = tit.id
              and g.asi_id = ac.asi_id
              and a.tit_id = cur_tit_id
              and g.curso_aca = ac.curso_aca
              and g.curso_aca = sg.grp_curso_aca
              and g.asi_id = sg.grp_asi_id
              and g.id = sg.grp_id
              and g.asi_id = asi.id
              and caracter = cr.id
              and sg.tipo = tsg.id
              and asi.tipo = ta.id
              and cur_id <> 7
              and te.id = 'G'
              and (   asi.tipo = 'A'
                   or g.semestre = sem.id)
         group by g.curso_aca,
                  sem.id,
                  grp_id,
                  sg.tipo,
                  tsg.nombre,
                  sg.id,
                  asi.tipo,
                  ta.nombre,
                  te.id,
                  te.nombre,
                  creditos,
                  g.asi_id,
                  asi.nombre,
                  tit_id,
                  tit.nombre,
                  cur_id,
                  caracter,
                  cr.nombre,
                  decode (sg.tipo, 'TE', 1, 'PR', 2, 'LA', 3, 'SE', 4, 'TU', 5, 6),
                  sg.limite_nuevos
         order by g.curso_aca,
                  g.asi_id,
                  sem.id,
                  sg.grp_id,
                  decode (sg.tipo, 'TE', 1, 'PR', 2, 'LA', 3, 'SE', 4, 'TU', 5, 6),
                  sg.id;

      cursor lista_sesiones (p_curso in number, p_asi in varchar2, p_grp in varchar2, p_tipo in varchar2, p_id in number) is
         select   dia_sem, ini, fin
             from pod_horarios
            where sgr_grp_curso_aca = p_curso
              and sgr_grp_asi_id = p_asi
              and sgr_grp_id = p_grp
              and sgr_tipo = p_tipo
              and sgr_id = p_id
         group by dia_sem,
                  ini,
                  fin
         order by 1,
                  2,
                  3;
   begin
      v_control := 1;
      htpx.br;

      for x in lista (p_tit) loop
         if x.comun = 0 then
            v_cuantos := 0;

            for s in lista_sesiones (p_curso - 1, x.asignatura_id, x.grupo_id, x.tipo_subgrupo_id, x.subgrupo_id) loop
               v_cuantos := v_cuantos + 1;
               htp.p ('Insertar ' || x.asignatura_id || ' ' || x.grupo_id || ' ' || x.tipo_subgrupo_id || x.subgrupo_id
                      || ' ' || s.dia_sem || ' ' || s.ini || ' ' || s.fin || ' - ' || x.comun || htfx.br);
               v_aux := uji_horarios.hibernate_Sequence.nextval;

               insert into uji_horarios.hor_items
                           (id, semestre_id, aula_planificacion_id, comun, porcentaje_comun, grupo_id,
                            tipo_subgrupo_id, tipo_subgrupo, subgrupo_id, dia_semana_id,
                            hora_inicio,
                            hora_fin, desde_el_dia,
                            hasta_el_dia, tipo_asignatura_id, tipo_asignatura, tipo_estudio_id, tipo_estudio, plazas,
                            repetir_cada_semanas, numero_iteraciones, detalle_manual, comun_texto,
                            aula_planificacion_nombre, creditos
                           )
               values      (v_aux, x.semestre_id, null, x.comun, x.comun_porcentaje, x.grupo_id,
                            x.tipo_subgrupo_id, x.tipo_subgrupo, x.subgrupo_id, s.dia_sem,
                            to_date ('1/1/2001 ' || to_char (s.ini, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss'),
                            to_date ('1/1/2001 ' || to_char (s.fin, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss'), null,
                            null, x.tipo_asignatura_id, x.tipo_asignatura, x.tipo_estudio_id, x.tipo_estudio, null,
                            null, null, 0, null,
                            null, x.creditos
                           );

               v_aux2 := uji_horarios.hibernate_Sequence.nextval;

               insert into uji_horarios.hor_items_asignaturas
                           (id, item_id, asignatura_id, asignatura, estudio_id, estudio, curso_id,
                            caracter_id, caracter
                           )
               values      (v_aux2, v_aux, x.asignatura_id, x.asignatura, x.estudio_id, x.estudio, x.curso_id,
                            x.caracter_id, x.caracter
                           );

               update uji_horarios.hor_items
               set dia_semana_id = s.dia_sem
               where  id = v_aux;

               commit;
            end loop;

            if v_cuantos = 0 then
               htp.p ('Insertar SIN planificar ' || x.asignatura_id || ' ' || x.grupo_id || ' ' || x.tipo_subgrupo_id
                      || x.subgrupo_id || ' - ' || x.comun || htfx.br);
               v_aux := uji_horarios.hibernate_Sequence.nextval;

               insert into uji_horarios.hor_items
                           (id, semestre_id, aula_planificacion_id, comun, porcentaje_comun, grupo_id,
                            tipo_subgrupo_id, tipo_subgrupo, subgrupo_id, dia_semana_id, hora_inicio, hora_fin,
                            desde_el_dia, hasta_el_dia, tipo_asignatura_id, tipo_asignatura, tipo_estudio_id,
                            tipo_estudio, plazas, repetir_cada_semanas, numero_iteraciones, detalle_manual,
                            comun_texto, aula_planificacion_nombre, creditos
                           )
               values      (v_aux, x.semestre_id, null, x.comun, x.comun_porcentaje, x.grupo_id,
                            x.tipo_subgrupo_id, x.tipo_subgrupo, x.subgrupo_id, null, null, null,
                            null, null, x.tipo_asignatura_id, x.tipo_asignatura, x.tipo_estudio_id,
                            x.tipo_estudio, null, null, null, 0,
                            null, null, x.creditos
                           );

               v_aux2 := uji_horarios.hibernate_Sequence.nextval;

               insert into uji_horarios.hor_items_asignaturas
                           (id, item_id, asignatura_id, asignatura, estudio_id, estudio, curso_id,
                            caracter_id, caracter
                           )
               values      (v_aux2, v_aux, x.asignatura_id, x.asignatura, x.estudio_id, x.estudio, x.curso_id,
                            x.caracter_id, x.caracter
                           );

               commit;
            end if;
         else
            /* comunes */
            v_cuantos := 0;

            for s in lista_sesiones (p_curso - 1, x.asignatura_id, x.grupo_id, x.tipo_subgrupo_id, x.subgrupo_id) loop
               v_cuantos := v_cuantos + 1;

               begin
                  select distinct i.id
                             into v_aux
                             from uji_horarios.hor_items i,
                                  uji_horarios.hor_items_asignaturas a
                            where i.id = a.item_id
                              and semestre_id = x.semestre_id
                              and grupo_id = x.grupo_id
                              and tipo_subgrupo_id = x.tipo_subgrupo_id
                              and subgrupo_id = x.subgrupo_id
                              and comun_texto like '%' || x.asignatura_id || '%'
                              and dia_semana_id = s.dia_sem
                              and hora_inicio =
                                         to_date ('1/1/2001 ' || to_char (s.ini, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss');

                  htp.p ('Insertar COMUN DETALLE ' || x.asignatura_id || ' ' || x.grupo_id || ' ' || x.tipo_subgrupo_id
                         || x.subgrupo_id || ' ' || s.dia_sem || ' ' || s.ini || ' ' || s.fin || ' - ' || x.comun
                         || htfx.br);
                  v_aux2 := uji_horarios.hibernate_Sequence.nextval;

                  insert into uji_horarios.hor_items_asignaturas
                              (id, item_id, asignatura_id, asignatura, estudio_id, estudio, curso_id,
                               caracter_id, caracter
                              )
                  values      (v_aux2, v_aux, x.asignatura_id, x.asignatura, x.estudio_id, x.estudio, x.curso_id,
                               x.caracter_id, x.caracter
                              );
               exception
                  when no_data_found then
                     htp.p ('Insertar COMUN  ' || x.asignatura_id || ' ' || x.grupo_id || ' ' || x.tipo_subgrupo_id
                            || x.subgrupo_id || ' ' || s.dia_sem || ' ' || s.ini || ' ' || s.fin || ' - ' || x.comun
                            || htfx.br);
                     v_aux := uji_horarios.hibernate_Sequence.nextval;

                     insert into uji_horarios.hor_items
                                 (id, semestre_id, aula_planificacion_id, comun, porcentaje_comun, grupo_id,
                                  tipo_subgrupo_id, tipo_subgrupo, subgrupo_id, dia_semana_id,
                                  hora_inicio,
                                  hora_fin,
                                  desde_el_dia, hasta_el_dia, tipo_asignatura_id, tipo_asignatura, tipo_estudio_id,
                                  tipo_estudio, plazas, repetir_cada_semanas, numero_iteraciones, detalle_manual,
                                  comun_texto, aula_planificacion_nombre, creditos
                                 )
                     values      (v_aux, x.semestre_id, null, x.comun, x.comun_porcentaje, x.grupo_id,
                                  x.tipo_subgrupo_id, x.tipo_subgrupo, x.subgrupo_id, s.dia_sem,
                                  to_date ('1/1/2001 ' || to_char (s.ini, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss'),
                                  to_date ('1/1/2001 ' || to_char (s.fin, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss'),
                                  null, null, x.tipo_asignatura_id, x.tipo_asignatura, x.tipo_estudio_id,
                                  x.tipo_estudio, x.plazas, null, null, 0,
                                  x.comun_Texto, null, x.creditos
                                 );

                     v_aux2 := uji_horarios.hibernate_Sequence.nextval;

                     insert into uji_horarios.hor_items_asignaturas
                                 (id, item_id, asignatura_id, asignatura, estudio_id, estudio, curso_id,
                                  caracter_id, caracter
                                 )
                     values      (v_aux2, v_aux, x.asignatura_id, x.asignatura, x.estudio_id, x.estudio, x.curso_id,
                                  x.caracter_id, x.caracter
                                 );

                     update uji_horarios.hor_items
                     set dia_semana_id = s.dia_sem
                     where  id = v_aux;
               end;

               commit;
            end loop;

            if v_cuantos = 0 then
               begin
                  select distinct i.id
                             into v_aux
                             from uji_horarios.hor_items i,
                                  uji_horarios.hor_items_asignaturas a
                            where i.id = a.item_id
                              and semestre_id = x.semestre_id
                              and grupo_id = x.grupo_id
                              and tipo_subgrupo_id = x.tipo_subgrupo_id
                              and subgrupo_id = x.subgrupo_id
                              and comun_texto like '%' || x.asignatura_id || '%';

                  htp.p ('Insertar COMUN DETALLE SIN planificar ' || x.asignatura_id || ' ' || x.grupo_id || ' '
                         || x.tipo_subgrupo_id || x.subgrupo_id || ' - ' || x.comun || htfx.br);
               exception
                  when no_Data_found then
                     htp.p ('Insertar COMUN SIN planificar ' || x.asignatura_id || ' ' || x.grupo_id || ' '
                            || x.tipo_subgrupo_id || x.subgrupo_id || ' - ' || x.comun || htfx.br);
                     v_aux := uji_horarios.hibernate_Sequence.nextval;

                     insert into uji_horarios.hor_items
                                 (id, semestre_id, aula_planificacion_id, comun, porcentaje_comun, grupo_id,
                                  tipo_subgrupo_id, tipo_subgrupo, subgrupo_id, dia_semana_id, hora_inicio, hora_fin,
                                  desde_el_dia, hasta_el_dia, tipo_asignatura_id, tipo_asignatura, tipo_estudio_id,
                                  tipo_estudio, plazas, repetir_cada_semanas, numero_iteraciones, detalle_manual,
                                  comun_texto, aula_planificacion_nombre, creditos
                                 )
                     values      (v_aux, x.semestre_id, null, x.comun, x.comun_porcentaje, x.grupo_id,
                                  x.tipo_subgrupo_id, x.tipo_subgrupo, x.subgrupo_id, null, null, null,
                                  null, null, x.tipo_asignatura_id, x.tipo_asignatura, x.tipo_estudio_id,
                                  x.tipo_estudio, x.plazas, null, null, 0,
                                  x.comun_texto, null, x.creditos
                                 );
               end;

               v_aux2 := uji_horarios.hibernate_Sequence.nextval;

               insert into uji_horarios.hor_items_asignaturas
                           (id, item_id, asignatura_id, asignatura, estudio_id, estudio, curso_id,
                            caracter_id, caracter
                           )
               values      (v_aux2, v_aux, x.asignatura_id, x.asignatura, x.estudio_id, x.estudio, x.curso_id,
                            x.caracter_id, x.caracter
                           );

               commit;
            end if;
         end if;
      end loop;

      p_Error := '1Dades generades correctament';
   exception
      when others then
         p_error := '0' || sqlerrm || ' (' || v_control || ')';
         rollback;
   end;

   PROCEDURE ajustar_Sesiones (p_error in out varchar2, p_curso in number, p_tit in number, p_modo varchar2 default 'X') is
      v_control                number;
      v_aux                    number;
      v_aux2                   number;
      v_aux_max                number;
      v_cuantos                number;
      v_sesiones               number;
      v_semanas                number;
      v_semanas_sin_Festivos   number;
      v_primera                date;
      v_ultima                 date;
      v_ini_semestre           date;
      v_fin_semestre           date;
      v_desde_el_dia           date;
      v_hasta_el_dia           date;
      v_repetir_cada_semanas   number;
      v_Detalle                number;
      v_numero_iteraciones     number;
      v_ini_s1                 date;
      v_ini_s2                 date;
      v_fin_s1                 date;
      v_fin_s2                 date;
      v_anterior               date;
      v_num_semanas            number;
      v_num_semanas_min        number;
      v_num_semanas_max        number;
      v_dia                    date;
      v_hora_ini               date;
      v_hora_fin               date;
      v_festivos_aux           number;
      v_tipo_aux               varchar2 (100);

      cursor lista (p_tit in number) is
         select   sem.id SEMESTRE_ID, grp_id GRUPO_ID, sg.tipo TIPO_SUBGRUPO_ID, tsg.nombre TIPO_SUBGRUPO,
                  sg.id SUBGRUPO_ID, asi.tipo TIPO_ASIGNATURA_ID, ta.nombre TIPO_ASIGNATURA, te.id TIPO_ESTUDIO_ID,
                  te.nombre TIPO_ESTUDIO, 0 DETALLE_MANUAL, creditos CREDITOS, g.asi_id asignatura_id,
                  asi.nombre asignatura, tit_id estudio_id, tit.nombre estudio, cur_id curso_id, caracter caracter_id,
                  cr.nombre caracter, decode (sg.tipo, 'TE', 1, 'PR', 2, 'LA', 3, 'SE', 4, 'TU', 5, 6) orden,
                  g.curso_aca, (select decode (count (*), 0, 0, 1)
                                  from pod_comunes com,
                                       pod_grp_comunes gc
                                 where gc.id = com.gco_id
                                   and curso_aca = g.curso_aca
                                   and asi_id = g.asi_id) comun,
                  (select porcentaje
                     from pod_comunes com,
                          pod_grp_comunes gc
                    where gc.id = com.gco_id
                      and curso_aca = g.curso_aca
                      and asi_id = g.asi_id) comun_porcentaje,
                  (select nombre
                     from pod_comunes com,
                          pod_grp_comunes gc
                    where gc.id = com.gco_id
                      and curso_aca = g.curso_aca
                      and asi_id = g.asi_id) comun_texto, sg.limite_nuevos plazas
             from pod_grupos g,
                  pod_asignaturas_titulaciones a,
                  pod_asi_cursos ac,
                  pod_subgrupos sg,
                  pod_asignaturas asi,
                  pod_titulaciones tit,
                  (select 'AV' id, 'Avaluació' nombre, 7 orden
                     from dual
                   union all
                   select 'LA' id, 'Laboratori' nombre, 4 orden
                     from dual
                   union all
                   select 'PR' id, 'Problemes' nombre, 3 orden
                     from dual
                   union all
                   select 'SE' id, 'Seminari' nombre, 5 orden
                     from dual
                   union all
                   select 'TE' id, 'Teoria' nombre, 1 orden
                     from dual
                   union all
                   select 'TP' id, 'Teoria i problemes' nombre, 2 orden
                     from dual
                   union all
                   select 'TU' id, 'Tutories' nombre, 6 orden
                     from dual) tsg,
                  (select 'S' id, 'Semestral' nombre, 1 orden
                     from dual
                   union all
                   select 'A' id, 'Anual' nombre, 2 orden
                     from dual) ta,
                  (select 'TR' id, 'Troncal' nombre, 1 orden
                     from dual
                   union all
                   select 'FB' id, 'Formació bàsica' nombre, 2 orden
                     from dual
                   union all
                   select 'OB' id, 'Obligatoria' nombre, 3 orden
                     from dual
                   union all
                   select 'OP' id, 'Optativa' nombre, 4 orden
                     from dual
                   union all
                   select 'LC' id, 'Lliure configuració' nombre, 5 orden
                     from dual
                   union all
                   select 'PR' id, 'Pràctiques externes' nombre, 6 orden
                     from dual
                   union all
                   select 'PF' id, 'Treball fi de grau' nombre, 7 orden
                     from dual) cr,
                  (select '12C' id, 'Primer i segon cicle' nombre, 2 orden
                     from dual
                   union all
                   select 'G' id, 'Grau' nombre, 1 orden
                     from dual
                   union all
                   select 'M' id, 'Màster' nombre, 3 orden
                     from dual) te,
                  (select 1 id
                     from dual
                   union
                   select 2
                     from dual) sem
            where g.curso_aca = p_curso
              --and g.asi_id = 'ED0904'
              --and g.asi_id = 'CS1001'
              --and g.asi_id like '%1021'
              --and g.asi_id like '%1002'
              --and g.asi_id in ('EI1043', 'EI1059')
              --and g.asi_id like '%1011'
              and g.asi_id = a.asi_id
              and a.tit_id = p_tit
              and a.tit_id = tit.id
              and g.asi_id = ac.asi_id
              and a.tit_id = cur_tit_id
              and g.curso_aca = ac.curso_aca
              and g.curso_aca = sg.grp_curso_aca
              and g.asi_id = sg.grp_asi_id
              and g.id = sg.grp_id
              and g.asi_id = asi.id
              and caracter = cr.id
              and sg.tipo = tsg.id
              and asi.tipo = ta.id
              and cur_id <> 7
              and te.id = 'G'
              and (   asi.tipo = 'A'
                   or g.semestre = sem.id)
         group by g.curso_aca,
                  sem.id,
                  grp_id,
                  sg.tipo,
                  tsg.nombre,
                  sg.id,
                  asi.tipo,
                  ta.nombre,
                  te.id,
                  te.nombre,
                  creditos,
                  g.asi_id,
                  asi.nombre,
                  tit_id,
                  tit.nombre,
                  cur_id,
                  caracter,
                  cr.nombre,
                  decode (sg.tipo, 'TE', 1, 'PR', 2, 'LA', 3, 'SE', 4, 'TU', 5, 6),
                  sg.limite_nuevos
         order by g.curso_aca,
                  g.asi_id,
                  sem.id,
                  sg.grp_id,
                  decode (sg.tipo, 'TE', 1, 'PR', 2, 'LA', 3, 'SE', 4, 'TU', 5, 6),
                  sg.id;

      cursor lista_sesiones (
         p_curso      in   number,
         p_asi        in   varchar2,
         p_grp        in   varchar2,
         p_tipo       in   varchar2,
         p_id         in   number,
         p_semestre   in   number
      ) is
         select   dia_sem, ini, fin
             from pod_horarios
            where sgr_grp_curso_aca = p_curso
              and sgr_grp_asi_id = p_asi
              and sgr_grp_id = p_grp
              and sgr_tipo = p_tipo
              and sgr_id = p_id
              and semestre = p_semestre
         group by dia_sem,
                  ini,
                  fin
         order by 1,
                  2,
                  3;

      cursor lista_fechas (
         p_curso      in   number,
         p_asi        in   varchar2,
         p_grp        in   varchar2,
         p_semestre   in   number,
         p_tipo       in   varchar2,
         p_subgrupo   in   number,
         p_dia_sem    in   number,
         p_ini        in   date
      ) is
         select   f.curso_aca, semestre, fecha, orden, sesiones, primera, ultima, (ultima - primera) / 7 + 1 semanas,
                  decode (semestre, 1, v_ini_s1, v_ini_s2) ini_semestre,
                  decode (semestre, 1, v_fin_s1, v_fin_s2) fin_semestre
             from (select hor_sgr_grp_curso_aca curso_aca, hor_semestre semestre, fecha,
                          row_number () over (partition by hor_sgr_grp_curso_aca, hor_sgr_grp_asi_id, hor_sgr_grp_id, hor_semestre, hor_sgr_tipo, hor_sgr_id, hor_dia_sem order by fecha)
                                                                                                                  orden,
                          count (*) over (partition by hor_sgr_grp_curso_aca, hor_sgr_grp_asi_id, hor_sgr_grp_id, hor_semestre, hor_sgr_tipo, hor_sgr_id, hor_dia_sem)
                                                                                                               sesiones,
                          min (fecha) over (partition by hor_sgr_grp_curso_aca, hor_sgr_grp_asi_id, hor_sgr_grp_id, hor_semestre, hor_sgr_tipo, hor_sgr_id, hor_dia_sem)
                                                                                                                primera,
                          max (fecha) over (partition by hor_sgr_grp_curso_aca, hor_sgr_grp_asi_id, hor_sgr_grp_id, hor_semestre, hor_sgr_tipo, hor_sgr_id, hor_dia_sem)
                                                                                                                 ultima
                     from pod_horarios_fechas
                    where hor_sgr_grp_curso_aca = p_curso
                      and hor_sgr_grp_asi_id = p_asi
                      and hor_sgr_grp_id = p_grp
                      and hor_Semestre = p_semestre
                      and hor_sgr_tipo = p_tipo
                      and hor_sgr_id = p_subgrupo
                      and hor_dia_sem = p_dia_sem
                      and hor_ini = p_ini
                      and fecha >= add_months (sysdate, -12)) f
         order by fecha;
   begin
      v_control := 1;
      htpx.br;
      htp.p (p_tit || ' ' || p_curso);
      htpx.br;

      select fecha_inicio, fecha_fin
        into v_ini_s1, v_fin_s1
        from uji_horarios.hor_semestres_detalle
       where tipo_estudio_id = 'G'
         and semestre_id = 1;

      select fecha_inicio, fecha_fin
        into v_ini_s2, v_fin_s2
        from uji_horarios.hor_semestres_detalle
       where tipo_estudio_id = 'G'
         and semestre_id = 2;

      begin
         select distinct e.fecha_inicio, e.fecha_fin
                    into v_ini_s1, v_fin_s1
                    from uji_horarios.hor_semestres_detalle d,
                         uji_horarios.hor_semestres_detalle_estudio e
                   where d.id = e.detalle_id
                     and tipo_estudio_id = 'G'
                     and semestre_id = 1
                     and estudio_id = p_tit;

         select distinct e.fecha_inicio, e.fecha_fin
                    into v_ini_s2, v_fin_s2
                    from uji_horarios.hor_semestres_detalle d,
                         uji_horarios.hor_semestres_detalle_estudio e
                   where d.id = e.detalle_id
                     and tipo_estudio_id = 'G'
                     and semestre_id = 2
                     and estudio_id = p_tit;
      exception
         when others then
            null;
      end;

      for x in lista (p_tit) loop
         v_control := 2;

         if x.comun = 0 then
            v_cuantos := 0;
            v_sesiones := null;
            v_semanas := null;
            v_primera := null;
            v_ultima := null;
            v_ini_semestre := null;
            v_fin_semestre := null;
            v_desde_el_dia := null;
            v_hasta_el_dia := null;
            v_repetir_cada_semanas := null;
            v_Detalle := null;
            v_numero_iteraciones := null;

            for s in lista_sesiones (p_curso - 1, x.asignatura_id, x.grupo_id, x.tipo_subgrupo_id, x.subgrupo_id,
                                     x.semestre_id) loop
               v_control := 3;
               v_cuantos := v_cuantos + 1;
               htp.p (x.asignatura_id || ' ' || x.grupo_id || ' semestre:' || x.semestre_id || ' '
                      || x.tipo_subgrupo_id || x.subgrupo_id || ' dia_sem:' || s.dia_sem || ' '
                      || to_char (s.ini, 'dd/mm/yyyy hh24:mi:ss') || ' ' || to_char (s.fin, 'dd/mm/yyyy hh24:mi:ss')
                      || htfx.br);
               v_control := 4;
               v_anterior := null;
               v_num_semanas := null;
               v_num_semanas_min := 999;
               v_num_semanas_max := 0;

               for f in lista_fechas (p_curso - 1, x.asignatura_id, x.grupo_id, x.semestre_id, x.tipo_subgrupo_id,
                                      x.subgrupo_id, s.dia_sem, s.ini) loop
                  v_control := 5;

                  if v_anterior is not null then
                     v_num_semanas :=
                        (f.fecha - v_anterior) / 7
                        - contar_festivos (v_anterior, f.fecha, to_number (to_char (f.fecha, 'd')));

                     if v_num_semanas < v_num_semanas_min then
                        v_num_semanas_min := v_num_semanas;
                     end if;

                     if v_num_semanas > v_num_semanas_max then
                        v_num_semanas_max := v_num_semanas;
                     end if;
                  end if;

                  v_control := 51;

                  if v_anterior is null then
                     v_control := 511;
                     htp.p (to_char (f.fecha, 'dd/mm/yyyy'));
                     htp.p (to_char (busca_fecha (f.fecha), 'dd/mm/yyyy'));
                     htp.p (busca_tipo_aca (busca_fecha (f.fecha)));
                     htp.p ('&nbsp; &nbsp; &nbsp; &nbsp; ' || to_char (f.fecha, 'dd/mm/yyyy') || ' - ('
                            || to_char (busca_fecha (f.fecha), 'dd/mm/yyyy') || ' '
                            || busca_tipo_aca (busca_fecha (f.fecha)) || ') - ' || f.orden || ' ' || f.sesiones || ' '
                            || f.semanas || ' ' || to_char (f.primera, 'dd/mm/yyyy') || ' '
                            || to_char (f.ultima, 'dd/mm/yyyy') || ' ' || to_char (f.ini_semestre, 'dd/mm/yyyy') || ' '
                            || to_char (f.fin_semestre, 'dd/mm/yyyy') || htfx.br);
                  else
                     v_control := 512;
                     htp.p ('&nbsp; &nbsp; &nbsp; &nbsp; ' || to_char (f.fecha, 'dd/mm/yyyy') || ' - ('
                            || to_char (busca_fecha (f.fecha), 'dd/mm/yyyy') || ' '
                            || busca_tipo_aca (busca_Fecha (f.fecha)) || ') - ' || f.orden || ' ' || f.sesiones || ' '
                            || f.semanas || ' ' || to_char (f.primera, 'dd/mm/yyyy') || ' '
                            || to_char (f.ultima, 'dd/mm/yyyy') || ' ' || to_char (f.ini_semestre, 'dd/mm/yyyy') || ' '
                            || to_char (f.fin_semestre, 'dd/mm/yyyy') || ' - ' || v_num_semanas || htfx.br);
                  end if;

                  v_control := 52;
                  v_sesiones := f.sesiones;
                  v_semanas := f.semanas;
                  v_primera := f.primera;
                  v_ultima := f.ultima;
                  v_ini_semestre := f.ini_semestre;
                  v_fin_semestre := f.fin_semestre;
                  v_anterior := f.fecha;
               end loop;

               v_control := 6;
               v_repetir_cada_semanas := null;
               v_semanas_sin_Festivos :=
                                 v_Semanas - contar_Festivos (v_primera, v_ultima, to_number (to_char (v_primera, 'd')));
               htp.p (v_sesiones || ' sesiones - ' || v_semanas || ' semanas - ' || v_semanas_sin_festivos
                      || ' semanas sin festivos -  (' || v_num_semanas_min || '-' || v_num_semanas_max || ')');

               if    v_sesiones = v_semanas
                  or abs (v_sesiones - v_semanas) = 1
                  or v_sesiones = v_semanas_sin_Festivos then
                  v_repetir_cada_Semanas := 1;
                  v_numero_iteraciones := v_sesiones;
                  v_detalle := 0;
               elsif     (   (v_sesiones * 2 = v_semanas + 1)
                          or (v_sesiones * 2 = v_semanas_sin_festivos)
                          or (v_sesiones * 2 = v_semanas)
                          or (v_sesiones * 2 = v_semanas_sin_festivos + 1)
                         )
                     and v_num_semanas_max = v_num_semanas_min then
                  v_repetir_cada_Semanas := 2;
                  v_numero_iteraciones := v_sesiones;
                  v_detalle := 0;
               elsif v_sesiones * 3 = v_semanas then
                  v_repetir_cada_Semanas := 3;
                  v_numero_iteraciones := v_sesiones;
                  v_detalle := 0;
               elsif v_sesiones * 4 = v_semanas then
                  v_repetir_cada_Semanas := 4;
                  v_numero_iteraciones := v_sesiones;
                  v_detalle := 0;
               elsif     v_sesiones <= round (v_semanas / 2, 0)
                     and v_num_semanas_max = v_num_semanas_min
                     and v_num_semanas_max = 2 then
                  v_repetir_cada_Semanas := 2;
                  v_numero_iteraciones := v_sesiones;
                  v_detalle := 0;
               else
                  v_repetir_cada_Semanas := 1;
                  v_detalle := 1;
               end if;

               v_desde_el_dia := busca_fecha (v_primera);

               if     busca_fecha (v_primera) > v_ini_semestre
                  and principio_semestre (busca_fecha (v_primera), p_curso, x.semestre_id) = 'S' then
                  v_desde_el_dia := null;
               end if;

               if     v_ultima < v_fin_semestre
                  and fin_semestre (busca_fecha (v_ultima), p_curso, x.semestre_id, p_tit) = 'N' then
                  v_hasta_el_dia := busca_fecha (v_ultima);
               end if;

               if v_numero_iteraciones is not null then
                  v_hasta_el_dia := null;
               end if;

               if v_detalle = 1 then
                  v_desde_el_dia := null;
                  v_hasta_el_dia := null;
               end if;

               htpx.br;

               if v_detalle = 0 then
                  htp.p ('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' || v_repetir_cada_semanas || ' - '
                         || v_numero_iteraciones || ' - ' || v_detalle || ' ini: '
                         || to_char (v_desde_el_dia, 'dd/mm/yyyy')
                         || '     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' || 'fin: '
                         || to_char (v_hasta_el_dia, 'dd/mm/yyyy') || htfx.br);
               else
                  htp.p ('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' || v_repetir_cada_semanas || ' - '
                         || v_numero_iteraciones || ' - ' || v_detalle || ' DETALL MANUAL - ini: '
                         || to_char (v_desde_el_dia, 'dd/mm/yyyy')
                         || '     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' || 'fin: '
                         || to_char (v_hasta_el_dia, 'dd/mm/yyyy') || htfx.br);
               end if;

               htpx.br;

               if p_modo = 'INSERT' then
                  v_aux := uji_horarios.hibernate_Sequence.nextval;

                  insert into uji_horarios.hor_items
                              (id, semestre_id, aula_planificacion_id, comun, porcentaje_comun, grupo_id,
                               tipo_subgrupo_id, tipo_subgrupo, subgrupo_id, dia_semana_id,
                               hora_inicio,
                               hora_fin,
                               desde_el_dia, hasta_el_dia, tipo_asignatura_id, tipo_asignatura,
                               tipo_estudio_id, tipo_estudio, plazas, repetir_cada_semanas,
                               numero_iteraciones, detalle_manual, comun_texto, aula_planificacion_nombre, creditos
                              )
                  values      (v_aux, x.semestre_id, null, x.comun, x.comun_porcentaje, x.grupo_id,
                               x.tipo_subgrupo_id, x.tipo_subgrupo, x.subgrupo_id, s.dia_sem,
                               to_date ('1/1/2001 ' || to_char (s.ini, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss'),
                               to_date ('1/1/2001 ' || to_char (s.fin, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss'),
                               v_Desde_el_dia, v_hasta_el_dia, x.tipo_asignatura_id, x.tipo_asignatura,
                               x.tipo_estudio_id, x.tipo_estudio, x.plazas, v_repetir_cada_semanas,
                               v_numero_iteraciones, v_detalle, x.comun_texto, null, x.creditos
                              );

                  v_aux2 := uji_horarios.hibernate_Sequence.nextval;

                  insert into uji_horarios.hor_items_asignaturas
                              (id, item_id, asignatura_id, asignatura, estudio_id, estudio, curso_id,
                               caracter_id, caracter
                              )
                  values      (v_aux2, v_aux, x.asignatura_id, x.asignatura, x.estudio_id, x.estudio, x.curso_id,
                               x.caracter_id, x.caracter
                              );

                  if v_detalle = 0 then
                     update uji_horarios.hor_items
                     set dia_semana_id = s.dia_sem
                     where  id = v_aux;
                  else
                     v_festivos_aux := 0;

                     for f in lista_fechas (p_curso - 1, x.asignatura_id, x.grupo_id, x.semestre_id,
                                            x.tipo_subgrupo_id, x.subgrupo_id, s.dia_sem, s.ini) loop
                        v_control := 500;
                        v_dia := busca_Fecha (f.fecha) + v_festivos_aux * 7;

                        select tipo_aca
                          into v_tipo_aux
                          from grh_calendario
                         where fecha_completa = v_dia;

                        if v_tipo_aux <> 'L' then
                           while v_tipo_aux <> 'L' loop
                              v_festivos_aux := v_festivos_aux + 1;
                              v_dia := v_dia + v_festivos_aux + 7;

                              select tipo_aca
                                into v_tipo_aux
                                from grh_calendario
                               where fecha_completa = v_dia;
                           end loop;
                        end if;

                        v_hora_ini :=
                           to_date (to_char (v_dia, 'dd/mm/yyyy') || ' ' || to_char (s.ini, 'hh24:mi:ss'),
                                    'dd/mm/yyyy hh24:mi:ss');
                        v_hora_fin :=
                           to_date (to_char (v_dia, 'dd/mm/yyyy') || ' ' || to_char (s.fin, 'hh24:mi:ss'),
                                    'dd/mm/yyyy hh24:mi:ss');
                        v_aux2 := uji_horarios.hibernate_Sequence.nextval;

                        insert into uji_horarios.hor_items_detalle
                                    (id, item_id, inicio, fin, descripcion
                                    )
                        values      (v_aux2, v_aux, v_hora_ini, v_hora_fin, null
                                    );
                     end loop;
                  end if;
               end if;

               commit;
            end loop;

            if v_cuantos = 0 then
               htp.p ('SIN planificar ' || x.asignatura_id || ' ' || x.grupo_id || ' ' || x.tipo_subgrupo_id
                      || x.subgrupo_id || htfx.br);

               if p_modo = 'INSERT' then
                  v_aux := uji_horarios.hibernate_Sequence.nextval;

                  insert into uji_horarios.hor_items
                              (id, semestre_id, aula_planificacion_id, comun, porcentaje_comun, grupo_id,
                               tipo_subgrupo_id, tipo_subgrupo, subgrupo_id, dia_semana_id, hora_inicio, hora_fin,
                               desde_el_dia, hasta_el_dia, tipo_asignatura_id, tipo_asignatura, tipo_estudio_id,
                               tipo_estudio, plazas, repetir_cada_semanas, numero_iteraciones, detalle_manual,
                               comun_texto, aula_planificacion_nombre, creditos
                              )
                  values      (v_aux, x.semestre_id, null, x.comun, x.comun_porcentaje, x.grupo_id,
                               x.tipo_subgrupo_id, x.tipo_subgrupo, x.subgrupo_id, null, null, null,
                               null, null, x.tipo_asignatura_id, x.tipo_asignatura, x.tipo_estudio_id,
                               x.tipo_estudio, x.plazas, null, null, 0,
                               x.comun_texto, null, x.creditos
                              );

                  v_aux2 := uji_horarios.hibernate_Sequence.nextval;

                  insert into uji_horarios.hor_items_asignaturas
                              (id, item_id, asignatura_id, asignatura, estudio_id, estudio, curso_id,
                               caracter_id, caracter
                              )
                  values      (v_aux2, v_aux, x.asignatura_id, x.asignatura, x.estudio_id, x.estudio, x.curso_id,
                               x.caracter_id, x.caracter
                              );

                  commit;
               end if;
            end if;
         else
            /* comunes */
            v_cuantos := 0;
            v_sesiones := null;
            v_semanas := null;
            v_primera := null;
            v_ultima := null;
            v_ini_semestre := null;
            v_fin_semestre := null;
            v_desde_el_dia := null;
            v_hasta_el_dia := null;
            v_repetir_cada_semanas := null;
            v_Detalle := null;
            v_numero_iteraciones := null;

            for s in lista_sesiones (p_curso - 1, x.asignatura_id, x.grupo_id, x.tipo_subgrupo_id, x.subgrupo_id,
                                     x.semestre_id) loop
               v_control := 3;
               v_cuantos := v_cuantos + 1;
               htp.p (x.asignatura_id || ' COMUN ' || x.grupo_id || ' semestre:' || x.semestre_id || ' '
                      || x.tipo_subgrupo_id || x.subgrupo_id || ' dia_sem:' || s.dia_sem || ' '
                      || to_char (s.ini, 'dd/mm/yyyy hh24:mi:ss') || ' ' || to_char (s.fin, 'dd/mm/yyyy hh24:mi:ss')
                      || htfx.br);
               v_control := 4;
               v_anterior := null;
               v_num_semanas := null;
               v_num_semanas_min := 999;
               v_num_semanas_max := 0;

               for f in lista_fechas (p_curso - 1, x.asignatura_id, x.grupo_id, x.semestre_id, x.tipo_subgrupo_id,
                                      x.subgrupo_id, s.dia_sem, s.ini) loop
                  v_control := 5;

                  if v_anterior is not null then
                     v_num_semanas :=
                        (f.fecha - v_anterior) / 7
                        - contar_festivos (v_anterior, f.fecha, to_number (to_char (f.fecha, 'd')));

                     if v_num_semanas < v_num_semanas_min then
                        v_num_semanas_min := v_num_semanas;
                     end if;

                     if v_num_semanas > v_num_semanas_max then
                        v_num_semanas_max := v_num_semanas;
                     end if;
                  end if;

                  if v_anterior is null then
                     htp.p ('&nbsp; &nbsp; &nbsp; &nbsp; ' || to_char (f.fecha, 'dd/mm/yyyy') || ' - ('
                            || to_char (busca_fecha (f.fecha), 'dd/mm/yyyy') || ' '
                            || busca_tipo_aca (busca_fecha (f.fecha)) || ') - ' || f.orden || ' ' || f.sesiones || ' '
                            || f.semanas || ' ' || to_char (f.primera, 'dd/mm/yyyy') || ' '
                            || to_char (f.ultima, 'dd/mm/yyyy') || ' ' || to_char (f.ini_semestre, 'dd/mm/yyyy') || ' '
                            || to_char (f.fin_semestre, 'dd/mm/yyyy') || htfx.br);
                  else
                     htp.p ('&nbsp; &nbsp; &nbsp; &nbsp; ' || to_char (f.fecha, 'dd/mm/yyyy') || ' - ('
                            || to_char (busca_fecha (f.fecha), 'dd/mm/yyyy') || ' '
                            || busca_tipo_aca (busca_Fecha (f.fecha)) || ') - ' || f.orden || ' ' || f.sesiones || ' '
                            || f.semanas || ' ' || to_char (f.primera, 'dd/mm/yyyy') || ' '
                            || to_char (f.ultima, 'dd/mm/yyyy') || ' ' || to_char (f.ini_semestre, 'dd/mm/yyyy') || ' '
                            || to_char (f.fin_semestre, 'dd/mm/yyyy') || ' - ' || v_num_semanas || htfx.br);
                  end if;

                  v_sesiones := f.sesiones;
                  v_semanas := f.semanas;
                  v_primera := f.primera;
                  v_ultima := f.ultima;
                  v_ini_semestre := f.ini_semestre;
                  v_fin_semestre := f.fin_semestre;
                  v_anterior := f.fecha;
               end loop;

               v_control := 6;
               v_repetir_cada_semanas := null;
               v_semanas_sin_Festivos :=
                                 v_Semanas - contar_Festivos (v_primera, v_ultima, to_number (to_char (v_primera, 'd')));
               htp.p (v_sesiones || ' sesiones - ' || v_semanas || ' semanas - ' || v_semanas_sin_festivos
                      || ' semanas sin festivos -  (' || v_num_semanas_min || '-' || v_num_semanas_max || ')');

               if    v_sesiones = v_semanas
                  or abs (v_sesiones - v_semanas) = 1
                  or v_sesiones = v_semanas_sin_Festivos then
                  v_repetir_cada_Semanas := 1;
                  v_numero_iteraciones := v_sesiones;
                  v_detalle := 0;
               elsif     (   (v_sesiones * 2 = v_semanas + 1)
                          or (v_sesiones * 2 = v_semanas_sin_festivos)
                          or (v_sesiones * 2 = v_semanas)
                          or (v_sesiones * 2 = v_semanas_sin_festivos + 1)
                         )
                     and v_num_semanas_max = v_num_semanas_min then
                  v_repetir_cada_Semanas := 2;
                  v_numero_iteraciones := v_sesiones;
                  v_detalle := 0;
               elsif v_sesiones * 3 = v_semanas then
                  v_repetir_cada_Semanas := 3;
                  v_numero_iteraciones := v_sesiones;
                  v_detalle := 0;
               elsif v_sesiones * 4 = v_semanas then
                  v_repetir_cada_Semanas := 4;
                  v_numero_iteraciones := v_sesiones;
                  v_detalle := 0;
               elsif     v_sesiones <= round (v_semanas / 2, 0)
                     and v_num_semanas_max = v_num_semanas_min
                     and v_num_semanas_max = 2 then
                  v_repetir_cada_Semanas := 2;
                  v_numero_iteraciones := v_sesiones;
                  v_detalle := 0;
               else
                  v_repetir_cada_Semanas := 1;
                  v_detalle := 1;
               end if;

               v_desde_el_dia := busca_fecha (v_primera);

               if     busca_fecha (v_primera) > v_ini_semestre
                  and principio_semestre (busca_fecha (v_primera), p_curso, x.semestre_id) = 'S' then
                  v_desde_el_dia := null;
               end if;

               if     v_ultima < v_fin_semestre
                  and fin_semestre (busca_fecha (v_ultima), p_curso, x.semestre_id, p_tit) = 'N' then
                  v_hasta_el_dia := busca_fecha (v_ultima);
               end if;

               if v_numero_iteraciones is not null then
                  v_hasta_el_dia := null;
               end if;

               if v_detalle = 1 then
                  v_desde_el_dia := null;
                  v_hasta_el_dia := null;
               end if;

               htpx.br;

               if v_detalle = 0 then
                  htp.p ('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' || v_repetir_cada_semanas || ' - '
                         || v_numero_iteraciones || ' - ' || v_detalle || ' ini: '
                         || to_char (v_desde_el_dia, 'dd/mm/yyyy')
                         || '     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' || 'fin: '
                         || to_char (v_hasta_el_dia, 'dd/mm/yyyy') || htfx.br);
               else
                  htp.p ('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' || v_repetir_cada_semanas || ' - '
                         || v_numero_iteraciones || ' - ' || v_detalle || ' DETALL MANUAL - ini: '
                         || to_char (v_desde_el_dia, 'dd/mm/yyyy')
                         || '     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' || 'fin: '
                         || to_char (v_hasta_el_dia, 'dd/mm/yyyy') || htfx.br);
               end if;

               htpx.br;

               if p_modo = 'INSERT' then
                  begin
                     select distinct i.id
                                into v_aux
                                from uji_horarios.hor_items i,
                                     uji_horarios.hor_items_asignaturas a
                               where i.id = a.item_id
                                 and semestre_id = x.semestre_id
                                 and grupo_id = x.grupo_id
                                 and tipo_subgrupo_id = x.tipo_subgrupo_id
                                 and subgrupo_id = x.subgrupo_id
                                 and comun_texto like '%' || x.asignatura_id || '%'
                                 and dia_semana_id = s.dia_sem
                                 and hora_inicio =
                                         to_date ('1/1/2001 ' || to_char (s.ini, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss');

                     htp.p (' insertar comunes detalle' || htfx.br);
                  exception
                     when no_data_found then
                        htp.p (' insertar comunes ' || htfx.br);
                        v_aux := uji_horarios.hibernate_Sequence.nextval;

                        insert into uji_horarios.hor_items
                                    (id, semestre_id, aula_planificacion_id, comun, porcentaje_comun, grupo_id,
                                     tipo_subgrupo_id, tipo_subgrupo, subgrupo_id, dia_semana_id,
                                     hora_inicio,
                                     hora_fin,
                                     desde_el_dia, hasta_el_dia, tipo_asignatura_id, tipo_asignatura,
                                     tipo_estudio_id, tipo_estudio, plazas, repetir_cada_semanas,
                                     numero_iteraciones, detalle_manual, comun_texto, aula_planificacion_nombre,
                                     creditos
                                    )
                        values      (v_aux, x.semestre_id, null, x.comun, x.comun_porcentaje, x.grupo_id,
                                     x.tipo_subgrupo_id, x.tipo_subgrupo, x.subgrupo_id, s.dia_sem,
                                     to_date ('1/1/2001 ' || to_char (s.ini, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss'),
                                     to_date ('1/1/2001 ' || to_char (s.fin, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss'),
                                     v_Desde_el_dia, v_hasta_el_dia, x.tipo_asignatura_id, x.tipo_asignatura,
                                     x.tipo_estudio_id, x.tipo_estudio, x.plazas, v_repetir_cada_semanas,
                                     v_numero_iteraciones, v_detalle, x.comun_texto, null,
                                     x.creditos
                                    );
                  end;

                  v_aux2 := uji_horarios.hibernate_Sequence.nextval;

                  insert into uji_horarios.hor_items_asignaturas
                              (id, item_id, asignatura_id, asignatura, estudio_id, estudio, curso_id,
                               caracter_id, caracter
                              )
                  values      (v_aux2, v_aux, x.asignatura_id, x.asignatura, x.estudio_id, x.estudio, x.curso_id,
                               x.caracter_id, x.caracter
                              );

                  if v_detalle = 0 then
                     update uji_horarios.hor_items
                     set dia_semana_id = s.dia_sem
                     where  id = v_aux;
                  else
                     v_festivos_aux := 0;

                     for f in lista_fechas (p_curso - 1, x.asignatura_id, x.grupo_id, x.semestre_id,
                                            x.tipo_subgrupo_id, x.subgrupo_id, s.dia_sem, s.ini) loop
                        v_control := 500;
                        v_dia := busca_Fecha (f.fecha) + v_festivos_aux * 7;

                        select tipo_aca
                          into v_tipo_aux
                          from grh_calendario
                         where fecha_completa = v_dia;

                        if v_tipo_aux <> 'L' then
                           while v_tipo_aux <> 'L' loop
                              v_festivos_aux := v_festivos_aux + 1;
                              v_dia := v_dia + v_festivos_aux + 7;

                              select tipo_aca
                                into v_tipo_aux
                                from grh_calendario
                               where fecha_completa = v_dia;
                           end loop;
                        end if;

                        v_hora_ini :=
                           to_date (to_char (v_dia, 'dd/mm/yyyy') || ' ' || to_char (s.ini, 'hh24:mi:ss'),
                                    'dd/mm/yyyy hh24:mi:ss');
                        v_hora_fin :=
                           to_date (to_char (v_dia, 'dd/mm/yyyy') || ' ' || to_char (s.fin, 'hh24:mi:ss'),
                                    'dd/mm/yyyy hh24:mi:ss');
                        v_aux2 := uji_horarios.hibernate_Sequence.nextval;

                        insert into uji_horarios.hor_items_detalle
                                    (id, item_id, inicio, fin, descripcion
                                    )
                        values      (v_aux2, v_aux, v_hora_ini, v_hora_fin, null
                                    );
                     end loop;
                  end if;
               end if;

               commit;
            end loop;

            v_control := 1000;

            if v_cuantos = 0 then
               if p_modo = 'INSERT' then
                  begin
                     v_aux_max := null;
                     v_control := 1001;

                     --htp.p ('1001 ' || x.asignatura_id || ' ' || x.grupo_id || ' ' || x.tipo_subgrupo_id
                       --     || x.subgrupo_id || htfx.br);
                     select distinct i.id
                                into v_aux
                                from uji_horarios.hor_items i,
                                     uji_horarios.hor_items_asignaturas a
                               where i.id = a.item_id
                                 and semestre_id = x.semestre_id
                                 and grupo_id = x.grupo_id
                                 and tipo_subgrupo_id = x.tipo_subgrupo_id
                                 and subgrupo_id = x.subgrupo_id
                                 and comun_texto like '%' || x.asignatura_id || '%';

                     v_control := 1002;
                     htp.p ('SIN planificar COMUN DETALLE  ' || x.asignatura_id || ' ' || x.grupo_id || ' '
                            || x.tipo_subgrupo_id || x.subgrupo_id || htfx.br);
                  exception
                     when too_many_rows then
                        select min (i.id)
                          into v_aux
                          from uji_horarios.hor_items i,
                               uji_horarios.hor_items_asignaturas a
                         where i.id = a.item_id
                           and semestre_id = x.semestre_id
                           and grupo_id = x.grupo_id
                           and tipo_subgrupo_id = x.tipo_subgrupo_id
                           and subgrupo_id = x.subgrupo_id
                           and comun_texto like '%' || x.asignatura_id || '%';

                        select max (i.id)
                          into v_aux_max
                          from uji_horarios.hor_items i,
                               uji_horarios.hor_items_asignaturas a
                         where i.id = a.item_id
                           and semestre_id = x.semestre_id
                           and grupo_id = x.grupo_id
                           and tipo_subgrupo_id = x.tipo_subgrupo_id
                           and subgrupo_id = x.subgrupo_id
                           and comun_texto like '%' || x.asignatura_id || '%';
                     when no_data_found then
                        v_control := 1003;
                        v_aux := uji_horarios.hibernate_Sequence.nextval;

                        insert into uji_horarios.hor_items
                                    (id, semestre_id, aula_planificacion_id, comun, porcentaje_comun, grupo_id,
                                     tipo_subgrupo_id, tipo_subgrupo, subgrupo_id, dia_semana_id, hora_inicio,
                                     hora_fin, desde_el_dia, hasta_el_dia, tipo_asignatura_id, tipo_asignatura,
                                     tipo_estudio_id, tipo_estudio, plazas, repetir_cada_semanas, numero_iteraciones,
                                     detalle_manual, comun_texto, aula_planificacion_nombre, creditos
                                    )
                        values      (v_aux, x.semestre_id, null, x.comun, x.comun_porcentaje, x.grupo_id,
                                     x.tipo_subgrupo_id, x.tipo_subgrupo, x.subgrupo_id, null, null,
                                     null, null, null, x.tipo_asignatura_id, x.tipo_asignatura,
                                     x.tipo_estudio_id, x.tipo_estudio, x.plazas, null, null,
                                     0, x.comun_texto, null, x.creditos
                                    );

                        v_control := 1004;
                        htp.p ('SIN planificar COMUN  ' || x.asignatura_id || ' ' || x.grupo_id || ' '
                               || x.tipo_subgrupo_id || x.subgrupo_id || htfx.br);
                  end;

                  v_control := 1005;
                  v_aux2 := uji_horarios.hibernate_Sequence.nextval;

                  insert into uji_horarios.hor_items_asignaturas
                              (id, item_id, asignatura_id, asignatura, estudio_id, estudio, curso_id,
                               caracter_id, caracter
                              )
                  values      (v_aux2, v_aux, x.asignatura_id, x.asignatura, x.estudio_id, x.estudio, x.curso_id,
                               x.caracter_id, x.caracter
                              );

                  if v_aux_max is not null then
                     v_aux2 := uji_horarios.hibernate_Sequence.nextval;

                     insert into uji_horarios.hor_items_asignaturas
                                 (id, item_id, asignatura_id, asignatura, estudio_id, estudio,
                                  curso_id, caracter_id, caracter
                                 )
                     values      (v_aux2, v_aux_max, x.asignatura_id, x.asignatura, x.estudio_id, x.estudio,
                                  x.curso_id, x.caracter_id, x.caracter
                                 );
                  end if;

                  commit;
               end if;
            end if;
         end if;
      end loop;

      p_Error := '1Dades generades correctament';
   exception
      when others then
         p_error := '0' || sqlerrm || ' (' || v_control || ')';
         rollback;
   end;

   PROCEDURE borrar_items (p_error in out varchar2, p_tit in number) is
      v_control   number;

      cursor lista (p_tit in number) is
         select i.id
           from uji_horarios.hor_items i,
                uji_horarios.hor_items_asignaturas a
          where i.id = a.item_id
            and estudio_id = p_tit;
   begin
      v_control := 1;

      for x in lista (p_tit) loop
         delete      uji_horarios.hor_items_asignaturas
         where       item_id = x.id
                 and estudio_id = p_tit;

         delete      uji_horarios.hor_items
         where       id = x.id
                 and not exists (select 1
                                   from uji_horarios.hor_items_asignaturas
                                  where item_id = x.id);

         commit;
      end loop;

      p_Error := '1Dades esborrades correctament';
   exception
      when others then
         p_error := '0' || sqlerrm || ' (' || v_control || ')';
         rollback;
   end;

   PROCEDURE generar_examenes (p_error in out varchar2, p_curso in number, p_tit in number) is
      v_control   number;
      v_aux       number;
      v_id        number;
      v_id2       number;

      cursor lista (p_curso in number, p_tit in number) is
         select distinct c.curso_aca, cur_tit_id estudio_id, t.nombre estudio, c.cur_id curso_id, c.asi_id asi_id,
                         a.nombre asi_nombre, decode (caracter, 'LC', 'EP', caracter) caracter,
                         decode (decode (a.eep, 'S', 'S', a.tipo),
                                 'S', decode (a.tipo, 'A', 'A', decode (g.semestre, 1, '1', '2')),
                                 'A'
                                ) semestre,
                         g.semestre sem_grupo, decode (com.asi_id, null, null, 'S') comun, com.nombre comun_nombre,
                         nvl (gco_id, 0) comun_id
                    from pod_asi_cursos c,
                         pod_grupos g,
                         pod_asignaturas a,
                         pod_titulaciones t,
                         (select curso_aca, c2.asi_id, nombre, c2.gco_id
                            from pod_grp_comunes g,
                                 pod_comunes c,
                                 pod_comunes c2
                           where g.id = c.gco_id
                             and c.gco_id = c2.gco_id) com
                   where c.asi_id = g.asi_id
                     and g.asi_id = a.id
                     and g.curso_aca = p_curso
                     and g.curso_aca = c.curso_aca
                     and cur_tit_id = t.id
                     and t.id = p_tit
                     and c.cur_id < 6
                     and c.visible = 'S'
                     --and a.id like '%02'
                     and cur_tit_id not in (100, 104)
                     and length (g.asi_id) >= 6
                     and g.curso_aca = com.curso_aca(+)
                     and g.asi_id = com.asi_id(+)
                     --and             cur_tit_id = 228
                     and caracter not in ('LC', 'PF')
                --and a.tipo ='A'
         order by        5,
                         11,
                         1,
                         2,
                         3,
                         4,
                         5;

      cursor convocatorias (p_sem in varchar2, p_sem_grupo in number) is
         select   id, nombre, '' parcial
             from pod_convocatorias
            where tipo = 'G'
              and extra = 'N'
              and (   (    p_Sem_grupo = 1
                       and id in (9, 11))
                   or (    p_Sem_grupo = 2
                       and id in (10, 11)))
         union all
         select   id, nombre, ' - Parcial' parcial
             from pod_convocatorias
            where p_sem = 'A'
              and id = 9
         order by 1;
   begin
      v_control := 1;
      v_aux := 0;
      htpx.br;

      for x in lista (p_curso, p_tit) loop
         v_aux := v_aux + 1;

         for c in convocatorias (x.semestre, x.sem_grupo) loop
            if x.comun is null then
               v_id := uji_horarios.hibernate_sequence.nextval;
               htp.p ('Insertar ' || x.asi_id || ' ' || c.nombre || htfx.br);

               insert into uji_horarios.hor_examenes
                           (id, nombre, convocatoria_id, convocatoria_nombre, comun, tipo_examen_id
                           )
               values      (v_id, x.asi_id || c.parcial, c.id, c.nombre, 0, 1
                           );

               insert into uji_horarios.hor_examenes_asignaturas
                           (ID, EXAMEN_ID, ASIGNATURA_ID, ASIGNATURA_NOMBRE, ESTUDIO_ID,
                            ESTUDIO_NOMBRE, SEMESTRE, TIPO_ASIGNATURA, CENTRO_ID
                           )
               values      (uji_horarios.hibernate_sequence.nextval, v_id, x.asi_id, x.asi_nombre, x.estudio_id,
                            x.estudio, x.sem_grupo, decode (x.semestre, 'A', 'Anual', 'Semestral'), null
                           );

               dbms_output.put_line (v_aux || ' ' || x.asi_id || ' ' || x.comun_nombre || ' ' || x.semestre || ' '
                                     || c.nombre);
            else
               begin
                  select e.id
                    into v_id
                    from uji_horarios.hor_examenes e,
                         uji_horarios.hor_examenes_asignaturas ea
                   where e.id = ea.examen_id
                     and x.comun_nombre like '%' || asignatura_id || '%'
                     and convocatoria_id = c.id;

                  htp.p ('Insertar comun detalle ' || x.asi_id || ' ' || c.nombre || htfx.br);
               exception
                  when too_many_rows then
                     dbms_output.put_line ('too many ' || v_aux || ' ' || x.asi_id || ' ' || x.comun_nombre || ' '
                                           || x.semestre || ' ' || c.nombre);
                  when no_data_found then
                     htp.p ('Insertar comun ' || x.asi_id || ' ' || c.nombre || htfx.br);
                     v_id := uji_horarios.hibernate_sequence.nextval;

                     insert into uji_horarios.hor_examenes
                                 (id, nombre, convocatoria_id, convocatoria_nombre, comun, comun_Texto,
                                  tipo_examen_id
                                 )
                     values      (v_id, x.asi_id || ' - ' || c.nombre || c.parcial, c.id, c.nombre, 1, x.comun_nombre,
                                  1
                                 );
               end;

               insert into uji_horarios.hor_examenes_asignaturas
                           (ID, EXAMEN_ID, ASIGNATURA_ID, ASIGNATURA_NOMBRE, ESTUDIO_ID,
                            ESTUDIO_NOMBRE, SEMESTRE, TIPO_ASIGNATURA, CENTRO_ID
                           )
               values      (uji_horarios.hibernate_sequence.nextval, v_id, x.asi_id, x.asi_nombre, x.estudio_id,
                            x.estudio, x.sem_grupo, decode (x.semestre, 'A', 'Anual', 'Semestral'), null
                           );

               dbms_output.put_line ('buscar si existe');
               dbms_output.put_line (v_aux || ' ' || x.asi_id || ' ' || x.comun_nombre || ' ' || x.semestre || ' '
                                     || c.nombre);
            end if;

            commit;
         end loop;
      end loop;

      p_Error := '1Dades generades correctament';
   exception
      when others then
         p_error := '0' || sqlerrm || ' (' || v_control || ')';
         rollback;
   end;

   PROCEDURE borrar_examenes (p_error in out varchar2, p_tit in number) is
      v_control   number;

      cursor lista (p_tit in number) is
         select e.*, asignatura_id, estudio_id
           from uji_horarios.hor_examenes e,
                uji_horarios.hor_examenes_asignaturas a
          where e.id = a.examen_id
            and estudio_id = p_tit;
   begin
      v_control := 1;
      htpx.br;

      for x in lista (p_tit) loop
         htp.p (x.asignatura_id || ' ' || x.convocatoria_nombre || htfx.br);

         delete      uji_horarios.hor_Examenes_asignaturas
         where       examen_id = x.id
                 and estudio_id = p_tit;

         delete      uji_horarios.hor_Examenes
         where       id = x.id
                 and id not in (select examen_id
                                  from uji_horarios.hor_Examenes_asignaturas);
      end loop;

      p_Error := '1Dades esborrades correctament';
   exception
      when others then
         p_error := '0' || sqlerrm || ' (' || v_control || ')';
         rollback;
   end;
END pack_horarios;


