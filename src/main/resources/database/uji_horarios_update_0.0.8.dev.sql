delete UJI_HORARIOS.HOR_EXT_CARGOS_PER;

Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (334, 65259, 'Amelia Sim� Vidal', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 223, 'Grau en Matem�tica Computacional', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (275, 104164, 'Ana Hermenegilda Alarcon Aguilar', 2922, 'Facultat de Ci�ncies de la Salut', 219, 'Grau en Psicologia', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (51, 7544, 'Andreu Casero Ripoll�s', 2, 'Facultat de Ci�ncies Humanes i Socials', 204, 'Grau en Periodisme', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (126, 176839, 'Angel Miguel Pitarch Roig', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 209, 'Grau en Arquitectura T�cnica', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (452, 61360, 'Beatriz Susana Tom�s Mall�n', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 232, 'Grau en Gesti� i Administraci� P�blica (pendent d''autoritzaci� d''implantaci�)', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (216, 65106, 'Carles Alfred Rabassa Vaquer', 2, 'Facultat de Ci�ncies Humanes i Socials', 215, 'Grau en Hist�ria i Patrimoni', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (201, 56094, 'Cristina Guisasola Lerma', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 214, 'Grau en Criminologia i Seguretat', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (196, 62481, 'Cristina Pauner Chulvi', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 214, 'Grau en Criminologia i Seguretat', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (447, 62481, 'Cristina Pauner Chulvi', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 232, 'Grau en Gesti� i Administraci� P�blica (pendent d''autoritzaci� d''implantaci�)', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (166, 62481, 'Cristina Pauner Chulvi', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 212, 'Grau en Finances i Comptabilitat', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (16, 62481, 'Cristina Pauner Chulvi', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 202, 'Grau en Turisme', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (136, 62481, 'Cristina Pauner Chulvi', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 210, 'Grau en Administraci� d''Empreses', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (151, 62481, 'Cristina Pauner Chulvi', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 211, 'Grau en Economia', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (181, 62481, 'Cristina Pauner Chulvi', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 213, 'Grau en Dret', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (1, 62481, 'Cristina Pauner Chulvi', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 201, 'Grau en Relacions Laborals i Recursos Humans', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (36, 74888, 'Emilio S�ez Soro', 2, 'Facultat de Ci�ncies Humanes i Socials', 203, 'Grau en Comunicaci� Audiovisual', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (394, 65314, 'Enrique Francisco Belenguer Balaguer', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 228, 'Grau en Enginyeria El�ctrica', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (242, 104181, 'Eva Breva Franch', 2, 'Facultat de Ci�ncies Humanes i Socials', 217, 'Grau en Mestre o Mestra d''Educaci� Infantil', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (257, 104181, 'Eva Breva Franch', 2, 'Facultat de Ci�ncies Humanes i Socials', 218, 'Grau en Mestre o Mestra d''Educaci� Prim�ria', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (32, 104181, 'Eva Breva Franch', 2, 'Facultat de Ci�ncies Humanes i Socials', 203, 'Grau en Comunicaci� Audiovisual', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (227, 104181, 'Eva Breva Franch', 2, 'Facultat de Ci�ncies Humanes i Socials', 216, 'Grau en Humanitats: Estudis Interculturals', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (47, 104181, 'Eva Breva Franch', 2, 'Facultat de Ci�ncies Humanes i Socials', 204, 'Grau en Periodisme', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (92, 104181, 'Eva Breva Franch', 2, 'Facultat de Ci�ncies Humanes i Socials', 207, 'Grau en Traducci� i Interpretaci�', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (77, 104181, 'Eva Breva Franch', 2, 'Facultat de Ci�ncies Humanes i Socials', 206, 'Grau en Publicitat i Relacions P�bliques', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (212, 104181, 'Eva Breva Franch', 2, 'Facultat de Ci�ncies Humanes i Socials', 215, 'Grau en Hist�ria i Patrimoni', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (62, 104181, 'Eva Breva Franch', 2, 'Facultat de Ci�ncies Humanes i Socials', 205, 'Grau en Estudis Anglesos', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (405, 63254, 'Eva Cifre Gallego', 2922, 'Facultat de Ci�ncies de la Salut', 229, 'Grau en Medicina', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (272, 63254, 'Eva Cifre Gallego', 2922, 'Facultat de Ci�ncies de la Salut', 219, 'Grau en Psicologia', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (419, 63254, 'Eva Cifre Gallego', 2922, 'Facultat de Ci�ncies de la Salut', 230, 'Grau en Infermeria', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (186, 63685, 'Fernando Juan Mateu', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 213, 'Grau en Dret', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (173, 62479, 'Iolanda Bernab� Mu�oz', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 212, 'Grau en Finances i Comptabilitat', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (445, 62479, 'Iolanda Bernab� Mu�oz', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 231, 'Grau en Disseny i Desenvolupament de Videojocs (pendent d''autoritzaci� d''implantaci�)', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (205, 62479, 'Iolanda Bernab� Mu�oz', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 214, 'Grau en Criminologia i Seguretat', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (218, 62479, 'Iolanda Bernab� Mu�oz', 2, 'Facultat de Ci�ncies Humanes i Socials', 215, 'Grau en Hist�ria i Patrimoni', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (239, 62479, 'Iolanda Bernab� Mu�oz', 2, 'Facultat de Ci�ncies Humanes i Socials', 216, 'Grau en Humanitats: Estudis Interculturals', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (247, 62479, 'Iolanda Bernab� Mu�oz', 2, 'Facultat de Ci�ncies Humanes i Socials', 217, 'Grau en Mestre o Mestra d''Educaci� Infantil', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (269, 62479, 'Iolanda Bernab� Mu�oz', 2, 'Facultat de Ci�ncies Humanes i Socials', 218, 'Grau en Mestre o Mestra d''Educaci� Prim�ria', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (276, 62479, 'Iolanda Bernab� Mu�oz', 2922, 'Facultat de Ci�ncies de la Salut', 219, 'Grau en Psicologia', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (291, 62479, 'Iolanda Bernab� Mu�oz', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 220, 'Grau en Enginyeria Qu�mica', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (312, 62479, 'Iolanda Bernab� Mu�oz', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 221, 'Grau en Enginyeria en Tecnologies Industrials', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (326, 62479, 'Iolanda Bernab� Mu�oz', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 222, 'Grau en Enginyeria Mec�nica', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (336, 62479, 'Iolanda Bernab� Mu�oz', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 223, 'Grau en Matem�tica Computacional', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (350, 62479, 'Iolanda Bernab� Mu�oz', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 224, 'Grau en Enginyeria en Disseny Industrial i Desenvolupament de Productes', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (370, 62479, 'Iolanda Bernab� Mu�oz', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 225, 'Grau en Enginyeria Inform�tica', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (386, 62479, 'Iolanda Bernab� Mu�oz', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 227, 'Grau en Enginyeria Agroaliment�ria i del Medi Rural', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (9, 62479, 'Iolanda Bernab� Mu�oz', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 201, 'Grau en Relacions Laborals i Recursos Humans', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (28, 62479, 'Iolanda Bernab� Mu�oz', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 202, 'Grau en Turisme', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (42, 62479, 'Iolanda Bernab� Mu�oz', 2, 'Facultat de Ci�ncies Humanes i Socials', 203, 'Grau en Comunicaci� Audiovisual', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (57, 62479, 'Iolanda Bernab� Mu�oz', 2, 'Facultat de Ci�ncies Humanes i Socials', 204, 'Grau en Periodisme', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (74, 62479, 'Iolanda Bernab� Mu�oz', 2, 'Facultat de Ci�ncies Humanes i Socials', 205, 'Grau en Estudis Anglesos', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (88, 62479, 'Iolanda Bernab� Mu�oz', 2, 'Facultat de Ci�ncies Humanes i Socials', 206, 'Grau en Publicitat i Relacions P�bliques', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (104, 62479, 'Iolanda Bernab� Mu�oz', 2, 'Facultat de Ci�ncies Humanes i Socials', 207, 'Grau en Traducci� i Interpretaci�', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (112, 62479, 'Iolanda Bernab� Mu�oz', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 208, 'Grau en Qu�mica', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (135, 62479, 'Iolanda Bernab� Mu�oz', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 209, 'Grau en Arquitectura T�cnica', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (143, 62479, 'Iolanda Bernab� Mu�oz', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 210, 'Grau en Administraci� d''Empreses', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (161, 62479, 'Iolanda Bernab� Mu�oz', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 211, 'Grau en Economia', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (458, 62479, 'Iolanda Bernab� Mu�oz', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 232, 'Grau en Gesti� i Administraci� P�blica (pendent d''autoritzaci� d''implantaci�)', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (397, 62479, 'Iolanda Bernab� Mu�oz', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 228, 'Grau en Enginyeria El�ctrica', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (412, 62479, 'Iolanda Bernab� Mu�oz', 2922, 'Facultat de Ci�ncies de la Salut', 229, 'Grau en Medicina', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (427, 62479, 'Iolanda Bernab� Mu�oz', 2922, 'Facultat de Ci�ncies de la Salut', 230, 'Grau en Infermeria', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (193, 62479, 'Iolanda Bernab� Mu�oz', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 213, 'Grau en Dret', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (163, 17292, 'Javier Mu�oz Ferrara', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 211, 'Grau en Economia', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (456, 17292, 'Javier Mu�oz Ferrara', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 232, 'Grau en Gesti� i Administraci� P�blica (pendent d''autoritzaci� d''implantaci�)', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (189, 17292, 'Javier Mu�oz Ferrara', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 213, 'Grau en Dret', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (204, 17292, 'Javier Mu�oz Ferrara', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 214, 'Grau en Criminologia i Seguretat', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (223, 17292, 'Javier Mu�oz Ferrara', 2, 'Facultat de Ci�ncies Humanes i Socials', 215, 'Grau en Hist�ria i Patrimoni', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (234, 17292, 'Javier Mu�oz Ferrara', 2, 'Facultat de Ci�ncies Humanes i Socials', 216, 'Grau en Humanitats: Estudis Interculturals', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (252, 17292, 'Javier Mu�oz Ferrara', 2, 'Facultat de Ci�ncies Humanes i Socials', 217, 'Grau en Mestre o Mestra d''Educaci� Infantil', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (267, 17292, 'Javier Mu�oz Ferrara', 2, 'Facultat de Ci�ncies Humanes i Socials', 218, 'Grau en Mestre o Mestra d''Educaci� Prim�ria', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (282, 17292, 'Javier Mu�oz Ferrara', 2922, 'Facultat de Ci�ncies de la Salut', 219, 'Grau en Psicologia', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (179, 17292, 'Javier Mu�oz Ferrara', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 212, 'Grau en Finances i Comptabilitat', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (439, 17292, 'Javier Mu�oz Ferrara', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 231, 'Grau en Disseny i Desenvolupament de Videojocs (pendent d''autoritzaci� d''implantaci�)', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (424, 17292, 'Javier Mu�oz Ferrara', 2922, 'Facultat de Ci�ncies de la Salut', 230, 'Grau en Infermeria', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (415, 17292, 'Javier Mu�oz Ferrara', 2922, 'Facultat de Ci�ncies de la Salut', 229, 'Grau en Medicina', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (399, 17292, 'Javier Mu�oz Ferrara', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 228, 'Grau en Enginyeria El�ctrica', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (146, 17292, 'Javier Mu�oz Ferrara', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 210, 'Grau en Administraci� d''Empreses', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (133, 17292, 'Javier Mu�oz Ferrara', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 209, 'Grau en Arquitectura T�cnica', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (115, 17292, 'Javier Mu�oz Ferrara', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 208, 'Grau en Qu�mica', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (100, 17292, 'Javier Mu�oz Ferrara', 2, 'Facultat de Ci�ncies Humanes i Socials', 207, 'Grau en Traducci� i Interpretaci�', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (90, 17292, 'Javier Mu�oz Ferrara', 2, 'Facultat de Ci�ncies Humanes i Socials', 206, 'Grau en Publicitat i Relacions P�bliques', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (72, 17292, 'Javier Mu�oz Ferrara', 2, 'Facultat de Ci�ncies Humanes i Socials', 205, 'Grau en Estudis Anglesos', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (55, 17292, 'Javier Mu�oz Ferrara', 2, 'Facultat de Ci�ncies Humanes i Socials', 204, 'Grau en Periodisme', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (297, 17292, 'Javier Mu�oz Ferrara', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 220, 'Grau en Enginyeria Qu�mica', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (308, 17292, 'Javier Mu�oz Ferrara', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 221, 'Grau en Enginyeria en Tecnologies Industrials', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (324, 17292, 'Javier Mu�oz Ferrara', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 222, 'Grau en Enginyeria Mec�nica', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (340, 17292, 'Javier Mu�oz Ferrara', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 223, 'Grau en Matem�tica Computacional', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (356, 17292, 'Javier Mu�oz Ferrara', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 224, 'Grau en Enginyeria en Disseny Industrial i Desenvolupament de Productes', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (367, 17292, 'Javier Mu�oz Ferrara', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 225, 'Grau en Enginyeria Inform�tica', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (382, 17292, 'Javier Mu�oz Ferrara', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 227, 'Grau en Enginyeria Agroaliment�ria i del Medi Rural', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (8, 17292, 'Javier Mu�oz Ferrara', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 201, 'Grau en Relacions Laborals i Recursos Humans', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (30, 17292, 'Javier Mu�oz Ferrara', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 202, 'Grau en Turisme', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (38, 17292, 'Javier Mu�oz Ferrara', 2, 'Facultat de Ci�ncies Humanes i Socials', 203, 'Grau en Comunicaci� Audiovisual', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (111, 60611, 'Joaqu�n Beltr�n Arandes', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 208, 'Grau en Qu�mica', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (107, 60700, 'Jos� Joaquin Gual Arnau', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 208, 'Grau en Qu�mica', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (345, 60700, 'Jos� Joaquin Gual Arnau', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 224, 'Grau en Enginyeria en Disseny Industrial i Desenvolupament de Productes', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (390, 60700, 'Jos� Joaquin Gual Arnau', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 228, 'Grau en Enginyeria El�ctrica', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (286, 60700, 'Jos� Joaquin Gual Arnau', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 220, 'Grau en Enginyeria Qu�mica', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (122, 60700, 'Jos� Joaquin Gual Arnau', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 209, 'Grau en Arquitectura T�cnica', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (316, 60700, 'Jos� Joaquin Gual Arnau', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 222, 'Grau en Enginyeria Mec�nica', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (433, 60700, 'Jos� Joaquin Gual Arnau', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 231, 'Grau en Disseny i Desenvolupament de Videojocs (pendent d''autoritzaci� d''implantaci�)', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (301, 60700, 'Jos� Joaquin Gual Arnau', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 221, 'Grau en Enginyeria en Tecnologies Industrials', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (375, 60700, 'Jos� Joaquin Gual Arnau', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 227, 'Grau en Enginyeria Agroaliment�ria i del Medi Rural', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (360, 60700, 'Jos� Joaquin Gual Arnau', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 225, 'Grau en Enginyeria Inform�tica', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (330, 60700, 'Jos� Joaquin Gual Arnau', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 223, 'Grau en Matem�tica Computacional', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (180, 64930, 'Juan Miguel Vilar Torres', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 212, 'Grau en Finances i Comptabilitat', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (188, 64930, 'Juan Miguel Vilar Torres', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 213, 'Grau en Dret', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (207, 64930, 'Juan Miguel Vilar Torres', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 214, 'Grau en Criminologia i Seguretat', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (225, 64930, 'Juan Miguel Vilar Torres', 2, 'Facultat de Ci�ncies Humanes i Socials', 215, 'Grau en Hist�ria i Patrimoni', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (238, 64930, 'Juan Miguel Vilar Torres', 2, 'Facultat de Ci�ncies Humanes i Socials', 216, 'Grau en Humanitats: Estudis Interculturals', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (248, 64930, 'Juan Miguel Vilar Torres', 2, 'Facultat de Ci�ncies Humanes i Socials', 217, 'Grau en Mestre o Mestra d''Educaci� Infantil', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (270, 64930, 'Juan Miguel Vilar Torres', 2, 'Facultat de Ci�ncies Humanes i Socials', 218, 'Grau en Mestre o Mestra d''Educaci� Prim�ria', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (277, 64930, 'Juan Miguel Vilar Torres', 2922, 'Facultat de Ci�ncies de la Salut', 219, 'Grau en Psicologia', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (296, 64930, 'Juan Miguel Vilar Torres', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 220, 'Grau en Enginyeria Qu�mica', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (310, 64930, 'Juan Miguel Vilar Torres', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 221, 'Grau en Enginyeria en Tecnologies Industrials', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (320, 64930, 'Juan Miguel Vilar Torres', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 222, 'Grau en Enginyeria Mec�nica', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (337, 64930, 'Juan Miguel Vilar Torres', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 223, 'Grau en Matem�tica Computacional', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (354, 64930, 'Juan Miguel Vilar Torres', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 224, 'Grau en Enginyeria en Disseny Industrial i Desenvolupament de Productes', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (373, 64930, 'Juan Miguel Vilar Torres', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 225, 'Grau en Enginyeria Inform�tica', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (7, 64930, 'Juan Miguel Vilar Torres', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 201, 'Grau en Relacions Laborals i Recursos Humans', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (24, 64930, 'Juan Miguel Vilar Torres', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 202, 'Grau en Turisme', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (39, 64930, 'Juan Miguel Vilar Torres', 2, 'Facultat de Ci�ncies Humanes i Socials', 203, 'Grau en Comunicaci� Audiovisual', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (56, 64930, 'Juan Miguel Vilar Torres', 2, 'Facultat de Ci�ncies Humanes i Socials', 204, 'Grau en Periodisme', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (70, 64930, 'Juan Miguel Vilar Torres', 2, 'Facultat de Ci�ncies Humanes i Socials', 205, 'Grau en Estudis Anglesos', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (87, 64930, 'Juan Miguel Vilar Torres', 2, 'Facultat de Ci�ncies Humanes i Socials', 206, 'Grau en Publicitat i Relacions P�bliques', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (102, 64930, 'Juan Miguel Vilar Torres', 2, 'Facultat de Ci�ncies Humanes i Socials', 207, 'Grau en Traducci� i Interpretaci�', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (114, 64930, 'Juan Miguel Vilar Torres', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 208, 'Grau en Qu�mica', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (131, 64930, 'Juan Miguel Vilar Torres', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 209, 'Grau en Arquitectura T�cnica', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (149, 64930, 'Juan Miguel Vilar Torres', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 210, 'Grau en Administraci� d''Empreses', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (158, 64930, 'Juan Miguel Vilar Torres', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 211, 'Grau en Economia', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (459, 64930, 'Juan Miguel Vilar Torres', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 232, 'Grau en Gesti� i Administraci� P�blica (pendent d''autoritzaci� d''implantaci�)', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (388, 64930, 'Juan Miguel Vilar Torres', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 227, 'Grau en Enginyeria Agroaliment�ria i del Medi Rural', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (396, 64930, 'Juan Miguel Vilar Torres', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 228, 'Grau en Enginyeria El�ctrica', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (417, 64930, 'Juan Miguel Vilar Torres', 2922, 'Facultat de Ci�ncies de la Salut', 229, 'Grau en Medicina', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (425, 64930, 'Juan Miguel Vilar Torres', 2922, 'Facultat de Ci�ncies de la Salut', 230, 'Grau en Infermeria', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (443, 64930, 'Juan Miguel Vilar Torres', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 231, 'Grau en Disseny i Desenvolupament de Videojocs (pendent d''autoritzaci� d''implantaci�)', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (349, 65066, 'Julia Gal�n Serrano', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 224, 'Grau en Enginyeria en Disseny Industrial i Desenvolupament de Productes', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (141, 58645, 'Luis Jose Callarisa Fiol', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 210, 'Grau en Administraci� d''Empreses', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (6, 60701, 'Mar�a Arantzazu Vicente Palacio', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 201, 'Grau en Relacions Laborals i Recursos Humans', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (81, 95402, 'Mar�a del Roc�o Blay Arr�ez', 2, 'Facultat de Ci�ncies Humanes i Socials', 206, 'Grau en Publicitat i Relacions P�bliques', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (305, 62314, 'Mar�a Dolores Bovea Edo', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 221, 'Grau en Enginyeria en Tecnologies Industrials', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (96, 65480, 'Mar�a Jes�s Blasco Mayor', 2, 'Facultat de Ci�ncies Humanes i Socials', 207, 'Grau en Traducci� i Interpretaci�', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (171, 65167, 'Mar�a Jes�s Mu�oz Torres', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 212, 'Grau en Finances i Comptabilitat', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (364, 54341, 'Mar�a Jos� Aramburu Cabo', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 225, 'Grau en Enginyeria Inform�tica', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (66, 87794, 'Maria Lluisa Gea Valor', 2, 'Facultat de Ci�ncies Humanes i Socials', 205, 'Grau en Estudis Anglesos', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (422, 249058, 'Maria Loreto Josefa Maci� Soler', 2922, 'Facultat de Ci�ncies de la Salut', 230, 'Grau en Infermeria', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (121, 65364, 'Mar�a Mercedes Fern�ndez Alonso', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 209, 'Grau en Arquitectura T�cnica', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (106, 65364, 'Mar�a Mercedes Fern�ndez Alonso', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 208, 'Grau en Qu�mica', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (374, 65364, 'Mar�a Mercedes Fern�ndez Alonso', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 227, 'Grau en Enginyeria Agroaliment�ria i del Medi Rural', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (359, 65364, 'Mar�a Mercedes Fern�ndez Alonso', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 225, 'Grau en Enginyeria Inform�tica', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (344, 65364, 'Mar�a Mercedes Fern�ndez Alonso', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 224, 'Grau en Enginyeria en Disseny Industrial i Desenvolupament de Productes', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (329, 65364, 'Mar�a Mercedes Fern�ndez Alonso', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 223, 'Grau en Matem�tica Computacional', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (315, 65364, 'Mar�a Mercedes Fern�ndez Alonso', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 222, 'Grau en Enginyeria Mec�nica', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (300, 65364, 'Mar�a Mercedes Fern�ndez Alonso', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 221, 'Grau en Enginyeria en Tecnologies Industrials', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (285, 65364, 'Mar�a Mercedes Fern�ndez Alonso', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 220, 'Grau en Enginyeria Qu�mica', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (432, 65364, 'Mar�a Mercedes Fern�ndez Alonso', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 231, 'Grau en Disseny i Desenvolupament de Videojocs (pendent d''autoritzaci� d''implantaci�)', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (389, 65364, 'Mar�a Mercedes Fern�ndez Alonso', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 228, 'Grau en Enginyeria El�ctrica', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (408, 471242, 'Mar�a Trinidad Herrero Ezquerro', 2922, 'Facultat de Ci�ncies de la Salut', 229, 'Grau en Medicina', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (437, 65388, 'Miguel Chover Selles', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 231, 'Grau en Disseny i Desenvolupament de Videojocs (pendent d''autoritzaci� d''implantaci�)', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (156, 85454, 'Miguel Gin�s Vilar', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 211, 'Grau en Economia', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (246, 59325, 'Miguel Salvador Bauza', 2, 'Facultat de Ci�ncies Humanes i Socials', 217, 'Grau en Mestre o Mestra d''Educaci� Infantil', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (177, 831, 'Pascual David Rubert Viana', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 212, 'Grau en Finances i Comptabilitat', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (454, 831, 'Pascual David Rubert Viana', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 232, 'Grau en Gesti� i Administraci� P�blica (pendent d''autoritzaci� d''implantaci�)', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (203, 831, 'Pascual David Rubert Viana', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 214, 'Grau en Criminologia i Seguretat', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (220, 831, 'Pascual David Rubert Viana', 2, 'Facultat de Ci�ncies Humanes i Socials', 215, 'Grau en Hist�ria i Patrimoni', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (237, 831, 'Pascual David Rubert Viana', 2, 'Facultat de Ci�ncies Humanes i Socials', 216, 'Grau en Humanitats: Estudis Interculturals', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (254, 831, 'Pascual David Rubert Viana', 2, 'Facultat de Ci�ncies Humanes i Socials', 217, 'Grau en Mestre o Mestra d''Educaci� Infantil', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (268, 831, 'Pascual David Rubert Viana', 2, 'Facultat de Ci�ncies Humanes i Socials', 218, 'Grau en Mestre o Mestra d''Educaci� Prim�ria', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (279, 831, 'Pascual David Rubert Viana', 2922, 'Facultat de Ci�ncies de la Salut', 219, 'Grau en Psicologia', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (295, 831, 'Pascual David Rubert Viana', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 220, 'Grau en Enginyeria Qu�mica', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (314, 831, 'Pascual David Rubert Viana', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 221, 'Grau en Enginyeria en Tecnologies Industrials', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (327, 831, 'Pascual David Rubert Viana', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 222, 'Grau en Enginyeria Mec�nica', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (335, 831, 'Pascual David Rubert Viana', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 223, 'Grau en Matem�tica Computacional', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (355, 831, 'Pascual David Rubert Viana', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 224, 'Grau en Enginyeria en Disseny Industrial i Desenvolupament de Productes', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (372, 831, 'Pascual David Rubert Viana', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 225, 'Grau en Enginyeria Inform�tica', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (381, 831, 'Pascual David Rubert Viana', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 227, 'Grau en Enginyeria Agroaliment�ria i del Medi Rural', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (12, 831, 'Pascual David Rubert Viana', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 201, 'Grau en Relacions Laborals i Recursos Humans', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (27, 831, 'Pascual David Rubert Viana', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 202, 'Grau en Turisme', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (44, 831, 'Pascual David Rubert Viana', 2, 'Facultat de Ci�ncies Humanes i Socials', 203, 'Grau en Comunicaci� Audiovisual', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (52, 831, 'Pascual David Rubert Viana', 2, 'Facultat de Ci�ncies Humanes i Socials', 204, 'Grau en Periodisme', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (69, 831, 'Pascual David Rubert Viana', 2, 'Facultat de Ci�ncies Humanes i Socials', 205, 'Grau en Estudis Anglesos', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (82, 831, 'Pascual David Rubert Viana', 2, 'Facultat de Ci�ncies Humanes i Socials', 206, 'Grau en Publicitat i Relacions P�bliques', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (103, 831, 'Pascual David Rubert Viana', 2, 'Facultat de Ci�ncies Humanes i Socials', 207, 'Grau en Traducci� i Interpretaci�', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (113, 831, 'Pascual David Rubert Viana', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 208, 'Grau en Qu�mica', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (128, 831, 'Pascual David Rubert Viana', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 209, 'Grau en Arquitectura T�cnica', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (148, 831, 'Pascual David Rubert Viana', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 210, 'Grau en Administraci� d''Empreses', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (157, 831, 'Pascual David Rubert Viana', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 211, 'Grau en Economia', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (395, 831, 'Pascual David Rubert Viana', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 228, 'Grau en Enginyeria El�ctrica', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (414, 831, 'Pascual David Rubert Viana', 2922, 'Facultat de Ci�ncies de la Salut', 229, 'Grau en Medicina', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (426, 831, 'Pascual David Rubert Viana', 2922, 'Facultat de Ci�ncies de la Salut', 230, 'Grau en Infermeria', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (441, 831, 'Pascual David Rubert Viana', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 231, 'Grau en Disseny i Desenvolupament de Videojocs (pendent d''autoritzaci� d''implantaci�)', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (192, 831, 'Pascual David Rubert Viana', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 213, 'Grau en Dret', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (175, 65428, 'Rafael Ballester Arnal', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 212, 'Grau en Finances i Comptabilitat', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (457, 65428, 'Rafael Ballester Arnal', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 232, 'Grau en Gesti� i Administraci� P�blica (pendent d''autoritzaci� d''implantaci�)', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (208, 65428, 'Rafael Ballester Arnal', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 214, 'Grau en Criminologia i Seguretat', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (191, 65428, 'Rafael Ballester Arnal', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 213, 'Grau en Dret', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (440, 65428, 'Rafael Ballester Arnal', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 231, 'Grau en Disseny i Desenvolupament de Videojocs (pendent d''autoritzaci� d''implantaci�)', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (428, 65428, 'Rafael Ballester Arnal', 2922, 'Facultat de Ci�ncies de la Salut', 230, 'Grau en Infermeria', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (418, 65428, 'Rafael Ballester Arnal', 2922, 'Facultat de Ci�ncies de la Salut', 230, 'Grau en Infermeria', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (413, 65428, 'Rafael Ballester Arnal', 2922, 'Facultat de Ci�ncies de la Salut', 229, 'Grau en Medicina', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (404, 65428, 'Rafael Ballester Arnal', 2922, 'Facultat de Ci�ncies de la Salut', 229, 'Grau en Medicina', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (402, 65428, 'Rafael Ballester Arnal', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 228, 'Grau en Enginyeria El�ctrica', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (159, 65428, 'Rafael Ballester Arnal', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 211, 'Grau en Economia', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (150, 65428, 'Rafael Ballester Arnal', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 210, 'Grau en Administraci� d''Empreses', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (127, 65428, 'Rafael Ballester Arnal', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 209, 'Grau en Arquitectura T�cnica', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (118, 65428, 'Rafael Ballester Arnal', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 208, 'Grau en Qu�mica', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (219, 65428, 'Rafael Ballester Arnal', 2, 'Facultat de Ci�ncies Humanes i Socials', 215, 'Grau en Hist�ria i Patrimoni', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (240, 65428, 'Rafael Ballester Arnal', 2, 'Facultat de Ci�ncies Humanes i Socials', 216, 'Grau en Humanitats: Estudis Interculturals', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (253, 65428, 'Rafael Ballester Arnal', 2, 'Facultat de Ci�ncies Humanes i Socials', 217, 'Grau en Mestre o Mestra d''Educaci� Infantil', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (265, 65428, 'Rafael Ballester Arnal', 2, 'Facultat de Ci�ncies Humanes i Socials', 218, 'Grau en Mestre o Mestra d''Educaci� Prim�ria', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (271, 65428, 'Rafael Ballester Arnal', 2922, 'Facultat de Ci�ncies de la Salut', 219, 'Grau en Psicologia', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (278, 65428, 'Rafael Ballester Arnal', 2922, 'Facultat de Ci�ncies de la Salut', 219, 'Grau en Psicologia', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (299, 65428, 'Rafael Ballester Arnal', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 220, 'Grau en Enginyeria Qu�mica', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (311, 65428, 'Rafael Ballester Arnal', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 221, 'Grau en Enginyeria en Tecnologies Industrials', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (325, 65428, 'Rafael Ballester Arnal', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 222, 'Grau en Enginyeria Mec�nica', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (342, 65428, 'Rafael Ballester Arnal', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 223, 'Grau en Matem�tica Computacional', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (358, 65428, 'Rafael Ballester Arnal', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 224, 'Grau en Enginyeria en Disseny Industrial i Desenvolupament de Productes', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (371, 65428, 'Rafael Ballester Arnal', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 225, 'Grau en Enginyeria Inform�tica', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (387, 65428, 'Rafael Ballester Arnal', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 227, 'Grau en Enginyeria Agroaliment�ria i del Medi Rural', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (11, 65428, 'Rafael Ballester Arnal', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 201, 'Grau en Relacions Laborals i Recursos Humans', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (23, 65428, 'Rafael Ballester Arnal', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 202, 'Grau en Turisme', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (40, 65428, 'Rafael Ballester Arnal', 2, 'Facultat de Ci�ncies Humanes i Socials', 203, 'Grau en Comunicaci� Audiovisual', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (59, 65428, 'Rafael Ballester Arnal', 2, 'Facultat de Ci�ncies Humanes i Socials', 204, 'Grau en Periodisme', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (75, 65428, 'Rafael Ballester Arnal', 2, 'Facultat de Ci�ncies Humanes i Socials', 205, 'Grau en Estudis Anglesos', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (86, 65428, 'Rafael Ballester Arnal', 2, 'Facultat de Ci�ncies Humanes i Socials', 206, 'Grau en Publicitat i Relacions P�bliques', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (99, 65428, 'Rafael Ballester Arnal', 2, 'Facultat de Ci�ncies Humanes i Socials', 207, 'Grau en Traducci� i Interpretaci�', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (21, 49999, 'Rafael Lapiedra Alcam�', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 202, 'Grau en Turisme', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (261, 64514, 'Roberto Jose Garc�a Antolin', 2, 'Facultat de Ci�ncies Humanes i Socials', 218, 'Grau en Mestre o Mestra d''Educaci� Prim�ria', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (61, 61463, 'Rosa Mar�a Agost Can�s', 2, 'Facultat de Ci�ncies Humanes i Socials', 205, 'Grau en Estudis Anglesos', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (256, 61463, 'Rosa Mar�a Agost Can�s', 2, 'Facultat de Ci�ncies Humanes i Socials', 218, 'Grau en Mestre o Mestra d''Educaci� Prim�ria', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (91, 61463, 'Rosa Mar�a Agost Can�s', 2, 'Facultat de Ci�ncies Humanes i Socials', 207, 'Grau en Traducci� i Interpretaci�', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (241, 61463, 'Rosa Mar�a Agost Can�s', 2, 'Facultat de Ci�ncies Humanes i Socials', 217, 'Grau en Mestre o Mestra d''Educaci� Infantil', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (76, 61463, 'Rosa Mar�a Agost Can�s', 2, 'Facultat de Ci�ncies Humanes i Socials', 206, 'Grau en Publicitat i Relacions P�bliques', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (226, 61463, 'Rosa Mar�a Agost Can�s', 2, 'Facultat de Ci�ncies Humanes i Socials', 216, 'Grau en Humanitats: Estudis Interculturals', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (46, 61463, 'Rosa Mar�a Agost Can�s', 2, 'Facultat de Ci�ncies Humanes i Socials', 204, 'Grau en Periodisme', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (31, 61463, 'Rosa Mar�a Agost Can�s', 2, 'Facultat de Ci�ncies Humanes i Socials', 203, 'Grau en Comunicaci� Audiovisual', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (211, 61463, 'Rosa Mar�a Agost Can�s', 2, 'Facultat de Ci�ncies Humanes i Socials', 215, 'Grau en Hist�ria i Patrimoni', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (231, 58607, 'Sonia Reverter Ba��n', 2, 'Facultat de Ci�ncies Humanes i Socials', 216, 'Grau en Humanitats: Estudis Interculturals', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (290, 65016, 'Vicente Beltr�n Porcar', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 220, 'Grau en Enginyeria Qu�mica', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (453, 56321, 'Vicente Bud� Ordu�a', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 232, 'Grau en Gesti� i Administraci� P�blica (pendent d''autoritzaci� d''implantaci�)', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (182, 56321, 'Vicente Bud� Ordu�a', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 213, 'Grau en Dret', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (190, 56321, 'Vicente Bud� Ordu�a', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 213, 'Grau en Dret', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (197, 56321, 'Vicente Bud� Ordu�a', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 214, 'Grau en Criminologia i Seguretat', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (209, 56321, 'Vicente Bud� Ordu�a', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 214, 'Grau en Criminologia i Seguretat', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (221, 56321, 'Vicente Bud� Ordu�a', 2, 'Facultat de Ci�ncies Humanes i Socials', 215, 'Grau en Hist�ria i Patrimoni', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (235, 56321, 'Vicente Bud� Ordu�a', 2, 'Facultat de Ci�ncies Humanes i Socials', 216, 'Grau en Humanitats: Estudis Interculturals', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (251, 56321, 'Vicente Bud� Ordu�a', 2, 'Facultat de Ci�ncies Humanes i Socials', 217, 'Grau en Mestre o Mestra d''Educaci� Infantil', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (266, 56321, 'Vicente Bud� Ordu�a', 2, 'Facultat de Ci�ncies Humanes i Socials', 218, 'Grau en Mestre o Mestra d''Educaci� Prim�ria', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (281, 56321, 'Vicente Bud� Ordu�a', 2922, 'Facultat de Ci�ncies de la Salut', 219, 'Grau en Psicologia', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (298, 56321, 'Vicente Bud� Ordu�a', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 220, 'Grau en Enginyeria Qu�mica', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (306, 56321, 'Vicente Bud� Ordu�a', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 221, 'Grau en Enginyeria en Tecnologies Industrials', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (328, 56321, 'Vicente Bud� Ordu�a', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 222, 'Grau en Enginyeria Mec�nica', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (339, 56321, 'Vicente Bud� Ordu�a', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 223, 'Grau en Matem�tica Computacional', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (353, 56321, 'Vicente Bud� Ordu�a', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 224, 'Grau en Enginyeria en Disseny Industrial i Desenvolupament de Productes', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (368, 56321, 'Vicente Bud� Ordu�a', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 225, 'Grau en Enginyeria Inform�tica', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (167, 56321, 'Vicente Bud� Ordu�a', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 212, 'Grau en Finances i Comptabilitat', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (172, 56321, 'Vicente Bud� Ordu�a', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 212, 'Grau en Finances i Comptabilitat', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (384, 56321, 'Vicente Bud� Ordu�a', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 227, 'Grau en Enginyeria Agroaliment�ria i del Medi Rural', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (2, 56321, 'Vicente Bud� Ordu�a', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 201, 'Grau en Relacions Laborals i Recursos Humans', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (15, 56321, 'Vicente Bud� Ordu�a', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 201, 'Grau en Relacions Laborals i Recursos Humans', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (17, 56321, 'Vicente Bud� Ordu�a', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 202, 'Grau en Turisme', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (22, 56321, 'Vicente Bud� Ordu�a', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 202, 'Grau en Turisme', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (45, 56321, 'Vicente Bud� Ordu�a', 2, 'Facultat de Ci�ncies Humanes i Socials', 203, 'Grau en Comunicaci� Audiovisual', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (60, 56321, 'Vicente Bud� Ordu�a', 2, 'Facultat de Ci�ncies Humanes i Socials', 204, 'Grau en Periodisme', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (73, 56321, 'Vicente Bud� Ordu�a', 2, 'Facultat de Ci�ncies Humanes i Socials', 205, 'Grau en Estudis Anglesos', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (89, 56321, 'Vicente Bud� Ordu�a', 2, 'Facultat de Ci�ncies Humanes i Socials', 206, 'Grau en Publicitat i Relacions P�bliques', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (101, 56321, 'Vicente Bud� Ordu�a', 2, 'Facultat de Ci�ncies Humanes i Socials', 207, 'Grau en Traducci� i Interpretaci�', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (120, 56321, 'Vicente Bud� Ordu�a', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 208, 'Grau en Qu�mica', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (134, 56321, 'Vicente Bud� Ordu�a', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 209, 'Grau en Arquitectura T�cnica', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (137, 56321, 'Vicente Bud� Ordu�a', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 210, 'Grau en Administraci� d''Empreses', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (142, 56321, 'Vicente Bud� Ordu�a', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 210, 'Grau en Administraci� d''Empreses', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (152, 56321, 'Vicente Bud� Ordu�a', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 211, 'Grau en Economia', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (160, 56321, 'Vicente Bud� Ordu�a', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 211, 'Grau en Economia', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (401, 56321, 'Vicente Bud� Ordu�a', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 228, 'Grau en Enginyeria El�ctrica', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (411, 56321, 'Vicente Bud� Ordu�a', 2922, 'Facultat de Ci�ncies de la Salut', 229, 'Grau en Medicina', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (429, 56321, 'Vicente Bud� Ordu�a', 2922, 'Facultat de Ci�ncies de la Salut', 230, 'Grau en Infermeria', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (442, 56321, 'Vicente Bud� Ordu�a', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 231, 'Grau en Disseny i Desenvolupament de Videojocs (pendent d''autoritzaci� d''implantaci�)', 1, 'Director d''estudi');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (448, 56321, 'Vicente Bud� Ordu�a', 3, 'Facultat de Ci�ncies Jur�diques i Econ�miques', 232, 'Grau en Gesti� i Administraci� P�blica (pendent d''autoritzaci� d''implantaci�)', 3, 'Director, Dega o Secretari de Centre');
Insert into UJI_HORARIOS.HOR_EXT_CARGOS_PER
   (ID, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, ESTUDIO_ID, ESTUDIO, CARGO_ID, CARGO)
 Values
   (379, 62645, 'V�ctor Flors Herrero', 4, 'Escola Superior de Tecnologia i Ci�ncies Experimentals', 227, 'Grau en Enginyeria Agroaliment�ria i del Medi Rural', 1, 'Director d''estudi');
COMMIT;

