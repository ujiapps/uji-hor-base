/* Formatted on 03/07/2014 15:05 (Formatter Plus v4.8.8) */
declare
   v_curso_aca   number        := 2014;
   v_existe      number;
   v_salida      boolean       := false;
   v_grupo       varchar2 (10);
   v_contar      number;

   cursor lista_inicial is
      select 'ALTER TRIGGER gra_pod.POD_UPD_FECHAS_FK DISABLE' accion
        from dual;

   cursor lista_final is
      select 'ALTER TRIGGER gra_pod.POD_UPD_FECHAS_FK ENABLE' accion
        from dual;

   cursor lista_examenes is
     
    select asignatura_id, convocatoria_id, epo_id, a.semestre, codigo, trunc (fecha) fecha, hora_inicio, hora_fin,
             comentario, decode (instr (e.nombre, 'arcial'), 0, 'N', 'S') parcial, 'N' definitivo, null tag,
             decode (comun, 0, 'N', 'S') compartido
        from uji_horarios.hor_examenes e,
             uji_horarios.hor_examenes_asignaturas a,
             pod_epocas_convoc ec,
             uji_horarios.hor_tipos_examenes tp
       where e.id = a.examen_id
         and convocatoria_id = con_id
         and a.semestre = ec.semestre
         and e.tipo_examen_id = tp.id
         --and estudio_id > 205
         and fecha is null;

   procedure ejecutar (p_accion in varchar2) is
   begin
      execute immediate p_accion;
   end;

   procedure desactivar_triggers is
   begin
      for x in lista_inicial loop
         ejecutar (x.accion);
      end loop;
   end;

   procedure activar_triggers is
   begin
      for x in lista_final loop
         ejecutar (x.accion);
      end loop;
   end;
begin
   desactivar_triggers;

   for x in lista_examenes loop
      begin
         insert into pod_fechas_examenes
                     (ASI_ID, CON_ID, EPO_ID, SEMESTRE, CURSO_ACA, TIPO, FECHA,
                      INI, FIN, COMENTARIO, PARCIAL, DEFINITIVO, TAG, COMPARTIDO
                     )
         values      (x.asignatura_id, x.convocatoria_id, x.epo_id, x.semestre, v_curso_aca, x.codigo, x.fecha,
                      x.hora_inicio, x.hora_fin, x.comentario, x.parcial, x.definitivo, x.tag, x.compartido
                     );
      exception
         when others then
            dbms_output.put_line (x.asignatura_id || ' ' || x.convocatoria_id || ' ' || sqlerrm);
      end;
   end loop;

   activar_triggers;
end;



--------------------------------





select * 
from pod_fechas_examenes
where curso_Aca =2014
order by 1




select distinct asignatura_id
from (
select asignatura_id, convocatoria_id, epo_id, to_char(a.semestre) semestre, 2014 curso_Aca, codigo, trunc (fecha) fecha, hora_inicio, hora_fin,
             comentario, decode (instr (e.nombre, 'arcial'), 0, 'N', 'S') parcial, 'N' definitivo, null tag,
             decode (comun, 0, 'N', 'S') compartido
        from uji_horarios.hor_examenes e,
             uji_horarios.hor_examenes_asignaturas a,
             pod_epocas_convoc ec,
             uji_horarios.hor_tipos_examenes tp
       where e.id = a.examen_id
         and convocatoria_id = con_id
         and a.semestre = ec.semestre
         and e.tipo_examen_id = tp.id
         --and estudio_id > 205
         and fecha is null
         and e.nombre not like '%arcial%'
minus
select *  
from pod_fechas_examenes
where curso_Aca =2014
)
order by 1


select *  
from pod_fechas_examenes
where curso_Aca =2014
minus
select asignatura_id, convocatoria_id, epo_id, to_char(a.semestre) semestre, 2014 curso_Aca, codigo, trunc (fecha) fecha, hora_inicio, hora_fin,
             comentario, decode (instr (e.nombre, 'arcial'), 0, 'N', 'S') parcial, 'N' definitivo, null tag,
             decode (comun, 0, 'N', 'S') compartido
        from uji_horarios.hor_examenes e,
             uji_horarios.hor_examenes_asignaturas a,
             pod_epocas_convoc ec,
             uji_horarios.hor_tipos_examenes tp
       where e.id = a.examen_id
         and convocatoria_id = con_id
         and a.semestre = ec.semestre
         and e.tipo_examen_id = tp.id
         --and estudio_id > 205
         and fecha is not null


