CREATE OR REPLACE PROCEDURE UJI_HORARIOS.CIRCUITOS_VALIDACION_PDF (
   name_array    IN   euji_util.ident_arr2,
   value_array   IN   euji_util.ident_arr2
) IS
   -- Parametros
   pEstudio    CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pEstudio');
   pCircuito   CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pCircuito');
   pGrupo      CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pGrupo');
   --pCurso      CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pCurso');
   pSesion     CONSTANT VARCHAR2 (4000) := euji_util.euji_getparam (name_array, value_array, 'pSesion');
   v_nombre             varchar2 (2000);
   vSesion              number;

   cursor lista_circuito is
      select grupo_id, nombre, plazas
      from   hor_circuitos
      where  id = pCircuito
      and    grupo_id = pGrupo;

   cursor lista_semestres is
      select   1 id
      from     dual
      union all
      select   2 id
      from     dual
      order by 1;

   procedure cabecera is
   begin
      owa_util.mime_header (ccontent_type => 'text/xml', bclose_header => true);
      fop.documentOpen (margin_left => 1.5, margin_right => 1.5, margin_top => 0.5, margin_bottom => 0.5,
                        extent_before => 3, extent_after => 1.5, extent_margin_top => 3, extent_margin_bottom => 1.5);
      fop.pageBodyOpen;

      for x in lista_circuito loop
         htp.p (fof.documentheaderopen || fof.blockopen (space_after => 0.1) || fof.tableOpen
                || fof.tablecolumndefinition (column_width => 3) || fof.tablecolumndefinition (column_width => 15)
                || fof.tablebodyopen || fof.tableRowOpen || fof.tablecellopen
                || fof.block (fof.logo_uji, text_align => 'center') || fof.tablecellclose
                || fof.tablecellopen (display_align => 'center', padding => 0.4)
                || fof.block (text => 'Controls Circuits', font_size => 12, font_weight => 'bold',
                              font_family => 'Arial', text_align => 'center')
                || fof.block (text => v_nombre, font_size => 12, font_weight => 'bold', font_family => 'Arial',
                              text_align => 'center')
                || fof.block (text => 'Curs: 1 - Grup: ' || pGrupo || ' - Circuit: ' || x.nombre || ' (' || x.plazas
                               || ' pla�es)',
                              font_size => 12, font_weight => 'bold', font_family => 'Arial', text_align => 'center')
                || fof.tablecellclose || fof.tableRowClose || fof.tablebodyclose || fof.tableClose || fof.blockclose
                || fof.documentheaderclose);
         -- El numero paginas se escribe a mano para poner el texto m?s peque?o
         htp.p (fof.documentfooteropen || fof.tableOpen || fof.tablecolumndefinition (column_width => 18)
                || fof.tablebodyopen || fof.tableRowOpen || fof.tablecellopen
                || fof.block (text => 'Pag. <fo:page-number /> de <fo:page-number-citation ref-id="lastBlock" />',
                              text_align => 'right', font_weight => 'bold', font_size => 8)
                || fof.tablecellclose || fof.tableRowClose || fof.tablebodyclose || fof.tableClose
                || fof.documentfooterclose);
      end loop;

      fop.pageFlowOpen;
      fop.block (fof.white_space);
   end;

   procedure pie is
   begin
      htp.p ('<fo:block  id="lastBlock" space-before="1cm" font-size="8pt">');
      htp.p ('Data de generaci�: ' || to_char (sysdate, 'dd/mm/yyy hh24:mi'));
      fop.blockclose ();
      fop.pageFlowClose;
      fop.pageBodyClose;
      fop.documentClose;
   end;

   function espacios (p_espacios in number)
      return varchar2 is
      v_rdo   varchar2 (2000);
   begin
      v_rdo := null;

      for x in 1 .. p_espacios loop
         v_rdo := v_rdo || fof.white_space;
      end loop;

      return v_rdo;
   end;

   procedure p (p_texto in varchar2) is
   begin
      fop.block (espacios (3) || p_texto);
   end;

   procedure control_1 (pSemestre in number) is
      v_aux   number := 0;

      cursor lista_errores_1 is
         select   *
         from     (select distinct ia.asignatura_id, tipo_subgrupo_id, grupo_id
                   from            hor_items i,
                                   hor_items_asignaturas ia
                   where           ia.item_id = i.id
                   and             ia.estudio_id = pEstudio
                   and             semestre_id = pSemestre
                   and             grupo_id = pGrupo
                   and             tipo_subgrupo_id not in ('AV', 'TU')
                   and             curso_id = 1
                   minus
                   select distinct ia.asignatura_id, tipo_subgrupo_id, grupo_id
                   from            hor_items_circuitos ic,
                                   hor_items i,
                                   hor_items_asignaturas ia
                   where           circuito_id = pCircuito
                   and             i.id = ic.item_id
                   and             ia.item_id = i.id
                   and             ia.estudio_id = pEstudio
                   and             semestre_id = pSemestre
                   and             grupo_id = pGrupo)
         order by 1,
                  decode (tipo_subgrupo_id, 'TE', 1, 'PR', 2, 'LA', 3, 'SE', 4, 5);
   begin
      fop.block ('Control 1 - Subgrups que no estan en el circuit', font_size => 12, border_bottom => 'solid',
                 space_after => 0.1);

      for x in lista_errores_1 loop
         v_aux := v_aux + 1;
         p (x.asignatura_id || ' ' || x.tipo_subgrupo_id);
      end loop;

      if v_aux = 0 then
         fop.block ('No hi ha cap error.', space_before => 0.5);
      elsif v_aux = 1 then
         fop.block ('Trovat ' || v_aux || ' error.', space_before => 0.5, space_after => 0.5);
      else
         fop.block ('Trovats ' || v_aux || ' errors.', space_before => 0.5, space_after => 0.5);
      end if;
   exception
      when others then
         p (sqlerrm);
   end;

   procedure control_2 (pSemestre in number) is
      v_aux   number := 0;

      cursor lista_errores_2 is
         select distinct ia.asignatura_id, tipo_subgrupo_id, subgrupo_id, grupo_id
         from            hor_items_circuitos ic,
                         hor_items i,
                         hor_items_asignaturas ia
         where           circuito_id = pCircuito
         and             i.id = ic.item_id
         and             ia.item_id = i.id
         and             ia.estudio_id = pEstudio
         and             semestre_id = pSemestre
         and             grupo_id = pGrupo
         and             (asignatura_id, grupo_id, tipo_subgrupo_id) in (
                            select   asignatura_id, grupo_id, tipo_subgrupo_id
                            from     (select distinct ia.asignatura_id, tipo_subgrupo_id, subgrupo_id, grupo_id
                                      from            hor_items_circuitos ic,
                                                      hor_items i,
                                                      hor_items_asignaturas ia
                                      where           circuito_id = pCircuito
                                      and             i.id = ic.item_id
                                      and             ia.item_id = i.id
                                      and             ia.estudio_id = pEstudio
                                      and             semestre_id = pSemestre
                                      and             grupo_id = pGrupo)
                            group by asignatura_id,
                                     grupo_id,
                                     tipo_subgrupo_id
                            having   count (*) > 1)
         order by        1,
                         decode (tipo_subgrupo_id, 'TE', 1, 'PR', 2, 'LA', 3, 'SE', 4, 5),
                         3;
   begin
      fop.block ('Control 2 - Assignatures amb m�s d''un subgrup per tipus al circuit', font_size => 12,
                 border_bottom => 'solid', space_after => 0.1);

      for x in lista_errores_2 loop
         v_aux := v_aux + 1;
         p (x.asignatura_id || ' ' || x.tipo_subgrupo_id || x.subgrupo_id);
      end loop;

      if v_aux = 0 then
         fop.block ('No hi ha cap error.', space_before => 0.5);
      elsif v_aux = 1 then
         fop.block ('Trovat ' || v_aux || ' error.', space_before => 0.5, space_after => 0.5);
      else
         fop.block ('Trovats ' || v_aux || ' errors.', space_before => 0.5, space_after => 0.5);
      end if;
   exception
      when others then
         p (sqlerrm);
   end;
BEGIN
   select count (*)
   into   vSesion
   from   apa_sesiones
   where  sesion_id = pSesion
   and    fecha < sysdate () - 0.33;

   if vSesion > 0 then
      select nombre
      into   v_nombre
      from   hor_estudios
      where  id = pEstudio;

      cabecera;

      for x in lista_semestres loop
         fop.block ('Semestre ' || x.id, font_weight => 'bold', text_align => 'center', font_size => 14,
                    space_after => 0.5);
         control_1 (x.id);
         control_2 (x.id);
      end loop;

      pie;
   else
      cabecera;
      fop.block ('No tens perm�s per accedir a aquest document.', font_weight => 'bold', text_align => 'center',
                 font_size => 14, space_after => 0.5);
      pie;
   end if;
EXCEPTION
   WHEN OTHERS THEN
      htp.p (sqlerrm);
END;


CREATE OR REPLACE PROCEDURE UJI_HORARIOS.HORARIOS_CIRCUITO_PDF (
   name_array    IN   euji_util.ident_arr2,
   value_array   IN   euji_util.ident_arr2
) IS
   -- Parametros
   pEstudio    CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pEstudio');
   pCircuito   CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pCircuito');
   pSesion     CONSTANT VARCHAR2 (4000) := euji_util.euji_getparam (name_array, value_array, 'pSesion');
   pSemestre   CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pSemestre');
   pGrupo      CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pGrupo');
   pSemana     CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pSemana');
   pCurso               number          := 1;
   vCursoAcaNum         number;
   vSesion              number;
   vFechaIniSemestre    date;
   vFechaFinSemestre    date;
   vEsGenerica          boolean         := pSemana = 'G';

     -- Variables
   --  vPerId              per_personas.id%TYPE := euji_get_perid ();
   CURSOR c_items_gen IS
      SELECT asignatura_id, asignatura, estudio, caracter, semestre_id, comun, grupo_id, tipo_subgrupo,
             tipo_subgrupo_id, subgrupo_id, dia_semana_id, hora_inicio, hora_fin, tipo_asignatura_id, tipo_asignatura,
             to_date (dia_semana_id + 1 || '/01/2006 ' || to_char (hora_fin, 'hh24:mi'), 'dd/mm/yyyy hh24:mi') fin,
             to_date (dia_semana_id + 1 || '/01/2006 ' || to_char (hora_inicio, 'hh24:mi'),
                      'dd/mm/yyyy hh24:mi') inicio
      FROM   UJI_HORARIOS.HOR_ITEMS items,
             uji_horarios.hor_items_asignaturas asi,
             uji_horarios.hor_items_circuitos cir
      WHERE  items.id = asi.item_id
      and    cir.item_id = items.id
      and    cir.circuito_id = pCircuito
      and    asi.estudio_id = pEstudio
      AND    items.curso_id = pCurso
      AND    items.grupo_id = pGrupo
      AND    items.semestre_id = pSemestre
      AND    items.dia_semana_id IS NOT NULL;

   CURSOR c_items_det (vIniDate date, vFinDate date) IS
      SELECT   asignatura_id, asignatura, estudio, grupo_id, tipo_subgrupo, tipo_subgrupo_id, subgrupo_id,
               dia_semana_id, hora_inicio, hora_fin, inicio, fin
      FROM     UJI_HORARIOS.HOR_ITEMS items,
               UJI_HORARIOS.HOR_ITEMS_DETALLE det,
               uji_horarios.hor_items_asignaturas asi,
               uji_horarios.hor_items_circuitos cir
      WHERE    items.id = asi.item_id
      and      cir.item_id = items.id
      and      cir.circuito_id = pCircuito
      and      asi.estudio_id = pEstudio
      AND      items.curso_id = pCurso
      AND      items.grupo_id = pGrupo
      AND      items.semestre_id = pSemestre
      AND      items.dia_semana_id IS NOT NULL
      and      det.item_id = items.id
      and      det.inicio > vIniDate
      and      det.fin < vFinDate
      order by det.inicio;

   CURSOR c_asigs is
      SELECT distinct (asignatura_id), asignatura
      FROM            UJI_HORARIOS.HOR_ITEMS items,
                      uji_horarios.hor_items_asignaturas asi,
                      uji_horarios.hor_items_circuitos itemcir
      WHERE           items.id = asi.item_id
      and             items.id = itemcir.item_id
      and             itemcir.circuito_id = pCircuito
      and             asi.estudio_id = pEstudio
      AND             items.curso_id = pCurso
      AND             items.grupo_id = pGrupo
      AND             items.semestre_id = pSemestre
      AND             items.dia_semana_id IS NOT NULL
      order by        asignatura_id;

   CURSOR c_clases_asig (vAsignaturaId varchar2) is
      SELECT   tipo_subgrupo_id || subgrupo_id subgrupo,
               decode (dia_semana_id, 1, 'L', 2, 'M', 3, 'X', 4, 'J', 5, 'V', '?') dia_sem,
               to_char (hora_fin, 'hh24:mi') hora_fin_str, to_char (hora_inicio, 'hh24:mi') hora_inicio_str
      FROM     UJI_HORARIOS.HOR_ITEMS items,
               uji_horarios.hor_items_asignaturas asi,
               uji_horarios.hor_items_circuitos itemcir
      WHERE    items.id = asi.item_id
      and      items.id = itemcir.item_id
      and      itemcir.circuito_id = pCircuito
      and      asi.estudio_id = pEstudio
      AND      items.curso_id = pCurso
      AND      items.grupo_id = pGrupo
      AND      items.semestre_id = pSemestre
      and      asi.asignatura_id = vAsignaturaId
      AND      items.dia_semana_id IS NOT NULL
      order by dia_semana_id,
               hora_inicio,
               subgrupo;

   vFechas              t_horario;
   vRdo                 clob;

   procedure cabecera is
   begin
      owa_util.mime_header (ccontent_type => 'text/xml', bclose_header => true);
      fop.documentOpen (page_width => 29.7, page_height => 21, margin_left => 0.5, margin_right => 0.5,
                        margin_top => 0.5, margin_bottom => 0, extent_before => 0, extent_after => 0.5,
                        extent_margin_top => 0, extent_margin_bottom => 0.5);
      fop.pageBodyOpen;
      fop.documentfooteropen;
      fop.block ('Fecha documento: ' || to_char (sysdate, 'yyyy/mm/dd HH24:MI'), font_size => 6,
                 font_style => 'italic');
      fop.documentfooterclose;
      fop.pageFlowOpen;
   end;

   procedure calcula_curso_aca is
   begin
      SELECT   curso_academico_id
      into     vCursoAcaNum
      FROM     hor_semestres_detalle d,
               hor_estudios e
      WHERE    e.id = pEstudio
      AND      e.tipo_id = d.tipo_estudio_id
      AND      ROWNUM = 1
      ORDER BY fecha_inicio;
   end;

   procedure info_calendario_gen is
      vCursoAca           varchar2 (4000);
      vTitulacionNombre   varchar2 (4000);
      vCircuitoNombre     varchar2 (4000);
      vCursoNombre        varchar2 (4000) := pCurso || '�';
      vTextoCabecera      varchar2 (4000);
   begin
      select nombre
      into   vTitulacionNombre
      from   hor_estudios
      where  id = pEstudio;

      select nombre
      into   vCircuitoNombre
      from   hor_circuitos
      where  id = pCircuito;

      vCursoAca := to_char (vCursoAcaNum) || '/' || to_char (vCursoAcaNum + 1);

      if pCurso in ('1', '3') then
         vCursoNombre := pCurso || 'er';
      elsif pCurso = '2' then
         vCursoNombre := '2on';
      elsif pCurso = '4' then
         vCursoNombre := '4t';
      else
         vCursoNombre := pCurso || '�';
      end if;

      vTextoCabecera :=
         vTitulacionNombre || ' - Circuit ' || vCircuitoNombre || ' - ' || vCursoNombre || ' curs - Grup ' || pGrupo
         || ' (semestre ' || pSemestre || ') - ' || vCursoAca;
      fop.block (text => vTextoCabecera, border_bottom => 'solid', border_width => 0.05);
   end;

   procedure muestra_leyenda_asig (item c_asigs%ROWTYPE) is
      vClasesText   varchar2 (4000);
   begin
      fop.block (item.asignatura_id || ', ' || item.asignatura || ':', font_size => 7, padding_top => 0.1);
      vClasesText := '<fo:inline color="white">___</fo:inline>';

      for c in c_clases_asig (item.asignatura_id) loop
         vClasesText :=
            vClasesText || c.dia_sem || ', ' || c.hora_inicio_str || '-' || c.hora_fin_str || ' (' || c.subgrupo
            || '); ';
      end loop;

      vClasesText := substr (vClasesText, 1, length (vClasesText) - 2);
      fop.block (vClasesText, font_size => 7, padding_top => 0.06);
   end;

   procedure leyenda_asigs is
   begin
      fop.block (' ', padding_top => 1);

      for item in c_asigs loop
         muestra_leyenda_asig (item);
      end loop;
   end;

   procedure pie is
   begin
      fop.pageFlowClose;
      fop.pageBodyClose;
      fop.documentClose;
   end;

   procedure calendario_gen is
   begin
      vFechas := t_horario ();

      for item in c_items_gen loop
         vFechas.extend;
         vFechas (vFechas.count) :=
            ItemHorarios (hor_contenido_item (item.hora_inicio, item.hora_fin, item.asignatura_id,
                                              '(' || item.tipo_subgrupo_id || item.subgrupo_id || ')'),
                          item.inicio, item.fin, null);
      end loop;

      if vFechas.count > 0 then
         vRdo := xpfpkg_horarios_academicos.drawTimetable (vFechas, 5, null, 'CA', 'PDF_APP_HORARIOS');
         xup_utils.dump (vRdo);
         dbms_lob.freeTemporary (vRdo);
      end if;
   end;

   procedure calendario_det (vFechaIni date, vFechaFin date) is
   begin
      vFechas := t_horario ();

      for item in c_items_det (vFechaIni, vFechaFin) loop
         vFechas.extend;
         vFechas (vFechas.count) :=
            ItemHorarios (hor_contenido_item (item.hora_inicio, item.hora_fin, item.asignatura_id,
                                              '(' || item.tipo_subgrupo_id || item.subgrupo_id || ')'),
                          item.inicio, item.fin, null);
      end loop;

      if vFechas.count > 0 then
         vRdo := xpfpkg_horarios_academicos.drawTimetable (vFechas, 5, null, 'CA', 'PDF_APP_HORARIOS_DET');
         xup_utils.dump (vRdo);
         dbms_lob.freeTemporary (vRdo);
      end if;
   end;

   procedure calcula_fechas_semestre is
   begin
      SELECT fecha_inicio, fecha_examenes_fin
      into   vFechaIniSemestre, vFechaFinSemestre
      FROM   hor_semestres_detalle,
             hor_estudios e
      WHERE  semestre_id = pSemestre
      and    e.id = pEstudio
      and    e.tipo_id = tipo_estudio_id;
   end;

   function semana_tiene_clase (vIniDate date, vFinDate date)
      return boolean is
      num_clases   number;
   begin
      SELECT   count (*)
      into     num_clases
      FROM     UJI_HORARIOS.HOR_ITEMS items,
               UJI_HORARIOS.HOR_ITEMS_DETALLE det,
               uji_horarios.hor_items_asignaturas asi
      WHERE    items.id = asi.item_id
      and      asi.estudio_id = pEstudio
      AND      items.curso_id = pCurso
      AND      items.grupo_id = pGrupo
      AND      items.semestre_id = pSemestre
      AND      items.dia_semana_id IS NOT NULL
      and      det.item_id = items.id
      and      det.inicio > vIniDate
      and      det.fin < vFinDate
      order by det.inicio;

      return num_clases > 0;
   end;

   procedure info_calendario_det (vFechaIni date, vFechaFin date) is
      vCursoAca           varchar2 (4000);
      vTitulacionNombre   varchar2 (4000);
      vCursoNombre        varchar2 (4000) := pCurso || '�';
      vCircuitoNombre     varchar2 (4000);
      vTextoCabecera      varchar2 (4000);
      vFechaIniStr        varchar2 (4000) := to_char (vFechaIni, 'dd/mm/yyyy');
      vFechaFinStr        varchar2 (4000) := to_char (vFechaFin - 1, 'dd/mm/yyyy');
   begin
      select nombre
      into   vTitulacionNombre
      from   hor_estudios
      where  id = pEstudio;

      select nombre
      into   vCircuitoNombre
      from   hor_circuitos
      where  id = pCircuito;

      vCursoAca := to_char (vCursoAcaNum) || '/' || to_char (vCursoAcaNum + 1);

      if pCurso in ('1', '3') then
         vCursoNombre := pCurso || 'er';
      elsif pCurso = '2' then
         vCursoNombre := '2on';
      elsif pCurso = '4' then
         vCursoNombre := '4t';
      else
         vCursoNombre := pCurso || '�';
      end if;

      vTextoCabecera :=
         vTitulacionNombre || ' - Circuit ' || vCircuitoNombre || ' - ' || vCursoNombre || ' curs - Grup ' || pGrupo
         || ' (semestre ' || pSemestre || ') - Del ' || vFechaIniStr || ' al ' || vFechaFinStr;
      fop.block (text => vTextoCabecera, border_bottom => 'solid', border_width => 0.05);
   end;

   procedure paginas_calendario_det is
      vFechaIniSemana   date;
      vFechaFinSemana   date;
   begin
      calcula_fechas_semestre;
      vFechaIniSemana := vFechaIniSemestre;
      vFechaFinSemana := vFechaIniSemana + 7;

      while vFechaFinSemana <= vFechaFinSemestre loop
         if semana_tiene_clase (vFechaIniSemana, vFechaFinSemana) then
            info_calendario_det (vFechaIniSemana, vFechaFinSemana);
            calendario_det (vFechaIniSemana, vFechaFinSemana);
            htp.p ('<fo:block break-before="page" color="white">_</fo:block>');
         end if;

         vFechaIniSemana := vFechaFinSemana;
         vFechaFinSemana := vFechaFinSemana + 7;
      end loop;
   end;
BEGIN
   select count (*)
   into   vSesion
   from   apa_sesiones
   where  sesion_id = pSesion
   and    fecha < sysdate () - 0.33;

   if vSesion > 0 then
      calcula_curso_aca;
      cabecera;

      if vEsGenerica then
         info_calendario_gen;
         calendario_gen;
         leyenda_asigs;
      else
         paginas_calendario_det;
         leyenda_asigs;
      end if;

      pie;
   else
      cabecera;
      fop.block ('No tens perm�s per accedir a aquest document.', font_weight => 'bold', text_align => 'center',
                 font_size => 14, space_after => 0.5);
      pie;
   end if;
EXCEPTION
   WHEN OTHERS THEN
      htp.p (sqlerrm);
END;


CREATE OR REPLACE PROCEDURE UJI_HORARIOS.horarios_ocupacion_pdf (
   name_array    IN   euji_util.ident_arr2,
   value_array   IN   euji_util.ident_arr2
) IS
   pAula       CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pAula');
   pSemestre   CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pSemestre');
   pSesion     CONSTANT VARCHAR2 (4000) := euji_util.euji_getparam (name_array, value_array, 'pSesion');
   pSemana     CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pSemana');
   vCursoAcaNum         number;
   vFechaIniSemestre    date;
   vFechaFinSemestre    date;
   vSesion              number;
   vEsGenerica          boolean         := pSemana = 'G';
   vFechas              t_horario;
   vRdo                 clob;

   CURSOR c_items_det (vIniDate date, vFinDate date) IS
      SELECT   items.id, items.grupo_id, items.tipo_subgrupo, items.tipo_subgrupo_id, items.subgrupo_id,
               items.dia_semana_id, items.hora_inicio, items.hora_fin, det.inicio, det.fin,
               rtrim (xmlagg (xmlelement (e, asi.asignatura_id || ', ')).extract ('//text()'), ', ') as asignatura_id
      FROM     UJI_HORARIOS.HOR_ITEMS items,
               UJI_HORARIOS.HOR_ITEMS_DETALLE det,
               uji_horarios.hor_items_asignaturas asi,
               hor_aulas_planificacion p
      WHERE    items.id = asi.item_id
      AND      items.aula_planificacion_id = p.id
      AND      p.aula_id = pAula
      AND      p.semestre_id = pSemestre
      AND      items.dia_semana_id IS NOT NULL
      and      det.item_id = items.id
      and      det.inicio > vIniDate
      and      det.fin < vFinDate
      group by items.id,
               items.grupo_id,
               items.tipo_subgrupo,
               items.tipo_subgrupo_id,
               items.subgrupo_id,
               items.dia_semana_id,
               items.hora_inicio,
               items.hora_fin,
               det.inicio,
               det.fin
      order by det.inicio;

   CURSOR c_items_gen IS
      SELECT   items.id, items.grupo_id, items.tipo_subgrupo, items.tipo_subgrupo_id, items.subgrupo_id,
               items.dia_semana_id, items.hora_inicio, items.hora_fin,
               rtrim (xmlagg (xmlelement (e, asi.asignatura_id || ', ')).extract ('//text()'), ', ') as asignatura_id,
               to_date (dia_semana_id + 1 || '/01/2006 ' || to_char (hora_fin, 'hh24:mi'), 'dd/mm/yyyy hh24:mi') fin,
               to_date (dia_semana_id + 1 || '/01/2006 ' || to_char (hora_inicio, 'hh24:mi'),
                        'dd/mm/yyyy hh24:mi') inicio
      FROM     UJI_HORARIOS.HOR_ITEMS items,
               uji_horarios.hor_items_asignaturas asi,
               hor_aulas_planificacion p
      WHERE    items.id = asi.item_id
      AND      items.aula_planificacion_id = p.id
      AND      p.aula_id = pAula
      AND      p.semestre_id = pSemestre
      AND      items.dia_semana_id IS NOT NULL
      group by items.id,
               items.grupo_id,
               items.tipo_subgrupo,
               items.tipo_subgrupo_id,
               items.subgrupo_id,
               items.dia_semana_id,
               items.hora_inicio,
               items.hora_fin
      order by items.hora_inicio;

   CURSOR c_asigs is
      SELECT distinct (asignatura_id), asignatura
      FROM            UJI_HORARIOS.HOR_ITEMS items,
                      uji_horarios.hor_items_asignaturas asi,
                      hor_aulas_planificacion p
      WHERE           items.id = asi.item_id
      AND             items.aula_planificacion_id = p.id
      AND             p.aula_id = pAula
      AND             p.semestre_id = pSemestre
      AND             items.dia_semana_id IS NOT NULL
      order by        asignatura_id;

   CURSOR c_clases_asig (vAsignaturaId varchar2) is
      SELECT   tipo_subgrupo_id || subgrupo_id subgrupo,
               decode (dia_semana_id, 1, 'L', 2, 'M', 3, 'X', 4, 'J', 5, 'V', '?') dia_sem,
               to_char (hora_fin, 'hh24:mi') hora_fin_str, to_char (hora_inicio, 'hh24:mi') hora_inicio_str
      FROM     UJI_HORARIOS.HOR_ITEMS items,
               uji_horarios.hor_items_asignaturas asi,
               hor_aulas_planificacion p
      WHERE    items.id = asi.item_id
      AND      items.aula_planificacion_id = p.id
      AND      p.aula_id = pAula
      AND      items.semestre_id = pSemestre
      and      asi.asignatura_id = vAsignaturaId
      AND      items.dia_semana_id IS NOT NULL
      order by dia_semana_id,
               hora_inicio,
               subgrupo;

   function semana_tiene_clase (vIniDate date, vFinDate date)
      return boolean is
      num_clases   number;
   begin
      SELECT count (*)
      into   num_clases
      FROM   UJI_HORARIOS.HOR_ITEMS items,
             UJI_HORARIOS.HOR_ITEMS_DETALLE det,
             hor_aulas_planificacion p
      WHERE  items.aula_planificacion_id = p.id
      AND    p.aula_id = pAula
      AND    p.semestre_id = pSemestre
      AND    items.dia_semana_id IS NOT NULL
      and    det.item_id = items.id
      and    det.inicio > vIniDate
      and    det.fin < vFinDate;

      return num_clases > 0;
   end;

   procedure calcula_curso_aca is
   begin
      SELECT distinct (curso_academico_id)
      into            vCursoAcaNum
      FROM            hor_semestres_detalle d,
                      hor_estudios e
      WHERE           ROWNUM = 1
      ORDER BY        curso_academico_id;
   end;

   procedure calcula_fechas_semestre is
   begin
      SELECT fecha_inicio, fecha_examenes_fin
      into   vFechaIniSemestre, vFechaFinSemestre
      FROM   hor_semestres_detalle,
             hor_estudios e
      WHERE  semestre_id = pSemestre
      and    rownum = 1;
   end;

   procedure calendario_det (vFechaIni date, vFechaFin date) is
   begin
      vFechas := t_horario ();

      for item in c_items_det (vFechaIni, vFechaFin) loop
         vFechas.extend;
         vFechas (vFechas.count) :=
            ItemHorarios (hor_contenido_item (item.hora_inicio, item.hora_fin,
                                              item.asignatura_id || ' ' || item.grupo_id,
                                              '(' || item.tipo_subgrupo_id || item.subgrupo_id || ')'),
                          item.inicio, item.fin, null);
      end loop;

      if vFechas.count > 0 then
         vRdo := xpfpkg_horarios_academicos.drawTimetable (vFechas, 5, null, 'CA', 'PDF_APP_HORARIOS_DET');
         xup_utils.dump (vRdo);
         dbms_lob.freeTemporary (vRdo);
      end if;
   end;

   procedure calendario_gen is
   begin
      vFechas := t_horario ();

      for item in c_items_gen loop
         vFechas.extend;
         vFechas (vFechas.count) :=
            ItemHorarios (hor_contenido_item (item.hora_inicio, item.hora_fin,
                                              item.asignatura_id || ' ' || item.grupo_id,
                                              '(' || item.tipo_subgrupo_id || item.subgrupo_id || ')'),
                          item.inicio, item.fin, null);
      end loop;

      if vFechas.count > 0 then
         vRdo := xpfpkg_horarios_academicos.drawTimetable (vFechas, 5, null, 'CA', 'PDF_APP_HORARIOS');
         xup_utils.dump (vRdo);
         dbms_lob.freeTemporary (vRdo);
      end if;
   end;

   procedure info_calendario_det (vFechaIni date, vFechaFin date) is
      vCursoAca        varchar2 (4000);
      vTextoCabecera   varchar2 (4000);
      vFechaIniStr     varchar2 (4000) := to_char (vFechaIni, 'dd/mm/yyyy');
      vFechaFinStr     varchar2 (4000) := to_char (vFechaFin - 1, 'dd/mm/yyyy');
      vAulaNombre      varchar2 (4000);
      vEdificio        varchar2 (4000);
      vPlanta          varchar2 (4000);
      vCentroNombre    varchar2 (4000);
   begin
      vCursoAca := to_char (vCursoAcaNum) || '/' || to_char (vCursoAcaNum + 1);

      SELECT a.nombre AS aulaNombre, a.edificio, a.planta, c.nombre AS centroNombre
      INTO   vAulaNombre, vEdificio, vPlanta, vCentroNombre
      FROM   hor_aulas a,
             hor_centros c
      WHERE  a.id = pAula
      AND    c.id = a.centro_id;

      vTextoCabecera :=
         vAulaNombre || ' - ' || vCentroNombre || ' - Edifici ' || vEdificio || ' - Planta ' || vPlanta
         || '  - (semestre ' || pSemestre || ') - Del ' || vFechaIniStr || ' al ' || vFechaFinStr;
      fop.block (text => vTextoCabecera, border_bottom => 'solid', border_width => 0.05);
   end;

   procedure info_calendario_gen is
      vCursoAca        varchar2 (4000);
      vTextoCabecera   varchar2 (4000);
      vAulaNombre      varchar2 (4000);
      vEdificio        varchar2 (4000);
      vPlanta          varchar2 (4000);
      vCentroNombre    varchar2 (4000);
   begin
      vCursoAca := to_char (vCursoAcaNum) || '/' || to_char (vCursoAcaNum + 1);

      SELECT a.nombre AS aulaNombre, a.edificio, a.planta, c.nombre AS centroNombre
      INTO   vAulaNombre, vEdificio, vPlanta, vCentroNombre
      FROM   hor_aulas a,
             hor_centros c
      WHERE  a.id = pAula
      AND    c.id = a.centro_id;

      vTextoCabecera :=
         vAulaNombre || ' - ' || vCentroNombre || ' - Edifici ' || vEdificio || ' - Planta ' || vPlanta
         || '  - (semestre ' || pSemestre || ') ';
      fop.block (text => vTextoCabecera, border_bottom => 'solid', border_width => 0.05);
   end;

   procedure paginas_calendario_det is
      vFechaIniSemana   date;
      vFechaFinSemana   date;
   begin
      calcula_fechas_semestre;
      vFechaIniSemana := vFechaIniSemestre;
      vFechaFinSemana := vFechaIniSemana + 7;

      while vFechaFinSemana <= vFechaFinSemestre loop
         if semana_tiene_clase (vFechaIniSemana, vFechaFinSemana) then
            info_calendario_det (vFechaIniSemana, vFechaFinSemana);
            calendario_det (vFechaIniSemana, vFechaFinSemana);
            htp.p ('<fo:block break-before="page" color="white">_</fo:block>');
         end if;

         vFechaIniSemana := vFechaFinSemana;
         vFechaFinSemana := vFechaFinSemana + 7;
      end loop;
   end;

   procedure cabecera is
   begin
      owa_util.mime_header (ccontent_type => 'text/xml', bclose_header => true);
      fop.documentOpen (page_width => 29.7, page_height => 21, margin_left => 0.5, margin_right => 0.5,
                        margin_top => 0.5, margin_bottom => 0, extent_before => 0, extent_after => 0.5,
                        extent_margin_top => 0, extent_margin_bottom => 0.5);
      fop.pageBodyOpen;
      fop.documentfooteropen;
      fop.block ('Fecha documento: ' || to_char (sysdate, 'yyyy/mm/dd HH24:MI'), font_size => 6,
                 font_style => 'italic');
      fop.documentfooterclose;
      fop.pageFlowOpen;
   end;

   procedure muestra_leyenda_asig (item c_asigs%ROWTYPE) is
      vClasesText   varchar2 (4000);
   begin
      fop.block (item.asignatura_id || ', ' || item.asignatura || ':', font_size => 7, padding_top => 0.1);
      vClasesText := '<fo:inline color="white">___</fo:inline>';

      for c in c_clases_asig (item.asignatura_id) loop
         vClasesText :=
            vClasesText || c.dia_sem || ', ' || c.hora_inicio_str || '-' || c.hora_fin_str || ' (' || c.subgrupo
            || '); ';
      end loop;

      vClasesText := substr (vClasesText, 1, length (vClasesText) - 2);
      fop.block (vClasesText, font_size => 7, padding_top => 0.06);
   end;

   procedure leyenda_asigs is
   begin
      fop.block (' ', padding_top => 1);

      for item in c_asigs loop
         muestra_leyenda_asig (item);
      end loop;
   end;

   procedure pie is
   begin
      fop.pageFlowClose;
      fop.pageBodyClose;
      fop.documentClose;
   end;
BEGIN
   select count (*)
   into   vSesion
   from   apa_sesiones
   where  sesion_id = pSesion
   and    fecha < sysdate () - 0.33;

   if vSesion > 0 then
      calcula_curso_aca;
      cabecera;

      if vEsGenerica then
         info_calendario_gen;
         calendario_gen;
         leyenda_asigs;
      else
         paginas_calendario_det;
         leyenda_asigs;
      end if;

      pie;
   else
      cabecera;
      fop.block ('No tens perm�s per accedir a aquest document.', font_weight => 'bold', text_align => 'center',
                 font_size => 14, space_after => 0.5);
      pie;
   end if;
EXCEPTION
   WHEN OTHERS THEN
      htp.p (sqlerrm);
END;


CREATE OR REPLACE PROCEDURE UJI_HORARIOS.HORARIOS_SEMANA_PDF (
   name_array    IN   euji_util.ident_arr2,
   value_array   IN   euji_util.ident_arr2
) IS
   -- Parametros
   pEstudio    CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pEstudio');
   pSemestre   CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pSemestre');
   pSesion     CONSTANT VARCHAR2 (4000) := euji_util.euji_getparam (name_array, value_array, 'pSesion');
   pGrupo      CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pGrupo');
   pCurso      CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pCurso');
   pSemana     CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pSemana');
   vCursoAcaNum         number;
   vSesion              number;
   vFechaIniSemestre    date;
   vFechaFinSemestre    date;
   vEsGenerica          boolean         := pSemana = 'G';

     -- Variables
   --  vPerId              per_personas.id%TYPE := euji_get_perid ();
   CURSOR c_items_gen IS
      SELECT asignatura_id, asignatura, estudio, caracter, semestre_id, comun, grupo_id, tipo_subgrupo,
             tipo_subgrupo_id, subgrupo_id, dia_semana_id, hora_inicio, hora_fin, tipo_asignatura_id, tipo_asignatura,
             to_date (dia_semana_id + 1 || '/01/2006 ' || to_char (hora_fin, 'hh24:mi'), 'dd/mm/yyyy hh24:mi') fin,
             to_date (dia_semana_id + 1 || '/01/2006 ' || to_char (hora_inicio, 'hh24:mi'),
                      'dd/mm/yyyy hh24:mi') inicio
      FROM   UJI_HORARIOS.HOR_ITEMS items,
             uji_horarios.hor_items_asignaturas asi
      WHERE  items.id = asi.item_id
      and    asi.estudio_id = pEstudio
      AND    items.curso_id = pCurso
      AND    items.grupo_id = pGrupo
      AND    items.semestre_id = pSemestre
      AND    items.dia_semana_id IS NOT NULL;

   CURSOR c_items_det (vIniDate date, vFinDate date) IS
      SELECT   asignatura_id, asignatura, estudio, grupo_id, tipo_subgrupo, tipo_subgrupo_id, subgrupo_id,
               dia_semana_id, hora_inicio, hora_fin, inicio, fin
      FROM     UJI_HORARIOS.HOR_ITEMS items,
               UJI_HORARIOS.HOR_ITEMS_DETALLE det,
               uji_horarios.hor_items_asignaturas asi
      WHERE    items.id = asi.item_id
      and      asi.estudio_id = pEstudio
      AND      items.curso_id = pCurso
      AND      items.grupo_id = pGrupo
      AND      items.semestre_id = pSemestre
      AND      items.dia_semana_id IS NOT NULL
      and      det.item_id = items.id
      and      det.inicio > vIniDate
      and      det.fin < vFinDate
      order by det.inicio;

   CURSOR c_asigs is
      SELECT distinct (asignatura_id), asignatura
      FROM            UJI_HORARIOS.HOR_ITEMS items,
                      uji_horarios.hor_items_asignaturas asi
      WHERE           items.id = asi.item_id
      and             asi.estudio_id = pEstudio
      AND             items.curso_id = pCurso
      AND             items.grupo_id = pGrupo
      AND             items.semestre_id = pSemestre
      AND             items.dia_semana_id IS NOT NULL
      order by        asignatura_id;

   CURSOR c_clases_asig (vAsignaturaId varchar2) is
      SELECT   tipo_subgrupo_id || subgrupo_id subgrupo,
               decode (dia_semana_id, 1, 'L', 2, 'M', 3, 'X', 4, 'J', 5, 'V', '?') dia_sem,
               to_char (hora_fin, 'hh24:mi') hora_fin_str, to_char (hora_inicio, 'hh24:mi') hora_inicio_str
      FROM     UJI_HORARIOS.HOR_ITEMS items,
               uji_horarios.hor_items_asignaturas asi
      WHERE    items.id = asi.item_id
      and      asi.estudio_id = pEstudio
      AND      items.curso_id = pCurso
      AND      items.grupo_id = pGrupo
      AND      items.semestre_id = pSemestre
      and      asi.asignatura_id = vAsignaturaId
      AND      items.dia_semana_id IS NOT NULL
      order by dia_semana_id,
               hora_inicio,
               subgrupo;

   vFechas              t_horario;
   vRdo                 clob;

   procedure cabecera is
   begin
      owa_util.mime_header (ccontent_type => 'text/xml', bclose_header => true);
      fop.documentOpen (page_width => 29.7, page_height => 21, margin_left => 0.5, margin_right => 0.5,
                        margin_top => 0.5, margin_bottom => 0, extent_before => 0, extent_after => 0.5,
                        extent_margin_top => 0, extent_margin_bottom => 0.5);
      fop.pageBodyOpen;
      fop.documentfooteropen;
      fop.block ('Fecha documento: ' || to_char (sysdate, 'yyyy/mm/dd HH24:MI'), font_size => 6,
                 font_style => 'italic');
      fop.documentfooterclose;
      fop.pageFlowOpen;
   end;

   procedure calcula_curso_aca is
   begin
      SELECT   curso_academico_id
      into     vCursoAcaNum
      FROM     hor_semestres_detalle d,
               hor_estudios e
      WHERE    e.id = pEstudio
      AND      e.tipo_id = d.tipo_estudio_id
      AND      ROWNUM = 1
      ORDER BY fecha_inicio;
   end;

   procedure info_calendario_gen is
      vCursoAca           varchar2 (4000);
      vTitulacionNombre   varchar2 (4000);
      vCursoNombre        varchar2 (4000) := pCurso || '�';
      vTextoCabecera      varchar2 (4000);
   begin
      select nombre
      into   vTitulacionNombre
      from   hor_estudios
      where  id = pEstudio;

      vCursoAca := to_char (vCursoAcaNum) || '/' || to_char (vCursoAcaNum + 1);

      if pCurso in ('1', '3') then
         vCursoNombre := pCurso || 'er';
      elsif pCurso = '2' then
         vCursoNombre := '2on';
      elsif pCurso = '4' then
         vCursoNombre := '4t';
      else
         vCursoNombre := pCurso || '�';
      end if;

      vTextoCabecera :=
         vTitulacionNombre || ' - ' || vCursoNombre || ' curs - Grup ' || pGrupo || ' (semestre ' || pSemestre || ') - '
         || vCursoAca;
      fop.block (text => vTextoCabecera, border_bottom => 'solid', border_width => 0.05);
   end;

   procedure muestra_leyenda_asig (item c_asigs%ROWTYPE) is
      vClasesText   varchar2 (4000);
   begin
      fop.block (item.asignatura_id || ', ' || item.asignatura || ':', font_size => 7, padding_top => 0.1);
      vClasesText := '<fo:inline color="white">___</fo:inline>';

      for c in c_clases_asig (item.asignatura_id) loop
         vClasesText :=
            vClasesText || c.dia_sem || ', ' || c.hora_inicio_str || '-' || c.hora_fin_str || ' (' || c.subgrupo
            || '); ';
      end loop;

      vClasesText := substr (vClasesText, 1, length (vClasesText) - 2);
      fop.block (vClasesText, font_size => 7, padding_top => 0.06);
   end;

   procedure leyenda_asigs is
   begin
      fop.block (' ', padding_top => 1);

      for item in c_asigs loop
         muestra_leyenda_asig (item);
      end loop;
   end;

   procedure pie is
   begin
      fop.pageFlowClose;
      fop.pageBodyClose;
      fop.documentClose;
   end;

   procedure calendario_gen is
   begin
      vFechas := t_horario ();

      for item in c_items_gen loop
         vFechas.extend;
         vFechas (vFechas.count) :=
            ItemHorarios (hor_contenido_item (item.hora_inicio, item.hora_fin, item.asignatura_id,
                                              '(' || item.tipo_subgrupo_id || item.subgrupo_id || ')'),
                          item.inicio, item.fin, null);
      end loop;

      if vFechas.count > 0 then
         vRdo := xpfpkg_horarios_academicos.drawTimetable (vFechas, 5, null, 'CA', 'PDF_APP_HORARIOS');
         xup_utils.dump (vRdo);
         dbms_lob.freeTemporary (vRdo);
      end if;
   end;

   procedure calendario_det (vFechaIni date, vFechaFin date) is
   begin
      vFechas := t_horario ();

      for item in c_items_det (vFechaIni, vFechaFin) loop
         vFechas.extend;
         vFechas (vFechas.count) :=
            ItemHorarios (hor_contenido_item (item.hora_inicio, item.hora_fin, item.asignatura_id,
                                              '(' || item.tipo_subgrupo_id || item.subgrupo_id || ')'),
                          item.inicio, item.fin, null);
      end loop;

      if vFechas.count > 0 then
         vRdo := xpfpkg_horarios_academicos.drawTimetable (vFechas, 5, null, 'CA', 'PDF_APP_HORARIOS_DET');
         xup_utils.dump (vRdo);
         dbms_lob.freeTemporary (vRdo);
      end if;
   end;

   procedure calcula_fechas_semestre is
   begin
      SELECT fecha_inicio, fecha_examenes_fin
      into   vFechaIniSemestre, vFechaFinSemestre
      FROM   hor_semestres_detalle,
             hor_estudios e
      WHERE  semestre_id = pSemestre
      and    e.id = pEstudio
      and    e.tipo_id = tipo_estudio_id;
   end;

   function semana_tiene_clase (vIniDate date, vFinDate date)
      return boolean is
      num_clases   number;
   begin
      SELECT   count (*)
      into     num_clases
      FROM     UJI_HORARIOS.HOR_ITEMS items,
               UJI_HORARIOS.HOR_ITEMS_DETALLE det,
               uji_horarios.hor_items_asignaturas asi
      WHERE    items.id = asi.item_id
      and      asi.estudio_id = pEstudio
      AND      items.curso_id = pCurso
      AND      items.grupo_id = pGrupo
      AND      items.semestre_id = pSemestre
      AND      items.dia_semana_id IS NOT NULL
      and      det.item_id = items.id
      and      det.inicio > vIniDate
      and      det.fin < vFinDate
      order by det.inicio;

      return num_clases > 0;
   end;

   procedure info_calendario_det (vFechaIni date, vFechaFin date) is
      vCursoAca           varchar2 (4000);
      vTitulacionNombre   varchar2 (4000);
      vCursoNombre        varchar2 (4000) := pCurso || '�';
      vTextoCabecera      varchar2 (4000);
      vFechaIniStr        varchar2 (4000) := to_char (vFechaIni, 'dd/mm/yyyy');
      vFechaFinStr        varchar2 (4000) := to_char (vFechaFin - 1, 'dd/mm/yyyy');
   begin
      select nombre
      into   vTitulacionNombre
      from   hor_estudios
      where  id = pEstudio;

      vCursoAca := to_char (vCursoAcaNum) || '/' || to_char (vCursoAcaNum + 1);

      if pCurso in ('1', '3') then
         vCursoNombre := pCurso || 'er';
      elsif pCurso = '2' then
         vCursoNombre := '2on';
      elsif pCurso = '4' then
         vCursoNombre := '4t';
      else
         vCursoNombre := pCurso || '�';
      end if;

      vTextoCabecera :=
         vTitulacionNombre || ' - ' || vCursoNombre || ' curs - Grup ' || pGrupo || ' (semestre ' || pSemestre
         || ') - Del ' || vFechaIniStr || ' al ' || vFechaFinStr;
      fop.block (text => vTextoCabecera, border_bottom => 'solid', border_width => 0.05);
   end;

   procedure paginas_calendario_det is
      vFechaIniSemana   date;
      vFechaFinSemana   date;
   begin
      calcula_fechas_semestre;
      vFechaIniSemana := vFechaIniSemestre;
      vFechaFinSemana := vFechaIniSemana + 7;

      while vFechaFinSemana <= vFechaFinSemestre loop
         if semana_tiene_clase (vFechaIniSemana, vFechaFinSemana) then
            info_calendario_det (vFechaIniSemana, vFechaFinSemana);
            calendario_det (vFechaIniSemana, vFechaFinSemana);
            htp.p ('<fo:block break-before="page" color="white">_</fo:block>');
         end if;

         vFechaIniSemana := vFechaFinSemana;
         vFechaFinSemana := vFechaFinSemana + 7;
      end loop;
   end;
BEGIN
   --euji_control_acceso (vItem);
   select count (*)
   into   vSesion
   from   apa_sesiones
   where  sesion_id = pSesion
   and    fecha < sysdate () - 0.33;

   if vSesion > 0 then
      calcula_curso_aca;
      cabecera;

      if vEsGenerica then
         info_calendario_gen;
         calendario_gen;
         leyenda_asigs;
      else
         paginas_calendario_det;
         leyenda_asigs;
      end if;

      pie;
   else
      cabecera;
      fop.block ('No tens perm�s per accedir a aquest document.', font_weight => 'bold', text_align => 'center',
                 font_size => 14, space_after => 0.5);
      pie;
   end if;
EXCEPTION
   WHEN OTHERS THEN
      htp.p (sqlerrm);
END;




CREATE OR REPLACE PROCEDURE UJI_HORARIOS.HORARIOS_VALIDACION_PDF (
   name_array    IN   euji_util.ident_arr2,
   value_array   IN   euji_util.ident_arr2
) IS
   -- Parametros
   pEstudio    CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pEstudio');
   pSemestre   CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pSemestre');
   pGrupo      CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pGrupo');
   pCurso      CONSTANT VARCHAR2 (40)   := euji_util.euji_getparam (name_array, value_array, 'pCurso');
   pSesion     CONSTANT VARCHAR2 (400)  := euji_util.euji_getparam (name_array, value_array, 'pSesion');
   v_nombre             varchar2 (2000);
   vSesion              number;

   procedure cabecera is
   begin
      owa_util.mime_header (ccontent_type => 'text/xml', bclose_header => true);
      fop.documentOpen (margin_left => 1.5, margin_right => 1.5, margin_top => 0.5, margin_bottom => 0.5,
                        extent_before => 3, extent_after => 1.5, extent_margin_top => 3, extent_margin_bottom => 1.5);
      fop.pageBodyOpen;
      htp.p (fof.documentheaderopen || fof.blockopen (space_after => 0.1) || fof.tableOpen
             || fof.tablecolumndefinition (column_width => 3) || fof.tablecolumndefinition (column_width => 15)
             || fof.tablebodyopen || fof.tableRowOpen || fof.tablecellopen
             || fof.block (fof.logo_uji, text_align => 'center') || fof.tablecellclose
             || fof.tablecellopen (display_align => 'center', padding => 0.4)
             || fof.block (text => 'Controls horaris', font_size => 12, font_weight => 'bold', font_family => 'Arial',
                           text_align => 'center')
             || fof.block (text => v_nombre, font_size => 12, font_weight => 'bold', font_family => 'Arial',
                           text_align => 'center')
             || fof.block (text => 'Curs: ' || pCurso || ' - Semestre: ' || pSemestre || ' - Grup: ' || pGrupo,
                           font_size => 12, font_weight => 'bold', font_family => 'Arial', text_align => 'center')
             || fof.tablecellclose || fof.tableRowClose || fof.tablebodyclose || fof.tableClose || fof.blockclose
             || fof.documentheaderclose);
      -- El numero paginas se escribe a mano para poner el texto m?s peque?o
      htp.p (fof.documentfooteropen || fof.tableOpen || fof.tablecolumndefinition (column_width => 18)
             || fof.tablebodyopen || fof.tableRowOpen || fof.tablecellopen
             || fof.block (text => 'Pag. <fo:page-number /> de <fo:page-number-citation ref-id="lastBlock" />',
                           text_align => 'right', font_weight => 'bold', font_size => 8)
             || fof.tablecellclose || fof.tableRowClose || fof.tablebodyclose || fof.tableClose
             || fof.documentfooterclose);
      fop.pageFlowOpen;
      fop.block (fof.white_space);
   end;

   procedure pie is
   begin
      htp.p ('<fo:block  id="lastBlock" space-before="1cm" font-size="8pt">');
      htp.p ('Data de generaci�: ' || to_char (sysdate, 'dd/mm/yyy hh24:mi'));
      fop.blockclose ();
      fop.pageFlowClose;
      fop.pageBodyClose;
      fop.documentClose;
   end;

   function espacios (p_espacios in number)
      return varchar2 is
      v_rdo   varchar2 (2000);
   begin
      v_rdo := null;

      for x in 1 .. p_espacios loop
         v_rdo := v_rdo || fof.white_space;
      end loop;

      return v_rdo;
   end;

   procedure p (p_texto in varchar2) is
   begin
      fop.block (espacios (3) || p_texto);
   end;

   procedure control_1 is
      v_aux   number := 0;

      cursor lista_errores_1 is
         select distinct asignatura_id, tipo_subgrupo_id || subgrupo_id subgrupo,
                         decode (tipo_subgrupo_id, 'TE', 1, 'PR', 2, 'LA', 3, 'SE', 4, 0) tipo
         from            uji_horarios.hor_items i,
                         hor_items_asignaturas asi
         where           i.id = asi.item_id
         and             curso_id = pCurso
         and             semestre_id = pSemestre
         and             grupo_id = pGrupo
         and             tipo_subgrupo_id not in ('AV', 'TU')
         and             dia_semana_id is null
         and             estudio_id = pEstudio
         order by        1,
                         decode (tipo_subgrupo_id, 'TE', 1, 'PR', 2, 'LA', 3, 'SE', 4, 0);
   begin
      fop.block ('Control 1 - Subgrups sense planificar', font_size => 12, font_weight => 'bold',
                 border_bottom => 'solid');

      for x in lista_errores_1 loop
         v_aux := v_aux + 1;
         p (x.asignatura_id || ' ' || x.subgrupo);
      end loop;

      if v_aux = 0 then
         fop.block ('No hi ha cap error.', space_before => 0.5);
      elsif v_aux = 1 then
         fop.block ('Trovat ' || v_aux || ' error.', space_before => 0.5);
      else
         fop.block ('Trovats ' || v_aux || ' errors.', space_before => 0.5);
      end if;
   exception
      when others then
         p (sqlerrm);
   end;

   procedure control_2 is
      v_aux          number        := 0;
      v_asignatura   varchar2 (20);

      cursor lista_errores_2_v1 is
         select   asignatura_id, tipo_subgrupo_id || subgrupo_id subgrupo,
                  decode (dia_semana_id,
                          1, 'Dilluns',
                          2, 'Dimarts',
                          3, 'Dimecres',
                          4, 'Dijous',
                          5, 'Divendres',
                          6, 'Dissabte',
                          7, 'Diumenge',
                          'Error'
                         ) dia_semana,
                  hora_inicio, hora_fin,
                  (select asi2.asignatura_id || ' ' || i2.tipo_subgrupo_id || i2.subgrupo_id grupo
                   from   uji_horarios.hor_items i2,
                          uji_horarios.hor_items_asignaturas asi2
                   where  i.id <> i2.id
                   and    i2.id = asi2.item_id
                   and    i.curso_id = i2.curso_id
                   and    i.semestre_id = i2.semestre_id
                   and    i.grupo_id = i2.grupo_id
                   and    i.tipo_subgrupo_id <> i2.tipo_subgrupo_id
                   and    i.dia_semana_id = i2.dia_semana_id
                   and    asi.estudio_id = asi2.estudio_id
                   and    asi.asignatura_id = asi2.asignatura_id
                   and    (   to_number (to_char (i.hora_inicio, 'hh24mi')) between to_number (to_char (i2.hora_inicio,
                                                                                                        'hh24mi'))
                                                                                and to_number (to_char (i2.hora_fin,
                                                                                                        'hh24mi'))
                           or to_number (to_char (i.hora_fin, 'hh24mi')) between to_number (to_char (i2.hora_inicio,
                                                                                                     'hh24mi'))
                                                                             and to_number (to_char (i2.hora_fin,
                                                                                                     'hh24mi'))
                          )
                   and    rownum <= 1) solape
         from     uji_horarios.hor_items i,
                  uji_horarios.hor_items_asignaturas asi
         where    i.id = asi.item_id
         and      curso_id = pCurso
         and      semestre_id = pSemestre
         and      grupo_id = pGrupo
         and      tipo_subgrupo_id in ('TE')
         and      estudio_id = pEstudio
         and      exists (
                     select 1
                     from   uji_horarios.hor_items i2,
                            uji_horarios.hor_items_asignaturas asi2
                     where  i.id <> i2.id
                     and    i2.id = asi2.item_id
                     and    i.curso_id = i2.curso_id
                     and    i.semestre_id = i2.semestre_id
                     and    i.grupo_id = i2.grupo_id
                     and    i.tipo_subgrupo_id <> i2.tipo_subgrupo_id
                     and    i.dia_semana_id = i2.dia_semana_id
                     and    asi.estudio_id = asi2.estudio_id
                     and    asi.asignatura_id = asi2.asignatura_id
                     and    (   to_number (to_char (i.hora_inicio, 'hh24mi')) between to_number
                                                                                               (to_char (i2.hora_inicio,
                                                                                                         'hh24mi'))
                                                                                  and to_number (to_char (i2.hora_fin,
                                                                                                          'hh24mi'))
                             or to_number (to_char (i.hora_fin, 'hh24mi')) between to_number (to_char (i2.hora_inicio,
                                                                                                       'hh24mi'))
                                                                               and to_number (to_char (i2.hora_fin,
                                                                                                       'hh24mi'))
                            ))
         order by 1,
                  2;

      cursor lista_errores_2 is
         select distinct asignatura_id, tipo_subgrupo_id || subgrupo_id subgrupo,
                         decode (dia_semana_id,
                                 1, 'Dilluns',
                                 2, 'Dimarts',
                                 3, 'Dimecres',
                                 4, 'Dijous',
                                 5, 'Divendres',
                                 6, 'Dissabte',
                                 7, 'Diumenge',
                                 'Error'
                                ) dia_semana,
                         to_char (inicio, 'dd/mm/yyyy') fecha, to_char (inicio, 'hh24:mi') hora_inicio,
                         to_char (fin, 'hh24:mi') hora_fin,
                         (select asi2.asignatura_id || ' ' || i2.tipo_subgrupo_id || i2.subgrupo_id || ' - '
                                 || to_char (inicio, 'hh24:mi') || '-' || to_char (fin, 'hh24:mi') grupo
                          from   uji_horarios.hor_items i2,
                                 uji_horarios.hor_items_asignaturas asi2,
                                 uji_horarios.hor_items_detalle det2
                          where  i.id <> i2.id
                          and    i2.id = det2.item_id
                          and    i2.id = asi2.item_id
                          and    i.curso_id = i2.curso_id
                          and    i.semestre_id = i2.semestre_id
                          and    i.grupo_id = i2.grupo_id
                          and    i.tipo_subgrupo_id <> i2.tipo_subgrupo_id
                          and    i.dia_semana_id = i2.dia_semana_id
                          and    asi.estudio_id = asi2.estudio_id
                          and    asi.asignatura_id = asi2.asignatura_id
                          and    trunc (det.inicio) = trunc (det2.inicio)
                          and    (   to_number (to_char (det.inicio + 1 / 1440, 'hh24mi'))
                                        between to_number (to_char (det2.inicio, 'hh24mi'))
                                            and to_number (to_char (det2.fin, 'hh24mi'))
                                  or to_number (to_char (det.fin - 1 / 1440, 'hh24mi'))
                                        between to_number (to_char (det2.inicio, 'hh24mi'))
                                            and to_number (to_char (det2.fin, 'hh24mi'))
                                 )
                          and    rownum <= 1) solape
         from            uji_horarios.hor_items i,
                         uji_horarios.hor_items_asignaturas asi,
                         uji_horarios.hor_items_detalle det
         where           i.id = asi.item_id
         and             i.id = det.item_id
         and             curso_id = pCurso
         and             semestre_id = pSemestre
         and             grupo_id = pGrupo
         and             tipo_subgrupo_id in ('TE')
         and             estudio_id = pEstudio
         and             exists (
                            select 1
                            from   uji_horarios.hor_items i2,
                                   uji_horarios.hor_items_asignaturas asi2,
                                   uji_horarios.hor_items_detalle det2
                            where  i.id <> i2.id
                            and    i2.id = det2.item_id
                            and    i2.id = asi2.item_id
                            and    i.curso_id = i2.curso_id
                            and    i.semestre_id = i2.semestre_id
                            and    i.grupo_id = i2.grupo_id
                            and    i.tipo_subgrupo_id <> i2.tipo_subgrupo_id
                            and    i.dia_semana_id = i2.dia_semana_id
                            and    asi.estudio_id = asi2.estudio_id
                            and    asi.asignatura_id = asi2.asignatura_id
                            and    trunc (det.inicio) = trunc (det2.inicio)
                            and    (   to_number (to_char (det.inicio + 1 / 1440, 'hh24mi'))
                                          between to_number (to_char (det2.inicio, 'hh24mi'))
                                              and to_number (to_char (det2.fin, 'hh24mi'))
                                    or to_number (to_char (det.fin - 1 / 1440, 'hh24mi'))
                                          between to_number (to_char (det2.inicio, 'hh24mi'))
                                              and to_number (to_char ((det2.fin), 'hh24mi'))
                                   ))
         order by        1,
                         2,
                         4;
   begin
      fop.block ('Control 2 - Solpament subgrups TE amb la resta', font_size => 12, font_weight => 'bold',
                 border_bottom => 'solid', space_before => 0.5);
      v_asignatura := 'PRIMERA';

      for x in lista_errores_2 loop
         v_aux := v_aux + 1;

         if     v_aux <> 1
            and v_asignatura <> x.asignatura_id then
            p (' ');
         end if;

         p (x.asignatura_id || ' ' || x.subgrupo || ' ' || x.dia_semana || ' - ' || x.fecha || ' ' || x.hora_inicio
            || '  ->  solapa amb: ' || x.solape);
         v_asignatura := x.asignatura_id;
      end loop;

      if v_aux = 0 then
         fop.block ('No hi ha cap error.', space_before => 0.5);
      elsif v_aux = 1 then
         fop.block ('Trovat ' || v_aux || ' error.', space_before => 0.5);
      else
         fop.block ('Trovats ' || v_aux || ' errors.', space_before => 0.5);
      end if;
   exception
      when others then
         p (sqlerrm);
   end;

   procedure control_3 is
      v_aux          number        := 0;
      v_asignatura   varchar2 (20);

      cursor lista_asignaturas is
         select distinct asi.asignatura_id
         from            uji_horarios.hor_items i,
                         uji_horarios.hor_items_asignaturas asi,
                         uji_horarios.hor_items_detalle det
         where           i.id = asi.item_id
         and             i.id = det.item_id
         and             curso_id = pCurso
         and             semestre_id = pSemestre
         and             grupo_id = pGrupo
         and             estudio_id = pEstudio
         order by        1;

      cursor lista_errores_3 (p_asignatura in varchar2) is
         select   asignatura_id, tipo_subgrupo_id, subgrupo, creditos, horas_totales, sum (horas_detalle) horas_detalle
         from     (select asi.asignatura_id, tipo_subgrupo_id, tipo_subgrupo_id || subgrupo_id subgrupo,
                          decode (tipo_subgrupo_id,
                                  'TE', crd_te,
                                  'PR', crd_pr,
                                  'LA', crd_la,
                                  'SE', crd_se,
                                  'TU', crd_tu,
                                  crd_ev
                                 ) creditos,
                          decode (tipo_subgrupo_id,
                                  'TE', crd_te,
                                  'PR', crd_pr,
                                  'LA', crd_la,
                                  'SE', crd_se,
                                  'TU', crd_tu,
                                  crd_ev
                                 )
                          * 10 horas_totales,
                          (fin - inicio) * 24 horas_detalle
                   from   uji_horarios.hor_items i,
                          uji_horarios.hor_items_asignaturas asi,
                          uji_horarios.hor_items_detalle det,
                          uji_horarios.hor_ext_asignaturas_detalle ad
                   where  i.id = asi.item_id
                   and    i.id = det.item_id
                   and    curso_id = pCurso
                   and    semestre_id = pSemestre
                   and    grupo_id = pGrupo
                   and    estudio_id = pEstudio
                   and    asi.asignatura_id = ad.asignatura_id
                   and    asi.asignatura_id = p_asignatura)
         group by asignatura_id,
                  tipo_subgrupo_id,
                  subgrupo,
                  creditos
         having   horas_totales not between sum (horas_detalle) * 0.95 and sum (horas_detalle) * 1.05
         order by 1,
                  decode (tipo_subgrupo_id, 'TE', 1, 'PR', 2, 'LA', 3, 'SE', 4, 'TU', 5, 6),
                  3;
   begin
      fop.block ('Control 3 - Control d''hores planificades per subgrup', font_size => 12, font_weight => 'bold',
                 border_bottom => 'solid', space_before => 0.5);
      v_asignatura := 'PRIMERA';

      for asi in lista_asignaturas loop
         for x in lista_errores_3 (asi.asignatura_id) loop
            v_aux := v_aux + 1;

            if     v_aux <> 1
               and v_asignatura <> x.asignatura_id then
               p (' ');
            end if;

            if x.horas_totales > x.horas_detalle then
               p (x.asignatura_id || ' ' || x.subgrupo || ' ' || 'Planificades de menys '
                  || to_char (x.horas_totales - x.horas_detalle) || ' hores, cal planificar-ne ' || x.horas_totales);
            else
               p (x.asignatura_id || ' ' || x.subgrupo || ' ' || 'Planificades de m?s '
                  || to_char (x.horas_detalle - x.horas_totales) || ' hores, cal planificar-ne ' || x.horas_totales);
            end if;

            v_asignatura := x.asignatura_id;
         end loop;
      end loop;

      if v_aux = 0 then
         fop.block ('No hi ha cap error.', space_before => 0.5);
      elsif v_aux = 1 then
         fop.block ('Trovat ' || v_aux || ' error.', space_before => 0.5);
      else
         fop.block ('Trovats ' || v_aux || ' errors.', space_before => 0.5);
      end if;
   exception
      when others then
         p (sqlerrm);
   end;

   procedure control_4 is
      v_aux          number        := 0;
      v_asignatura   varchar2 (20);

      cursor lista_errores_4 is
         select   asi.asignatura_id, grupo_id, i.tipo_subgrupo_id, subgrupo_id,
                  to_char (hora_inicio, 'hh24:mi') hora_inicio,
                  decode (dia_semana_id,
                          1, 'Dilluns',
                          2, 'Dimarts',
                          3, 'Dimecres',
                          4, 'Dijous',
                          5, 'Divendres',
                          6, 'Dissabte',
                          7, 'Diumenge',
                          'Error'
                         ) dia_semana,
                  dia_semana_id
         from     uji_horarios.hor_items i,
                  uji_horarios.hor_items_asignaturas asi
         where    i.id = asi.item_id
         and      curso_id = pCurso
         and      semestre_id = pSemestre
         and      grupo_id = pGrupo
         and      estudio_id = pEstudio
         and      dia_semana_id is not null
         and      aula_planificacion_id is null
         order by 1,
                  decode (tipo_subgrupo_id, 'TE', 1, 'PR', 2, 'LA', 3, 'SE', 4, 'TU', 5, 6),
                  4;
   begin
      fop.block ('Control 4 - Control d''hores planificades per subgrup', font_size => 12, font_weight => 'bold',
                 border_bottom => 'solid', space_before => 0.5);
      v_asignatura := 'PRIMERA';

      for x in lista_errores_4 loop
         v_aux := v_aux + 1;

         if     v_aux <> 1
            and v_asignatura <> x.asignatura_id then
            p (' ');
         end if;

         p (x.asignatura_id || ' ' || x.tipo_subgrupo_id || x.subgrupo_id || ' ' || x.dia_semana || ' ' || x.hora_inicio
            || ' - no te aula assignada');
         v_asignatura := x.asignatura_id;
      end loop;
   exception
      when others then
         p (sqlerrm);
   end;

   procedure control_5 is
      v_aux          number          := 0;
      v_aula         number;
      v_dia_semana   number;
      v_hora         varchar2 (20);
      v_txt          varchar2 (2000);

      cursor lista_errores_5 is
         select   dia_semana_id, trunc (det.inicio) fecha, to_char (hora_inicio, 'hh24:mi') hora_inicio,
                  aula_planificacion_id,
                  decode (dia_semana_id,
                          1, 'Dilluns',
                          2, 'Dimarts',
                          3, 'Dimecres',
                          4, 'Dijous',
                          5, 'Divendres',
                          6, 'Dissabte',
                          7, 'Diumenge',
                          'Error'
                         ) dia_semana,
                  nvl (a.nombre, 'Aula mal assignada a l''estudi (' || aula_planificacion_id || ')') nombre_aula
         from     uji_horarios.hor_items i,
                  uji_horarios.hor_items_asignaturas asi,
                  uji_horarios.hor_items_detalle det,
                  uji_horarios.hor_aulas a
         where    i.id = asi.item_id
         and      i.id = det.item_id
         and      curso_id = pCurso
         and      semestre_id = pSemestre
         and      grupo_id = pGrupo
         and      estudio_id = pEstudio
         and      dia_semana_id is not null
         and      aula_planificacion_id is not null
         and      aula_planificacion_id = a.id(+)
         group by asi.asignatura_id,
                  grupo_id,
                  dia_semana_id,
                  trunc (det.inicio),
                  to_char (hora_inicio, 'hh24:mi'),
                  aula_planificacion_id,
                  a.nombre
         having   count (*) > 1
         order by 5,
                  1,
                  3,
                  2;

      cursor lista_subgrupos (p_dia_Semana in number, p_fecha in date, p_hora in varchar2, p_aula in number) is
         select   nvl (comun_texto, asignatura_id) asignatura_id, tipo_subgrupo_id || subgrupo_id subgrupo
         from     uji_horarios.hor_items i,
                  uji_horarios.hor_items_asignaturas asi,
                  uji_horarios.hor_items_detalle det
         where    i.id = asi.item_id
         and      i.id = det.item_id
         and      dia_semana_id = p_dia_semana
         and      trunc (inicio) = p_fecha
         and      to_char (hora_inicio, 'hh24:mi') = p_hora
         and      aula_planificacion_id = p_aula
         and      estudio_id = pEstudio
         order by 1,
                  2;
   begin
      fop.block ('Control 5 - Control de solapament en aules', font_size => 12, font_weight => 'bold',
                 border_bottom => 'solid', space_before => 0.5);
      v_aula := 0;
      v_dia_semana := 0;
      v_hora := '';

      for x in lista_errores_5 loop
         v_aux := v_aux + 1;

         if     v_aux <> 1
            and not (    v_aula = x.aula_planificacion_id
                     and v_dia_semana = x.dia_semana_id
                     and v_hora = x.hora_inicio) then
            p (' ');
         end if;

         v_txt := null;

         if 1 = 2 then
            p (x.nombre_aula || ' - ' || to_char (x.fecha, 'dd/mm/yyyy') || ' ' || x.dia_semana || ' ' || x.hora_inicio);

            for det in lista_subgrupos (x.dia_semana_id, x.fecha, x.hora_inicio, x.aula_planificacion_id) loop
               p (espacios (30) || det.asignatura_id || '-' || det.subgrupo);
            end loop;
         else
            for det in lista_subgrupos (x.dia_semana_id, x.fecha, x.hora_inicio, x.aula_planificacion_id) loop
               v_txt := v_txt || det.asignatura_id || '-' || det.subgrupo || ' | ';
            end loop;

            v_txt := trim (v_txt);
            v_txt := substr (v_txt, 1, length (v_txt) - 2);
            p (x.nombre_aula || ' - ' || to_char (x.fecha, 'dd/mm/yyyy') || ' ' || x.dia_semana || ' ' || x.hora_inicio
               || ' ==> ' || v_txt);
         end if;

         v_aula := x.aula_planificacion_id;
         v_dia_semana := x.dia_semana_id;
         v_hora := x.hora_inicio;
      end loop;
   exception
      when others then
         p (sqlerrm);
   end;

   procedure advertencia_1 is
      v_aux             number        := 0;
      v_asignatura      varchar2 (20);
      v_tipo_subgrupo   varchar2 (20);
      v_subgrupo        number;

      cursor lista_advertencia_1 is
         select   asignatura_id, grupo_id, tipo_subgrupo_id, subgrupo_id
         from     uji_horarios.hor_items i,
                  uji_horarios.hor_items_asignaturas asi
         where    i.id = asi.item_id
         and      curso_id = pCurso
         and      semestre_id = pSemestre
         and      grupo_id = pGrupo
         and      estudio_id = pEstudio
         and      dia_semana_id is not null
         and      aula_planificacion_id is not null
         group by asignatura_id,
                  grupo_id,
                  tipo_subgrupo_id,
                  subgrupo_id
         having   count (distinct aula_planificacion_id) > 1;

      cursor lista_aulas (p_asignatura in varchar2, p_tipo_subgrupo in varchar2, p_subgrupo in number) is
         select distinct nvl (a.nombre, 'Aula mal assignada a l''estudi (' || aula_planificacion_id || ')') nombre_aula
         from            uji_horarios.hor_items i,
                         uji_horarios.hor_items_asignaturas asi,
                         uji_horarios.hor_aulas a
         where           i.id = asi.item_id
         and             i.aula_planificacion_id = a.id(+)
         and             curso_id = pCurso
         and             semestre_id = pSemestre
         and             grupo_id = pGrupo
         and             estudio_id = pEstudio
         and             dia_semana_id is not null
         and             aula_planificacion_id is not null
         and             asignatura_id = p_asignatura
         and             tipo_subgrupo_id = p_tipo_subgrupo
         and             subgrupo_id = p_subgrupo
         order by        1;
   begin
      fop.block ('Advertencia 1 - Classes d''un subgrup en aules diferents', font_size => 12, font_weight => 'bold',
                 border_bottom => 'solid', space_before => 0.5);
      v_aux := v_aux + 1;
      v_asignatura := 'PRIMERA';
      v_tipo_subgrupo := 'XX';
      v_subgrupo := 0;

      for x in lista_advertencia_1 loop
         v_aux := v_aux + 1;

         if     v_aux <> 1
            and not (    v_asignatura = x.asignatura_id
                     and v_tipo_subgrupo = x.tipo_subgrupo_id
                     and v_subgrupo = x.subgrupo_id
                    ) then
            p (' ');
         end if;

         for det in lista_aulas (x.asignatura_id, x.tipo_subgrupo_id, x.subgrupo_id) loop
            p (x.asignatura_id || ' - ' || x.tipo_subgrupo_id || x.subgrupo_id || ' ' || det.nombre_aula);
         end loop;

         v_asignatura := x.asignatura_id;
         v_tipo_subgrupo := x.tipo_subgrupo_id;
         v_subgrupo := x.subgrupo_id;
      end loop;
   exception
      when others then
         p (sqlerrm);
   end;
BEGIN
   select count (*)
   into   vSesion
   from   apa_sesiones
   where  sesion_id = pSesion
   and    fecha < sysdate () - 0.33;

   if vSesion > 0 then
      select nombre
      into   v_nombre
      from   hor_estudios
      where  id = pEstudio;

      cabecera;
      control_1;
      control_2;
      control_3;
      control_4;
      control_5;
      advertencia_1;
      pie;
   else
      cabecera;
      fop.block ('No tens perm�s per accedir a aquest document.', font_weight => 'bold', text_align => 'center',
                 font_size => 14, space_after => 0.5);
      pie;
   end if;
EXCEPTION
   WHEN OTHERS THEN
      htp.p (sqlerrm);
END;


