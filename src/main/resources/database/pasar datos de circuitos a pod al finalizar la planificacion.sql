/* Formatted on 25/06/2013 17:59 (Formatter Plus v4.8.8) */
select *
from   pod_circuitos_cab
where  curso_aca = 2012
and    tit_id = 217

select *
from pod_circuitos_det
where cic_curso_aca = 2012
and cic_tit_id = 217
and cic_id = 1
and sgd_sgr_grp_asi_id = 'MI1002'

select *
from pod_subgrupos_det
where sgr_grp_curso_aca= 2012
and sgr_grp_asi_id= 'MI1002'
and sgr_grp_id ='A'

select *
from pod_subgrupos
where grp_curso_aca= 2012
and grp_asi_id= 'MI1002'
and grp_id ='A'

and tipo ='TE'








--------------------------

FALTA ADECUAR ASIGNATURAS A TITULACIONES, EN CIRCUIRS COMPARTITS



/* Formatted on 26/06/2013 08:49 (Formatter Plus v4.8.8) */
declare
   v_tit         number;
   v_curso_aca   number;
   v_cuantos     number;

   cursor lista_estudios is
      select   id
      from     pod_titulaciones
      where    uest_id = 2
      and      id = 218
      order by id;

   cursor lista_circuitos (p_tit in number) is
      select id, grupo_id, nombre, estudio_id,
             row_number () over (partition by estudio_id order by grupo_id,
              nombre) orden
      from   (select   c.id, c.grupo_id, c.nombre, ce.estudio_id
              from     uji_horarios.hor_circuitos c,
                       uji_horarios.hor_items_circuitos ic,
                       uji_horarios.hor_items i,
                       uji_horarios.hor_items_asignaturas a,
                       uji_horarios.hor_circuitos_estudios ce
              where    c.id = ic.circuito_id
              and      ic.item_id = i.id
              and      i.id = a.item_id
              and      c.id = ce.circuito_id
              and      ce.estudio_id = a.estudio_id
              and      ce.estudio_id = p_tit
              group by c.id,
                       c.grupo_id,
                       c.nombre,
                       ce.estudio_id);

   cursor lista_detalle_1 (p_cir_id in number) is
      select   a.asignatura_id, i.grupo_id, i.tipo_subgrupo_id, i.subgrupo_id, c.plazas
      from     uji_horarios.hor_circuitos c,
               uji_horarios.hor_items_circuitos ic,
               uji_horarios.hor_items i,
               uji_horarios.hor_items_asignaturas a,
               uji_horarios.hor_circuitos_estudios ce
      where    c.id = ic.circuito_id
      and      ic.item_id = i.id
      and      i.id = a.item_id
      and      c.id = ce.circuito_id
      and      ce.estudio_id = a.estudio_id
      and      c.id = p_cir_id
      and      i.tipo_subgrupo_id not in ('AV')
      group by a.asignatura_id,
               i.grupo_id,
               i.tipo_subgrupo_id,
               i.subgrupo_id,
               c.plazas
      order by 1,
               2,
               3,
               4;
begin
   v_curso_aca := 2013;
   dbms_output.put_line ('----------------------------');

   for t in lista_estudios loop
      for x in lista_circuitos (t.id) loop
         dbms_output.put_line ('Insertar pod_circuitos_cab (' || x.id || ') ' || x.estudio_id || ' - ' || x.nombre
                               || ' - ' || x.orden);

         insert into pod_circuitos_cab
                     (TIT_ID, ID, CURSO_ACA, NOMBRE, GRP_ID, DESCRIPCION
                     )
         values      (x.estudio_id, x.orden, v_curso_aca, x.nombre, x.grupo_id, x.nombre
                     );

         for d in lista_detalle_1 (x.id) loop
            dbms_output.put_line ('   Insertar pod_subgrupos_det - circuitos_det ' || d.asignatura_id || ' '
                                  || d.grupo_id || ' ' || d.tipo_subgrupo_id || ' ' || d.subgrupo_id || ' ' || d.plazas);

            select count (*)
            into   v_cuantos
            from   pod_subgrupos_det
            where  sgr_grp_asi_id = d.asignatura_id
            and    sgr_grp_id = d.grupo_id
            and    sgr_grp_curso_aca = v_curso_aca
            and    sgr_tipo = d.tipo_subgrupo_id
            and    sgr_id = d.subgrupo_id;

            insert into pod_subgrupos_det
                        (sgr_grp_asi_id, sgr_grp_id, sgr_grp_curso_aca, sgr_tipo, sgr_id, id,
                         limite
                        )
            values      (d.asignatura_id, d.grupo_id, v_curso_aca, d.tipo_subgrupo_id, d.subgrupo_id, v_cuantos + 1,
                         d.plazas
                        );

            insert into pod_circuitos_det
                        (sgd_sgr_grp_curso_aca, sgd_sgr_tipo, sgd_sgr_id, sgd_sgr_grp_id, sgd_sgr_grp_asi_id, sgd_id,
                         cic_curso_aca, cic_id, cic_tit_id
                        )
            values      (v_curso_aca, d.tipo_subgrupo_id, d.subgrupo_id, d.grupo_id, d.asignatura_id, v_cuantos + 1,
                         v_curso_aca, x.orden, x.estudio_id
                        );
         end loop;
      end loop;
   end loop;
end;

------------------------------


delete pod_circuitos_det
where cic_curso_aca = 2013;

delete pod_circuitos_cab
where curso_aca = 2013;

delete pod_subgrupos_det
where sgr_Grp_curso_aca= 2013;


--------------------------------


circuitos compartidos, sobran las asignaturas que no son de la titulacion
habria que incluirlo en la generación de los circuitos

delete pod_circuitos_det cd
where cic_curso_aca = 2013
and cic_tit_id = 216
and sgd_sgr_grp_asi_id not in (select asi_id from pod_asignaturas_titulaciones a where a.tit_id= cd.cic_tit_id);




select *
from pod_circuitos_det cd
where cic_curso_aca = 2013
and cic_tit_id = 215
and sgd_sgr_grp_asi_id not in (select asi_id from pod_asignaturas_titulaciones a where a.tit_id= cd.cic_tit_id);



