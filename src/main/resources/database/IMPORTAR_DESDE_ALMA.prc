/* Formatted on 03/03/2014 14:22 (Formatter Plus v4.8.8) */
BEGIN
   delete      uji_horarios.HOR_EXAMENES_AULAS;

   delete      uji_horarios.HOR_EXAMENES_ASIGNATURAS;

   delete      uji_horarios.HOR_EXAMENES;

   delete      uji_horarios.HOR_ITEMS_CIRCUITOS;

   delete      uji_horarios.HOR_ITEMS_COMUNES;

   delete      uji_horarios.HOR_ITEMS_DETALLE;

   delete      uji_horarios.HOR_ITEMS_ASIGNATURAS;

   delete      uji_horarios.HOR_ITEMS_PROFESORES;

   delete      uji_horarios.HOR_ITEMS;

   delete      uji_horarios.HOR_PERMISOS_EXTRA;

   delete      uji_horarios.HOR_CIRCUITOS_ESTUDIOS;

   delete      uji_horarios.HOR_CIRCUITOS;

   delete      uji_horarios.HOR_HORARIOS_HORAS;

   delete      uji_horarios.HOR_EXT_CALENDARIO;

   delete      uji_horarios.HOR_EXT_CARGOS_PER;

   delete      uji_horarios.HOR_EXT_ASIGNATURAS_DETALLE;

   delete      uji_horarios.HOR_EXT_PERSONAS;

   delete      uji_horarios.HOR_DIAS_SEMANA;

   delete      uji_horarios.HOR_PROFESORES;

   delete      uji_horarios.HOR_SEMESTRES_DETALLE;

   delete      uji_horarios.HOR_SEMESTRES;

   delete      uji_horarios.HOR_AREAS;

   delete      uji_horarios.HOR_DEPARTAMENTOS;

   delete      uji_horarios.HOR_AULAS_PLANIFICACION;

   delete      uji_horarios.HOR_ESTUDIOS_COMPARTIDOS;

   delete      uji_horarios.HOR_ESTUDIOS;

   delete      uji_horarios.HOR_AULAS;

   delete      uji_horarios.HOR_CENTROS;

   delete      uji_horarios.HOR_TIPOS_CARGOS;

   delete      uji_horarios.HOR_TIPOS_ESTUDIOS;

   delete      uji_horarios.HOR_CURSO_ACADEMICO;

   delete      hor_ext_circuitos;

   delete      hor_ext_asignaturas_comunes;

   delete      hor_ext_asignaturas_area;

   commit;

   insert into uji_horarios.HOR_CURSO_ACADEMICO
      select *
      from   uji_horarios.HOR_CURSO_ACADEMICO@alma1;

   insert into hor_Centros
      select *
      from   HOR_CENTROS@alma1;

   insert into hor_semestres
      select *
      from   HOR_SEMESTRES@alma1;

   insert into hor_tipos_estudios
      select *
      from   HOR_TIPOS_ESTUDIOS@alma1;

   insert into hor_semestres_detalle
      select *
      from   HOR_SEMESTRES_DETALLE@alma1;

   insert into hor_tipos_cargos
      select *
      from   HOR_TIPOS_CARGOS@alma1;

   insert into hor_departamentos
      select *
      from   HOR_DEPARTAMENTOS@alma1;

   insert into hor_areas
      select *
      from   HOR_AREAS@alma1;

   insert into hor_aulas
      select *
      from   HOR_AULAS@alma1;

   insert into hor_estudios
      select *
      from   HOR_ESTUDIOS@alma1;

   insert into hor_permisos_extra
      select *
      from   HOR_PERMISOS_EXTRA@alma1;

   insert into hor_profesores
      select *
      from   HOR_PROFESORES@alma1;

   insert into hor_aulas_planificacion
      select *
      from   HOR_AULAS_PLANIFICACION@alma1;

   insert into hor_circuitos
      select *
      from   HOR_CIRCUITOS@alma1;

   insert into hor_dias_semana
      select *
      from   HOR_DIAS_SEMANA@alma1;

   insert into hor_ext_asignaturas_comunes
      select *
      from   HOR_EXT_ASIGNATURAS_COMUNES@alma1;

   insert into uji_horarios.hor_ext_asignaturas_area
      select *
      from   uji_horarios.hor_ext_asignaturas_area@alma1
      where  length (asi_id) >= 6;

   insert into hor_ext_calendario
      select *
      from   HOR_EXT_CALENDARIO@alma1;

   insert into hor_ext_circuitos
               (id, curso_aca, estudio_id, asignatura_id, grupo_id, tipo, subgrupo_id, detalle_id, circuito_id, plazas)
      select *
      from   HOR_EXT_CIRCUITOS@alma1;

   insert into uji_horarios.HOR_EXT_ASIGNATURAS_DETALLE
               (ASIGNATURA_ID, NOMBRE_ASIGNATURA, CREDITOS, CARACTER, CICLO, CRD_TE, CRD_PR, CRD_LA, CRD_SE, CRD_TU,
                CRD_EV, CRD_CTOTAL, TIPO)
      select ASIGNATURA_ID, NOMBRE_ASIGNATURA, CREDITOS, CARACTER, CICLO, CRD_TE, CRD_PR, CRD_LA, CRD_SE, CRD_TU,
             CRD_EV, CRD_CTOTAL, TIPO
      from   uji_horarios.HOR_EXT_ASIGNATURAS_DETALLE@alma1
      where  length (asignatura_id) >= 6;

   insert into hor_ext_personas
      select *
      from   HOR_EXT_PERSONAS@alma1;

   insert into hor_horarios_horas
      select *
      from   HOR_HORARIOS_HORAS@alma1;

   insert into hor_items
      select *
      from   HOR_ITEMS@alma1;

   insert into hor_items_circuitos
      select *
      from   HOR_ITEMS_CIRCUITOS@alma1;

   insert into hor_items_comunes
      select *
      from   HOR_ITEMS_COMUNES@alma1;

   insert into uji_horarios.hor_items_asignaturas
      select *
      from   uji_horarios.hor_items_asignaturas@alma1;

   insert into hor_examenes
      select *
      from   HOR_EXAMENES@alma1;

   insert into hor_examenes_asignaturas
      select *
      from   HOR_EXAMENES_ASIGNATURAS@alma1;

   insert into hor_examenes_aulas
      select *
      from   HOR_EXAMENES_AULAS@alma1;

   insert into uji_horarios.HOR_EXT_CARGOS_PER
      select *
      from   uji_horarios.HOR_EXT_CARGOS_PER@alma1;

   insert into uji_horarios.HOR_estudios_compartidos
      select *
      from   uji_horarios.HOR_estudios_compartidos@alma1;

   commit;
END;