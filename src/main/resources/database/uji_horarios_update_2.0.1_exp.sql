ALTER TABLE UJI_HORARIOS.HOR_ITEMS
 ADD (COMENTARIOS  VARCHAR2(4000));


ALTER TABLE UJI_HORARIOS.HOR_EXAMENES
 ADD (DEFINITIVO  NUMBER                            DEFAULT 0                     NOT NULL);



ALTER TABLE UJI_HORARIOS.HOR_ITEMS_PROFESORES
 ADD (CREDITOS_PACTADOS  NUMBER);

 ALTER TABLE UJI_HORARIOS.HOR_ITEMS ADD (creditos_computables  NUMBER);


ALTER TABLE UJI_HORARIOS.HOR_ITEMS_PROFESORES ADD (creditos_computables  NUMBER);



update hor_items_profesores
set creditos_computables = creditos_pactados;

commit;


CREATE OR REPLACE FORCE VIEW UJI_HORARIOS.HOR_EXT_ASIGNATURAS_AREA (ASI_ID, UEST_ID, RECIBE_ACTA, PORCENTAJE, CURSO_ACA) AS
   select "ASI_ID", "UEST_ID", "RECIBE_ACTA", "PORCENTAJE", "CURSO_ACA"
   from   pod_asignaturas_area p,
          hor_curso_academico c
   where  curso_aca = c.id
   and    porcentaje > 0
   union all
   select pasi_id, uest_id, 'S' recibe_acta, porcentaje, curso_aca
   from   pop_asignaturas_area,
          hor_curso_academico c
   where  curso_aca = c.id
   and    porcentaje > 0;

CREATE OR REPLACE FORCE VIEW UJI_HORARIOS.HOR_EXT_PERSONAS (ID, NOMBRE, EMAIL, ACTIVIDAD_ID, UBICACION_ID,
                                                            NOMBRE_BUSCAR) AS
   SELECT per_id id, nombre, busca_cuenta (per_id) email, act_id actividad_id, ubicacion_id,
          cmp_util.limpia_txt (nombre) nombre_buscar
   FROM   grh_vw_contrataciones_ult c
   WHERE  act_id IN ('PAS', 'PDI')
   /*AND ubicacion_id IN (SELECT   id
                          FROM   est_ubic_estructurales
                         WHERE   tuest_id = 'DE' AND status = 'A')*/
   AND    act_id = (SELECT MAX (act_id)
                    FROM   grh_vw_contrataciones_ult c2
                    WHERE  c.per_id = c2.per_id)
   UNION ALL
   SELECT 8126 id, 'ALVARIÑO GALDO, CRISTÓBAL', 'calvarin@uji.es', 'PAS', 218,
          cmp_util.limpia_txt ('ALVARIÑO GALDO, CRISTÓBAL') nombre_buscar
   FROM   DUAL
   UNION ALL
   SELECT 62934 id, 'GONZÁLEZ MONSONÍS, VICENTE', 'vmonsoni@uji.es', 'PAS', 1883,
          cmp_util.limpia_txt ('GONZÁLEZ MONSONÍS, VICENTE') nombre_buscar
   FROM   DUAL
   union all
   select p.id, upper(apellido1 || ' ' || apellido2||', '||nombre) nombre, busca_cuenta (per_id) email, 'PAS' actividad_id,
          uest_id, cmp_util.limpia_txt (nombre || ' ' || apellido1 || ' ' || apellido2) nombre_buscaar
   from   gra_pod.pod_permisos_Extra_pod x,
          per_personas p
   where  x.per_id = p.id;

