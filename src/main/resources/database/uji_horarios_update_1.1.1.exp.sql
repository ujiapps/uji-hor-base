ALTER TABLE UJI_HORARIOS.HOR_SEMESTRES_DETALLE
 ADD (fechas_examenes_inicio_c2  DATE);

ALTER TABLE UJI_HORARIOS.HOR_SEMESTRES_DETALLE
 ADD (fechas_examenes_fin_c2  DATE);
 
 
 CREATE TABLE uji_horarios.hor_examenes 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2 (1000) , 
     fecha DATE , 
     hora_inicio DATE , 
     hora_fin DATE , 
     comun NUMBER , 
     comun_texto VARCHAR2 (100) , 
     convocatoria_id NUMBER , 
     convocatoria_nombre VARCHAR2 (100) 
    ) 
;



ALTER TABLE uji_horarios.hor_examenes 
    ADD CONSTRAINT hor_examenes_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_horarios.hor_examenes_asignaturas 
    ( 
     id NUMBER  NOT NULL , 
     examen_id NUMBER  NOT NULL , 
     asignatura_id VARCHAR2 (10) , 
     asignatura_nombre VARCHAR2 (1000) , 
     estudio_id NUMBER  NOT NULL , 
     estudio_nombre VARCHAR2 (1000) , 
     semestre NUMBER , 
     tipo_asignatura VARCHAR2 (10) , 
     centro_id NUMBER 
    ) 
;


CREATE INDEX uji_horarios.hor_examenes_asi_exa_IDX ON uji_horarios.hor_examenes_asignaturas 
    ( 
     examen_id ASC 
    ) 
;
CREATE INDEX uji_horarios.hor_examenes_asi_est_IDX ON uji_horarios.hor_examenes_asignaturas 
    ( 
     estudio_id ASC 
    ) 
;

ALTER TABLE uji_horarios.hor_examenes_asignaturas 
    ADD CONSTRAINT hor_examenes_asignaturas_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_horarios.hor_examenes_aulas 
    ( 
     id NUMBER  NOT NULL , 
     examen_id NUMBER  NOT NULL , 
     aula_id NUMBER  NOT NULL 
    ) 
;


CREATE INDEX uji_horarios.hor_examenes_aulas_exa_IDX ON uji_horarios.hor_examenes_aulas 
    ( 
     examen_id ASC 
    ) 
;
CREATE INDEX uji_horarios.hor_examenes_aulas_aula_IDX ON uji_horarios.hor_examenes_aulas 
    ( 
     aula_id ASC 
    ) 
;

ALTER TABLE uji_horarios.hor_examenes_aulas 
    ADD CONSTRAINT hor_examenes_aulas_PK PRIMARY KEY ( id ) ;


ALTER TABLE uji_horarios.hor_examenes_aulas 
    ADD CONSTRAINT hor_examenes_aulas__UN UNIQUE ( examen_id , aula_id ) ;




ALTER TABLE uji_horarios.hor_examenes_asignaturas 
    ADD CONSTRAINT hor_examenes_asi_est_FK FOREIGN KEY 
    ( 
     estudio_id
    ) 
    REFERENCES uji_horarios.hor_estudios 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_examenes_asignaturas 
    ADD CONSTRAINT hor_examenes_asi_exa_FK FOREIGN KEY 
    ( 
     examen_id
    ) 
    REFERENCES uji_horarios.hor_examenes 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_examenes_aulas 
    ADD CONSTRAINT hor_examenes_aulas_aula_FK FOREIGN KEY 
    ( 
     aula_id
    ) 
    REFERENCES uji_horarios.hor_aulas 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_examenes_aulas 
    ADD CONSTRAINT hor_examenes_aulas_exa_FK FOREIGN KEY 
    ( 
     examen_id
    ) 
    REFERENCES uji_horarios.hor_examenes 
    ( 
     id
    ) 
;




 
CREATE OR REPLACE FORCE VIEW UJI_HORARIOS.HOR_EXT_CARGOS_PER (ID,
                                                              PERSONA_ID,
                                                              NOMBRE,
                                                              CENTRO_ID,
                                                              CENTRO,
                                                              ESTUDIO_ID,
                                                              ESTUDIO,
                                                              CARGO_ID,
                                                              CARGO
                                                             ) AS
   select rownum id, PERSONA_ID, NOMBRE, CENTRO_ID, CENTRO, TITULACION_ID estudio_id, TITULACION estudio, CARGO_ID,
          CARGO
   from   (select   cp.per_id persona_id, p.nombre || ' ' || apellido1 || ' ' || apellido2 nombre, ulogica_id centro_id,
                    ubic.nombre centro, tit.id titulacion_id, tit.nombre titulacion, tc.id cargo_id, tc.nombre cargo
           from     grh_grh.grh_cargos_per cp,
                    gri_est.est_ubic_estructurales ubic,
                    gra_Exp.exp_v_titu_todas tit,
                    uji_horarios.hor_tipos_Cargos tc,
                    gri_per.per_personas p
           where    (    f_fin is null
                     and (   f_fin is null
                          or f_fin >= sysdate)
                     and crg_id in (189, 195, 188, 30))
           and      ulogica_id = ubic.id
           and      ulogica_id = tit.uest_id
           and      tit.activa = 'S'
           and      tit.tipo = 'G'
           and      tc.id = 3
           and      cp.per_id = p.id
           union all
/* PAS de centro */
           select   cp.per_id persona_id, p.nombre || ' ' || apellido1 || ' ' || apellido2 nombre,
                    ubicacion_id centro_id, ubic.nombre centro, tit.id titulacion_id, tit.nombre titulacion,
                    tc.id cargo_id, tc.nombre cargo
           from     grh_grh.grh_vw_contrataciones_ult cp,
                    gri_est.est_ubic_estructurales ubic,
                    gra_Exp.exp_v_titu_todas tit,
                    uji_horarios.hor_tipos_Cargos tc,
                    gri_per.per_personas p
           where    ubicacion_id = ubic.id
           and      ubicacion_id = tit.uest_id
           and      tit.activa = 'S'
           and      tit.tipo = 'G'
           and      tc.id = 4
           and      cp.per_id = p.id
           union all
/* directores de titulacion */
           select   cp.per_id persona_id, p.nombre || ' ' || apellido1 || ' ' || apellido2 nombre, ulogica_id centro_id,
                    ubic.nombre centro, tit.id titulacion_id, tit.nombre titulacion, tc.id cargo_id, tc.nombre cargo
           from     grh_grh.grh_cargos_per cp,
                    gri_est.est_ubic_estructurales ubic,
                    gra_Exp.exp_v_titu_todas tit,
                    uji_horarios.hor_tipos_Cargos tc,
                    gri_per.per_personas p
           where    (    f_fin is null
                     and (   f_fin is null
                          or f_fin >= sysdate)
                     and crg_id in (192, 193))
           and      ulogica_id = ubic.id
           and      ulogica_id = tit.uest_id
           and      tit.activa = 'S'
           and      tit.tipo = 'G'
           and      tc.id = 1
           and      cp.per_id = p.id
           and      cp.tit_id = tit.id
           union all
/* permisos extra */
           select   per.persona_id, p.nombre || ' ' || apellido1 || ' ' || apellido2 nombre, ubic.id centro_id,
                    ubic.nombre centro, tit.id titulacion_id, tit.nombre titulacion, tc.id cargo_id, tc.nombre cargo
           from     uji_horarios.hor_permisos_extra per,
                    gri_per.per_personas p,
                    gra_Exp.exp_v_titu_todas tit,
                    uji_horarios.hor_tipos_cargos tc,
                    gri_est.est_ubic_estructurales ubic
           where    persona_id = p.id
           and      estudio_id = tit.id
           and      tipo_Cargo_id = tc.id
           and      tit.activa = 'S'
           and      tit.tipo = 'G'
           and      tit.uest_id = ubic.id
/* administradores */
           union all
           select distinct per.persona_id, p.nombre || ' ' || apellido1 || ' ' || apellido2 nombre, u.id centro_id,
                           u.nombre centro, tit.id titulacion_id, tit.nombre titulacion, tc.id cargo_id,
                           tc.nombre cargo
           from            uji_apa.apa_vw_personas_items per,
                           gri_per.per_personas p,
                           gra_Exp.exp_v_titu_todas tit,
                           uji_horarios.hor_tipos_cargos tc,
                           gri_Est.est_ubic_estructurales u
           where           persona_id = p.id
           and             tc.id = 1
           and             tit.activa = 'S'
           and             tit.tipo = 'G'
           and             aplicacion_id = 46
           and             role = 'ADMIN'
           and             tit.uest_id = u.id
           order by        titulacion_id);

DROP VIEW UJI_HORARIOS.HOR_EXT_ASIGNATURAS_AREA;

/* Formatted on 03/03/2014 14:03 (Formatter Plus v4.8.8) */
CREATE OR REPLACE FORCE VIEW UJI_HORARIOS.HOR_EXT_ASIGNATURAS_AREA (ASI_ID, UEST_ID, RECIBE_ACTA, PORCENTAJE, CURSO_ACA) AS
   select "ASI_ID", "UEST_ID", "RECIBE_ACTA", "PORCENTAJE", "CURSO_ACA"
   from   pod_asignaturas_area p, hor_curso_academico c
   where  curso_aca = c.id

ALTER TABLE UJI_HORARIOS.HOR_ITEMS_PROFESORES
 ADD (creditos  NUMBER);


CREATE OR REPLACE FORCE VIEW UJI_HORARIOS.HOR_V_ITEMS_PROF_DETALLE (ID,
                                                                    DETALLE_ID,
                                                                    DET_PROFESOR_ID,
                                                                    ITEM_ID,
                                                                    INICIO,
                                                                    FIN,
                                                                    DESCRIPCION,
                                                                    PROFESOR_ID,
                                                                    DETALLE_MANUAL,
                                                                    CREDITOS,
                                                                    CREDITOS_DETALLE,
                                                                    SELECCIONADO,
                                                                    NOMBRE,
                                                                    PROFESOR_COMPLETO
                                                                   ) AS
   select   to_number (id || det_profesor_id || item_id || to_char (inicio, 'yyyymmddhh24miss')) id, id detalle_id,
            det_profesor_id, item_id, inicio, fin, descripcion, profesor_id, detalle_manual, creditos, creditos_detalle,
            decode (detalle_manual, 0, 'S', decode (min (orden), 1, 'S', 'N')) seleccionado, nombre, profesor_completo
   from     (select d.*, profesor_id, p.detalle_manual, i.creditos, p.creditos creditos_detalle, p.id det_profesor_id,
                    99 orden, nombre, (select wm_concat (nombre)
                                       from   hor_items_profesores pr,
                                              hor_profesores p
                                       where  item_id = i.id
                                       and    pr.profesor_id = p.id) profesor_completo
             from   hor_items i,
                    hor_items_detalle d,
                    hor_items_profesores p,
                    hor_profesores pro
             where  i.id = d.item_id
             and    i.id = p.item_id
             and    p.profesor_id = pro.id
             union all
             select d.*, profesor_id, p.detalle_manual, i.creditos, p.creditos creditos_detalle, p.id det_profesor_id,
                    1 orden, nombre, (select wm_concat (nombre)
                                      from   hor_items_profesores pr,
                                             hor_profesores p
                                      where  item_id = i.id
                                      and    pr.profesor_id = p.id) profesor_completo
             from   hor_items i,
                    hor_items_detalle d,
                    hor_items_profesores p,
                    hor_items_det_profesores dp,
                    hor_profesores pro
             where  i.id = d.item_id
             and    i.id = p.item_id
             and    p.detalle_manual = 1
             and    dp.detalle_id = d.id
             and    item_profesor_id = p.id
             and    p.profesor_id = pro.id)
   group by id,
            item_id,
            inicio,
            fin,
            descripcion,
            profesor_id,
            detalle_manual,
            creditos,
            creditos_detalle,
            det_profesor_id,
            nombre,
            profesor_completo;


			
			