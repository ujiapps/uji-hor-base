CREATE TABLE uji_horarios.hor_informaciones 
    ( 
     id NUMBER  NOT NULL , 
     texto VARCHAR2 (2000) , 
     orden NUMBER  NOT NULL , 
     fecha_inicio DATE  NOT NULL , 
     fecha_fin DATE 
    ) 
;



ALTER TABLE uji_horarios.hor_informaciones 
    ADD CONSTRAINT hor_informaciones_PK PRIMARY KEY ( id ) ;



ALTER TABLE UJI_HORARIOS.HOR_CIRCUITOS DROP COLUMN SEMESTRE_ID;


CREATE OR REPLACE FUNCTION UJI_HORARIOS.hor_f_fecha_orden (
   p_estudio         in   number,
   p_semestre        in   number,
   p_curso           in   number,
   p_asignatura      in   varchar2,
   p_grupo           in   varchar2,
   p_subgrupo_tipo   in   varchar2,
   p_subgrupo        in   number,
   p_dia_semana      in   number,
   p_fecha           in   date
)
   RETURN NUMBER IS
   v_aux   NUMBER;
BEGIN
   select count (*)
   into   v_aux
   from   (select ia.estudio_id, i.curso_id, i.semestre_id, i.grupo_id, i.tipo_subgrupo_id, i.subgrupo_id,
                  i.dia_Semana_id, ia.asignatura_id, fecha_inicio, fecha_fin, fecha_examenes_inicio, fecha_examenes_fin,
                  i.desde_el_dia, hasta_el_dia, repetir_cada_semanas, numero_iteraciones, detalle_manual, c.fecha,
                  tipo_dia, dia_semana
           from   hor_estudios e,
                  hor_semestres_detalle s,
                  hor_items i,
                  hor_items_asignaturas ia,
                  hor_ext_calendario c
           where  e.tipo_id = s.tipo_estudio_id
           and    i.semestre_id = s.semestre_id
           and    c.fecha between fecha_inicio and nvl (fecha_examenes_fin, fecha_fin)
           and    c.dia_semana_id = i.dia_semana_id
           and    tipo_dia = 'L'
           and    detalle_manual = 0
           and    i.semestre_id = p_semestre
           and    i.curso_id = p_curso
           and    i.id = ia.item_id
           and    ia.asignatura_id = p_asignatura
           and    ia.estudio_id = p_estudio
           and    ia.estudio_id = e.id
           and    i.grupo_id = p_grupo
           and    i.tipo_subgrupo_id = p_subgrupo_tipo
           and    i.subgrupo_id = p_subgrupo
           and    i.dia_semana_id = p_dia_semana
           and    fecha <= p_fecha);

   RETURN v_aux;
END hor_f_fecha_orden;


CREATE TABLE uji_horarios.hor_logs 
    ( 
     id NUMBER  NOT NULL , 
     item_id NUMBER  NOT NULL , 
     fecha DATE  NOT NULL , 
     usuario VARCHAR2 (100)  NOT NULL , 
     descripcion VARCHAR2 (1000)  NOT NULL 
    ) 
;


CREATE INDEX uji_horarios.hor_logs_fecha_IDX ON uji_horarios.hor_logs 
    ( 
     fecha ASC 
    ) 
;
CREATE INDEX uji_horarios.hor_logs_usu_IDX ON uji_horarios.hor_logs 
    ( 
     usuario ASC 
    ) 
;

ALTER TABLE uji_horarios.hor_logs 
    ADD CONSTRAINT hor_logs_PK PRIMARY KEY ( id ) ;



CREATE OR REPLACE FORCE VIEW UJI_HORARIOS.HOR_EXT_PERSONAS (ID, NOMBRE, EMAIL, ACTIVIDAD_ID, DEPARTAMENTO_ID, NOMBRE_BUSCAR) AS
   SELECT per_id id, nombre, busca_cuenta (per_id) email, act_id actividad_id, ubicacion_id departamento_id, cmp_util.limpia_txt(nombre) nombre_buscar
   FROM   grh_vw_contrataciones_ult c
   WHERE  act_id IN ('PAS', 'PDI')
   /*AND ubicacion_id IN (SELECT   id
                          FROM   est_ubic_estructurales
                         WHERE   tuest_id = 'DE' AND status = 'A')*/
   AND    act_id = (SELECT MAX (act_id)
                    FROM   grh_vw_contrataciones_ult c2
                    WHERE  c.per_id = c2.per_id)
   union all
   select 8126 id, 'ALVARI�O GALDO, CRIST�BAL', 'calvarin@uji.es', 'PAS', 218, cmp_util.limpia_txt('ALVARI�O GALDO, CRIST�BAL') nombre_buscar
   from   dual;

