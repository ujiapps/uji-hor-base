CREATE TABLE uji_horarios.hor_examenes 
    ( 
     id NUMBER  NOT NULL , 
     nombre VARCHAR2 (1000) , 
     fecha DATE , 
     hora_inicio DATE , 
     hora_fin DATE , 
     comun NUMBER , 
     comun_texto VARCHAR2 (100) , 
     convocatoria_id NUMBER , 
     convocatoria_nombre VARCHAR2 (100) 
    ) 
;



ALTER TABLE uji_horarios.hor_examenes 
    ADD CONSTRAINT hor_examenes_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_horarios.hor_examenes_asignaturas 
    ( 
     id NUMBER  NOT NULL , 
     examen_id NUMBER  NOT NULL , 
     asignatura_id VARCHAR2 (10) , 
     asignatura_nombre VARCHAR2 (1000) , 
     estudio_id NUMBER  NOT NULL , 
     estudio_nombre VARCHAR2 (1000) , 
     semestre NUMBER , 
     tipo_asignatura VARCHAR2 (10) , 
     centro_id NUMBER 
    ) 
;


CREATE INDEX uji_horarios.hor_examenes_asi_exa_IDX ON uji_horarios.hor_examenes_asignaturas 
    ( 
     examen_id ASC 
    ) 
;
CREATE INDEX uji_horarios.hor_examenes_asi_est_IDX ON uji_horarios.hor_examenes_asignaturas 
    ( 
     estudio_id ASC 
    ) 
;

ALTER TABLE uji_horarios.hor_examenes_asignaturas 
    ADD CONSTRAINT hor_examenes_asignaturas_PK PRIMARY KEY ( id ) ;



CREATE TABLE uji_horarios.hor_examenes_aulas 
    ( 
     id NUMBER  NOT NULL , 
     examen_id NUMBER  NOT NULL , 
     aula_id NUMBER  NOT NULL 
    ) 
;


CREATE INDEX uji_horarios.hor_examenes_aulas_exa_IDX ON uji_horarios.hor_examenes_aulas 
    ( 
     examen_id ASC 
    ) 
;
CREATE INDEX uji_horarios.hor_examenes_aulas_aula_IDX ON uji_horarios.hor_examenes_aulas 
    ( 
     aula_id ASC 
    ) 
;

ALTER TABLE uji_horarios.hor_examenes_aulas 
    ADD CONSTRAINT hor_examenes_aulas_PK PRIMARY KEY ( id ) ;


ALTER TABLE uji_horarios.hor_examenes_aulas 
    ADD CONSTRAINT hor_examenes_aulas__UN UNIQUE ( examen_id , aula_id ) ;




ALTER TABLE uji_horarios.hor_examenes_asignaturas 
    ADD CONSTRAINT hor_examenes_asi_est_FK FOREIGN KEY 
    ( 
     estudio_id
    ) 
    REFERENCES uji_horarios.hor_estudios 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_examenes_asignaturas 
    ADD CONSTRAINT hor_examenes_asi_exa_FK FOREIGN KEY 
    ( 
     examen_id
    ) 
    REFERENCES uji_horarios.hor_examenes 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_examenes_aulas 
    ADD CONSTRAINT hor_examenes_aulas_aula_FK FOREIGN KEY 
    ( 
     aula_id
    ) 
    REFERENCES uji_horarios.hor_aulas 
    ( 
     id
    ) 
;


ALTER TABLE uji_horarios.hor_examenes_aulas 
    ADD CONSTRAINT hor_examenes_aulas_exa_FK FOREIGN KEY 
    ( 
     examen_id
    ) 
    REFERENCES uji_horarios.hor_examenes 
    ( 
     id
    ) 
;


ALTER TABLE UJI_HORARIOS.HOR_SEMESTRES_DETALLE
 ADD (fechas_examenes_inicio_c2  DATE);

ALTER TABLE UJI_HORARIOS.HOR_SEMESTRES_DETALLE
 ADD (fechas_examenes_fin_c2  DATE);

 ALTER TABLE UJI_HORARIOS.HOR_ITEMS_PROFESORES
 ADD (creditos  NUMBER);


CREATE OR REPLACE FORCE VIEW UJI_HORARIOS.HOR_V_ITEMS_PROF_DETALLE (ID,
                                                                    DETALLE_ID,
                                                                    DET_PROFESOR_ID,
                                                                    ITEM_ID,
                                                                    INICIO,
                                                                    FIN,
                                                                    DESCRIPCION,
                                                                    PROFESOR_ID,
                                                                    DETALLE_MANUAL,
                                                                    CREDITOS,
                                                                    CREDITOS_DETALLE,
                                                                    SELECCIONADO,
                                                                    NOMBRE,
                                                                    PROFESOR_COMPLETO
                                                                   ) AS
   select   to_number (id || det_profesor_id || item_id || to_char (inicio, 'yyyymmddhh24miss')) id, id detalle_id,
            det_profesor_id, item_id, inicio, fin, descripcion, profesor_id, detalle_manual, creditos, creditos_detalle,
            decode (detalle_manual, 0, 'S', decode (min (orden), 1, 'S', 'N')) seleccionado, nombre, profesor_completo
   from     (select d.*, profesor_id, p.detalle_manual, i.creditos, p.creditos creditos_detalle, p.id det_profesor_id,
                    99 orden, nombre, (select wm_concat (nombre)
                                       from   hor_items_profesores pr,
                                              hor_profesores p
                                       where  item_id = i.id
                                       and    pr.profesor_id = p.id) profesor_completo
             from   hor_items i,
                    hor_items_detalle d,
                    hor_items_profesores p,
                    hor_profesores pro
             where  i.id = d.item_id
             and    i.id = p.item_id
             and    p.profesor_id = pro.id
             union all
             select d.*, profesor_id, p.detalle_manual, i.creditos, p.creditos creditos_detalle, p.id det_profesor_id,
                    1 orden, nombre, (select wm_concat (nombre)
                                      from   hor_items_profesores pr,
                                             hor_profesores p
                                      where  item_id = i.id
                                      and    pr.profesor_id = p.id) profesor_completo
             from   hor_items i,
                    hor_items_detalle d,
                    hor_items_profesores p,
                    hor_items_det_profesores dp,
                    hor_profesores pro
             where  i.id = d.item_id
             and    i.id = p.item_id
             and    p.detalle_manual = 1
             and    dp.detalle_id = d.id
             and    item_profesor_id = p.id
             and    p.profesor_id = pro.id)
   group by id,
            item_id,
            inicio,
            fin,
            descripcion,
            profesor_id,
            detalle_manual,
            creditos,
            creditos_detalle,
            det_profesor_id,
            nombre,
            profesor_completo;


			
			