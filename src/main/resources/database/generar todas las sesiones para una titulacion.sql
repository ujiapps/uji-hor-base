/* Formatted on 10/07/2013 13:43 (Formatter Plus v4.8.8) */
insert into pod_horarios_fechas
            (hor_sgr_grp_asi_id, hor_sgr_grp_id, hor_sgr_grp_curso_aca, hor_sgr_id, hor_sgr_tipo, hor_semestre,
             hor_dia_sem, hor_ini, fecha)


   select sgr_grp_asi_id, Sgr_grp_id, sgr_grp_curso_Aca, sgr_id, sgr_tipo, semestre, dia_sem, ini, fecha
   from   pod_horarios h,
          (select   hor_semestre, hor_dia_sem, fecha
           from     pod_horarios_fechas
           where    hor_sgr_grp_curso_aca = 2013
           group by hor_semestre,
                    hor_dia_sem,
                    fecha) f, grh_calendario c
   where  sgr_grp_curso_aca = 2013
   and    sgr_grp_asi_id in (select asi_id
                             from   pod_asignaturas_titulaciones
                             where  tit_id in (26, 223, 225)
                             and    tit_id = 225)
   and    semestre = hor_semestre
   and    dia_sem = hor_dia_sem
   and f.fecha = fecha_completa
   and tipo_aca not in ('E')
   --and    sgr_grp_asi_id = 'II74'
   --and    sgr_grp_id = 'A'
   --and    sgr_tipo = 'LA'
   --and    sgr_id = 1
   --and    dia_sem = 3
   --and    hor_semestre = 2
   minus
   select hor_sgr_grp_asi_id, hor_sgr_grp_id, hor_sgr_grp_curso_aca, hor_sgr_id, hor_sgr_tipo, hor_semestre,
          hor_dia_sem, hor_ini, fecha
   from   pod_horarios_fechas
   where  hor_sgr_grp_curso_aca = 2013
   and    hor_sgr_grp_asi_id in (select asi_id
                                 from   pod_asignaturas_titulaciones
                                 where  tit_id in (26, 223, 225))
   --and    hor_sgr_grp_asi_id = 'II32'
   --and    hor_sgr_grp_id = 'A'
   --and    hor_sgr_tipo = 'LA'
   --and    hor_sgr_id = 1
   --and    hor_semestre = 2
   --and    hor_dia_sem = 3
   
   
   
   