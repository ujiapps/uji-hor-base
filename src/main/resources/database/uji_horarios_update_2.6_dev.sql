

ALTER TABLE UJI_HORARIOS.HOR_PERMISOS_EXTRA
 ADD (persona_id_otorga  NUMBER);
 
 create or replace force view UJI_HORARIOS.HOR_V_ITEMS_CREDITOS_DETALLE
(
   ID
 , NOMBRE
 , CREDITOS_PROFESOR
 , ITEM_ID
 , CRD_PROF
 , CRD_PACTADOS
 , CRD_ITEM
 , CRD_ITEM_PACTADOS
 , CUANTOS
 , CRD_FINAL
 , CRD_PACTADOS_FINAL
 , GRUPO_ID
 , TIPO
 , ASIGNATURA_ID
 , ASIGNATURA_TXT
 , DEPARTAMENTO_ID
 , AREA_ID
)
   bequeath definer as
   select id, nombre, creditos_profesor, item_id, round(crd_prof, 2) crd_prof, round(crd_pactados, 2) crd_pactados, crd_item, crd_item_pactados, cuantos, round(nvl(crd_prof, trunc(crd_item / cuantos, 2)), 2) crd_final, round(nvl(crd_pactados, nvl(crd_prof, trunc(crd_item_pactados / cuantos, 2))), 2) crd_pactados_final, GRUPO_ID, TIPO_SUBGRUPO_ID || SUBGRUPO_ID tipo, asignatura_id, asignatura_txt, departamento_id, area_id
     from (  select h.id
                  , nombre
                  , h.creditos                                                 creditos_profesor
                  , ip.item_id
                  , ip.creditos                                                crd_prof
                  , nvl(ip.creditos_computables, i.creditos)                   crd_pactados
                  , i.creditos                                                 crd_item
                  , nvl(i.creditos_computables, i.creditos)                    crd_item_pactados
                  , (select count(*)
                       from hor_items_profesores ip2
                      where item_id = ip.item_id)
                       cuantos
                  , GRUPO_ID
                  , TIPO_SUBGRUPO_ID
                  , SUBGRUPO_ID
                  , listagg(asignatura_id) within group (order by asignatura_id) asignatura_txt
                  , min(asignatura_id)                                         asignatura_id
                  , departamento_id
                  , area_id
               from hor_profesores      h
                  , hor_items_profesores ip
                  , hor_items           i
                  , hor_items_asignaturas ia
              where h.id = ip.profesor_id
                and ip.item_id = i.id
                and i.id = ia.item_id
           group by h.id
                  , nombre
                  , h.creditos
                  , ip.item_id
                  , ip.creditos
                  , ip.creditos_computables
                  , i.creditos
                  , i.creditos_computables
                  , GRUPO_ID
                  , TIPO_SUBGRUPO_ID
                  , SUBGRUPO_ID
                  , departamento_id
                  , area_id) x
   union all
   select id, nombre, creditos creditos_profesor, null item_id, null crd_prof, null crd_pactados, null crd_item, null crd_item_pactados, 0 cuantos, 0 crd_final, 0 crd_pactados_final, null grupo_id, null tipo, null asignatura_id, null asignatura_txt, departamento_id, area_id
     from hor_profesores h
    where not exists
             (select profesor_id
                from hor_items_profesores
               where profesor_id = h.id);