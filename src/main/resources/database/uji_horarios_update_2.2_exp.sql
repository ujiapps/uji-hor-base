create or replace force view UJI_HORARIOS.HOR_EXT_ASIGNATURAS_AREA(ASI_ID, UEST_ID, RECIBE_ACTA, PORCENTAJE, CURSO_ACA) as
   select "ASI_ID", "UEST_ID", "RECIBE_ACTA", "PORCENTAJE", "CURSO_ACA"
     from pod_asignaturas_area p, hor_curso_academico c
    where curso_aca = c.id
      and porcentaje >= 0
   union all
   select pasi_id, uest_id, 'S' recibe_acta, porcentaje, curso_aca
     from pop_asignaturas_area, hor_curso_academico c
    where curso_aca = c.id
      and porcentaje >= 0;

