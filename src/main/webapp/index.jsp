<%@page import="java.io.*,java.util.*, javax.servlet.*" %>
<%@page contentType="text/html; charset=utf-8"%>
<%@page import="es.uji.commons.sso.User"%>
<%@ page import="java.text.SimpleDateFormat" %><!DOCTYPE html>
<html>
  <head>
    <title>Horarios</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" type="text/css" href="//static.uji.es/js/extjs/extjs-4.1.1/resources/css/ext-all.css">
    <link rel="stylesheet" type="text/css" href="//static.uji.es/js/extjs/extensible-1.5.2/resources/css/extensible-all.css">
    <link rel="stylesheet" type="text/css" href="css/BoxSelect.css"> 
    <link rel="stylesheet" type="text/css" href="css/custom.css"> 
	<link rel="stylesheet" type="text/css"	href="//static.uji.es/js/extjs/uji-commons-extjs/css/icons.css" />
    <script type="text/javascript" src="//static.uji.es/js/extjs/extjs-4.1.1/ext-all.js"></script>
    <script type="text/javascript" src="//static.uji.es/js/extjs/extensible-1.5.2/lib/extensible-all.js"></script>
    <script type="text/javascript" src="js/detectIE.js"></script>
    <%
	User user = (User) session.getAttribute(User.SESSION_USER);
    String login = user.getName();
    String activeSession = user.getActiveSession();
	ServletContext context = getServletContext();
    String appVersion = context.getInitParameter("appVersion");
    %>
	<script type="text/javascript">
	var login = '<%=login%>';
	var session = '<%=activeSession%>';
	var appversion ='<%=appVersion%>';

    if (detectIE())
    {
       alert('Estàs utilitzant la versió ' + detectIE() + ' d\'Internet Explorer que no està suportada per aquesta aplicació, per a un correcte funcionament utilitza Google Chrome, Opera o Firefox');
    }

	</script>
    <script type="text/javascript" src="app/Application.js?v=<%=new SimpleDateFormat ("yyyyMMddhhmmss").format(new Date())%>"></script>
    <style>
      #errChk label { cursor: pointer; }
      #errTitle { font-weight: bold; color: #AF1515; }
    </style>    
  </head>
  <body>
  </body>
</html>
