Ext.define('Ext.ux.uji.form.model.Lookup', {
   extend : 'Ext.data.Model',

   fields : [ 'id', 'nombre' ]
});