Ext.define('Ext.ux.uji.grid.Panel',
{
    extend : 'Ext.grid.Panel',
    alias : 'widget.ujigridpanel',

    requires : [ 'Ext.grid.plugin.RowEditing', 'Ext.ux.form.SearchField' ],

    plugins : [
    {
        ptype : 'rowediting',
        clicksToEdit : 2
    } ],

    tbar : [],
    bbar : [],

    config :
    {
        allowEdit : true,
        showSearchField : false,
        showAddButton : true,
        showRemoveButton : true,
        showReloadButton : true,
        showExportbutton : true,
        showTopToolbar : true,
        showBottomToolbar : true,
        handlerAddButton : function()
        {
            var ref = this.up('grid');

            var rowEditor = ref.getPlugin();
            var record = Ext.create(ref.store.model);

            rowEditor.cancelEdit();
            ref.store.insert(0, record);
            rowEditor.startEdit(0, 0);
        },
        handlerRemoveButton : function()
        {
            var ref = this.up('grid');
            var records = ref.getSelectionModel().getSelection();

            if (records.length > 0)
            {
                Ext.Msg.confirm('Esborrar', 'Esteu segur/a de voler esborrar el registre ?', function(btn, text)
                {
                    if (btn == 'yes')
                    {
                        var rowEditor = ref.getPlugin();

                        rowEditor.cancelEdit();
                        ref.store.remove(records);
                    }
                });
            }
        }
    },

    initComponent : function()
    {
        var ref = this;

        this.callParent(arguments);

        var tbar = this.getDockedItems('toolbar[dock="top"]')[0];
        var bbar = this.getDockedItems('toolbar[dock="bottom"]')[0];

        tbar.insert(0,
        {
            xtype : 'button',
            text : 'Esborrar',
            action : 'remove',
            iconCls : 'application-delete',
            hidden : !this.showRemoveButton,
            handler : this.handlerRemoveButton
        });

        tbar.insert(0,
        {
            xtype : 'button',
            text : 'Afegir',
            action : 'add',
            iconCls : 'application-add',
            hidden : !this.showAddButton,
            handler : this.handlerAddButton
        });

        if (this.showSearchField)
        {
            tbar.insert(0, [
            {
                xtype : searchfield,
                emptyText : 'Recerca...',
                store : this.store,
                width : 180
            }, ' ', '-' ]);
        }

        if (this.showReloadButton)
        {
            bbar.add([ '->',
            {
                xtype : 'button',
                text : 'Recarregar',
                action : 'reload',
                iconCls : 'arrow-refresh',
                handler : function(button, event, opts)
                {
                    ref.store.reload();
                }
            } ]);
        }

        if (this.showExportButton)
        {
        }

        if (!this.showTopToolbar)
        {
            tbar.hide();
        }

        if (!this.showBottomToolbar)
        {
            bbar.hide();
        }

        this.store.getProxy().on('exception', function()
        {
            ref.store.rejectChanges();
        });
    },

    listeners :
    {
        'beforeedit' : function(editor, context, eOpts)
        {
            if (!this.allowEdit && !context.record.phantom)
            {
                return false;
            }
        },

        'canceledit' : function(editor, context, eOpts)
        {
            var record = context.record;

            if ((record.phantom))
            {
                this.store.remove(context.record);
            }
        }
    }
});