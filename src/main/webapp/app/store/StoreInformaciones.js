Ext.define('HOR.store.StoreInformaciones',
{
    extend : 'Ext.data.Store',
    model : 'HOR.model.Informacion',
    autoLoad : true,
    proxy :
    {
        type : 'rest',
        url : '/hor/rest/informacion',
        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },
        writer :
        {
            type : 'json'
        }
    }
});