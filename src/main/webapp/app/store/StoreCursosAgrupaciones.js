Ext.define('HOR.store.StoreCursosAgrupaciones',
{
    extend : 'Ext.data.Store',
    model : 'HOR.model.CursoAgrupacion',

    proxy :
    {
        type : 'rest',
        url : '/hor/rest/estudio/-1',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});