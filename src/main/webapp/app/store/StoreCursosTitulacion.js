Ext.define('HOR.store.StoreCursosTitulacion',
{
    extend : 'Ext.data.Store',
    model : 'HOR.model.Curso',

    proxy :
    {
        type : 'rest',
        url : '/hor/rest/examen/titulacion/curso',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});