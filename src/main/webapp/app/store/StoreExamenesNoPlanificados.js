Ext.define('HOR.store.StoreExamenesNoPlanificados',
{
    extend : 'Ext.data.Store',
    model : 'HOR.model.Examen',

    proxy :
    {
        type : 'rest',
        url : '/hor/rest/examen/noplanificados',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }
});
