Ext.define('HOR.store.StoreTiposExamen',
{
    extend : 'Ext.data.Store',
    model : 'HOR.model.TipoExamen',
    autoLoad : true,
    autoSync : false,
    proxy :
    {
        type : 'rest',
        url : '/hor/rest/examen/tipo',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        }
    }
});