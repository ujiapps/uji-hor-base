Ext.define('HOR.store.StoreEstudiosCompartidos',
{
    extend : 'Ext.data.Store',
    model : 'HOR.model.Estudio',

    proxy :
    {
        type : 'rest',
        url : '/hor/rest/estudio/compartido',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});