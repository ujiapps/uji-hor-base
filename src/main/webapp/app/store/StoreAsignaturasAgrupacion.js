Ext.define('HOR.store.StoreAsignaturasAgrupacion',
{
    extend : 'Ext.data.Store',
    model : 'HOR.model.AsignaturaAgrupacion',
    autoLoad : false,
    autoSync : false,
    sortOnLoad : true,

    proxy :
    {
        type : 'rest',
        url : '/hor/rest/agrupacion/1/asignaturas',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    },

    sorters : [
    {
        property : 'asignaturaId',
        direction : 'ASC'
    } ]
});