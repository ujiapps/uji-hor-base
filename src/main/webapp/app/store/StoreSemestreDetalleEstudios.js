Ext.define('HOR.store.StoreSemestreDetalleEstudios',
{
    extend : 'Ext.data.Store',
    model : 'HOR.model.SemestreDetalleEstudio',
    autoLoad : true,
    autoSync : true,

    proxy :
    {
        type : 'rest',
        url : '/hor/rest/semestredetalleestudio',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});