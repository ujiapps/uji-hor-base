Ext.define('HOR.store.StoreEventosLogs',
{
    extend : 'Ext.data.Store',
    model : 'HOR.model.EventoLog',
    autoLoad : false,
    autoSync : false,
    proxy :
    {
        type : 'rest',
        url : '/hor/rest/log/evento',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }
});