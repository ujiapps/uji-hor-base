Ext.define('HOR.store.StoreExamenes',
{
    extend : 'Ext.data.Store',
    model : 'HOR.model.Examen',

    autoLoad : false,
    proxy :
    {
        type : 'rest',
        url : '/hor/rest/examen',
        noCache : false,

        reader :
        {
            type : 'json',
            root : 'data'
        },

        writer :
        {
            type : 'json',
            nameProperty : 'mapping'
        }
    }
});
