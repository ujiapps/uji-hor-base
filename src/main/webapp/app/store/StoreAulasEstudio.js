Ext.define('HOR.store.StoreAulasEstudio',
{
    extend : 'Ext.data.Store',
    model : 'HOR.model.Aula',
    autoLoad : false,
    autoSync : false,
    sortOnLoad : true,
    proxy :
    {
        type : 'rest',
        url : '/hor/rest/aula/estudio/-1',
        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    },
    sorters : [
    {
        property : 'nombre',
        direction : 'ASC'
    } ]
});