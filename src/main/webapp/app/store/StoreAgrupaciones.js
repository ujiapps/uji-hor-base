Ext.define('HOR.store.StoreAgrupaciones',
{
    extend : 'Ext.data.Store',
    model : 'HOR.model.Agrupacion',
    autoLoad : false,
    autoSync : false,
    proxy :
    {
        type : 'rest',
        url : '/hor/rest/agrupacion',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }
});