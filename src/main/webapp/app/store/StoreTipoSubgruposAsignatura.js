Ext.define('HOR.store.StoreTipoSubgruposAsignatura',
{
    extend : 'Ext.data.Store',
    model : 'HOR.model.TipoSubgrupo',

    proxy :
    {
        type : 'rest',
        url : '/hor/rest/tiposubgrupo/asignatura',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});