Ext.define('HOR.store.StoreSemestresAsignatura',
{
    extend : 'Ext.data.Store',
    model : 'HOR.model.Semestre',

    proxy :
    {
        type : 'rest',
        url : '/hor/rest/semestre/asignatura',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});