Ext.define('HOR.store.StoreConfiguracionAjustada',
{
    extend : 'Ext.data.Store',
    model : 'HOR.model.Configuracion',

    autoLoad : false,
    autoSync : false,

    proxy :
    {
        type : 'rest',
        url : '/hor/rest/calendario/config/adjust',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json',
            successProperty : 'success'
        }
    }
});