Ext.define('HOR.store.StoreEventosPodProfesor',
{
    extend : 'Extensible.calendar.data.EventStore',
    autoLoad : true,
    proxy :
    {
        type : 'rest',
        url : '/hor/rest/calendario/eventos/profesor',
        noCache : false,

        reader :
        {
            type : 'json',
            root : 'data'
        },

        writer :
        {
            type : 'json',
            nameProperty : 'mapping'
        },

        extraParams :
        {
            semestreId : null,
            asignaturaId : null
        },
        listeners: {
            exception: function(store, res) {
                var myResponseJSON = JSON.parse(res.responseText);
                Ext.Msg.alert('Error', myResponseJSON.message);
            }
        }
    }
});
