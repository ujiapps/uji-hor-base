Ext.define('HOR.store.StoreSemestresTodos',
{
    extend : 'Ext.data.Store',
    model : 'HOR.model.Semestre',

    proxy :
    {
        type : 'memory'
    },

    data : [
    {
        id : 1,
        nombre : 'Primer semestre'
    },
    {
        id : 2,
        nombre : 'Segon semestre'
    } ]

});