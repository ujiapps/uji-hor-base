Ext.define('HOR.store.TreeStoreAsignaturas',
{
    extend : 'Ext.data.TreeStore',
    autoLoad : false,
    expanded : true,
    sorters : [
    {
        property : 'text',
        direction : 'ASC'
    } ],
    folderSort : true,

    proxy :
    {
        type : 'ajax',
        url : '/hor/rest/asignaruras/estudio/-1/tree',
        noCache : false,

        reader :
        {
            type : 'json',
            root : 'row'
        }
    }
});