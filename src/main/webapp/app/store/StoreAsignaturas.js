Ext.define('HOR.store.StoreAsignaturas',
{
    extend : 'Ext.data.Store',
    model : 'HOR.model.Asignatura',
    autoLoad : false,
    autoSync : false,
    sortOnLoad : true,

    proxy :
    {
        type : 'rest',
        url : '/hor/rest/asignatura',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    },

    sorters : [
    {
        property : 'asignaturaId',
        direction : 'ASC'
    } ]
});