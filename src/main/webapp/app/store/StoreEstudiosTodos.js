Ext.define('HOR.store.StoreEstudiosTodos',
{
    extend : 'Ext.data.Store',
    model : 'HOR.model.Estudio',

    autoLoad : true,

    proxy :
    {
        type : 'rest',
        url : '/hor/rest/estudio',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});