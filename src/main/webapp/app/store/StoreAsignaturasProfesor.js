Ext.define('HOR.store.StoreAsignaturasProfesor',
{
    extend : 'Ext.data.Store',
    model : 'HOR.model.Asignatura',
    autoLoad : false,
    autoSync : false,
    sortOnLoad : true,

    proxy :
    {
        type : 'rest',
        url : '/hor/rest/asignatura/profesor',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    },

    sorters : [
    {
        property : 'asignaturaId',
        direction : 'ASC'
    } ]
});