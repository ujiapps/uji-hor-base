Ext.define('HOR.store.StoreCalendariosAsignaturas',
{
    extend : 'Extensible.calendar.data.MemoryCalendarStore',
    requires : [ 'HOR.model.enums.TipoCalendarioAsignatura' ],
    autoLoad : true,

    proxy :
    {
        type : 'memory'
    },

    data : [ HOR.model.enums.TipoCalendarioAsignatura.SIN_PROFESOR_ASIGNADO, HOR.model.enums.TipoCalendarioAsignatura.PROFESORES_ASIGNADOS,
            HOR.model.enums.TipoCalendarioAsignatura.MISMO_PROFESOR_ASIGNADO, HOR.model.enums.TipoCalendarioAsignatura.MISMO_PROFESOR_ASIGNADO_SIN_SESIONES,
            HOR.model.enums.TipoCalendarioAsignatura.OCUPACION_PROFESOR, HOR.model.enums.TipoCalendarioAsignatura.RESTO_AREA ]

});