Ext.define('HOR.store.StoreLogsEvento',
{
    extend : 'Ext.data.Store',
    model : 'HOR.model.Log',
    autoLoad : false,
    autoSync : false,
    proxy :
    {
        type : 'rest',
        url : '/hor/rest/log/evento/-1',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }
});