Ext.define('HOR.store.StoreAreasMiPOD',
{
    extend : 'Ext.data.Store',
    model : 'HOR.model.Area',
    autoLoad : false,
    autoSync : false,
    proxy :
    {
        type : 'rest',
        url : '/hor/rest/area/mipod',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }
});