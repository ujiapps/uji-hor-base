Ext.define('HOR.store.StoreAulasGenerica',
{
    extend : 'Extensible.calendar.data.EventStore',
    autoLoad : false,
    proxy :
    {
        type : 'rest',
        url : '/hor/rest/calendario/eventos/aula/generica',
        noCache : false,

        reader :
        {
            type : 'json',
            root : 'data'
        },

        extraParams :
        {
            aulaId : null,
            semestreId : null,
            calendariosIds : null
        },

        writer :
        {
            type : 'json',
            nameProperty : 'mapping'
        },
        listeners: {
            exception: function(store, res) {
                var myResponseJSON = JSON.parse(res.responseText);
                Ext.Msg.alert('Error', myResponseJSON.message);
            }
        }
    },
    initComponent : function(cfg)
    {
        this.initConfig(cfg);
        this.callParent(arguments);
    }
});
