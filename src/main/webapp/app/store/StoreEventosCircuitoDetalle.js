Ext.define('HOR.store.StoreEventosCircuitoDetalle',
{
    extend : 'Extensible.calendar.data.EventStore',
    autoLoad : false,
    proxy :
    {
        type : 'rest',
        url : '/hor/rest/calendario/eventos/detalle/circuito',
        noCache : false,

        reader :
        {
            type : 'json',
            root : 'data'
        },

        extraParams :
        {
            estudioId : null,
            semestreId : null,
            grupoId : null,
            calendariosIds : null
        },

        writer :
        {
            type : 'json',
            nameProperty : 'mapping'
        },
        listeners: {
            exception: function(store, res) {
                var myResponseJSON = JSON.parse(res.responseText);
                Ext.Msg.alert('Error', myResponseJSON.message);
            }
        }
    }
});