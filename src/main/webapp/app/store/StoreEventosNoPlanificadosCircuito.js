Ext.define('HOR.store.StoreEventosNoPlanificadosCircuito',
{
    extend : 'Ext.data.Store',
    model : 'HOR.model.EventoNoPlanificado',

    proxy :
    {
        type : 'rest',
        url : '/hor/rest/calendario/eventos/noplanificados/circuito',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }
});
