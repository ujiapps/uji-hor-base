Ext.define('HOR.store.StoreAreas',
{
    extend : 'Ext.data.Store',
    model : 'HOR.model.Area',
    autoLoad : false,
    autoSync : false,
    proxy :
    {
        type : 'rest',
        url : '/hor/rest/area',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }
});