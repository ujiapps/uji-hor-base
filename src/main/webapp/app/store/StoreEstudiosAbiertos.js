Ext.define('HOR.store.StoreEstudiosAbiertos',
{
    extend : 'Ext.data.Store',
    model : 'HOR.model.Estudio',

    proxy :
    {
        type : 'rest',
        url : '/hor/rest/estudio/abierto',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});