Ext.define('HOR.store.StoreEventosPlanificadosAsignatura',
{
    extend : 'Extensible.calendar.data.EventStore',
    autoLoad : false,
    proxy :
    {
        type : 'rest',
        url : '/hor/rest/calendario/eventos/planificados/asignatura/generica',
        noCache : false,

        reader :
        {
            type : 'json',
            root : 'data'
        },

        writer :
        {
            type : 'json',
            nameProperty : 'mapping'
        },

        extraParams :
        {
            semestreId : null,
            asignaturaId : null
        },
        listeners: {
            exception: function(store, res) {
                var myResponseJSON = JSON.parse(res.responseText);
                Ext.Msg.alert('Error', myResponseJSON.message);
            }
        }
    }
});
