Ext.define('HOR.store.StoreDepartamentos',
{
    extend : 'Ext.data.Store',
    model : 'HOR.model.Departamento',

    proxy :
    {
        type : 'rest',
        url : '/hor/rest/departamento',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});