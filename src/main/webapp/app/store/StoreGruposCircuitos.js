Ext.define('HOR.store.StoreGruposCircuitos',
{
    extend : 'Ext.data.Store',
    model : 'HOR.model.Grupo',

    proxy :
    {
        type : 'rest',
        url : '/hor/rest/grupo/circuito',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});