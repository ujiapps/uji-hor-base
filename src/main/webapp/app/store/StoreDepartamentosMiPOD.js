Ext.define('HOR.store.StoreDepartamentosMiPOD',
{
    extend : 'Ext.data.Store',
    model : 'HOR.model.Departamento',

    proxy :
    {
        type : 'rest',
        url : '/hor/rest/departamento/mipod',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});