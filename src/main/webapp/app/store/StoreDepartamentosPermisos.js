Ext.define('HOR.store.StoreDepartamentosPermisos',
{
    extend : 'Ext.data.Store',
    model : 'HOR.model.Departamento',
    autoLoad : true,
    autoSync : false,
    proxy :
    {
        type : 'rest',
        url : '/hor/rest/departamento',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});