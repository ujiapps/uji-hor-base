Ext.define('HOR.store.StoreGruposAgrupacion',
{
    extend : 'Ext.data.Store',
    model : 'HOR.model.Grupo',

    proxy :
    {
        type : 'rest',
        url : '/hor/rest/grupo/agrupacion',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});