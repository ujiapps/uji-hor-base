Ext.define('HOR.store.StoreCursoAgrupacion',
{
    extend : 'Ext.data.Store',
    model : 'HOR.model.CursoAgrupacion',

    proxy :
    {
        type : 'rest',
        url : '/hor/rest/cursoAgrupacion',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});