Ext.define('HOR.store.StoreEventosNoPlanificadosAsignatura',
{
    extend : 'Ext.data.Store',
    model : 'HOR.model.EventoNoPlanificado',

    proxy :
    {
        type : 'rest',
        url : '/hor/rest/calendario/eventos/noplanificados/asignatura/generica',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }
});
