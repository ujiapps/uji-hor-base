Ext.define('HOR.store.StoreExamenesPlanificados',
    {
        extend : 'Extensible.calendar.data.EventStore',
        autoLoad : false,
        proxy :
        {
            type : 'rest',
            url : '/hor/rest/examen/planificados',
            noCache : false,

            reader :
            {
                type : 'json',
                root : 'data'
            },

            writer :
            {
                type : 'json',
                nameProperty : 'mapping'
            },

            extraParams :
            {
                estudioId : null,
                convocatoriaId : null
            },
            listeners: {
                exception: function(store, res) {
                    var myResponseJSON = JSON.parse(res.responseText);
                    Ext.Msg.alert('Error', myResponseJSON.message);
                }
            }
        }
    });
