Ext.define('HOR.store.StoreConvocatoria',
{
    extend : 'Ext.data.Store',
    model : 'HOR.model.Convocatoria',
    autoLoad : false,
    autoSync : false,
    proxy :
    {
        type : 'rest',
        url : '/hor/rest/examen/estudio/-1/convocatoria',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});