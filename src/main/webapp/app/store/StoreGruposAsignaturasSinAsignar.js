Ext.define('HOR.store.StoreGruposAsignaturasSinAsignar',
{
    extend : 'Ext.data.Store',
    autoLoad : false,
    autoSync : false,
    model : 'HOR.model.GrupoAsignatura',
    sortOnLoad : true,

    proxy :
    {
        type : 'rest',
        url : '/hor/rest/grupoAsignatura/sinAsignar',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        extraParams :
        {
            cursoId : null,
            semestreId : null
        },

        writer :
        {
            type : 'json',
            successProperty : 'success'
        }
    },
    sorters : [
    {
        property : 'titulo',
        direction : 'ASC'
    } ]

});