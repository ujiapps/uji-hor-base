Ext.define('HOR.store.StoreLogs',
{
    extend : 'Ext.data.Store',
    model : 'HOR.model.Log',
    autoLoad : false,
    autoSync : false,
    proxy :
    {
        type : 'rest',
        url : '/hor/rest/log',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    },

    sorters : [
    {
        property : 'fecha',
        direction : 'desc'
    } ]
});