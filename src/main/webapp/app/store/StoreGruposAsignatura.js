Ext.define('HOR.store.StoreGruposAsignatura',
{
    extend : 'Ext.data.Store',
    model : 'HOR.model.Grupo',

    proxy :
    {
        type : 'rest',
        url : '/hor/rest/grupo/asignatura',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});