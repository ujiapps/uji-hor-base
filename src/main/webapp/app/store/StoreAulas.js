Ext.define('HOR.store.StoreAulas',
{
    extend : 'Ext.data.Store',
    model : 'HOR.model.Aula',
    autoLoad : false,
    autoSync : false,
    sortOnLoad : true,
    proxy :
    {
        type : 'rest',
        url : '/hor/rest/aula',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    },
    sorters : [
    {
        property : 'ocupado',
        direction : 'DES'
    },
    {
        property : 'nombreYPlazas',
        direction : 'ASC'
    } ]
});