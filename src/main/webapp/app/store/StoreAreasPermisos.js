Ext.define('HOR.store.StoreAreasPermisos',
{
    extend : 'Ext.data.Store',
    model : 'HOR.model.Area',
    autoLoad : false,
    autoSync : false,
    proxy :
    {
        type : 'rest',
        url : '/hor/rest/area',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }
});