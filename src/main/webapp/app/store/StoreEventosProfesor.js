Ext.define('HOR.store.StoreEventosProfesor',
{
    extend : 'Ext.data.Store',
    model : 'HOR.model.Evento',
    autoLoad : false,
    autoSync : false,
    sortOnLoad : true,

    proxy :
    {
        type : 'rest',
        url : '/hor/rest/calendario/eventos/profesor',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    },

    sorters : [
    {
        property : 'id',
        direction : 'ASC'
    } ]
});