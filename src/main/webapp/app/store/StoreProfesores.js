Ext.define('HOR.store.StoreProfesores',
{
    extend : 'Ext.data.Store',
    model : 'HOR.model.Profesor',
    sortOnLoad: true,

    sorters: [{
        property : 'nombre',
        direction: 'asc'
    }],

    proxy :
    {
        type : 'rest',
        url : '/hor/rest/profesor',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }

});