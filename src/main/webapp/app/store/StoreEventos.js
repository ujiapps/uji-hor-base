Ext.define('HOR.store.StoreEventos',
{
    extend : 'Extensible.calendar.data.EventStore',
    autoLoad : false,
    proxy :
    {
        type : 'rest',
        url : '/hor/rest/calendario/eventos/generica',
        noCache : false,

        reader :
        {
            type : 'json',
            root : 'data'
        },

        extraParams :
        {
            estudioId : null,
            cursoId : null,
            semestreId : null,
            gruposId : null,
            calendariosIds : null
        },

        writer :
        {
            type : 'json',
            nameProperty : 'mapping'
        },
        listeners: {
            exception: function(store, res) {
                var myResponseJSON = JSON.parse(res.responseText);
                Ext.Msg.alert('Error', myResponseJSON.message);
            }
        }
    }
});
