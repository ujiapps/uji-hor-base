Ext.define('HOR.model.EventoLog',
{
    extend : 'Ext.data.Model',

    fields : [ 'id', 'name' ]
});