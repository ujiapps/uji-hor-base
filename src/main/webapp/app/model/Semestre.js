Ext.define('HOR.model.Semestre',
{
    extend : 'Ext.data.Model',

    fields : [ 'semestre', 'nombre' ]
});