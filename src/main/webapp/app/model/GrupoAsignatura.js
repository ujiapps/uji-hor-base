Ext.define('HOR.model.GrupoAsignatura',
{
    extend : 'Ext.data.Model',

    fields : [ 'id', 'titulo', 'asignado' ]
});