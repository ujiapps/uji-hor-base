Ext.define('HOR.model.Circuito',
{
    extend : 'Ext.data.Model',

    fields : [ 'id', 'nombre', 'plazas', 'estudioId', 'semestre', 'grupo', 'estudiosCompartidos', 'estudiosCompartidosStr' ]
});