Ext.define('HOR.model.enums.TipoCalendarioCircuito',
{
    statics :
    {
        EN_CIRCUITO :
        {
            id : 1,
            title : 'En Circuit'
        },
        FUERA_CIRCUITO :
        {
            id : 2,
            title : 'Fora de Circuit'
        }
    }
});
