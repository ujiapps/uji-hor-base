Ext.define('HOR.model.enums.TipoCalendarioAsignatura',
{
    statics :
    {
        SIN_PROFESOR_ASIGNADO :
        {
            id : 1,
            title : 'Sense professor assignat',
            color : '33'
        },
        PROFESORES_ASIGNADOS :
        {
            id : 2,
            title : 'Amb professors assignats',
            color : '17'
        },
        MISMO_PROFESOR_ASIGNADO :
        {
            id : 3,
            title : 'Assignat al professor',
            color : '20'
        },
        MISMO_PROFESOR_ASIGNADO_SIN_SESIONES :
        {
            id : 6,
            title : 'Assignat al professor sense sessions',
            color : '16'
        },
        OCUPACION_PROFESOR :
        {
            id : 4,
            title : 'Ocupació professor',
            color : '7'
        },
        RESTO_AREA :
        {
            id : 5,
            title : 'Resta de l\'àrea',
            color : '32'
        }
    }
});
