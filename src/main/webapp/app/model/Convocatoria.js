Ext.define('HOR.model.Convocatoria',
{
    extend : 'Ext.data.Model',

    fields : [ 'id', 'nombre',
    {
        name : 'inicio',
        type : 'date'
    },
    {
        name : 'fin',
        type : 'date'
    } ]
});