Ext.define('HOR.model.Asignatura',
{
    extend : 'Ext.data.Model',

    fields : [
    {
        name : 'id',
        convert : function(v, record)
        {
            return record.data.asignaturaId;
        }
    }, 'nombre', 'asignaturaId', 'codigoComun', 'semestreId',
    {
        name : 'comun',
        type : 'boolean'
    },
    {
        name : 'nombreConCodigo',
        convert : function(v, record)
        {
            if (record.data.codigoComun)
            {
                return record.data.codigoComun + ' - ' + record.data.nombre;
            }
            else
            {
                return record.data.asignaturaId + ' - ' + record.data.nombre;
            }
        }
    } ]
});