Ext.define('HOR.model.Evento',
{
    extend : 'Ext.data.Model',

    fields : [ 'id', 'nombre' ]
});