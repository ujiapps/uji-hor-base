Ext.define('HOR.model.ProfesorClase',
{
    extend : 'Ext.data.Model',

    fields : [ 'itemId', 'profesorId' ],

    proxy :
    {
        type : 'rest',
        url : '/hor/rest/profesor/clase',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }
});