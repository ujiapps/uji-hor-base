Ext.define('HOR.model.Profesor',
{
    extend : 'Ext.data.Model',

    fields : [ 'id', 'nombre', 'email', 'areaId', 'departamentoId', 'pendienteContratacion', 'creditos', 'creditosMaximos', 'creditosReduccion', 'creditosPendientes', 'nombreCategoria', 'creditosAsignados', 'creditosComputables' ],

    validations : [
    {
        type : 'length',
        field : 'id',
        min : 1
    } ]
});