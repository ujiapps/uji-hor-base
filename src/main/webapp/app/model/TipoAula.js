Ext.define('HOR.model.TipoAula',
{
    extend : 'Ext.data.Model',

    fields : [ 'nombre', 'valor' ]
});