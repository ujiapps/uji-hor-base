Ext.define('HOR.model.Centro',
{
    extend : 'Ext.data.Model',

    fields : [ 'id', 'nombre' ]
});