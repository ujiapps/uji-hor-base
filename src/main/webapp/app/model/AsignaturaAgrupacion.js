Ext.define('HOR.model.AsignaturaAgrupacion',
{
    extend : 'Ext.data.Model',

    fields : [
    {
        name : 'id',
        type : 'int',
        useNull : true
    },
    {
        name : 'agrupacionId',
        type : 'int'
    },
    {
        name : 'asignaturaId',
        type : 'string'
    },
    {
        name : 'asignaturaNombre',
        type : 'string'
    },
    {
        name : 'asignaturaCurso',
        type : 'int'
    },
    {
        name : 'subgrupoId',
        type : 'int',
        useNull : true
    },
    {
        name : 'tipoSubgrupo',
        type : 'string'
    },
    {
        name : 'subgrupo',
        convert : function(value, record)
        {
            if (record.data.tipoSubgrupo === null || record.data.subgrupoId === null)
            {
                return 'Tots';
            }

            return record.data.tipoSubgrupo + '  ' + record.data.subgrupoId;
        }
    } ]
});