Ext.define('HOR.model.Departamento',
{
    extend : 'Ext.data.Model',

    fields : [ 'id', 'nombre', 'activo', 'centroId', 'centroNombre' ],

    validations : [
    {
        type : 'length',
        field : 'id',
        min : 1
    } ]
});