Ext.define('HOR.model.Agrupacion',
{
    extend : 'Ext.data.Model',

    fields : [
    {
        name : 'id',
        type : 'int',
        useNull : true
    },
    {
        name : 'nombre',
        type : 'string'
    },
    {
        name : 'estudioId',
        type : 'int'
    },
    {
        name : 'estudiosCompartidos',
        type : 'auto'
    },
    {
        name : 'estudiosCompartidosNombre',
        type : 'auto'
    },
    {
        name : 'estudiosCompartidosStr',
        type : 'string'
    } ],

    validations : [
    {
        type : 'presence',
        field : 'nombre'
    } ]
});