Ext.define('HOR.model.TipoSubgrupo',
{
    extend : 'Ext.data.Model',

    fields : [ 'id', 'nombre' ]
});