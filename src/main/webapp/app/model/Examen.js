Ext.define('HOR.model.Examen',
{
    extend : 'Ext.data.Model',

    fields : [ 'id', 'nombre',
    {
        name : 'horaInicio',
        type : 'date',
        dateFormat : 'd/m/Y H:i:s'
    },
    {
        name : 'horaFin',
        type : 'date',
        dateFormat : 'd/m/Y H:i:s'
    },
    {
        name : 'fecha',
        type : 'date',
        dateFormat : 'd/m/Y H:i:s'
    }, 'convocatoriaId', 'convocatoriaNombre', 'asignaturasComunes', 'tipoExamenId', 'comentario', 'comentarioES', 'comentarioUK', 'comun',
    {
        name : 'definitivo',
        type : 'boolean'
    }, 'asignaturas', 'estudioId', 'codigoAulas' ]
});