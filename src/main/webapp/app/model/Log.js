Ext.define('HOR.model.Log',
{
    extend : 'Ext.data.Model',

    fields : [ 'id', 'fecha', 'usuario', 'eventoId', 'descripcion', 'tipoAccion', 'tipoAccionNombre' ]
});