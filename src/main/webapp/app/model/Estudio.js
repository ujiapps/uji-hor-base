Ext.define('HOR.model.Estudio',
{
    extend : 'Ext.data.Model',

    fields : [ 'id', 'nombre' ]
});