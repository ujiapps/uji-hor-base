Ext.define('HOR.model.SemestreDetalleEstudio',
{
    extend : 'Ext.data.Model',

    fields : [ 'id',
    {
        name : 'fechaFin',
        type : 'date',
        dateFormat : 'd/m/Y H:i:s'
    },
    {
        name : 'fechaInicio',
        type : 'date',
        dateFormat : 'd/m/Y H:i:s'
    },
    {
        name : 'fechaExamenesFin',
        type : 'date',
        dateFormat : 'd/m/Y H:i:s'
    },
    {
        name : 'fechaExamenesInicio',
        type : 'date',
        dateFormat : 'd/m/Y H:i:s'
    },
    {
        name : 'fechaExamenesInicioC2',
        type : 'date',
        dateFormat : 'd/m/Y H:i:s'
    },
    {
        name : 'fechaExamenesFinC2',
        type : 'date',
        dateFormat : 'd/m/Y H:i:s'
    },
    {
        name : 'estudioId',
        type : 'int',
        useNull : true
    },
    {
        name : 'semestreDetalleId',
        type : 'int',
        useNull : true
    },
    {
        name : 'numeroSemanas',
        type : 'int'
    },
    {
        name : 'cursoAcademico',
        type : 'int'
    },
    {
        name : '_estudio',
        type : 'auto'
    },
    {
        name : '_semestreDetalle',
        type : 'auto'
    } ],

    validations : [
    {
        type : 'presence',
        field : 'fechaInicio'
    },
    {
        type : 'presence',
        field : 'fechaFin'
    } ]
});