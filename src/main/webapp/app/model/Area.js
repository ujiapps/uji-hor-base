Ext.define('HOR.model.Area',
{
    extend : 'Ext.data.Model',

    fields : [ 'id', 'nombre' ]
});