Ext.define('HOR.model.PlantaEdificio',
{
    extend : 'Ext.data.Model',

    fields : [ 'nombre', 'valor' ]
});