Ext.define('HOR.model.Aula',
{
    extend : 'Ext.data.Model',

    fields : [ 'id', 'codigo', 'nombre', 'ocupado', 'plazas',
    {
        name : 'nombreYPlazas',
        mapping : 'nombre',
        convert : function(v, record)
        {
            var res;

            if (record.data.plazas)
            {
                res = v + " (" + record.data.plazas + " places)";
            }
            else
            {
                res = v + " (sense places)";
            }

            if (record.data.ocupado == 'N') {
                res += ' [LLIURE]';
            }

            return res;
        }
    } ]
});