Ext.define('HOR.model.CursoAgrupacion',
{
    extend : 'Ext.data.Model',

    fields : [
    {
        name : 'tipoId',
        type : 'int'
    },
    {
        name : 'tipoNombre',
        type : 'string'
    },
    {
        name : 'valor',
        type : 'int'
    },
    {
        name : 'nombre',
        type : 'string'
    } ]
});