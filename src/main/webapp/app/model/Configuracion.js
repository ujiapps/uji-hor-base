Ext.define('HOR.model.Configuracion',
{
    extend : 'Ext.data.Model',

    fields : [ 'estudioId', 'semestreId', 'cursoId', 'grupoId', 'horaInicio', 'horaFin' ]

});