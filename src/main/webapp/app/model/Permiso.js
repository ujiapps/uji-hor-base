Ext.define('HOR.model.Permiso',
{
    extend : 'Ext.data.Model',

    fields : [ 'id', 'persona', 'personaId', 'tipoCargo', 'tipoCargoId', 'estudio', 'estudioId', 'departamento', 'departamentoId', 'area', 'areaId' ]
});