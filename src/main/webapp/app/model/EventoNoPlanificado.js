Ext.define('HOR.model.EventoNoPlanificado',
{
    extend : 'Ext.data.Model',

    fields : [ 'id', 'title' , 'profesores', 'nombreAsignatura', {
        name: 'cid',
        type: 'int'
    }, 'creditos', 'creditosAsignados', 'creditosComputables', 'creditosAsignadosComputables' ]
});