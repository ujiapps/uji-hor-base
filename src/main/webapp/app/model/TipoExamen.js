Ext.define('HOR.model.TipoExamen',
{
    extend : 'Ext.data.Model',

    fields : [ 'id', 'nombre', 'porDefecto', 'codigo' ]
});