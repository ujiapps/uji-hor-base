Ext.define('HOR.model.Cargo',
{
    extend : 'Ext.data.Model',

    fields : [ 'id', 'nombre',
    {
        name : 'estudio',
        type : 'int'
    },
    {
        name : 'departamento',
        type : 'int'
    } ]
});