Ext.define('HOR.model.Informacion',
{
    extend : 'Ext.data.Model',
    fields : [ 'id', 'orden', 'texto', 'fechaInicio', 'fechaFin' ]
});