Ext.Loader.setConfig(
{
    enabled : true,
    paths :
    {
        'Ext.ux' : '/hor/examples/ux',
        'Ext.ux.uji' : '/hor/Ext/ux/uji',
        'Extensible' : '/hor/extensible-1.5.2/src',
        'Event' : '/hor/app/event',
        'Ext.ux.form.field' : '/hor/js'
    }
});

Ext.require('Ext.data.proxy.Rest');
Ext.require('Ext.data.reader.Xml');
Ext.require('Ext.data.reader.Json');
Ext.require('Ext.data.writer.Xml');
Ext.require('Ext.data.writer.Json');
Ext.require('Ext.data.TreeStore');
Ext.require('Ext.container.Viewport');
Ext.require('Ext.layout.container.Border');
Ext.require('Ext.tab.Panel');
Ext.require('Ext.tree.Panel');
Ext.require('Ext.ux.TabCloseMenu');
Ext.require('Ext.ux.uji.TabPanel');
Ext.require('Ext.ux.uji.form.LookupComboBox');
Ext.require('Ext.ux.uji.form.LookupWindow');
Ext.require('Ext.ux.uji.form.model.Lookup');
Ext.require('Ext.ux.uji.ApplicationViewport');
Ext.require('Ext.form.field.Trigger');
Ext.require('Ext.form.FieldSet');
Ext.require('Ext.form.field.Text');
Ext.require('Ext.form.Label');
Ext.require('Ext.toolbar.Toolbar');
Ext.require('Ext.toolbar.Spacer');
Ext.require('Extensible.calendar.menu.Event');
Ext.require('Extensible.calendar.data.MemoryCalendarStore');
Ext.require('Extensible.calendar.data.EventMappings');
Ext.require('Extensible.calendar.data.EventStore');
Ext.require('Extensible.calendar.CalendarPanel');
Ext.require('Extensible.calendar.template.BoxLayout');
Ext.require('Event.form.field.DateRange');
Ext.require('Ext.form.field.Checkbox');
Ext.require('Ext.form.field.Radio');
Ext.require('Ext.form.field.Hidden');
Ext.require('Ext.form.RadioGroup');
Ext.require('Event.form.field.RadioNumber');
Ext.require('Event.form.field.RadioDate');
Ext.require('Event.form.field.DetalleManual');
Ext.require('Ext.layout.container.Table');
Ext.require('Event.form.DetalleClases');
Ext.require('Ext.grid.Panel');
Ext.require('Ext.Date');
Ext.require('Event.form.PanelInfo');
Ext.require('Ext.ux.form.field.BoxSelect');

// var login = 'borillo';

function fixLoadMaskBug(store, combo)
{
    store.on('load', function(store, records, successful, options)
    {
        if (successful && Ext.typeOf(combo.getPicker().loadMask) !== "boolean")
        {
            combo.getPicker().loadMask.hide();
        }
    });
}

function overrideDatePicker()
{
    Ext.apply(Ext.DatePicker.prototype,
    {
        renderTpl : [ '<div id="{id}-innerEl" role="grid">', '<div role="presentation" class="{baseCls}-header">',
                '<div class="{baseCls}-prev"><a id="{id}-prevEl" href="#" role="button" title="{prevText}"></a></div>',
                '<div class="{baseCls}-month" id="{id}-middleBtnEl">{%this.renderMonthBtn(values, out)%}</div>',
                '<div class="{baseCls}-next"><a id="{id}-nextEl" href="#" role="button" title="{nextText}"></a></div>', '</div>',
                '<table id="{id}-eventEl" class="{baseCls}-inner" cellspacing="0" role="presentation">', '<thead role="presentation"><tr role="presentation">', '<tpl for="dayNames">',
                '<th role="columnheader" title="{.}"><span>{.:this.firstInitial}</span></th>', '</tpl>', '</tr></thead>', '<tbody role="presentation"><tr role="presentation">', '<tpl for="days">',
                '{#:this.isEndOfWeek}', '<td role="gridcell" id="{[Ext.id()]}">', '<a role="presentation" href="#" hidefocus="on" class="{parent.baseCls}-date" tabIndex="1">',
                '<em role="presentation"><span role="presentation"></span></em>', '</a>', '</td>', '</tpl>', '</tr></tbody>', '</table>', '<tpl if="showToday">',
                '<div id="{id}-footerEl" role="presentation" class="{baseCls}-footer">{%this.renderTodayBtn(values, out)%}</div>', '</tpl>', '</div>',
                {
                    firstInitial : function(value)
                    {
                        var arr =
                        {
                            Dilluns : "L",
                            Dimarts : "M",
                            Dimecres : "C",
                            Dijous : "J",
                            Divendres : "V",
                            Dissabte : "S",
                            Diumenge : "G"
                        };
                        return arr[value];
                    },
                    isEndOfWeek : function(value)
                    {
                        // convert from 1 based index to 0 based
                        // by decrementing value once.
                        value--;
                        var end = value % 7 === 0 && value !== 0;
                        return end ? '</tr><tr role="row">' : '';
                    },
                    renderTodayBtn : function(values, out)
                    {
                        Ext.DomHelper.generateMarkup(values.$comp.todayBtn.getRenderTree(), out);
                    },
                    renderMonthBtn : function(values, out)
                    {
                        Ext.DomHelper.generateMarkup(values.$comp.monthBtn.getRenderTree(), out);
                    }
                } ],

    });
}

Ext.application(
{
    name : 'HOR',
    appFolder : 'app',
    autoCreateViewport : false,

    views : [ 'dashboard.PanelDashboard', 'horarios.PanelHorarios', 'horarios.FiltroGrupos', 'horarios.PanelCalendario', 'horarios.PanelCalendarioDetalle', 'horarios.SelectorIntervaloHorario',
            'commons.MenuSuperior', 'permisos.PanelPermisos', 'permisos.VentanaNewPermiso', 'semestres.PanelSemestres', 'ApplicationViewport', 'aulas.asignacion.FormAsignacionAulas',
            'aulas.calendar.PanelCalendarioAulas', 'aulas.calendar.PanelCalendarioPorAula', 'aulas.calendar.PanelCalendarioDetallePorAula', 'aulas.calendar.FiltroAulas',
            'aulas.calendar.SelectorAulas', 'circuitos.FiltroCircuitos', 'circuitos.PanelCalendarioCircuitos', 'circuitos.PanelCalendarioCircuitosDetalle', 'circuitos.SelectorCircuitos',
            'logs.PanelLogs', 'logs.FiltroLogs', 'logs.GridLogs', 'pod.PanelEdicion', 'mipod.PanelSeleccion' ],
    controllers : [ 'ControllerDashboards', 'ControllerCalendario', 'ControllerGrupoAsignatura', 'ControllerFiltroCalendario', 'ControllerConfiguracion', 'ControllerPrincipal', 'ControllerPermisos',
            'ControllerSemestreDetalle', 'ControllerFiltroAsignacionAulas', 'ControllerAsignacionAulasForm', 'ControllerFiltroAulasCalendario', 'ControllerSelectorAulasCalendario',
            'ControllerCalendarioAulas', 'ControllerFiltroCircuitos', 'ControllerGestionCircuitos', 'ControllerCalendarioCircuitos', 'ControllerLogs', 'ControllerFiltroProfesores',
            'ControllerCalendarioAsignaturas', 'ControllerAsignacionProfesorForm', 'ControllerExamenes', 'ControllerAsignacionAulasPanelCalendarioPorAulas', 'ControllerMiPOD', 'ControllerFechas',
            'ControllerAgrupaciones', 'ControllerCalendarioConsulta', 'ControllerFiltroCalendarioConsulta', 'ControllerGrupoAsignaturaConsulta', 'ControllerSesiones' ],

    launch : function()
    {
        Ext.Loader.loadScriptFile('//static.uji.es/js/extjs/extjs-4.1.1/locale/ext-lang-ca.js', function()
        {
            Ext.Loader.loadScriptFile('//static.uji.es/js/extjs/extensible-1.5.2/src/locale/extensible-lang-ca.js', function()
            {
                Ext.Loader.loadScriptFile('/hor/app/config/calendar.js', function()
                {
                    if (Ext.DatePicker)
                    {
                        overrideDatePicker();
                    }

                    var viewport = Ext.create('HOR.view.ApplicationViewport',
                    {
                        codigoAplicacion : 'HOR',
                        tituloAplicacion : 'Gestión de horarios',
                        dashboard : true
                    });
                    viewport.addNewTab('HOR.view.dashboard.PanelDashboard');

                    Ext.onReady(function()
                    {
                        if (Ext.Date)
                        {
                            Ext.Date.getShortDayName = function(day)
                            {
                                return Ext.Date.dayNames[day].substring(0, 4);
                            };
                        }
                    });
                });
            });
        });
    }
});