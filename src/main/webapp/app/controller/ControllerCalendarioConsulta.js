Ext.define('HOR.controller.ControllerCalendarioConsulta',
{
    extend : 'Ext.app.Controller',
    stores : [ 'StoreCalendarios', 'StoreEventos', 'StoreEventosDetalle', 'StoreGruposAsignaturasSinAsignar', 'StoreConfiguracionAjustada', 'StoreLogsEvento' ],
    model : [ 'Configuracion' ],
    refs : [
    {
        selector : 'panelHorariosConsulta filtroGruposConsulta',
        ref : 'filtroGrupos'
    },
    {
        selector : 'selectorGruposConsulta',
        ref : 'selectorGrupos'
    },
    {
        selector : 'panelCalendarioConsulta',
        ref : 'panelCalendario'
    },
    {
        selector : 'panelHorariosConsulta selectorCalendariosConsulta',
        ref : 'selectorCalendarios'
    },
    {
        selector : 'button[name=calendarioDetalle]',
        ref : 'botonCalendarioDetalle'
    },
    {
        selector : 'button[name=calendarioGenerica]',
        ref : 'botonCalendarioGenerica'
    },
    {
        selector : 'panelHorariosConsulta filtroGruposConsulta combobox[name=grupo]',
        ref : 'comboGrupos'
    },
    {
        selector : 'filtroGruposConsulta selectorCursoAgrupacionConsulta[name=cursoAgrupacion]',
        ref : 'selectorCursoAgrupacion'
    } ],

    init : function()
    {
        this.control(
        {
            'panelHorariosConsulta filtroGruposConsulta combobox[name=grupo]' :
            {
                change : this.refreshActiveCalendar
            },
            'panelHorariosConsulta filtroGruposConsulta combobox[name=asignatura]' :
            {
                change : this.refreshActiveCalendar
            },
            'panelHorariosConsulta' :
            {
                refreshCalendar : this.refreshCalendar,
                refreshCalendarDetalle : this.refreshCalendarDetalle,
                afterrender : this.onPanelCalendarioRendered
            },
            'panelHorariosConsulta selectorCalendariosConsulta checkbox' :
            {
                change : this.filtroTipoClaseChanged
            },
            'panelCalendarioConsulta' :
            {
                eventresize : function()
                {
                    return false;
                },
                dayclick : function()
                {
                    return false;
                },
                rangeselect : function()
                {
                    return false;
                },
                refresh: function() {
                    this.refreshCalendar();
                },
                beforeeventmove : function()
                {
                    return false;
                },
                eventclick : function()
                {
                    return false;
                }
            },
            'panelHorariosConsulta button[name=calendarioDetalle]' :
            {
                click : function(button)
                {
                    if (!button.pressed)
                    {
                        button.toggle();
                    }
                    var otherButton = this.getBotonCalendarioGenerica();
                    if (otherButton.pressed)
                    {
                        otherButton.toggle();
                    }
                    this.getSelectorGrupos().setVisible(false);
                    this.refreshCalendarDetalle();
                }

            },
            'panelHorariosConsulta button[name=calendarioGenerica]' :
            {
                click : function(button)
                {
                    if (!button.pressed)
                    {
                        button.toggle();
                    }
                    var otherButton = this.getBotonCalendarioDetalle();
                    if (otherButton.pressed)
                    {
                        otherButton.toggle();
                    }
                    this.getSelectorGrupos().setVisible(true);
                    this.refreshCalendar();
                }
            },
            'panelHorariosConsulta button[name=imprimir]' :
            {
                click : this.imprimirCalendario
            },
            'panelHorariosConsulta [name=imprimirSemestre]' :
            {
                click : this.imprimirSemestreCalendario
            },
            'panelHorariosConsulta [name=imprimirSesionesPOD]' :
            {
                click : this.imprimirSesionesPOD
            },
            'panelHorariosConsulta [name=imprimirTitulacion]' :
            {
                click : this.imprimirTitulacionCalendario
            },
            'panelHorariosConsulta [name=imprimirAgrupaciones]' :
            {
                click : this.imprimirAgrupacionesCalendario
            },
            'panelHorariosConsulta button[name=validar]' :
            {
                click : this.mostrarValidaciones
            }
        });
    },

    debounceTipoClase: null,

    filtroTipoClaseChanged: function() {
        if (this.debounceTipoClase) {
            clearTimeout(this.debounceTipoClase);
        }

        var ref = this;
        this.debounceTipoClase = setTimeout(function() {
            ref.debounceTipoClase = null;
            ref.refreshActiveCalendar();
        }, 1500);
    },

    refreshActiveCalendar : function()
    {
        if (this.getFiltroGrupos().down('button[name=calendarioDetalle]').pressed)
        {
            this.refreshCalendarDetalle();
        }
        else
        {
            this.refreshCalendar();
        }
    },

    refreshCalendarDetalle : function()
    {
        var titulaciones = this.getFiltroGrupos().down('combobox[name=estudio]');
        var semestres = this.getFiltroGrupos().down('combobox[name=semestre]');
        var grupos = this.getFiltroGrupos().getGruposSelected();
        var asignaturas = this.getFiltroGrupos().getAsignaturasSelected();
        var calendarios = this.getSelectorCalendarios().getCalendarsSelected();
        var cursoId = this.getSelectorCursoAgrupacion().getCursoId();
        var agrupacionId = this.getSelectorCursoAgrupacion().getAgrupacionId();
        var storeConfiguracion = this.getStoreConfiguracionAjustadaStore();

        if (!((cursoId || agrupacionId) && titulaciones.getValue() && grupos && semestres.getValue()))
        {
            return;
        }

        var ref = this;
        storeConfiguracion.load(
        {
            params :
            {
                estudioId : titulaciones.getValue(),
                cursoId : this.getSelectorCursoAgrupacion().getCursoId(),
                agrupacionId : this.getSelectorCursoAgrupacion().getAgrupacionId(),
                semestreId : semestres.getValue(),
                gruposId : grupos,
                asignaturasId : asignaturas
            },
            scope : this,
            callback : function(records, operation, success)
            {
                if (success)
                {
                    var record = records[0];
                    var fechaInicio = record.get('horaInicio');
                    var fechaFin = record.get('horaFin');

                    var inicio = Ext.Date.parse(fechaInicio, 'd/m/Y H:i:s', true);
                    var fin = Ext.Date.parse(fechaFin, 'd/m/Y H:i:s', true);
                    var horaInicio = Ext.Date.format(inicio, 'H');
                    var horaFin = Ext.Date.format(fin, 'H');

                    var panelCalendario = ref.getPanelCalendario();
                    if (!panelCalendario)
                    {
                        panelCalendario = ref.getPanelCalendarioDetalle();
                    }
                    var panelPadre = panelCalendario.up('panel');

                    Ext.Array.each(Ext.ComponentQuery.query('panelCalendarioConsulta'), function(panel)
                    {
                        panel.destroy();
                    });

                    Ext.Array.each(Ext.ComponentQuery.query('panelCalendarioDetalleConsulta'), function(panel)
                    {
                        panel.destroy();
                    });

                    var eventos = Ext.create('HOR.store.StoreEventosDetalle');

                    Extensible.calendar.data.EventModel.reconfigure();
                    Ext.Ajax.request(
                    {
                        url : '/hor/rest/semestredetalle/estudio/' + titulaciones.getValue() + "/semestre/" + semestres.getValue(),
                        params :
                        {
                            cursoId : this.getSelectorCursoAgrupacion().getCursoId()
                        },
                        method : 'GET',
                        success : function(response)
                        {
                            var jsonResp = Ext.decode(response.responseText);
                            var cadenaFechaInicio = jsonResp.data[0].fechaInicio;
                            var inicio = Ext.Date.parse(cadenaFechaInicio, 'd/m/Y H:i:s', true);
                            var fin = new Date();
                            fin.setDate(inicio.getDate() + 7);

                            panelPadre.add(
                            {
                                xtype : 'panelCalendarioDetalleConsulta',
                                eventStore : eventos,
                                showMultiDayView : true,
                                startDate : inicio,
                                enableEditDetails : false,
                                viewConfig :
                                {
                                    viewStartHour : horaInicio,
                                    viewEndHour : horaFin
                                },

                                onInitDrag : function()
                                {

                                },
                                listeners :
                                {
                                    dayclick : function(dt, allday, el)
                                    {
                                        return false;
                                    },

                                    eventclick : function()
                                    {
                                        return false;
                                    },

                                    rangeselect : function()
                                    {
                                        return false;
                                    },

                                    afterrender : function()
                                    {
                                        params =
                                        {
                                            estudioId : titulaciones.getValue(),
                                            cursoId : ref.getSelectorCursoAgrupacion().getCursoId(),
                                            agrupacionId : ref.getSelectorCursoAgrupacion().getAgrupacionId(),
                                            semestreId : semestres.getValue(),
                                            calendariosIds : calendarios,
                                            asignaturasId : asignaturas,
                                            gruposId : grupos,
                                            startDate : inicio,
                                            endDate : fin
                                        };

                                        eventos.getProxy().extraParams = params;
                                        this.setStartDate(inicio);
                                    },

                                    beforeeventmove : function(cal, rec, date)
                                    {
                                        eventos.rejectChanges();
                                        if (rec.get(Extensible.calendar.data.EventMappings.CalendarId.name) == "100")
                                        {
                                            return false;
                                        }
                                    },

                                    eventover : function(calendar, record, event)
                                    {
                                        ref.mostrarInfoAdicionalEvento(event, record);
                                    },

                                    eventout : function()
                                    {
                                        if (ref.tooltip)
                                        {
                                            ref.tooltip.destroy();
                                            delete ref.tooltip;
                                        }
                                    }
                                }
                            });
                        }
                    });
                }
            }
        });
    },

    refreshCalendar : function()
    {
        var titulaciones = this.getFiltroGrupos().down('combobox[name=estudio]');
        var semestres = this.getFiltroGrupos().down('combobox[name=semestre]');
        var grupos = this.getFiltroGrupos().getGruposSelected();
        var asignaturas = this.getFiltroGrupos().getAsignaturasSelected();
        var calendarios = this.getSelectorCalendarios().getCalendarsSelected();
        var cursoId = this.getSelectorCursoAgrupacion().getCursoId();
        var agrupacionId = this.getSelectorCursoAgrupacion().getAgrupacionId();
        var storeConfiguracion = this.getStoreConfiguracionAjustadaStore();

        if (!((cursoId || agrupacionId) && titulaciones.getValue() && grupos && semestres.getValue()))
        {
            return;
        }

        var ref = this;
        storeConfiguracion.load(
        {
            params :
            {
                estudioId : titulaciones.getValue(),
                cursoId : this.getSelectorCursoAgrupacion().getCursoId(),
                agrupacionId : this.getSelectorCursoAgrupacion().getAgrupacionId(),
                semestreId : semestres.getValue(),
                gruposId : grupos,
                asignaturasId : asignaturas
            },
            scope : this,
            callback : function(records, operation, success)
            {
                if (success)
                {
                    var record = records[0];
                    var fechaInicio = record.get('horaInicio');
                    var fechaFin = record.get('horaFin');

                    var inicio = Ext.Date.parse(fechaInicio, 'd/m/Y H:i:s', true);
                    var fin = Ext.Date.parse(fechaFin, 'd/m/Y H:i:s', true);
                    var horaInicio = Ext.Date.format(inicio, 'H');
                    var horaFin = Ext.Date.format(fin, 'H');

                    var panelCalendario = ref.getPanelCalendario();
                    if (!panelCalendario)
                    {
                        panelCalendario = ref.getPanelCalendarioDetalle();
                    }
                    var panelPadre = panelCalendario.up('panel');

                    Ext.Array.each(Ext.ComponentQuery.query('panelCalendarioConsulta'), function(panel)
                    {
                        panel.destroy();
                    });

                    Ext.Array.each(Ext.ComponentQuery.query('panelCalendarioDetalleConsulta'), function(panel)
                    {
                        panel.destroy();
                    });

                    var eventos = Ext.create('HOR.store.StoreEventos');

                    eventos.addListener('write', function()
                    {
                        ref.getPanelCalendario().getEventStore().reload();

                    }, this);

                    Extensible.calendar.data.EventModel.reconfigure();

                    params =
                    {
                        estudioId : titulaciones.getValue(),
                        cursoId : ref.getSelectorCursoAgrupacion().getCursoId(),
                        agrupacionId : ref.getSelectorCursoAgrupacion().getAgrupacionId(),
                        semestreId : semestres.getValue(),
                        calendariosIds : calendarios,
                        gruposId : grupos,
                        asignaturasId : asignaturas
                    };
                    eventos.getProxy().extraParams = params;

                    panelPadre.add(
                    {
                        xtype : 'panelCalendarioConsulta',
                        eventStore : eventos,
                        showMultiDayView : true,
                        viewConfig :
                        {
                            viewStartHour : horaInicio,
                            viewEndHour : horaFin
                        },
                        listeners :
                        {
                            eventover : function(calendar, record, event)
                            {
                                ref.mostrarInfoAdicionalEvento(event, record);
                            },
                            eventout : function()
                            {
                                if (ref.tooltip)
                                {
                                    ref.tooltip.destroy();
                                    delete ref.tooltip;
                                }
                            },
                            afterrender : function()
                            {
                                eventos.load();
                            }
                        }
                    });
                }
            }
        });
    },

    mostrarInfoAdicionalEvento : function(event, record)
    {
        if (event.disabled)
        {
            this.tooltip.hide();
            return;
        }

        if (!this.tooltip)
        {
            var panelCalendario = Ext.ComponentQuery.query('panelCalendarioConsulta,panelCalendarioDetalleConsulta')[0];
            this.tooltip = Ext.create('Ext.tip.ToolTip',
            {
                hidden : true,
                target : panelCalendario.getEl(),
                trackMouse : true,
                autoHide : false
            });
        }
        if (record.data.NombreAsignatura)
        {
            var panelCalendario = Ext.ComponentQuery.query('panelCalendarioConsulta,panelCalendarioDetalleConsulta')[0];
            this.tooltip.setTarget(panelCalendario.getEl());

            var nombre = record.data.NombreAsignatura;
            var plazas = record.data.PlazasAula;
            var comunes = record.data.Comunes;
            var fechasDetalles = record.data.FechasDetalles;
            var tooltipHtml = nombre + '<br />';
            tooltipHtml += record.data.Title;

            if (comunes)
            {
                tooltipHtml += ' (' + comunes + ')';
            }
            if (plazas)
            {
                tooltipHtml += '<br/>Plaçes aula: ' + plazas;
            }
            if (fechasDetalles)
            {
                tooltipHtml += '<br/>Dates: ' + fechasDetalles;
            }
            if (record.data.Comentarios)
            {
                tooltipHtml += '<br/>Comentari: ' + record.data.Comentarios;
            }

            this.tooltip.update(tooltipHtml);
            this.tooltip.show();
        }
        else
        {
            if (this.tooltip)
            {
                this.tooltip.destroy();
                delete this.tooltip;
            }
        }
    },

    onPanelCalendarioRendered : function()
    {

        var panelPadre = Ext.ComponentQuery.query('panelHorariosConsulta panel[name=contenedorCalendario]')[0];
        var eventos = Ext.create('HOR.store.StoreEventos');
        Extensible.calendar.data.EventModel.reconfigure();

        panelPadre.add(
        {
            xtype : 'panelCalendarioConsulta',
            flex : 1,
            eventStore : eventos,
            showMultiDayView : true,
            viewConfig :
            {
                viewStartHour : 8,
                viewEndHour : 22
            }
        });
    },

    imprimirCalendario : function(titulacion, curso, agrupacion, semestre, grupo)
    {
        if (this.getBotonCalendarioGenerica().pressed)
        {
            if (curso != null)
            {
                window.open("http://www.uji.es/cocoon/" + session + "/" + titulacion + "/" + curso + "/" + semestre + "/" + grupo + "/horario-semana-generica.pdf");
            }
            else
            {
                window.open("http://www.uji.es/cocoon/" + session + "/" + titulacion + "/" + agrupacion + "/" + semestre + "/" + grupo + "/horario-agrupacion-semana-generica.pdf");
            }
        }
        else
        {
            if (curso != null)
            {
                window.open("http://www.uji.es/cocoon/" + session + "/" + titulacion + "/" + curso + "/" + semestre + "/" + grupo + "/horario-semana-detalle.pdf");
            }
            else
            {
                window.open("http://www.uji.es/cocoon/" + session + "/" + titulacion + "/" + agrupacion + "/" + semestre + "/" + grupo + "/horario-agrupacion-semana-detalle.pdf");
            }
        }
    },

    imprimirSemestreCalendario : function()
    {
        var titulacion = this.getFiltroGrupos().down('combobox[name=estudio]').getValue();
        var curso = this.getSelectorCursoAgrupacion().getCursoId();
        var agrupacion = this.getSelectorCursoAgrupacion().getAgrupacionId();
        var semestre = this.getFiltroGrupos().down('combobox[name=semestre]').getValue();
        var grupo = this.getFiltroGrupos().down('combobox[name=grupo]').getValue();
        this.imprimirCalendario(titulacion, curso, agrupacion, semestre, grupo);
    },

    imprimirSesionesPOD : function()
    {
        var titulacion = this.getFiltroGrupos().down('combobox[name=estudio]').getValue();
        var curso = this.getSelectorCursoAgrupacion().getCursoId();
        var semestre = this.getFiltroGrupos().down('combobox[name=semestre]').getValue();
        window.open("http://www.uji.es/cocoon/" + session + "/" + titulacion + "/" + curso + "/" + semestre + "/validacion-sesiones-hor.pdf");
    },

    imprimirTitulacionCalendario : function()
    {
        var titulacion = this.getFiltroGrupos().down('combobox[name=estudio]').getValue();
        this.imprimirCalendario(titulacion, -1, null, -1, -1);
    },

    imprimirAgrupacionesCalendario : function()
    {
        var titulacion = this.getFiltroGrupos().down('combobox[name=estudio]').getValue();
        this.imprimirCalendario(titulacion, null, -1, -1, -1);
    },

    mostrarValidaciones : function()
    {
        var titulacion = this.getFiltroGrupos().down('combobox[name=estudio]').getValue();
        var curso = this.getSelectorCursoAgrupacion().getCursoId();
        var semestre = this.getFiltroGrupos().down('combobox[name=semestre]').getValue();
        var grupo = this.getFiltroGrupos().down('combobox[name=grupo]').getValue();

        if (curso === null)
        {
            Ext.Msg.alert('Informació', 'Les validacions no estan disponibles per agrupació, seleccioneu un curs per poder mostrar-les');
            return;
        }
        window.open("http://www.uji.es/cocoon/" + session + "/" + titulacion + "/" + curso + "/" + semestre + "/" + grupo + "/validaciones-horarios.pdf");
    }

});