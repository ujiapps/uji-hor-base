Ext.define('HOR.controller.ControllerGrupoAsignatura',
{
    extend : 'Ext.app.Controller',
    stores : [ 'StoreGruposAsignaturasSinAsignar', 'StoreCalendarios' ],
    model : [ 'GrupoAsignatura' ],
    refs : [
    {
        selector : 'selectorGrupos',
        ref : 'selectorGrupos'
    },
    {
        selector : 'panelHorarios filtroGrupos',
        ref : 'filtroGrupos'
    },
    {
        selector : 'panelHorarios selectorCalendarios',
        ref : 'selectorCalendarios'
    },
    {
        selector : 'panelHorarios formAsignacionAulas',
        ref : 'formAsignacionAulas'
    },
    {
        selector : 'panelHorarios filtroGrupos combobox[name=grupo]',
        ref : 'comboGrupos'
    },
    {
        selector : 'filtroGrupos selectorCursoAgrupacion[name=cursoAgrupacion]',
        ref : 'selectorCursoAgrupacion'
    } ],

    initComponent : function()
    {
        this.addEvents('updateGrupos');
        this.callParent(arguments);
    },
    init : function()
    {
        var me = this;
        this.control(
        {
            'selectorGrupos' :
            {
                updateGrupos : this.updateAsignaturasSinAsignar
            },
            'panelHorarios filtroGrupos combobox[name=grupo]' :
            {
                change : this.updateAsignaturasSinAsignar
            },
            'panelHorarios filtroGrupos combobox[name=asignatura]' :
            {
                change : this.updateAsignaturasSinAsignar
            },
            'panelHorarios selectorCalendarios checkbox' :
            {
                change : this.filtroTipoClaseChanged
            },
            'panelCalendario' :
            {
                editdetails : function()
                {
                    me.getFiltroGrupos().setDisabled(true);
                    me.getSelectorGrupos().setDisabled(true);
                    me.getSelectorCalendarios().setDisabled(true);

                },
                eventcancel : function()
                {
                    me.getFiltroGrupos().setDisabled(false);
                    me.getSelectorGrupos().setDisabled(false);
                    me.getSelectorCalendarios().setDisabled(false);

                },
                eventasignaaula : function()
                {
                    me.getFiltroGrupos().setDisabled(true);
                    me.getSelectorGrupos().setDisabled(true);
                    me.getSelectorCalendarios().setDisabled(true);
                }
            },
            'formAsignacionAulas' :
            {
                eventasigaaulacancel : function()
                {
                    me.getFiltroGrupos().setDisabled(false);
                    me.getSelectorGrupos().setDisabled(false);
                    me.getSelectorCalendarios().setDisabled(false);
                }
            }
        });
    },

    debounceTipoClase: null,

    filtroTipoClaseChanged: function() {
        if (this.debounceTipoClase) {
            clearTimeout(this.debounceTipoClase);
        }

        var ref = this;
        this.debounceTipoClase = setTimeout(function() {
            ref.debounceTipoClase = null;
            ref.updateAsignaturasSinAsignar();
        }, 1500);
    },

    updateAsignaturasSinAsignar : function()
    {
        if (this.getComboGrupos().getValue() == '')
        {
            return;
        }

        var store = this.getStoreGruposAsignaturasSinAsignarStore();

        var estudio = this.getFiltroGrupos().down('combobox[name=estudio]').getValue();
        var curso = this.getSelectorCursoAgrupacion().getCursoId();
        var agrupacion = this.getSelectorCursoAgrupacion().getAgrupacionId();
        var semestre = this.getFiltroGrupos().down('combobox[name=semestre]').getValue();
        var grupo = this.getFiltroGrupos().getGruposSelected();

        var calendarios = this.getSelectorCalendarios().getCalendarsSelected();
        var asignaturas = this.getFiltroGrupos().getAsignaturasSelected();

        store.load(
        {
            callback : this.ordenaStore,
            params :
            {
                estudioId : estudio,
                cursoId : curso,
                agrupacionId : agrupacion,
                semestreId : semestre,
                gruposId : grupo,
                calendariosIds : calendarios,
                asignaturasIds : asignaturas
            },
            scope : this
        });

    },

    ordenaStore : function()
    {
        var store = this.getStoreGruposAsignaturasSinAsignarStore();
        store.sort('titulo', 'ASC');

        var gruposAsignaturas = [];
        store.each(function(record)
        {
            gruposAsignaturas.push(record)
        });

        this.cargaGruposAsignaturasSinAsignarEnSelector(gruposAsignaturas);
    },

    cargaGruposAsignaturasSinAsignarEnSelector : function(gruposAsignaturas)
    {
        var view = this.getSelectorGrupos();
        view.removeAll();

        var botones = new Array();

        for (var i = 0, len = gruposAsignaturas.length; i < len; i++)
        {
            var margin = '10 10 0 10';

            var button =
            {
                xtype : 'button',
                text : gruposAsignaturas[i].data.titulo,
                padding : '2 5 2 5',
                margin : margin,
                grupoAsignaturaId : gruposAsignaturas[i].data.id
            };

            botones.push(button);

        }

        view.add(botones);
    }

});