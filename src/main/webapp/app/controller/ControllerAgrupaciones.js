Ext.define('HOR.controller.ControllerAgrupaciones',{
    extend : 'Ext.app.Controller',
    stores : [ 'StoreAgrupaciones', 'StoreEstudiosCompartidos', 'StoreAsignaturasAgrupacion', 'TreeStoreAsignaturas' ],
    model : [ 'Agrupacion', 'AgrupacionAsignatura' ],
    views : [ 'agrupaciones.PanelAgrupaciones', 'agrupaciones.VentanaEdicionAgrupaciones', 'agrupaciones.TreePanelAsignaturas', 'agrupaciones.GridAsignaturas' ],

    ventanaEdicionAgrupaciones : {},

    refs : [
    {
        selector : 'filtroAgrupaciones',
        ref : 'filtroAgrupaciones'
    },
    {
        selector : 'gridAgrupaciones',
        ref : 'gridAgrupaciones'
    },
    {
        selector : 'selectorAgrupaciones',
        ref : 'selectorAgrupaciones'
    },
    {
        selector : 'panelGestionAgrupaciones',
        ref : 'panelGestionAgrupaciones'
    },
    {
        selector : 'filtroAgrupaciones combobox[name=estudio]',
        ref : 'filtroTitulacion'
    },
    {
        selector : 'panelGestionAgrupaciones button[name=add-agrupacion]',
        ref : 'botonAddAgrupacion'
    },
    {
        selector : 'panelGestionAgrupaciones button[name=edit-agrupacion]',
        ref : 'botonEditAgrupacion'
    },
    {
        selector : 'panelGestionAgrupaciones button[name=delete-agrupacion]',
        ref : 'botonDeleteAgrupacion'
    },
    {
        selector : 'ventanaEdicionAgrupaciones form[name=formEdicionAgrupaciones]',
        ref : 'formEdicionAgrupaciones'
    },
    {
        selector : 'ventanaEdicionAgrupaciones',
        ref : 'ventanaEdicionAgrupaciones'
    },
    {
        selector : 'panelAsignaturas',
        ref : 'panelAsignaturas'
    },
    {
        selector : 'gridAsignaturasAgrupacion',
        ref : 'gridAsignaturasAgrupacion'
    },
    {
        selector : 'treePanelAsignaturas',
        ref : 'treePanelAsignaturas'
    } ],

    init : function()
    {
        this.control(
        {
            'filtroAgrupaciones combobox[name=estudio]' :
            {
                select : this.onTitulacionSelected
            },
            'panelGestionAgrupaciones button[name=add-agrupacion]' :
            {
                click : this.onAddAgrupacion
            },
            'panelGestionAgrupaciones button[name=edit-agrupacion]' :
            {
                click : this.showVentanaEdicionAgrupacion
            },

            'panelGestionAgrupaciones button[name=delete-agrupacion]' :
            {
                click : this.eliminarAgrupacion
            },
            'ventanaEdicionAgrupaciones button[action=cancelar]' :
            {
                click : this.closeVentanaEdicionAgrupacion
            },
            'ventanaEdicionAgrupaciones button[action=guardar]' :
            {
                click : this.guardarAgrupacion
            },
            'selectorAgrupaciones' :
            {
                agrupacionSelected : this.onAgrupacionSelected
            },
            'treePanelAsignaturas button[name=anyadir]' :
            {
                click : this.anyadirAsignaturas
            },
            'gridAsignaturasAgrupacion button[name=borrar]' :
            {
                click : this.borrarAsignaturas
            },
            'panelGestionAgrupaciones' :
            {
                afterrender : this.inicializaComponentes
            }
        });

    },

    guardarAgrupacion : function()
    {
        var ref = this;
        var form = this.getFormEdicionAgrupaciones().getForm();

        var agrupacionId = form.findField('id-agrupacion').getValue();
        var nombre = form.findField('nombre').getValue();
        var estudiosCompartidos = form.findField('estudios-compartidos').getValue();
        var estudiosCompartidosStr = this.getVentanaEdicionAgrupaciones().getEstudiosCompartidosSelected();

        var estudioId = this.getFiltroTitulacion().getValue();

        if (form.isValid())
        {
            var agrupacion = Ext.ModelManager.create(
            {
                id : agrupacionId,
                nombre : nombre,
                estudioId : estudioId,
                estudiosCompartidos : estudiosCompartidos,
                estudiosCompartidosStr : estudiosCompartidosStr
            }, "HOR.model.Agrupacion");

            var store = this.getStoreAgrupacionesStore();

            if (agrupacionId == '')
            {
                store.add(agrupacion);
            }
            else
            {
                var record = store.getAt(store.find('id', agrupacionId));
                record.set('nombre', nombre);
                record.set('estudiosCompartidos', estudiosCompartidos);
                record.set('estudiosCompartidosStr', estudiosCompartidosStr);

                store.getProxy().extraParams['agrupacionId'] = agrupacionId;
            }

            store.sync(
            {
                success : function(response)
                {
                    ref.closeVentanaEdicionAgrupacion();
                    ref.loadAgrupaciones(true);
                }
            });
        }
    },

    eliminarAgrupacion : function()
    {
        var ref = this;
        var store = this.getStoreAgrupacionesStore();

        var estudioId = this.getFiltroTitulacion().getValue();
        var agrupacionId = this.getSelectorAgrupaciones().getValue();
        var agrupacion = store.getAt(store.find('id', agrupacionId));

        Ext.Msg.confirm('Eliminar agrupació', 'Totes les dades d\'aquesta agrupació s\'esborraràn. Estàs segur de voler continuar?', function(btn, text)
        {
            if (btn == 'yes')
            {
                store.getProxy().extraParams['estudioId'] = estudioId;
                store.remove([ agrupacion ]);

                store.sync(
                {
                    success : function()
                    {
                        ref.loadAgrupaciones();
                    },
                    failure : function()
                    {
                        Ext.Msg.alert('Error', 'No s\'ha pogut esborrar la agrupació perque té assignatures assignades.');
                    }
                });
            }
        });
    },

    onTitulacionSelected : function(combo, records)
    {
        this.loadAgrupaciones();
    },

    onAddAgrupacion : function()
    {
        this.ventanaEdicionAgrupaciones = this.getVentanaEdicionAgrupacionView();
        this.loadEstudiosCompartidos();
        this.ventanaEdicionAgrupaciones.down('combobox').setVisible(true);
        this.ventanaEdicionAgrupaciones.show();
    },

    showVentanaEdicionAgrupacion : function()
    {
        var store = this.getStoreAgrupacionesStore();
        var agrupacionId = this.getSelectorAgrupaciones().getValue();
        var agrupacion = store.getAt(store.find('id', agrupacionId));

        this.ventanaEdicionAgrupaciones = this.getVentanaEdicionAgrupacionView();
        this.loadEstudiosCompartidos();

        var form = this.getFormEdicionAgrupaciones().getForm();

        form.findField('id-agrupacion').setValue(agrupacion.get('id'));
        form.findField('nombre').setValue(agrupacion.get('nombre'));

        var estudiosCompartidos = [];
        if (agrupacion.get('estudiosCompartidos') instanceof Array)
        {
            for ( var i in agrupacion.get('estudiosCompartidos'))
            {
                var idStr = agrupacion.get('estudiosCompartidos')[i];
                estudiosCompartidos.push(parseInt(idStr, 10));
            }
        }
        else
        {
            var id = parseInt(agrupacion.get('estudiosCompartidos'), 10);
            estudiosCompartidos.push(id);
        }
        form.findField('estudios-compartidos').setValue(estudiosCompartidos);

        this.ventanaEdicionAgrupaciones.down('combobox').setVisible(false);
        this.ventanaEdicionAgrupaciones.show();
    },

    closeVentanaEdicionAgrupacion : function()
    {
        this.ventanaEdicionAgrupaciones.destroy();
    },

    getVentanaEdicionAgrupacionView : function()
    {
        return this.getView('agrupaciones.VentanaEdicionAgrupaciones').create();
    },

    loadEstudiosCompartidos : function()
    {
        var estudioId = this.getFiltroTitulacion().getValue();
        var store = this.getStoreEstudiosCompartidosStore();

        store.load(
        {
            params :
            {
                estudioId : estudioId
            }
        });
    },

    loadAgrupaciones : function(seleccionarUltimo)
    {
        var estudioId = this.getFiltroTitulacion().getValue();
        var store = this.getStoreAgrupacionesStore();

        store.load(
        {
            callback : function(records)
            {
                this.actualizaSelectorAgrupaciones(records);
                if (seleccionarUltimo === true)
                {
                    var lastRecord = records[records.length - 1];
                    this.getSelectorAgrupaciones().selecciona(lastRecord.getId());
                }
                this.setEstadoComponentes();
            },
            params :
            {
                estudioId : estudioId
            },
            scope : this
        });
    },

    actualizaSelectorAgrupaciones : function(agrupaciones)
    {
        var view = this.getSelectorAgrupaciones();
        view.addAgrupaciones(agrupaciones);
    },

    onAgrupacionSelected : function()
    {
        this.loadAsignaturasDisponibles();
        this.loadAsignaturasAsociadas();
    },

    loadAsignaturasDisponibles : function()
    {
        var agrupacionId = this.getSelectorAgrupaciones().getValue();
        var estudioId = this.getFiltroTitulacion().getValue();

        var storeAsignaturasDisponibles = this.getTreeStoreAsignaturasStore();
        storeAsignaturasDisponibles.load(
        {
            url : '/hor/rest/agrupacion/' + agrupacionId + '/estudio/' + estudioId + '/asignaturasdisponibles/tree',
            callback : function(records)
            {
                this.setEstadoComponentes();
            },
            scope : this
        });
    },

    loadAsignaturasAsociadas : function()
    {
        var agrupacionId = this.getSelectorAgrupaciones().getValue();
        var storeAsignaturasSeleccionadas = this.getStoreAsignaturasAgrupacionStore();
        storeAsignaturasSeleccionadas.load(
        {
            url : '/hor/rest/agrupacion/' + agrupacionId + '/asignaturas',
            callback : function(records)
            {
                this.setEstadoComponentes();
            },
            scope : this
        });
    },

    borrarAsignaturas : function()
    {
        var agrupacionId = this.getSelectorAgrupaciones().getValue();
        var listaSeleccion = this.getGridAsignaturasAgrupacion().getSelectionModel().getSelection();
        if (listaSeleccion.length > 0)
        {
            var gridStore = this.getGridAsignaturasAgrupacion().getStore();
            gridStore.getProxy().url = '/hor/rest/agrupacion/' + agrupacionId + '/asignaturas';
            gridStore.remove(listaSeleccion);
            gridStore.sync(
            {
                callback : function()
                {
                    this.loadAsignaturasAsociadas();
                },
                scope : this
            });
        }
    },

    anyadirAsignaturas : function()
    {
        var ref = this;
        var listaSeleccion = this.getTreePanelAsignaturas().getSelectionModel().getSelection();
        if (listaSeleccion.length > 0)
        {
            var agrupacionId = this.getSelectorAgrupaciones().getValue();
            var storeAsignaturasSeleccionadas = this.getStoreAsignaturasAgrupacionStore();
            var itemsParaAnyadir = new Array();
            for ( var index in listaSeleccion)
            {
                var item = listaSeleccion[index];
                if (item.get("depth") > 1)
                {
                    var params = item.get("id").split(';');
                    var asignaturaAgrupacion = Ext.ModelManager.create(
                    {
                        agrupacionId : agrupacionId,
                        asignaturaId : params[0],
                        tipoSubgrupo : params[1],
                        subgrupoId : params[2]
                    }, 'HOR.model.AsignaturaAgrupacion');
                    itemsParaAnyadir.push(asignaturaAgrupacion);
                }
            }

            if (itemsParaAnyadir.length > 0)
            {
                storeAsignaturasSeleccionadas.getProxy().url = '/hor/rest/agrupacion/' + agrupacionId + '/asignaturas';
                storeAsignaturasSeleccionadas.add(itemsParaAnyadir);
                storeAsignaturasSeleccionadas.sync(
                {
                    callback : function()
                    {
                        ref.loadAsignaturasAsociadas();
                    }
                });
            }
        }
    },

    inicializaComponentes : function()
    {
        this.getStoreAgrupacionesStore().load([]);
        this.getStoreAsignaturasAgrupacionStore().load([]);
        this.getTreeStoreAsignaturasStore().load([]);
    },

    setEstadoComponentes : function()
    {
        var estudioId = this.getFiltroTitulacion().getValue();
        var agrupacionId = this.getSelectorAgrupaciones().getValue();

        this.getBotonAddAgrupacion().setDisabled(estudioId === null);
        this.getBotonEditAgrupacion().setDisabled(agrupacionId === null);
        this.getBotonDeleteAgrupacion().setDisabled(agrupacionId === null);
        this.getPanelAsignaturas().setDisabled(agrupacionId === null);
    }

});