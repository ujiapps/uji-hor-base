Ext.define('HOR.controller.ControllerDashboards',
{
    extend : 'Ext.app.Controller',
    stores : [ 'StoreInformaciones' ],
    views : [ 'dashboard.PanelDashboard', 'ApplicationViewport' ]
});