Ext.define('HOR.controller.ControllerCalendarioAsignaturas',
{
    extend : 'Ext.app.Controller',
    stores : [ 'StoreEventosPlanificadosAsignatura', 'StoreEventosNoPlanificadosAsignatura', 'StoreEventosPlanificadosAsignaturaDetalle', 'StoreProfesores' ],
    requires : [ 'HOR.model.enums.TipoCalendarioAsignatura' ],
    views : [ 'HOR.view.pod.VentanaCreditos' ],
    tooltip : undefined,
    loadingMask : undefined,
    refs : [
    {
        selector : 'panelAsignaturas filtroAsignaturas',
        ref : 'filtroAsignaturas'
    },
    {
        selector : 'panelCalendarioAsignatura',
        ref : 'panelCalendarioAsignatura'
    },
    {
        selector : 'ventanaCreditos',
        ref : 'ventanaCreditos'
    },
    {
        selector : 'filtroProfesores combobox[name=semestre]',
        ref : 'comboSemestre'
    },
    {
        selector : 'panelEdicionPOD panel[name=contenedorCalendarios]',
        ref : 'panelContenedorCalendarios'
    },
    {
        selector : 'filtroProfesores combobox[name=asignatura]',
        ref : 'comboAsignatura'
    },
    {
        selector : 'filtroProfesores combobox[name=area]',
        ref : 'comboArea'
    },
    {
        selector : 'filtroProfesores combobox[name=departamento]',
        ref : 'comboDepartamento'
    },
    {
        selector : 'panelEdicionPOD ventanaAsignacionIdioma',
        ref : 'ventanaAsignacionIdioma'
    },
    {
        selector : 'filtroProfesores',
        ref : 'filtroProfesores'
    },
    {
        selector : 'panelEdicionPOD panel[name=panelFiltroGrupoTipo]',
        ref : 'panelFiltroGrupoTipo'
    },
    {
        selector : 'selectorProfesores',
        ref : 'selectorProfesores'
    },
    {
        selector : 'selectorClasesSinPlanificar',
        ref : 'selectorClasesSinPlanificar'
    },
    {
        selector : 'filtroProfesores button[name=menu-acciones]',
        ref : 'botonMenuAcciones'
    },
    {
        selector : 'panelEdicionPOD checkbox[name=ocupacion-profesor]',
        ref : 'checkboxOcupacionProfesor'
    },
    {
        selector : 'panelEdicionPOD checkbox[name=area-completa]',
        ref : 'checkboxAreaCompleta'
    },
    {
        selector : 'filtroProfesores menuitem[name=miPOD]',
        ref : 'botonMiPOD'
    },
    {
        selector : 'panelEdicionPOD boxselect[name=grupo]',
        ref : 'comboGrupo'
    },
    {
        selector : 'panelEdicionPOD boxselect[name=tipoSubgrupo]',
        ref : 'comboTipoSubgrupo'
    },
    {
        selector : 'filtroProfesores button[name=calendarioAsignaturasGenerica]',
        ref : 'botonCalendarioGenerica'
    },
    {
        selector : 'filtroProfesores button[name=calendarioAsignaturasDetalle]',
        ref : 'botonCalendarioDetalle'
    },
    {
        selector : 'filtroProfesores text[name=estadoAsignacion]',
        ref : 'estadoAsignacion'
    } ],

    init : function()
    {
        this.loadingMask = new Ext.LoadMask(Ext.getBody(),
        {
            msg : "Carregant..."
        });

        this.control(
        {
            'filtroProfesores' :
            {
                afterrender : function()
                {
                    var panelPadre = this.getPanelContenedorCalendarios();
                    var eventos = Ext.create('HOR.store.StoreEventosPlanificadosAsignatura');

                    panelPadre.insert(0,
                    {
                        xtype : 'panelCalendarioAsignatura',
                        eventStore : eventos,
                        showMultiDayView : true,
                        readOnly : false,
                        viewConfig :
                        {
                            viewStartHour : 8,
                            viewEndHour : 22
                        }
                    });
                }
            },
            'filtroProfesores combobox[name=semestre]' :
            {
                change : this.onSemestreChange
            },
            'panelEdicionPOD boxselect[name=grupo]' :
            {
                select : function()
                {
                    this.createCalendar();
                    this.refreshClasesSinAsignar();
                }
            },
            'panelEdicionPOD boxselect[name=tipoSubgrupo]' :
            {
                select : function()
                {
                    this.createCalendar();
                    this.refreshClasesSinAsignar();
                }
            },
            'panelEdicionPOD checkbox[name=ocupacion-profesor]' :
            {
                change : function()
                {
                    this.createCalendar();
                    this.refreshClasesSinAsignar();
                }
            },
            'panelEdicionPOD checkbox[name=area-completa]' :
            {
                change : function()
                {
                    this.createCalendar();
                }
            },
            'filtroProfesores button[name=refrescar]' :
            {
                click : this.refreshAll
            },
            'filtroProfesores menuitem[name=validacionesPOD]' :
            {
                click : this.mostrarValidacionesPOD
            },
            'filtroProfesores menuitem[name=validacionesSolapamientos]' :
            {
                click : this.mostrarValidacionesSolapamientos
            },
            'filtroProfesores menuitem[name=controlesPOD]' :
            {
                click : this.mostrarControlesPOD
            },
            'filtroProfesores menuitem[name=asignaturaPOD]' :
            {
                click : this.mostrarAsignaturaPOD
            },
            'filtroProfesores menuitem[name=detalleAsignaturaPOD]' :
            {
                click : this.mostrarDetalleAsignaturaPOD
            },
            'filtroProfesores menuitem[name=detalleSesionesPOD]' :
            {
                click : this.mostrarDetalleSesionesPOD
            },
            'filtroProfesores menuitem[name=asignaturaSimplePOD]' :
            {
                click : this.mostrarAsignaturaSimplePOD
            },
            'filtroProfesores menuitem[name=miPOD]' :
            {
                click : this.mostrarMiPOD
            },
            'selectorProfesores' :
            {
                profesorSelected : function()
                {
                    this.createCalendar();
                    this.refreshClasesSinAsignar();
                    this.setEstadoComponentes();
                }
            },
            'selectorClasesSinPlanificar' :
            {
                claseSelected : this.onClaseSelected,
                claseDeselected : this.onClaseDeselected,
                asignarCreditos : this.onAsignarCreditos,
                asignarIdioma : this.onAsignarIdioma
            },
            'filtroProfesores button[name=calendarioAsignaturasDetalle]' :
            {
                click : function()
                {
                    this.createCalendarSemanaDetalle();
                    this.setEstadoComponentes();
                }
            },
            'filtroProfesores button[name=calendarioAsignaturasGenerica]' :
            {
                click : function()
                {
                    this.createCalendarSemanaGenerica();
                    this.setEstadoComponentes();
                }
            },
            'ventanaCreditos button[name=guardar]' :
            {
                click : this.guardarVentanaCreditos
            },
            'ventanaCreditos button[name=cancelar]' :
            {
                click : this.cancelarVentanaCreditos
            },
            'panelEdicionPOD ventanaAsignacionIdioma button[name=cancelar]' :
            {
                click : this.cancelarVentanaAsignacionIdioma
            },
            'panelEdicionPOD ventanaAsignacionIdioma button[name=guardar]' :
            {
                click : this.guardarIdiomas
            }

        });
    },

    onSemestreChange : function()
    {
        var semestreId = this.getComboSemestre().getValue();
        if (semestreId)
        {
            this.getComboGrupo().clearValue();
            this.getComboTipoSubgrupo().clearValue();
            this.createCalendar();
            this.refreshClasesSinAsignar();
            this.refreshAsignacionAsignatura();
        }
    },

    showLoading : function()
    {
        this.loadingMask.show();
    },

    hideLoading : function()
    {
        if (this.loadingMask)
        {
            this.loadingMask.hide();
        }
    },

    guardarVentanaCreditos : function()
    {
        var data = this.getVentanaCreditos().getData();
        var ref = this;

        if (data.creditosComputables !== null && data.creditos == null)
        {
            alert("No es poden especificar crèdits computables sense especificar els crèdits del subgrup");
            return;
        }

        if (data.creditosComputables > data.creditos)
        {
            alert("El nombre de crèdits computables no pot superar el nombre de crèdits del subgrup de la assignatura");
            return;
        }

        if (this.getVentanaCreditos().down('form').getForm().isValid())
        {
            Ext.Ajax.request(
            {
                url : '/hor/rest/profesor/detalle/',
                method : 'PUT',
                jsonData : JSON.stringify(data),
                success : function(response)
                {
                    this.getVentanaCreditos().hide();
                    ref.refreshAll();
                },
                failure : function(response)
                {
                    var serverResponse = JSON.parse(response.responseText);
                    Ext.MessageBox.show(
                    {
                        title : 'Error',
                        msg : serverResponse.message,
                        icon : Ext.MessageBox.ERROR,
                        buttons : Ext.Msg.OK
                    });
                },
                scope : this
            });
        }
    },

    cancelarVentanaCreditos : function()
    {
        this.getVentanaCreditos().hide();
    },

    borrarAsignacion : function(itemId, profesorId)
    {
        var ref = this;
        if (profesorId)
        {
            var profesorClase = Ext.create('HOR.model.ProfesorClase',
            {
                itemId : itemId,
                profesorId : profesorId
            });
            this.showLoading();
            profesorClase.destroy(
            {
                success : function()
                {
                    ref.refreshAll(ref.hideLoading);
                },
                failure : function()
                {
                    alert('No s\'ha pogut esborrar l\'assignació');
                    ref.hideLoading();
                },
                scope : this
            });
        }
    },

    crearAsignacion : function(itemId, profesorId)
    {
        var ref = this;
        if (profesorId)
        {
            var profesorClase = Ext.create('HOR.model.ProfesorClase',
            {
                itemId : itemId,
                profesorId : profesorId
            });
            this.showLoading();
            profesorClase.save(
            {
                success : function()
                {
                    ref.refreshAll(ref.hideLoading);
                },
                failure : function()
                {
                    alert('No s\'ha pogut desar l\'assignació');
                    ref.hideLoading();
                },
                scope : this
            });
        }
    },

    onClaseSelected : function(claseId)
    {
        var profesorId = this.getSelectorProfesores().getValue();
        this.crearAsignacion(claseId, profesorId);
    },

    onClaseDeselected : function(claseId)
    {
        var profesorId = this.getSelectorProfesores().getValue();
        this.borrarAsignacion(claseId, profesorId);
    },

    onAsignarCreditos : function(clase)
    {
        var profesorId = this.getSelectorProfesores().getValue();
        var ref = this;

        Ext.Ajax.request(
        {
            url : '/hor/rest/profesor/detalle',
            params :
            {
                itemId : clase.data.id,
                profesorId : profesorId
            },
            method : 'GET',
            success : function(response)
            {
                var profesorDetalle = Ext.JSON.decode(response.responseText).data;
                ref.getVentanaCreditos().setData(clase.data.title, profesorId, clase.data.id, profesorDetalle.creditos, profesorDetalle.creditosComputables, clase.data.creditos,
                        clase.data.creditosComputables);
                ref.getVentanaCreditos().show();
            }
        });
    },

    onAsignarIdioma : function(clase)
    {
        var profesorId = this.getSelectorProfesores().getValue();

        if (profesorId === null)
        {
            Ext.MessageBox.alert('Informació', 'Has de tenir seleccionat un professor per a establir els idiomes');
            return;
        }

        var profesor =
        {
            id : profesorId,
            nombre : this.getSelectorProfesores().getText()
        };

        var itemId = clase.data.id;
        var nombreClase = clase.data.title;

        this.showLoading();
        Ext.Ajax.request(
        {
            url : '/hor/rest/idioma/profesor/' + profesor.id,
            params :
            {
                itemId : itemId
            },
            method : 'GET',
            scope : this,
            success : function(response)
            {
                var idiomas = Ext.JSON.decode(response.responseText).data;

                this.getVentanaAsignacionIdioma().setData(itemId, profesor.id, idiomas);
                this.getVentanaAsignacionIdioma().setTitle('Idiomes de la classe ' + nombreClase);
                this.getVentanaAsignacionIdioma().setProfesor(profesor.nombre);
                this.getVentanaAsignacionIdioma().show();
                this.desactivarControlesPantalla();
                this.hideLoading();
            }
        });
    },

    refreshAll : function(callback)
    {
        var asignaturaId = this.getComboAsignatura().getValue();
        var semestreId = this.getComboSemestre().getValue();
        var profesorId = this.getSelectorProfesores().getValue();

        if (profesorId)
        {
            this.refreshSelectorProfesores();
        }

        if (profesorId && semestreId && asignaturaId)
        {
            this.refreshClasesSinAsignar();
        }

        if (semestreId && asignaturaId)
        {
            this.refreshCalendar(callback);
        }

        this.refreshAsignacionAsignatura();
        this.setEstadoComponentes();
    },

    refreshSelectorProfesores : function()
    {
        var ref = this;
        this.getStoreProfesoresStore().load(
        {
            params :
            {
                areaId : this.getComboArea().getValue()
            },
            callback : function(profesores, operation, success)
            {
                var profesorId = ref.getSelectorProfesores().getValue();
                for ( var i in profesores)
                {
                    var profActual = profesores[i];
                    if (profesorId === profActual.internalId)
                    {
                        ref.getSelectorProfesores().updateProfesor(profActual.data);
                    }
                }
            }
        });
    },

    refreshClasesSinAsignar : function()
    {
        var store = this.getStoreEventosNoPlanificadosAsignaturaStore();
        var profesorId = this.getSelectorProfesores().getValue();
        var asignaturaId = this.getComboAsignatura().getValue();

        if (!asignaturaId)
        {
            store.removeAll(false);
            return;
        }

        store.load(
        {
            params :
            {
                asignaturaId : asignaturaId,
                semestreId : this.getComboSemestre().getValue(),
                profesorId : profesorId,
                gruposId : this.getComboGrupo().getValue().join(';'),
                tipoSubgruposId : this.getComboTipoSubgrupo().getValue().join(';'),
                ocupacion : this.getCheckboxOcupacionProfesor().getValue()
            },
            callback : function(clases, operation, success)
            {
                if (success)
                {
                    this.getSelectorClasesSinPlanificar().addClases(clases, profesorId);
                }
            },
            scope : this
        });
    },

    refreshAsignacionAsignatura : function()
    {
        var asignaturaId = this.getComboAsignatura().getValue();
        Ext.Ajax.request(
        {
            url : '/hor/rest/asignatura/' + asignaturaId + '/creditos',
            method : 'GET',
            success : function(response)
            {
                var jsonResp = Ext.decode(response.responseText);
                this.getEstadoAsignacion().setText(
                        'Crèdits [' + jsonResp.data.codigoComun + '] assignats ' + this.formatNumber(jsonResp.data.creditosAsignadosPod) + '/' + this.formatNumber(jsonResp.data.creditosTotalesPod)
                                + ', computables ' + this.formatNumber(jsonResp.data.creditosAsignadosComputablesPod) + '/' + this.formatNumber(jsonResp.data.creditosTotalesComputablesPod));
            },
            scope : this
        });
    },

    formatNumber : function(value)
    {
        return parseFloat(Math.round(value * 100) / 100).toFixed(2);
    },

    refreshCalendar : function(callback)
    {
        if (this.getPanelCalendarioAsignatura())
        {
            this.refreshAsignacionAsignatura();
            this.getPanelCalendarioAsignatura().getEventStore().reload(
            {
                callback : function()
                {
                    if (callback)
                    {
                        callback();
                    }
                }
            });
        }
    },

    createCalendar : function()
    {
        this.refreshAsignacionAsignatura();
        if (this.getBotonCalendarioGenerica().pressed)
        {
            this.createCalendarSemanaGenerica();
        }
        else
        {
            this.createCalendarSemanaDetalle();
        }
    },

    mostrarValidacionesPOD : function()
    {
        var departamentoId = this.getComboDepartamento().getValue();
        window.open('https://e-ujier.uji.es/cocoon/' + session + '/' + departamentoId + '/validacion-pod.pdf');
    },

    mostrarValidacionesSolapamientos : function()
    {
        var departamentoId = this.getComboDepartamento().getValue();
        window.open('https://e-ujier.uji.es/cocoon/' + session + '/' + departamentoId + '/validacion-pod-solapamientos.pdf');
    },

    mostrarControlesPOD : function()
    {
        var departamentoId = this.getComboDepartamento().getValue();
        window.open("http://www.uji.es/cocoon/" + session + "/" + departamentoId + "/consulta-pod.pdf");
    },

    mostrarAsignaturaPOD : function()
    {
        var departamentoId = this.getComboDepartamento().getValue();
        var areaId = this.getComboArea().getValue();
        window.open("http://www.uji.es/cocoon/" + session + "/" + departamentoId + "/" + areaId + "/consulta-asignaturas-pod.pdf");
    },

    mostrarDetalleAsignaturaPOD : function()
    {
        var departamentoId = this.getComboDepartamento().getValue();
        var areaId = this.getComboArea().getValue() || -1;
        var semestreId = this.getComboSemestre().getValue() || -1;
        window.open("http://www.uji.es/cocoon/" + session + "/" + departamentoId + "/" + areaId + "/" + semestreId + "/asignatura-detalle-pod.pdf");
    },

    mostrarAsignaturaSimplePOD : function()
    {
        var departamentoId = this.getComboDepartamento().getValue();
        var areaId = this.getComboArea().getValue() || -1;
        window.open("http://www.uji.es/cocoon/" + session + "/" + departamentoId + "/" + areaId + "/asignatura-simplificada-pod.pdf");
    },

    mostrarDetalleSesionesPOD : function()
    {
        var departamentoId = this.getComboDepartamento().getValue();
        var areaId = this.getComboArea().getValue() || -1;
        window.open("http://www.uji.es/cocoon/" + session + "/" + departamentoId + "/" + areaId + "/validacion-sesiones-pod.pdf");
    },

    mostrarMiPOD : function()
    {
        var departamentoId = this.getComboDepartamento().getValue();
        var areaId = this.getComboArea().getValue();
        var profesorId = this.getSelectorProfesores().getValue();
        window.open("http://www.uji.es/cocoon/" + session + "/" + departamentoId + "/" + areaId + "/" + profesorId + "/consulta-detalle-pod.pdf");
    },

    createCalendarSemanaGenerica : function()
    {
        var ref = this;
        var params =
        {
            asignaturaId : this.getComboAsignatura().getValue(),
            semestreId : this.getComboSemestre().getValue(),
            profesorId : this.getSelectorProfesores().getValue(),
            gruposId : this.getComboGrupo().getValue().join(';'),
            tipoSubgruposId : this.getComboTipoSubgrupo().getValue().join(';'),
            ocupacion : this.getCheckboxOcupacionProfesor().getValue(),
            areaCompleta : this.getCheckboxAreaCompleta().getValue(),
            areaId : this.getComboArea().getValue()
        };

        if (!params.asignaturaId)
        {
            return;
        }

        var eventos = Ext.create('HOR.store.StoreEventosPlanificadosAsignatura');
        eventos.getProxy().extraParams = params;

        eventos.addListener('beforeload', function()
        {
            ref.showLoading();
        });
        eventos.addListener('load', function()
        {
            ref.hideLoading();
        });

        var panelPadre = this.getPanelContenedorCalendarios();

        Ext.Array.each(Ext.ComponentQuery.query('panelCalendarioAsignatura,panelCalendarioAsignaturaDetalle'), function(panel)
        {
            panel.destroy();
        });

        panelPadre.insert(0,
        {
            xtype : 'panelCalendarioAsignatura',
            eventStore : eventos,
            showMultiDayView : true,
            readOnly : false,
            viewConfig :
            {
                viewStartHour : 8,
                viewEndHour : 22
            },
            listeners :
            {
                dayclick : function(dt, allday, el)
                {
                    return false;
                },

                eventclick : function(calendar, record)
                {
                    if (record.data.CalendarId === HOR.model.enums.TipoCalendarioAsignatura.MISMO_PROFESOR_ASIGNADO_SIN_SESIONES.id
                            || record.data.CalendarId === HOR.model.enums.TipoCalendarioAsignatura.OCUPACION_PROFESOR.id
                            || record.data.CalendarId === HOR.model.enums.TipoCalendarioAsignatura.RESTO_AREA.id)
                    {
                        return false;
                    }
                    ref.onEventClicked(calendar, record);
                    return false;
                },

                rangeselect : function()
                {
                    return false;
                },

                afterrender : function()
                {
                    eventos.load();
                },

                beforeeventmove : function()
                {
                    return false;
                },

                beforeeventresize : function()
                {
                    return false;
                },

                eventover : function(calendar, record, event)
                {
                    ref.mostrarInfoAdicionalEvento(event, record);
                },

                eventout : function()
                {
                    if (ref.tooltip)
                    {
                        ref.tooltip.destroy();
                        delete ref.tooltip;
                    }
                },

                asignacionClasesProfesorDetails : function(calendar, eventoPrincipal)
                {
                    if (eventoPrincipal.data.CalendarId !== HOR.model.enums.TipoCalendarioAsignatura.MISMO_PROFESOR_ASIGNADO.id
                            && eventoPrincipal.data.CalendarId !== HOR.model.enums.TipoCalendarioAsignatura.MISMO_PROFESOR_ASIGNADO_SIN_SESIONES.id)
                    {
                        Ext.MessageBox.alert('Informació', 'No es poden editar els detalls perquè el professor no té assignada la classe');
                        return;
                    }
                    if (ref.getSelectorProfesores().getValue() === null)
                    {
                        Ext.MessageBox.alert('Informació', 'Has de tenir seleccionat un professor per a editar els detalls');
                        return;
                    }

                    var profesor =
                    {
                        id : ref.getSelectorProfesores().getValue(),
                        nombre : ref.getSelectorProfesores().getText()
                    };

                    ref.showLoading();
                    Ext.Ajax.request(
                    {
                        url : '/hor/rest/calendario/eventos/mismogrupo/' + eventoPrincipal.data.EventId,
                        params :
                        {
                            profesorId : profesor.id
                        },
                        method : 'GET',
                        success : function(response)
                        {
                            var eventos = Ext.JSON.decode(response.responseText).data;
                            ref.getPanelCalendarioAsignatura().showAsignacionClasesProfesorDetails(profesor, eventoPrincipal, eventos);
                            ref.desactivarControlesPantalla();
                            ref.hideLoading();
                        }
                    });
                },
                asignacionIdiomasClaseProfesor : function(calendar, eventoPrincipal)
                {
                    if (ref.getSelectorProfesores().getValue() === null)
                    {
                        Ext.MessageBox.alert('Informació', 'Has de tenir seleccionat un professor per a establir els idiomes');
                        return;
                    }
                    if (eventoPrincipal.data.CalendarId !== HOR.model.enums.TipoCalendarioAsignatura.MISMO_PROFESOR_ASIGNADO.id
                            && eventoPrincipal.data.CalendarId !== HOR.model.enums.TipoCalendarioAsignatura.MISMO_PROFESOR_ASIGNADO_SIN_SESIONES.id)
                    {
                        Ext.MessageBox.alert('Informació', 'No es poden establir els idiomes perquè el professor no té assignada la classe');
                        return;
                    }

                    var profesor =
                    {
                        id : ref.getSelectorProfesores().getValue(),
                        nombre : ref.getSelectorProfesores().getText()
                    };

                    var itemId = eventoPrincipal.data.EventId;
                    var nombreClase = eventoPrincipal.data.Title;
                    ref.showLoading();
                    Ext.Ajax.request(
                    {
                        url : '/hor/rest/idioma/profesor/' + profesor.id,
                        params :
                        {
                            itemId : itemId
                        },
                        method : 'GET',
                        success : function(response)
                        {
                            var idiomas = Ext.JSON.decode(response.responseText).data;

                            ref.getVentanaAsignacionIdioma().setData(itemId, profesor.id, idiomas);
                            ref.getVentanaAsignacionIdioma().setTitle('Idiomes de la classe ' + nombreClase);
                            ref.getVentanaAsignacionIdioma().setProfesor(profesor.nombre);
                            ref.getVentanaAsignacionIdioma().show();
                            ref.desactivarControlesPantalla();
                            ref.hideLoading();
                        }
                    });
                }

            }
        });
    },

    createCalendarSemanaDetalle : function()
    {
        var ref = this;
        var params =
        {
            asignaturaId : this.getComboAsignatura().getValue(),
            semestreId : this.getComboSemestre().getValue(),
            profesorId : this.getSelectorProfesores().getValue(),
            gruposId : this.getComboGrupo().getValue().join(';'),
            tipoSubgruposId : this.getComboTipoSubgrupo().getValue().join(';'),
            ocupacion : this.getCheckboxOcupacionProfesor().getValue()
        };

        if (!params.asignaturaId)
        {
            return;
        }

        var eventos = Ext.create('HOR.store.StoreEventosPlanificadosAsignaturaDetalle');
        eventos.getProxy().extraParams = params;

        eventos.addListener('beforeload', function()
        {
            ref.showLoading();
        });
        eventos.addListener('load', function()
        {
            ref.hideLoading();
        });

        var panelPadre = this.getPanelContenedorCalendarios();

        Ext.Array.each(Ext.ComponentQuery.query('panelCalendarioAsignatura,panelCalendarioAsignaturaDetalle'), function(panel)
        {
            panel.destroy();
        });

        panelPadre.insert(0,
        {
            xtype : 'panelCalendarioAsignaturaDetalle',
            eventStore : eventos,
            showMultiDayView : true,
            readOnly : true,
            viewConfig :
            {
                viewStartHour : 8,
                viewEndHour : 22
            },
            listeners :
            {
                dayclick : function(dt, allday, el)
                {
                    return false;
                },

                eventclick : function(calendar, record)
                {
                    return false;
                },

                rangeselect : function()
                {
                    return false;
                },

                afterrender : function()
                {
                    var ref = this;
                    eventos.load(
                    {
                        callback : function(records, operation, success)
                        {
                            if (records.length > 0)
                            {
                                var inicio = records[0].get("StartDate");
                                ref.setStartDate(inicio);
                            }
                        }
                    });
                },

                beforeeventmove : function()
                {
                    return false;
                },

                beforeeventresize : function()
                {
                    return false;
                },

                eventover : function(calendar, record, event)
                {
                    ref.mostrarInfoAdicionalEvento(event, record);
                },
                eventout : function()
                {
                    if (ref.tooltip)
                    {
                        ref.tooltip.destroy();
                        delete ref.tooltip;
                    }
                }
            }
        });
    },

    onEventClicked : function(calendar, record)
    {
        var profesorId = this.getSelectorProfesores().getValue();

        if (profesorId)
        {
            var cid = record.get(Extensible.calendar.data.EventMappings.CalendarId.name);
            var itemId = record.get("EventId");

            if (cid === HOR.model.enums.TipoCalendarioAsignatura.MISMO_PROFESOR_ASIGNADO.id)
            {
                var detalleManualProfesor = record.get("DetalleManualProfesor");
                var creditosProfesor = record.get("CreditosProfesor");
                var ref = this;

                if (detalleManualProfesor === 'true' || creditosProfesor !== null)
                {
                    Ext.Msg.confirm('Professor amb planificació detallada',
                            'El professor té establerta una planificació detallada de crèdits i / o dates, si continues amb aquesta operació aquestes dades es perdran. ¿Continuar?', function(btn)
                            {
                                if (btn == 'yes')
                                {
                                    ref.borrarAsignacion(itemId, profesorId);
                                }
                            });
                }
                else
                {
                    this.borrarAsignacion(itemId, profesorId);
                }
            }
            else
            {
                this.crearAsignacion(itemId, profesorId);
            }
        }
        else
        {
            Ext.MessageBox.alert('Informació', 'Has de tenir seleccionat un professor per crear / eliminar una associació');
        }
    },

    getCreditosPorAsignar : function(creditosAsignatura, creditosAsignados)
    {
        return Math.round(100 * (creditosAsignatura - creditosAsignados)) / 100;
    },

    mostrarInfoAdicionalEvento : function(event, record)
    {
        if (event.disabled)
        {
            this.tooltip.hide();
            return;
        }

        if (!this.tooltip)
        {
            var panelCalendarioAsignatura = Ext.ComponentQuery.query('panelCalendarioAsignatura,panelCalendarioAsignaturaDetalle')[0];
            this.tooltip = Ext.create('Ext.tip.ToolTip',
            {
                hidden : true,
                target : panelCalendarioAsignatura.getEl(),
                trackMouse : true,
                autoHide : false
            });
        }

        var panelCalendarioAsignatura = Ext.ComponentQuery.query('panelCalendarioAsignatura,panelCalendarioAsignaturaDetalle')[0];
        if (panelCalendarioAsignatura.getActiveView().xtype !== 'extensible.multidayview')
        {
            return;
        }
        this.tooltip.setTarget(panelCalendarioAsignatura.getEl());
        var tooltipHtml = '<strong>' + record.data.Title + '</strong><br><br>';
        var profesores = record.data.Profesores.split("; ");
        for ( var i in profesores)
        {
            var profesor = profesores[i];
            tooltipHtml += profesor + "<br>";
        }
        var creditosPorAsignar = this.getCreditosPorAsignar(record.data.Creditos, record.data.CreditosAsignados);
        tooltipHtml += '<br />Per assignar: ' + creditosPorAsignar + " crèdits";
        var creditosPorAsignarComputables = this.getCreditosPorAsignar(record.data.CreditosComputables, record.data.CreditosAsignadosComputables);

        if (record.data.CreditosComputables != null && creditosPorAsignar != creditosPorAsignarComputables) {
            tooltipHtml += " (" + creditosPorAsignarComputables + " comp.)";
        }

        tooltipHtml += "<br/>";

        this.tooltip.update(tooltipHtml);
        this.tooltip.show();
    },

    desactivarControlesPantalla : function()
    {
        this.getFiltroProfesores().setDisabled(true);
        this.getPanelFiltroGrupoTipo().setDisabled(true);
        this.getSelectorProfesores().disableTooltips();
        this.getSelectorProfesores().setDisabled(true);
        this.getSelectorClasesSinPlanificar().setDisabled(true);
    },

    activarControlesPantalla : function()
    {
        this.getFiltroProfesores().setDisabled(false);
        this.getPanelFiltroGrupoTipo().setDisabled(false);
        this.getSelectorProfesores().setDisabled(false);
        this.getSelectorClasesSinPlanificar().setDisabled(false);
    },

    setEstadoComponentes : function()
    {
        var calendarioDetalleActivo = this.getBotonCalendarioDetalle().pressed;
        var profesorId = this.getSelectorProfesores().getValue();
        this.getSelectorClasesSinPlanificar().setDisabled(profesorId == null);
        this.getBotonMiPOD().setDisabled(profesorId == null);
        this.getCheckboxOcupacionProfesor().setDisabled(profesorId == null || calendarioDetalleActivo);
    },

    cancelarVentanaAsignacionIdioma : function()
    {
        this.activarControlesPantalla();
        this.getVentanaAsignacionIdioma().hide();
    },

    guardarIdiomas : function()
    {
        var data = this.getVentanaAsignacionIdioma().getData();
        var idiomas = data.idiomas;

        if (!(idiomas instanceof Array))
        {
            idiomas = [ idiomas ];
        }

        Ext.Ajax.request(
        {
            url : '/hor/rest/idioma/profesor/' + data.profesorId,
            method : 'PUT',
            jsonData : JSON.stringify(
            {
                itemId : data.itemId,
                idiomas : idiomas.join(',')
            }),
            success : function(response)
            {
                this.getVentanaAsignacionIdioma().hide();
                this.getFiltroProfesores().setDisabled(false);
                this.getPanelFiltroGrupoTipo().setDisabled(false);
                this.getSelectorProfesores().setDisabled(false);
                this.getSelectorClasesSinPlanificar().setDisabled(false);
                this.refreshCalendar();
                this.refreshClasesSinAsignar();
            },
            failure : function(response)
            {
                var serverResponse = JSON.parse(response.responseText);
                Ext.MessageBox.show(
                {
                    title : 'Error',
                    msg : serverResponse.message,
                    icon : Ext.MessageBox.ERROR,
                    buttons : Ext.Msg.OK
                });
            },
            scope : this
        });

    }
});