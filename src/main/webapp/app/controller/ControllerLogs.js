Ext.define('HOR.controller.ControllerLogs',
{
    extend : 'Ext.app.Controller',
    stores : [ 'StoreEventosLogs', 'StoreLogs' ],
    refs : [
    {
        selector : 'filtroLogs datefield[name=fecha]',
        ref : 'selectorFechas'
    },
    {
        selector : 'filtroLogs combobox[name=eventoId]',
        ref : 'comboEventos'
    },
    {
        selector : 'gridLogs',
        ref : 'gridLogs'
    } ],

    init : function()
    {
        this.control(
        {
            'filtroLogs datefield[name=fecha]' :
            {
                select : this.onFechaSelected
            },
            'filtroLogs combobox[name=eventoId]' :
            {
                select : this.onEventoSelected
            },
            'gridLogs': {
                render: function() {
                    this.getStoreLogsStore().removeAll();
                }
            }
        });
    },

    onFechaSelected : function(field, value)
    {
        var storeEventos = this.getStoreEventosLogsStore();
        var eventos = this.getComboEventos();

        storeEventos.load(
        {
            params :
            {
                fecha : value
            },
            callback : function(records, operation, success)
            {
                if (success && records.length > 0)
                {
                    this.insert(0,
                    {
                        id : '',
                        name : 'Tots els events'
                    });
                    eventos.setValue('');
                    eventos.enable();
                }
                else
                {
                    eventos.clearValue();
                    storeEventos.removeAll();
                    eventos.disable();
                }
            }
        });

        this.loadStoreLogs(value, '');
    },

    onEventoSelected : function(combo, records)
    {
        var fecha = this.getSelectorFechas().getValue();

        this.loadStoreLogs(fecha, records[0].get('id'));
    },

    loadStoreLogs : function(fecha, evento)
    {
        var storeLogs = this.getStoreLogsStore();
        var grid = this.getGridLogs();

        var fechaStr = this.getSelectorFechas().getRawValue();

        storeLogs.load(
        {
            params :
            {
                fecha : fecha,
                itemId : evento
            },
            callback : function(records, operation, success)
            {
                if (success && records.length > 0)
                {
                    grid.setTitle('Logs del dia ' + fechaStr);
                    grid.enable();
                }
                else
                {
                    grid.setTitle('Logs');
                    grid.disable();
                }
            }
        });

    }

});