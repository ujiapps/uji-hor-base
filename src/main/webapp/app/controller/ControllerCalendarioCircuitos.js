Ext.define('HOR.controller.ControllerCalendarioCircuitos',
{
    extend : 'Ext.app.Controller',
    stores : [ 'StoreCalendariosCircuitos', 'StoreEventosCircuito', 'StoreConfiguracionAjustada', 'StoreEventosNoPlanificadosCircuito' ],
    requires : [ 'HOR.model.enums.TipoCalendarioCircuito' ],
    loadingMask : undefined,
    refs : [
    {
        selector : 'panelCircuitos filtroCircuitos',
        ref : 'filtroCircuitos'
    },
    {
        selector : 'panelCircuitos selectorCircuitos',
        ref : 'selectorCircuitos'
    },
    {
        selector : 'panelCircuitos panelCalendarioCircuito',
        ref : 'panelCalendarioCircuito'
    },
    {
        selector : 'panelCircuitos panelCalendarioDetalleCircuito',
        ref : 'panelCalendarioDetalleCircuito'
    },
    {
        selector : 'panelCircuitos selectorCalendariosCircuitos',
        ref : 'selectorCalendariosCircuitos'
    },
    {
        selector : 'panelCircuitos filtroCircuitos combobox[name=grupo]',
        ref : 'comboGrupos'
    },
    {
        selector : 'panelCircuitos filtroCircuitos combobox[name=semestre]',
        ref : 'comboSemestres'
    },
    {
        selector : 'filtroCircuitos button[name=calendarioCircuitosDetalle]',
        ref : 'botonCalendarioDetalle'
    },
    {
        selector : 'filtroCircuitos button[name=calendarioCircuitosGenerica]',
        ref : 'botonCalendarioGenerica'
    },
    {
        selector : 'filtroCircuitos button[name=imprimir]',
        ref : 'botonImprimir'
    },
    {
        selector : 'filtroCircuitos button[name=validar]',
        ref : 'botonValidar'
    },
    {
        selector : 'panelCalendarioCircuitos',
        ref : 'panelCalendarioCircuitos'
    },
    {
        selector : 'panelCalendarioCircuitosDetalle',
        ref : 'panelCalendarioCircuitosDetalle'
    },
    {
        selector : 'panelCircuitos panel[name=contenedorCalendariosCircuitos]',
        ref : 'contenedorCalendariosCircuitos'
    },
    {
        selector : 'selectorClasesSinPlanificarCircuitos',
        ref : 'selectorClasesSinPlanificarCircuitos'
    } ],

    init : function()
    {
        this.loadingMask = new Ext.LoadMask(Ext.getBody(),
        {
            msg : "Carregant..."
        });

        this.control(
        {
            'panelCircuitos' :
            {
                afterrender : function()
                {
                    var panelPadre = this.getContenedorCalendariosCircuitos();

                    var eventos = Ext.create('HOR.store.StoreEventosCircuitoDetalle');

                    eventos.addListener('beforeload', function()
                    {
                        ref.showLoading();
                    });
                    eventos.addListener('load', function()
                    {
                        ref.hideLoading();
                    });

                    panelPadre.insert(0,
                    {
                        xtype : 'panelCalendarioCircuitos',
                        eventStore : eventos,
                        showMultiDayView : true,
                        readOnly : false,
                        viewConfig :
                        {
                            viewStartHour : 8,
                            viewEndHour : 22
                        }
                    });
                }
            },
            'filtroCircuitos combobox[name=grupo]' :
            {
                select : this.disableSelectorCalendarios
            },
            'filtroCircuitos combobox[name=semestre]' :
            {
                select : this.onSemestreSelected
            },
            'selectorCircuitos button' :
            {
                click : this.onButtonCircuitoClicked
            },
            'filtroCircuitos button[name=imprimir]' :
            {
                click : this.imprimirCalendario
            },
            'filtroCircuitos button[name=validar]' :
            {
                click : this.mostrarValidaciones
            },
            'panelCircuitos button[name=calendarioCircuitosDetalle]' :
            {
                click : function(button)
                {
                    if (!button.pressed)
                    {
                        button.toggle();
                    }
                    var otherButton = this.getBotonCalendarioGenerica();
                    if (otherButton.pressed)
                    {
                        otherButton.toggle();
                    }

                    this.refreshCalendarDetalle(this.getCircuitoSeleccionadoId());
                }

            },
            'panelCircuitos button[name=calendarioCircuitosGenerica]' :
            {
                click : function(button)
                {
                    if (!button.pressed)
                    {
                        button.toggle();
                    }
                    var otherButton = this.getBotonCalendarioDetalle();
                    if (otherButton.pressed)
                    {
                        otherButton.toggle();
                    }

                    this.refreshCalendar(this.getCircuitoSeleccionadoId());
                }
            },
            'panelCircuitos selectorCalendariosCircuitos checkbox' :
            {
                change : this.onSelectorCalendarioChanged
            },
            'selectorCircuitos' :
            {
                circuitoSelected : function(circuito)
                {
                    if (this.getComboSemestres().getValue() != null)
                    {
                        if (this.getBotonCalendarioGenerica().pressed)
                        {
                            this.refreshCalendar(circuito);
                            this.refrescarClasesNoPlanificadas(circuito);
                        }
                        else
                        {
                            this.refreshCalendarDetalle(circuito);
                            this.refrescarClasesNoPlanificadas(circuito);
                        }
                    }
                }
            },
            'panelGestionCircuitos' :
            {
                circuitoDeleted : function()
                {
                    if (this.getComboSemestres().getValue() != null)
                    {
                        if (this.getBotonCalendarioGenerica().pressed)
                        {
                            this.refreshCalendar();
                        }
                        else
                        {
                            this.refreshCalendarDetalle();
                        }
                    }
                }
            },
            'selectorClasesSinPlanificarCircuitos' :
            {
                claseSelectedCircuito : function(eventoId)
                {
                    this.gestionaAsignacionEventoACircuito(eventoId, HOR.model.enums.TipoCalendarioCircuito.FUERA_CIRCUITO.id);
                },
                claseDeselectedCircuito : function(eventoId)
                {
                    this.gestionaAsignacionEventoACircuito(eventoId, HOR.model.enums.TipoCalendarioCircuito.EN_CIRCUITO.id);
                }
            }
        });
    },

    showLoading : function()
    {
        this.loadingMask.show();
    },

    hideLoading : function()
    {
        this.loadingMask.hide();
    },

    showBotonesCalendario : function()
    {
        this.getBotonCalendarioDetalle().show();
        this.getBotonCalendarioGenerica().show();
    },

    showBotonesCircuitoCalendario : function()
    {
        this.getBotonImprimir().show();
        this.getBotonValidar().show();
    },

    hideBotonesCircuitoCalendario : function()
    {
        this.getBotonImprimir().hide();
        this.getBotonValidar().hide();
    },

    refreshCalendarDetalle : function(circuito)
    {
        var titulaciones = this.getFiltroCircuitos().down('combobox[name=estudio]');
        var cursos = this.getFiltroCircuitos().down('combobox[name=curso]');
        var semestres = this.getFiltroCircuitos().down('combobox[name=semestre]');
        var grupos = this.getFiltroCircuitos().down('combobox[name=grupo]');

        var calendarios = this.getSelectorCalendariosCircuitos().getCalendarsSelected();
        var storeConfiguracion = this.getStoreConfiguracionAjustadaStore();

        var ref = this;
        storeConfiguracion.load(
        {
            params :
            {
                estudioId : titulaciones.getValue(),
                cursoId : 1,
                semestreId : semestres.getValue(),
                gruposId : grupos.getValue()
            },
            scope : this,
            callback : function(records, operation, success)
            {
                if (success)
                {
                    var record = records[0];
                    var fechaInicio = record.get('horaInicio');
                    var fechaFin = record.get('horaFin');

                    var inicio = Ext.Date.parse(fechaInicio, 'd/m/Y H:i:s', true);
                    var fin = Ext.Date.parse(fechaFin, 'd/m/Y H:i:s', true);
                    var horaInicio = Ext.Date.format(inicio, 'H');
                    var horaFin = Ext.Date.format(fin, 'H');

                    var panelPadre = this.getContenedorCalendariosCircuitos();

                    Ext.Array.each(Ext.ComponentQuery.query('panelCalendarioCircuitos'), function(panel)
                    {
                        panel.destroy();
                    });

                    Ext.Array.each(Ext.ComponentQuery.query('panelCalendarioCircuitosDetalle'), function(panel)
                    {
                        panel.destroy();
                    });

                    var eventos = Ext.create('HOR.store.StoreEventosCircuitoDetalle');

                    eventos.addListener('beforeload', function()
                    {
                        ref.showLoading();
                    });
                    eventos.addListener('load', function()
                    {
                        ref.hideLoading();
                    });

                    Extensible.calendar.data.EventModel.reconfigure();
                    Ext.Ajax.request(
                    {
                        url : '/hor/rest/semestredetalle/estudio/' + titulaciones.getValue() + "/semestre/" + semestres.getValue(),
                        params :
                        {
                            "cursoId" : cursos.getValue()
                        },
                        method : 'GET',
                        success : function(response)
                        {
                            var jsonResp = Ext.decode(response.responseText);
                            var cadenaFechaInicio = jsonResp.data[0].fechaInicio;
                            var inicio = Ext.Date.parse(cadenaFechaInicio, 'd/m/Y H:i:s', true);
                            var fin = new Date();
                            fin.setDate(inicio.getDate() + 7);

                            panelPadre.insert(0,
                            {
                                xtype : 'panelCalendarioCircuitosDetalle',
                                eventStore : eventos,
                                showMultiDayView : true,
                                startDate : inicio,
                                enableEditDetails : false,
                                viewConfig :
                                {
                                    viewStartHour : horaInicio,
                                    viewEndHour : horaFin
                                },

                                onInitDrag : function()
                                {

                                },
                                listeners :
                                {

                                    dayclick : function(dt, allday, el)
                                    {
                                        return false;
                                    },

                                    eventclick : function()
                                    {
                                        return false;
                                    },

                                    rangeselect : function()
                                    {
                                        return false;
                                    },

                                    afterrender : function()
                                    {
                                        params =
                                        {
                                            estudioId : titulaciones.getValue(),
                                            semestreId : semestres.getValue(),
                                            calendariosIds : calendarios,
                                            grupoId : grupos.getValue(),
                                            startDate : inicio,
                                            endDate : fin,
                                            circuitoId : circuito
                                        };

                                        eventos.getProxy().extraParams = params;
                                        this.setStartDate(inicio);
                                    },

                                    beforeeventmove : function()
                                    {
                                        return false;
                                    },

                                    beforeeventresize : function()
                                    {
                                        return false;
                                    },

                                    eventover : function(calendar, record, event)
                                    {
                                        ref.mostrarInfoAdicionalEvento(event, record);
                                    },
                                    eventout : function()
                                    {
                                        if (ref.tooltip)
                                        {
                                            ref.tooltip.destroy();
                                            delete ref.tooltip;
                                        }
                                    }
                                }
                            });
                        }
                    });
                }
            }
        });
    },

    refreshCalendar : function(circuito)
    {
        var titulaciones = this.getFiltroCircuitos().down('combobox[name=estudio]');
        var semestres = this.getFiltroCircuitos().down('combobox[name=semestre]');
        var grupos = this.getFiltroCircuitos().down('combobox[name=grupo]');

        var calendarios = this.getSelectorCalendariosCircuitos().getCalendarsSelected();
        var storeConfiguracion = this.getStoreConfiguracionAjustadaStore();

        var ref = this;
        storeConfiguracion.load(
        {
            params :
            {
                estudioId : titulaciones.getValue(),
                cursoId : 1,
                semestreId : semestres.getValue(),
                gruposId : grupos.getValue()
            },
            scope : this,
            callback : function(records, operation, success)
            {
                if (success)
                {
                    var record = records[0];
                    var fechaInicio = record.get('horaInicio');
                    var fechaFin = record.get('horaFin');

                    var inicio = Ext.Date.parse(fechaInicio, 'd/m/Y H:i:s', true);
                    var fin = Ext.Date.parse(fechaFin, 'd/m/Y H:i:s', true);
                    var horaInicio = Ext.Date.format(inicio, 'H');
                    var horaFin = Ext.Date.format(fin, 'H');

                    var panelPadre = this.getContenedorCalendariosCircuitos();

                    Ext.Array.each(Ext.ComponentQuery.query('panelCalendarioCircuitos'), function(panel)
                    {
                        panel.destroy();
                    });

                    Ext.Array.each(Ext.ComponentQuery.query('panelCalendarioCircuitosDetalle'), function(panel)
                    {
                        panel.destroy();
                    });

                    var eventos = Ext.create('HOR.store.StoreEventosCircuito');
                    eventos.addListener('beforeload', function()
                    {
                        ref.showLoading();
                    });
                    eventos.addListener('load', function()
                    {
                        ref.hideLoading();
                    });

                    Extensible.calendar.data.EventModel.reconfigure();

                    params =
                    {
                        estudioId : titulaciones.getValue(),
                        semestreId : semestres.getValue(),
                        calendariosIds : calendarios,
                        grupoId : grupos.getValue(),
                        circuitoId : circuito
                    };
                    eventos.getProxy().extraParams = params;
                    panelPadre.insert(0,
                    {
                        xtype : 'panelCalendarioCircuitos',
                        eventStore : eventos,
                        showMultiDayView : true,
                        viewConfig :
                        {
                            viewStartHour : horaInicio,
                            viewEndHour : horaFin
                        },
                        onInitDrag : function()
                        {

                        },
                        listeners :
                        {
                            dayclick : function(dt, allday, el)
                            {
                                return false;
                            },

                            eventclick : function(calendar, record)
                            {
                                ref.onEventClicked(calendar, record);
                                return false;
                            },

                            rangeselect : function()
                            {
                                return false;
                            },

                            afterrender : function()
                            {
                                eventos.load();
                            },

                            beforeeventmove : function()
                            {
                                return false;
                            },

                            beforeeventresize : function()
                            {
                                return false;
                            },

                            eventover : function(calendar, record, event)
                            {
                                ref.mostrarInfoAdicionalEvento(event, record);
                            },
                            eventout : function()
                            {
                                if (ref.tooltip)
                                {
                                    ref.tooltip.destroy();
                                    delete ref.tooltip;
                                }
                            }
                        }
                    });
                }
            }
        });
    },

    refrescarClasesNoPlanificadas : function(circuito)
    {
        var titulaciones = this.getFiltroCircuitos().down('combobox[name=estudio]');
        var semestres = this.getFiltroCircuitos().down('combobox[name=semestre]');
        var grupos = this.getFiltroCircuitos().down('combobox[name=grupo]');
        var calendarios = this.getSelectorCalendariosCircuitos().getCalendarsSelected();

        var store = this.getStoreEventosNoPlanificadosCircuitoStore();

        store.load(
        {
            params :
            {
                estudioId : titulaciones.getValue(),
                semestreId : semestres.getValue(),
                calendariosIds : calendarios,
                grupoId : grupos.getValue(),
                circuitoId : circuito
            },
            callback : function(clases, operation, success)
            {
                if (success)
                {
                    this.getSelectorClasesSinPlanificarCircuitos().addClases(clases);
                    this.setEstadoComponentes();
                }
            },
            scope : this
        });
    },

    onButtonCircuitoClicked : function(circuito)
    {
        if (this.getComboSemestres().getValue() != null)
        {
            if (!circuito.pressed)
            {
                this.hideBotonesCircuitoCalendario();
                this.disableSelectorCalendarios();
                this.loadCalendarioSinCircuito();
            }
            else
            {
                this.showBotonesCircuitoCalendario();
                this.getSelectorCalendariosCircuitos().enable();
                this.loadCalendarioConCircuito(circuito.circuitoId);
            }
        }
    },

    disableSelectorCalendarios : function()
    {
        this.getSelectorCalendariosCircuitos().disable();

        Ext.ComponentQuery.query('panelCircuitos selectorCalendariosCircuitos checkbox').forEach(function(checkbox)
        {
            checkbox.reset();
        });
    },

    loadCalendarioSinCircuito : function()
    {
        if (this.getFiltroCircuitos().down('button[name=calendarioCircuitosDetalle]').pressed)
        {
            this.refreshCalendarDetalle();
        }
        else
        {
            this.refreshCalendar();
        }
        this.refrescarClasesNoPlanificadas();
    },

    loadCalendarioConCircuito : function(circuito)
    {
        if (this.getFiltroCircuitos().down('button[name=calendarioCircuitosDetalle]').pressed)
        {
            this.refreshCalendarDetalle(circuito);
        }
        else
        {
            this.refreshCalendar(circuito);
        }
        this.refrescarClasesNoPlanificadas(circuito);
    },

    getCircuitoSeleccionadoId : function()
    {
        var circuitos = Ext.ComponentQuery.query('panelCircuitos selectorCircuitos button');

        for (var i = 0; i < circuitos.length; i++)
        {
            if (circuitos[i].pressed == true)
            {
                return circuitos[i].circuitoId;
            }
        }
    },

    onEventClicked : function(calendar, record)
    {

        var circuitoId = this.getCircuitoSeleccionadoId();
        var estudioId = this.getFiltroCircuitos().down('combobox[name=estudio]').getValue();
        var ref = this;

        if (circuitoId != undefined)
        {
            var calendar = record.get(Extensible.calendar.data.EventMappings.CalendarId.name);
            if (calendar == this.getSelectorCalendariosCircuitos().enCircuito)
            {
                calendar = this.getSelectorCalendariosCircuitos().fueraCircuito;
            }
            else
            {
                calendar = this.getSelectorCalendariosCircuitos().enCircuito;
            }
            params =
            {
                circuitoId : circuitoId,
                tipoAccion : calendar,
                estudioId : estudioId
            };

            var store = this.getPanelCalendarioCircuitos().getEventStore();
            var oldParams = store.getProxy().extraParams;

            store.getProxy().extraParams = params;
            record.set(Extensible.calendar.data.EventMappings.CalendarId.name, calendar);

            ref.showLoading();
            store.save(
            {
                success : function()
                {
                    store.getProxy().extraParams = oldParams;
                    store.reload();
                    ref.hideLoading();
                },
                failure : function()
                {
                    ref.hideLoading();
                }
            });

        }
    },

    gestionaAsignacionEventoACircuito : function(eventoId, tipoAccion)
    {
        var circuitoId = this.getCircuitoSeleccionadoId();
        var estudioId = this.getFiltroCircuitos().down('combobox[name=estudio]').getValue();
        var ref = this;

        ref.showLoading();
        var params = Ext.urlEncode(
        {
            circuitoId : circuitoId,
            tipoAccion : tipoAccion,
            estudioId : estudioId
        });

        Ext.Ajax.request(
        {
            url : '/hor/rest/calendario/eventos/generica/circuito/' + eventoId + '?' + params,
            method : 'PUT',
            success : function()
            {
                ref.refrescarClasesNoPlanificadas(circuitoId);
                ref.hideLoading();
            },
            failure : function()
            {
                ref.hideLoading();
            }
        });
    },

    onSelectorCalendarioChanged : function()
    {
        var circuitoId = this.getCircuitoSeleccionadoId();

        if (circuitoId != undefined)
        {
            this.loadCalendarioConCircuito(circuitoId);
        }
        else
        {
            this.loadCalendarioSinCircuito();
        }
    },

    onSemestreSelected : function()
    {
        var circuitoId = this.getCircuitoSeleccionadoId();

        if (circuitoId != undefined)
        {
            this.loadCalendarioConCircuito(circuitoId);
            this.showBotonesCircuitoCalendario();
            this.getSelectorCalendariosCircuitos().enable();
        }
        else
        {
            this.loadCalendarioSinCircuito();
            this.disableSelectorCalendarios();
        }

        this.showBotonesCalendario();
    },

    imprimirCalendario : function()
    {
        var titulacionId = this.getFiltroCircuitos().down('combobox[name=estudio]').getValue();
        var circuitoId = this.getCircuitoSeleccionadoId();
        var semestreId = this.getComboSemestres().getValue();
        var grupoId = this.getComboGrupos().getValue();

        if (this.getBotonCalendarioGenerica().pressed)
        {
            window.open("http://www.uji.es/cocoon/" + session + "/" + titulacionId + "/" + circuitoId + "/" + semestreId + "/" + grupoId + "/horario-circuito-generica.pdf");
        }
        else
        {
            window.open("http://www.uji.es/cocoon/" + session + "/" + titulacionId + "/" + circuitoId + "/" + semestreId + "/" + grupoId + "/horario-circuito-detalle.pdf");
        }

    },

    mostrarInfoAdicionalEvento : function(event, record)
    {
        if (event.disabled)
        {
            this.tooltip.hide();
            return;
        }

        if (!this.tooltip)
        {
            var panelCalendarioCircuito = Ext.ComponentQuery.query('panelCalendarioCircuitos,panelCalendarioCircuitosDetalle')[0];
            this.tooltip = Ext.create('Ext.tip.ToolTip',
            {
                hidden : true,
                target : panelCalendarioCircuito.getEl(),
                trackMouse : true,
                autoHide : false
            });
        }

        if (record.data.NombreAsignatura)
        {
            var panelCalendarioCircuito = Ext.ComponentQuery.query('panelCalendarioCircuitos,panelCalendarioCircuitosDetalle')[0];
            this.tooltip.setTarget(panelCalendarioCircuito.getEl());

            var nombre = record.data.NombreAsignatura;
            var tooltipHtml = record.data.Title + ' - ' + nombre;
            this.tooltip.update(tooltipHtml);
            this.tooltip.show();
        }
        else
        {
            if (this.tooltip)
            {
                this.tooltip.destroy();
                delete this.tooltip;
            }
        }
    },

    mostrarValidaciones : function()
    {
        var titulacionId = this.getFiltroCircuitos().down('combobox[name=estudio]').getValue();
        var circuitoId = this.getCircuitoSeleccionadoId();
        var semestreId = this.getComboSemestres().getValue();
        var grupoId = this.getComboGrupos().getValue();

        window.open("http://www.uji.es/cocoon/" + session + "/" + titulacionId + "/" + circuitoId + "/" + semestreId + "/" + grupoId + "/validacion-circuito.pdf");
    },

    setEstadoComponentes : function()
    {
        var circuitoId = this.getCircuitoSeleccionadoId();
        this.getSelectorClasesSinPlanificarCircuitos().setDisabled(circuitoId == null);
    }

});