Ext.define('HOR.controller.ControllerGestionCircuitos',
{
    extend : 'Ext.app.Controller',
    stores : [ 'StoreCircuitos', 'StoreEstudiosCompartidos' ],
    views : [ 'circuitos.VentanaEdicionCircuitos' ],
    ventanaEdicionCircuitos : {},

    refs : [
    {
        selector : 'panelCircuitos panelGestionCircuitos',
        ref : 'panelGestionCircuitos'
    },
    {
        selector : 'panelCircuitos filtroCircuitos',
        ref : 'filtroCircuitos'
    },
    {
        selector : 'ventanaEdicionCircuitos',
        ref : 'ventanaEdicionCircuitos'
    },
    {
        selector : 'ventanaEdicionCircuitos form[name=formEdicionCircuitos]',
        ref : 'formEdicionCircuitos'
    },
    {
        selector : 'panelCircuitos selectorCircuitos',
        ref : 'selectorCircuitos'
    },
    {
        selector : 'panelCircuitos filtroCircuitos combobox[name=grupo]',
        ref : 'comboGrupos'
    },
    {
        selector : 'panelCircuitos selectorCalendariosCircuitos',
        ref : 'selectorCalendariosCircuitos'
    } ],

    init : function()
    {
        this.control(
        {
            'panelGestionCircuitos button[name=nuevo-circuito]' :
            {
                click : this.showVentanaNuevoCircuito
            },
            'panelGestionCircuitos button[name=editar-circuito]' :
            {
                click : this.showVentanaEdicionCircuito
            },
            'panelGestionCircuitos button[name=eliminar-circuito]' :
            {
                click : this.eliminarCircuito
            },
            'ventanaEdicionCircuitos button[action=cancelar]' :
            {
                click : this.closeVentanaEdicionCircuitos
            },
            'ventanaEdicionCircuitos button[action=guardar]' :
            {
                click : this.guardarCircuito
            },
            'filtroCircuitos combobox[name=grupo]' :
            {
                select : function()
                {
                    this.loadCircuitos();
                    this.getPanelGestionCircuitos().down('button[name=nuevo-circuito]').show();
                    this.getPanelGestionCircuitos().down('button[name=editar-circuito]').show();
                    this.getPanelGestionCircuitos().down('button[name=eliminar-circuito]').show();
                }
            },
            'panelCircuitos selectorCircuitos button' :
            {
                click : this.clickOnCircuito
            }
        });
    },

    getVentanaEdicionCurcuitosView : function()
    {
        return this.getView('circuitos.VentanaEdicionCircuitos').create();
    },

    loadEstudiosCompartidos : function()
    {
        var estudio = this.getFiltroCircuitos().down('combobox[name=estudio]').getValue();
        var store = this.getStoreEstudiosCompartidosStore();

        store.load(
        {
            params :
            {
                estudioId : estudio
            }
        });
    },

    showVentanaNuevoCircuito : function()
    {
        this.ventanaEdicionCircuitos = this.getVentanaEdicionCurcuitosView();
        this.loadEstudiosCompartidos();
        this.ventanaEdicionCircuitos.show();
    },

    showVentanaEdicionCircuito : function()
    {
        var store = this.getStoreCircuitosStore();
        var circuito = store.getAt(store.find('id', this.getCircuitoSeleccionadoId()));
        this.ventanaEdicionCircuitos = this.getVentanaEdicionCurcuitosView();
        this.loadEstudiosCompartidos();

        var form = this.getFormEdicionCircuitos().getForm();

        form.findField('id-circuito').setValue(circuito.get('id'));
        form.findField('nombre').setValue(circuito.get('nombre'));
        form.findField('plazas').setValue(circuito.get('plazas'));

        var estudiosCompartidos = [];
        if (circuito.get('estudiosCompartidos') instanceof Array) {
            for (var i in circuito.get('estudiosCompartidos')) {
                var idStr = circuito.get('estudiosCompartidos')[i];
                estudiosCompartidos.push(parseInt(idStr, 10));
            }
        } else {
            var id = parseInt(circuito.get('estudiosCompartidos'), 10);
            estudiosCompartidos.push(id);
        }
        form.findField('estudios-compartidos').setValue(estudiosCompartidos);

        this.ventanaEdicionCircuitos.show();
    },

    closeVentanaEdicionCircuitos : function()
    {
        this.ventanaEdicionCircuitos.destroy();
    },

    guardarCircuito : function()
    {
        var ref = this;
        var form = this.getFormEdicionCircuitos().getForm();

        var circuitoId = form.findField('id-circuito').getValue();
        var nombre = form.findField('nombre').getValue();
        var plazas = form.findField('plazas').getValue();
        var estudiosCompartidos = form.findField('estudios-compartidos').getValue();
        var estudiosCompartidosStr = this.getVentanaEdicionCircuitos().getEstudiosCompartidosSelected();

        var estudio = this.getFiltroCircuitos().down('combobox[name=estudio]').getValue();
        var grupo = this.getFiltroCircuitos().down('combobox[name=grupo]').getValue();

        if (form.isValid())
        {
            var circuito = Ext.ModelManager.create(
            {
                id : circuitoId,
                nombre : nombre,
                plazas : plazas,
                estudioId : estudio,
                grupo : grupo,
                estudiosCompartidos : estudiosCompartidos,
                estudiosCompartidosStr : estudiosCompartidosStr
            }, "HOR.model.Circuito");

            var store = this.getStoreCircuitosStore();

            if (circuitoId == '')
            {
                store.add(circuito);
            }
            else
            {
                var record = store.getAt(store.find('id', circuitoId));
                record.set('nombre', nombre);
                record.set('plazas', plazas);
                record.set('estudiosCompartidos', estudiosCompartidos);
                record.set('estudiosCompartidosStr', estudiosCompartidosStr);

                store.getProxy().extraParams['estudioId'] = estudio;
            }

            store.sync(
            {
                success : function(response)
                {
                    ref.closeVentanaEdicionCircuitos();
                    var circuitoId = response.operations[0].records[0].data.id;
                    ref.actualizaSelectorCircuitos(circuitoId);
                    ref.getSelectorCalendariosCircuitos().enable();
                }
            });
        }
    },

    eliminarCircuito : function()
    {
        var ref = this;
        var store = this.getStoreCircuitosStore();
        var circuito = store.getAt(store.find('id', this.getCircuitoSeleccionadoId()));
        var estudio = this.getFiltroCircuitos().down('combobox[name=estudio]').getValue();

        Ext.Msg.confirm('Eliminar circuit', 'Totes les dades d\'aquest circuit s\'esborraràn. Estàs segur de voler continuar?', function(btn, text)
        {
            if (btn == 'yes')
            {
                store.getProxy().extraParams['estudioId'] = estudio;
                store.remove([ circuito ]);

                store.sync(
                {
                    success : function()
                    {
                        ref.actualizaSelectorCircuitos();
                        ref.getPanelGestionCircuitos().down('button[name=editar-circuito]').disable();
                        ref.getPanelGestionCircuitos().down('button[name=eliminar-circuito]').disable();
                        ref.getSelectorCalendariosCircuitos().disable();
                        ref.getPanelGestionCircuitos().fireEvent('circuitoDeleted');
                    },
                    failure : function(response)
                    {
                        Ext.Msg.alert('Error', 'No s\'ha pogut esborrar el circuit perque té classes assignades.');
                    }
                });
            }
        });
    },

    getCircuitoSeleccionadoId : function()
    {
        var circuitos = Ext.ComponentQuery.query('panelCircuitos selectorCircuitos button');

        for ( var i = 0; i < circuitos.length; i++)
        {
            if (circuitos[i].pressed == true)
            {
                return circuitos[i].circuitoId;
            }
        }
    },

    loadCircuitos : function()
    {
        var estudio = this.getFiltroCircuitos().down('combobox[name=estudio]').getValue();
        var grupo = this.getFiltroCircuitos().down('combobox[name=grupo]').getValue();

        var store = this.getStoreCircuitosStore();

        store.load(
        {
            callback : function()
            {
                this.actualizaSelectorCircuitos();
            },
            params :
            {
                estudioId : estudio,
                grupoId : grupo
            },
            scope : this
        });
    },

    actualizaSelectorCircuitos : function(circuitoAMostrar)
    {
        var view = this.getSelectorCircuitos();

        view.removeAll();

        var store = this.getStoreCircuitosStore();
        store.sort('nombre', 'ASC');

        var botones = new Array();

        for ( var i = 0; i < store.count(); i++)
        {
            var circuito = store.getAt(i);

            var pressed = false;
            if (circuitoAMostrar && circuitoAMostrar == circuito.get('id'))
            {
                this.getPanelGestionCircuitos().down('button[name=editar-circuito]').enable();
                this.getPanelGestionCircuitos().down('button[name=eliminar-circuito]').enable();
                pressed = true;
            }

            var button =
            {
                xtype : 'button',
                text : circuito.get('nombre'),
                padding : '2 5 2 5',
                margin : '10 10 0 10',
                circuitoId : circuito.get('id'),
                enableToggle : true,
                toggleGroup : 'circuitos',
                pressed : pressed
            };

            botones.push(button);
        }

        view.add(botones);

        if (circuitoAMostrar)
        {
            this.getSelectorCircuitos().fireEvent('circuitoSelected', circuitoAMostrar);
        }
    },

    clickOnCircuito : function(button)
    {
        if (button.pressed)
        {
            this.getPanelGestionCircuitos().down('button[name=editar-circuito]').enable();
            this.getPanelGestionCircuitos().down('button[name=eliminar-circuito]').enable();
        }
        else
        {
            this.getPanelGestionCircuitos().down('button[name=editar-circuito]').disable();
            this.getPanelGestionCircuitos().down('button[name=eliminar-circuito]').disable();
        }
    }
});