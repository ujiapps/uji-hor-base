Ext.define('HOR.controller.ControllerAsignacionAulasPanelCalendarioPorAulas',
{
    extend : 'Ext.app.Controller',
    stores : [ 'StoreAulas', 'StoreEventos' ],
    refs : [
    {
        selector : 'formAsignacionAulasPanelCalendarioPorAulas',
        ref : 'formAsignacionAulasPanelCalendarioPorAulas'
    },
    {
        selector : 'filtroAulas combobox[name=semestre]',
        ref : 'comboSemestre'
    },
    {
        selector : 'filtroAulas combobox[name=centro]',
        ref : 'comboCentro'
    },

    {
        selector : 'panelCalendarioPorAula',
        ref : 'panelCalendarioPorAula'
    } ],

    dirty : false,

    init : function()
    {
        this.control(
        {
            'formAsignacionAulasPanelCalendarioPorAulas button[name=close]' :
            {
                click : this.cerrarFormulario
            },
            'panelCalendarioPorAula' :
            {
                eventasignaaula : function(cal, rec)
                {
                    this.cargarDatosFormulario(rec);
                }
            },
            'formAsignacionAulasPanelCalendarioPorAulas button[name=borrarAsignacion]' :
            {
                click : this.borrarAsignacionAula
            },
            'formAsignacionAulasPanelCalendarioPorAulas button[name=save]' :
            {
                click : this.confirmaGuardarDatosFormulario
            },
            'formAsignacionAulasPanelCalendarioPorAulas combobox[name=aula]' :
            {
                change : function()
                {
                    this.setIsFormularioDirty(Ext.ComponentQuery.query('formAsignacionAulasPanelCalendarioPorAulas')[0]);
                    var boton = Ext.ComponentQuery.query('formAsignacionAulasPanelCalendarioPorAulas button[name=borrarAsignacion]')[0];
                    if (Ext.ComponentQuery.query('formAsignacionAulasPanelCalendarioPorAulas combobox[name=aula]')[0].getValue() != null)
                    {
                        boton.show();
                    }
                    else
                    {
                        boton.hide();
                    }
                }
            },
            'formAsignacionAulasPanelCalendarioPorAulas combobox[name=tipoAccion]' :
            {
                change : function()
                {
                    this.setIsFormularioDirty(Ext.ComponentQuery.query('formAsignacionAulasPanelCalendarioPorAulas')[0]);
                }
            }
        });
    },

    cerrarFormulario : function()
    {
        var me = this;

        if (this.dirty)
        {
            Ext.Msg.confirm('Dades sense guardar', 'Tens dades sense guardar en el event, estàs segur de voler tancar la edició?', function(btn, text)
            {
                if (btn == 'yes')
                {
                    me.logicaCerrarFormulario();
                }
            });
        }
        else
        {
            this.logicaCerrarFormulario();
        }
    },

    logicaCerrarFormulario : function()
    {
        var panelCalendario = Ext.ComponentQuery.query('panelCalendarioPorAula')[0];
        panelCalendario.hideAsignarAulaView();
        var panelCalendarioAulas = Ext.ComponentQuery.query('panelCalendarioAulas')[0];
        panelCalendarioAulas.fireEvent('eventasigaaulaclose');
    },

    cargarDatosFormulario : function(rec)
    {
        var me = this;
        var formulario = Ext.ComponentQuery.query('formAsignacionAulasPanelCalendarioPorAulas')[0];
        formulario.getForm().reset();

        formulario.down('displayfield[name=event]').setValue(rec.data[Extensible.calendar.data.EventMappings.Descripcion.name]);
        formulario.down('hiddenfield[name=eventId]').setValue(rec.data[Extensible.calendar.data.EventMappings.EventId.name]);

        if (rec.data[Extensible.calendar.data.EventMappings.Comunes.name])
        {
            formulario.down('displayfield[name=comunes]').setValue(rec.data[Extensible.calendar.data.EventMappings.Comunes.name]);
            formulario.down('displayfield[name=comunes]').show();
        }
        else
        {
            formulario.down('displayfield[name=comunes]').hide();
        }

        formulario.down('combobox[name=tipoAccion]').setValue('U');
        var panelCalendario = Ext.ComponentQuery.query('panelCalendarioPorAula')[0];
        var centroId = this.getComboCentro().getValue();
        var semestreId = this.getComboSemestre().getValue();

        var store = this.getStoreAulasStore();
        store.load(
        {
            url : '/hor/rest/aula/disponibles/evento/',
            params :
            {
                eventoId : rec.data.EventId,
                semestreId : semestreId,
                centroId : centroId
            },
            callback : function(records, operation, success)
            {
                if (success && rec.data[Extensible.calendar.data.EventMappings.AulaPlanificacionId.name])
                {
                    formulario.down('combobox[name=aula]').setValue(parseInt(rec.data[Extensible.calendar.data.EventMappings.AulaPlanificacionId.name]));
                    formulario.down('combobox[name=aula]').lastValue = rec.data[Extensible.calendar.data.EventMappings.AulaPlanificacionId.name];
                    formulario.down('button[name=borrarAsignacion]').show();
                }
                else
                {
                    formulario.down('combobox[name=aula]').clearValue();
                    formulario.down('button[name=borrarAsignacion]').hide();
                }

                me.resetForm(formulario);
            }
        });
    },

    borrarAsignacionAula : function()
    {
        Ext.ComponentQuery.query('formAsignacionAulasPanelCalendarioPorAulas')[0].down('combobox').clearValue();
    },

    confirmaGuardarDatosFormulario : function()
    {
        var ref = this;
        var formulario = Ext.ComponentQuery.query('formAsignacionAulasPanelCalendarioPorAulas')[0];
        var tipoAccion = formulario.down('combobox[name=tipoAccion]').getValue();
        if (tipoAccion === 'T')
        {
            Ext.Msg.confirm('Confirmació de la operació',
                    'Assignar el aula a totes les classes del subgrup no garanteix la disponibilitat horària en la resta de classes. Estás segur/a de voler continuar?', function(btn, text)
                    {
                        if (btn === 'yes')
                        {
                            ref.guardarDatosFormulario();
                        }
                    });
        }
        else
        {
            this.guardarDatosFormulario();
        }
    },

    guardarDatosFormulario : function()
    {
        var me = this;
        var formulario = Ext.ComponentQuery.query('formAsignacionAulasPanelCalendarioPorAulas')[0];
        var store = formulario.up("panelCalendarioPorAula").getEventStore();

        var eventoId = formulario.down('hiddenfield[name=eventId]').getValue();
        var aulaId = formulario.down('combobox[name=aula]').getValue();
        var tipoAccion = formulario.down('combobox[name=tipoAccion]').getValue();

        Ext.Ajax.request(
        {
            url : '/hor/rest/calendario/eventos/aula/evento/' + eventoId,
            method : 'PUT',
            params :
            {
                aulaId : aulaId,
                tipoAccion : tipoAccion
            },
            success : function(response)
            {
                me.resetForm(formulario);
                var eventos = Ext.JSON.decode(response.responseText).data;

                for (var i = 0; i < eventos.length; i++)
                {
                    var record = store.getById(eventos[i].id);
                    if (record)
                    {
                        record.set(Extensible.calendar.data.EventMappings.AulaPlanificacionId.name, aulaId);
                        record.set(Extensible.calendar.data.EventMappings.Title.name, eventos[i].title);
                        if (eventos[i].id == eventoId)
                        {
                            formulario.down('displayfield[name=event]').setValue(record.data[Extensible.calendar.data.EventMappings.Title.name]);
                        }
                    }
                }
            },
            failure : function(response)
            {
                if (response.responseXML)
                {
                    var msgList = response.responseXML.getElementsByTagName("msg");

                    if (msgList && msgList[0] && msgList[0].firstChild)
                    {
                        Ext.MessageBox.show(
                        {
                            title : 'Server error',
                            msg : msgList[0].firstChild.nodeValue,
                            icon : Ext.MessageBox.ERROR,
                            buttons : Ext.Msg.OK
                        });
                    }
                }
            }
        });
    },

    setIsFormularioDirty : function(formulario)
    {
        var comboAulas = formulario.down('combobox[name=aula]');
        var comboAccion = formulario.down('combobox[name=tipoAccion]');

        if (comboAulas.getValue() != comboAulas.originalValue || comboAccion.getValue() != comboAccion.originalValue)
        {
            this.dirty = true;
            Ext.ComponentQuery.query('formAsignacionAulasPanelCalendarioPorAulas')[0].down('button[name=close]').setText('Tancar sense guardar');
        }
        else
        {
            Ext.ComponentQuery.query('formAsignacionAulasPanelCalendarioPorAulas')[0].down('button[name=close]').setText('Tancar');
            this.dirty = false;
        }
    },

    resetForm : function(formulario)
    {
        var comboAulas = formulario.down('combobox[name=aula]');
        var comboAccion = formulario.down('combobox[name=tipoAccion]');

        comboAulas.originalValue = comboAulas.getValue();
        comboAccion.originalValue = comboAccion.getValue();

        Ext.ComponentQuery.query('formAsignacionAulasPanelCalendarioPorAulas')[0].down('button[name=close]').setText('Tancar');
        this.dirty = false;
    }
});