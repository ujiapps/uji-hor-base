Ext.define('HOR.controller.ControllerMiPOD',
{
    extend : 'Ext.app.Controller',
    stores : [ 'StoreDepartamentosMiPOD', 'StoreAreasMiPOD', 'StoreProfesoresMiPOD' ],
    refs : [
    {
        selector : 'panelSeleccionPOD combobox[name=departamento]',
        ref : 'comboDepartamento'
    },
    {
        selector : 'panelSeleccionPOD combobox[name=area]',
        ref : 'comboArea'
    },
    {
        selector : 'panelSeleccionPOD combobox[name=profesor]',
        ref : 'comboProfesor'
    },
    {
        selector : 'panelSeleccionPOD button[name=descargar-pod-departamento]',
        ref : 'botonDescargarDepartamento'
    },
    {
        selector : 'panelSeleccionPOD button[name=descargar-pod-area]',
        ref : 'botonDescargarArea'
    },
    {
        selector : 'panelSeleccionPOD button[name=descargar-pod-asignatura]',
        ref : 'botonDescargarAsignatura'
    },
    {
        selector : 'panelSeleccionPOD button[name=descargar-pod-profesor]',
        ref : 'botonDescargarProfesor'
    } ],

    init : function()
    {
        this.control(
        {
            'panelSeleccionPOD' :
            {
                render : this.activaBotones
            },
            'panelSeleccionPOD combobox[name=departamento]' :
            {
                select : this.onDepartamentoSelected
            },
            'panelSeleccionPOD combobox[name=area]' :
            {
                select : this.onAreaSelected
            },
            'panelSeleccionPOD button[name=descargar-pod-profesor]' :
            {
                click : this.descargarPODProfesor
            },
            'panelSeleccionPOD button[name=descargar-pod-area]' :
            {
                click : this.descargarPODArea
            },
            'panelSeleccionPOD button[name=descargar-pod-asignatura]' :
            {
                click : this.descargarPODAsignatura
            },
            'panelSeleccionPOD button[name=descargar-pod-departamento]' :
            {
                click : this.descargarPODDepartamento
            },

            'panelSeleccionPOD combobox[name=profesor]' :
            {
                select : this.onProfesorSelected
            }
        });
    },

    onDepartamentoSelected : function(combo, records)
    {
        this.getComboArea().clearValue();
        this.getComboProfesor().clearValue();

        this.getStoreAreasMiPODStore().load(
        {
            params :
            {
                departamentoId : records[0].get('id')
            },
            scope : this,
            callback : function(areas, operation, success)
            {
                if (success && areas.length == 1)
                {
                    this.getComboArea().select(areas[0]);
                    this.onAreaSelected(this.getComboArea(), areas);
                }
                this.setEstadoComponentes();
            }
        });
    },

    onAreaSelected : function(combo, records)
    {
        this.getStoreProfesoresMiPODStore().load(
        {
            callback : function(profesores, operation, success)
            {
                if (success && profesores.length == 1)
                {
                    this.getComboProfesor().select(profesores[0]);
                    this.onProfesorSelected(this.getComboProfesor(), profesores);
                }
                this.setEstadoComponentes();
            },
            params :
            {
                areaId : records[0].get('id')
            },
            scope : this
        });
    },

    onProfesorSelected : function()
    {
        this.setEstadoComponentes();
    },

    descargarPODDepartamento: function()
    {
        var departamentoId = this.getComboDepartamento().getValue();
        this.descargarPOD(departamentoId, -1, -1);
    },

    descargarPODArea: function()
    {
        var departamentoId = this.getComboDepartamento().getValue();
        var areaId = this.getComboArea().getValue();
        this.descargarPOD(departamentoId, areaId, -1);
    },

    descargarPODAsignatura: function()
    {
        var departamentoId = this.getComboDepartamento().getValue();
        var areaId = this.getComboArea().getValue();
        window.open("http://www.uji.es/cocoon/" + session + "/" + departamentoId + "/" + areaId + "/asignatura-simplificada-pod.pdf");
    },

    descargarPODProfesor : function()
    {
        var departamentoId = this.getComboDepartamento().getValue();
        var areaId = this.getComboArea().getValue();
        var profesorId = this.getComboProfesor().getValue();
        this.descargarPOD(departamentoId, areaId, profesorId);
    },

    descargarPOD : function(departamentoId, areaId, profesorId)
    {
        window.open("http://www.uji.es/cocoon/" + session + "/" + departamentoId + "/" + areaId + "/" + profesorId + "/consulta-detalle-pod.pdf");
    },

    setEstadoComponentes : function()
    {
        var departamentoId = this.getComboDepartamento().getValue();
        var areaId = this.getComboArea().getValue();
        var profesorId = this.getComboProfesor().getValue();

        this.getComboArea().setDisabled(departamentoId == null);
        this.getComboProfesor().setDisabled(areaId == null);
        this.getBotonDescargarDepartamento().setDisabled(departamentoId == null);
        this.getBotonDescargarArea().setDisabled(areaId == null);
        this.getBotonDescargarAsignatura().setDisabled(areaId == null);
        this.getBotonDescargarProfesor().setDisabled(profesorId == null);
    },

    activaBotones : function()
    {
        var TIPO_CARGO_DIRECTOR_CENTRO = 3;
        var TIPO_CARGO_PAS_CENTRO = 4;
        var TIPO_CARGO_DIRECTOR_DEPARTAMENTO = 6;

        Ext.Ajax.request(
        {
            url : '/hor/rest/cargo/',
            method : 'GET',
            success : function(response)
            {
                var data = JSON.parse(response.responseText);
                var cargos = data.data;

                for ( var i in cargos)
                {
                    var cargo = cargos[i];

                    if ([ TIPO_CARGO_DIRECTOR_CENTRO, TIPO_CARGO_PAS_CENTRO, TIPO_CARGO_DIRECTOR_DEPARTAMENTO].indexOf(cargo.tipoCargoId) !== -1)
                    {
                        this.getBotonDescargarDepartamento().setVisible(true);
                        this.getBotonDescargarArea().setVisible(true);
                    }
                }
            },
            scope : this
        });

        this.getStoreDepartamentosMiPODStore().load(
        {
            callback : function(departamentos, operation, success)
            {
                if (success && departamentos.length == 1)
                {
                    this.getComboDepartamento().select(departamentos[0]);
                    this.onDepartamentoSelected(this.getComboDepartamento(), departamentos);
                }
                this.setEstadoComponentes();
            },
            scope : this
        });

    }

});