Ext.define('HOR.controller.ControllerFiltroCircuitos',
{
    extend : 'Ext.app.Controller',
    stores : [ 'StoreEstudios', 'StoreSemestres', 'StoreGruposCircuitos' ],
    model : [ 'Estudio', 'Semestre', 'Grupo' ],
    refs : [
    {
        selector : 'filtroCircuitos',
        ref : 'filtroCircuitos'
    },
    {
        selector : 'selectorCircuitos',
        ref : 'selectorCircuitos'
    },
    {
        selector : 'selectorCalendariosCircuitos',
        ref : 'selectorCalendariosCircuitos'
    },
    {
        selector : 'panelCalendarioCircuitos',
        ref : 'panelCalendarioCircuitos'
    },
    {
        selector : 'filtroCircuitos combobox[name=grupo]',
        ref : 'comboGrupos'
    },
    {
        selector : 'panelCalendarioCircuitosDetalle',
        ref : 'panelCalendarioCircuitosDetalle'
    },
    {
        selector : 'panelGestionCircuitos',
        ref : 'panelGestionCircuitos'
    } ],
    init : function()
    {
        this.control(
        {
            'filtroCircuitos combobox[name=estudio]' :
            {
                select : this.onTitulacionSelected
            },

            'filtroCircuitos combobox[name=semestre]' :
            {
                select : this.onSemestreSelected
            },
            'filtroCircuitos combobox[name=grupo]' :
            {
                select : this.onGrupoSelected
            }
        });
    },

    limpiaCamposComunes : function()
    {
        this.getFiltroCircuitos().down('combobox[name=semestre]').clearValue();
        this.limpiaPanelesCalendario();
    },

    limpiaPanelesCalendario : function()
    {
        if (this.getPanelCalendarioCircuitos())
        {
            this.getPanelCalendarioCircuitos().limpiaCalendario();
        }

        if (this.getPanelCalendarioCircuitosDetalle())
        {
            this.getPanelCalendarioCircuitosDetalle().limpiaCalendario();
        }

        this.getSelectorCircuitos().removeAll();
        this.getPanelGestionCircuitos().down('button[name=nuevo-circuito]').disable();
        this.getPanelGestionCircuitos().down('button[name=editar-circuito]').disable();
        this.getPanelGestionCircuitos().down('button[name=eliminar-circuito]').disable();

        this.getFiltroCircuitos().down('button[name=calendarioCircuitosDetalle]').hide();
        this.getFiltroCircuitos().down('button[name=calendarioCircuitosGenerica]').hide();
        this.getFiltroCircuitos().down('button[name=imprimir]').hide();
        this.getFiltroCircuitos().down('button[name=validar]').hide();
    },

    onTitulacionSelected : function(combo, records)
    {
        this.getFiltroCircuitos().down('combobox[name=grupo]').clearValue();
        this.getFiltroCircuitos().down('combobox[name=grupo]').enable();
        this.getStoreSemestresStore().removeAll();
        this.limpiaCamposComunes();

        var store = this.getStoreGruposCircuitosStore();

        store.load(
        {
            params :
            {
                estudioId : records[0].get('id'),
                cursoId : 1
            },
            scope : this
        });
    },

    onSemestreSelected : function(combo, records)
    {
        this.getFiltroCircuitos().down('button[name=imprimir]').hide();
        this.getFiltroCircuitos().down('button[name=validar]').hide();
    },

    onGrupoSelected : function()
    {
        this.limpiaCamposComunes();
        this.getFiltroCircuitos().down('combobox[name=semestre]').enable();

        this.getPanelGestionCircuitos().down('button[name=nuevo-circuito]').enable();
        this.getPanelGestionCircuitos().down('button[name=editar-circuito]').disable();
        this.getPanelGestionCircuitos().down('button[name=eliminar-circuito]').disable();

        var estudio = this.getFiltroCircuitos().down('combobox[name=estudio]').getValue();
        
        var semestres = this.getFiltroCircuitos().down('combobox[name=semestre]');

        this.getStoreSemestresStore().load(
        {
            params :
            {
                estudioId : estudio,
                cursoId : 1
            },
            scope : this,
            callback : function()
            {
                semestres.select(1);
                semestres.fireEvent('select');
            }
        });
    }

});