Ext.define('HOR.controller.ControllerFiltroCalendario',
{
    extend : 'Ext.app.Controller',
    stores : [ 'StoreEstudios', 'StoreCursos', 'StoreSemestres', 'StoreSemestresTodos', 'StoreGrupos', 'StoreAsignaturasEstudio', 'StoreCursosAgrupaciones' ],
    model : [ 'Estudio', 'Curso', 'Semestre', 'Grupo', 'Asignatura' ],
    refs : [
    {
        selector : 'panelHorarios filtroGrupos',
        ref : 'filtroGrupos'
    },
    {
        selector : 'selectorGrupos',
        ref : 'selectorGrupos'
    },
    {
        selector : 'panelCalendarioDetalle',
        ref : 'panelCalendarioDetalle'
    },
    {
        selector : 'panelCalendario',
        ref : 'panelCalendario'
    },
    {
        selector : 'panelHorarios filtroGrupos combobox[name=grupo]',
        ref : 'comboGrupos'
    },
    {
        selector : 'filtroGrupos combobox[name=estudio]',
        ref : 'filtroTitulacion'
    },
    {
        selector : 'filtroGrupos combobox[name=semestre]',
        ref : 'filtroSemestre'
    },
    {
        selector : 'filtroGrupos boxselect[name=grupo]',
        ref : 'filtroGrupo'
    },
    {
        selector : 'filtroGrupos boxselect[name=asignatura]',
        ref : 'filtroAsignatura'
    },
    {
        selector : 'filtroGrupos selectorCursoAgrupacion[name=cursoAgrupacion]',
        ref : 'selectorCursoAgrupacion'
    },
    {
        selector : 'filtroGrupos [name=imprimirTitulacion]',
        ref : 'menuImprimirTitulacion'
    },
    {
        selector : 'filtroGrupos [name=imprimirAgrupaciones]',
        ref : 'menuImprimirAgrupaciones'
    } ],

    init : function()
    {
        this.control(
        {
            'panelHorarios filtroGrupos combobox[name=estudio]' :
            {
                select : this.onTitulacionSelected
            },
            'panelHorarios filtroGrupos combobox[name=semestre]' :
            {
                select : this.cargaGrupos
            },
            'panelHorarios filtroGrupos combobox[name=grupo]' :
            {
                change : this.onGrupoChange
            },
            'filtroGrupos selectorCursoAgrupacion[name=cursoAgrupacion]' :
            {
                select : this.cargaGrupos
            },
            'panelHorarios button[name=calendarioDetalle]' :
            {
                click : this.setEstadoComponentes
            },
            'panelHorarios button[name=calendarioGenerica]' :
            {
                click : this.setEstadoComponentes
            }
        });
    },

    limpiaCamposComunes : function()
    {
        this.getFiltroGrupos().down('combobox[name=grupo]').clearValue();
        this.getFiltroGrupos().down('button[name=intervaloHorario]').hide();
        this.getFiltroGrupos().down('button[name=calendarioDetalle]').hide();
        this.getFiltroGrupos().down('button[name=calendarioGenerica]').hide();
        this.getFiltroGrupos().down('button[name=btnDeshacer]').hide();
        this.getFiltroGrupos().down('[name=menu-imprimir]').hide();
        this.getFiltroGrupos().down('button[name=validar]').hide();

        this.limpiaPanelesCalendario();
    },

    limpiaPanelesCalendario : function()
    {
        if (this.getPanelCalendario())
        {
            this.getPanelCalendario().limpiaCalendario();
        }

        if (this.getPanelCalendarioDetalle())
        {
            this.getPanelCalendarioDetalle().limpiaCalendario();
        }

        this.getSelectorGrupos().limpiaGrupos();
    },

    onTitulacionSelected : function(combo, records)
    {
        var estudioId = this.getFiltroTitulacion().getValue();
        this.getSelectorCursoAgrupacion().clearValue();
        this.getFiltroAsignatura().clearValue();
        this.limpiaCamposComunes();

        var store = this.getStoreCursosAgrupacionesStore();
        store.load(
        {
            url : '/hor/rest/estudio/' + estudioId + '/cursoagrupacion',
            params :
            {
                estudioId : estudioId
            },
            callback : function()
            {
                this.setEstadoComponentes();
            },
            scope : this
        });
    },

    cargaGrupos : function()
    {
        var semestreId = this.getFiltroSemestre().getValue();
        var cursoId = this.getSelectorCursoAgrupacion().getCursoId();
        var agrupacionId = this.getSelectorCursoAgrupacion().getAgrupacionId();
        var estudioId = this.getFiltroTitulacion().getValue();
        this.getFiltroAsignatura().clearValue();

        this.setEstadoComponentes();
        if (semestreId !== null && (agrupacionId !== null || cursoId !== null))
        {
            this.limpiaCamposComunes();

            var storeGrupos = this.getStoreGruposStore();
            storeGrupos.load(
            {
                params :
                {
                    estudioId : estudioId,
                    agrupacionId : agrupacionId,
                    cursoId : cursoId,
                    semestreId : semestreId
                },
                callback : function()
                {
                    this.setEstadoComponentes();
                },
                scope : this
            });
        }
    },

    onGrupoChange : function()
    {
        var estudio = this.getFiltroGrupos().down('combobox[name=estudio]').getValue();
        var semestre = this.getFiltroGrupos().down('combobox[name=semestre]').getValue();
        var grupos = this.getFiltroGrupos().getGruposSelected();
        var cursoId = this.getSelectorCursoAgrupacion().getCursoId();
        var agrupacionId = this.getSelectorCursoAgrupacion().getAgrupacionId();

        if (!(estudio && semestre && grupos && (cursoId || agrupacionId)))
        {
            return;
        }

        this.setEstadoComponentes();

        var storeAsignaturas = this.getStoreAsignaturasEstudioStore();
        storeAsignaturas.loadData([]);
        storeAsignaturas.load(
        {
            url : '/hor/rest/asignatura/estudio/' + estudio,
            params :
            {
                cursoId : cursoId,
                agrupacionId : agrupacionId,
                semestreId : semestre,
                gruposIds : grupos
            },
            callback : function()
            {
                this.setEstadoComponentes();
            },
            scope : this
        });

        if (this.getComboGrupos().getValue() == '')
        {
            this.limpiaPanelesCalendario();
        }
    },

    setEstadoComponentes : function()
    {
        var estudioId = this.getFiltroTitulacion().getValue();
        var cursoId = this.getSelectorCursoAgrupacion().getCursoId();
        var agrupacionId = this.getSelectorCursoAgrupacion().getAgrupacionId();
        var semestreId = this.getFiltroSemestre().getValue();
        var grupos = this.getFiltroGrupos().getGruposSelected();

        this.getSelectorCursoAgrupacion().setDisabled(estudioId === null);
        this.getFiltroSemestre().setDisabled(cursoId === null && agrupacionId === null);
        this.getFiltroGrupo().setDisabled(semestreId === null || (cursoId === null && agrupacionId === null));
        this.getFiltroAsignatura().setDisabled(grupos == '');
        this.getFiltroGrupos().down('button[name=intervaloHorario]').setVisible(estudioId !== null && cursoId != null && semestreId != null && grupos !== '');

        this.getMenuImprimirTitulacion().setDisabled(this.getFiltroGrupos().down('button[name=calendarioDetalle]').pressed);

        var hayAgrupaciones = false;
        this.getStoreCursosAgrupacionesStore().data.each(function(item)
        {
            if (item.data['tipoId'] == 2)
            {
                hayAgrupaciones = true;
            }
        });
        this.getMenuImprimirAgrupaciones().setDisabled(!hayAgrupaciones || this.getFiltroGrupos().down('button[name=calendarioDetalle]').pressed);
    }
});