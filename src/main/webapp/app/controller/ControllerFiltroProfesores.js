Ext.define('HOR.controller.ControllerFiltroProfesores',
{
    extend : 'Ext.app.Controller',
    stores : [ 'StoreDepartamentos', 'StoreAreas', 'StoreSemestresAsignatura', 'StoreAsignaturas', 'StoreProfesores', 'StoreGruposAsignatura', 'StoreTipoSubgruposAsignatura' ],

    refs : [
    {
        selector : 'filtroProfesores',
        ref : 'filtroProfesores'
    },
    {
        selector : 'selectorProfesores',
        ref : 'selectorProfesores'
    },
    {
        selector : 'selectorClasesSinPlanificar',
        ref : 'selectorClasesSinPlanificar'
    },
    {
        selector : 'filtroProfesores combobox[name=departamento]',
        ref : 'comboDepartamento'
    },
    {
        selector : 'filtroProfesores combobox[name=area]',
        ref : 'comboArea'
    },
    {
        selector : 'filtroProfesores combobox[name=semestre]',
        ref : 'comboSemestre'
    },
    {
        selector : 'filtroProfesores combobox[name=asignatura]',
        ref : 'comboAsignatura'
    },
    {
        selector : 'filtroProfesores menuitem[name=asignaturaPOD]',
        ref : 'botonAsignaturaPOD'
    },
    {
        selector : 'filtroProfesores menuitem[name=detalleAsignaturaPOD]',
        ref : 'botonDetalleAsignaturaPOD'
    },
    {
        selector : 'filtroProfesores menuitem[name=detalleSesionesPOD]',
        ref : 'botonDetalleSesionesPOD'
    },
    {
        selector : 'filtroProfesores menuitem[name=asignaturaSimplePOD]',
        ref : 'botonAsignaturaSimplePOD'
    },
    {
        selector : 'panelEdicionPOD boxselect[name=grupo]',
        ref : 'comboGrupo'
    },
    {
        selector : 'panelEdicionPOD boxselect[name=tipoSubgrupo]',
        ref : 'comboTipoSubgrupo'
    },
    {
        selector : 'panelEdicionPOD panel[name=panelFiltroGrupoTipo]',
        ref : 'panelFiltroGrupoTipo'
    },
    {
        selector : 'filtroProfesores button[name=refrescar]',
        ref : 'botonRefrescar'
    },
    {
        selector : 'filtroProfesores menuitem[name=miPOD]',
        ref : 'botonMiPOD'
    },
    {
        selector : 'filtroProfesores button[name=menu-acciones]',
        ref : 'botonMenuAcciones'
    },
    {
        selector : 'filtroProfesores text[name=estadoAsignacion]',
        ref : 'estadoAsignacion'
    },
    {
        selector : 'panelEdicionPOD checkbox[name=ocupacion-profesor]',
        ref : 'checkboxOcupacionProfesor'
    },
    {
        selector : 'filtroProfesores button[name=calendarioAsignaturasGenerica]',
        ref : 'botonCalendarioGenerica'
    },
    {
        selector : 'filtroProfesores button[name=calendarioAsignaturasDetalle]',
        ref : 'botonCalendarioDetalle'
    } ],

    init : function()
    {
        this.control(
        {
            'selectorProfesores' :
            {
                profesorSelected : this.setEstadoComponentes
            },
            'filtroProfesores combobox[name=departamento]' :
            {
                select : this.onDepartamentoSelected
            },
            'filtroProfesores combobox[name=area]' :
            {
                select : this.onAreaSelected
            },
            'filtroProfesores combobox[name=semestre]' :
            {
                select : this.onSemestreSelected
            },
            'filtroProfesores combobox[name=asignatura]' :
            {
                select : this.onAsignaturaSelected
            }
        });
    },

    onDepartamentoSelected : function(combo, records)
    {
        this.getComboArea().clearValue();
        this.getComboAsignatura().clearValue();

        this.getStoreAsignaturasStore().removeAll();
        this.getStoreSemestresAsignaturaStore().removeAll();
        this.getSelectorProfesores().removeAll();
        this.getSelectorClasesSinPlanificar().removeAll();
        this.getComboGrupo().clearValue();
        this.getComboTipoSubgrupo().clearValue();

        this.getStoreAreasStore().load(
        {
            params :
            {
                departamentoId : records[0].get('id')
            },
            scope : this,
            callback : function(areas, operation, success)
            {
                if (success && areas.length == 1)
                {
                    this.getComboArea().select(areas[0]);
                    this.onAreaSelected(this.getComboArea(), areas);
                }
                this.setEstadoComponentes();
            }
        });
    },

    limpiaEventosCalendario : function()
    {
        Ext.Array.each(Ext.ComponentQuery.query('panelCalendarioAsignatura,panelCalendarioAsignaturaDetalle'), function(panel)
        {
            if (panel.getEventStore)
            {
                panel.getEventStore().removeAll(false);
            }
        });
    },

    onAreaSelected : function(combo, records)
    {
        this.getSelectorClasesSinPlanificar().removeAll();
        this.getComboGrupo().clearValue();
        this.getStoreSemestresAsignaturaStore().removeAll();
        this.getComboTipoSubgrupo().clearValue();
        this.getStoreProfesoresStore().load(
        {
            callback : function(profesores, operation, success)
            {
                this.updateListaProfesores(profesores);
                this.setEstadoComponentes();
            },
            params :
            {
                areaId : records[0].get('id')
            },
            scope : this
        });

        this.limpiaEventosCalendario();
        this.updateComboAsignaturas();
    },

    onSemestreSelected : function()
    {
        var semestreId = this.getComboSemestre().getValue();
        var asignaturaId = this.getComboAsignatura().getValue();

        this.cargaComboGrupos(asignaturaId, semestreId);
        this.cargaComboTipoSubgrupos(asignaturaId, semestreId);

        this.setEstadoComponentes();
    },

    onAsignaturaSelected : function()
    {
        this.getSelectorClasesSinPlanificar().removeAll();
        this.getStoreSemestresAsignaturaStore().removeAll();
        this.getComboSemestre().clearValue();
        this.getComboGrupo().clearValue();
        this.getComboTipoSubgrupo().clearValue();
        this.limpiaEventosCalendario();
        this.setEstadoComponentes();
        this.updateComboSemestres();
    },

    cargaComboGrupos : function(asignaturaId, semestreId)
    {
        this.getComboGrupo().clearValue();
        this.getStoreGruposAsignaturaStore().load(
        {
            params :
            {
                asignaturaId : asignaturaId,
                semestreId : semestreId
            },
            callback : function(records, options, success)
            {
                if (success)
                {
                    var keys = [];
                    Ext.each(records, function(value)
                    {
                        keys.push(value.get('grupo'));
                    });
                    this.getComboGrupo().setValue(keys);
                }
            },
            scope : this
        });
    },

    cargaComboTipoSubgrupos : function(asignaturaId, semestreId)
    {
        this.getComboTipoSubgrupo().clearValue();
        this.getStoreTipoSubgruposAsignaturaStore().load(
        {
            params :
            {
                asignaturaId : asignaturaId,
                semestreId : semestreId
            },
            callback : function(records, options, success)
            {
                if (success)
                {
                    var keys = [];
                    Ext.each(records, function(value)
                    {
                        keys.push(value.get('id'));
                    });
                    this.getComboTipoSubgrupo().setValue(keys);
                }
            },
            scope : this
        });
    },

    updateComboAsignaturas : function()
    {
        var areaId = this.getComboArea().getValue();

        if (areaId)
        {
            this.getComboAsignatura().clearValue();
            this.getComboSemestre().clearValue();
            this.getComboAsignatura().enable();

            this.getStoreAsignaturasStore().load(
            {
                params :
                {
                    areaId : areaId
                },
                scope : this
            });
        }
    },

    updateComboSemestres : function()
    {
        var asignaturaId = this.getComboAsignatura().getValue();
        if (asignaturaId)
        {
            this.getComboSemestre().enable();

            this.getStoreSemestresAsignaturaStore().load(
            {
                params :
                {
                    asignaturaId : asignaturaId
                },
                scope : this,
                callback : function(semestres, operation, success)
                {
                    if (semestres.length === 1)
                    {
                        this.getComboSemestre().select(semestres[0]);
                        this.onSemestreSelected();
                    }
                }
            });
        }
    },

    updateListaProfesores : function(profesores)
    {
        this.getSelectorProfesores().addProfesores(profesores);
    },

    setEstadoComponentes : function()
    {
        var departamentoId = this.getComboDepartamento().getValue();
        var areaId = this.getComboArea().getValue();
        var semestreId = this.getComboSemestre().getValue();
        var asignaturaId = this.getComboAsignatura().getValue();
        var profesorId = this.getSelectorProfesores().getValue();

        this.getEstadoAsignacion().setVisible(asignaturaId != null);
        this.getComboArea().setDisabled(departamentoId == null);
        this.getComboAsignatura().setDisabled(areaId == null);
        this.getComboSemestre().setDisabled(asignaturaId == null);
        this.getBotonRefrescar().setDisabled(asignaturaId == null);
        this.getBotonMenuAcciones().setDisabled(departamentoId == null);
        this.getBotonMiPOD().setDisabled(profesorId == null)
        this.getSelectorProfesores().setDisabled(asignaturaId == null);
        this.getSelectorClasesSinPlanificar().setDisabled(profesorId == null);

        this.getBotonDetalleAsignaturaPOD().setDisabled(departamentoId == null);
        this.getBotonAsignaturaPOD().setDisabled(areaId == null);
        this.getCheckboxOcupacionProfesor().setDisabled(profesorId == null);
        this.getBotonRefrescar().setDisabled(semestreId == null || asignaturaId == null);
        this.getPanelFiltroGrupoTipo().setDisabled(asignaturaId == null || semestreId == null);
        this.getBotonCalendarioGenerica().setDisabled(asignaturaId == null || semestreId == null);
        this.getBotonCalendarioDetalle().setDisabled(asignaturaId == null || semestreId == null);
        if (profesorId == null)
        {
            this.getCheckboxOcupacionProfesor().setValue(false);
        }
    }
});