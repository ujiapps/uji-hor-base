Ext.define('HOR.controller.ControllerAsignacionProfesorForm',
{
    extend : 'Ext.app.Controller',
    stores : [],
    requires : [ 'HOR.view.pod.asignacion.FormAsignacionProfesores' ],
    refs : [
    {
        selector : 'formAsignacionProfesores',
        ref : 'formAsignacionProfesores'
    },
    {
        selector : 'panelCalendarioAsignatura',
        ref : 'panelCalendarioAsignatura'
    },
    {
        selector : 'filtroProfesores',
        ref : 'filtroProfesores'
    },
    {
        selector : 'panelEdicionPOD panel[name=panelFiltroGrupoTipo]',
        ref : 'panelFiltroGrupoTipo'
    },
    {
        selector : 'selectorProfesores',
        ref : 'selectorProfesores'
    },
    {
        selector : 'selectorClasesSinPlanificar',
        ref : 'selectorClasesSinPlanificar'
    } ],

    init : function()
    {
        this.control(
        {
            'panelCalendarioAsignatura' :
            {
                eventasignaaula : function(cal, rec)
                {
                    this.cargarDatosFormulario(rec);
                }
            },
            'formAsignacionProfesores numberfield[name=creditos]' :
            {
                change : function()
                {
                    this.getFormAsignacionProfesores().updateFormState();
                }
            },
            'formAsignacionProfesores numberfield[name=creditosComputables]' :
            {
                change : function()
                {
                    this.getFormAsignacionProfesores().updateFormState();
                }
            },
            'formAsignacionProfesores button[name=close]' :
            {
                click : this.cerrarFormulario
            },
            'formAsignacionProfesores button[name=save]' :
            {
                click : this.guardarDatosFormulario
            },
            'formAsignacionProfesores checkbox[name=detalleManual]' :
            {
                change : this.toggleDetalleManual
            }
        });
    },

    cerrarFormulario : function()
    {
        this.activarControlesPantalla();

        var me = this;

        if (this.getFormAsignacionProfesores().dirty)
        {
            Ext.Msg.confirm('Dades sense guardar', 'Tens dades sense guardar en el event, estàs segur de voler tancar la edició?', function(btn, text)
            {
                if (btn == 'yes')
                {
                    me.getController('ControllerCalendarioAsignaturas').createCalendar();
                }
            });
        }
        else
        {
            this.getController('ControllerCalendarioAsignaturas').createCalendar();
        }
    },

    toggleDetalleManual : function(checkbox, enable)
    {
        var form = this.getFormAsignacionProfesores();
        if (enable)
        {
            form.disableFechas(false);
        }
        else
        {
            form.disableFechas(true);
            form.query('detalleEventoGrupoManual').forEach(function(group)
            {
                group.checkAllBoxes();
            });
        }
        this.getFormAsignacionProfesores().updateFormState();
    },

    guardarDatosFormulario : function()
    {
        var formulario = Ext.ComponentQuery.query('formAsignacionProfesores')[0];
        var data = formulario.getEstructuraDatos();

        var creditos = this.getFormAsignacionProfesores().getCreditos();

        if (data.creditos !== null && (data.creditos > creditos || data.creditos <= 0))
        {
            Ext.MessageBox.show(
            {
                title : 'Error amb els crèdits',
                msg : "El nombre de crèdits assignats al professor no pot superar el nombre total de crèdits de la assignatura i ha de ser major de zero",
                icon : Ext.MessageBox.ERROR,
                buttons : Ext.Msg.OK
            });

            return;
        }

        if (data.detalleManual && data.fechas === "")
        {
            Ext.MessageBox.show(
            {
                title : 'Error amb les dates',
                msg : 'No es pot marcar detall manual sense seleccionar cap data activa de clase',
                icon : Ext.MessageBox.ERROR,
                buttons : Ext.Msg.OK
            });

            return;
        }

        Ext.Ajax.request(
        {
            url : '/hor/rest/profesor/sincronizaitemsdetalle/',
            method : 'POST',
            jsonData : JSON.stringify(data),
            success : function(response)
            {
                var form = this.getFormAsignacionProfesores();
                form.originalFormState = JSON.stringify(form.getEstructuraDatos());
                form.updateFormState();
            },
            failure : function(response)
            {
                var serverResponse = JSON.parse(response.responseText);
                Ext.MessageBox.show(
                {
                    title : 'Error',
                    msg : serverResponse.message,
                    icon : Ext.MessageBox.ERROR,
                    buttons : Ext.Msg.OK
                });
            },
            scope : this
        });
    },

    activarControlesPantalla : function()
    {
        this.getFiltroProfesores().setDisabled(false);
        this.getPanelFiltroGrupoTipo().setDisabled(false);
        this.getSelectorProfesores().setDisabled(false);
        this.getSelectorClasesSinPlanificar().setDisabled(false);
    }
});