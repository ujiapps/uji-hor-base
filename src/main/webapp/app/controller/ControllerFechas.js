Ext.define('HOR.controller.ControllerFechas',
{
    extend : 'Ext.app.Controller',
    stores : [ 'StoreSemestreDetalleEstudios', 'StoreEstudiosTodos', 'StoreSemestreDetalles' ],
    model : [ 'SemestreDetalleEstudio' ],
    views : [ 'semestres.PanelSemestres' ],
    refs : [
    {
        selector : 'gridSemestreEstudioss',
        ref : 'gridSemestreEstudioss'
    } ],

    init : function()
    {
        this.control(
        {

        });
    }

});