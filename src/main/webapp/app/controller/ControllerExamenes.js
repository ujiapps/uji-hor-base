Ext.define('HOR.controller.ControllerExamenes',
{
    extend : 'Ext.app.Controller',
    stores : [ 'StoreEstudios', 'StoreConvocatoria', 'StoreExamenesNoPlanificados', 'StoreExamenesPlanificados', 'StoreTiposExamen', 'StoreExamenes', 'StoreCursosTitulacion', 'StoreAulasEstudio' ],

    tooltip : undefined,

    refs : [
    {
        selector : 'filtroExamenes combobox[name=estudio]',
        ref : 'comboEstudio'
    },
    {
        selector : 'filtroExamenes combobox[name=convocatoria]',
        ref : 'comboConvocatoria'
    },
    {
        selector : 'panelExamenes combobox[name=curso]',
        ref : 'comboCurso'
    },
    {
        selector : 'filtroExamenes button[name=imprimir]',
        ref : 'botonImprimir'
    },
    {
        selector : 'panelExamenes panelCalendarioExamenes',
        ref : 'panelCalendarioExamenes'
    },
    {
        selector : 'panelExamenes selectorExamenes',
        ref : 'selectorExamenes'
    },
    {
        selector : 'filtroExamenes button[name=controles]',
        ref : 'botonControles'
    },
    {
        selector : 'panelExamenes panel[name=contenedorCalendario]',
        ref : 'panelContenedorCalendario'
    },
    {
        selector : 'panelExamenes panel[name=seccionCalendario]',
        ref : 'panelSeccionCalendario'
    },
    {
        selector : 'ventanaEdicionExamen',
        ref : 'ventanaEdicionExamen'
    },
    {
        selector : 'ventanaAsignacionAula',
        ref : 'ventanaAsignacionAula'
    } ],

    init : function()
    {
        this.control(
        {
            'filtroExamenes combobox[name=estudio]' :
            {
                select : this.onEstudioSelected
            },
            'filtroExamenes combobox[name=convocatoria]' :
            {
                select : this.onConvocatoriaSelected
            },
            'filtroExamenes button[name=controles]' :
            {
                click : this.muestraControlExamenes
            },
            'panelExamenes selectorExamenes button' :
            {
                click : this.planificarExamen
            },
            'filtroExamenes button[name=imprimir]' :
            {
                click : this.imprimir
            },
            'panelExamenes combo[name=curso]' :
            {
                select : function()
                {
                    this.loadExamenes();
                    this.crearCalendario();
                }
            },
            'ventanaEdicionExamen button[name=guardar]' :
            {
                click : this.guardarVentanaExamen
            },
            'ventanaEdicionExamen button[name=cancelar]' :
            {
                click : this.cerrarVentanaExamen
            },
            'ventanaAsignacionAula button[name=guardar]' :
            {
                click : this.guardarVentanaAsignacionAula
            },
            'ventanaAsignacionAula button[name=cancelar]' :
            {
                click : this.cerrarVentanaAsignacionAula
            }

        });
    },

    cargaVentanaExamen : function(examenId)
    {
        var storeExamenes = this.getStoreExamenesStore();
        storeExamenes.load(
        {
            url : '/hor/rest/examen/' + examenId,
            scope : this,
            callback : function(records, operation, success)
            {
                if (records && success)
                {
                    this.getVentanaEdicionExamen().setData(records[0]);
                    this.getVentanaEdicionExamen().show();
                }
            }
        });
    },

    cargaVentanaAsignarAula : function(examenId)
    {
        var storeExamenes = this.getStoreExamenesStore();
        storeExamenes.load(
        {
            url : '/hor/rest/examen/' + examenId,
            scope : this,
            callback : function(records, operation, success)
            {
                if (records && success)
                {
                    this.getVentanaAsignacionAula().setData(records[0]);
                    this.getVentanaAsignacionAula().show();
                }
            }
        });
    },

    imprimir : function()
    {
        var estudioId = this.getComboEstudio().getValue();
        var convocatoriaId = this.getComboConvocatoria().getValue();
        var paramId = -1;

        window.open("http://www.uji.es/cocoon/" + session + "/" + estudioId + "/" + convocatoriaId + "/" + paramId + "/horario-examenes.pdf");
    },

    guardarVentanaExamen : function()
    {
        var data = this.getVentanaEdicionExamen().getData();
        var storeExamenes = this.getStoreExamenesStore();
        var estudioId = this.getComboEstudio().getValue();

        var examen = storeExamenes.getById(parseInt(data.examenId));
        examen.set('tipoExamenId', data.tipoExamenId);
        examen.set('comentario', data.comentario);
        examen.set('comentarioES', data.comentarioES);
        examen.set('comentarioUK', data.comentarioUK);
        examen.set('fecha', Ext.Date.parse(data.fecha, 'd/m/Y'));
        examen.set('estudioId', estudioId);
        examen.set('definitivo', data.definitivo == 'on');

        storeExamenes.sync(
        {
            success : function()
            {
                this.getPanelCalendarioExamenes().store.reload();
                this.cerrarVentanaExamen();
            },
            failure : function(batch)
            {
                Ext.Msg.alert('Error', 'No s`ha pogut modificar l\'examen. Comprova que la data introduïda està dins de la convocatòria d\'exàmens i no és un dia festiu');
                storeExamenes.rejectChanges();
            },
            scope : this
        });
    },

    cerrarVentanaExamen : function()
    {
        this.getVentanaEdicionExamen().hide();
    },

    guardarVentanaAsignacionAula : function()
    {
        var data = this.getVentanaAsignacionAula().getData();
        data.aulas = data.aulas.join();

        Ext.Ajax.request(
        {
            url : '/hor/rest/examen/sincronizaaulas',
            method : 'POST',
            jsonData : JSON.stringify(data),
            success : function(response)
            {
                this.cerrarVentanaAsignacionAula();
            },
            failure : function(response)
            {
                var serverResponse = JSON.parse(response.responseText);
                Ext.MessageBox.show(
                {
                    title : 'Error',
                    msg : serverResponse.message,
                    icon : Ext.MessageBox.ERROR,
                    buttons : Ext.Msg.OK
                });
            },
            scope : this
        });
    },

    cerrarVentanaAsignacionAula : function()
    {
        this.getVentanaAsignacionAula().hide();
    },

    onEstudioSelected : function()
    {
        var estudioId = this.getComboEstudio().getValue();
        this.cargaComboConvocatorias(estudioId);
        this.cargaComboCursos(estudioId);
        this.cargaAulas(estudioId);
        this.setEstadoComponentes();
    },

    cargaComboCursos : function(estudioId)
    {
        this.getComboCurso().clearValue();
        this.getStoreCursosTitulacionStore().load(
        {
            params :
            {
                estudioId : estudioId
            },
            callback : function(records, options, success)
            {
                if (success)
                {
                    var keys = [];
                    Ext.each(records, function(value)
                    {
                        keys.push(value.get('curso'));
                    });
                    this.getComboCurso().setValue(keys);
                }
            },
            scope : this
        });
    },

    cargaComboConvocatorias : function(estudioId)
    {
        this.getComboConvocatoria().clearValue();
        this.getStoreConvocatoriaStore().load(
        {
            url : '/hor/rest/examen/estudio/' + estudioId + '/convocatoria',
            callback : function()
            {
                this.setEstadoComponentes()
            },
            scope : this
        });
    },

    cargaAulas : function(estudioId)
    {
        this.getStoreAulasEstudioStore().load(
        {
            url : '/hor/rest/aula/estudio/' + estudioId + '/todas',
            callback : function()
            {
                this.setEstadoComponentes();
            },
            scope : this
        });
    },

    muestraControlExamenes : function()
    {
        var titulacionId = this.getComboEstudio().getValue(), convocatoriaId = this.getComboConvocatoria().getValue() || -1;
        window.open("http://www.uji.es/cocoon/" + session + "/" + titulacionId + "/" + convocatoriaId + "/validacion-examenes.pdf");
    },

    onConvocatoriaSelected : function()
    {
        this.setEstadoComponentes();
        this.loadExamenes();
        this.crearCalendario();
        this.estableceFechaCalendarioPrimerDiaConvocatoria();
    },

    estableceFechaCalendarioPrimerDiaConvocatoria : function()
    {
        var convocatoriaId = this.getComboConvocatoria().getValue();
        var storeConvocatoria = this.getStoreConvocatoriaStore();
        var convocatoria = storeConvocatoria.getById(convocatoriaId);
        this.getPanelCalendarioExamenes().getActiveView().setStartDate(convocatoria.get('inicio'));
    },

    setEstadoComponentes : function()
    {
        var estudioId = this.getComboEstudio().getValue();
        var convocatoriaId = this.getComboConvocatoria().getValue();

        this.getComboConvocatoria().setDisabled(estudioId == null);
        this.getBotonControles().setDisabled(estudioId == null);
        this.getBotonImprimir().setDisabled(estudioId == null || convocatoriaId == null);
        this.getPanelSeccionCalendario().setDisabled(estudioId == null || convocatoriaId == null);
    },

    loadExamenes : function()
    {
        var estudioId = this.getComboEstudio().getValue();
        var convocatoriaId = this.getComboConvocatoria().getValue();
        var cursosId = this.getComboCurso().getValue().join(';');

        var store = this.getStoreExamenesNoPlanificadosStore();

        store.load(
        {
            callback : function(examenes)
            {
                this.actualizaSelectorExamenes(examenes);
                this.setEstadoComponentes();
            },
            params :
            {
                estudioId : estudioId,
                convocatoriaId : convocatoriaId,
                cursosId : cursosId
            },
            scope : this
        });
    },

    clickExamen : function(calendar, record)
    {
    },

    crearCalendario : function()
    {
        var ref = this;
        var indiceVistaActual = this.getIndiceVistaCalendario();
        var inicio = this.getPanelCalendarioExamenes().getActiveView().getViewBounds().start;
        var convocatoriaId = this.getComboConvocatoria().getValue();
        var cursosId = this.getComboCurso().getValue().join(';');

        var params =
        {
            estudioId : this.getComboEstudio().getValue(),
            convocatoriaId : convocatoriaId,
            cursosId : cursosId
        };

        var store = Ext.create('HOR.store.StoreExamenesPlanificados');
        store.getProxy().extraParams = params;

        var panelPadre = this.getPanelContenedorCalendario();

        var storeConvocatoria = this.getStoreConvocatoriaStore();
        var convocatoria = storeConvocatoria.getById(convocatoriaId);

        panelPadre.removeAll();

        panelPadre.add(
        {
            xtype : 'panelCalendarioExamenes',
            eventStore : store,
            showMultiDayView : true,
            readOnly : false,
            activeItem : indiceVistaActual,
            viewConfig :
            {
                viewStartHour : 8,
                viewEndHour : 22
            },

            listeners :
            {
                dayclick : function(dt, allday, el)
                {
                    return false;
                },

                eventclick : function(calendar, record)
                {
                    ref.clickExamen(calendar, record);
                    return false;
                },

                rangeselect : function()
                {
                    return false;
                },

                desplanificar : this.desplanificar,
                dividir : this.dividir,

                afterrender : function()
                {
                    store.load();
                },

                beforeeventmove : function(cal, rec, date)
                {
                    if (rec.get(Extensible.calendar.data.EventMappings.CalendarId.name) == "100")
                    {
                        return false;
                    }
                },

                editarDetalleExamen : function(calendar, examen)
                {
                    ref.cargaVentanaExamen(examen.internalId);
                },

                asignarAula : function(calendar, examen)
                {
                    ref.cargaVentanaAsignarAula(examen.internalId);
                },

                eventover : function(calendar, record, event)
                {
                    ref.mostrarTooltip(event, record);
                },

                eventout : function()
                {
                    if (ref.tooltip)
                    {
                        ref.tooltip.destroy();
                        delete ref.tooltip;
                    }
                }
            }
        });

        this.getPanelCalendarioExamenes().getActiveView().setStartDate(inicio);
    },

    mostrarTooltip : function(event, record)
    {
        var panelCalendarioExamen = Ext.ComponentQuery.query('panelCalendarioExamenes')[0];
        if (!this.tooltip && (record.data.Comentario || record.data.Asignaturas))
        {
            this.tooltip = Ext.create('Ext.tip.ToolTip',
            {
                hidden : true,
                target : panelCalendarioExamen.getEl(),
                trackMouse : true,
                autoHide : false
            });
        }

        if (record.data.Comentario || record.data.Asignaturas)
        {
            var toolTip = '';
            if (record.data.Asignaturas)
            {
                toolTip += record.data.Asignaturas;
            }
            if (record.data.Comentario)
            {
                toolTip += '\n<strong>' + record.data.Comentario + '</strong>'
            }
            if (record.data.Aulas)
            {
                toolTip += '<br/>Aules: <strong>' + record.data.Aulas + '</strong>'
            }
            if (record.data.Definitivo == 'true')
            {
                toolTip += '<br/><strong>Definitiu</strong>'
            }
            this.tooltip.setTarget(panelCalendarioExamen.getEl());
            this.tooltip.update(toolTip);
            this.tooltip.show();
        }
    },

    getIndiceVistaCalendario : function()
    {
        var currentView = this.getPanelCalendarioExamenes().getActiveView().xtype;

        if (currentView === 'extensible.multiweekview')
        {
            return 1;
        }
        else if (currentView === 'extensible.multidayview')
        {
            return 0;
        }
    },

    desplanificar : function(event)
    {
        var ref = this;
        var examenId = event.internalId;
        var view = this.getActiveView();
        var selectorExamenes = Ext.ComponentQuery.query('selectorExamenes')[0];
        var store = Ext.data.StoreManager.get("StoreExamenesNoPlanificados");

        Ext.Ajax.request(
        {
            url : '/hor/rest/examen/desplanifica/' + examenId,
            method : 'PUT',
            success : function()
            {
                view.refresh(true);
                ref.store.reload();
                store.reload(
                {
                    callback : function(records)
                    {
                        selectorExamenes.addExamenes(records);
                    }
                });
            }
        });
    },

    dividir : function(event)
    {
        var examenId = event.internalId;
        var ref = this;

        Ext.Ajax.request(
        {
            url : '/hor/rest/examen/dividir/' + examenId,
            method : 'PUT',
            success : function()
            {
                ref.store.reload();
            }
        });
    },

    actualizaSelectorExamenes : function(examenes)
    {
        this.getSelectorExamenes().addExamenes(examenes);
    },

    planificarExamen : function(button)
    {
        var ref = this;
        var view = this.getPanelCalendarioExamenes().getActiveView();
        var boundsStartDate = view.getViewBounds().start;

        var fechaInicial = boundsStartDate.getFullYear() + '-' + (boundsStartDate.getMonth() + 1) + "-" + boundsStartDate.getDate();
        var estudioId = this.getComboEstudio().getValue();

        Ext.Ajax.request(
        {
            url : '/hor/rest/examen/planificados',
            method : 'POST',
            params :
            {
                examenId : button.examenId,
                fechaInicial : fechaInicial,
                estudioId : estudioId
            },
            success : function()
            {
                button.destroy();
                view.refresh(true);
                ref.getPanelCalendarioExamenes().store.reload();
            }
        });
    }
});