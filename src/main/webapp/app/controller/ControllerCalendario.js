Ext.define('HOR.controller.ControllerCalendario',
{
    extend : 'Ext.app.Controller',
    stores : [ 'StoreCalendarios', 'StoreEventos', 'StoreEventosDetalle', 'StoreGruposAsignaturasSinAsignar', 'StoreConfiguracionAjustada', 'StoreLogsEvento', 'StoreDeshacer' ],
    model : [ 'Configuracion' ],
    refs : [
    {
        selector : 'panelHorarios filtroGrupos',
        ref : 'filtroGrupos'
    },
    {
        selector : 'selectorGrupos',
        ref : 'selectorGrupos'
    },
    {
        selector : 'panelCalendario',
        ref : 'panelCalendario'
    },
    {
        selector : 'panelCalendarioDetalle',
        ref : 'panelCalendarioDetalle'
    },
    {
        selector : 'panelHorarios selectorCalendarios',
        ref : 'selectorCalendarios'
    },
    {
        selector : 'button[name=btnDeshacer]',
        ref : 'botonDeshacer'
    },
    {
        selector : 'button[name=calendarioDetalle]',
        ref : 'botonCalendarioDetalle'
    },
    {
        selector : 'button[name=calendarioGenerica]',
        ref : 'botonCalendarioGenerica'
    },
    {
        selector : 'panelHorarios filtroGrupos combobox[name=grupo]',
        ref : 'comboGrupos'
    },
    {
        selector : 'filtroGrupos selectorCursoAgrupacion[name=cursoAgrupacion]',
        ref : 'selectorCursoAgrupacion'
    },
    {
        selector : 'ventanaLogsEvento',
        ref : 'ventanaLogsEvento'
    },
    {
        selector : 'ventanaUndoEvento',
        ref : 'ventanaUndoEvento'
    } ],

    init : function()
    {
        var ref = this;

        this.control(
        {
            'panelHorarios filtroGrupos combobox[name=grupo]' :
            {
                change : this.refreshActiveCalendar
            },
            'panelHorarios filtroGrupos combobox[name=asignatura]' :
            {
                change : this.refreshActiveCalendar
            },
            'panelHorarios' :
            {
                refreshCalendar : this.refreshCalendar,
                refreshCalendarDetalle : this.refreshCalendarDetalle,
                afterrender : this.onPanelCalendarioRendered
            },
            'panelHorarios selectorCalendarios checkbox' :
            {
                change : this.filtroTipoClaseChanged
            },
            'selectorGrupos button' :
            {
                click : this.addEvento
            },
            'panelCalendario' :
            {
                eventresize : this.updateEvento,
                dayclick : function()
                {
                    return false;
                },
                beforeeventdelete : function(cal, rec)
                {
                    this.deleteEvento(cal, rec);
                    return false;
                },
                eventdivide : function(cal, rec)
                {
                    this.duplicarEvento(cal, rec);
                },
                eventmuestralog : function(cal, rec)
                {
                    this.cargaVentanaLogs(rec.data.EventId);
                },
                eventmuestracambios : function(cal, rec)
                {
                    this.cargaVentanaDeshacer(rec.data.EventId);
                },
                rangeselect : function()
                {
                    return false;
                },
                refresh: function() {
                    this.refreshCalendar();
                },
                eventasignaaula : function(cal, rec)
                {
                    this.mostrarVentanaAsignarAulaAEvento();
                },
                beforeeventmove : function(cal, rec, date)
                {
                    if ((rec.get(Extensible.calendar.data.EventMappings.DetalleManual.name) == 'true') || (rec.get(Extensible.calendar.data.EventMappings.DetalleManualProfesor.name) == 'true'))
                    {
                        return this.moverEventoConDetalleManual(cal, rec, date);
                    }
                }
            },
            'panelHorarios button[name=calendarioDetalle]' :
            {
                click : function(button)
                {
                    if (!button.pressed)
                    {
                        button.toggle();
                    }
                    var otherButton = this.getBotonCalendarioGenerica();
                    if (otherButton.pressed)
                    {
                        otherButton.toggle();
                    }
                    this.getSelectorGrupos().setVisible(false);
                    this.refreshCalendarDetalle();
                }

            },
            'panelHorarios button[name=calendarioGenerica]' :
            {
                click : function(button)
                {
                    if (!button.pressed)
                    {
                        button.toggle();
                    }
                    var otherButton = this.getBotonCalendarioDetalle();
                    if (otherButton.pressed)
                    {
                        otherButton.toggle();
                    }
                    this.getSelectorGrupos().setVisible(true);
                    this.refreshCalendar();
                }
            },
            'panelHorarios button[name=imprimir]' :
            {
                click : this.imprimirCalendario
            },
            'panelHorarios [name=imprimirSemestre]' :
            {
                click : this.imprimirSemestreCalendario
            },
            'panelHorarios [name=imprimirSesionesPOD]' :
            {
                click : this.imprimirSesionesPOD
            },
            'panelHorarios [name=imprimirTitulacion]' :
            {
                click : this.imprimirTitulacionCalendario
            },
            'panelHorarios [name=imprimirAgrupaciones]' :
            {
                click : this.imprimirAgrupacionesCalendario
            },
            'panelHorarios button[name=validar]' :
            {
                click : this.mostrarValidaciones
            },
            'formAsignacionAulas' :
            {
                eventasigaaulacancel : function()
                {
                    this.refreshCalendar();
                }
            },
            'ventanaLogsEvento [name=btnCerrar]' :
            {
                click : this.cerrarVentanaLogs
            },
            'panelHorarios button[name=btnDeshacer]' :
            {
                click : function() {
                    this.cargaVentanaDeshacer();
                }
            },
            'ventanaUndoEvento [name=btnCerrar]' :
            {
                click : this.cerrarVentanaDeshacer
            },
            'ventanaUndoEvento [name=btnDeshacer]' :
            {
                click : this.deshacerUltimoCambio
            }
        });
    },

    debounceTipoClase: null,

    filtroTipoClaseChanged: function() {
        if (this.debounceTipoClase) {
            clearTimeout(this.debounceTipoClase);
        }

        var ref = this;
        this.debounceTipoClase = setTimeout(function() {
            ref.debounceTipoClase = null;
            ref.refreshActiveCalendar();
        }, 1500);

    },

    cargaVentanaLogs : function(itemId)
    {
        var storeLogsEveno = this.getStoreLogsEventoStore();
        storeLogsEveno.load(
        {
            url : '/hor/rest/log/evento/' + itemId,
            callback : function(records, operation, success)
            {
                if (records && success)
                {
                    this.getVentanaLogsEvento().show();
                    this.getVentanaLogsEvento().maximize();
                }
            },
            scope : this
        });
    },

    cerrarVentanaLogs : function()
    {
        this.getVentanaLogsEvento().hide();
    },

    cargaVentanaDeshacer : function(itemId)
    {
        var storeDeshacer = this.getStoreDeshacerStore();
        var url = (itemId === undefined) ? '/hor/rest/undo/' : '/hor/rest/undo/item/' + itemId;
        storeDeshacer.load(
        {
            url : url,
            callback : function(records, operation, success)
            {
                if (records && success)
                {
                    this.getVentanaUndoEvento().show();
                    this.getVentanaUndoEvento().maximize();
                }
            },
            scope : this
        });
    },

    cerrarVentanaDeshacer : function()
    {
        this.getVentanaUndoEvento().hide();
    },

    deshacerUltimoCambio : function()
    {
        if (this.getStoreDeshacerStore().getAt(0) == null) {
            alert('No hi ha cap canvi disponible per desfer');
            return;
        }

        var record = this.getStoreDeshacerStore().getAt(0).data;
        if (confirm('Desfer el canvi? ' + record.descripcion)) {
            var ref = this;
            Ext.Ajax.request({
                url: '/hor/rest/undo/log/' + record.id,
                method: 'PUT',
                jsonData: {},
                success: function () {
                    ref.refreshActiveCalendar();
                    ref.cerrarVentanaDeshacer();
                },
                failure: function(res) {
                    var myResponseJSON = JSON.parse(res.responseText);
                    Ext.Msg.alert('Error', myResponseJSON.message);
                    ref.refreshActiveCalendar();
                    ref.cerrarVentanaDeshacer();
                }
            });
        }
    },

    refreshActiveCalendar : function()
    {
        if (this.getFiltroGrupos().down('button[name=calendarioDetalle]').pressed)
        {
            this.refreshCalendarDetalle();
        }
        else
        {
            this.refreshCalendar();
        }
    },

    refreshCalendarDetalle : function()
    {
        var titulaciones = this.getFiltroGrupos().down('combobox[name=estudio]');
        var semestres = this.getFiltroGrupos().down('combobox[name=semestre]');
        var grupos = this.getFiltroGrupos().getGruposSelected();
        var asignaturas = this.getFiltroGrupos().getAsignaturasSelected();
        var calendarios = this.getSelectorCalendarios().getCalendarsSelected();
        var cursoId = this.getSelectorCursoAgrupacion().getCursoId();
        var agrupacionId = this.getSelectorCursoAgrupacion().getAgrupacionId();
        var storeConfiguracion = this.getStoreConfiguracionAjustadaStore();

        if (!((cursoId || agrupacionId) && titulaciones.getValue() && grupos && semestres.getValue()))
        {
            return;
        }

        var ref = this;
        storeConfiguracion.load(
        {
            params :
            {
                estudioId : titulaciones.getValue(),
                cursoId : this.getSelectorCursoAgrupacion().getCursoId(),
                agrupacionId : this.getSelectorCursoAgrupacion().getAgrupacionId(),
                semestreId : semestres.getValue(),
                gruposId : grupos,
                asignaturasId : asignaturas
            },
            scope : this,
            callback : function(records, operation, success)
            {
                if (success)
                {
                    var record = records[0];
                    var fechaInicio = record.get('horaInicio');
                    var fechaFin = record.get('horaFin');

                    var inicio = Ext.Date.parse(fechaInicio, 'd/m/Y H:i:s', true);
                    var fin = Ext.Date.parse(fechaFin, 'd/m/Y H:i:s', true);
                    var horaInicio = Ext.Date.format(inicio, 'H');
                    var horaFin = Ext.Date.format(fin, 'H');

                    var panelCalendario = ref.getPanelCalendario();
                    if (!panelCalendario)
                    {
                        panelCalendario = ref.getPanelCalendarioDetalle();
                    }
                    var panelPadre = panelCalendario.up('panel');

                    Ext.Array.each(Ext.ComponentQuery.query('panelCalendario'), function(panel)
                    {
                        panel.destroy();
                    });

                    Ext.Array.each(Ext.ComponentQuery.query('panelCalendarioDetalle'), function(panel)
                    {
                        panel.destroy();
                    });

                    var eventos = Ext.create('HOR.store.StoreEventosDetalle');

                    Extensible.calendar.data.EventModel.reconfigure();
                    Ext.Ajax.request(
                    {
                        url : '/hor/rest/semestredetalle/estudio/' + titulaciones.getValue() + "/semestre/" + semestres.getValue(),
                        params :
                        {
                            cursoId : this.getSelectorCursoAgrupacion().getCursoId()
                        },
                        method : 'GET',
                        success : function(response)
                        {
                            var jsonResp = Ext.decode(response.responseText);
                            var cadenaFechaInicio = jsonResp.data[0].fechaInicio;
                            var inicio = Ext.Date.parse(cadenaFechaInicio, 'd/m/Y H:i:s', true);
                            var fin = new Date();
                            fin.setDate(inicio.getDate() + 7);

                            panelPadre.add(
                            {
                                xtype : 'panelCalendarioDetalle',
                                eventStore : eventos,
                                showMultiDayView : true,
                                startDate : inicio,
                                enableEditDetails : false,
                                viewConfig :
                                {
                                    viewStartHour : horaInicio,
                                    viewEndHour : horaFin
                                },

                                onInitDrag : function()
                                {

                                },
                                listeners :
                                {
                                    dayclick : function(dt, allday, el)
                                    {
                                        return false;
                                    },

                                    eventclick : function()
                                    {
                                        return false;
                                    },

                                    rangeselect : function()
                                    {
                                        return false;
                                    },

                                    afterrender : function()
                                    {
                                        params =
                                        {
                                            estudioId : titulaciones.getValue(),
                                            cursoId : ref.getSelectorCursoAgrupacion().getCursoId(),
                                            agrupacionId : ref.getSelectorCursoAgrupacion().getAgrupacionId(),
                                            semestreId : semestres.getValue(),
                                            calendariosIds : calendarios,
                                            asignaturasId : asignaturas,
                                            gruposId : grupos,
                                            startDate : inicio,
                                            endDate : fin
                                        };

                                        eventos.getProxy().extraParams = params;
                                        this.setStartDate(inicio);
                                    },

                                    beforeeventmove : function(cal, rec, date)
                                    {
                                        eventos.rejectChanges();
                                        if (rec.get(Extensible.calendar.data.EventMappings.CalendarId.name) == "100")
                                        {
                                            return false;
                                        }
                                    },

                                    eventover : function(calendar, record, event)
                                    {
                                        ref.mostrarInfoAdicionalEvento(event, record);
                                    },

                                    eventout : function()
                                    {
                                        if (ref.tooltip)
                                        {
                                            ref.tooltip.destroy();
                                            delete ref.tooltip;
                                        }
                                    }
                                }
                            });
                        }
                    });
                }
            }
        });
    },

    refreshCalendar : function()
    {
        var titulaciones = this.getFiltroGrupos().down('combobox[name=estudio]');
        var semestres = this.getFiltroGrupos().down('combobox[name=semestre]');
        var grupos = this.getFiltroGrupos().getGruposSelected();
        var asignaturas = this.getFiltroGrupos().getAsignaturasSelected();
        var calendarios = this.getSelectorCalendarios().getCalendarsSelected();
        var cursoId = this.getSelectorCursoAgrupacion().getCursoId();
        var agrupacionId = this.getSelectorCursoAgrupacion().getAgrupacionId();
        var storeConfiguracion = this.getStoreConfiguracionAjustadaStore();

        if (!((cursoId || agrupacionId) && titulaciones.getValue() && grupos && semestres.getValue()))
        {
            return;
        }

        var ref = this;
        storeConfiguracion.load(
        {
            params :
            {
                estudioId : titulaciones.getValue(),
                cursoId : this.getSelectorCursoAgrupacion().getCursoId(),
                agrupacionId : this.getSelectorCursoAgrupacion().getAgrupacionId(),
                semestreId : semestres.getValue(),
                gruposId : grupos,
                asignaturasId : asignaturas
            },
            scope : this,
            callback : function(records, operation, success)
            {
                if (success)
                {
                    var record = records[0];
                    var fechaInicio = record.get('horaInicio');
                    var fechaFin = record.get('horaFin');

                    var inicio = Ext.Date.parse(fechaInicio, 'd/m/Y H:i:s', true);
                    var fin = Ext.Date.parse(fechaFin, 'd/m/Y H:i:s', true);
                    var horaInicio = Ext.Date.format(inicio, 'H');
                    var horaFin = Ext.Date.format(fin, 'H');

                    var panelCalendario = ref.getPanelCalendario();
                    if (!panelCalendario)
                    {
                        panelCalendario = ref.getPanelCalendarioDetalle();
                    }
                    var panelPadre = panelCalendario.up('panel');

                    Ext.Array.each(Ext.ComponentQuery.query('panelCalendario'), function(panel)
                    {
                        panel.destroy();
                    });

                    Ext.Array.each(Ext.ComponentQuery.query('panelCalendarioDetalle'), function(panel)
                    {
                        panel.destroy();
                    });

                    var eventos = Ext.create('HOR.store.StoreEventos');

                    eventos.addListener('write', function()
                    {
                        ref.getPanelCalendario().getEventStore().reload();

                    }, this);

                    Extensible.calendar.data.EventModel.reconfigure();

                    params =
                    {
                        estudioId : titulaciones.getValue(),
                        cursoId : ref.getSelectorCursoAgrupacion().getCursoId(),
                        agrupacionId : ref.getSelectorCursoAgrupacion().getAgrupacionId(),
                        semestreId : semestres.getValue(),
                        calendariosIds : calendarios,
                        gruposId : grupos,
                        asignaturasId : asignaturas
                    };
                    eventos.getProxy().extraParams = params;

                    panelPadre.add(
                    {
                        xtype : 'panelCalendario',
                        eventStore : eventos,
                        showMultiDayView : true,
                        viewConfig :
                        {
                            viewStartHour : horaInicio,
                            viewEndHour : horaFin
                        },
                        listeners :
                        {
                            eventover : function(calendar, record, event)
                            {
                                ref.mostrarInfoAdicionalEvento(event, record);
                            },
                            eventout : function()
                            {
                                if (ref.tooltip)
                                {
                                    ref.tooltip.destroy();
                                    delete ref.tooltip;
                                }
                            },
                            afterrender : function()
                            {
                                eventos.load();
                            }
                        }
                    });
                }
            }
        });
    },

    mostrarInfoAdicionalEvento : function(event, record)
    {
        if (event.disabled)
        {
            this.tooltip.hide();
            return;
        }

        if (!this.tooltip)
        {
            var panelCalendario = Ext.ComponentQuery.query('panelCalendario,panelCalendarioDetalle')[0];
            this.tooltip = Ext.create('Ext.tip.ToolTip',
            {
                hidden : true,
                target : panelCalendario.getEl(),
                trackMouse : true,
                autoHide : false
            });
        }
        if (record.data.NombreAsignatura)
        {
            var panelCalendario = Ext.ComponentQuery.query('panelCalendario,panelCalendarioDetalle')[0];
            this.tooltip.setTarget(panelCalendario.getEl());

            var nombre = record.data.NombreAsignatura;
            var plazas = record.data.PlazasAula;
            var comunes = record.data.Comunes;
            var fechasDetalles = record.data.FechasDetalles;
            var tooltipHtml = nombre + '<br />';
            tooltipHtml += record.data.Title;

            if (comunes)
            {
                tooltipHtml += ' (' + comunes + ')';
            }
            if (plazas)
            {
                tooltipHtml += '<br/>Plaçes aula: ' + plazas;
            }
            if (fechasDetalles)
            {
                tooltipHtml += '<br/>Dates: ' + fechasDetalles;
            }
            if (record.data.Comentarios)
            {
                tooltipHtml += '<br/>Comentari: ' + record.data.Comentarios;
            }

            this.tooltip.update(tooltipHtml);
            this.tooltip.show();
        }
        else
        {
            if (this.tooltip)
            {
                this.tooltip.destroy();
                delete this.tooltip;
            }
        }
    },

    addEvento : function(button)
    {
        var grupoId = button.grupoAsignaturaId;
        var storeGrupo = this.getStoreGruposAsignaturasSinAsignarStore();
        var record = storeGrupo.getById(grupoId);
        record.set('asignado', true);
        var ref = this;

        var estudio = this.getFiltroGrupos().down('combobox[name=estudio]').getValue();
        var semestre = this.getFiltroGrupos().down('combobox[name=semestre]').getValue();

        storeGrupo.getProxy().extraParams =
        {
            estudioId : estudio,
            cursoId : this.getSelectorCursoAgrupacion().getCursoId(),
            agrupacionId : this.getSelectorCursoAgrupacion().getAgrupacionId(),
            semestreId : semestre
        };

        storeGrupo.sync(
        {
            callback : function()
            {
                button.destroy();
                ref.getPanelCalendario().getEventStore().reload();
            }
        });
    },

    updateEvento : function(calendario, registro)
    {
        var storeEventos = this.getStoreEventosStore();
        storeEventos.sync();
    },

    deleteEvento : function(calendario, registro)
    {
        var calendario = this.getPanelCalendario();
        var store = calendario.store;
        store.remove(registro);

        store.sync(
        {
            success : function()
            {
                this.getSelectorGrupos().fireEvent("updateGrupos");
            },
            scope : this
        });
    },

    duplicarEvento : function(calendario, registro)
    {
        var calendario = this.getPanelCalendario();

        Ext.Ajax.request(
        {
            url : '/hor/rest/calendario/eventos/generica/divide/' + registro.get("EventId"),
            method : 'POST',
            success : function(response)
            {
                calendario.getEventStore().reload();
            },
            failure : function(response)
            {
                if (response.responseXML)
                {
                    var msgList = response.responseXML.getElementsByTagName("msg");

                    if (msgList && msgList[0] && msgList[0].firstChild)
                    {
                        Ext.MessageBox.show(
                        {
                            title : 'Server error',
                            msg : msgList[0].firstChild.nodeValue,
                            icon : Ext.MessageBox.ERROR,
                            buttons : Ext.Msg.OK
                        });
                    }
                }
            }
        });
    },

    onPanelCalendarioRendered : function()
    {
        var panelPadre = Ext.ComponentQuery.query('panelHorarios panel[name=contenedorCalendario]')[0];
        var eventos = Ext.create('HOR.store.StoreEventos');
        Extensible.calendar.data.EventModel.reconfigure();

        panelPadre.add(
        {
            xtype : 'panelCalendario',
            flex : 1,
            eventStore : eventos,
            showMultiDayView : true,
            viewConfig :
            {
                viewStartHour : 8,
                viewEndHour : 22
            }
        });
    },

    mostrarVentanaAsignarAulaAEvento : function()
    {
        Ext.ComponentQuery.query('panelCalendario')[0].showAsignarAulaView();
    },

    imprimirCalendario : function(titulacion, curso, agrupacion, semestre, grupo)
    {
        if (this.getBotonCalendarioGenerica().pressed)
        {
            if (curso != null)
            {
                window.open("http://www.uji.es/cocoon/" + session + "/" + titulacion + "/" + curso + "/" + semestre + "/" + grupo + "/horario-semana-generica.pdf");
            }
            else
            {
                window.open("http://www.uji.es/cocoon/" + session + "/" + titulacion + "/" + agrupacion + "/" + semestre + "/" + grupo + "/horario-agrupacion-semana-generica.pdf");
            }
        }
        else
        {
            if (curso != null)
            {
                window.open("http://www.uji.es/cocoon/" + session + "/" + titulacion + "/" + curso + "/" + semestre + "/" + grupo + "/horario-semana-detalle.pdf");
            }
            else
            {
                window.open("http://www.uji.es/cocoon/" + session + "/" + titulacion + "/" + agrupacion + "/" + semestre + "/" + grupo + "/horario-agrupacion-semana-detalle.pdf");
            }
        }
    },

    imprimirSemestreCalendario : function()
    {
        var titulacion = this.getFiltroGrupos().down('combobox[name=estudio]').getValue();
        var curso = this.getSelectorCursoAgrupacion().getCursoId();
        var agrupacion = this.getSelectorCursoAgrupacion().getAgrupacionId();
        var semestre = this.getFiltroGrupos().down('combobox[name=semestre]').getValue();
        var grupo = this.getFiltroGrupos().down('combobox[name=grupo]').getValue();
        this.imprimirCalendario(titulacion, curso, agrupacion, semestre, grupo);
    },

    imprimirSesionesPOD : function()
    {
        var titulacion = this.getFiltroGrupos().down('combobox[name=estudio]').getValue();
        var curso = this.getSelectorCursoAgrupacion().getCursoId();
        var semestre = this.getFiltroGrupos().down('combobox[name=semestre]').getValue();
        window.open("http://www.uji.es/cocoon/" + session + "/" + titulacion + "/" + curso + "/" + semestre + "/validacion-sesiones-hor.pdf");
    },

    imprimirTitulacionCalendario : function()
    {
        var titulacion = this.getFiltroGrupos().down('combobox[name=estudio]').getValue();
        this.imprimirCalendario(titulacion, -1, null, -1, -1);
    },

    imprimirAgrupacionesCalendario : function()
    {
        var titulacion = this.getFiltroGrupos().down('combobox[name=estudio]').getValue();
        this.imprimirCalendario(titulacion, null, -1, -1, -1);
    },

    mostrarValidaciones : function()
    {
        var titulacion = this.getFiltroGrupos().down('combobox[name=estudio]').getValue();
        var curso = this.getSelectorCursoAgrupacion().getCursoId();
        var semestre = this.getFiltroGrupos().down('combobox[name=semestre]').getValue();
        var grupo = this.getFiltroGrupos().down('combobox[name=grupo]').getValue();

        if (curso === null)
        {
            Ext.Msg.alert('Informació', 'Les validacions no estan disponibles per agrupació, seleccioneu un curs per poder mostrar-les');
            return;
        }
        window.open("http://www.uji.es/cocoon/" + session + "/" + titulacion + "/" + curso + "/" + semestre + "/" + grupo + "/validaciones-horarios.pdf");
    },

    moverEventoConDetalleManual : function(cal, rec, date)
    {
        var horaInicio = rec.get(Extensible.calendar.data.EventMappings.StartDate.name);
        var horaFin = rec.get(Extensible.calendar.data.EventMappings.EndDate.name);

        var me = this.getPanelCalendario();

        if (Extensible.Date.diffDays(horaInicio, date) != 0)
        {
            Ext.Msg.confirm('Event amb detall manual',
                    'Aquest event té detall manual, si el canvies de dia hauràs revisar que les sessions detallades i l\'assignació de POD són correctes. Estàs segur de voler continuar? Aquesta acció no es podrà desfer', function(
                            btn, text)
                    {
                        if (btn == 'yes')
                        {
                            rec.set(Extensible.calendar.data.EventMappings.StartDate.name, date);

                            var diff = Extensible.Date.diff(horaInicio, horaFin);
                            var endDate = Extensible.Date.add(date,
                            {
                                millis : diff
                            });

                            rec.set(Extensible.calendar.data.EventMappings.EndDate.name, endDate);

                            me.getEventStore().save();
                        }
                    });

            return false;
        }

        return true;
    }

});