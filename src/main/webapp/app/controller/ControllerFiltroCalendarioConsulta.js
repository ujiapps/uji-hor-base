Ext.define('HOR.controller.ControllerFiltroCalendarioConsulta',
{
    extend : 'Ext.app.Controller',
    stores : [ 'StoreEstudiosAbiertos', 'StoreCursos', 'StoreSemestres', 'StoreSemestresTodos', 'StoreGrupos', 'StoreAsignaturasEstudio', 'StoreCursosAgrupaciones' ],
    model : [ 'Estudio', 'Curso', 'Semestre', 'Grupo', 'Asignatura' ],
    refs : [
    {
        selector : 'panelHorariosConsulta filtroGruposConsulta',
        ref : 'filtroGrupos'
    },
    {
        selector : 'selectorGruposConsulta',
        ref : 'selectorGrupos'
    },
    {
        selector : 'panelCalendarioDetalle',
        ref : 'panelCalendarioDetalle'
    },
    {
        selector : 'panelCalendario',
        ref : 'panelCalendario'
    },
    {
        selector : 'panelHorariosConsulta filtroGruposConsulta combobox[name=grupo]',
        ref : 'comboGrupos'
    },
    {
        selector : 'filtroGruposConsulta combobox[name=estudio]',
        ref : 'filtroTitulacion'
    },
    {
        selector : 'filtroGruposConsulta combobox[name=semestre]',
        ref : 'filtroSemestre'
    },
    {
        selector : 'filtroGruposConsulta boxselect[name=grupo]',
        ref : 'filtroGrupo'
    },
    {
        selector : 'filtroGruposConsulta boxselect[name=asignatura]',
        ref : 'filtroAsignatura'
    },
    {
        selector : 'filtroGruposConsulta selectorCursoAgrupacionConsulta[name=cursoAgrupacion]',
        ref : 'selectorCursoAgrupacion'
    },
    {
        selector : 'filtroGruposConsulta [name=imprimirTitulacion]',
        ref : 'menuImprimirTitulacion'
    },
    {
        selector : 'filtroGruposConsulta [name=imprimirAgrupaciones]',
        ref : 'menuImprimirAgrupaciones'
    } ],

    init : function()
    {
        this.control(
        {
            'panelHorariosConsulta filtroGruposConsulta combobox[name=estudio]' :
            {
                select : this.onTitulacionSelected
            },
            'panelHorariosConsulta filtroGruposConsulta combobox[name=semestre]' :
            {
                select : this.cargaGrupos
            },
            'panelHorariosConsulta filtroGruposConsulta combobox[name=grupo]' :
            {
                change : this.onGrupoChange
            },
            'filtroGruposConsulta selectorCursoAgrupacionConsulta[name=cursoAgrupacion]' :
            {
                select : this.cargaGrupos
            },
            'panelHorariosConsulta button[name=calendarioDetalle]' :
            {
                click : this.setEstadoComponentes
            },
            'panelHorariosConsulta button[name=calendarioGenerica]' :
            {
                click : this.setEstadoComponentes
            }
        });
    },

    limpiaCamposComunes : function()
    {
        this.getFiltroGrupos().down('combobox[name=grupo]').clearValue();
        this.limpiaPanelesCalendario();
    },

    limpiaPanelesCalendario : function()
    {
        if (this.getPanelCalendario())
        {
            this.getPanelCalendario().limpiaCalendario();
        }

        if (this.getPanelCalendarioDetalle())
        {
            this.getPanelCalendarioDetalle().limpiaCalendario();
        }

        this.getSelectorGrupos().limpiaGrupos();
    },

    onTitulacionSelected : function(combo, records)
    {
        var estudioId = this.getFiltroTitulacion().getValue();
        this.getSelectorCursoAgrupacion().clearValue();
        this.getFiltroAsignatura().clearValue();
        this.limpiaCamposComunes();

        var store = this.getSelectorCursoAgrupacion().getStore();
        store.load(
        {
            url : '/hor/rest/estudio/' + estudioId + '/cursoagrupacion',
            params :
            {
                estudioId : estudioId
            },
            callback : function()
            {
                this.setEstadoComponentes();
            },
            scope : this
        });
    },

    cargaGrupos : function()
    {
        var semestreId = this.getFiltroSemestre().getValue();
        var cursoId = this.getSelectorCursoAgrupacion().getCursoId();
        var agrupacionId = this.getSelectorCursoAgrupacion().getAgrupacionId();
        var estudioId = this.getFiltroTitulacion().getValue();
        this.getFiltroAsignatura().clearValue();

        this.setEstadoComponentes();
        if (semestreId !== null && (agrupacionId !== null || cursoId !== null))
        {
            this.limpiaCamposComunes();

            var storeGrupos = this.getFiltroGrupo().getStore();

            storeGrupos.load(
            {
                params :
                {
                    estudioId : estudioId,
                    agrupacionId : agrupacionId,
                    cursoId : cursoId,
                    semestreId : semestreId
                },
                callback : function()
                {
                    this.setEstadoComponentes();
                },
                scope : this
            });
        }
    },

    onGrupoChange : function()
    {
        var estudio = this.getFiltroGrupos().down('combobox[name=estudio]').getValue();
        var semestre = this.getFiltroGrupos().down('combobox[name=semestre]').getValue();
        var grupos = this.getFiltroGrupos().getGruposSelected();
        var cursoId = this.getSelectorCursoAgrupacion().getCursoId();
        var agrupacionId = this.getSelectorCursoAgrupacion().getAgrupacionId();

        if (!(estudio && semestre && grupos && (cursoId || agrupacionId)))
        {
            return;
        }

        this.setEstadoComponentes();

        var storeAsignaturas = this.getFiltroAsignatura().getStore();

        storeAsignaturas.loadData([]);
        storeAsignaturas.load(
        {
            url : '/hor/rest/asignatura/estudio/' + estudio,
            params :
            {
                cursoId : cursoId,
                agrupacionId : agrupacionId,
                semestreId : semestre,
                gruposIds : grupos
            },
            callback : function()
            {
                this.setEstadoComponentes();
            },
            scope : this
        });

        if (this.getComboGrupos().getValue() == '')
        {
            this.limpiaPanelesCalendario();
        }
    },

    setEstadoComponentes : function()
    {
        var estudioId = this.getFiltroTitulacion().getValue();
        var cursoId = this.getSelectorCursoAgrupacion().getCursoId();
        var agrupacionId = this.getSelectorCursoAgrupacion().getAgrupacionId();
        var semestreId = this.getFiltroSemestre().getValue();
        var grupos = this.getFiltroGrupos().getGruposSelected();

        this.getSelectorCursoAgrupacion().setDisabled(estudioId === null);
        this.getFiltroSemestre().setDisabled(cursoId === null && agrupacionId === null);
        this.getFiltroGrupo().setDisabled(semestreId === null || (cursoId === null && agrupacionId === null));
        this.getFiltroAsignatura().setDisabled(grupos == '');

        this.getMenuImprimirTitulacion().setDisabled(this.getFiltroGrupos().down('button[name=calendarioDetalle]').pressed);

        var hayAgrupaciones = false;
        this.getStoreCursosAgrupacionesStore().data.each(function(item)
        {
            if (item.data['tipoId'] == 2)
            {
                hayAgrupaciones = true;
            }
        });
        this.getMenuImprimirAgrupaciones().setDisabled(!hayAgrupaciones || this.getFiltroGrupos().down('button[name=calendarioDetalle]').pressed);
    }
});