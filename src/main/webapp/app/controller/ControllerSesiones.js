Ext.define('HOR.controller.ControllerSesiones',
{
    extend : 'Ext.app.Controller',
    stores : [ 'StoreAsignaturasProfesor', 'StoreEventosPodProfesor' ],
    refs : [
    {
        selector : 'panelSesiones [name=calendarioPod]',
        ref : 'calendarioPod'
    },
    {
        selector : 'panelSesiones combobox[name=semestre]',
        ref : 'comboSemestre'
    },
    {
        selector : 'panelSesiones combobox[name=asignatura]',
        ref : 'comboAsignatura'
    },
    {
        selector : 'panelSesiones combobox[name=item]',
        ref : 'comboItem'
    },
    {
        selector : 'panelSesiones combobox[name=item]',
        ref : 'comboItem'
    },
    {
        selector : 'formAsignacionFechas',
        ref : 'formAsignacionFechas'
    }],

    init : function()
    {
        this.control(
        {
            'panelSesiones' :
            {
                render : this.inicializa
            },
            'panelSesiones combobox[name=semestre]' :
            {
                select : this.onSemestreSelected
            },
            'panelSesiones combobox[name=asignatura]' :
            {
                select : this.onAsignaturaSelected
            },
            'panelSesiones combobox[name=item]' :
            {
                select : this.onItemSelected
            },
            'formAsignacionFechas button[name=close]' :
            {
                click : this.cerrarFormulario
            },
            'formAsignacionFechas button[name=save]' :
            {
                click : this.guardarDatosFormulario
            },
            'formAsignacionFechas checkbox[name=detalleManual]' :
            {
                change : this.toggleDetalleManual
            }
        });
    },

    onSemestreSelected : function(combo, records)
    {
        this.cargaAsignaturas();
        this.getComboAsignatura().clearValue();
        this.getCalendarioPod().eventStore.load([], false);
    },

    onAsignaturaSelected : function(combo, records)
    {
        this.cargaCalendario();
    },

    cargaAsignaturas : function()
    {
        var calendarioPod = this.getCalendarioPod();
        calendarioPod.showLoading();
        var storeAsignaturas = this.getStoreAsignaturasProfesorStore();
        storeAsignaturas.load(
        {
            params :
            {
                semestreId : this.getComboSemestre().getValue()
            },
            callback : function(data) {
                calendarioPod.hideLoading();
                if (data.length == 0) {
                    Ext.MessageBox.show(
                    {
                        title : 'Info',
                        msg : 'No tens cap assignatura assignada en aquest semestre',
                        icon : Ext.MessageBox.INFO,
                        buttons : Ext.Msg.OK
                    });
                }

            }
        });
    },

    cargaCalendario : function()
    {
        var calendarioPod = this.getCalendarioPod();
        calendarioPod.showLoading();
        calendarioPod.eventStore.load(
        {
            params :
            {
                asignaturaId : this.getComboAsignatura().getValue(),
                semestreId : this.getComboSemestre().getValue()
            },
            callback : function() {
                calendarioPod.hideLoading();
            }
        });
    },

    inicializa : function()
    {
    },

    cerrarFormulario : function()
    {
        var me = this;

        if (this.dirty)
        {
            Ext.Msg.confirm('Dades sense guardar', 'Tens dades sense guardar en el event, estàs segur de voler tancar la edició?', function(btn, text)
            {
                if (btn == 'yes')
                {
                    me.logicaCerrarFormulario();
                }
            });
        }
        else
        {
            this.logicaCerrarFormulario();
        }
    },

    logicaCerrarFormulario : function()
    {
        var panelCalendario = Ext.ComponentQuery.query('panelCalendarioPod')[0];
        panelCalendario.hideAsignarSesionesView();
        var formulario = Ext.ComponentQuery.query('panelCalendarioPod')[0];
        formulario.fireEvent('eventasigaaulacancel');
    },

    toggleDetalleManual : function(checkbox, enable)
    {
        var form = this.getFormAsignacionFechas();
        if (enable)
        {
            form.disableFechas(false);
        }
        else
        {
            form.disableFechas(true);
            form.query('detalleEventoGrupoManual').forEach(function(group)
            {
                group.checkAllBoxes();
            });
        }
        form.updateFormState();
    },

    guardarDatosFormulario : function()
    {
        var ref = this;
        var formulario = this.getFormAsignacionFechas();
        var data = formulario.getEstructuraDatos();

        var creditos = formulario.getCreditos();

        if (data.creditos !== null && (data.creditos > creditos || data.creditos <= 0))
        {
            Ext.MessageBox.show(
            {
                title : 'Error amb els crèdits',
                msg : "El nombre de crèdits assignats al professor no pot superar el nombre total de crèdits de la assignatura i ha de ser major de zero",
                icon : Ext.MessageBox.ERROR,
                buttons : Ext.Msg.OK
            });

            return;
        }

        if (data.detalleManual && data.fechas === "")
        {
            Ext.MessageBox.show(
            {
                title : 'Error amb les dates',
                msg : 'No es pot marcar detall manual sense seleccionar cap data activa de clase',
                icon : Ext.MessageBox.ERROR,
                buttons : Ext.Msg.OK
            });

            return;
        }
        var calendarioPod = this.getCalendarioPod();
        calendarioPod.showLoading();
        Ext.Ajax.request(
        {
            url : '/hor/rest/profesor/sincronizaitemsdetalle/',
            method : 'POST',
            jsonData : JSON.stringify(data),
            success : function(response)
            {
                calendarioPod.hideLoading();
                formulario.originalFormState = JSON.stringify(formulario.getEstructuraDatos());
                formulario.updateFormState();
                ref.cargaCalendario();
            },
            failure : function(response)
            {
                calendarioPod.hideLoading();
                var serverResponse = JSON.parse(response.responseText);
                Ext.MessageBox.show(
                {
                    title : 'Error',
                    msg : serverResponse.message,
                    icon : Ext.MessageBox.ERROR,
                    buttons : Ext.Msg.OK
                });
            },
            scope : this
        });
    }

});