Ext.define('HOR.controller.ControllerPermisos',
{
    extend : 'Ext.app.Controller',
    stores : [ 'StorePermisos', 'StoreEstudios', 'StoreCargos', 'StoreDepartamentosPermisos', 'StoreAreasPermisos' ],
    model : [ 'Permiso' ],
    views : [ 'permisos.VentanaNewPermiso' ],
    ventanaPermiso : {},

    getVentanaNewPermisoView : function()
    {
        return this.getView('permisos.VentanaNewPermiso').create();
    },

    refs : [
    {
        selector : 'ventanaNewPermiso',
        ref : 'ventanaNewPermiso'
    },
    {
        selector : 'ventanaNewPermiso combo[name=comboPersona]',
        ref : 'comboPersona'
    },
    {
        selector : 'ventanaNewPermiso form[name=formNewPermiso]',
        ref : 'formNewPermiso'
    },
    {
        selector : 'panelPermisos grid',
        ref : 'gridPermisos'
    },
    {
        selector : 'ventanaNewPermiso combo[name=comboTitulacion]',
        ref : 'comboTitulacion'
    },
    {
        selector : 'ventanaNewPermiso combo[name=comboCargo]',
        ref : 'comboCargo'
    },
    {
        selector : 'ventanaNewPermiso combo[name=comboDepartamento]',
        ref : 'comboDepartamento'
    },
    {
        selector : 'ventanaNewPermiso combo[name=comboArea]',
        ref : 'comboArea'
    },
    {
        selector : 'ventanaNewPermiso button[name=botonTodasAreas]',
        ref : 'comboArea'
    },
    {
        selector : 'ventanaNewPermiso panel[name=contenedorArea]',
        ref : 'contenedorArea'
    } ],

    init : function()
    {
        var ref = this;
        this.control(
        {
            'panelPermisos button[action=add-permiso]' :
            {
                click : function(button)
                {
                    this.showVentanaAddPermiso();
                }
            },

            'panelPermisos button[action=borrar-permiso]' :
            {
                click : function(button)
                {
                    this.borraPermisos();
                }
            },

            'ventanaNewPermiso button[action=cancelar]' :
            {
                click : function(button)
                {
                    this.closeVentanaAddPermiso();
                }
            },

            'ventanaNewPermiso button[action=guardar-permiso]' :
            {
                click : function(button)
                {
                    this.guardarPermiso();
                }
            },

            'ventanaNewPermiso combo[name=comboCargo]' :
            {
                select : this.onCargoChange
            },

            'ventanaNewPermiso combo[name=comboDepartamento]' :
            {
                select : this.onDepartamentoChange
            },

            'ventanaNewPermiso button[name=botonTodasAreas]' :
            {
                click : function(button)
                {
                    this.getComboArea().clearValue();
                }
            }
        });
    },

    showVentanaAddPermiso : function()
    {
        this.ventanaPermiso = this.getVentanaNewPermisoView();
        this.ventanaPermiso.show();
    },

    closeVentanaAddPermiso : function()
    {
        this.ventanaPermiso.destroy();
    },

    onCargoChange : function(combo, rec)
    {
        var mostrarTitulacion = rec[0].get("estudio");
        var mostrarDepartamento = rec[0].get("departamento");

        this.getComboTitulacion().setVisible(mostrarTitulacion);
        this.getComboTitulacion().allowBlank = !mostrarTitulacion;

        this.getComboDepartamento().setVisible(mostrarDepartamento);
        this.getComboDepartamento().allowBlank = !mostrarDepartamento;
        this.getContenedorArea().setVisible(mostrarDepartamento);

        if (!mostrarTitulacion)
        {
            this.getComboTitulacion().setValue(null);
        }

        if (!mostrarDepartamento)
        {
            this.getComboDepartamento().setValue(null);
            this.getComboArea().setValue(null);
        }
    },

    onDepartamentoChange : function(combo, records)
    {
        var storeAreas = this.getStoreAreasPermisosStore();
        storeAreas.removeAll();
        this.getComboArea().clearValue();

        storeAreas.load(
        {
            params :
            {
                departamentoId : records[0].get('id')
            }
        });
    },

    guardarPermiso : function()
    {
        var ref = this;
        var formNewPermiso = this.getFormNewPermiso().getForm();
        var comboPersona = this.getComboPersona();
        var comboTitulacion = this.getComboTitulacion();
        var comboCargo = this.getComboCargo();
        var comboDepartamento = this.getComboDepartamento();
        var comboArea = this.getComboArea();

        var personaId = comboPersona.getValue();
        var estudioId = comboTitulacion.getValue();
        var tipoCargoId = comboCargo.getValue();
        var departamentoId = comboDepartamento.getValue();
        var areaId = comboArea.getValue();

        if (formNewPermiso.isValid())
        {
            var permiso = Ext.ModelManager.create(
            {
                personaId : personaId,
                persona : comboPersona.getRawValue(),
                estudioId : estudioId,
                estudio : comboTitulacion.getRawValue(),
                tipoCargoId : tipoCargoId,
                tipoCargo : comboCargo.getRawValue(),
                departamentoId : departamentoId,
                departamento : comboDepartamento.getRawValue(),
                areaId : areaId,
                area : comboArea.getRawValue()
            }, "HOR.model.Permiso");

            var gridPermisos = this.getGridPermisos();
            gridPermisos.getStore().add(permiso);
            gridPermisos.getStore().sync(
            {
                success : function()
                {
                    ref.closeVentanaAddPermiso();
                }
            });
        }
    },

    borraPermisos : function()
    {
        var gridPermisos = this.getGridPermisos();
        var registros = gridPermisos.getSelectionModel().getSelection();

        if (registros.length > 0)
        {
            Ext.Msg.confirm('Eliminació de permisos', '<b>Esteu segur/a de voler esborrar els permisos seleccionats?</b>', function(btn, text)
            {
                if (btn == 'yes')
                {
                    gridPermisos.getStore().remove(registros);
                    gridPermisos.getStore().sync();
                }
            });
        }
    }

});