Ext.define('Event.form.field.DetalleEventoGrupoManual',
{
    extend : 'Ext.form.Panel',
    alias : 'widget.detalleEventoGrupoManual',
    border : 0,
    hidden : false,
    align : 'vbox',
    disabled : true,
    evento: undefined,
    itemProfesorId : undefined,
    bubbleEvents: [ 'hoursChanged' ],
    items : [
    {
        xtype : 'text',
        cls : "tituloGrupo",
        name : 'eventoTitle'
    },
    {
        xtype : 'fieldset',
        columns : 6,
        items : [
        {
            xtype : 'checkboxgroup',
            title : 'Hola - Adios',
            name : 'grupoDetalleEventoGrupoManualFechas',
            columns : 6,
            items : []
        },
        {
            xtype : 'panel',
            layout :
            {
                type : 'hbox',
                pack : 'end'
            },
            border : 0,
            padding : 5,
            items : [
            {
                xtype : 'button',
                margin : 2,
                text : 'Tots',
                listeners :
                {
                    click : function(boton)
                    {
                        boton.up('detalleEventoGrupoManual').checkAllBoxes();
                    },
                    scope : this
                }
            },
            {
                xtype : 'button',
                margin : 2,
                text : 'Cap',
                listeners :
                {
                    click : function(boton)
                    {
                        boton.up('detalleEventoGrupoManual').uncheckAllBoxes();
                    },
                    scope : this
                }
            } ]
        } ]
    } ],

    initComponent : function()
    {
        this.callParent(arguments);
    },

    setDescripcionGrupoByEvento : function(evento)
    {
        this.evento = evento;
        var fieldset = this.down('text[name="eventoTitle"]');
        var horaInicio = evento.start.substr(11, 5);
        var horaFin = evento.end.substr(11, 5);
        fieldset.setText(evento.titulo + ' - ' + evento.dia_semana + ' ' + horaInicio + ' a ' + horaFin);
        this.itemProfesorId = evento.item_profesor_id;
    },

    calculaDuracion: function(evento) {
        var start = new Date(evento.start);
        var end = new Date(evento.end);

        return (end - start) / 3600000;
    },

    setCalculaDatosSesionByClases: function(clases) {
        var fieldset = this.down('text[name="eventoTitle"]');
        var horaInicio = this.evento.start.substr(11, 5);
        var horaFin = this.evento.end.substr(11, 5);
        var aula = this.evento.aula_codigo;

        var numHoras = 0;
        var numSesiones = 0;
        var duracion = this.calculaDuracion(this.evento);

        for (var i in this.getFechas()) {
            numSesiones++;
            numHoras += duracion;
        }

        this.fireEvent('hoursChanged', numSesiones, numHoras, this.evento);

        fieldset.setText(this.evento.titulo + ' - ' + this.evento.dia_semana + ' ' + horaInicio + ' a ' + horaFin + ', semestre ' + this.evento.semestre + ' ' + aula + ' (' + numSesiones + ' sessions, ' + numHoras + ' hores assignades)');
    },

    getFechas : function()
    {
        var checkboxgroup = this.down('checkboxgroup');
        var fechas = [];
        var checks = checkboxgroup.getChecked();
        for ( var i in checks)
        {
            var check = checks[i];
            fechas.push(this.itemProfesorId + ":" + check.detalleId);
        }
        return fechas;
    },

    addPosiblesFechas : function(clases, parent)
    {
        var ref = this;
        var checkboxgroup = this.down('checkboxgroup');

        if (checkboxgroup) {
            checkboxgroup.removeAll();
        }

        for ( var i = 0; i < clases.length; i++)
        {
            var fecha = Ext.Date.parse(clases[i].fecha, "d\/m\/Y H:i:s");
            fecha = Ext.Date.format(fecha, "d/m/Y");

            var checked = (clases[i].seleccionado === 'S');
            var cls = 'form-label-date-manual';
            var disabled = false;

            if (clases[i].tipoDia == 'E')
            {
                fecha = "[*] " + fecha;
                cls = 'form-label-exam-date-manual';
            }
            else if (clases[i].tipoDia == 'F')
            {
                disabled = true;
            }

            if (clases[i].seleccionado === 'X') {
                disabled = true;
                fecha = "[*] " + fecha;
                cls = 'form-label-bloqueado-date-manual';
            }

            var checkbox = Ext.create('Ext.form.field.Checkbox',
            {
                inputValue : clases[i].fecha,
                flex : 1,
                detalleId : clases[i].detalleId,
                name : this.nameCheckbox,
                boxLabel : fecha,
                checked : checked,
                cls : cls,
                disabled : disabled,
                listeners :
                {
                    change : function()
                    {
                        ref.setCalculaDatosSesionByClases(clases);
                        parent.updateFormState();
                    }
                }
            });
            checkboxgroup.add(checkbox);
        }
        this.setCalculaDatosSesionByClases(clases);
    },

    checkAllBoxes : function()
    {
        var checkboxgroup = this.down('checkboxgroup');
        Ext.Array.each(checkboxgroup.getBoxes(), function(checkbox)
        {
            if (!checkbox.disabled)
            {
                checkbox.setValue(true);
            }
        });
    },

    uncheckAllBoxes : function()
    {
        var checkboxgroup = this.down('checkboxgroup');
        Ext.Array.each(checkboxgroup.getBoxes(), function(checkbox)
        {
            checkbox.setValue(false);
        });
    }

});