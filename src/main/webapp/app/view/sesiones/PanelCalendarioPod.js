Ext.define('HOR.view.sesiones.PanelCalendarioPod',
{
    extend : 'Extensible.calendar.CalendarPanel',
    alias : 'widget.panelCalendarioPod',
    name: 'calendarioPod',
    region : 'center',
    title : 'Calendari POD',
    depends : [ 'HOR.store.StoreEventosPlanificadosAsignatura', 'HOR.store.StoreCalendariosAsignaturas', 'HOR.store.StoreEventosPodProfesor' ],
    requires : [ 'HOR.view.sesiones.FormAsignacionFechas' ],
    calendarStore : Ext.create('HOR.store.StoreCalendariosAsignaturas'),
    eventStore : Ext.create('HOR.store.StoreEventosPodProfesor'),
    editModal : false,
    enableEditDetails : false,
    flex : 1,
    padding : 5,
    showTodayText : false,
    showNavToday : false,
    showDayView : false,
    showWeekView : false,
    showMonthView : false,
    viewConfig :
    {
        viewStartHour : 8,
        viewEndHour : 22
    },
    weekViewCfg :
    {
        dayCount : 5,
        startDay : 1,
        startDayIsStatic : true
    },
    showMultiDayView : true,
    showMultiWeekView : false,
    showNavJump : false,
    showNavNextPrev : false,
    multiDayViewCfg :
    {
        dayCount : 5,
        startDay : 1,
        startDayIsStatic : true,
        showTime : false,
        showMonth : false
    },

    asignarProfesorView : {},

    loadingMask : undefined,

    getMultiDayText : function()
    {
        return '';
    },

    initComponent : function()
    {
        var self = this;
        this.loadingMask = new Ext.LoadMask(Ext.getBody(),
        {
            msg : "Carregant..."
        });

        Extensible.calendar.template.BoxLayout.override(
        {
            firstWeekDateFormat : 'l',
            multiDayFirstDayFormat : 'l',
            multiDayMonthStartFormat : 'l'
        });

        this.callParent(arguments);

        this.creaFormularioDeAsignacionProfesor();

        Extensible.calendar.menu.Event.override(
        {
            buildMenu : function()
            {
                var me = this;

                if (me.rendered)
                {
                    return;
                }
                Ext.apply(me,
                {
                    items : [                    {
                        text : 'Definir sessions',
                        iconCls : 'extensible-cal-icon-evt-edit',
                        scope : me,
                        handler : function()
                        {
                            self.definirSesiones(me, me.rec)
                        }
                    } ]
                });
            },

            showForEvent : function(rec, el, xy)
            {
                if (rec.data.CalendarId === '4')
                {
                    return false;
                }
                var me = this;
                me.rec = rec;
                me.ctxEl = el;
                me.showAt(xy);
            }
        });
    },

    definirSesiones : function(calendar, eventoPrincipal)
    {
        var profesor =
        {
            id : eventoPrincipal.raw.profesorId,
            nombre : eventoPrincipal.raw.profesorNombre
        };

        var ref = this;
        ref.showLoading();
        Ext.Ajax.request(
        {
            url : '/hor/rest/calendario/eventos/mismogrupo/' + eventoPrincipal.data.EventId,
            params :
            {
                profesorId : profesor.id
            },
            method : 'GET',
            success : function(response)
            {
                var eventos = Ext.JSON.decode(response.responseText).data;
                ref.showAsignacionSesionesProfesor(profesor, eventoPrincipal, eventos);
                ref.hideLoading();
            },
            failure : function() {
                ref.hideLoading();
            }
        });
    },

    showAsignacionSesionesProfesor : function(profesor, eventoPrincipal, eventos)
    {
        var asignarClaseId = this.id + '-profesor';
        this.asignarProfesorView = this.layout.getActiveItem().id;
        this.setActiveViewForAsignarSesiones(asignarClaseId, profesor, eventoPrincipal);

        var formularioAsignacionClases = this.layout.getActiveItem();
        formularioAsignacionClases.setDatos(profesor, eventoPrincipal, eventos, this);
        return this;
    },

    setActiveViewForAsignarSesiones : function(id, profesor, evento)
    {
        var me = this, layout = this.layout, asignarClaseViewId = me.id + '-profesor', toolbar;

        if (id !== layout.getActiveItem().id)
        {
            toolbar = me.getDockedItems('toolbar')[0];
            if (toolbar)
            {
                toolbar[id === asignarClaseViewId ? 'hide' : 'show']();
            }

            layout.setActiveItem(id || me.activeItem);
            me.doComponentLayout();
            me.activeView = layout.getActiveItem();

            if (id !== asignarClaseViewId)
            {
                if (id && id !== me.asignarProfesorView)
                {
                    layout.activeItem.setStartDate(me.startDate, true);
                }
                me.updateNavState();
            }
            me.fireViewChange();
        }
    },

    creaFormularioDeAsignacionProfesor : function()
    {
        this.add([
        {
            xtype : 'formAsignacionFechas',
            id : this.id + '-profesor'
        } ]);
    },

    showLoading : function()
    {
        this.loadingMask.show();
    },

    hideLoading : function()
    {
        this.loadingMask.hide();
    },

    showAsignacionClasesProfesorDetails : function(profesor, eventoPrincipal, eventos)
    {
        var asignarClaseId = this.id + '-profesor';
        this.asignarProfesorView = this.layout.getActiveItem().id;
        this.setActiveViewForAsignarProfesor(asignarClaseId, profesor, eventoPrincipal);

        var formularioAsignacionClases = this.layout.getActiveItem();
        formularioAsignacionClases.setDatos(profesor, eventoPrincipal, eventos, this);
        return this;
    },

    setActiveViewForAsignarProfesor : function(id, profesor, evento)
    {
        var me = this, layout = this.layout, asignarClaseViewId = me.id + '-profesor', toolbar;

        if (id !== layout.getActiveItem().id)
        {
            toolbar = me.getDockedItems('toolbar')[0];
            if (toolbar)
            {
                toolbar[id === asignarClaseViewId ? 'hide' : 'show']();
            }

            layout.setActiveItem(id || me.activeItem);
            me.doComponentLayout();
            me.activeView = layout.getActiveItem();

            if (id !== asignarClaseViewId)
            {
                if (id && id !== me.asignarProfesorView)
                {
                    layout.activeItem.setStartDate(me.startDate, true);
                }
                me.updateNavState();
            }
            me.fireViewChange();
        }
    },

    hideAsignarSesionesView : function()
    {
        if (this.asignarProfesorView)
        {
            this.setActiveViewForAsignarProfesor(this.asignarProfesorView);
            delete this.preEditView;
        }
        return this;
    },

    onStoreUpdate : function()
    {
    },

    getEventStore : function()
    {
        return this.store;
    },

    listeners :
    {
        dayclick : function(dt, allday, el)
        {
            return false;
        },

        rangeselect : function()
        {
            return false;
        },

        eventclick : function(calendar, record, cmp)
        {
            Ext.ComponentQuery.query("panelCalendarioPod")[0].definirSesiones(calendar, record);
            return false;
        },

        eventover : function(calendar, record, event)
        {
            Ext.ComponentQuery.query("panelCalendarioPod")[0].mostrarInfoAdicionalEvento(event, record);
        },

        eventout : function(calendar, record, event)
        {
            var panelCalendario = Ext.ComponentQuery.query('panelCalendarioPod')[0];
            if (panelCalendario.tooltip)
            {
                panelCalendario.tooltip.destroy();
                delete panelCalendario.tooltip;
            }
        }
    },

    mostrarInfoAdicionalEvento : function(event, record)
    {
        var panelCalendario = Ext.ComponentQuery.query('panelCalendarioPod')[0];
        if (event.disabled)
        {
            this.tooltip.hide();
            return;
        }

        if (!this.tooltip)
        {
            this.tooltip = Ext.create('Ext.tip.ToolTip',
            {
                hidden : true,
                target : panelCalendario.getEl(),
                trackMouse : true,
                autoHide : false
            });
        }
        if (record.data.Title)
        {
            this.tooltip.setTarget(panelCalendario.getEl());

            var fechasDetalles = record.data.FechasDetalles;
            var tooltipHtml = record.data.Title + '<br />';

            if (fechasDetalles)
            {
                tooltipHtml += '<br/>Dates: ' + fechasDetalles;
            }
            if (record.data.Comentarios)
            {
                tooltipHtml += '<br/>Comentari: ' + record.data.Comentarios;
            }

            this.tooltip.update(tooltipHtml);
            this.tooltip.show();
        }
        else
        {
            if (this.tooltip)
            {
                this.tooltip.destroy();
                delete this.tooltip;
            }
        }
    }
});