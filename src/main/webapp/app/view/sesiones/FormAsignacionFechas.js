Ext.define('HOR.view.sesiones.FormAsignacionFechas',
{
    extend : 'Ext.form.Panel',
    alias : 'widget.formAsignacionFechas',
    title : "Assignació de sessions",
    labelWidthRightCol : 65,
    colWidthLeft : .6,
    colWidthRight : .4,
    bodyStyle : 'padding:20px 20px 10px;',
    border : false,
    buttonAlign : 'center',
    autoHeight : true,
    cls : 'ext-evt-edit-form',
    autoScroll : true,
    maxCreditos : 0,
    creditosProfesorCalculados : 0,
    profesorId : undefined,
    itemId : undefined,
    dirty : false,
    originalFormState : undefined,
    delayed : undefined,

    items : [
    {
        type : 'panel',
        layout : 'hbox',
        border : 0,
        items : [
        {
            xtype : 'text',
            flex : 1,
            name : 'professor',
            labelWidth : 400,
            cls : 'caption'
        },
        {
            xtype : 'text',
            flex : 1,
            name : 'sesiones',
            width : 400,
            cls : 'infobox',
            text : 'Resum d\'assignació:'
        } ]
    },
    {
        type : 'panel',
        layout : 'hbox',
        border : 0,
        items : [
        {
            xtype : 'text',
            flex : 1,
            name : 'evento',
            width : 400,
            cls : 'caption'
        },
        {
            xtype : 'text',
            flex : 1,
            name : 'avisos',
            width : 400,
            //cls : 'errorbox',
            //fieldStyle: 'color: #f00; background-color: #ddd; background-image: none;'
        } ]
    },
    {
        xtype : 'hiddenfield',
        name : 'eventId'
    },
    {
        xtype : 'numberfield',
        allowDecimals : true,
        decimalSeparator : ",",
        fieldLabel : 'Credits professor (si no imparteix tot el sub grup)',
        name : 'creditos',
        padding : 0,
        labelWidth : 280,
        readOnly : true
    },
    {
        xtype : 'numberfield',
        allowDecimals : true,
        decimalSeparator : ",",
        fieldLabel : 'Crèdits computables',
        name : 'creditosComputables',
        padding : 0,
        labelWidth : 280,
        readOnly : true
    },
    {
        xtype : 'checkbox',
        boxLabel : 'Detall manual',
        checked : false,
        padding : 0,
        name : 'detalleManual',
        inputValue : '0'
    },
    {
        xtype : 'panel',
        name : 'contenedorDetalles',
        border : 0,
        items : []
    },
    {
        xtype : 'displayfield',
        value : '[*] Ja assignada a un altre professor',
        fieldCls : 'form-legend-bloqueada'
    } ],

    resumenEventos : {},

    listeners :
    {
        'hoursChanged' : function(numSesiones, numHoras, evento)
        {
            this.resumenEventos[evento.id] =
            {
                sesiones : numSesiones,
                horas : numHoras
            };

            var totalSesiones = 0;
            var totalHorasSeleccionadas = 0;
            var totalHorasProfesor = parseFloat(this.creditosProfesorCalculados) * 10;

            for ( var i in this.resumenEventos)
            {
                var e = this.resumenEventos[i];

                totalSesiones += e.sesiones;
                totalHorasSeleccionadas += e.horas;
            }

            var sesiones = this.down("[name='sesiones']");
            sesiones.setText('Resum d\'assignació: ' + totalSesiones + ' sessions, ' + totalHorasSeleccionadas + ' hores, ' + (totalHorasSeleccionadas/10) + ' crèdits.');

            var avisos = this.down("[name='avisos']");
            if (totalHorasSeleccionadas > totalHorasProfesor) {
                avisos.setText('Avís, excés de ' + (Math.round((totalHorasSeleccionadas - totalHorasProfesor)*100) / 100) + ' hores');
            }
            else if (totalHorasSeleccionadas < totalHorasProfesor) {
                avisos.setText('Avís, falten ' + (Math.round((totalHorasProfesor - totalHorasSeleccionadas)*100) / 100) + ' hores');
            }
            else {
                avisos.setText('Nombre d\'hores correcte');
            }

            if ((totalHorasSeleccionadas >= (totalHorasProfesor * 1.15)) || (totalHorasSeleccionadas <= (totalHorasProfesor / 1.15))) {
                avisos.getEl().applyStyles({
                    backgroundColor: '#fcc',
                    padding: '5px',
                    'font-weight' : 'bold'
                });
            } else {
                avisos.getEl().applyStyles({
                    backgroundColor: '#cfc',
                    padding: '5px',
                    'font-weight' : 'bold'
                });
            }
        }
    },

    buttons : [
    {
        text : 'Guardar',
        name : 'save',
        disabled : true
    },
    {
        text : 'Tancar',
        name : 'close'
    } ],

    setStartDate : function(dt)
    {
        this.startDate = dt;
        return this;
    },

    setDatos : function(profesor, eventoPrincipal, eventos, parentPanel)
    {
        this.originalFormState = undefined;
        this.resumenEventos = {};

        this.setNombreProfesor(profesor.nombre);
        this.setNombreEvento(eventoPrincipal);
        this.maxCreditos = eventoPrincipal.data.Creditos;
        this.creditosProfesorCalculados = parseFloat(eventoPrincipal.raw.creditos_profesor_calculados);
        this.profesorId = profesor.id;
        this.itemId = eventoPrincipal.data.EventId;

        this.down("numberfield[name=creditos]").setValue(eventos[0].creditos_profesor);
        this.down("numberfield[name=creditosComputables]").setValue(eventos[0].creditos_computables_profesor);
        var detalleManual = (eventos[0].detalle_manual_profesor === "true");

        this.down("checkbox[name=detalleManual]").setValue(detalleManual);

        var creditos = eventos[0].creditos;

        var contenedor = Ext.ComponentQuery.query("[name=contenedorDetalles]")[0];
        contenedor.removeAll();
        for ( var i in eventos)
        {
            var evento = eventos[i];

            this.addDetallesEvento(profesor, evento, detalleManual, contenedor);
        }
    },

    getCreditos : function()
    {
        return this.maxCreditos;
    },

    setNombreProfesor : function(nombre)
    {
        this.down("[name='professor']").setText(nombre);
    },

    setNombreEvento : function(evento)
    {
        var tituloEvento = evento.data.Title + ', ' +  evento.data.Creditos + ' credits';
        if (evento.data.TipoAsignatura == 'A')
        {
            tituloEvento += " (*) Aquesta assignatura és anual, els crèdits mostrats corresponen a l'any complet";
        }
        this.down("[name='evento']").setText(tituloEvento);
    },

    getStartDate : function()
    {
        return this.startDate || Extensible.Date.today();
    },

    disableFechas : function(disabled)
    {
        var listaDetalleEventoGrupoManual = this.query("detalleEventoGrupoManual");
        for ( var i in listaDetalleEventoGrupoManual)
        {
            var detalleEventoGrupoManual = listaDetalleEventoGrupoManual[i];
            detalleEventoGrupoManual.setDisabled(disabled);
        }
    },

    getEstructuraDatos : function()
    {
        var detalleManual = this.down("checkbox[name=detalleManual]").getValue();
        var itemId = this.itemId;
        var creditos = this.down("textfield[name=creditos]").getValue();
        var creditosComputables = this.down("textfield[name=creditosComputables]").getValue();
        var profesorId = this.profesorId;
        var fechas = [];

        var listaDetalleEventoGrupoManual = this.query("detalleEventoGrupoManual");
        for ( var i in listaDetalleEventoGrupoManual)
        {
            var detalleEventoGrupoManual = listaDetalleEventoGrupoManual[i];
            // Añadimos los elementos del nuevo array en nuestro array de fechas
            fechas.push.apply(fechas, detalleEventoGrupoManual.getFechas());
        }
        var cadenaFechas = fechas.join(",");
        return {
            profesorId : profesorId,
            itemId : itemId,
            creditos : creditos,
            creditosComputables : creditosComputables,
            detalleManual : detalleManual,
            fechas : cadenaFechas
        };
    },

    addDetallesEvento : function(profesor, evento, detalleManual, contenedor)
    {
        var self = this;
        var detalleEventoGrupoManual = Ext.create("Event.form.field.DetalleEventoGrupoManual");
        detalleEventoGrupoManual.setDisabled(!detalleManual);
        detalleEventoGrupoManual.setDescripcionGrupoByEvento(evento);
        contenedor.add(detalleEventoGrupoManual);

        Ext.Ajax.request(
        {
            url : '/hor/rest/calendario/eventos/fechas/profesor/' + profesor.id,
            params :
            {
                eventoId : evento.id
            },
            method : 'GET',
            success : function(response)
            {
                var fechas = Ext.JSON.decode(response.responseText).data;
                detalleEventoGrupoManual.addPosiblesFechas(fechas, self);
                self.originalFormState = JSON.stringify(self.getEstructuraDatos());
            }
        });
    },

    updateFormState : function()
    {
        var data = JSON.stringify(this.getEstructuraDatos());

        if (this.delayed)
        {
            clearTimeout(this.delayed);
        }

        var self = this;
        this.delayed = setTimeout(function()
        {
            if (data && self.originalFormState && data !== self.originalFormState)
            {
                self.dirty = true;
                self.down('button[name=close]').setText('Tancar sense guardar');
                self.down('button[name=save]').setDisabled(false);
            }
            else
            {
                self.dirty = false;
                self.down('button[name=close]').setText('Tancar');
                self.down('button[name=save]').setDisabled(true);
            }
        }, 30);
    }

});