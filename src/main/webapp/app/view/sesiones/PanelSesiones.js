Ext.define('HOR.view.sesiones.PanelSesiones',
{
    extend : 'Ext.panel.Panel',
    title : 'Assignació de Sessions',
    alias : 'widget.panelSesiones',
    requires : [ 'HOR.view.commons.GroupedComboBox', 'HOR.view.sesiones.FiltrosSesiones', 'HOR.view.sesiones.DetalleFechas', 'HOR.view.sesiones.PanelCalendarioPod' ],
    closable : true,
    autoHeight : true,
    border : 0,
    layout :
    {
        type : 'vbox',
        align : 'stretch',
        padding : 5
    },

    items : [ {
        xtype : 'filtrosSesiones'
    },
    {
        xtype : 'panelCalendarioPod'
    }]
});