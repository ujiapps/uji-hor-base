Ext.define('HOR.view.sesiones.FiltrosSesiones',
{
    extend : 'Ext.panel.Panel',
    alias : 'widget.filtrosSesiones',

    border : false,
    padding : 5,
    closable : false,
    layout : 'anchor',
    items : [
    {
        xtype : 'combobox',
        lastQuery : '',
        fieldLabel : 'Semestre:',
        labelWidth : 90,
        labelAlign : 'right',
        store : Ext.create('Ext.data.ArrayStore',
        {
            fields : [ 'index', 'name' ],
            data : [ [ '1', '1' ], [ '2', '2' ] ]
        }),
        editable : false,
        displayField : 'name',
        valueField : 'index',
        name : 'semestre',
        width : 500,
        disabled : false,
        alias : 'widget.comboSemestre'
    },
    {
        border : 0,
        xtype : 'combobox',
        editable : false,
        labelWidth : 90,
        labelAlign : 'right',
        margin : '0 0 5 0',
        fieldLabel : 'Assignatura',
        queryMode : 'local',
        store : 'StoreAsignaturasProfesor',
        displayField : 'nombreConCodigo',
        valueField : 'asignaturaId',
        name : 'asignatura',
        disabled : false,
        width : 500
    }
    ]
});