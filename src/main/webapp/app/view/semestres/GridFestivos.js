Ext.define('HOR.view.semestres.GridFestivos',
{
    extend : 'Ext.grid.Panel',
    title : 'Dies no lectius',
    requires : [],
    store : 'StoreFestivos',
    alias : 'widget.gridFestivos',
    disableSelection : true,

    columns : [
    {
        text : 'Dia',
        dataIndex : 'diaSemana',
        menuDisabled : true
    },
    {
        text : 'Data',
        dataIndex : 'fecha',
        menuDisabled : true,
        renderer : function(value)
        {
            return Ext.Date.format(value, "d/m/Y");
        }
    },
    {
        text : 'Tipus',
        dataIndex : 'tipoDia',
        menuDisabled : true,
        renderer : function(value)
        {
            if (value === 'F')
            {
                return 'Festiu';
            }

            if (value === 'E')
            {
                return 'Examen';
            }
        }
    } ]
});
