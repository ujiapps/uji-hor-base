Ext.define('HOR.view.semestres.PanelSemestres',
{
    extend : 'Ext.panel.Panel',
    title : 'Dates curs acadèmic',
    requires : [ 'HOR.view.semestres.GridSemestres', 'HOR.view.semestres.GridFestivos', 'HOR.view.semestres.GridSemestreEstudios' ],
    alias : 'widget.panelSemestres',
    closable : true,
    layout :
    {
        type : 'vbox',
        align : 'stretch'
    },
    items : [
    {
        xtype : 'gridSemestres',
        height : 160
    },
    {
        xtype : 'gridFestivos',
        flex : 1
    },
    {
        xtype : 'gridFechasSemestreEstudios',
        flex : 1
    } ]
});