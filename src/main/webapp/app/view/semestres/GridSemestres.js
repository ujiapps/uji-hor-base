Ext.define('HOR.view.semestres.GridSemestres',
{
    extend : 'Ext.grid.Panel',
    title: 'Dates generals dels semestres',
    requires : [],
    store : 'StoreSemestreDetalles',
    alias : 'widget.gridSemestres',
    sortableColumns : false,
    columns : [
    {
        text : 'Tipus estudi',
        dataIndex : 'nombreTipoEstudio',
        menuDisabled : true
    },
    {
        text : 'Nom  Semestre',
        dataIndex : 'nombreSemestre',
        menuDisabled : true
    },
    {
        text : 'Inici docència',
        dataIndex : 'fechaInicio',
        menuDisabled : true,
        renderer : function(value)
        {
            return Ext.Date.format(value, "d/m/Y");
        }
    },
    {
        text : 'Fi docència',
        dataIndex : 'fechaFin',
        menuDisabled : true,
        renderer : function(value)
        {
            return Ext.Date.format(value, "d/m/Y");
        }
    },
    {
        text : 'Inici exàmens (1ª convocatòria)',
        width: 180,
        dataIndex : 'fechaExamenesInicio',
        menuDisabled : true,
        renderer : function(value)
        {
            return Ext.Date.format(value, "d/m/Y");
        }
    },
    {
        text : 'Fi exàmens (1ª convocatòria)',
        width: 180,
        dataIndex : 'fechaExamenesFin',
        menuDisabled : true,
        renderer : function(value)
        {
            return Ext.Date.format(value, "d/m/Y");
        }
    },
    {
        text : 'Inici exàmens (2ª convocatòria)',
        width: 180,
        dataIndex : 'fechaExamenesInicioC2',
        menuDisabled : true,
        renderer : function(value)
        {
            return Ext.Date.format(value, "d/m/Y");
        }
    },
    {
        text : 'Fi exàmens (2ª convocatòria)',
        width: 180,
        dataIndex : 'fechaExamenesFinC2',
        menuDisabled : true,
        renderer : function(value)
        {
            return Ext.Date.format(value, "d/m/Y");
        }
    } ]
});
