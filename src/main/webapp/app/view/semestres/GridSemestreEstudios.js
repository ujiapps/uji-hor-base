Ext.define('HOR.view.semestres.GridSemestreEstudios',
{
    extend : 'Ext.grid.Panel',
    title: 'Dates per estudi',
    store : 'StoreSemestreDetalleEstudios',
    alias : 'widget.gridFechasSemestreEstudios',
    sortableColumns : false,
    columns : [
    {
        text : 'Estudi',
        dataIndex : 'estudioId',
        flex : 4,
        renderer : function(value, metadata, record)
        {
            return record.get('_estudio').nombre;
        }
    },
    {
        text : 'Semestre',
        dataIndex : 'semestreDetalleId',
        flex : 3,
        renderer : function(value, metadata, record)
        {
            var detalleSemestre = record.get('_semestreDetalle');
            var tipoEstudioMap =
            {
                'G' : 'Graus',
                'M' : 'Masters'
            };
            var semestreMap =
            {
                '1' : 'Primer semestre',
                '2' : 'Segon semestre'
            };
            return tipoEstudioMap[detalleSemestre.tipoEstudio] + ' - ' + semestreMap[detalleSemestre.semestre];
        }
    },
    {
        text : 'Curs',
        dataIndex : 'cursoAcademico',
        flex : 1
    },
    {
        text : 'Inici docència',
        dataIndex : 'fechaInicio',
        flex : 2,
        renderer : function(value)
        {
            return Ext.Date.format(value, "d/m/Y");
        }
    },
    {
        text : 'Fi docència',
        dataIndex : 'fechaFin',
        flex : 2,
        renderer : function(value)
        {
            return Ext.Date.format(value, "d/m/Y");
        }
    },
    {
        text : 'Inici exàmens (1ª conv.)',
        dataIndex : 'fechaExamenesInicio',
        flex : 2,
        renderer : function(value)
        {
            return Ext.Date.format(value, "d/m/Y");
        }
    },
    {
        text : 'Fi exàmens (1ª conv.)',
        dataIndex : 'fechaExamenesFin',
        flex : 2,
        renderer : function(value)
        {
            return Ext.Date.format(value, "d/m/Y");
        }
    },
    {
        text : 'Inici exàmens (2ª conv.)',
        dataIndex : 'fechaExamenesInicioC2',
        flex : 2,
        renderer : function(value)
        {
            return Ext.Date.format(value, "d/m/Y");
        }
    },
    {
        text : 'Fi exàmens (2ª conv.)',
        dataIndex : 'fechaExamenesFinC2',
        flex : 2,
        renderer : function(value)
        {
            return Ext.Date.format(value, "d/m/Y");
        }
    } ]
});
