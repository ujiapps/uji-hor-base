Ext.define('HOR.view.pod.PanelEdicion',
{
    extend : 'Ext.panel.Panel',
    title : 'Assignació de POD',
    requires : [ 'HOR.view.pod.FiltroProfesores', 'HOR.view.pod.SelectorProfesores', 'HOR.view.pod.PanelCalendarioAsignatura', 'HOR.view.pod.PanelCalendarioAsignaturaDetalle',
            'HOR.view.pod.VentanaAsignacionIdioma', 'HOR.view.pod.SelectorClasesSinPlanificar', 'HOR.view.pod.VentanaCreditos', 'HOR.view.pod.VentanaAsignacionIdioma' ],
    alias : 'widget.panelEdicionPOD',
    closable : true,
    autoHeight : true,
    border : 0,
    layout :
    {
        type : 'vbox',
        align : 'stretch',
        padding : 5
    },

    items : [
    {
        xtype : 'ventanaCreditos'
    },
    {
        xtype : 'filtroProfesores',
        height : 115
    },
    {
        xtype : 'panel',
        border : 0,
        flex : 1,
        layout :
        {
            type : 'hbox',
            align : 'stretch'
        },

        items : [
        {
            xtype : 'ventanaAsignacionIdioma'
        },
        {
            xtype : 'panel',
            width : 200,
            border : 0,
            flex : 0,
            layout :
            {
                type : 'vbox',
                align : 'stretch'
            },

            items : [
            {
                width : 200,
                flex : 1,
                xtype : 'selectorProfesores',
                disabled : true
            },
            {
                xtype : 'panel',
                name : 'panelFiltroGrupoTipo',
                border : 1,
                flex : 0,
                title : 'Filtres',
                autoScroll : true,
                disabled : true,
                padding : 5,
                height : 260,
                layout :
                {
                    type : 'vbox',
                    align : 'stretch'
                },
                items : [
                {
                    xtype : 'checkbox',
                    boxLabel : 'POD professor',
                    disabled : true,
                    name : 'ocupacion-profesor',
                    padding : 5
                },
                {
                    xtype : 'checkbox',
                    boxLabel : 'Àrea completa',
                    name : 'area-completa',
                    padding : 5
                },
                {
                    xtype : 'boxselect',
                    editable : true,
                    labelAlign : 'top',
                    lastQuery : '',
                    margin : '5 5 0 5',
                    fieldLabel : 'Grup',
                    store : 'StoreGruposAsignatura',
                    displayField : 'grupo',
                    valueField : 'grupo',
                    name : 'grupo'
                },
                {
                    xtype : 'boxselect',
                    editable : true,
                    labelAlign : 'top',
                    lastQuery : '',
                    margin : '5 5 0 5',
                    fieldLabel : 'Tipus activitat',
                    store : 'StoreTipoSubgruposAsignatura',
                    displayField : 'id',
                    valueField : 'id',
                    name : 'tipoSubgrupo'
                } ]
            } ]
        },
        {
            xtype : 'panel',
            name : 'contenedorCalendarios',
            border : 0,
            flex : 1,
            layout :
            {
                type : 'vbox',
                align : 'stretch'
            },

            items : [
            {
                xtype : 'selectorClasesSinPlanificar',
                height : 100,
                disabled : true
            } ]
        } ]
    } ]

});