Ext.define('HOR.view.pod.VentanaCreditos',
{
    extend : 'Ext.Window',
    title : 'Crédits impartits per el professor',
    closeAction : 'hide',
    width : 500,
    alias : 'widget.ventanaCreditos',
    modal : true,

    items : [
    {
        xtype : 'form',
        name : 'formAsignacionCreditos',
        padding : 10,
        enableKeyEvents : true,
        frame : true,
        layout :
        {
            type : 'vbox',
            align : 'stretch'
        },
        fbar : [
        {
            xtype : 'button',
            text : 'Guardar',
            name : 'guardar'
        },
        {
            xtype : 'button',
            text : 'Cancel·lar',
            name : 'cancelar'
        } ],
        items : [
        {
            xtype : 'hiddenfield',
            name : 'itemId'
        },
        {
            xtype : 'hiddenfield',
            name : 'profesorId'
        },
        {
            xtype : 'numberfield',
            fieldLabel : 'Crèdits',
            name : 'creditos',
            padding : 5,
            labelWidth : 130,
            flex : 1,
            allowBlank : true,
            minValue : 0
        },
        {
            xtype : 'numberfield',
            fieldLabel : 'Crèdits computables',
            name : 'creditosComputables',
            padding : 5,
            labelWidth : 130,
            flex : 1,
            allowBlank : true,
            minValue : 0
        } ]
    } ],

    setData : function(asignatura, profesorId, itemId, creditos, creditosComputables, maxCreditos, maxCreditosComputables)
    {
        var form = Ext.ComponentQuery.query('form[name=formAsignacionCreditos]')[0];
        form.getForm().findField('itemId').setValue(itemId);
        form.getForm().findField('profesorId').setValue(profesorId);
        form.getForm().findField('creditos').setValue(creditos);
        form.getForm().findField('creditos').setMaxValue(maxCreditos);
        form.getForm().findField('creditosComputables').setValue(creditosComputables);
        form.getForm().findField('creditosComputables').setMaxValue(maxCreditosComputables || 0);
        this.setTitle(asignatura)
    },

    getData : function()
    {
        var form = Ext.ComponentQuery.query('form[name=formAsignacionCreditos]')[0];
        return form.getForm().getValues();
    }

});