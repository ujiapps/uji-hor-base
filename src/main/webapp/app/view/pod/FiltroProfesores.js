Ext.define('HOR.view.pod.FiltroProfesores',
{
    extend : 'Ext.panel.Panel',
    alias : 'widget.filtroProfesores',
    requires : [ 'HOR.view.commons.GroupedComboBox' ],

    border : false,
    padding : 5,
    closable : false,
    layout : 'anchor',

    items : [
    {
        type : 'panel',
        layout : 'hbox',
        border : 0,
        items : [
        {
            border : 0,
            xtype : 'groupedComboBox',
            editable : false,
            labelWidth : 90,
            labelAlign : 'right',
            margin : '0 0 5 0',
            fieldLabel : 'Departament',
            store : 'StoreDepartamentos',
            displayField : 'nombre',
            groupField : "centroId",
            groupDisplayField : "centroNombre",
            valueField : 'id',
            name : 'departamento',
            width : 420,
            matchFieldWidth : false,
            listConfig :
            {
                width : 640
            }
        },
        {
            xtype : 'text',
            flex : 1,
            name : 'estadoAsignacion',
            width : 400,
            cls : 'infobox',
            hidden : true,
            margin : '0 0 0 20'
        } ]
    },
    {
        border : 0,
        xtype : 'combobox',
        editable : false,
        disabled : true,
        labelWidth : 90,
        labelAlign : 'right',
        margin : '0 0 5 0',
        fieldLabel : 'Area',
        queryMode : 'local',
        store : 'StoreAreas',
        displayField : 'nombre',
        valueField : 'id',
        name : 'area',
        width : 420,
        matchFieldWidth : false,
        listConfig :
        {
            width : 640
        }
    },
    {
        border : 0,
        xtype : 'combobox',
        editable : false,
        disabled : true,
        labelWidth : 90,
        labelAlign : 'right',
        margin : '0 0 5 0',
        fieldLabel : 'Assignatura',
        queryMode : 'local',
        store : 'StoreAsignaturas',
        displayField : 'nombreConCodigo',
        valueField : 'id',
        name : 'asignatura',
        width : 420,
        matchFieldWidth : false,
        listConfig :
        {
            width : 640
        }
    },
    {
        xtype : 'panel',
        border : 0,
        anchor : '100%',
        layout :
        {
            type : 'hbox',
            align : 'fit'
        },
        items : [
        {
            border : 0,
            xtype : 'combobox',
            editable : false,
            disabled : true,
            labelWidth : 90,
            labelAlign : 'right',
            margin : '0 0 5 0',
            fieldLabel : 'Semestre',
            queryMode : 'local',
            store : 'StoreSemestresAsignatura',
            displayField : 'nombre',
            valueField : 'semestre',
            name : 'semestre',
            width : 420
        },
        {
            xtype : 'panel',
            border : 0,
            anchor : '50%',
            flex : 1,
            layout :
            {
                type : 'hbox',
                align : 'fit',
                pack : 'end'
            },
            defaults :
            {
                width : 120,
                labelWidth : 75,
                labelAlign : 'left',
                margin : '0 20 0 0'
            },
            items : [
            {
                name : 'menu-acciones',
                xtype : 'button',
                disabled : true,
                text : 'Informes',
                menu :
                {
                    xtype : 'menu',
                    border : false,
                    plain : true,
                    items : [
                    {
                        name : 'validacionesPOD',
                        margin : '0 0 0 5',
                        flex : 0,
                        text : 'Validacions crèdits',
                        iconCls : 'tick',
                        width : 150
                    },
                    {
                        name : 'validacionesSolapamientos',
                        margin : '0 0 0 5',
                        flex : 0,
                        text : 'Validacions solapaments',
                        iconCls : 'tick',
                        width : 150
                    },
                    {
                        name : 'controlesPOD',
                        margin : '0 0 0 5',
                        flex : 0,
                        text : 'POD del departament',
                        iconCls : 'tick',
                        width : 150

                    },
                    {
                        name : 'asignaturaPOD',
                        margin : '0 0 0 5',
                        flex : 0,
                        text : 'POD per assignatura',
                        iconCls : 'tick',
                        width : 150

                    },
                    {
                        name : 'detalleAsignaturaPOD',
                        margin : '0 0 0 5',
                        flex : 0,
                        text : 'POD per assignatura detallat',
                        iconCls : 'tick',
                        width : 200
                    },
                    {
                        name : 'asignaturaSimplePOD',
                        margin : '0 0 0 5',
                        flex : 0,
                        text : 'POD per assignatura simplificat',
                        iconCls : 'tick',
                        width : 200
                    },
                    {
                        name : 'detalleSesionesPOD',
                        margin : '0 0 0 5',
                        flex : 0,
                        text : 'POD detall sessions',
                        iconCls : 'tick',
                        width : 200
                    },
                    {
                        name : 'miPOD',
                        margin : '0 0 0 5',
                        flex : 0,
                        text : 'El meu POD',
                        iconCls : 'tick',
                        width : 150

                    },
                    {
                        name : 'imprimir',
                        hidden : true,
                        margin : '0 0 0 5',
                        width : '40',
                        flex : 0,
                        text : 'Imprimir',
                        iconCls : 'printer'
                    } ]
                }
            },
            {
                name : 'refrescar',
                xtype : 'button',
                width : 140,
                disabled : true,
                margin : '0 0 0 5',
                flex : 0,
                text : 'Refrescar calendari',
                iconCls : 'calendar'
            },
            {
                margin : '0 0 0 10',
                name : 'calendarioAsignaturasDetalle',
                xtype : 'button',
                enableToggle : true,
                toggleGroup : 'calendario',
                hidden : false,
                disabled : true,
                width : '40',
                flex : 0,
                text : 'Set. detallada',
                iconCls : 'calendar-week'
            },
            {
                name : 'calendarioAsignaturasGenerica',
                xtype : 'button',
                enableToggle : true,
                toggleGroup : 'calendario',
                pressed : true,
                hidden : false,
                disabled : true,
                margin : '0 0 0 5',
                width : '40',
                flex : 0,
                text : 'Set. genèrica',
                iconCls : 'calendar-edit'
            } ]
        } ]
    } ]
});