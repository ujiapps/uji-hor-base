Ext.define('HOR.view.pod.SelectorClasesSinPlanificar',
{
    extend : 'Ext.panel.Panel',
    requires : [ 'HOR.model.enums.TipoCalendarioAsignatura' ],
    alias : 'widget.selectorClasesSinPlanificar',
    title : 'Sense horari assignat',
    autoScroll : true,
    padding : 5,
    flex : 0,
    contextMenu : {},
    layout :
    {
        type : 'hbox'
    },

    addClases : function(clases, profesorId)
    {
        this.removeAll();
        var botones = [];
        var ref = this;

        this.contextMenu = new Ext.menu.Menu(
        {
            items : [
            {
                text : 'Assignar crèdits',
                iconCls : 'extensible-cal-icon-evt-edit',
                handler : ref.asignarCreditos
            },
            {
                text : 'Establir idioma',
                iconCls : 'extensible-cal-icon-evt-lang',
                handler : ref.asignarIdiomas
            } ]
        });

        for (var i = 0, len = clases.length; i < len; i++)
        {
            var pressed = clases[i].data.cid === HOR.model.enums.TipoCalendarioAsignatura.MISMO_PROFESOR_ASIGNADO.id;

            var button =
            {
                xtype : 'button',
                enableToggle : true,
                text : clases[i].data.title,
                claseId : clases[i].data.id,
                clase : clases[i],
                padding : '2 5 2 5',
                data : clases[i].data,
                margin : '10 10 0 10',
                height : 24,
                toggleHandler : this.onToggleBoton,
                pressed : pressed,
                listeners :
                {
                    mouseover : function(event, target, options)
                    {
                        ref.updateTooltip(event);
                    },
                    mouseout : function(event, target, options)
                    {
                        ref.tooltip.hide();
                    },
                    afterrender : function()
                    {
                        var boton = this;
                        this.getEl().on('contextmenu', function(e)
                        {
                            if (boton.pressed)
                            {
                                ref.contextMenu.clase = boton.clase;
                                ref.contextMenu.componente = ref;
                                ref.contextMenu.showAt(e.getXY());
                            }
                            e.preventDefault();
                        });
                    }
                },
                cls : 'clase-sin-planificar-estado-' + clases[i].data.cid
            };

            botones.push(button);
        }

        this.add(botones);
    },

    getCreditosPorAsignar : function(creditosAsignatura, creditosAsignados)
    {
        return Math.round(100 * (creditosAsignatura - creditosAsignados)) / 100;
    },

    updateTooltip : function(event, target)
    {
        if (!this.tooltip)
        {
            this.tooltip = Ext.create('Ext.tip.ToolTip',
            {
                hidden : true,
                target : this.getEl(),
                trackMouse : true,
                autoHide : false
            });
        }

        if (event.data)
        {
            var html = '';
            if (event.data.profesores)
            {
                html = "Professors: <br/><br/>" + event.data.profesores.replace(/;/g, '<br/>') + "<br /><br />";
            }
            var creditosPorAsignar = this.getCreditosPorAsignar(event.data.creditos, event.data.creditosAsignados);
            html += "Per assignar: " + creditosPorAsignar + " crèdits";
            var creditosPorAsignarComputables = this.getCreditosPorAsignar(event.data.creditosComputables, event.data.creditosAsignadosComputables);
            if (event.data.creditosComputables != null && creditosPorAsignar != creditosPorAsignarComputables) {
                html += " (" + creditosPorAsignarComputables + " comp.)";
            }
            this.tooltip.update(html);
            this.tooltip.show();
        }
        else
        {
            this.tooltip.hide();
        }
    },

    onToggleBoton : function(boton)
    {
        if (boton.clase.data.cid === HOR.model.enums.TipoCalendarioAsignatura.OCUPACION_PROFESOR.id)
        {
            return;
        }
        if (boton.pressed)
        {
            this.up('panel').fireEvent('claseSelected', boton.claseId);
        }
        else
        {
            this.up('panel').fireEvent('claseDeselected', boton.claseId);
        }
    },

    asignarCreditos : function(item)
    {
        var clase = item.parentMenu.clase;
        var componente = item.parentMenu.componente;
        componente.fireEvent('asignarCreditos', clase);
    },

    asignarIdiomas : function(item)
    {
        var clase = item.parentMenu.clase;
        var componente = item.parentMenu.componente;
        componente.fireEvent('asignarIdioma', clase);
    }

});
