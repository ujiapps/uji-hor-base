Ext.define('HOR.view.circuitos.PanelCalendarioAsignatura',
{
    extend : 'Extensible.calendar.CalendarPanel',
    alias : 'widget.panelCalendarioAsignatura',
    region : 'center',
    title : 'Calendari POD',
    depends : [ 'HOR.store.StoreEventosPlanificadosAsignatura', 'HOR.store.StoreCalendariosAsignaturas' ],
    requires : [ 'HOR.view.pod.asignacion.FormAsignacionProfesores' ],
    calendarStore : Ext.create('HOR.store.StoreCalendariosAsignaturas'),
    editModal : false,
    enableEditDetails : false,
    flex : 1,
    padding : 5,
    showTodayText : false,
    showNavToday : false,
    showDayView : false,
    showWeekView : false,
    showMonthView : false,
    viewConfig :
    {
        viewStartHour : 8,
        viewEndHour : 22
    },
    weekViewCfg :
    {
        dayCount : 5,
        startDay : 1,
        startDayIsStatic : true
    },
    showMultiDayView : true,
    showMultiWeekView : false,
    showNavJump : false,
    showNavNextPrev : false,
    multiDayViewCfg :
    {
        dayCount : 5,
        startDay : 1,
        startDayIsStatic : true,
        showTime : false,
        showMonth : false,
        getStoreParams : function()
        {
            var params = this.getStoreDateParams();
            params.estudioId = this.store.getProxy().extraParams['asignaturaId'];
            params.semestreId = this.store.getProxy().extraParams['semestreId'];
            return params;
        }
    },

    asignarProfesorView : {},

    getMultiDayText : function()
    {
        return 'Setmana genèrica';
    },

    initComponent : function()
    {
        Extensible.calendar.template.BoxLayout.override(
        {
            firstWeekDateFormat : 'l',
            multiDayFirstDayFormat : 'l',
            multiDayMonthStartFormat : 'l'
        });

        this.callParent(arguments);

        this.creaFormularioDeAsignacionProfesor();

        Extensible.calendar.menu.Event.override(
        {
            buildMenu : function()
            {
                var me = this;

                if (me.rendered)
                {
                    return;
                }
                Ext.apply(me,
                {
                    items : [
                    {
                        text : me.editDetailsText,
                        iconCls : 'extensible-cal-icon-evt-edit',
                        scope : me,
                        handler : function()
                        {
                            Ext.ComponentQuery.query("panelCalendarioAsignatura")[0].fireEvent('asignacionClasesProfesorDetails', me, me.rec, me.ctxEl);
                        }
                    },
                    {
                        text : 'Assignar Professor',
                        iconCls : 'extensible-cal-icon-evt-edit',
                        scope : me,
                        handler : function()
                        {
                            Ext.ComponentQuery.query("panelCalendarioAsignatura")[0].fireEvent('eventclick', me, me.rec);
                        }
                    },
                    {
                        text : 'Establir idioma',
                        iconCls : 'extensible-cal-icon-evt-lang',
                        scope : me,
                        handler : function()
                        {
                            Ext.ComponentQuery.query("panelCalendarioAsignatura")[0].fireEvent('asignacionIdiomasClaseProfesor', me, me.rec);
                        }
                    } ]
                });
            },

            showForEvent : function(rec, el, xy)
            {
                if (rec.data.CalendarId === '4')
                {
                    return false;
                }
                var me = this;
                me.rec = rec;
                me.ctxEl = el;
                me.showAt(xy);
            }
        });
    },

    showAsignacionClasesProfesorDetails : function(profesor, eventoPrincipal, eventos)
    {
        var asignarClaseId = this.id + '-profesor';
        this.asignarProfesorView = this.layout.getActiveItem().id;
        this.setActiveViewForAsignarProfesor(asignarClaseId, profesor, eventoPrincipal);

        var formularioAsignacionClases = this.layout.getActiveItem();
        formularioAsignacionClases.setDatos(profesor, eventoPrincipal, eventos, this);
        return this;
    },

    setActiveViewForAsignarProfesor : function(id, profesor, evento)
    {
        var me = this, layout = this.layout, asignarClaseViewId = me.id + '-profesor', toolbar;

        if (id !== layout.getActiveItem().id)
        {
            toolbar = me.getDockedItems('toolbar')[0];
            if (toolbar)
            {
                toolbar[id === asignarClaseViewId ? 'hide' : 'show']();
            }

            layout.setActiveItem(id || me.activeItem);
            me.doComponentLayout();
            me.activeView = layout.getActiveItem();

            if (id !== asignarClaseViewId)
            {
                if (id && id !== me.asignarProfesorView)
                {
                    layout.activeItem.setStartDate(me.startDate, true);
                }
                me.updateNavState();
            }
            me.fireViewChange();
        }
    },

    creaFormularioDeAsignacionProfesor : function()
    {
        this.add([
        {
            xtype : 'formAsignacionProfesores',
            id : this.id + '-profesor'
        } ]);
    },

    onStoreUpdate : function()
    {
    },

    getEventStore : function()
    {
        return this.store;
    },

    listeners :
    {
        dayclick : function(dt, allday, el)
        {
            return false;
        },

        rangeselect : function()
        {
            return false;
        }
    }
});