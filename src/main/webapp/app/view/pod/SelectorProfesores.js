Ext.define('HOR.view.pod.SelectorProfesores',
{
    extend : 'Ext.panel.Panel',
    alias : 'widget.selectorProfesores',
    title : 'Professors de l\'area',
    autoScroll : true,
    padding : 5,
    flex : 1,
    layout :
    {
        type : 'vbox',
        align : 'stretch'
    },

    addProfesores : function(profesores)
    {
        this.removeAll();
        var botones = [];
        var ref = this;

        for (var i = 0, len = profesores.length; i < len; i++)
        {
            var button =
            {
                xtype : 'button',
                enableToggle : true,
                allowDepress : false,
                toggleGroup : 'profesores',
                text : profesores[i].data.nombre,
                creditosPendientes : profesores[i].data.creditosPendientes,
                creditosMaximos : profesores[i].data.creditosMaximos,
                nombreCategoria : profesores[i].data.nombreCategoria,
                creditosReduccion : profesores[i].data.creditosReduccion,
                creditosAsignados : profesores[i].data.creditosAsignados,
                creditosComputables : profesores[i].data.creditosComputables,
                creditos : profesores[i].data.creditos,
                profesorId : profesores[i].data.id,
                padding : '2 5 2 5',
                margin : '10 10 0 10',
                toggleHandler : this.onToggleBoton,
                listeners :
                {
                    mouseover : function(event, target, options)
                    {
                        ref.updateTooltip(event);
                    },
                    mouseout : function(event, target, options)
                    {
                        ref.tooltip.hide();
                    }
                }
            };
            botones.push(button);
        }

        this.add(botones);
    },

    updateProfesor : function(profesorData)
    {
        var botones = Ext.ComponentQuery.query('selectorProfesores button');
        for (var i = 0; i < botones.length; i++)
        {
            if (botones[i].profesorId === profesorData.id)
            {
                botones[i].creditosPendientes = profesorData.creditosPendientes;
                botones[i].creditosAsignados = profesorData.creditosAsignados;
                botones[i].creditosComputables = profesorData.creditosComputables;
                botones[i].creditos = profesorData.creditos;
            }
        }
    },

    getValue : function()
    {
        var botones = Ext.ComponentQuery.query('selectorProfesores button');
        for (var i = 0; i < botones.length; i++)
        {
            if (botones[i].pressed == true)
            {
                return botones[i].profesorId;
            }
        }
        return null;
    },

    getText : function()
    {
        var botones = Ext.ComponentQuery.query('selectorProfesores button');
        for (var i = 0; i < botones.length; i++)
        {
            if (botones[i].pressed == true)
            {
                return botones[i].text;
            }
        }
        return null;
    },

    updateTooltip : function(event)
    {
        if (event.disabled)
        {
            this.tooltip.hide();
            return;
        }
        if (!this.tooltip)
        {
            this.tooltip = Ext.create('Ext.tip.ToolTip',
            {
                hidden : true,
                target : this.getEl(),
                trackMouse : true,
                autoHide : false
            });
        }

        if (event.creditosPendientes)
        {
            var msg = '';

            if (event.nombreCategoria)
            {
                msg += 'Categoria: ' + event.nombreCategoria + '<br />';
            }

            msg += 'Crèdits a impartir: ' + event.creditosMaximos + '<br />';
            msg += 'Crèdits reducció: ' + event.creditosReduccion + '<br />';
            msg += 'Crèdits assignats: ' + event.creditosAsignados + '<br />';
            msg += 'Crèdits assignats comp: ' + event.creditosComputables + '<br />';
            if (event.creditosPendientes < 0)
            {
                msg += 'Crèdits sobrants: ' + (-event.creditosPendientes) + '<br />';
            }
            else
            {
                msg += 'Crèdits per assignar: ' + event.creditosPendientes + '<br />';
            }

            if (event.creditos > 0)
            {
                msg += 'Assignació: ' + (Math.round(event.creditosComputables * 1000 / event.creditos) / 10) + '%<br />';
            }

            this.tooltip.update(msg);
            this.tooltip.show();
        }
        else
        {
            this.tooltip.hide();
        }
    },

    disableTooltips : function()
    {
        if (this.tooltip)
        {
            this.tooltip.destroy();
            delete this.tooltip;
        }
    },

    onToggleBoton : function(boton)
    {
        if (boton.pressed)
        {
            this.up('panel').fireEvent('profesorSelected', boton);
        }
    }
});