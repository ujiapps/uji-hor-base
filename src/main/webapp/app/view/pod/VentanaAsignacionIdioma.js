Ext.define('HOR.view.examenes.VentanaAsignacionIdioma',
{
    extend : 'Ext.Window',
    title : 'Assignació d\'idioma',
    width : 500,
    alias : 'widget.ventanaAsignacionIdioma',
    modal : true,

    closeAction : 'close',

    onEsc : function()
    {
    },

    items : [
    {
        xtype : 'form',
        name : 'formIdiomas',
        padding : 10,
        enableKeyEvents : true,
        layout :
        {
            type : 'vbox',
            align : 'stretch'
        },
        fbar : [
        {
            xtype : 'button',
            text : 'Guardar',
            name : 'guardar'
        },
        {
            xtype : 'button',
            text : 'Cancel·lar',
            name : 'cancelar'
        } ],
        items : [
        {
            xtype : 'hiddenfield',
            name : 'itemId'
        },
        {
            xtype : 'hiddenfield',
            name : 'profesorId'
        },
        {
            xtype : 'textfield',
            name : 'profesorNombre',
            disabled : true,
            disabledCls : 'dummy'
        },
        {
            xtype : 'radiogroup',
            padding : 10,
            name : 'grupoIdiomas',
            columns : 2,
            items : []
        } ]
    } ],

    setData : function(itemId, profesorId, idiomas)
    {
        var itemField = this.down('hiddenfield[name=itemId]');
        itemField.setValue(itemId);

        var profesorField = this.down('hiddenfield[name=profesorId]');
        profesorField.setValue(profesorId);

        var radiogroup = this.down('radiogroup');
        radiogroup.removeAll();
        for (var i = 0; i < idiomas.length; i++)
        {
            var radio = Ext.create('Ext.form.field.Radio',
            {
                inputValue : idiomas[i].id,
                boxLabel : idiomas[i].nombre,
                name : 'idiomas',
                checked : idiomas[i].activo === 'true',
            });

            radiogroup.add(radio);
        }
    },

    setProfesor : function(nombre)
    {
        var box = this.down('textfield[name=profesorNombre]');
        box.setValue('Professor: ' + nombre);
    },

    getData : function()
    {
        var form = Ext.ComponentQuery.query('form[name=formIdiomas]')[0];
        return form.getForm().getValues();
    }

});