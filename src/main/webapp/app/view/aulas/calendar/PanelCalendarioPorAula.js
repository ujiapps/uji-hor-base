Ext.define('HOR.view.horarios.PanelCalendarioPorAula',
{
    extend : 'Extensible.calendar.CalendarPanel',
    alias : 'widget.panelCalendarioPorAula',
    region : 'center',
    title : 'Ocupació Aula',
    requires : [ 'HOR.view.aulas.asignacion.FormAsignacionAulasPanelCalendarioPorAulas' ],
    depends : [ 'HOR.store.StoreCalendarios', 'HOR.store.StoreAulasGenerica' ],
    calendarStore : Ext.create('HOR.store.StoreCalendarios'),
    editModal : true,
    flex : 1,
    padding : 5,
    showTodayText : false,
    showNavToday : false,
    showDayView : false,
    showWeekView : false,
    showMonthView : false,
    viewConfig :
    {
        viewStartHour : 8,
        viewEndHour : 22
    },
    weekViewCfg :
    {
        dayCount : 5,
        startDay : 1,
        startDayIsStatic : true
    },
    showMultiDayView : true,
    showMultiWeekView : false,
    showNavJump : false,
    showNavNextPrev : false,
    multiDayViewCfg :
    {
        dayCount : 5,
        startDay : 1,
        startDayIsStatic : true,
        showTime : false,
        showMonth : false,
        getStoreParams : function()
        {
            var params = this.getStoreDateParams();
            params.aulaId = this.store.getProxy().extraParams['aulaId'];
            params.semestreId = this.store.getProxy().extraParams['semestreId'];
            return params;
        }
    },
    getMultiDayText : function()
    {
        return 'Setmana genèrica';
    },

    limpiaCalendario : function()
    {
        this.store.removeAll(false);
        this.setTitle('Ocupació Aula');
    },

    initComponent : function()
    {
        Extensible.calendar.template.BoxLayout.override(
        {
            firstWeekDateFormat : 'l',
            multiDayFirstDayFormat : 'l',
            multiDayMonthStartFormat : 'l'
        });

        this.callParent(arguments);

        var calendario = this;

        this.creaFormularioDeAsignacionAulas();

        Extensible.calendar.menu.Event.override(
        {
            buildMenu : function()
            {
                var me = this;

                if (me.rendered)
                {
                    return;
                }
                Ext.apply(me,
                {
                    items : [
                    {
                        text : 'Assignar aula',
                        iconCls : 'extensible-cal-icon-evt-edit',
                        scope : me,
                        handler : function()
                        {
                            calendario.fireEvent('eventasignaaula', me, me.rec, me.ctxEl);
                        }
                    } ]
                });
            },

            showForEvent : function(rec, el, xy)
            {
                var me = this;
                me.rec = rec;
                me.ctxEl = el;
                me.showAt(xy);
            }
        });
    },
    onStoreUpdate : function()
    {
    },

    getEventStore : function()
    {
        return this.store;
    },

    showAsignarAulaView : function()
    {
        var asignarAulaId = this.id + '-aula';
        this.preAsignarAulaView = this.layout.getActiveItem().id;
        this.setActiveViewForAsignarAula(asignarAulaId);
        return this;
    },

    hideAsignarAulaView : function()
    {
        if (this.preAsignarAulaView)
        {
            this.setActiveViewForAsignarAula(this.preAsignarAulaView);
            delete this.preEditView;
        }
        return this;
    },

    creaFormularioDeAsignacionAulas : function()
    {
        this.add([
            {
                xtype : 'formAsignacionAulasPanelCalendarioPorAulas',
                id : this.id + '-aula'
            } ]);
    },

    setActiveViewForAsignarAula : function(id, startDate)
    {
        var me = this, layout = me.layout, asignarAulaViewId = me.id + '-aula', toolbar;

        if (startDate)
        {
            me.startDate = startDate;
        }

        // Make sure we're actually changing views
        if (id !== layout.getActiveItem().id)
        {
            // Show/hide the toolbar first so that the layout will calculate the correct item size
            toolbar = me.getDockedItems('toolbar')[0];
            if (toolbar)
            {
                toolbar[id === asignarAulaViewId ? 'hide' : 'show']();
            }

            // Activate the new view and refresh the layout
            layout.setActiveItem(id || me.activeItem);
            me.doComponentLayout();
            me.activeView = layout.getActiveItem();

            if (id !== asignarAulaViewId)
            {
                if (id && id !== me.preAsignarAulaView)
                {
                    // We're changing to a different view, so the view dates are likely different.
                    // Re-set the start date so that the view range will be updated if needed.
                    // If id is undefined, it means this is the initial pass after render so we can
                    // skip this (as we don't want to cause a duplicate forced reload).
                    layout.activeItem.setStartDate(me.startDate, true);
                }
                // Switching to a view that's not the edit view (i.e., the nav bar will be visible)
                // so update the nav bar's selected view button
                me.updateNavState();
            }
            // Notify any listeners that the view changed
            me.fireViewChange();
        }
    }

});