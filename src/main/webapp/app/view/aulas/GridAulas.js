Ext.define('HOR.view.aulas.GridAulas',
{
    extend : 'Ext.grid.Panel',
    title : 'Aules assignades',
    alias : 'widget.gridAulas',
    store : 'StoreAulasAsignadas',
    disableSelection : false,
    selModel :
    {
        mode : 'MULTI'
    },
    sortableColumns : true,
    tbar : [
    {
        xtype : 'button',
        name : 'borrar',
        text : 'Esborrar aula',
        iconCls : 'application-delete'
    } ],
    columns : [ {
        text: 'Id',
        dataIndex: 'id',
        hidden : true
    },
    {
        text : 'Aula',
        dataIndex : 'nombre',
        width : '40%'
    },
    {
        text : 'Places',
        dataIndex : 'plazas',
        width : '15%'
    },
    {
        text : 'Edifici',
        dataIndex : 'edificio',
        width : '15%'
    },
    {
        text : 'Tipus',
        dataIndex : 'tipo',
        width : '15%'
    },
    {
        text : 'Planta',
        dataIndex : 'planta',
        width : '15%'
    },
    {
        text : 'Semestre',
        dataIndex : 'semestreId',
        width : '15%'
    } ]
});