Ext.define('HOR.view.permisos.VentanaNewPermiso',
{
    extend : 'Ext.Window',
    title : 'Afegir un nou permís',
    width : 800,
    alias : 'widget.ventanaNewPermiso',
    layout : 'fit',
    modal : true,

    buttonAlign : 'right',
    bbar : [
    {
        xtype : 'tbfill'
    },
    {
        xtype : 'button',
        text : 'Guardar',
        action : 'guardar-permiso'
    },
    {
        xtype : 'button',
        text : 'Cancel·lar',
        action : 'cancelar'
    } ],

    items : [
    {
        xtype : 'form',
        name : 'formNewPermiso',
        padding : 10,
        items : [
        {
            xtype : 'lookupCombobox',
            appPrefix : 'hor',
            bean : 'persona',
            name : 'comboPersona',
            padding : 20,
            fieldLabel : 'Persona',
            labelWidth : 75,
            editable : false,
            allowBlank : false,
            displayField : 'nombre',
            valueField : 'id',
            anchor : '100%'
        },
        {
            xtype : 'combo',
            name : 'comboCargo',
            padding : '0 20 20 20',
            fieldLabel : 'Càrrec',
            labelWidth : 75,
            store : 'StoreCargos',
            editable : false,
            displayField : 'nombre',
            valueField : 'id',
            allowBlank : false,
            anchor : '100%'
        },
        {
            xtype : 'combo',
            name : 'comboTitulacion',
            padding : '0 20 20 20',
            fieldLabel : 'Titulació',
            labelWidth : 75,
            store : 'StoreEstudios',
            editable : false,
            displayField : 'nombre',
            valueField : 'id',
            allowBlank : false,
            hidden : true,
            anchor : '100%'
        },
        {
            xtype : 'combo',
            name : 'comboDepartamento',
            padding : '0 20 20 20',
            fieldLabel : 'Departament',
            labelWidth : 75,
            store : 'StoreDepartamentosPermisos',
            editable : false,
            displayField : 'nombre',
            valueField : 'id',
            allowBlank : false,
            hidden : true,
            anchor : '100%'
        },
        {
            xtype : 'panel',
            layout : 'hbox',
            name : 'contenedorArea',
            border : false,
            hidden : true,
            items : [
            {
                xtype : 'combo',
                name : 'comboArea',
                padding : '0 20 20 20',
                fieldLabel : 'Àrees',
                labelWidth : 75,
                store : 'StoreAreasPermisos',
                editable : false,
                displayField : 'nombre',
                valueField : 'id',
                allowBlank : true,
                emptyText : 'Totes les àrees',
                anchor : '100%',
                width : 555
            },
            {
                xtype : 'button',
                name : 'botonTodasAreas',
                text : 'Seleccionar totes les àrees',
                width : 150
            } ]
        } ]
    } ]

});