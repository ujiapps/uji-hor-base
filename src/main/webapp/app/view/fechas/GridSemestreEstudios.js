Ext.define('HOR.view.fechas.GridSemestreEstudios',
{
    extend : 'Ext.ux.uji.grid.Panel',
    requires : [],
    store : 'StoreSemestreDetalleEstudios',
    alias : 'widget.gridSemestreEstudios',
    sortableColumns : false,
    columns : [
    {
        text : 'Estudi',
        dataIndex : 'estudioId',
        flex : 4,
        renderer : function(value, metadata, record)
        {
            return record.get('_estudio').nombre;
        },
        editor :
        {
            xtype : 'combobox',
            store : 'StoreEstudiosTodos',
            displayField : 'nombre',
            valueField : 'id',
            allowBlank : false,
            queryMode : 'local',
            editable : false
        }
    },
    {
        text : 'Semestre',
        dataIndex : 'semestreDetalleId',
        flex : 3,
        editor :
        {
            xtype : 'combobox',
            store : 'StoreSemestreDetalles',
            displayField : 'displayName',
            valueField : 'id',
            allowBlank : false,
            queryMode : 'local',
            editable : false
        },
        renderer : function(value, metadata, record)
        {
            var detalleSemestre = record.get('_semestreDetalle');
            var tipoEstudioMap =
            {
                'G' : 'Graus',
                'M' : 'Masters'
            };
            var semestreMap =
            {
                '1' : 'Primer semestre',
                '2' : 'Segon semestre'
            };
            return tipoEstudioMap[detalleSemestre.tipoEstudio] + ' - ' + semestreMap[detalleSemestre.semestre];
        }
    },
    {
        text : 'Curs',
        dataIndex : 'cursoAcademico',
        flex : 1,
        field :
        {
            xtype : 'numberfield',
            allowBlank : false,
            minValue : 1,
            maxValue : 6
        }
    },
    {
        text : 'Setmanes lectives',
        dataIndex : 'numeroSemanas',
        flex : 2,
        field :
        {
            xtype : 'numberfield',
            allowBlank : false,
            minValue : 1,
            maxValue : 52
        }
    },
    {
        text : 'Inici docència',
        dataIndex : 'fechaInicio',
        flex : 2,
        editor :
        {
            xtype : 'datefield',
            format : 'd/m/Y',
            minValue : '1',
            allowBlank : false
        },
        renderer : function(value)
        {
            return Ext.Date.format(value, "d/m/Y");
        }
    },
    {
        text : 'Fi docència',
        dataIndex : 'fechaFin',
        flex : 2,
        editor :
        {
            xtype : 'datefield',
            format : 'd/m/Y',
            minValue : '1',
            allowBlank : false
        },
        renderer : function(value)
        {
            return Ext.Date.format(value, "d/m/Y");
        }
    },
    {
        text : 'Inici exàmens (1ª conv.)',
        dataIndex : 'fechaExamenesInicio',
        flex : 2,
        editor :
        {
            xtype : 'datefield',
            format : 'd/m/Y',
            minValue : '1',
            allowBlank : false
        },
        renderer : function(value)
        {
            return Ext.Date.format(value, "d/m/Y");
        }
    },
    {
        text : 'Fi exàmens (1ª conv.)',
        dataIndex : 'fechaExamenesFin',
        flex : 2,
        editor :
        {
            xtype : 'datefield',
            format : 'd/m/Y',
            minValue : '1',
            allowBlank : false
        },
        renderer : function(value)
        {
            return Ext.Date.format(value, "d/m/Y");
        }
    },
    {
        text : 'Inici exàmens (2ª conv.)',
        dataIndex : 'fechaExamenesInicioC2',
        flex : 2,
        editor :
        {
            xtype : 'datefield',
            format : 'd/m/Y',
            minValue : '1',
            allowBlank : false
        },
        renderer : function(value)
        {
            return Ext.Date.format(value, "d/m/Y");
        }
    },
    {
        text : 'Fi exàmens (2ª conv.)',
        dataIndex : 'fechaExamenesFinC2',
        flex : 2,
        editor :
        {
            xtype : 'datefield',
            format : 'd/m/Y',
            minValue : '1',
            allowBlank : false
        },
        renderer : function(value)
        {
            return Ext.Date.format(value, "d/m/Y");
        }
    } ]
});
