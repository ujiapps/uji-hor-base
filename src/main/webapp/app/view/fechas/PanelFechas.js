Ext.define('HOR.view.fechas.PanelFechas',
{
    extend : 'Ext.panel.Panel',
    title : 'Gestió del curs acadèmic per estudi',
    requires : [ 'HOR.view.fechas.GridSemestreEstudios' ],
    alias : 'widget.panelSemestres',
    closable : true,
    layout :
    {
        type : 'vbox',
        align : 'stretch'
    },
    items : [
    {
        xtype : 'gridSemestreEstudios',
        flex : 1
    } ]
});