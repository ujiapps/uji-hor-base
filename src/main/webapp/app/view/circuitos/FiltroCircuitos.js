Ext.define('HOR.view.circuitos.FiltroCircuitos',
{
    extend : 'Ext.panel.Panel',
    alias : 'widget.filtroCircuitos',

    border : false,
    padding : 5,
    closable : false,
    layout : 'anchor',

    items : [
    {
        border : 0,
        xtype : 'combobox',
        editable : false,
        labelWidth : 60,
        labelAlign : 'left',
        margin : '0 20 0 0',
        fieldLabel : 'Titulacio',
        store : 'StoreEstudios',
        displayField : 'nombre',
        valueField : 'id',
        name : 'estudio',
        width : 500
    },
    {
        xtype : 'panel',
        border : 0,
        anchor : '100%',
        layout :
        {
            type : 'hbox',
            align : 'fit'
        },

        items : [
        {
            xtype : 'panel',
            border : 0,
            anchor : '70%',
            layout :
            {
                type : 'hbox',
                align : 'fit',
                padding : '5 0 0 0'
            },
            defaults :
            {
                xtype : 'combobox',
                editable : false,
                lastQuery : '',
                labelWidth : 60,
                labelAlign : 'left',
                margin : '0 20 0 0'
            },
            items : [
            {
                fieldLabel : 'Curs',
                store : [ 1 ],
                width : 120,
                value : 1,
                displayField : 'name',
                valueField : 'id',
                name : 'curso'
            },
            {
                fieldLabel : 'Grup',
                store : 'StoreGruposCircuitos',
                displayField : 'grupo',
                valueField : 'grupo',
                name : 'grupo',
                disabled : true,
                labelWidth : 55,
                width : 150
            },
            {
                xtype : 'combobox',
                lastQuery : '',
                fieldLabel : 'Semestre:',
                labelWidth : 120,
                labelAlign : 'right',
                width : 190,
                store : Ext.create('Ext.data.ArrayStore',
                {
                    fields : [ 'index', 'name' ],
                    data : [ [ '1', '1' ], [ '2', '2' ] ]
                }),
                editable : false,
                displayField : 'name',
                valueField : 'index',
                name : 'semestre',
                anchor : '20%',
                disabled : true,
                alias : 'widget.comboSemestre'
            } ]
        },
        {
            xtype : 'panel',
            border : 0,
            anchor : '50%',
            flex : 1,
            layout :
            {
                type : 'hbox',
                align : 'fit',
                pack : 'end'
            },
            defaults :
            {
                width : 120,
                labelWidth : 75,
                labelAlign : 'left',
                margin : '0 20 0 0'
            },
            items : [
            {
                name : 'validar',
                xtype : 'button',
                hidden : true,
                margin : '0 0 0 5',
                width : '40',
                flex : 0,
                text : 'Validaciones',
                iconCls : 'tick'
            },
            {
                name : 'imprimir',
                xtype : 'button',
                hidden : true,
                margin : '0 0 0 5',
                width : '40',
                flex : 0,
                text : 'Imprimir',
                iconCls : 'printer'
            },
            {
                margin : '0 0 0 10',
                name : 'calendarioCircuitosDetalle',
                xtype : 'button',
                enableToggle : true,
                hidden : true,
                width : '40',
                flex : 0,
                text : 'Set. detallada',
                iconCls : 'calendar-week'
            },
            {
                name : 'calendarioCircuitosGenerica',
                xtype : 'button',
                enableToggle : true,
                pressed : true,
                hidden : true,
                margin : '0 0 0 5',
                width : '40',
                flex : 0,
                text : 'Set. genèrica',
                iconCls : 'calendar-edit'
            } ]
        } ]
    } ]
});