Ext.define('HOR.view.circuitos.SelectorClasesSinPlanificar',
{
    extend : 'Ext.panel.Panel',
    requires : [ 'HOR.model.enums.TipoCalendarioAsignatura' ],
    alias : 'widget.selectorClasesSinPlanificarCircuitos',
    title : 'Sense horari assignat',
    autoScroll : true,
    padding : 5,
    flex : 0,
    contextMenu : {},
    layout :
    {
        type : 'hbox'
    },

    addClases : function(clases)
    {
        this.removeAll();
        var botones = [];
        var ref = this;

        for (var i = 0, len = clases.length; i < len; i++)
        {
            var pressed = clases[i].data.cid === 1;

            var button =
            {
                xtype : 'button',
                enableToggle : true,
                text : clases[i].data.title,
                claseId : clases[i].data.id,
                clase : clases[i],
                padding : '2 5 2 5',
                data : clases[i].data,
                margin : '10 10 0 10',
                height : 24,
                toggleHandler : this.onToggleBoton,
                pressed : pressed,
                listeners :
                {
                    mouseover : function(event, target, options)
                    {
                        ref.updateTooltip(event);
                    },
                    mouseout : function(event, target, options)
                    {
                        ref.tooltip.hide();
                    },
                    afterrender : function()
                    {
                        var boton = this;
                        this.getEl().on('contextmenu', function(e)
                        {
                            if (boton.pressed)
                            {
                                ref.contextMenu.clase = boton.clase;
                                ref.contextMenu.componente = ref;
                                ref.contextMenu.showAt(e.getXY());
                            }
                            e.preventDefault();
                        });
                    }
                },
                cls : 'clase-sin-planificar-circuito-estado-' + clases[i].data.cid
            };

            botones.push(button);
        }

        this.add(botones);
    },

    updateTooltip : function(event)
    {
        if (!this.tooltip)
        {
            this.tooltip = Ext.create('Ext.tip.ToolTip',
            {
                hidden : true,
                target : this.getEl(),
                trackMouse : true,
                autoHide : false
            });
        }

        if (event.data)
        {
            var html = '';
            if (event.data.title)
            {
                html = event.data.title + ' - ' + event.data.nombreAsignatura;
            }
            this.tooltip.update(html);
            this.tooltip.show();
        }
        else
        {
            this.tooltip.hide();
        }
    },

    onToggleBoton : function(boton)
    {
        if (boton.pressed)
        {
            this.up('panel').fireEvent('claseDeselectedCircuito', boton.claseId);
        }
        else
        {
            this.up('panel').fireEvent('claseSelectedCircuito', boton.claseId);
        }
    }
});
