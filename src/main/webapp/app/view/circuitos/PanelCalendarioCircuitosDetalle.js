Ext.define('HOR.view.circuitos.PanelCalendarioCircuitosDetalle',
{
    extend : 'Extensible.calendar.CalendarPanel',
    alias : 'widget.panelCalendarioCircuitosDetalle',
    region : 'center',
    title : 'Circuit',
    depends : [ 'HOR.store.StoreCalendariosCircuitos', 'HOR.store.StoreEventosCircuitoDetalle' ],
    calendarStore : Ext.create('HOR.store.StoreCalendariosCircuitos'),
    eventStore : Ext.create('HOR.store.StoreEventosCircuitoDetalle'),
    editModal : false,
    flex : 1,
    padding : 5,
    showMultiDayView : true,
    showMultiWeekView : true,
    showMonthView : false,
    showWeekView : false,
    activeItem : 1,
    viewConfig :
    {
        viewStartHour : 8,
        viewEndHour : 22
    },
    multiDayViewCfg :
    {
        dayCount : 5,
        startDay : 1,
        startDayIsStatic : true,
        showTime : false,
        showMonth : false,
        getStoreParams : function()
        {
            var params = this.getStoreDateParams();
            params.estudioId = this.store.getProxy().extraParams['estudioId'];
            params.semestreId = this.store.getProxy().extraParams['semestreId'];
            params.grupoId = this.store.getProxy().extraParams['grupoId'];
            params.circuitoId = this.store.getProxy().extraParams['circuitoId'];
            return params;
        }
    },
    multiWeekViewCfg :
    {
        weekCount : 4,
        showTime : false,
        showMonth : true

    },

    limpiaCalendario : function()
    {
        this.store.removeAll(false);
        this.setTitle('Circuit');
    },

    initComponent : function()
    {
        Extensible.calendar.template.BoxLayout.override(
        {
            firstWeekDateFormat : 'D j',
            multiDayFirstDayFormat : 'M j, Y',
            multiDayMonthStartFormat : 'M j'
        });

        this.callParent(arguments);

        Extensible.calendar.menu.Event.override(
        {
            buildMenu : function()
            {
                var me = this;

                if (me.rendered)
                {
                    return;
                }
                Ext.apply(me,
                {
                    items : []
                });
            },

            showForEvent : function(rec, el, xy)
            {

            }
        });
    },
    getMultiDayText : function()
    {
        return 'Setmana';
    },

    getMultiWeekText : function()
    {
        return 'Mes';
    },
    onStoreUpdate : function()
    {
    },

    getEventStore : function()
    {
        return this.store;
    }
});