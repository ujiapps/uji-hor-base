Ext.define('HOR.view.examenes.PanelCalendarioExamenes',
{
    extend : 'Extensible.calendar.CalendarPanel',
    alias : 'widget.panelCalendarioExamenes',
    region : 'center',
    title : 'Calendari detall',
    depends : [ 'HOR.store.StoreCalendarios', 'HOR.store.StoreEventosDetalle' ],
    calendarStore : Ext.create('HOR.store.StoreCalendarios'),
    eventStore : Ext.create('HOR.store.StoreExamenesPlanificados'),
    editModal : true,
    readOnly : false,
    flex : 1,
    padding : 5,
    showDayView : false,
    showMultiDayView : true,
    showMultiWeekView : true,
    showMonthView : false,
    showWeekView : false,
    activeItem : 1,
    multiDayViewCfg :
    {
        dayCount : 6,
        startDay : 1,
        ddIncrement : 15,
        startDayIsStatic : true,
        showTime : false,
        showMonth : false,
        getStoreParams : function()
        {
            var params = this.getStoreDateParams();
            params.estudioId = this.store.getProxy().extraParams['estudioId'];
            params.cursoId = this.store.getProxy().extraParams['cursoId'];
            params.grupoId = this.store.getProxy().extraParams['grupoId'];
            params.semestreId = this.store.getProxy().extraParams['semestreId'];
            return params;
        }
    },
    multiWeekViewCfg :
    {
        weekCount : 4,
        showTime : false,
        showMonth : true

    },

    limpiaCalendario : function()
    {
        this.store.removeAll(false);
        this.store.getProxy().extraParams['estudioId'] = "";
        this.store.getProxy().extraParams['cursoId'] = "";
        this.store.getProxy().extraParams['grupoId'] = "";
        this.store.getProxy().extraParams['semestreId'] = "";
    },

    getMultiDayText : function()
    {
        return 'Setmana';
    },

    getMultiWeekText : function()
    {
        return 'Mes';
    },

    onStoreUpdate : function()
    {
    },

    initComponent : function()
    {
        Extensible.calendar.template.BoxLayout.override(
            {
                firstWeekDateFormat : 'D j',
                multiDayFirstDayFormat : 'M j, Y',
                multiDayMonthStartFormat : 'M j'
            });

        this.callParent(arguments);

        var calendario = this;

        Extensible.calendar.menu.Event.override(
        {
            buildMenu : function()
            {
                var me = this;

                if (me.rendered)
                {
                    return;
                }
                Ext.apply(me,
                {
                    items : [
                    {
                        text : 'Edital detalls',
                        iconCls : 'extensible-cal-icon-evt-edit',
                        scope : me,
                        handler : function()
                        {
                            calendario.fireEvent('editarDetalleExamen', me, me.rec, me.ctxEl);
                        }
                    },
                    {
                        text : 'Assignar aules',
                        iconCls : 'extensible-cal-icon-evt-edit',
                        scope : me,
                        handler : function()
                        {
                            calendario.fireEvent('asignarAula', me, me.rec, me.ctxEl);
                        }
                    },
                    {
                        text : 'Afegir sessió',
                        iconCls : 'extensible-cal-icon-evt-edit',
                        hidden : false,
                        scope : me,
                        handler : function()
                        {
                            calendario.fireEvent('dividir', me.rec);
                        }
                    },
                    {
                        text : 'Esborrar',
                        iconCls : 'extensible-cal-icon-evt-del',
                        handler : function()
                        {
                            calendario.fireEvent('desplanificar', me.rec);
                        }
                    } ]
                });
            },

            showForEvent : function(rec, el, xy)
            {
                var me = this;
                me.rec = rec;
                me.ctxEl = el;
                me.showAt(xy);
            }
        });
    }

});