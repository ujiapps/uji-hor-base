Ext.define('HOR.view.examenes.VentanaEdicionExamen',
{
    extend : 'Ext.Window',
    title : 'Edició exámen',
    width : 500,
    alias : 'widget.ventanaEdicionExamen',
    modal : true,

    items : [
    {
        xtype : 'form',
        name : 'formEdicionExamen',
        padding : 10,
        enableKeyEvents : true,
        frame : true,
        layout :
        {
            type : 'vbox',
            align : 'stretch'
        },
        fbar : [
        {
            xtype : 'button',
            text : 'Guardar',
            name : 'guardar'
        },
        {
            xtype : 'button',
            text : 'Cancel·lar',
            name : 'cancelar'
        } ],
        items : [
        {
            xtype : 'hiddenfield',
            name : 'examenId'
        },
        {
            xtype : 'datefield',
            fieldLabel : 'Data',
            labelWidth : 75,
            name : 'fecha',
            anchor : '80%'
        },
        {
            xtype : 'combobox',
            fieldLabel : 'Tipus examen',
            labelWidth : 75,
            queryMode : 'local',
            store : 'StoreTiposExamen',
            editable : false,
            displayField : 'nombre',
            valueField : 'id',
            name : 'tipoExamenId',
            anchor : '80%'
        },
        {
            xtype : 'textarea',
            fieldLabel : 'Comentaris (CA)',
            labelWidth : 75,
            name : 'comentario',
            anchor : '80%'
        },
        {
            xtype : 'textarea',
            fieldLabel : 'Comentaris (ES)',
            labelWidth : 75,
            name : 'comentarioES',
            anchor : '80%'
        },
        {
            xtype : 'textarea',
            fieldLabel : 'Comentaris (UK)',
            labelWidth : 75,
            name : 'comentarioUK',
            anchor : '80%'
        },
        {
            xtype : 'checkbox',
            fieldLabel : 'Definitiu',
            labelWidth : 75,
            name : 'definitivo',
            anchor : '80%'
        } ]
    } ],

    setData : function(examen)
    {
        var form = Ext.ComponentQuery.query('form[name=formEdicionExamen]')[0];
        form.getForm().findField('examenId').setValue(examen.get('id'));
        form.getForm().findField('tipoExamenId').setValue(examen.get('tipoExamenId'));
        form.getForm().findField('comentario').setValue(examen.get('comentario'));
        form.getForm().findField('comentarioES').setValue(examen.get('comentarioES'));
        form.getForm().findField('comentarioUK').setValue(examen.get('comentarioUK'));
        form.getForm().findField('fecha').setValue(examen.get('fecha'));
        form.getForm().findField('definitivo').setValue(examen.get('definitivo'));
        this.setTitle(examen.get('nombre'));
    },

    getData : function()
    {
        var form = Ext.ComponentQuery.query('form[name=formEdicionExamen]')[0];
        return form.getForm().getValues();
    }

});