Ext.define('HOR.view.examenes.VentanaAsignacionAula',
{
    extend : 'Ext.Window',
    title : 'Edició exámen',
    width : 500,
    alias : 'widget.ventanaAsignacionAula',
    modal : true,
    closeAction : 'hide',

    items : [
    {
        xtype : 'form',
        name : 'formAsignacionAula',
        padding : 10,
        enableKeyEvents : true,
        frame : true,
        layout :
        {
            type : 'vbox',
            align : 'stretch'
        },
        fbar : [
        {
            xtype : 'button',
            text : 'Guardar',
            name : 'guardar'
        },
        {
            xtype : 'button',
            text : 'Cancel·lar',
            name : 'cancelar'
        } ],
        items : [
        {
            xtype : 'hiddenfield',
            name : 'examenId'
        },
        {
            xtype : 'boxselect',
            editable : true,
            lastQuery : '',
            width : 180,
            labelAlign : 'left',
            margin : '0 20 0 0',
            fieldLabel : 'Aules',
            store : 'StoreAulasEstudio',
            displayField : 'nombre',
            queryMode : 'local',
            valueField : 'id',
            name : 'aulas',
            labelWidth : 35
        } ]
    } ],

    setData : function(examen)
    {
        var form = Ext.ComponentQuery.query('form[name=formAsignacionAula]')[0];

        var aulaIds = examen.get('codigoAulas').split(',').map(function(value)
        {
            return parseInt(value);
        });
        form.getForm().findField('aulas').setValue(aulaIds);
        form.getForm().findField('examenId').setValue(examen.get('id'));

        this.setTitle(examen.get('nombre'));
    },

    getData : function()
    {
        var form = Ext.ComponentQuery.query('form[name=formAsignacionAula]')[0];
        return form.getForm().getValues();
    }

});