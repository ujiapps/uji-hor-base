Ext.define('HOR.view.examenes.FiltroExamenes',
{
    extend : 'Ext.panel.Panel',
    alias : 'widget.filtroExamenes',

    border : false,
    padding : 5,
    closable : false,
    layout : 'anchor',

    items : [
    {
        xtype : 'combobox',
        fieldLabel : 'Titulació',
        labelWidth : 75,
        store : 'StoreEstudios',
        editable : false,
        displayField : 'nombre',
        valueField : 'id',
        name : 'estudio',
        anchor : '80%'
    },
    {
        xtype : 'panel',
        border : 0,
        anchor : '100%',
        layout :
        {
            type : 'hbox',
            align : 'fit'
        },

        items : [
        {
            xtype : 'panel',
            border : 0,
            anchor : '50%',
            layout :
            {
                type : 'hbox',
                align : 'fit'
            },
            defaults :
            {
                xtype : 'combobox',
                editable : false,
                lastQuery : '',
                width : 450,
                labelWidth : 75,
                labelAlign : 'left',
                margin : '0 20 0 0'
            },
            items : [
            {
                fieldLabel : 'Convocatoria',
                store : 'StoreConvocatoria',
                disabled: true,
                displayField : 'nombre',
                valueField : 'id',
                name : 'convocatoria'
            } ]
        },
        {
            xtype : 'panel',
            border : 0,
            anchor : '50%',
            flex : 1,
            layout :
            {
                type : 'hbox',
                align : 'fit',
                pack : 'end'
            },
            defaults :
            {
                width : 120,
                labelWidth : 75,
                labelAlign : 'left',
                margin : '0 20 0 0'
            },
            items : [
            {
                name : 'controles',
                xtype : 'button',
                disabled: true,
                hidden : false,
                margin : '0 0 0 5',
                width : '40',
                flex : 0,
                text : 'Controls',
                iconCls : 'tick'
            },
            {
                name : 'imprimir',
                xtype : 'button',
                disabled: true,
                hidden : false,
                margin : '0 0 0 5',
                width : '40',
                flex : 0,
                text : 'Imprimir',
                iconCls : 'printer'
            } ]
        } ]

    } ],

    getGruposSelected : function()
    {
        var grupos = this.down('combobox[name=grupo]').getValue();

        return grupos.join(';');
    }
});