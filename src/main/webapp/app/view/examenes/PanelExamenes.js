Ext.define('HOR.view.examenes.PanelExamenes',
{
    extend : 'Ext.panel.Panel',
    title : 'Gestió d\'exàmens',
    requires : [ 'HOR.view.examenes.FiltroExamenes', 'HOR.view.examenes.PanelCalendarioExamenes', 'HOR.view.examenes.SelectorExamenes', 'HOR.view.examenes.VentanaEdicionExamen',
            'HOR.view.examenes.VentanaAsignacionAula' ],
    alias : 'widget.panelExamenes',
    closable : true,
    layout :
    {
        type : 'vbox',
        align : 'stretch',
        padding : 5
    },

    items : [
    {
        xtype : 'ventanaEdicionExamen'
    },
    {
        xtype : 'ventanaAsignacionAula'
    },
    {
        xtype : 'filtroExamenes',
        height : 62,
        autoScroll : true
    },
    {
        xtype : 'panel',
        disabled : true,
        name : 'seccionCalendario',
        flex : 1,
        border : 0,
        layout :
        {
            type : 'hbox',
            align : 'stretch'
        },
        items : [
        {
            width : 150,
            border : 0,
            layout :
            {
                type : 'vbox',
                align : 'stretch'
            },
            items : [
            {
                xtype : 'selectorExamenes'
            },
            {
                xtype : 'boxselect',
                editable : true,
                labelAlign : 'top',
                lastQuery : '',
                margin : '5 5 0 5',
                fieldLabel : 'Cursos',
                store : 'StoreCursosTitulacion',
                displayField : 'curso',
                valueField : 'curso',
                name : 'curso'
            } ]
        },
        {
            xtype : 'panel',
            name : 'contenedorCalendario',
            flex : 1,
            border : 0,
            layout :
            {
                type : 'hbox',
                align : 'stretch'
            },
            items : [
            {
                xtype : 'panelCalendarioExamenes',
                flex : 1
            } ]
        } ]
    } ]
});