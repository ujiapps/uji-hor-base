Ext.define('HOR.view.examenes.SelectorExamenes',
{
    extend : 'Ext.panel.Panel',
    alias : 'widget.selectorExamenes',
    title : 'Examens a planificar',
    autoScroll : true,
    padding : 5,
    flex : 1,
    layout :
    {
        type : 'vbox',
        align : 'stretch'
    },

    addExamenes : function(examenes)
    {
        this.removeAll();
        var botones = [];

        for ( var i = 0, len = examenes.length; i < len; i++)
        {
            var button =
            {
                xtype : 'button',
                allowDepress: false,
                text : examenes[i].data.nombre,
                examenId : examenes[i].data.id,
                padding : '2 5 2 5',
                margin : '10 10 0 10',
                toggleHandler : this.onToggleBoton
            };
            botones.push(button);
        }

        this.add(botones);
    }

});