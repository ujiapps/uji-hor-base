Ext.define('HOR.view.logs.PanelLogs',
{
    extend : 'Ext.panel.Panel',
    title : 'Logs del sistema',
    requires : [ 'HOR.view.logs.FiltroLogs', 'HOR.view.logs.GridLogs' ],
    alias : 'widget.panelLogs',
    autoScroll: true,
    closable : true,
    layout :
    {
        type : 'vbox',
        align : 'stretch',
        padding : 5
    },

    items : [
    {
        xtype : 'filtroLogs',
        height : 50
    },
    {
        xtype : 'gridLogs',
        disabled : true
    } ]
});