Ext.define('HOR.view.logs.GridLogs',
{
    extend : 'Ext.grid.Panel',
    alias : 'widget.gridLogs',
    disableSelection : true,
    autoScroll: true,
    title : 'Logs',
    store : 'StoreLogs',

    columns : [
    {
        text : 'Event',
        dataIndex : 'eventoId',
        menuDisabled : true
    },
    {
        text : 'Usuari',
        dataIndex : 'usuario',
        menuDisabled : true
    },
    {
        text : 'Data',
        dataIndex : 'fecha',
        menuDisabled : true
    },
    {
        text : 'Acció',
        dataIndex : 'tipoAccionNombre',
        menuDisabled : true
    },
    {
        text : 'Descripció',
        dataIndex : 'descripcion',
        menuDisabled : true,
        flex : 1
    } ]
});