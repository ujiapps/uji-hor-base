Ext.define('HOR.view.logs.FiltroLogs',
{
    extend : 'Ext.panel.Panel',
    alias : 'widget.filtroLogs',

    border : false,
    padding : 5,
    closable : false,
    anchor : '100%',

    layout :
    {
        type : 'hbox',
        align : 'fit'
    },

    defaults :
    {
        labelWidth : 60,
        labelAlign : 'left',
        margin : '0 20 0 0'
    },

    items : [
    {
        xtype : 'datefield',
        fieldLabel : 'Data',
        name : 'fecha',
        submitFormat : 'd/m/Y'
    },
    {
        xtype : 'combobox',
        editable : false,
        lastQuery : '',
        fieldLabel : 'Event',
        displayField : 'name',
        valueField : 'id',
        name : 'eventoId',
        disabled : true,
        store : 'StoreEventosLogs'
    } ]
});