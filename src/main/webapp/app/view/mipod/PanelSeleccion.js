Ext.define('HOR.view.mipod.PanelSeleccion',
{
    extend : 'Ext.panel.Panel',
    title : 'El meu POD',
    alias : 'widget.panelSeleccionPOD',
    requires : [ 'HOR.view.commons.GroupedComboBox' ],
    closable : true,
    autoHeight : true,
    margin : 10,
    border : 0,
    layout :
    {
        type : 'vbox',
        align : 'fit',
        padding : 5
    },

    items : [
    {
        xtype : 'panel',
        border : 0,
        layout :
        {
            type : 'hbox'
        },
        items : [
        {
            border : 0,
            xtype : 'groupedComboBox',
            editable : false,
            labelWidth : 90,
            labelAlign : 'right',
            margin : '0 0 5 0',
            fieldLabel : 'Departament',
            store : 'StoreDepartamentosMiPOD',
            displayField : 'nombre',
            groupField : "centroId",
            groupDisplayField : "centroNombre",
            valueField : 'id',
            name : 'departamento',
            width : 420,
            matchFieldWidth : false,
            queryMode : 'local',
            listConfig :
            {
                width : 640
            }
        },
        {
            xtype : 'button',
            margin : '0 0 0 10',
            text : 'Consultar POD',
            hidden: true,
            disabled: true,
            name : 'descargar-pod-departamento'
        } ]
    },
    {
        xtype : 'panel',
        border : 0,
        layout :
        {
            type : 'hbox'
        },
        items : [
        {

            border : 0,
            xtype : 'combobox',
            editable : false,
            disabled : true,
            labelWidth : 90,
            labelAlign : 'right',
            margin : '0 0 5 0',
            fieldLabel : 'Area',
            queryMode : 'local',
            store : 'StoreAreasMiPOD',
            displayField : 'nombre',
            valueField : 'id',
            name : 'area',
            width : 420,
            matchFieldWidth : false,
            listConfig :
            {
                width : 640
            }
        },
        {
            xtype : 'button',
            hidden: false,
            disabled: true,
            margin : '0 0 0 10',
            text : 'Consultar POD',
            name : 'descargar-pod-area'
        },
        {
            xtype : 'button',
            hidden: false,
            disabled: true,
            margin : '0 0 0 10',
            text : 'Consultar POD per Asignatura Simplificat',
            name : 'descargar-pod-asignatura'
        } ]
    },

    {
        xtype : 'panel',
        border : 0,
        layout :
        {
            type : 'hbox'
        },
        items : [
        {

            border : 0,
            xtype : 'combobox',
            editable : false,
            labelWidth : 90,
            labelAlign : 'right',
            margin : '0 0 5 0',
            fieldLabel : 'Professor',
            queryMode : 'local',
            store : 'StoreProfesoresMiPOD',
            displayField : 'nombre',
            valueField : 'id',
            name : 'profesor',
            disabled : true,
            width : 420
        },
        {
            xtype : 'button',
            margin : '0 0 0 10',
            text : 'Consultar POD',
            disabled: true,
            name : 'descargar-pod-profesor'
        } ]
    } ]
});