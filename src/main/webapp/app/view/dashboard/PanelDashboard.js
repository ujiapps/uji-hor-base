Ext.define('HOR.view.dashboard.PanelDashboard',
{
    extend : 'Ext.panel.Panel',
    alias : 'widget.dashboard',
    title : 'Dashboard',
    autoScroll : true,
    frame : true,
    closable : false,
    layout :
    {
        type : 'vbox',
        width : '100%',
        align : 'center'
    },

    items : [
            {
                xtype : 'label',
                width : '100%',
                text : 'Aplicació de Gestió d\'Horaris Acadèmics i POD',
                style : 'font-size : 30px; font-weight : bold; text-align : center; margin-top : 30px'
            },
            {
                xtype : 'label',
                width : '100%',
                text : 'Versió ' + appversion,
                style : 'font-size : 12px; text-align : center'
            },
            {
                xtype : 'dataview',
                margins : '50px',
                store : 'StoreInformaciones',
                itemSelector : 'div:first-child',
                tpl : new Ext.XTemplate('<tpl for=".">', '<div style="width:600px;margin:10px;padding:5px;text-align:justify;">', '<span>{texto}</span>', '<hr style="margin-top:20px;width:300px" />',
                        '</div>', '</tpl>')
            } ]
});
