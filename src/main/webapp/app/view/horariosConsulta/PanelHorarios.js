Ext.define('HOR.view.horariosConsulta.PanelHorarios',
{
    extend : 'Ext.panel.Panel',
    title : 'Consulta d\'horaris',
    requires : [ 'HOR.view.horariosConsulta.FiltroGrupos', 'HOR.view.horariosConsulta.PanelCalendario', 'HOR.view.horariosConsulta.SelectorGrupos',
            'HOR.view.horariosConsulta.SelectorCalendarios', 'HOR.view.commons.MenuSuperior', 'HOR.view.horariosConsulta.SelectorCursoAgrupacion' ],
    alias : 'widget.panelHorariosConsulta',
    closable : true,
    layout :
    {
        type : 'vbox',
        align : 'stretch',
        padding : 5
    },

    items : [
    {
        xtype : 'filtroGruposConsulta',
        height : 62
    },
    {
        xtype : 'panel',
        flex : 1,
        border : 0,
        layout :
        {
            type : 'hbox',
            align : 'stretch'
        },
        items : [
        {
            width : 150,
            border : 0,
            layout :
            {
                type : 'vbox',
                align : 'stretch'
            },
            items : [
            {
                xtype : 'selectorGruposConsulta'
            },
            {
                xtype : 'selectorCalendariosConsulta'
            } ]
        },
        {
            xtype : 'panel',
            name : 'contenedorCalendario',
            flex : 1,
            layout : 'fit'
        } ]
    } ]
});