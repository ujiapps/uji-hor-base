Ext.define('HOR.view.horariosConsulta.PanelCalendario',
{
    extend : 'Extensible.calendar.CalendarPanel',
    alias : 'widget.panelCalendarioConsulta',
    region : 'center',
    title : 'Calendari',
    depends : [ 'HOR.store.StoreCalendarios', 'HOR.store.StoreEventos' ],
    requires : [ 'HOR.view.aulas.asignacion.FormAsignacionAulas' ],
    calendarStore : Ext.create('HOR.store.StoreCalendarios'),
    eventStore : Ext.create('HOR.store.StoreEventos'),
    editModal : false,
    enableEditDetails : false,
    flex : 1,
    padding : 5,
    showTodayText : false,
    showNavToday : false,
    showDayView : false,
    showWeekView : false,
    showMonthView : false,
    weekViewCfg :
    {
        dayCount : 5,
        startDay : 1,
        startDayIsStatic : true
    },
    showMultiDayView : true,
    showMultiWeekView : false,
    showNavJump : false,
    showNavNextPrev : false,
    multiDayViewCfg :
    {
        dayCount : 5,
        startDay : 1,
        ddIncrement : 15,
        startDayIsStatic : true,
        showTime : false,
        showMonth : false,
        getStoreParams : function()
        {
            var params = this.getStoreDateParams();
            params.estudioId = this.store.getProxy().extraParams['estudioId'];
            params.cursoId = this.store.getProxy().extraParams['cursoId'];
            params.grupoId = this.store.getProxy().extraParams['grupoId'];
            params.semestreId = this.store.getProxy().extraParams['semestreId'];
            return params;
        }
    },

    getMultiDayText : function()
    {
        return 'Setmana genèrica';
    },

    limpiaCalendario : function()
    {
        this.store.removeAll(false);
    },

    initComponent : function()
    {
        this.asignaEtiquetasDiasSemanaACalendario();

        this.callParent(arguments);

        this.activaMenuContextualYEdicionDetalle();
    },

    asignaEtiquetasDiasSemanaACalendario : function()
    {
        Extensible.calendar.template.BoxLayout.override(
        {
            firstWeekDateFormat : 'l',
            multiDayFirstDayFormat : 'l',
            multiDayMonthStartFormat : 'l'
        });

    },

    activaMenuContextualYEdicionDetalle : function()
    {
        Extensible.calendar.menu.Event.override(
        {
            buildMenu : function()
            {
                var me = this;

                if (me.rendered)
                {
                    return;
                }
                Ext.apply(me,
                {
                    items : []
                });
            },

            showForEvent : function(rec, el, xy)
            {
                var me = this;
                me.rec = rec;
                me.ctxEl = el;
                me.showAt(xy);
            }
        });

        Extensible.calendar.form.EventWindow.override(
        {
            getFooterBarConfig : function()
            {
                var cfg = [ '->',
                {
                    text : this.saveButtonText,
                    itemId : this.id + '-save-btn',
                    disabled : false,
                    handler : this.onSave,
                    scope : this
                },
                {
                    text : this.deleteButtonText,
                    itemId : this.id + '-delete-btn',
                    disabled : false,
                    handler : this.onDelete,
                    scope : this,
                    hideMode : 'offsets' // IE requires this
                },
                {
                    text : this.cancelButtonText,
                    itemId : this.id + '-cancel-btn',
                    disabled : false,
                    handler : this.onCancel,
                    scope : this
                } ];

                if (this.enableEditDetails !== false)
                {
                    cfg.unshift(
                    {
                        xtype : 'tbtext',
                        itemId : this.id + '-details-btn',
                        text : '<a href="#" class="' + this.editDetailsLinkClass + '">' + this.detailsLinkText + '</a>'
                    });
                }
                return cfg;

            }
        });
    },

    onStoreUpdate : function()
    {
    },

    getEventStore : function()
    {
        return this.store;
    },

    updateEventManually : function(rec)
    {
        this.onEventUpdate(null, rec);
    }
});