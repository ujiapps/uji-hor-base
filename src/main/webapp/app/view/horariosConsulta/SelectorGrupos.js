Ext.define('HOR.view.horariosConsulta.SelectorGrupos',
{
    extend : 'Ext.panel.Panel',
    alias : 'widget.selectorGruposConsulta',
    title : 'Sense horari assignat',
    autoScroll : true,
    padding : 5,
    flex : 1,
    layout :
    {
        type : 'vbox',
        align : 'stretch'
    },
    
    limpiaGrupos: function() {
        this.removeAll();
    }
});