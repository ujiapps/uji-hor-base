Ext.define('HOR.view.horariosConsulta.FiltroGrupos',
{
    extend : 'Ext.panel.Panel',
    alias : 'widget.filtroGruposConsulta',

    border : false,
    padding : 5,
    closable : false,
    layout : 'anchor',
    items : [
    {
        xtype : 'panel',
        border : 0,
        margin : '0 0 5 0',
        anchor : '100%',
        layout :
        {
            type : 'hbox',
            align : 'fit'
        },
        items : [
        {
            xtype : 'combobox',
            fieldLabel : 'Titulació',
            labelWidth : 55,
            store : 'StoreEstudiosAbiertos',
            editable : false,
            displayField : 'nombre',
            valueField : 'id',
            name : 'estudio',
            width : 310,
            matchFieldWidth : false,
            listConfig :
            {
                width : 600
            }
        },
        {
            xtype : 'selectorCursoAgrupacionConsulta',
            border : 0,
            editable : false,
            labelWidth : 120,
            labelAlign : 'right',
            margin : 0,
            fieldLabel : 'Curs / Agrupació',
            store : Ext.create('HOR.store.StoreCursosAgrupaciones'),
            displayField : 'nombre',
            valueField : 'valor',
            groupField : "tipoId",
            groupDisplayField : "tipoNombre",
            name : 'cursoAgrupacion',
            width : 300,
            matchFieldWidth : false,
            listConfig :
            {
                width : 360
            },
            disabled : true,
            queryMode : 'local'
        },
        {
            xtype : 'panel',
            border : 0,
            anchor : '50%',
            flex : 1,
            layout :
            {
                type : 'hbox',
                align : 'fit',
                pack : 'end'
            },
            defaults :
            {
                width : 120,
                labelWidth : 75,
                labelAlign : 'left',
                margin : '0 20 0 0'
            },
            items : [
            {
                name : 'menu-imprimir',
                xtype : 'button',
                hidden : false,
                margin : '0 0 0 5',
                text : 'Imprimir',
                iconCls : 'printer',
                menu :
                {
                    xtype : 'menu',
                    border : false,
                    plain : true,
                    items : [
                    {
                        name : 'imprimirSemestre',
                        flex : 0,
                        text : 'Imprimir Semestre',
                        iconCls : 'tick'
                    },
                    {
                        name : 'imprimirTitulacion',
                        flex : 0,
                        text : 'Imprimir Cursos',
                        iconCls : 'tick'
                    },
                    {
                        name : 'imprimirAgrupaciones',
                        flex : 0,
                        text : 'Imprimir Agrupacions',
                        iconCls : 'tick'
                    },
                    {
                        name : 'imprimirSesionesPOD',
                        flex : 0,
                        text : 'Imprimir Sessions POD',
                        iconCls : 'tick'
                    } ]
                }
            } ]
        } ]
    },
    {
        xtype : 'panel',
        border : 0,
        anchor : '100%',
        layout :
        {
            type : 'hbox',
            align : 'fit'
        },

        items : [
        {
            xtype : 'panel',
            border : 0,
            anchor : '50%',
            layout :
            {
                type : 'hbox',
                align : 'fit'
            },
            defaults :
            {
                xtype : 'combobox',
                editable : false,
                lastQuery : '',
                width : 110,
                labelWidth : 55,
                labelAlign : 'left',
                margin : '0 20 0 0'
            },
            items : [
            {
                fieldLabel : 'Semestre',
                store : 'StoreSemestresTodos',
                displayField : 'id',
                valueField : 'id',
                name : 'semestre',
                disabled : true
            },
            {
                xtype : 'boxselect',
                editable : true,
                lastQuery : '',
                width : 180,
                labelAlign : 'left',
                margin : '0 20 0 0',
                fieldLabel : 'Grup',
                store : Ext.create('HOR.store.StoreGrupos'),
                displayField : 'grupo',
                valueField : 'grupo',
                name : 'grupo',
                labelWidth : 35,
                disabled : true
            },
            {
                xtype : 'boxselect',
                editable : true,
                lastQuery : '',
                width : 280,
                labelAlign : 'left',
                margin : '0 20 0 0',
                fieldLabel : 'Assignatura',
                store : Ext.create('HOR.store.StoreAsignaturasEstudio'),
                displayField : 'asignaturaId',
                valueField : 'asignaturaId',
                name : 'asignatura',
                labelWidth : 100,
                disabled : true
            } ]
        },
        {
            xtype : 'panel',
            border : 0,
            anchor : '50%',
            flex : 1,
            layout :
            {
                type : 'hbox',
                align : 'fit',
                pack : 'end'
            },
            defaults :
            {
                width : 120,
                labelWidth : 75,
                labelAlign : 'left',
                margin : '0 20 0 0'
            },
            items : [
            {
                margin : '0 0 0 10',
                name : 'calendarioDetalle',
                xtype : 'button',
                enableToggle : true,
                hidden : true,
                width : '40',
                flex : 0,
                text : 'Set. detallada',
                iconCls : 'calendar-week'
            },
            {
                name : 'calendarioGenerica',
                xtype : 'button',
                enableToggle : true,
                pressed : true,
                hidden : true,
                margin : '0 0 0 5',
                width : '40',
                flex : 0,
                text : 'Set. genèrica',
                iconCls : 'calendar-edit'
            } ]
        } ]

    } ],

    getGruposSelected : function()
    {
        var grupos = this.down('combobox[name=grupo]').getValue();

        return grupos.join(';');
    },

    getAsignaturasSelected : function()
    {
        var asignaturas = this.down('combobox[name=asignatura]').getValue();

        return asignaturas.join(';');
    }
});