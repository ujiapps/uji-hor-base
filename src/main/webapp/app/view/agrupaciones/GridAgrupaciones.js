Ext.define('HOR.view.agrupaciones.GridAgrupaciones',
{
    extend : 'Ext.ux.uji.grid.Panel',
    requires : [],
    store : 'StoreAgrupaciones',
    alias : 'widget.gridAgrupaciones',
    sortableColumns : false,
    columns : [
    {
        text : 'Id',
        dataIndex : 'id',
        width: 40
    },
    {
        text : 'Nom',
        dataIndex : 'nombre',
        flex : 5,
        editor : {
            xtype: 'textfield',
            allowBlank : false
        }
    } ]
});
