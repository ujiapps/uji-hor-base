Ext.define('HOR.view.agrupaciones.TreePanelAsignaturas',
{
    extend : 'Ext.tree.Panel',
    title : 'Assignatures disponibles',
    alias : 'widget.treePanelAsignaturas',
    rootVisible : false,
    selModel :
    {
        mode : 'MULTI'
    },
    store : 'TreeStoreAsignaturas',
    tbar : [
    {
        xtype : 'button',
        text : 'Afegir',
        name : 'anyadir',
        iconCls : 'application-add'
    } ]

});