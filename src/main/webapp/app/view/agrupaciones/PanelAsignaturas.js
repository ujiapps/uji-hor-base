Ext.define('HOR.view.agrupaciones.PanelAsignaturas',
{
    extend : 'Ext.panel.Panel',
    requires : [],
    alias : 'widget.panelAsignaturas',
    disabled : true,
    layout :
    {
        type : 'hbox',
        align : 'stretch'
    },
    items : [
    {
        xtype : 'treePanelAsignaturas',
        flex : 1
    },
    {
        xtype : 'gridAsignaturasAgrupacion',
        flex : 1
    } ]
});