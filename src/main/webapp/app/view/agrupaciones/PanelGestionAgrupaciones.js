Ext.define('HOR.view.agrupaciones.PanelGestionAgrupaciones',
{
    extend : 'Ext.panel.Panel',
    alias : 'widget.panelGestionAgrupaciones',
    border : 0,
    padding: '5 5 0 5',
    autoScroll : true,
    layout :
    {
        type : 'vbox',
        align : 'stretch'
    },
    items : [
    {
        name : 'add-agrupacion',
        xtype : 'button',
        margin : '0 0 5 0',
        width : '40',
        flex : 0,
        text : 'Crear agrupació',
        iconCls : 'application-add',
        disabled : true
    },
    {
        name : 'edit-agrupacion',
        xtype : 'button',
        margin : '0 0 5 0',
        width : '40',
        flex : 0,
        text : 'Editar agrupació',
        iconCls : 'application-edit',
        disabled : true
    },
    {
        name : 'delete-agrupacion',
        xtype : 'button',
        margin : '0 0 5 0',
        width : '40',
        flex : 0,
        text : 'Esborrar agrupació',
        iconCls : 'application-delete',
        disabled : true
    } ]
});