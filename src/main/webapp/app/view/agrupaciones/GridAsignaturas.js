Ext.define('HOR.view.agrupaciones.GridAsignaturas',
{
    extend : 'Ext.grid.Panel',
    title : 'Assignatures assignades',
    alias : 'widget.gridAsignaturasAgrupacion',
    store : 'StoreAsignaturasAgrupacion',
    disableSelection : false,
    selModel :
    {
        mode : 'MULTI'
    },
    sortableColumns : true,
    tbar : [
    {
        xtype : 'button',
        name : 'borrar',
        text : 'Esborrar',
        iconCls : 'application-delete'
    } ],
    columns : [
    {
        text : 'Codi',
        dataIndex : 'asignaturaId',
        width : 90
    },
    {
        text : 'Assignatura',
        dataIndex : 'asignaturaNombre',
        flex : 1
    },
    {
        text : 'Curs',
        dataIndex : 'asignaturaCurso',
        width : 60
    },
    {
        text : 'Subgrup',
        dataIndex : 'subgrupo',
        width : 60
    } ]
});