Ext.define('HOR.view.agrupaciones.SelectorAgrupaciones',
{
    extend : 'Ext.panel.Panel',
    alias : 'widget.selectorAgrupaciones',
    title : 'Agrupacions',
    autoScroll : true,
    padding : 5,
    flex : 1,
    layout :
    {
        type : 'vbox',
        align : 'stretch'
    },

    addAgrupaciones : function(agrupaciones)
    {
        var ref = this;
        this.removeAll();
        var botones = [];
        for (var i = 0, len = agrupaciones.length; i < len; i++)
        {
            var agrupacion = agrupaciones[i];

            var nombre = agrupacion.get('nombre');

            var estudiosCompartidos = (agrupacion.get('estudiosCompartidosNombre') instanceof Array) ? agrupacion.get('estudiosCompartidosNombre') : [ agrupacion.get('estudiosCompartidosNombre') ];

            if (estudiosCompartidos.length > 0)
            {
                nombre += ' (C)';
            }

            var button =
            {
                xtype : 'button',
                allowDepress : false,
                text : nombre,
                padding : '2 5 2 5',
                margin : '10 10 0 10',
                agrupacionId : agrupacion.get('id'),
                estudiosCompartidos : estudiosCompartidos,
                enableToggle : true,
                toggleGroup : 'agrupaciones',
                toggleHandler : this.onToggleBoton,
                listeners :
                {
                    mouseover : function(event, target, options)
                    {
                        ref.updateTooltip(event);
                    },
                    mouseout : function(event, target, options)
                    {
                        if (ref.tooltip) {
                            ref.tooltip.hide();
                        }
                    }
                }
            };

            botones.push(button);
        }

        this.add(botones);
    },

    updateTooltip : function(event)
    {
        if (event.disabled)
        {
            this.tooltip.hide();
            return;
        }
        if (!this.tooltip)
        {
            this.tooltip = Ext.create('Ext.tip.ToolTip',
                {
                    hidden : true,
                    target : this.getEl(),
                    trackMouse : true,
                    autoHide : false
                });
        }

        var msg = event.text;
        if (event.estudiosCompartidos &&  event.estudiosCompartidos.length > 0)
        {
            msg += ', compartit amb:<br/>';

            for (var index in event.estudiosCompartidos)
            {
                msg = msg + event.estudiosCompartidos[index] + '<br/>';
            }
        }

        this.tooltip.update(msg);
        this.tooltip.show();
    },

    disableTooltips : function()
    {
        if (this.tooltip)
        {
            this.tooltip.destroy();
            delete this.tooltip;
        }
    },

    getValue : function()
    {
        var botones = Ext.ComponentQuery.query('selectorAgrupaciones button');
        for (var i = 0; i < botones.length; i++)
        {
            if (botones[i].pressed == true)
            {
                return botones[i].agrupacionId;
            }
        }
        return null;
    },

    onToggleBoton : function(boton)
    {
        if (boton.pressed)
        {
            this.up('panel').fireEvent('agrupacionSelected', boton);
        }
    },

    selecciona : function(agrupacionId)
    {
        var botones = Ext.ComponentQuery.query('selectorAgrupaciones button');
        for (var i = 0; i < botones.length; i++)
        {
            if (botones[i].agrupacionId == agrupacionId)
            {
                return botones[i].toggle(true);
            }
        }
    },

    limpiaGrupos : function()
    {
        this.removeAll();
    }
});