Ext.define('HOR.view.agrupaciones.VentanaEdicionAgrupaciones',
{
    extend : 'Ext.Window',
    title : 'Edició de agrupacions',
    alias : 'widget.ventanaEdicionAgrupaciones',
    layout : 'fit',
    modal : true,

    buttonAlign : 'right',
    bbar : [
    {
        xtype : 'tbfill'
    },
    {
        xtype : 'button',
        text : 'Guardar',
        action : 'guardar'
    },
    {
        xtype : 'button',
        text : 'Cancel·lar',
        action : 'cancelar'
    } ],

    items : [
    {
        xtype : 'form',
        name : 'formEdicionAgrupaciones',
        padding : 10,
        items : [
        {
            xtype : 'textfield',
            fieldLabel : 'Nom agrupació',
            name : 'nombre',
            allowBlank : false,
            padding : 10,
            width : 500
        },
        {
            xtype : 'combobox',
            editable : false,
            lastQuery : '',
            multiSelect : true,
            fieldLabel : 'Estudis compartits',
            store : 'StoreEstudiosCompartidos',
            displayField : 'nombre',
            valueField : 'id',
            name : 'estudios-compartidos',
            padding : 10,
            width : 500
        },
        {
            xtype : 'hidden',
            name : 'id-agrupacion'
        } ]
    } ],

    getEstudiosCompartidosSelected : function()
    {
        var grupos = this.down('combobox[name=estudios-compartidos]').getValue();

        return grupos.join(';');
    }
});