Ext.define('HOR.view.agrupaciones.PanelAgrupaciones',
{
    extend : 'Ext.panel.Panel',
    title : 'Gestió d\'agrupacions',
    requires : [ 'HOR.view.agrupaciones.FiltroAgrupaciones', 'HOR.view.agrupaciones.PanelGestionAgrupaciones', 'HOR.view.agrupaciones.SelectorAgrupaciones', 'HOR.view.agrupaciones.GridAgrupaciones',
            'HOR.view.agrupaciones.PanelAsignaturas', 'HOR.view.agrupaciones.GridAsignaturas', 'HOR.view.agrupaciones.TreePanelAsignaturas' ],
    alias : 'widget.panelAgrupaciones',
    closable : true,

    layout :
    {
        type : 'vbox',
        align : 'stretch'
    },
    items : [
    {
        xtype : 'filtroAgrupaciones',
        height : 60
    },
    {
        xtype : 'panel',
        flex : 1,
        border : 0,
        layout :
        {
            type : 'hbox',
            align : 'stretch'
        },
        items : [
        {
            width : 150,
            border : 0,

            layout :
            {
                type : 'vbox',
                align : 'stretch'
            },
            items : [
            {
                xtype : 'panelGestionAgrupaciones'
            },
            {
                xtype : 'selectorAgrupaciones'
            } ]
        },
        {
            xtype : 'panelAsignaturas',
            flex : 1
        } ]
    } ]
});