Ext.define('HOR.view.agrupaciones.FiltroAgrupaciones',
{
    extend : 'Ext.panel.Panel',
    alias : 'widget.filtroAgrupaciones',

    border : false,
    padding : 5,
    closable : false,
    layout : 'anchor',

    items : [
    {
        border : 0,
        xtype : 'combobox',
        editable : false,
        labelWidth : 60,
        labelAlign : 'left',
        margin : '0 20 0 0',
        fieldLabel : 'Titulacio',
        store : 'StoreEstudios',
        displayField : 'nombre',
        valueField : 'id',
        name : 'estudio',
        width : 500
    } ]
});