Ext.define('HOR.view.fechas.GridLogsEvento',
{
    extend : 'Ext.grid.Panel',
    store : 'StoreLogsEvento',
    alias : 'widget.gridLogsEvento',
    sortableColumns : false,
    columns : [
    {
        text : 'Data',
        dataIndex : 'fecha',
        flex : 2
    },
    {
        text : 'Usuari',
        dataIndex : 'usuario',
        flex : 1
    },
    {
        text : 'Descripció',
        dataIndex : 'descripcion',
        flex : 11
    } ]
});
