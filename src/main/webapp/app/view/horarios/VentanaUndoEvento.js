Ext.define('HOR.view.examenes.VentanaUndoEvento', {
    extend : 'Ext.Window',
    title : 'Històric de canvis que es poden desfer',
    width : 1000,
    height : 400,
    alias : 'widget.ventanaUndoEvento',

    requires : [ 'HOR.view.horarios.GridUndoEvento' ],

    modal : true,
    closeAction : 'hide',

    layout :
    {
        type : 'vbox',
        align : 'stretch'
    },
    items : [
    {
        xtype : 'panel',
        html : 'Aquestes són les accions que has fet que es poden desfer ordenades les més recents primer. Per desfer un canvi has de fer clic sobre el botó "Desfer últim canvi"',
        border: 0,
        padding: 5
    },
    {
        xtype : 'gridUndoEvento',
        flex : 1
    } ],
    buttons : [
    {
        text : 'Desfer últim canvi',
        name : 'btnDeshacer'
    },
    {
        text : 'Tancar',
        name : 'btnCerrar'
    } ]
});