Ext.define('HOR.view.fechas.GridUndoEvento',
{
    extend : 'Ext.grid.Panel',
    store : 'StoreDeshacer',
    alias : 'widget.gridUndoEvento',
    sortableColumns : false,
    viewConfig: {
        emptyText: 'No hi cap acció per desfer'
    },
    columns : [
    {
        text : 'Data',
        dataIndex : 'fecha',
        flex : 2
    },
    {
        text : 'Acció',
        dataIndex : 'tipoAccionNombre',
        menuDisabled : true
    },
    {
        text : 'Usuari',
        dataIndex : 'usuario',
        flex : 1
    },
    {
        text : 'Descripció',
        dataIndex : 'descripcion',
        flex : 11
    } ],
});
