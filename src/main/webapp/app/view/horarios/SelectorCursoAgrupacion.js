Ext.define('HOR.view.horarios.SelectorCursoAgrupacion',
{
    extend : 'HOR.view.commons.GroupedComboBox',
    alias : 'widget.selectorCursoAgrupacion',

    requires : [ 'HOR.view.commons.GroupedComboBox' ],

    getCursoId : function()
    {
        var selectedValue = this.getValue();
        var record = this.getStore().findRecord('valor', selectedValue);
        return (record !== null && record.get('tipoId') === 1) ? selectedValue : null;
    },

    getAgrupacionId : function()
    {
        var selectedValue = this.getValue();
        var record = this.getStore().findRecord('valor', selectedValue);
        return (record !== null && record.get('tipoId') === 2) ? selectedValue : null;
    }
});