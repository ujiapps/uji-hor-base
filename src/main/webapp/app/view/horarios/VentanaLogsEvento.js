Ext.define('HOR.view.examenes.VentanaLogsEvento',
{
    extend : 'Ext.Window',
    title : 'Historial de canvis',
    width : 1000,
    height : 400,
    alias : 'widget.ventanaLogsEvento',

    requires : [ 'HOR.view.horarios.GridLogsEvento' ],

    modal : true,
    closeAction : 'hide',

    layout :
    {
        type : 'vbox',
        align : 'stretch'
    },
    items : [
    {
        xtype : 'gridLogsEvento',
        flex : 1
    } ],
    buttons : [
    {
        text : 'Tancar',
        name : 'btnCerrar'
    } ]

});