package es.uji.apps.hor.builders;

import es.uji.apps.hor.dao.AulaPersonaDAO;
import es.uji.apps.hor.model.Aula;
import es.uji.apps.hor.model.Centro;

public class AulaPersonaBuilder
{
    private Long personaId;
    private Aula aula;
    private Centro centro;
    private AulaPersonaDAO aulaPersonaDAO;

    public AulaPersonaBuilder(AulaPersonaDAO aulaPersonaDAO)
    {
        this.aulaPersonaDAO = aulaPersonaDAO;
    }

    public AulaPersonaBuilder()
    {
        this(null);
    }

    public AulaPersonaBuilder withAula(Aula aula)
    {
        this.aula = aula;
        return this;
    }

    public AulaPersonaBuilder withCentro(Centro centro)
    {
        this.centro = centro;
        return this;
    }

    public AulaPersonaBuilder withPersonaId(Long personaId)
    {
        this.personaId = personaId;
        return this;
    }


    public Aula build()
    {
        if (aulaPersonaDAO != null)
        {
            aula = aulaPersonaDAO.insertAulaPersona(personaId, aula, centro);
        }

        return aula;
    }

}
