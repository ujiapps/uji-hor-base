package es.uji.apps.hor.builders;

import es.uji.apps.hor.dao.ItemsAsignaturaDAO;
import es.uji.apps.hor.model.Asignatura;

public class ItemAsignaturaBuilder
{
    private Asignatura asignatura;
    private ItemsAsignaturaDAO itemAsignaturaDAO;
    private Long itemId;

    public ItemAsignaturaBuilder(ItemsAsignaturaDAO itemAsignaturaDAO)
    {
        this.itemAsignaturaDAO = itemAsignaturaDAO;
        asignatura = new Asignatura();
    }

    public ItemAsignaturaBuilder()
    {
        this(null);
    }

    public ItemAsignaturaBuilder withItemId(Long itemId) {
        this.itemId = itemId;
        return this;
    }

    public ItemAsignaturaBuilder withAsignatura(Asignatura asignatura)
    {
        this.asignatura = asignatura;
        return this;
    }

    public Asignatura build()
    {
        if (itemAsignaturaDAO != null)
        {
            asignatura = itemAsignaturaDAO.insertItemAsignatura(asignatura, itemId);
        }

        return asignatura;
    }

}
