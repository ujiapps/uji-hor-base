package es.uji.apps.hor.dao;

import es.uji.apps.hor.builders.CentroBuilder;
import es.uji.apps.hor.builders.DepartamentoBuilder;
import es.uji.apps.hor.builders.PersonaBuilder;
import es.uji.apps.hor.model.Centro;
import es.uji.apps.hor.model.Departamento;
import es.uji.commons.rest.json.lookup.LookupItem;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.Assert.assertTrue;

@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext-test.xml"})
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = false)
public class PersonaSearchDAOTest
{
    @Autowired
    private PersonaDAO personaDAO;

    @Autowired
    private DepartamentoDAO departamentoDAO;

    @Autowired
    private CentroDAO centroDAO;

    @Before
    @Transactional
    public void init()
    {
        Centro centro = new CentroBuilder(centroDAO).
                withNombre("Centro 1")
                .build();

        Departamento departamento = new DepartamentoBuilder(departamentoDAO).
                withNombre("Departamento 1").
                withCentro(centro).
                build();

        new PersonaBuilder(personaDAO).
                withNombre("José").
                withDepartamento(departamento).
                withCentroAutorizado(centro).
                build();
    }

    @Test
    public void buscaValorConAcentosIgualQueElOriginal()
    {
        List<LookupItem> searchResult = personaDAO.search("José");
        assertTrue("No se han encontrado personas buscando con el mismo valor que el original",
                   searchResult.size() > 0);
    }
    
    @Test
    public void busquedaConAcentosLimpiadosSobreOriginalConAcentos()
    {
        List<LookupItem> searchResult = personaDAO.search("jose");
        assertTrue("No se han encontrado personas buscando con el texto limpiado", searchResult.size() > 0);
    }
}
