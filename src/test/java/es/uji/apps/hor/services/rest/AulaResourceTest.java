package es.uji.apps.hor.services.rest;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.core.util.StringKeyStringValueIgnoreCaseMultivaluedMap;
import es.uji.apps.hor.builders.AreaEdificioBuilder;
import es.uji.apps.hor.builders.AulaBuilder;
import es.uji.apps.hor.builders.AulaPersonaBuilder;
import es.uji.apps.hor.builders.AulaPlanificacionBuilder;
import es.uji.apps.hor.builders.CentroBuilder;
import es.uji.apps.hor.builders.EdificioBuilder;
import es.uji.apps.hor.builders.EstudioBuilder;
import es.uji.apps.hor.builders.PlantaEdificioBuilder;
import es.uji.apps.hor.builders.TipoAulaBuilder;
import es.uji.apps.hor.builders.TipoEstudioBuilder;
import es.uji.apps.hor.dao.AulaDAO;
import es.uji.apps.hor.dao.AulaPersonaDAO;
import es.uji.apps.hor.dao.CentroDAO;
import es.uji.apps.hor.dao.EstudiosDAO;
import es.uji.apps.hor.dao.TipoEstudioDAO;
import es.uji.apps.hor.model.AreaEdificio;
import es.uji.apps.hor.model.Aula;
import es.uji.apps.hor.model.Centro;
import es.uji.apps.hor.model.Edificio;
import es.uji.apps.hor.model.Estudio;
import es.uji.apps.hor.model.PlantaEdificio;
import es.uji.apps.hor.model.Semestre;
import es.uji.apps.hor.model.TipoAula;
import es.uji.apps.hor.model.TipoEstudio;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

@Ignore
public class AulaResourceTest extends AbstractRestTest
{
    private Long centroId;
    private String edificio;
    private Semestre semestre;
    private TipoEstudio tipoEstudio;
    private Estudio estudio;
    private String tipoAula;
    private String plantaEdificio;

    @Autowired
    private AulaDAO aulaDAO;

    @Autowired
    private CentroDAO centroDAO;

    @Autowired
    private AulaPersonaDAO aulaPersonaDAO;

    @Autowired
    private EstudiosDAO estudioDAO;

    @Autowired
    private TipoEstudioDAO tipoEstudioDAO;

    @Before
    @Transactional
    public void creaDatosIniciales() throws RegistroNoEncontradoException
    {
        Centro centro = new CentroBuilder(centroDAO).withNombre("Centro de prueba").build();
        centroId = centro.getId();

        Edificio edificio = new EdificioBuilder().withNombre("Edificio 1").withCentro(centro)
                .build();
        this.edificio = edificio.getNombre();

        Semestre semestre = new Semestre();
        semestre.setSemestre(1L);
        this.semestre = semestre;

        tipoEstudio = new TipoEstudioBuilder(tipoEstudioDAO).withId("PR").withNombre("Pruebas")
                .build();

        estudio = new EstudioBuilder(estudioDAO).withNombre("Estudio de prueba")
                .withTipoEstudio(tipoEstudio).withCentro(centro).withOficial(true).build();

        PlantaEdificio plantaEdificio1 = new PlantaEdificioBuilder().withNombre("Planta 1")
                .withEdificio(edificio).build();
        this.plantaEdificio = plantaEdificio1.getNombre();
        PlantaEdificio plantaEdificio2 = new PlantaEdificioBuilder().withNombre("Planta 2")
                .withEdificio(edificio).build();

        AreaEdificio areaEdificio1 = new AreaEdificioBuilder().withNombre("Area 1")
                .withEdificio(edificio).build();
        AreaEdificio areaEdificio2 = new AreaEdificioBuilder().withNombre("Area 2")
                .withEdificio(edificio).build();

        TipoAula tipoAula = new TipoAulaBuilder().withNombre("Tipo Aula 1").withEdificio(edificio)
                .build();
        this.tipoAula = tipoAula.getNombre();
        TipoAula tipoAula2 = new TipoAulaBuilder().withNombre("Tipo Aula 2").withEdificio(edificio)
                .build();

        Aula aula1 = new AulaBuilder(aulaDAO).withNombre("Aula 1").withArea(areaEdificio1)
                .withTipo(tipoAula).withCentro(centro).withPlanta(plantaEdificio1)
                .withEdificio(edificio).build();
        Aula aula2 = new AulaBuilder(aulaDAO).withNombre("Aula 2").withArea(areaEdificio1).withTipo(tipoAula2)
                .withCentro(centro).withPlanta(plantaEdificio1).withEdificio(edificio).build();
        new AulaBuilder(aulaDAO).withNombre("Aula 3").withArea(areaEdificio1).withTipo(tipoAula2)
                .withCentro(centro).withPlanta(plantaEdificio1).withEdificio(edificio).build();
        new AulaBuilder(aulaDAO).withNombre("Aula 4").withArea(areaEdificio2).withTipo(tipoAula)
                .withCentro(centro).withPlanta(plantaEdificio2).withEdificio(edificio).build();
        new AulaBuilder(aulaDAO).withNombre("Aula 5").withArea(areaEdificio2).withTipo(tipoAula)
                .withCentro(centro).withPlanta(plantaEdificio2).withEdificio(edificio).build();
        new AulaBuilder(aulaDAO).withNombre("Aula 6").withArea(areaEdificio2).withTipo(tipoAula2)
                .withCentro(centro).withPlanta(plantaEdificio2).withEdificio(edificio).build();

        new AulaPlanificacionBuilder(aulaDAO).withAula(aula1).withSemestre(semestre)
                .withEstudio(estudio).build();
        new AulaPlanificacionBuilder(aulaDAO).withAula(aula2).withSemestre(semestre)
                .withEstudio(estudio).build();

        new AulaPersonaBuilder(aulaPersonaDAO).withAula(aula1).withPersonaId(1L).withCentro(centro).build();
        new AulaPersonaBuilder(aulaPersonaDAO).withAula(aula2).withPersonaId(1L).withCentro(centro).build();

    }

    @Test
    @Transactional
    public void recuperaTiposAulaSegunCentroYEdificio()
    {
        MultivaluedMap<String, String> params = new StringKeyStringValueIgnoreCaseMultivaluedMap();
        params.putSingle("centroId", String.valueOf(centroId));
        params.putSingle("edificio", edificio);
        params.putSingle("semestreId", semestre.getSemestre().toString());

        ClientResponse response = resource.path("aula/tipo").queryParams(params)
                .accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);

        List<UIEntity> tiposAula = response.getEntity(new GenericType<List<UIEntity>>()
        {
        });

        assertThat(tiposAula, hasSize(2));
    }

    @Test
    @Transactional
    public void recuperaAulasPorCentroYEdificio()
    {
        MultivaluedMap<String, String> params = new StringKeyStringValueIgnoreCaseMultivaluedMap();
        params.putSingle("centroId", String.valueOf(centroId));
        params.putSingle("edificio", edificio);

        ClientResponse response = resource.path("aula").queryParams(params)
                .accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);

        List<UIEntity> tiposAula = response.getEntity(new GenericType<List<UIEntity>>()
        {
        });

        assertThat(tiposAula, hasSize(6));
    }

    @Test
    @Transactional
    public void recuperaAulasPorCentroEdificioYTipo()
    {
        MultivaluedMap<String, String> params = new StringKeyStringValueIgnoreCaseMultivaluedMap();
        params.putSingle("centroId", String.valueOf(centroId));
        params.putSingle("edificio", edificio);
        params.putSingle("tipoAula", tipoAula);

        ClientResponse response = resource.path("aula").queryParams(params)
                .accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);

        List<UIEntity> tiposAula = response.getEntity(new GenericType<List<UIEntity>>()
        {
        });

        assertThat(tiposAula, hasSize(4));
    }

    @Test
    @Transactional
    public void recuperaAulasPorCentroEdificioYPlanta()
    {
        MultivaluedMap<String, String> params = new StringKeyStringValueIgnoreCaseMultivaluedMap();
        params.putSingle("centroId", String.valueOf(centroId));
        params.putSingle("edificio", edificio);
        params.putSingle("planta", plantaEdificio);

        ClientResponse response = resource.path("aula").queryParams(params)
                .accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);

        List<UIEntity> tiposAula = response.getEntity(new GenericType<List<UIEntity>>()
        {
        });

        assertThat(tiposAula, hasSize(3));
    }

    @Test
    @Transactional
    public void recuperaAulasPorCentroEdificioTipoYPlanta()
    {
        MultivaluedMap<String, String> params = new StringKeyStringValueIgnoreCaseMultivaluedMap();
        params.putSingle("centroId", String.valueOf(centroId));
        params.putSingle("edificio", edificio);
        params.putSingle("tipoAula", tipoAula);
        params.putSingle("planta", plantaEdificio);

        ClientResponse response = resource.path("aula").queryParams(params)
                .accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);

        List<UIEntity> tiposAula = response.getEntity(new GenericType<List<UIEntity>>()
        {
        });

        assertThat(tiposAula, hasSize(2));
    }
}
